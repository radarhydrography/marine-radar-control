/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "radarcommunicatorudp.h"

#include <QDataStream>

RadarCommunicatorUdp::RadarCommunicatorUdp(QObject *parent) : QThread(parent)
{
    m_mutex = new QMutex;

    setPcHostAddress();
    setRadarHostAddress();

    m_commandsBuffer = new QList<QByteArray>;
}

RadarCommunicatorUdp::~RadarCommunicatorUdp()
{
    if (m_workingThreadEnable) {

        stopWorkingThread();
        msleep(1);
    }

    m_udpWriteSocket->close();
    m_udpReadSocket->close();
}

void RadarCommunicatorUdp::run()
{
    if (m_workingThreadEnable) {
        emit poolStatus(true);

        m_udpWriteSocket = new QUdpSocket;
        m_udpReadSocket = new QUdpSocket;

        m_udpWriteSocket->abort();
        m_udpReadSocket->abort();

        m_udpWriteSocket->bind(m_pcHostAddress, m_pcWritePort);
        m_udpReadSocket->bind(m_pcHostAddress, m_pcReadPort);

        m_udpWriteSocket->open(QUdpSocket::ReadWrite);
        m_udpReadSocket->open(QUdpSocket::ReadOnly);
    }

    RadarMessageUdp statusRequest(RadarHeaderE::Inquire, RadarAddressBytesE::Status);

    QByteArray answer;
    unsigned char currentRegisterAddress;
    unsigned char currentRegisterValue;
    unsigned char statusRegisterValue;
    QByteArray statusMessage;
    statusMessage.append(3, 0);

    while (m_workingThreadEnable) {
        m_mutex->lock();
        if (m_commandsBuffer->isEmpty()) {
            answer = sendControlMessage(RadarMessageUdp::updateMessage());
        } else {
            answer = sendControlMessage(m_commandsBuffer->at(0));
            m_commandsBuffer->removeAt(0);
        }
        m_mutex->unlock();
        if (answer.length() == 1034) {
            currentRegisterAddress = answer[6];
            currentRegisterValue = answer[7];
            statusRegisterValue = answer[1032];
            statusMessage[0] = currentRegisterAddress;
            statusMessage[1] = currentRegisterValue;
            statusMessage[2] = statusRegisterValue;
            emit SignalStatusMessageReceived(statusMessage);
        } else if (answer.isEmpty()) {
            emit SignalNetworkError();
            emit SignalstatusMessage(QString("Radar Network Error: No Answer!"));
        } else if (answer.length() != 1034) {
            emit SignalNetworkError();
            emit SignalstatusMessage(QString("Radar Network Error: Wrong Answer Length!"));
        }
        msleep(5);
    }

    if (!m_workingThreadEnable) {
        emit poolStatus(false);
        emit SignalDisconnect();
    }
    m_mutex->lock();
    m_udpWriteSocket->close();
    m_udpReadSocket->close();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

void RadarCommunicatorUdp::setPcHostAddress(const QString &hostAddress, const int &pcReadPort, const int &pcWritePort)
{
    m_pcHostAddress = QHostAddress(hostAddress);
    m_pcReadPort = pcReadPort;
    m_pcWritePort = pcWritePort;
}

void RadarCommunicatorUdp::setRadarHostAddress(const QString &hostAddress, const int &radarReadPort, const int &radarWritePort)
{
    m_radarHostAddress = QHostAddress(hostAddress);
    m_radarReadPort = radarReadPort;
    m_radarWritePort = radarWritePort;
}

bool RadarCommunicatorUdp::isWorking()
{
    m_mutex->lock();
    bool status = m_workingThreadEnable;
    m_mutex->unlock();
    return status;
}

void RadarCommunicatorUdp::setRegisters(const RadarAddressE &address, const unsigned char &data)
{
     m_mutex->lock();
     m_commandsBuffer->append(RadarMessageUdp(RadarHeaderE::WriteReg, address, data).message());
     m_mutex->unlock();
}

void RadarCommunicatorUdp::setRegisters(const QList<RadarAddressBytesE> &addressList, const QByteArray &messageList)
{
    if (!messageList.isEmpty() && addressList.size() == messageList.size()) {
        RadarMessageUdp message;
        m_mutex->lock();
        m_commandsBuffer->append(RadarMessageUdp::complexMessage(addressList, messageList));
        m_mutex->unlock();
    }
}

QByteArray RadarCommunicatorUdp::sendControlMessage(const QByteArray &message)
{
    emit timeoutError(false);
    if (message.isEmpty()) {
        return 0;
    }
    m_udpWriteSocket->writeDatagram(message, m_radarHostAddress, m_radarReadPort);
    msleep(5);
    QByteArray answer = m_udpReadSocket->receiveDatagram().data();
    return answer;
}

void RadarCommunicatorUdp::startWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = true;
    m_mutex->unlock();

    if (!isRunning()) {
        start();
    }
}

void RadarCommunicatorUdp::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}
