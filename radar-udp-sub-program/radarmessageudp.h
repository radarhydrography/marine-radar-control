/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef RADARMESSAGEUDP_H
#define RADARMESSAGEUDP_H

#include "radarstructsudp.h"

#include <QObject>

class RadarMessageUdp : public QObject
{
    Q_OBJECT

    RadarHeaderE m_header;
    RadarAddressE m_address;
    unsigned char m_data;
    QByteArray m_dataArray;

public:
    explicit RadarMessageUdp(QObject *parent = nullptr);
    RadarMessageUdp(const RadarMessageUdp &radarMessageUdp, QObject *parent = nullptr);
    RadarMessageUdp operator=(const RadarMessageUdp &radarMessageUdp);
    RadarMessageUdp(const RadarHeaderE &header, const RadarAddressE &address, QObject *parent = nullptr);
    RadarMessageUdp(const RadarHeaderE &header, const RadarAddressE &address, const unsigned char &data, QObject *parent = nullptr);
    RadarMessageUdp(const RadarHeaderE &header, const RadarAddressE &address, const QByteArray &dataArray, QObject *parent = nullptr);

    void setHeader(const RadarHeaderE &header)        {m_header = header;}
    void setAddress(const RadarAddressE &address)     {m_address = address;}
    void setData(const unsigned char &data)         {m_data = data;}
    void setDataArray(const QByteArray &dataArray)  {m_dataArray = dataArray;}

    RadarHeaderE      getHeader()     {return m_header;}
    RadarAddressE     getAddress()    {return m_address;}
    unsigned char   getData()       {return m_data;}
    QByteArray      getDataArray()  {return m_dataArray;}

    static QByteArray message(const RadarHeaderE &header, const QByteArray &data);
    static QByteArray message(const RadarHeaderE &header, const unsigned char &address, const unsigned char &data);
    QByteArray message();

    static QByteArray updateMessage();
    static QByteArray complexMessage(const QList<RadarAddressBytesE> &addressList, const QByteArray &messageList);


signals:

};

#endif // RADARMESSAGEUDP_H
