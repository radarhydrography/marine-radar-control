/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef RADARSTRUCTSUDP_H
#define RADARSTRUCTSUDP_H

#include <QString>
#include <QStringList>

enum struct RadarHeaderE
{
    WriteReg                =   0x60,
    Inquire                 =   0x70
};

enum struct RadarAddressBytesE
{
    Trigger_0               =   0x00,
    Trigger_1               =   0x01,
    Trigger_2               =   0x02,

    TriggerDelay_0          =   0x03,
    TriggerDelay_1          =   0x04,

    ADControl               =   0x05,

    Misc_0                  =   0x06,
    Misc_1                  =   0x07,

    HLSetup_0               =   0x08,
    HLSetup_1               =   0x09,

    Enable                  =   0x0A,

    TuneCoarse              =   0x0B,
    TuneFine                =   0x0C,
    TuneReg                 =   0x0D,

    Setup                   =   0x0F,

    GateTune                =   0x10,

    AutoTuneFine            =   0x11,
    AutotuneReg             =   0x12,

    AntSpeed                =   0x13,

    MasterIP_Low            =   0x16,

    MAGIndicator            =   0x1D,
    TuneIndicator           =   0x1E,

    Status                  =   0x1F,

    PowerUpTimer            =   0x20,

    Debug                   =   0x21,

    VeEionMajor             =   0x3A,
    VeEionMinor             =   0x3B,
    VeEionDay               =   0x3C,
    VeEionMonth             =   0x3D,
    VeEionYear_0            =   0x3E,
    VeEionYear_1            =   0x3F,

    EthernetTest            =   0xFE,

    Update                  =   0xFF
};

struct RadarAddressE
{
    RadarAddressBytesE m_address;

    RadarAddressE()
    {
        m_address = addressBytesE(0);
    }

    RadarAddressE(const unsigned char &address)
    {
        m_address = addressBytesE(address);
    }

    RadarAddressE(const RadarAddressBytesE &address)
    {
        m_address = address;
    }

    inline unsigned char radarAddressE()
    {
        return radarAddressE(m_address);
    }

    inline void setRadarAddress(const quint8 &number)
    {
        m_address = num2radarAddress(number);
    }

    inline QString address()
    {
        return address(m_address);
    }

    inline static RadarAddressBytesE addressBytesE(const unsigned char &addressByteArray)
    {
        return static_cast<RadarAddressBytesE>(static_cast<quint8>((addressByteArray)));
    }

    inline static unsigned char radarAddressE(const RadarAddressBytesE &radarAddress)
    {
        return (static_cast<quint8>(radarAddress));
    }

    inline static QString address(RadarAddressBytesE radarAddress)
    {
        switch (radarAddress) {
        case RadarAddressBytesE::Trigger_0        :   return QString("Trigger_0");
        case RadarAddressBytesE::Trigger_1        :   return QString("Trigger_1");
        case RadarAddressBytesE::Trigger_2        :   return QString("Trigger_2");
        case RadarAddressBytesE::TriggerDelay_0   :   return QString("TriggerDelay_0");
        case RadarAddressBytesE::TriggerDelay_1   :   return QString("TriggerDelay_1");
        case RadarAddressBytesE::ADControl        :   return QString("ADControl");
        case RadarAddressBytesE::Misc_0           :   return QString("Misc_0");
        case RadarAddressBytesE::Misc_1           :   return QString("Misc_1");
        case RadarAddressBytesE::HLSetup_0        :   return QString("HLSetup_0");
        case RadarAddressBytesE::HLSetup_1        :   return QString("HLSetup_1");
        case RadarAddressBytesE::Enable           :   return QString("Enable");
        case RadarAddressBytesE::TuneCoarse       :   return QString("TuneCoaEe");
        case RadarAddressBytesE::TuneFine         :   return QString("TuneFine");
        case RadarAddressBytesE::TuneReg          :   return QString("TuneReg");
        case RadarAddressBytesE::Setup            :   return QString("Setup");
        case RadarAddressBytesE::GateTune         :   return QString("GateTune");
        case RadarAddressBytesE::AutoTuneFine     :   return QString("AutoTuneFine");
        case RadarAddressBytesE::AutotuneReg      :   return QString("AutotuneReg");
        case RadarAddressBytesE::AntSpeed         :   return QString("AntSpeed");
        case RadarAddressBytesE::MasterIP_Low     :   return QString("MasterIP_Low");
        case RadarAddressBytesE::MAGIndicator     :   return QString("MAGIndicator");
        case RadarAddressBytesE::TuneIndicator    :   return QString("TuneIndicator");
        case RadarAddressBytesE::Status           :   return QString("Status");
        case RadarAddressBytesE::PowerUpTimer     :   return QString("PowerUpTimer");
        case RadarAddressBytesE::Debug            :   return QString("Debug");
        case RadarAddressBytesE::VeEionMajor     :   return QString("VeEionMajor");
        case RadarAddressBytesE::VeEionMinor     :   return QString("VeEionMinor");
        case RadarAddressBytesE::VeEionDay       :   return QString("VeEionDay");
        case RadarAddressBytesE::VeEionMonth     :   return QString("VeEionMonth");
        case RadarAddressBytesE::VeEionYear_0    :   return QString("VeEionYear_0");
        case RadarAddressBytesE::VeEionYear_1    :   return QString("VeEionYear_1");
        case RadarAddressBytesE::EthernetTest     :   return QString("EthernetTest");
        case RadarAddressBytesE::Update           :   return QString("Update");
        default                                 :   return QString("Undefined");
        }
    }

    inline static QStringList addressList()
    {
        QStringList addressList;
        addressList.append(QString("Trigger_0"));
        addressList.append(QString("Trigger_1"));
        addressList.append(QString("Trigger_2"));
        addressList.append(QString("TriggerDelay_0"));
        addressList.append(QString("TriggerDelay_1"));
        addressList.append(QString("ADControl"));
        addressList.append(QString("Misc_0"));
        addressList.append(QString("Misc_1"));
        addressList.append(QString("HLSetup_0"));
        addressList.append(QString("HLSetup_1"));
        addressList.append(QString("Enable"));
        addressList.append(QString("TuneCoase"));
        addressList.append(QString("TuneFine"));
        addressList.append(QString("TuneReg"));
        addressList.append(QString("Setup"));
        addressList.append(QString("GateTune"));
        addressList.append(QString("AutoTuneFine"));
        addressList.append(QString("AutotuneReg"));
        addressList.append(QString("AntSpeed"));
        addressList.append(QString("MasterIP_Low"));
        addressList.append(QString("MAGIndicator"));
        addressList.append(QString("TuneIndicator"));
        addressList.append(QString("Status"));
        addressList.append(QString("PowerUpTimer"));
        addressList.append(QString("Debug"));
        addressList.append(QString("VeEionMajor"));
        addressList.append(QString("VeEionMinor"));
        addressList.append(QString("VeEionDay"));
        addressList.append(QString("VeEionMonth"));
        addressList.append(QString("VeEionYear_0"));
        addressList.append(QString("VeEionYear_1"));
        addressList.append(QString("EthernetTest"));
        addressList.append(QString("Update"));
        return addressList;
    }

    inline static RadarAddressBytesE num2radarAddress(const quint8 &number)
    {
        switch (number) {
        case 0  :   return RadarAddressBytesE::Trigger_0;
        case 1  :   return RadarAddressBytesE::Trigger_1;
        case 2  :   return RadarAddressBytesE::Trigger_2;
        case 3  :   return RadarAddressBytesE::TriggerDelay_0;
        case 4  :   return RadarAddressBytesE::TriggerDelay_1;
        case 5  :   return RadarAddressBytesE::ADControl;
        case 6  :   return RadarAddressBytesE::Misc_0;
        case 7  :   return RadarAddressBytesE::Misc_1;
        case 8  :   return RadarAddressBytesE::HLSetup_0;
        case 9  :   return RadarAddressBytesE::HLSetup_1;
        case 10 :   return RadarAddressBytesE::Enable;
        case 11 :   return RadarAddressBytesE::TuneCoarse;
        case 12 :   return RadarAddressBytesE::TuneFine;
        case 13 :   return RadarAddressBytesE::TuneReg;
        case 14 :   return RadarAddressBytesE::Setup;
        case 15 :   return RadarAddressBytesE::GateTune;
        case 16 :   return RadarAddressBytesE::AutoTuneFine;
        case 17 :   return RadarAddressBytesE::AutotuneReg;
        case 18 :   return RadarAddressBytesE::AntSpeed;
        case 19 :   return RadarAddressBytesE::MasterIP_Low;
        case 20 :   return RadarAddressBytesE::MAGIndicator;
        case 21 :   return RadarAddressBytesE::TuneIndicator;
        case 22 :   return RadarAddressBytesE::Status;
        case 23 :   return RadarAddressBytesE::PowerUpTimer;
        case 24 :   return RadarAddressBytesE::Debug;
        case 25 :   return RadarAddressBytesE::VeEionMajor;
        case 26 :   return RadarAddressBytesE::VeEionMinor;
        case 27 :   return RadarAddressBytesE::VeEionDay;
        case 28 :   return RadarAddressBytesE::VeEionMonth;
        case 29 :   return RadarAddressBytesE::VeEionYear_0;
        case 30 :   return RadarAddressBytesE::VeEionYear_1;
        case 31 :   return RadarAddressBytesE::EthernetTest;
        case 32 :   return RadarAddressBytesE::Update;
        default :   return RadarAddressBytesE::Update;
        }
    }
};

enum struct RadarTriggerGeneratorStateE
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarTriggerPeriodE
{
    PRF3200Hz               =   0x03D08,
    PRF1600Hz               =   0x07A11,
    PRF800Hz                =   0x0D423,
    PRF500Hz                =   0x1869F
};

struct RadarTriggerE
{
    RadarTriggerGeneratorStateE m_triggerGeneratorState;
    quint8 m_staggerValue;
    RadarTriggerPeriodE m_triggerPeriod;

    RadarTriggerE()
    {
        m_triggerGeneratorState = RadarTriggerGeneratorStateE::Disabled;
        m_staggerValue = 0;
        m_triggerPeriod = RadarTriggerPeriodE::PRF500Hz;
    }
    RadarTriggerE(const RadarTriggerE &radarTriggerE)
    {
        m_triggerGeneratorState = radarTriggerE.m_triggerGeneratorState;
        m_staggerValue = radarTriggerE.m_staggerValue;
        m_triggerPeriod = radarTriggerE.m_triggerPeriod;
    }
    RadarTriggerE(const QByteArray &triggerByteArray)
    {
        m_triggerGeneratorState = triggerGeneratorStateE(triggerByteArray);
        m_staggerValue = staggerValueE(triggerByteArray);
        m_triggerPeriod = triggerPeriodE(triggerByteArray);
    }
    RadarTriggerE(const QList<QByteArray> &triggerMessages)
    {
        QByteArray triggerByteArray;
        triggerByteArray.append(triggerMessages[0][2]);
        triggerByteArray.append(triggerMessages[1][2]);
        triggerByteArray.append(triggerMessages[2][2]);
        m_triggerGeneratorState = triggerGeneratorStateE(triggerByteArray);
        m_staggerValue = staggerValueE(triggerByteArray);
        m_triggerPeriod = triggerPeriodE(triggerByteArray);
    }
    RadarTriggerE operator=(const RadarTriggerE &radarTriggerE)
    {
        m_triggerGeneratorState = radarTriggerE.m_triggerGeneratorState;
        m_staggerValue = radarTriggerE.m_staggerValue;
        m_triggerPeriod = radarTriggerE.m_triggerPeriod;
        return *this;
    }

    inline QByteArray radarTriggerE()
    {
        return radarTriggerE(m_triggerGeneratorState, m_staggerValue, m_triggerPeriod);
    }

    inline static RadarTriggerGeneratorStateE triggerGeneratorStateE(const QByteArray &triggerByteArray)
    {
        return static_cast<RadarTriggerGeneratorStateE>(((static_cast<quint8>(triggerByteArray[2]) >> 7) & 0b1));
    }

    inline static quint8 staggerValueE(const QByteArray &staggerByteArray)
    {
        return ((static_cast<quint8>(staggerByteArray[2]) >> 3) & 0b1111);
    }

    inline static RadarTriggerPeriodE triggerPeriodE(const QByteArray &triggerPeriodByteArray)
    {
        return static_cast<RadarTriggerPeriodE>(((static_cast<quint8>(triggerPeriodByteArray[2]) & 0b1) << 16) |
                                               (static_cast<quint8>(triggerPeriodByteArray[1]) << 8) |
                                                static_cast<quint8>(triggerPeriodByteArray[0]));
    }

    inline static QByteArray radarTriggerE(const RadarTriggerGeneratorStateE &triggerGeneratorState,
                                         const quint8 &staggerValue, const RadarTriggerPeriodE &triggerPeriod)
    {
        quint32 triggeE{0};

        triggeE = static_cast<quint32>(triggerGeneratorState);
        triggeE = triggeE << 4;
        triggeE |= (static_cast<quint8>(staggerValue) & 0b1111);
        triggeE = triggeE << 19;
        triggeE |= static_cast<quint32>(triggerPeriod);

        QByteArray radartriggeE;
        radartriggeE.append(triggeE);
        radartriggeE.append(triggeE >> 8);
        radartriggeE.append(triggeE >> 16);

        return radartriggeE;
    }

    inline quint8 getTriggerGenetatorState()
    {
        return static_cast<quint8>(m_triggerGeneratorState);
    }

    inline quint8 getTriggerStaggerValue()
    {
        return m_staggerValue;
    }

    inline quint8 getTriggerPeriod()
    {
        switch (m_triggerPeriod) {
        case RadarTriggerPeriodE::PRF500Hz : {
            return 0;
        }
        case RadarTriggerPeriodE::PRF800Hz : {
            return 1;
        }
        case RadarTriggerPeriodE::PRF1600Hz : {
            return 2;
        }
        case RadarTriggerPeriodE::PRF3200Hz : {
            return 3;
        }
        }
        return 0;
    }

    inline void setTriggerGenetatorState(const quint8 &GeneratorStateIndex)
    {
        switch (GeneratorStateIndex) {
        case 0: {
            m_triggerGeneratorState = RadarTriggerGeneratorStateE::Disabled;
            break;
        }
        case 1: {
            m_triggerGeneratorState = RadarTriggerGeneratorStateE::Enabled;
            break;
        }
        default:
            m_triggerGeneratorState = RadarTriggerGeneratorStateE::Disabled;
        }
    }

    inline void setTriggerStaggerValue(const quint8 &staggerValue)
    {
        m_staggerValue = 0b1111 & staggerValue;
    }

    inline void setTriggerPeriod(const quint8 &triggerPeriodIndex)
    {
        switch (triggerPeriodIndex) {
        case  0: {
            m_triggerPeriod = RadarTriggerPeriodE::PRF500Hz;
            break;
        }
        case  1: {
            m_triggerPeriod = RadarTriggerPeriodE::PRF800Hz;
            break;
        }
        case  2: {
            m_triggerPeriod = RadarTriggerPeriodE::PRF1600Hz;
            break;
        }
        case  3: {
            m_triggerPeriod = RadarTriggerPeriodE::PRF3200Hz;
            break;
        }
        default: {
            m_triggerPeriod = RadarTriggerPeriodE::PRF500Hz;
        }
        }
    }
};

struct RadarTriggerDelayE
{
    quint16 m_triggerDelayValue;

    RadarTriggerDelayE()
    {
        m_triggerDelayValue = static_cast<quint16>(0);
    }

    RadarTriggerDelayE(QByteArray triggerDelayValueArray)
    {
        while (triggerDelayValueArray.size() < 2) {
            triggerDelayValueArray.append(static_cast<quint8>(0));
        }
        m_triggerDelayValue = static_cast<quint16>(triggerDelayValueArray[1] << 8 | triggerDelayValueArray[0]);
    }

    inline QByteArray radarTriggerDelay()
    {
        return radarTriggerDelay(m_triggerDelayValue);
    }

    inline static quint16 triggerDelay(const unsigned char &coaEeByteArray)
    {
        return static_cast<quint16>(coaEeByteArray);
    }

    inline static QByteArray radarTriggerDelay(const quint16 &triggerDelayValue)
    {
        QByteArray triggerDelayByteArray;
        triggerDelayByteArray += static_cast<quint8>(triggerDelayValue);
        triggerDelayByteArray += static_cast<quint8>(triggerDelayValue >> 8);
        return triggerDelayByteArray;
    }

    inline void setTriggerDelayValue(const quint16 &triggerDelayValue)
    {
        m_triggerDelayValue = triggerDelayValue;
    }
};

enum struct RadarADFsE
{
    ADSF50000kHz                =   0b00,
    ADSF48387kHz                =   0b01,
    ADSF52273kHz                =   0b10
};

struct RadarADControlE
{
    RadarADFsE m_radarADFs;

    RadarADControlE()
    {
        m_radarADFs = RadarADFsE(0);
    }

    RadarADControlE(const unsigned char radarADFsByteArray)
    {
        m_radarADFs = radarADFsE(radarADFsByteArray);
    }

    inline unsigned char radarADControl()
    {
        return radarADControl(m_radarADFs);
    }

    inline static RadarADFsE radarADFsE(const unsigned char &radarADFsByteArray)
    {
        return static_cast<RadarADFsE>(static_cast<quint8>(radarADFsByteArray));
    }

    inline unsigned char radarADControl(const RadarADFsE &radarADFs)
    {
        return static_cast<quint8>(radarADFs);
    }

    inline void setRadarADFs(const quint8 &radarADFsIndex)
    {
        switch (radarADFsIndex) {
        case 0: {
            m_radarADFs = RadarADFsE::ADSF50000kHz;
            break;
        }
        case 1: {
            m_radarADFs = RadarADFsE::ADSF48387kHz;
            break;
        }
        case 2: {
            m_radarADFs = RadarADFsE::ADSF52273kHz;
            break;
        }
        }
    }

    inline quint8 getRadarADFs()
    {
        switch (m_radarADFs) {
        case RadarADFsE::ADSF50000kHz : {
            return 0;
        }
        case RadarADFsE::ADSF48387kHz : {
            return 1;
        }
        case RadarADFsE::ADSF52273kHz : {
            return 2;
        }
        default : {
            return 0;
        }
        }
    }

};

enum struct RadarMiscZeroBlankMemComtrolE
{
    SerialContol            =   0b0,
    EthernetControl         =   0b1
};

enum struct RadarMiscZeroTestACPE
{
    NormalOperation         =   0b0,
    AcpArpInternalGen       =   0b1
};

enum struct RadarMiscZeroAutoTuneFineE
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarMiscZeroAutoTuneRegE
{
    NormalOperation         =   0b0,
    AutomaticSearching      =   0b1
};

enum struct RadarMiscZeroDivideACPInputE
{
    NoDivide                =   0b00,
    DivideByTwo             =   0b01,
    DivideByFour            =   0b10,
    DivideByEight           =   0b11
};

struct RadarMiscZeroE
{
    RadarMiscZeroBlankMemComtrolE m_blankMemControl;
    RadarMiscZeroTestACPE m_testACP;
    RadarMiscZeroAutoTuneFineE m_autoTuneFine;
    RadarMiscZeroAutoTuneRegE m_autoTuneReg;
    RadarMiscZeroDivideACPInputE m_divideACPInput;

    RadarMiscZeroE()
    {
        m_blankMemControl = blankMemControlE(0);
        m_testACP = testACPE(0);
        m_autoTuneFine = autoTuneFineE(0);
        m_autoTuneReg = autoTuneRegE(0);
        m_divideACPInput = divideACPInputE(0);
    }

    RadarMiscZeroE(const unsigned char &miscZeroByteArray)
    {
        m_blankMemControl = blankMemControlE(miscZeroByteArray);
        m_testACP = testACPE(miscZeroByteArray);
        m_autoTuneFine = autoTuneFineE(miscZeroByteArray);
        m_autoTuneReg = autoTuneRegE(miscZeroByteArray);
        m_divideACPInput = divideACPInputE(miscZeroByteArray);
    }

    inline unsigned char radarMiscZero()
    {
        return radarMiscZero(m_blankMemControl, m_testACP, m_autoTuneFine, m_autoTuneReg, m_divideACPInput);
    }

    inline static RadarMiscZeroBlankMemComtrolE blankMemControlE(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroBlankMemComtrolE>(((static_cast<quint8>(miscZeroByteArray) >> 6) & 0b1));
    }

    inline static RadarMiscZeroTestACPE testACPE(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroTestACPE>(((static_cast<quint8>(miscZeroByteArray) >> 5) & 0b1));
    }

    inline static RadarMiscZeroAutoTuneFineE autoTuneFineE(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroAutoTuneFineE>(((static_cast<quint8>(miscZeroByteArray) >> 3) & 0b1));
    }

    inline static RadarMiscZeroAutoTuneRegE autoTuneRegE(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroAutoTuneRegE>(((static_cast<quint8>(miscZeroByteArray) >> 2) & 0b1));
    }

    inline static RadarMiscZeroDivideACPInputE divideACPInputE(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroDivideACPInputE>((static_cast<quint8>(miscZeroByteArray)&0b11));
    }

    inline static unsigned char radarMiscZero(const RadarMiscZeroBlankMemComtrolE &blankMemControl,
                                            const RadarMiscZeroTestACPE &testACP,
                                            const RadarMiscZeroAutoTuneFineE &autoTuneFine,
                                            const RadarMiscZeroAutoTuneRegE &autoTuneReg,
                                            const RadarMiscZeroDivideACPInputE &divideACPInput)
    {
        return (static_cast<quint8>(blankMemControl) << 6) |
               (static_cast<quint8>(testACP) << 5) |
               (static_cast<quint8>(autoTuneFine) << 3) |
               (static_cast<quint8>(autoTuneReg) << 2) |
               (static_cast<quint8>(divideACPInput));
    }

    inline void setBlankMemControl(const quint8 &blankMemControlIndex)
    {
        switch (blankMemControlIndex) {
        case 0: {
            m_blankMemControl = RadarMiscZeroBlankMemComtrolE::SerialContol;
            break;
        }
        case 1: {
            m_blankMemControl = RadarMiscZeroBlankMemComtrolE::EthernetControl;
            break;
        }
        default: {
            m_blankMemControl = RadarMiscZeroBlankMemComtrolE::SerialContol;
        }
        }
    }

    inline void setAutotuneReg(const quint8 &autotuneRegIndex)
    {
        switch (autotuneRegIndex) {
        case 0: {
            m_autoTuneReg = RadarMiscZeroAutoTuneRegE::NormalOperation;
            break;
        }
        case 1: {
            m_autoTuneReg = RadarMiscZeroAutoTuneRegE::AutomaticSearching;
            break;
        }
        default: {
            m_autoTuneReg = RadarMiscZeroAutoTuneRegE::NormalOperation;
        }
        }
    }

    inline void setTestACP(const quint8 &testACPIndex)
    {
        switch (testACPIndex) {
        case 0: {
            m_testACP = RadarMiscZeroTestACPE::NormalOperation;
            break;
        }
        case 1: {
            m_testACP = RadarMiscZeroTestACPE::AcpArpInternalGen;
            break;
        }
        default: {
            m_testACP = RadarMiscZeroTestACPE::NormalOperation;
        }
        }
    }

    inline void setAutotuneFine(const quint8 &autoTuneFineIndex)
    {
        switch (autoTuneFineIndex) {
        case 0: {
            m_autoTuneFine = RadarMiscZeroAutoTuneFineE::Disabled;
            break;
        }
        case 1: {
            m_autoTuneFine = RadarMiscZeroAutoTuneFineE::Enabled;
            break;
        }
        default: {
            m_autoTuneFine = RadarMiscZeroAutoTuneFineE::Disabled;
        }
        }
    }

    inline void setDivideACPInputSignal(const quint8 &devideACPInputSignalIndex)
    {
        switch (devideACPInputSignalIndex) {
        case 0: {
            m_divideACPInput = RadarMiscZeroDivideACPInputE::NoDivide;
            break;
        }
        case 1: {
            m_divideACPInput = RadarMiscZeroDivideACPInputE::DivideByTwo;
            break;
        }
        case 2: {
            m_divideACPInput = RadarMiscZeroDivideACPInputE::DivideByFour;
            break;
        }
        case 3: {
            m_divideACPInput = RadarMiscZeroDivideACPInputE::DivideByEight;
            break;
        }
        default: {
            m_divideACPInput = RadarMiscZeroDivideACPInputE::NoDivide;
        }
        }
    }

    inline quint8 getBlankMemControl()
    {
        switch (m_blankMemControl) {
        case RadarMiscZeroBlankMemComtrolE::SerialContol: {
            return 0;
        }
        case RadarMiscZeroBlankMemComtrolE::EthernetControl: {
            return 1;
        }
        default: {
            return 0;
        }
        }
    }

    inline quint8 getTestACP()
    {
        switch (m_testACP) {
        case RadarMiscZeroTestACPE::NormalOperation : {
            return 0;
        }
        case RadarMiscZeroTestACPE::AcpArpInternalGen : {
            return 1;
        }
        }
    }

    inline quint8 getAutotuneFine()
    {
        switch (m_autoTuneFine) {
        case RadarMiscZeroAutoTuneFineE::Disabled : {
            return 0;
        }
        case RadarMiscZeroAutoTuneFineE::Enabled : {
            return 1;
        }
        }
    }

    inline quint8 getAutotuneReg()
    {
        switch (m_autoTuneReg) {
        case RadarMiscZeroAutoTuneRegE::NormalOperation : {
            return 0;
        }
        case RadarMiscZeroAutoTuneRegE::AutomaticSearching : {
            return 1;
        }
        }
    }

    inline quint8 getDivideACPInputSignal()
    {
        switch (m_divideACPInput) {
        case RadarMiscZeroDivideACPInputE::NoDivide : {
            return 0;
        }
        case RadarMiscZeroDivideACPInputE::DivideByTwo : {
            return 1;
        }
        case RadarMiscZeroDivideACPInputE::DivideByFour : {
            return 2;
        }
        case RadarMiscZeroDivideACPInputE::DivideByEight : {
            return 3;
        }
        }
    }

};

enum struct RadarMiscOneVideoSampleResE
{
    DefaultBS               =   0b00,
    BSdivide2               =   0b01,
    BSdivide5               =   0b10,
    BSdivide10              =   0b11
};

enum struct RadarMiscOneAcpPeriodE
{
    ACP2048Pulses           =   0b001,
    ACP4096Pulses           =   0b010
};

enum struct RadarMiscOneBlankingRamE
{
    BlockFrom0to511         =   0b0,
    BlockFrom512to1023      =   0b1
};

enum struct RadarMiscOneBandwithE
{
    WideBand                =   0b0,
    NarrowBand              =   0b1
};

struct RadarMiscOneE
{
    RadarMiscOneVideoSampleResE m_videoSampleRes;
    RadarMiscOneAcpPeriodE m_acpPeriod;
    RadarMiscOneBlankingRamE m_blankingRam;
    RadarMiscOneBandwithE m_bandwith;

    RadarMiscOneE()
    {
        m_videoSampleRes = videoSampleResE(0);
        m_acpPeriod = acpPeriodE(0);
        m_blankingRam = blankingRamE(0);
        m_bandwith = bandwithE(0);
    }

    RadarMiscOneE(const unsigned char &miscOneByteArray)
    {
        m_videoSampleRes = videoSampleResE(miscOneByteArray);
        m_acpPeriod = acpPeriodE(miscOneByteArray);
        m_blankingRam = blankingRamE(miscOneByteArray);
        m_bandwith = bandwithE(miscOneByteArray);
    }

    inline unsigned char radarMiscOne()
    {
        return radarMiscOne(m_videoSampleRes, m_acpPeriod, m_blankingRam, m_bandwith);
    }

    inline static RadarMiscOneVideoSampleResE videoSampleResE(const unsigned char &videoSampleResByteArray)
    {
        return static_cast<RadarMiscOneVideoSampleResE>(((static_cast<quint8>(videoSampleResByteArray) >> 6) & 0b11));
    }

    inline static RadarMiscOneAcpPeriodE acpPeriodE(const unsigned char &miscOneByteArray)
    {
        return static_cast<RadarMiscOneAcpPeriodE>(((static_cast<quint8>(miscOneByteArray) >> 3) & 0b111));
    }

    inline static RadarMiscOneBlankingRamE blankingRamE(const unsigned char &miscOneByteArray)
    {
        return static_cast<RadarMiscOneBlankingRamE>(((static_cast<quint8>(miscOneByteArray) >> 2) & 0b1));
    }

    inline static RadarMiscOneBandwithE bandwithE(const unsigned char &miscOneByteArray)
    {
        return static_cast<RadarMiscOneBandwithE>((static_cast<quint8>(miscOneByteArray) & 0b1));
    }

    inline static unsigned char radarMiscOne(const RadarMiscOneVideoSampleResE &videoSampleRes,
                                           const RadarMiscOneAcpPeriodE &acpPeriod,
                                           const RadarMiscOneBlankingRamE &blankingRam,
                                           const RadarMiscOneBandwithE &bandwidth)
    {
        return (static_cast<quint8>(videoSampleRes) << 6) |
               (static_cast<quint8>(acpPeriod) << 3) |
               (static_cast<quint8>(blankingRam) << 2) |
               (static_cast<quint8>(bandwidth));
    }


    inline void setVideoSampleRes(const quint8 &videoSampleResIndex)
    {
        switch (videoSampleResIndex) {
        case 0: {
            m_videoSampleRes = RadarMiscOneVideoSampleResE::DefaultBS;
            break;
        }
        case 1: {
            m_videoSampleRes = RadarMiscOneVideoSampleResE::BSdivide2;
            break;
        }
        case 2: {
            m_videoSampleRes = RadarMiscOneVideoSampleResE::BSdivide5;
            break;
        }
        case 3: {
            m_videoSampleRes = RadarMiscOneVideoSampleResE::BSdivide10;
            break;
        }
        default: {
            m_videoSampleRes = RadarMiscOneVideoSampleResE::DefaultBS;
        }
        }
    }

    inline void setAcpPeriod(const quint8 &acpPeriodIndex)
    {
        switch (acpPeriodIndex) {
        case 0: {
            m_acpPeriod = RadarMiscOneAcpPeriodE::ACP2048Pulses;
            break;
        }
        case 1: {
            m_acpPeriod = RadarMiscOneAcpPeriodE::ACP4096Pulses;
            break;
        }
        default: {
            m_acpPeriod = RadarMiscOneAcpPeriodE::ACP2048Pulses;
        }
        }
    }

    inline void setBlankingRam(const quint8 &blankingRamIndex)
    {
        switch (blankingRamIndex) {
        case 0: {
            m_blankingRam = RadarMiscOneBlankingRamE::BlockFrom0to511;
            break;
        }
        case 1: {
            m_blankingRam = RadarMiscOneBlankingRamE::BlockFrom512to1023;
            break;
        }
        default: {
            m_blankingRam = RadarMiscOneBlankingRamE::BlockFrom0to511;
        }
        }
    }

    inline void setBandwith(const quint8 &bandwithIndex)
    {
        switch (bandwithIndex) {
        case 0: {
            m_bandwith = RadarMiscOneBandwithE::WideBand;
            break;
        }
        case 1: {
            m_bandwith = RadarMiscOneBandwithE::NarrowBand;
            break;
        }
        default: {
            m_bandwith = RadarMiscOneBandwithE::WideBand;
        }
        }
    }

    inline quint8 getVideoSampleRes()
    {
        switch (m_videoSampleRes) {
        case RadarMiscOneVideoSampleResE::DefaultBS: {
            return 0;
        }
        case RadarMiscOneVideoSampleResE::BSdivide2: {
            return 1;
        }
        case RadarMiscOneVideoSampleResE::BSdivide5: {
            return 2;
        }
        case RadarMiscOneVideoSampleResE::BSdivide10: {
            return 3;
        }
        default: {
            return 0;
        }
        }
    }

    inline quint8 getAcpPeriod()
    {
        switch (m_acpPeriod) {
        case RadarMiscOneAcpPeriodE::ACP2048Pulses : {
            return 0;
        }
        case RadarMiscOneAcpPeriodE::ACP4096Pulses : {
            return 1;
        }
        }
    }

    inline quint8 getBlankingRam()
    {
        switch (m_blankingRam) {
        case RadarMiscOneBlankingRamE::BlockFrom0to511 : {
            return 0;
        }
        case RadarMiscOneBlankingRamE::BlockFrom512to1023 : {
            return 1;
        }
        }
    }

    inline quint8 getBandwith()
    {
        switch (m_bandwith) {
        case RadarMiscOneBandwithE::WideBand : {
            return 0;
        }
        case RadarMiscOneBandwithE::NarrowBand : {
            return 1;
        }
        }
    }
};

enum struct RadarHlSetupWidthE
{
    AcpHalfPulse            =   0b00,
    AcpOnePulse             =   0b01,
    AcpTwoPulses            =   0b10,
    AcpFourPulses           =   0b11
};

enum struct RadarHlSetupInvertE
{
    SamePolarity            =   0b0,
    InversePolarity         =   0b1
};

enum struct RadarHlSetupEdgeE
{
    FallingEdge             =   0b0,
    RisingEdge              =   0b1
};

struct RadarHlSetupE
{
    RadarHlSetupWidthE m_widgth;
    RadarHlSetupInvertE m_invert;
    RadarHlSetupEdgeE m_edge;
    quint16 m_delayValue;

    RadarHlSetupE()
    {
        m_widgth = RadarHlSetupWidthE(0);
        m_invert = RadarHlSetupInvertE(0);
        m_edge = RadarHlSetupEdgeE(0);
        m_delayValue = 0;
    }

    RadarHlSetupE(const QByteArray &hlSetupByteArray)
    {
        m_widgth = widgthE(hlSetupByteArray);
        m_invert = invertE(hlSetupByteArray);
        m_edge = edgeE(hlSetupByteArray);
        m_delayValue = delayValueE(hlSetupByteArray);
    }

    inline QByteArray radarHlSetup()
    {
        return radarHlSetup(m_widgth, m_invert, m_edge, m_delayValue);
    }

    inline unsigned char radarHlSetupTwo()
    {
        return radarHlSetupTwo(m_widgth, m_invert, m_edge, m_delayValue);
    }

    inline unsigned char radarHlSetupOne()
    {
        return radarHlSetupOne(m_delayValue);
    }

    inline static RadarHlSetupWidthE widgthE(const QByteArray &hlSetupByteArray)
    {
        return static_cast<RadarHlSetupWidthE>(((static_cast<quint8>(hlSetupByteArray[1]) >> 6) & 0b11));
    }

    inline static RadarHlSetupInvertE invertE(const QByteArray &hlSetupByteArray)
    {
        return static_cast<RadarHlSetupInvertE>(((static_cast<quint8>(hlSetupByteArray[1]) >> 5) & 0b1));
    }

    inline static RadarHlSetupEdgeE edgeE(const QByteArray &hlSetupByteArray)
    {
        return static_cast<RadarHlSetupEdgeE>(((static_cast<quint8>(hlSetupByteArray[1]) >> 4) & 0b1));
    }

    inline static quint16 delayValueE(const QByteArray &hlSetupByteArray)
    {
        return static_cast<quint16>(((static_cast<quint8>(hlSetupByteArray[1]) & 0b1111) << 8) |
                                    (static_cast<quint8>(hlSetupByteArray[0])));
    }

    inline static QByteArray radarHlSetup(const RadarHlSetupWidthE &widgth,
                                        const RadarHlSetupInvertE &invert,
                                        const RadarHlSetupEdgeE &edge,
                                        const quint16 &delayValue)
    {
        QByteArray hlSetup;
        hlSetup += static_cast<quint8>(delayValue);
        hlSetup += (static_cast<quint8>(widgth) << 6) | (static_cast<quint8>(invert) << 5) |
                   (static_cast<quint8>(edge) << 4) | (0b1111&(static_cast<quint8>(delayValue >> 8)));
        return hlSetup;
    }

    inline static unsigned char radarHlSetupTwo(const RadarHlSetupWidthE &widgth,
                                              const RadarHlSetupInvertE &invert,
                                              const RadarHlSetupEdgeE &edge,
                                              const quint16 &delayValue)
    {
        return static_cast<unsigned char>((static_cast<quint8>(widgth) << 6) | (static_cast<quint8>(invert) << 5) |
                                          (static_cast<quint8>(edge) << 4) | (0b1111&(static_cast<quint8>(delayValue >> 8))));
    }

    inline static unsigned char radarHlSetupOne(const quint16 &delayValue)
    {
        return static_cast<unsigned char>(static_cast<quint8>(delayValue));
    }

    inline void setWidth(const quint8 &widgthIndex)
    {
        switch (widgthIndex) {
        case 0: {
            m_widgth = RadarHlSetupWidthE::AcpHalfPulse;
            break;
        }
        case 1: {
            m_widgth = RadarHlSetupWidthE::AcpOnePulse;
            break;
        }
        case 2: {
            m_widgth = RadarHlSetupWidthE::AcpTwoPulses;
            break;
        }
        case 3: {
            m_widgth = RadarHlSetupWidthE::AcpFourPulses;
            break;
        }
        default: {
            m_widgth = RadarHlSetupWidthE::AcpHalfPulse;
        }
        }
    }

    inline void setInvert(const quint8 &invertIndex)
    {
        switch (invertIndex) {
        case 0: {
            m_invert = RadarHlSetupInvertE::SamePolarity;
            break;
        }
        case 1: {
            m_invert = RadarHlSetupInvertE::InversePolarity;
            break;
        }
        default: {
            m_invert = RadarHlSetupInvertE::SamePolarity;
        }
        }
    }

    inline void setEdge(const quint8 &edgeIndex)
    {
        switch (edgeIndex) {
        case 0: {
            m_edge = RadarHlSetupEdgeE::FallingEdge;
            break;
        }
        case 1: {
            m_edge = RadarHlSetupEdgeE::RisingEdge;
            break;
        }
        default: {
            m_edge = RadarHlSetupEdgeE::FallingEdge;
        }
        }
    }

    inline void setDelayValue(const quint16 &delayValue)
    {
        m_delayValue = delayValue;
    }

    inline quint8 getWidth()
    {
        switch (m_widgth) {
        case RadarHlSetupWidthE::AcpHalfPulse : {
            return 0;
        }
        case RadarHlSetupWidthE::AcpOnePulse : {
            return 1;
        }
        case RadarHlSetupWidthE::AcpTwoPulses : {
            return 2;
        }
        case RadarHlSetupWidthE::AcpFourPulses : {
            return 3;
        }
        }
    }

    inline quint8 getInvert()
    {
        switch (m_invert) {
        case RadarHlSetupInvertE::SamePolarity : {
            return 0;
        }
        case RadarHlSetupInvertE::InversePolarity : {
            return 1;
        }
        }
    }

    inline quint8 getEdge()
    {
        switch (m_edge) {
        case RadarHlSetupEdgeE::FallingEdge : {
            return 0;
        }
        case RadarHlSetupEdgeE::RisingEdge : {
            return 1;
        }
        }
    }

    inline quint16 getDelayValue()
    {
        return m_delayValue;
    }
};

enum struct RadarEnableVideoAcquisitionE
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarEnableDoubleBufferE
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarEnableAutoTuneE
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarEnableDAConvertersE
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarEnableADConvertersE
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

struct RadarEnableE
{
    RadarEnableVideoAcquisitionE m_videoAcquisition;
    RadarEnableDoubleBufferE m_doubleBuffer;
    RadarEnableAutoTuneE m_autoTune;
    RadarEnableDAConvertersE m_dac;
    RadarEnableADConvertersE m_adc;

    RadarEnableE()
    {
        m_videoAcquisition = videoAcquisitionE(0);
        m_doubleBuffer = doubleBufferE(0);
        m_autoTune = autoTuneE(0);
        m_dac = dacE(0);
        m_adc = adcE(0);
    }

    RadarEnableE(const unsigned char &enableByteArray)
    {
        m_videoAcquisition = videoAcquisitionE(enableByteArray);
        m_doubleBuffer = doubleBufferE(enableByteArray);
        m_autoTune = autoTuneE(enableByteArray);
        m_dac = dacE(enableByteArray);
        m_adc = adcE(enableByteArray);
    }

    inline unsigned char radarEnableE()
    {
        return radarEnableE(m_videoAcquisition, m_doubleBuffer, m_autoTune, m_dac, m_adc);
    }

    inline static RadarEnableVideoAcquisitionE videoAcquisitionE(const unsigned char &videoAcquisitionByteArray)
    {
        return static_cast<RadarEnableVideoAcquisitionE>(((static_cast<quint8>(videoAcquisitionByteArray) >> 4) & 0b1));
    }

    inline static RadarEnableDoubleBufferE doubleBufferE(const unsigned char &doubleBufferByteArray)
    {
        return static_cast<RadarEnableDoubleBufferE>(((static_cast<quint8>(doubleBufferByteArray) >> 3) & 0b1));
    }

    inline static RadarEnableAutoTuneE autoTuneE(const unsigned char &enableByteArray)
    {
        return static_cast<RadarEnableAutoTuneE>(((static_cast<quint8>(enableByteArray) >> 2) & 0b1));
    }

    inline static RadarEnableDAConvertersE dacE(const unsigned char &enableByteArray)
    {
        return static_cast<RadarEnableDAConvertersE>(((static_cast<quint8>(enableByteArray) >> 1) & 0b1));
    }

    inline static RadarEnableADConvertersE adcE(const unsigned char &enableByteArray)
    {
        return static_cast<RadarEnableADConvertersE>((static_cast<quint8>(enableByteArray) & 0b1));
    }

    inline static unsigned char radarEnableE(const RadarEnableVideoAcquisitionE &videoAcquisition,
                                           const RadarEnableDoubleBufferE &doubleBuffer,
                                           const RadarEnableAutoTuneE &autoTune,
                                           const RadarEnableDAConvertersE &dac,
                                           const RadarEnableADConvertersE &adc)
    {
        return (static_cast<quint8>(videoAcquisition) << 4) |
               (static_cast<quint8>(doubleBuffer) << 3) |
               (static_cast<quint8>(autoTune) << 2) |
               (static_cast<quint8>(dac) << 1) |
               (static_cast<quint8>(adc));
    }

    inline void setVideoAcquisition(const quint8 &videoAcquisitionIndex)
    {
        switch (videoAcquisitionIndex) {
        case 0: {
            m_videoAcquisition = RadarEnableVideoAcquisitionE::Disabled;
            break;
        }
        case 1: {
            m_videoAcquisition = RadarEnableVideoAcquisitionE::Enabled;
            break;
        }
        default: {
            m_videoAcquisition = RadarEnableVideoAcquisitionE::Disabled;
        }
        }
    }

    inline void setDoubleBuffer(const quint8 &doubleBufferIndex)
    {
        switch (doubleBufferIndex) {
        case 0: {
            m_doubleBuffer = RadarEnableDoubleBufferE::Disabled;
            break;
        }
        case 1: {
            m_doubleBuffer = RadarEnableDoubleBufferE::Enabled;
            break;
        }
        default: {
            m_doubleBuffer = RadarEnableDoubleBufferE::Disabled;
        }
        }
    }

    inline void setAutoTune(const quint8 &autoTuneIndex)
    {
        switch (autoTuneIndex) {
        case 0: {
            m_autoTune = RadarEnableAutoTuneE::Disabled;
            break;
        }
        case 1: {
            m_autoTune = RadarEnableAutoTuneE::Enabled;
            break;
        }
        default: {
            m_autoTune = RadarEnableAutoTuneE::Disabled;
        }
        }
    }

    inline void setDac(const quint8 &dacIndex)
    {
        switch (dacIndex) {
        case 0: {
            m_dac = RadarEnableDAConvertersE::Disabled;
            break;
        }
        case 1: {
            m_dac = RadarEnableDAConvertersE::Enabled;
            break;
        }
        default: {
            m_dac = RadarEnableDAConvertersE::Disabled;
        }
        }
    }

    inline void setAdc(const quint8 &adcIndex)
    {
        switch (adcIndex) {
        case 0: {
            m_adc = RadarEnableADConvertersE::Disabled;
            break;
        }
        case 1: {
            m_adc = RadarEnableADConvertersE::Enabled;
            break;
        }
        default: {
            m_adc = RadarEnableADConvertersE::Disabled;
        }
        }
    }

    inline quint8 getVideoAcquisition()
    {
        switch (m_videoAcquisition) {
        case RadarEnableVideoAcquisitionE::Disabled : {
            return 0;
        }
        case RadarEnableVideoAcquisitionE::Enabled : {
            return 1;
        }
        default : {
            return 0;
        }
        }
    }

    inline quint8 getDoubleBuffer()
    {
        switch (m_doubleBuffer) {
        case RadarEnableDoubleBufferE::Disabled : {
            return 0;
        }
        case RadarEnableDoubleBufferE::Enabled : {
            return 1;
        }
        default : {
            return 0;
        }
        }
    }

    inline quint8 getAutoTune()
    {
        switch (m_autoTune) {
        case RadarEnableAutoTuneE::Disabled : {
            return 0;
        }
        case RadarEnableAutoTuneE::Enabled : {
            return 1;
        }
        }
    }

    inline quint8 getDac()
    {
        switch (m_dac) {
        case RadarEnableDAConvertersE::Disabled : {
            return 0;
        }
        case RadarEnableDAConvertersE::Enabled : {
            return 1;
        }
        }
    }

    inline quint8 getAdc()
    {
        switch (m_adc) {
        case RadarEnableADConvertersE::Disabled : {
            return 0;
        }
        case RadarEnableADConvertersE::Enabled : {
            return 1;
        }
        }
    }
};

struct RadarTuneCoarseE
{
    quint8 m_coarseValue;

    RadarTuneCoarseE()
    {
        m_coarseValue = static_cast<quint8>(0);
    }

    RadarTuneCoarseE(const unsigned char &coarseByteArray)
    {
        m_coarseValue = static_cast<quint8>(coarseByteArray);
    }

    inline unsigned char radarTuneCoarseE()
    {
        return radarTuneCoarseE(m_coarseValue);
    }

    inline static quint8 coarseE(const unsigned char &coarseByteArray)
    {
        return static_cast<quint8>(coarseByteArray);
    }

    inline static unsigned char radarTuneCoarseE(const quint8 &coarseValue)
    {
        return coarseValue;
    }

    inline void setCoarseValue(const quint8 &coarseValue)
    {
        m_coarseValue = coarseValue;
    }
};

struct RadarTuneFineE
{
    quint8 m_fineValue;

    RadarTuneFineE()
    {
        m_fineValue = static_cast<quint8>(0);
    }

    RadarTuneFineE(const unsigned char &fineByteArray)
    {
        m_fineValue = static_cast<quint8>(fineByteArray);
    }

    inline unsigned char radarTuneFineE()
    {
        return radarTuneFineE(m_fineValue);
    }

    inline static quint8 fineE(const unsigned char &fineByteArray)
    {
        return static_cast<quint8>(fineByteArray);
    }

    inline static unsigned char radarTuneFineE(const quint8 &fineValue)
    {
        return fineValue;
    }

    inline void setFineValue(const quint8 &fineValue)
    {
        m_fineValue = fineValue;
    }

    inline quint8 getFineValue()
    {
        return m_fineValue;
    }
};

struct RadarTuneRegE
{
    quint8 m_regValue;

    RadarTuneRegE()
    {
        m_regValue = static_cast<quint8>(0);
    }

    RadarTuneRegE(const unsigned char &regByteArray)
    {
        m_regValue = static_cast<quint8>(regByteArray);
    }

    inline unsigned char radarTuneRegE()
    {
        return radarTuneRegE(m_regValue);
    }

    inline static quint8 regE(const unsigned char &regByteArray)
    {
        return static_cast<quint8>(regByteArray);
    }

    inline static unsigned char radarTuneRegE(const quint8 &regValue)
    {
        return regValue;
    }

    inline void setRegValue(const quint8 &regValue)
    {
        m_regValue = regValue;
    }

    inline quint8 getRegValue()
    {
        return m_regValue;
    }
};

enum struct RadarSetupTransceiverModeE
{
    TransceiverStandBy      =   0b0,
    TransceiverTxMode       =   0b1
};

enum struct RadarSetupPulseModeE
{
    ExtraLongPulse          =   0b000,
    LongPolse               =   0b010,
    MediumPulse             =   0b100,
    ShortPulse              =   0b110,
    ExtraShortPulse         =   0b111
};

enum struct RadarSetupEnableMotorE
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

struct RadarSetupE
{
    RadarSetupTransceiverModeE m_transceiverMode;
    RadarSetupPulseModeE m_pulseMode;
    RadarSetupEnableMotorE m_enableMotor;

    RadarSetupE()
    {
        m_transceiverMode = transceiverModeE(0);
        m_pulseMode = pulseModeE(0);
        m_enableMotor = enableMotorE(0);
    }

    RadarSetupE(const unsigned char &setupByteArray)
    {
        m_transceiverMode = transceiverModeE(setupByteArray);
        m_pulseMode = pulseModeE(setupByteArray);
        m_enableMotor = enableMotorE(setupByteArray);
    }

    inline unsigned char radarSetupE()
    {
        return radarSetupE(m_transceiverMode, m_pulseMode, m_enableMotor);
    }

    inline static RadarSetupTransceiverModeE transceiverModeE(const unsigned char &setupByteArray)
    {
        return static_cast<RadarSetupTransceiverModeE>(((static_cast<quint8>((setupByteArray)) >> 7) & 0b1));
    }
    inline static RadarSetupPulseModeE pulseModeE(const unsigned char &setupByteArray)
    {
        return static_cast<RadarSetupPulseModeE>(((static_cast<quint8>((setupByteArray)) >> 4) & 0b111));
    }
    inline static RadarSetupEnableMotorE enableMotorE(const unsigned char &setupByteArray)
    {
        return static_cast<RadarSetupEnableMotorE>(((static_cast<quint8>((setupByteArray)) >> 1) & 0b1));
    }

    inline static unsigned char radarSetupE(const RadarSetupTransceiverModeE &transceiverMode,
                                           const RadarSetupPulseModeE &pulseMode,
                                           const RadarSetupEnableMotorE &enableMotor)
    {
        return ((static_cast<quint8>(transceiverMode) << 7) |
               (static_cast<quint8>(pulseMode) << 4) |
               (static_cast<quint8>(enableMotor) << 1));
    }

    inline void setTransceiverMode(const quint8 &transceiverModeIndex)
    {
        switch (transceiverModeIndex) {
        case 0: {
            m_transceiverMode = RadarSetupTransceiverModeE::TransceiverStandBy;
            break;
        }
        case 1: {
            m_transceiverMode = RadarSetupTransceiverModeE::TransceiverTxMode;
            break;
        }
        default: {
            m_transceiverMode = RadarSetupTransceiverModeE::TransceiverStandBy;
        }
        }
    }

    inline void setPulseMode(const quint8 &pulseModeIndex)
    {
        switch (pulseModeIndex) {
        case 0: {
            m_pulseMode = RadarSetupPulseModeE::ExtraLongPulse;
            break;
        }
        case 1: {
            m_pulseMode = RadarSetupPulseModeE::LongPolse;
            break;
        }
        case 2: {
            m_pulseMode = RadarSetupPulseModeE::MediumPulse;
            break;
        }
        case 3: {
            m_pulseMode = RadarSetupPulseModeE::ShortPulse;
            break;
        }
        case 4: {
            m_pulseMode = RadarSetupPulseModeE::ExtraShortPulse;
            break;
        }
        default: {
            m_pulseMode = RadarSetupPulseModeE::ExtraLongPulse;
        }
        }
    }

    inline void setEnableMotor(const quint8 &enableMotorIndex)
    {
        switch (enableMotorIndex) {
        case 0: {
            m_enableMotor = RadarSetupEnableMotorE::Disabled;
            break;
        }
        case 1: {
            m_enableMotor = RadarSetupEnableMotorE::Enabled;
            break;
        }
        default: {
            m_enableMotor = RadarSetupEnableMotorE::Disabled;
        }
        }
    }

    inline quint8 getTransceiverMode()
    {
        switch (m_transceiverMode) {
        case RadarSetupTransceiverModeE::TransceiverStandBy : {
            return 0;
        }
        case RadarSetupTransceiverModeE::TransceiverTxMode : {
            return 1;
        }
        }
    }

    inline quint8 getPulseMode()
    {
        switch (m_pulseMode) {
        case RadarSetupPulseModeE::ExtraLongPulse : {
            return 0;
        }
        case RadarSetupPulseModeE::LongPolse : {
            return 1;
        }
        case RadarSetupPulseModeE::MediumPulse : {
            return 2;
        }
        case RadarSetupPulseModeE::ShortPulse : {
            return 3;
        }
        case RadarSetupPulseModeE::ExtraShortPulse : {
            return 4;
        }
        default : {
            return 0;
        }
        }
    }

    inline quint8 getEnableMotor()
    {
        switch (m_enableMotor) {
        case RadarSetupEnableMotorE::Disabled : {
            return 0;
        }
        case RadarSetupEnableMotorE::Enabled : {
            return 1;
        }
        }
    }
};

struct RadarGateTuneDelayE
{
    quint8 m_tuneDelay;

    RadarGateTuneDelayE()
    {
        m_tuneDelay = static_cast<quint8>(0);
    }

    RadarGateTuneDelayE(const unsigned char &tuneDelayByteArray)
    {
        m_tuneDelay = static_cast<quint8>(tuneDelayByteArray);
    }

    inline unsigned char radarGateTuneDelayE()
    {
        return radarGateTuneDelayE(m_tuneDelay);
    }

    inline static quint8 tuneDelayE(const unsigned char &tuneDelayByteArray)
    {
        return static_cast<quint8>(tuneDelayByteArray);
    }

    inline static unsigned char radarGateTuneDelayE(const quint8 &tuneDelayValue)
    {
        return tuneDelayValue;
    }

    inline void setTuneDelayValue(const quint8 &tuneDelayValue)
    {
        m_tuneDelay = tuneDelayValue;
    }

    inline quint8 getTuneDelayValue()
    {
        return m_tuneDelay;
    }
};

struct RadarAutoTuneFineE
{
    quint8 m_fineAutoTuneValue;

    RadarAutoTuneFineE();
    RadarAutoTuneFineE(const unsigned char &fineAutoTuneByteArray)
    {
        m_fineAutoTuneValue = static_cast<quint8>(fineAutoTuneByteArray);
    }

    inline static quint8 regAutoTuneE(const unsigned char &fineAutoTuneByteArray)
    {
        return static_cast<quint8>(fineAutoTuneByteArray);
    }

    inline quint8 getFineAutoTuneValue()
    {
        return m_fineAutoTuneValue;
    }
};

struct RadarAutoTuneRegE
{
    quint8 m_regAutoTuneValue;

    RadarAutoTuneRegE();
    RadarAutoTuneRegE(const unsigned char &regAutoTuneByteArray)
    {
        m_regAutoTuneValue = static_cast<quint8>(regAutoTuneByteArray);
    }

    inline static quint8 regAutoTuneE(const unsigned char &regAutoTuneByteArray)
    {
        return static_cast<quint8>(regAutoTuneByteArray);
    }

    inline quint8 getRegAutoTuneValue()
    {
        return m_regAutoTuneValue;
    }
};

enum struct RadarAntSpeedControlE
{
    HalfStepControlled      =   0b0,
    FullStepControlled      =   0b1
};

enum struct RadarAntSpeedValueE
{
    Speed33RPM              =   0b00,
    Speed30RPM              =   0b01,
    Speed27RPM              =   0b10,
    Speed24RPM              =   0b11
};

struct RadarAntSpeedE
{
    RadarAntSpeedControlE m_speedControl;
    RadarAntSpeedValueE m_speedValue;

    RadarAntSpeedE()
    {
        m_speedControl = speedControlE(0);
        m_speedValue = speedValueE(0);
    }

    RadarAntSpeedE(const unsigned char antSpeedByteArray)
    {
        m_speedControl = speedControlE(antSpeedByteArray);
        m_speedValue = speedValueE(antSpeedByteArray);
    }

    inline unsigned char radarAntSpeedE()
    {
        return radarAntSpeedE(m_speedControl, m_speedValue);
    }

    inline static RadarAntSpeedControlE speedControlE(const unsigned char speedControlByteArray)
    {
        return static_cast<RadarAntSpeedControlE>(((static_cast<quint8>(speedControlByteArray) >> 2) &0b1));
    }

    inline static RadarAntSpeedValueE speedValueE(const unsigned char speedValueByteArray)
    {
        return static_cast<RadarAntSpeedValueE>(((static_cast<quint8>(speedValueByteArray)) &0b11));
    }

    inline static unsigned char radarAntSpeedE(const RadarAntSpeedControlE &speedControl,
                                                  const RadarAntSpeedValueE &speedValue)
    {
        return (static_cast<quint8>(speedControl) << 2) |
               (static_cast<quint8>(speedValue));
    }

    inline void setSpeedControl(const quint8 &speedControlIndex)
    {
        switch (speedControlIndex) {
        case 0: {
            m_speedControl = RadarAntSpeedControlE::HalfStepControlled;
            break;
        }
        case 1: {
            m_speedControl = RadarAntSpeedControlE::FullStepControlled;
            break;
        }
        default: {
            m_speedControl = RadarAntSpeedControlE::HalfStepControlled;
        }
        }
    }

    inline void setSpeedValue(const quint8 &speedValueIndex)
    {
        switch (speedValueIndex) {
        case 0: {
            m_speedValue = RadarAntSpeedValueE::Speed33RPM;
            break;
        }
        case 1: {
            m_speedValue = RadarAntSpeedValueE::Speed30RPM;
            break;
        }
        case 2: {
            m_speedValue = RadarAntSpeedValueE::Speed27RPM;
            break;
        }
        case 3: {
            m_speedValue = RadarAntSpeedValueE::Speed24RPM;
            break;
        }
        default: {
            m_speedValue = RadarAntSpeedValueE::Speed33RPM;
        }
        }
    }

    inline quint8 getSpeedControl()
    {
        switch (m_speedControl) {
        case RadarAntSpeedControlE::HalfStepControlled: {
            return 0;
        }
        case RadarAntSpeedControlE::FullStepControlled: {
            return 1;
        }
        default: {
            return 0;
        }
        }
    }

    inline quint8 getSpeedValue()
    {
        switch (m_speedValue) {
        case RadarAntSpeedValueE::Speed33RPM: {
            return 0;
        }
        case RadarAntSpeedValueE::Speed30RPM: {
            return 1;
        }
        case RadarAntSpeedValueE::Speed27RPM: {
            return 2;
        }
        case RadarAntSpeedValueE::Speed24RPM: {
            return 3;
        }
        default: {
            return 0;
        }
        }
    }

};

struct RadarMasterIPLowE
{
    unsigned char m_masterIPLow;

    RadarMasterIPLowE()
    {
        m_masterIPLow = static_cast<unsigned char>(0);
    }

    RadarMasterIPLowE(const unsigned char &masterIPLowByteArray)
    {
        m_masterIPLow = static_cast<unsigned char>(masterIPLowByteArray);
    }

    inline unsigned char radarMasterIPLowE()
    {
        return radarMasterIPLowE(m_masterIPLow);
    }

    inline static unsigned char masterIPLowE(const unsigned char &masterIPLowByteArray)
    {
        return static_cast<unsigned char>(masterIPLowByteArray);
    }

    inline static unsigned char radarMasterIPLowE(const unsigned char &masterIPLowValue)
    {
        return masterIPLowValue;
    }

    inline void setMasterIPLowValue(const unsigned char &masterIPLowValue)
    {
        m_masterIPLow = masterIPLowValue;
    }

    inline unsigned char getMasterIPLowValue()
    {
        return m_masterIPLow;
    }
};

struct RadarMagIndicatorE
{
    quint8 m_magIndicatorValue;

    RadarMagIndicatorE();
    RadarMagIndicatorE(const unsigned char &magIndicatorByteArray)
    {
        m_magIndicatorValue = static_cast<quint8>(magIndicatorByteArray);
    }

    inline static quint8 regAutoTuneE(const unsigned char &magIndicatorByteArray)
    {
        return static_cast<quint8>(magIndicatorByteArray);
    }

    inline quint8 getMagIndicatorValue()
    {
        return m_magIndicatorValue;
    }
};

struct RadarTuneIndicatorE
{
    quint8 m_tuneIndicatorValue;

    RadarTuneIndicatorE();
    RadarTuneIndicatorE(const unsigned char &tuneIndicatorByteArray)
    {
        m_tuneIndicatorValue = static_cast<quint8>(tuneIndicatorByteArray);
    }

    inline static quint8 regAutoTuneE(const unsigned char &tuneIndicatorByteArray)
    {
        return static_cast<quint8>(tuneIndicatorByteArray);
    }

    inline quint8 getTuneIndicatorValue()
    {
        return m_tuneIndicatorValue;
    }
};

enum struct RadarStatusPowerUpCompleteE
{
    PowerUpMode             =   0b0,
    ReadyToTransmit         =   0b1
};

enum struct RadarStatusRegAutoTuneE
{
    NotCompleted            =   0b0,
    Completed               =   0b1
};

enum struct RadarStatusFineAutoTuneE
{
    NotCompleted            =   0b0,
    Completed               =   0b1
};

enum struct RadarStatusAcpFailE
{
    AcpInputSignalOk        =   0b0,
    AcpInputSignalFail      =   0b1
};

enum struct RadarStatusHlFailE
{
    HlInputSignalOk         =   0b0,
    HlInputSignalFail       =   0b1
};

enum struct RadarStatusTriggerFailE
{
    TriggerInputSignalOk    =   0b0,
    TriggerInputSignalFail  =   0b1
};

struct RadarStatusE
{
    RadarStatusPowerUpCompleteE m_powerUpComplete;
    RadarStatusRegAutoTuneE m_regAutiTune;
    RadarStatusFineAutoTuneE m_fineAutoTune;
    RadarStatusAcpFailE m_acpFail;
    RadarStatusHlFailE m_hlFail;
    RadarStatusTriggerFailE m_triggerFail;

    RadarStatusE()
    {
        m_powerUpComplete = powerUpCompleteE(0);
        m_regAutiTune = regAutoTuneE(0);
        m_fineAutoTune = fineAutoTuneE(0);
        m_acpFail = acpFailE(0);
        m_hlFail = hlFailE(0);
        m_triggerFail = triggerpFailE(0);
    }

    RadarStatusE(const unsigned char &radarStatusByteArray)
    {
        m_powerUpComplete = powerUpCompleteE(radarStatusByteArray);
        m_regAutiTune = regAutoTuneE(radarStatusByteArray);
        m_fineAutoTune = fineAutoTuneE(radarStatusByteArray);
        m_acpFail = acpFailE(radarStatusByteArray);
        m_hlFail = hlFailE(radarStatusByteArray);
        m_triggerFail = triggerpFailE(radarStatusByteArray);
    }

    inline static RadarStatusPowerUpCompleteE powerUpCompleteE(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusPowerUpCompleteE>(((static_cast<quint8>((radarStatusByteArray)) >> 7) & 0b1));
    }

    inline static RadarStatusRegAutoTuneE regAutoTuneE(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusRegAutoTuneE>(((static_cast<quint8>((radarStatusByteArray)) >> 4) & 0b1));
    }

    inline static RadarStatusFineAutoTuneE fineAutoTuneE(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusFineAutoTuneE>(((static_cast<quint8>((radarStatusByteArray)) >> 3) & 0b1));
    }

    inline static RadarStatusAcpFailE acpFailE(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusAcpFailE>(((static_cast<quint8>((radarStatusByteArray)) >> 2) & 0b1));
    }

    inline static RadarStatusHlFailE hlFailE(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusHlFailE>(((static_cast<quint8>((radarStatusByteArray)) >> 1) & 0b1));
    }

    inline static RadarStatusTriggerFailE triggerpFailE(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusTriggerFailE>((static_cast<quint8>((radarStatusByteArray)) & 0b1));
    }

    inline quint8  getPowerUpComplete()
    {
        switch (m_powerUpComplete) {
        case RadarStatusPowerUpCompleteE::PowerUpMode : {
            return 0;
        }
        case RadarStatusPowerUpCompleteE::ReadyToTransmit : {
            return 1;
        }
        }
    }

    inline quint8  getRegAutoTune()
    {
        switch (m_regAutiTune) {
        case RadarStatusRegAutoTuneE::NotCompleted : {
            return 0;
        }
        case RadarStatusRegAutoTuneE::Completed : {
            return 1;
        }
        }
    }

    inline quint8  getFineAutoTune()
    {
        switch (m_fineAutoTune) {
        case RadarStatusFineAutoTuneE::NotCompleted : {
            return 0;
        }
        case RadarStatusFineAutoTuneE::Completed : {
            return 1;
        }
        }
    }

    inline quint8  getAcpFail()
    {
        switch (m_acpFail) {
        case RadarStatusAcpFailE::AcpInputSignalOk : {
            return 0;
        }
        case RadarStatusAcpFailE::AcpInputSignalFail : {
            return 1;
        }
        }
    }

    inline quint8  getHlFail()
    {
        switch (m_hlFail) {
        case RadarStatusHlFailE::HlInputSignalOk : {
            return 0;
        }
        case RadarStatusHlFailE::HlInputSignalFail : {
            return 1;
        }
        }
    }

    inline quint8  getTriggerFail()
    {
        switch (m_triggerFail) {
        case RadarStatusTriggerFailE::TriggerInputSignalOk : {
            return 0;
        }
        case RadarStatusTriggerFailE::TriggerInputSignalFail : {
            return 1;
        }
        }
    }
};

struct RadarPowerUpTimerE
{
    quint8 m_powerUpTimer;

    RadarPowerUpTimerE();
    RadarPowerUpTimerE(const unsigned char &powerUpTimerByteArray)
    {
        m_powerUpTimer = static_cast<quint8>((powerUpTimerByteArray));
    }

    inline static quint8 powerUpTimerE(const unsigned char &powerUpTimerByteArray)
    {
        return static_cast<quint8>((powerUpTimerByteArray));
    }
};

enum struct RadarDebugNoMasterModeE
{
    Disabled            =   0b0,
    Enabled             =   0b1
};

enum struct RadarDebugTxTestModeE
{
    Disabled            =   0b0,
    Enabled             =   0b1
};

enum struct RadarDebugPowerUpModeE
{
    Disabled            =   0b0,
    SkipPowerUpTime      =   0b1
};

struct RadarDebugE
{
    RadarDebugNoMasterModeE m_noMasterMode;
    RadarDebugTxTestModeE m_txTestMode;
    RadarDebugPowerUpModeE m_powerUpMode;

    RadarDebugE()
    {
        m_noMasterMode = noMasterModeE(0);
        m_txTestMode = txTestModeE(0);
        m_powerUpMode = powerUpModeE(0);
    }

    RadarDebugE(const unsigned char &radarDebugByteArray)
    {
        m_noMasterMode = noMasterModeE(radarDebugByteArray);
        m_txTestMode = txTestModeE(radarDebugByteArray);
        m_powerUpMode = powerUpModeE(radarDebugByteArray);
    }

    inline unsigned char radarDebugE()
    {
        return radarDebugE(m_noMasterMode, m_txTestMode, m_powerUpMode);
    }

    inline static RadarDebugNoMasterModeE noMasterModeE(const unsigned char &noMasterModeByteArray)
    {
        return static_cast<RadarDebugNoMasterModeE>(((static_cast<quint8>(noMasterModeByteArray) >> 7) &0b1));
    }

    inline static RadarDebugTxTestModeE txTestModeE(const unsigned char &txTestModeByteArray)
    {
        return static_cast<RadarDebugTxTestModeE>(((static_cast<quint8>(txTestModeByteArray) >> 4) &0b1));
    }

    inline static RadarDebugPowerUpModeE powerUpModeE(const unsigned char &powerUpModeByteArray)
    {
        return static_cast<RadarDebugPowerUpModeE>(((static_cast<quint8>(powerUpModeByteArray) >> 2) &0b1));
    }

    inline static unsigned char radarDebugE(const RadarDebugNoMasterModeE &noMasterMode,
                                          const RadarDebugTxTestModeE &txTestMode,
                                          const RadarDebugPowerUpModeE &powerUpMode)
    {
        return (static_cast<quint8>(noMasterMode) << 7) |
               (static_cast<quint8>(txTestMode) << 4) |
               (static_cast<quint8>(powerUpMode) << 2);
    }

    inline void setNoMasterMode(const quint8 &noMasterModeIndex)
    {
        switch (noMasterModeIndex) {
        case 0: {
            m_noMasterMode = RadarDebugNoMasterModeE::Disabled;
            break;
        }
        case 1: {
            m_noMasterMode = RadarDebugNoMasterModeE::Enabled;
            break;
        }
        default: {
            m_noMasterMode = RadarDebugNoMasterModeE::Disabled;
        }
        }
    }

    inline void setTxTestMode(const quint8 &txTestModendex)
    {
        switch (txTestModendex) {
        case 0: {
            m_txTestMode = RadarDebugTxTestModeE::Disabled;
            break;
        }
        case 1: {
            m_txTestMode = RadarDebugTxTestModeE::Enabled;
            break;
        }
        default: {
            m_txTestMode = RadarDebugTxTestModeE::Disabled;
        }
        }
    }

    inline void setPowerUpMode(const quint8 &powerUpModeIndex)
    {
        switch (powerUpModeIndex) {
        case 0: {
            m_powerUpMode = RadarDebugPowerUpModeE::Disabled;
            break;
        }
        case 1: {
            m_powerUpMode = RadarDebugPowerUpModeE::SkipPowerUpTime;
            break;
        }
        default: {
            m_powerUpMode = RadarDebugPowerUpModeE::Disabled;
        }
        }
    }

    inline quint8 getNoMasterMode()
    {
        switch (m_noMasterMode) {
        case RadarDebugNoMasterModeE::Disabled: {
            return 0;
        }
        case RadarDebugNoMasterModeE::Enabled: {
            return 1;
        }
        default: {
            return 0;
        }
        }
    }

    inline quint8 getTxTestMode()
    {
        switch (m_txTestMode) {
        case RadarDebugTxTestModeE::Disabled: {
            return 0;
        }
        case RadarDebugTxTestModeE::Enabled: {
            return 1;
        }
        default: {
            return 0;
        }
        }
    }

    inline quint8 getPowerUpMode()
    {
        switch (m_powerUpMode) {
        case RadarDebugPowerUpModeE::Disabled: {
            return 0;
        }
        case RadarDebugPowerUpModeE::SkipPowerUpTime: {
            return 1;
        }
        default: {
            return 0;
        }
        }
    }
};

struct RadarEthernetResetE
{
    quint8 m_dummyValue = 69;

    RadarEthernetResetE();

    inline unsigned char radarEthernetResetE()
    {
        return radarEthernetResetE(m_dummyValue);
    }

    inline static unsigned char radarEthernetResetE(const quint8 &dummyValue)
    {
        return dummyValue;
    }
};

struct RadarUpdateE
{
    quint8 m_dummyValue = 69;

    RadarUpdateE();

    inline unsigned char radarUpdateE()
    {
        return radarUpdateE(m_dummyValue);
    }

    inline static unsigned char radarUpdateE(const quint8 &dummyValue)
    {
        return dummyValue;
    }
};

#endif // RADARSTRUCTSUDP_H
