/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef RADARCOMMUNICATORUDP_H
#define RADARCOMMUNICATORUDP_H

#include <QThread>
#include <QtNetwork>
#include <QUdpSocket>
#include <QMutex>

#include "radarmessageudp.h"

class RadarCommunicatorUdp : public QThread
{
    Q_OBJECT

    QUdpSocket *m_udpWriteSocket;
    QUdpSocket *m_udpReadSocket;
    QHostAddress m_pcHostAddress;
    quint64 m_pcReadPort;
    quint64 m_pcWritePort;
    QHostAddress m_radarHostAddress;
    quint64 m_radarReadPort;
    quint64 m_radarWritePort;

    QList<QByteArray> *m_commandsBuffer;

    QMutex *m_mutex;
    bool m_workingThreadEnable;

    RadarMessageUdp *m_message;

public:

    explicit RadarCommunicatorUdp(QObject *parent = nullptr);
    ~RadarCommunicatorUdp();

    void run() override;

    void setPcHostAddress(const QString &hostAddress = QString("192.168.42.255"), const int &pcReadPort = 5004, const int &pcWritePort = 5005);
    void setRadarHostAddress(const QString &hostAddress = QString("192.168.42.33"), const int &radarReadPort = 5004, const int &radarWritePort = 5005);

    bool isWorking();

    void setRegisters(const RadarAddressE &address, const unsigned char &data);
    void setRegisters(const QList<RadarAddressBytesE> &addressList, const QByteArray &messageList);

public slots:

    QByteArray sendControlMessage(const QByteArray &message);

    void startWorkingThread();
    void stopWorkingThread();

signals:
    void poolStatus(const bool &threadStart);
    void timeoutError(const bool &error);
    void errorsOccurred(const quint8 &errors);
    void signalPackagesLost(const quint8 &number);
    void signalBytesReceived(const quint64 &bytes);
    void signalBytesWritten(const quint64 &bytes);
    void radarStatus(const RadarStatusE &status);
    void SignalstatusMessage(const QString &message);

    void SignalStatusMessageReceived(const QByteArray &statusMessage);

    void SignalNetworkError();
    void SignalDisconnect();

    void SignalLogEvent(const QString &logEvent);
};

#endif // RADARCOMMUNICATORUDP_H
