/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/
#include "radarudpcontrolui.h"

#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QFrame>

#include "shared-libs/global.h"

RadarUdpControlUI::RadarUdpControlUI(QMap<QString, QString> iniSettings, QWidget *parent) : QWidget(parent)
{
    IniReader::checkConfiguration(iniSettings, SettingsGroup::RadarEthernetSettings);

    m_iniSettings = new QMap<QString, QString>;
    m_iniSettings->insert(iniSettings);

    m_pcHostAddress = iniSettings["HostAddrressPC"];
    m_pcReadPort = iniSettings["ReadPortPC"].toUInt();
    m_pcWritePort = iniSettings["WritePortPC"].toUInt();
    m_radarHostAddress = iniSettings["HostAddressRADAR"];
    m_radarReadPort = iniSettings["ReadPortRADAR"].toUInt();
    m_radarWritePort = iniSettings["WritePortRADAR"].toUInt();

    m_radarCommunicatorUdp = new RadarCommunicatorUdp;

    m_radarCommunicatorUdp->setPcHostAddress(m_pcHostAddress, m_pcReadPort, m_pcWritePort);
    m_radarCommunicatorUdp->setRadarHostAddress(m_radarHostAddress, m_radarReadPort, m_radarWritePort);

    connect(m_radarCommunicatorUdp, &RadarCommunicatorUdp::SignalStatusMessageReceived, this, &RadarUdpControlUI::SlotStatusMessageRceived);

    m_radarTriggerSettings.m_triggerGeneratorState = RadarTriggerGeneratorStateE::Disabled;
    m_radarTriggerSettings.m_triggerPeriod = RadarTriggerPeriodE::PRF3200Hz;
    m_radarTriggerSettings.m_staggerValue = 0;

    m_radarTriggerDelaySettings.m_triggerDelayValue = 0;

    m_radarADSamplingFrequencySettings.m_radarADFs = RadarADFsE::ADSF50000kHz;

    m_radarMiscZeroSettings.m_blankMemControl = RadarMiscZeroBlankMemComtrolE::EthernetControl;
    m_radarMiscZeroSettings.m_testACP = RadarMiscZeroTestACPE::NormalOperation;
    m_radarMiscZeroSettings.m_autoTuneFine = RadarMiscZeroAutoTuneFineE::Disabled;
    m_radarMiscZeroSettings.m_autoTuneReg = RadarMiscZeroAutoTuneRegE::NormalOperation;
    m_radarMiscZeroSettings.m_divideACPInput = RadarMiscZeroDivideACPInputE::NoDivide;

    m_radarMiscOneSettings.m_videoSampleRes = RadarMiscOneVideoSampleResE::DefaultBS;
    m_radarMiscOneSettings.m_acpPeriod = RadarMiscOneAcpPeriodE::ACP2048Pulses;
    m_radarMiscOneSettings.m_blankingRam = RadarMiscOneBlankingRamE::BlockFrom0to511;
    m_radarMiscOneSettings.m_bandwith = RadarMiscOneBandwithE::WideBand;

    m_radarHlSetupSettings.m_widgth = RadarHlSetupWidthE::AcpHalfPulse;
    m_radarHlSetupSettings.m_invert = RadarHlSetupInvertE::SamePolarity;
    m_radarHlSetupSettings.m_edge = RadarHlSetupEdgeE::FallingEdge;
    m_radarHlSetupSettings.m_delayValue = 0;

    m_radarEnableSettings.m_videoAcquisition = RadarEnableVideoAcquisitionE::Disabled;
    m_radarEnableSettings.m_doubleBuffer = RadarEnableDoubleBufferE::Disabled;
    m_radarEnableSettings.m_autoTune = RadarEnableAutoTuneE::Disabled;
    m_radarEnableSettings.m_dac = RadarEnableDAConvertersE::Disabled;
    m_radarEnableSettings.m_adc = RadarEnableADConvertersE::Disabled;

    m_radarGateTuneDelaySettings.m_tuneDelay = 0;

    m_radarAntSpeedSettings.m_speedControl = RadarAntSpeedControlE::HalfStepControlled;
    m_radarAntSpeedSettings.m_speedValue = RadarAntSpeedValueE::Speed33RPM;

    m_radarMasterIPLowSettings.m_masterIPLow = 0;

    m_radarTuneCoarseSettings.m_coarseValue = 0;
    m_radarTuneFineSettings.m_fineValue = 0;
    m_radarTuneRegSettings.m_regValue = 0;

    m_radarDebugSettings.m_noMasterMode = RadarDebugNoMasterModeE::Enabled;
    m_radarDebugSettings.m_txTestMode = RadarDebugTxTestModeE::Disabled;
    m_radarDebugSettings.m_powerUpMode = RadarDebugPowerUpModeE::Disabled;

    m_radarSetupSetiings.m_transceiverMode = RadarSetupTransceiverModeE::TransceiverStandBy;
    m_radarSetupSetiings.m_pulseMode = RadarSetupPulseModeE::ShortPulse;
    m_radarSetupSetiings.m_enableMotor = RadarSetupEnableMotorE::Disabled;

    m_radarTriggerSettingsReceived = m_radarTriggerSettings;
    m_radarTriggerDelaySettingsReceived = m_radarTriggerDelaySettings;
    m_radarADSamplingFrequencySettingsReceived = m_radarADSamplingFrequencySettings;
    m_radarMiscZeroSettingsReceived = m_radarMiscZeroSettings;
    m_radarMiscOneSettingsReceived = m_radarMiscOneSettings;
    m_radarHlSetupSettingsReceived = m_radarHlSetupSettings;
    m_radarEnableSettingsReceived = m_radarEnableSettings;
    m_radarTuneCoarseSettingsReceived = m_radarTuneCoarseSettings;
    m_radarTuneFineSettingsReceived = m_radarTuneFineSettings;
    m_radarTuneRegSettingsReceived = m_radarTuneRegSettings;
    m_radarSetupSetiingsReceived = m_radarSetupSetiings;
    m_radarGateTuneDelaySettingsReceived = m_radarGateTuneDelaySettings;
    m_radarAntSpeedSettingsReceived = m_radarAntSpeedSettings;
    m_radarMasterIPLowSettingsReceived = m_radarMasterIPLowSettings;
    m_radarDebugSettingsReceived = m_radarDebugSettings;

    m_radarTriggerSettingsDefault = m_radarTriggerSettings;
    m_radarTriggerDelaySettingsDefault = m_radarTriggerDelaySettings;
    m_radarADSamplingFrequencySettingsDefault = m_radarADSamplingFrequencySettings;
    m_radarMiscZeroSettingsDefault = m_radarMiscZeroSettings;
    m_radarMiscOneSettingsDefault = m_radarMiscOneSettings;
    m_radarHlSetupSettingsDefault = m_radarHlSetupSettings;
    m_radarEnableSettingsDefault = m_radarEnableSettings;
    m_radarTuneCoarseSettingsDefault = m_radarTuneCoarseSettings;
    m_radarTuneFineSettingsDefault = m_radarTuneFineSettings;
    m_radarTuneRegSettingsDefault = m_radarTuneRegSettings;
    m_radarSetupSetiingsDefault = m_radarSetupSetiings;
    m_radarGateTuneDelaySettingsDefault = m_radarGateTuneDelaySettings;
    m_radarAntSpeedSettingsDefault = m_radarAntSpeedSettings;
    m_radarMasterIPLowSettingsDefault = m_radarMasterIPLowSettings;
    m_radarDebugSettingsDefault = m_radarDebugSettings;

    m_radarBuffer.clear();

    QWidget *timerWidget = TimerWidget();
    QWidget *triggerWidget = TriggerWidget();
    QWidget *triggerDelayWidget = TriggerDelayWidget();
    QWidget *adControlWidget = ADControlWidget();
    QWidget *miscZeroWidget = MiscZeroWidget();
    QWidget *miscOneWidget = MiscOneWidget();
    QWidget *hlSetupWidget = HlSetupWidget();
    QWidget *enableWidget = EnableWidget();
    QWidget *tuneCoarseWidget = TuneCoarseWidget();
    QWidget *tuneFineWidget = TuneFineWidget();
    QWidget *tuneRegWidget = TuneRegWidget();
    QWidget *setupWidget = SetupWidget();
    QWidget *gateTuneDelayWidget = GateTuneDelayWidget();
    QWidget *autotuneFineWidget = AutotuneFineWidget();
    QWidget *autotuneRegWidget = AutotuneRegWidget();
    QWidget *antSpeedWidget = AntSpeedWidget();
    QWidget *masterIPLowWidget = MasterIPLowWidget();
    QWidget *magIndicatorWidget = MagIndicatorWidget();
    QWidget *tuneIndicatorWidget = TuneIndicatorWidget();
    QWidget *debugWidget = DebugWidget();
    QWidget *statusWidget = StatusWidget();
    QWidget *statusWidgetRegular = StatusWidgetRegular();
    QWidget *generalWidget = GeneralWidget();

    QGridLayout *mainLayout = new QGridLayout;

    QGridLayout *tuneAndDelayLayout = new QGridLayout;
    tuneAndDelayLayout->addWidget(triggerDelayWidget, 0, 0);
    tuneAndDelayLayout->addWidget(tuneCoarseWidget, 0, 1);
    tuneAndDelayLayout->addWidget(tuneFineWidget, 1, 0);
    tuneAndDelayLayout->addWidget(tuneRegWidget, 1, 1);

    QGridLayout *indicatorLayout = new QGridLayout;
    indicatorLayout->addWidget(autotuneFineWidget, 0, 0);
    indicatorLayout->addWidget(autotuneRegWidget, 0, 1);
    indicatorLayout->addWidget(magIndicatorWidget, 1, 0);
    indicatorLayout->addWidget(tuneIndicatorWidget, 1, 1);

    QHBoxLayout *gateAndIpTuneLayout = new QHBoxLayout;
    gateAndIpTuneLayout->addWidget(gateTuneDelayWidget);
    gateAndIpTuneLayout->addWidget(masterIPLowWidget);

    QVBoxLayout *rightLayout = new QVBoxLayout;
    rightLayout->addWidget(adControlWidget);
    rightLayout->addWidget(antSpeedWidget);

    QHBoxLayout *controlLayout = new QHBoxLayout;
    controlLayout->addWidget(generalWidget);
    controlLayout->addWidget(timerWidget);

    mainLayout->addLayout(controlLayout, 0, 0, 1, 2);
    mainLayout->addLayout(gateAndIpTuneLayout, 0, 2);
    mainLayout->addWidget(setupWidget, 1, 0);
    mainLayout->addWidget(enableWidget, 1, 1);
    mainLayout->addWidget(triggerWidget, 1, 2);
    mainLayout->addWidget(statusWidgetRegular, 2, 0);
    mainLayout->addWidget(statusWidget, 2, 1);
    mainLayout->addWidget(hlSetupWidget, 2, 2);
    mainLayout->addLayout(tuneAndDelayLayout, 3, 1);
    mainLayout->addLayout(indicatorLayout, 3, 0);
    mainLayout->addWidget(debugWidget, 3, 2);
    mainLayout->addWidget(miscZeroWidget, 4, 1);
    mainLayout->addWidget(miscOneWidget, 4, 2);
    mainLayout->addLayout(rightLayout, 4, 0);

    setLayout(mainLayout);

    m_externalControlWidget = ExternalControlWidget();

    SlotAllowSetup(false);

    m_radarCommunicatorUdp->startWorkingThread();

    m_updateTimer = new QTimer;
    connect(m_updateTimer, &QTimer::timeout, this, &RadarUdpControlUI::SlotAutoUpdate);
    m_updateTimer->start(5000);

    m_statusTimer = new QTimer;
    connect(m_statusTimer, &QTimer::timeout, this, &RadarUdpControlUI::SlotCheckStatus);
    m_statusTimer->start(100);

    m_onlineTimer = new QTimer;
    connect(m_onlineTimer, &QTimer::timeout, this, &RadarUdpControlUI::SlotRadarIsOffline);
    m_onlineTimer->setInterval(100);
    m_onlineTimer->start();

    m_statusMsgTimer = new QTimer;
    m_statusMsgTimer->setInterval(3000);
    connect(m_radarCommunicatorUdp, &RadarCommunicatorUdp::SignalstatusMessage, this, &RadarUdpControlUI::SlotStatusMessageReceived);
    connect(m_statusMsgTimer, &QTimer::timeout, this, &RadarUdpControlUI::SlotClearStatusMessage);

    connect(m_radarCommunicatorUdp, &RadarCommunicatorUdp::SignalLogEvent, this, &RadarUdpControlUI::SignalLogEvent);

    m_autoreconnectionTimer = new QTimer;
    m_autoreconnectionTimer->setInterval(12500);
    connect(m_autoreconnectionTimer, &QTimer::timeout, this, &RadarUdpControlUI::timerStartCounterClicked);

    m_autodisconnectionTimer = new QTimer;
    m_autodisconnectionTimer->setInterval(500);
    connect(m_autodisconnectionTimer, &QTimer::timeout, this, &RadarUdpControlUI::timerStopCounterClicked);

    m_timerBeforeDisconnection = new QTimer;
    m_timerBeforeDisconnection->setInterval(5000);
    connect(m_timerBeforeDisconnection, &QTimer::timeout, this, &RadarUdpControlUI::SlotPreDisconnection);

    setWindowTitle("RADAR Ethernet Control Pannel");

    m_warmingUpStatus = true;
}

RadarUdpControlUI::~RadarUdpControlUI()
{
}

QWidget *RadarUdpControlUI::ExternalControlWidget()
{
    QWidget *externalControlWidget = new QWidget;

    QGroupBox *controlGroupBox = new QGroupBox;
    controlGroupBox->setTitle("Transmitter Ethernet Control");

    QGridLayout *mainControlLayout = new QGridLayout;

    m_startTx = new QToolButton;
    m_startTx->setToolTip("Connect");
    m_startTx->setIcon(QIcon(m_iniSettings->value(QString("icons")) + g_pathDevider + QString("connect.svg")));
    connect(m_startTx, &QPushButton::clicked, this, &RadarUdpControlUI::timerStartCounterClicked);
    m_startTx->setVisible(false);

    m_stopTx = new QToolButton;
    m_stopTx->setToolTip("Disconnect");
    m_stopTx->setIcon(QIcon(m_iniSettings->value(QString("icons")) + g_pathDevider + QString("disconnect.svg")));
    connect(m_stopTx, &QPushButton::clicked, this, &RadarUdpControlUI::timerStopCounterClicked);
    m_extStartSwitch = new QPushButton("Start");
    connect(m_extStartSwitch, &QPushButton::clicked, this, &RadarUdpControlUI::SlotExtStartTxClicked);
    m_extStartSwitch->setVisible(false);

    m_powerTxLedIndicator = new LedIndicator;
    m_powerTxLedIndicator->setState(LedState::LedOff);

    m_powerMotorLedIndicator = new LedIndicator;
    m_powerMotorLedIndicator->setState(LedState::LedOff);

    m_extAdcDacLedIndicator = new LedIndicator;
    m_extAdcDacLedIndicator->setState(LedState::LedOff);

    m_extEthernetConnectionLedIndicator = new LedIndicator;
    m_extEthernetConnectionLedIndicator->setState(LedState::LedOff);

    m_powerMotorCheckBox = new QPushButton(QString("Motor"));
    m_powerMotorCheckBox->setCheckable(true);
    m_powerMotorCheckBox->setToolTip(QString("Enable/Disable Motor Rotation"));
//    m_powerMotorCheckBox->setLayoutDirection(Qt::LayoutDirection::RightToLeft);
    m_powerMotorCheckBox->setChecked(false);
    m_powerMotorCheckBox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(m_powerMotorCheckBox, &QPushButton::clicked, this, &RadarUdpControlUI::SlotMotorControlCheckStateChanged);

    m_extTuneCoarse = new QSpinBox;
    m_extTuneCoarse->setMinimum(0);
    m_extTuneCoarse->setMaximum(255);
    m_extTuneCoarse->setValue(0);
    m_extTuneCoarse->setButtonSymbols(QSpinBox::PlusMinus);
    m_extTuneCoarse->setToolTip(QString("Tune Coarse"));
    connect(m_extTuneCoarse, &QSpinBox::editingFinished, this, &RadarUdpControlUI::UISlotExtTuneCoarseEditingFinished);

    m_extTuneFine = new QSpinBox;
    m_extTuneFine->setMinimum(0);
    m_extTuneFine->setMaximum(255);
    m_extTuneFine->setValue(0);
    m_extTuneFine->setButtonSymbols(QSpinBox::PlusMinus);
    m_extTuneFine->setToolTip(QString("Tune Fine"));
    connect(m_extTuneFine, &QSpinBox::editingFinished, this, &RadarUdpControlUI::UISlotExtTuneFineEditingFinished);

    m_extPowerIndicator = new QSpinBox;
    m_extPowerIndicator->setMinimum(0);
    m_extPowerIndicator->setMaximum(255);
    m_extPowerIndicator->setValue(0);
    m_extPowerIndicator->setButtonSymbols(QSpinBox::NoButtons);
    m_extPowerIndicator->setReadOnly(true);
    m_extPowerIndicator->setToolTip(QString("Power"));

    QFrame *line = new QFrame;
    line->setFrameShape(QFrame::VLine);
    line->setFrameShadow(QFrame::Sunken);

    m_extPulseWidth = new QComboBox;
    QStringList setupPulseWidth;
    setupPulseWidth.append(QString(tr("XL")));
    setupPulseWidth.append(QString(tr("L")));
    setupPulseWidth.append(QString(tr("M")));
    setupPulseWidth.append(QString(tr("S")));
    setupPulseWidth.append(QString(tr("XS")));
    m_extPulseWidth->addItems(setupPulseWidth);
    m_extPulseWidth->setCurrentIndex(m_radarSetupSetiings.getPulseMode());
    m_extPulseWidth->setToolTip(QString("Pulse Width"));
    connect(m_extPulseWidth, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &RadarUdpControlUI::UISlotExtPulseWidthChanged);

    m_extSetPulseWidth = new QPushButton(QString("Pulse"));
    m_extSetPulseWidth->setMaximumWidth(70);
    m_extSetPulseWidth->setCheckable(true);
    connect(m_extSetPulseWidth, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotExtSetPulseWidthStateChanged);
    m_extSetPulseWidth->setChecked(true);
    m_extSetPulseWidth->click();
    m_extSetPulseWidth->setToolTip(QString("Pulse Width Adjustment"));

    m_extInitButton = new QToolButton;
    m_extInitButton->setIcon(QIcon(m_iniSettings->value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_extInitButton->setToolTip(QString("Reset Radar Settings to Default"));
    connect(m_extInitButton, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotExtInitClicked);

    mainControlLayout->addWidget(m_powerTxLedIndicator, 1, 0);
    mainControlLayout->addWidget(m_powerMotorLedIndicator, 2, 0);
    mainControlLayout->addWidget(new QLabel(QString("Tx Power")), 1, 1);
    mainControlLayout->addWidget(m_powerMotorCheckBox, 2, 1);
    mainControlLayout->addWidget(m_extEthernetConnectionLedIndicator, 1, 2);
    mainControlLayout->addWidget(m_extAdcDacLedIndicator, 2, 2);
    mainControlLayout->addWidget(new QLabel(QString("Network")), 1, 3);
    mainControlLayout->addWidget(new QLabel(QString("ADC/DAC")), 2, 3);
    mainControlLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum), 1, 4, 2, 1);
    mainControlLayout->addWidget(line, 1, 5, 2, 1);
    mainControlLayout->addWidget(new QLabel(QString("Tune")), 1, 6);
    mainControlLayout->addWidget(new QLabel(QString("C:")), 1, 7, Qt::AlignRight);
    mainControlLayout->addWidget(m_extSetPulseWidth, 2, 6, 1, 2);
    mainControlLayout->addWidget(m_extTuneCoarse, 1, 8);
    mainControlLayout->addWidget(m_extPulseWidth, 2, 8);
    mainControlLayout->addWidget(new QLabel(QString("F:")), 1, 9);
    mainControlLayout->addWidget(m_extTuneFine, 1, 10);
    mainControlLayout->addWidget(new QLabel(QString("P:")), 2, 9);
    mainControlLayout->addWidget(m_extPowerIndicator, 2, 10);

    m_openSettings = new QToolButton;
    m_openSettings->setIcon(QIcon(m_iniSettings->value(QString("icons")) + g_pathDevider + QString("document-properties.svg")));
    m_openSettings->setToolTip(QString("Open Radar Settings"));

    QVBoxLayout *mainLayout = new QVBoxLayout;

    m_extPowerUpTimer = new QSpinBox;
    m_extPowerUpTimer->setReadOnly(true);
    m_extPowerUpTimer->setButtonSymbols(QSpinBox::NoButtons);
    m_extPowerUpTimer->setMinimum(0);
    m_extPowerUpTimer->setMaximum(180);
    m_extPowerUpTimer->setValue(0);
    m_extPowerUpTimer->setAlignment(Qt::AlignRight);
    m_extPowerUpTimer->setVisible(false);

    m_radarStatusLabel = new QLabel(QString("Not Connected"));
    m_radarStatusLabel->setMinimumWidth(116);
    connect(m_radarCommunicatorUdp, &RadarCommunicatorUdp::SignalNetworkError, this, &RadarUdpControlUI::SlotNetworkError);
    connect(m_radarCommunicatorUdp, &RadarCommunicatorUdp::SignalDisconnect, this, &RadarUdpControlUI::SlotDisconnect);

    m_radarStatusLedIndicator = new LedIndicator;
    m_radarStatusLedIndicator->setState(LedState::LedOff);

    m_reconnect = new QPushButton(QString("Reconnect"));
    connect(m_reconnect, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotRestartWorkingThread);

    m_extSkipWarmUpTimeButton = new QPushButton(QString("Skip"));
    m_extSkipWarmUpTimeButton->setVisible(false);
    connect(m_extSkipWarmUpTimeButton, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotExtSkipWarmUpTimeClicked);

    QHBoxLayout *topLayout = new QHBoxLayout;
    topLayout->addWidget(m_extSkipWarmUpTimeButton);
    topLayout->addWidget(m_extStartSwitch);
    topLayout->addWidget(m_reconnect);
    topLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
    mainControlLayout->addLayout(topLayout, 0, 0, 1, 2);
    mainControlLayout->addWidget(m_radarStatusLedIndicator, 0, 2);
    QHBoxLayout *statusLayout = new QHBoxLayout;
    statusLayout->addWidget(m_radarStatusLabel);
    statusLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
    statusLayout->addWidget(m_extPowerUpTimer);
    statusLayout->addWidget(m_startTx);
    statusLayout->addWidget(m_stopTx);
    statusLayout->addWidget(m_extInitButton);
    statusLayout->addWidget(m_openSettings);
    mainControlLayout->addLayout(statusLayout, 0, 3, 1, 8);

    QVBoxLayout *internalLayout = new QVBoxLayout;

    internalLayout->addLayout(mainControlLayout);
    controlGroupBox->setLayout(internalLayout);

    mainLayout->addWidget(controlGroupBox);

    externalControlWidget->setLayout(mainLayout);

    connect(m_openSettings, &QPushButton::clicked, this, &RadarUdpControlUI::OpenSettingsClicked);

    return externalControlWidget;
}

void RadarUdpControlUI::exit()
{
    SlotStopTxClicked();
}

void RadarUdpControlUI::setIniSettings(QMap<QString, QString> &iniSettings)
{
    m_iniSettings->insert(QString("RadarTriggerGeneratorStateUDP"), iniSettings.value(QString("RadarTriggerGeneratorStateUDP")));
    m_iniSettings->insert(QString("RadarTriggerPeriodUDP"), iniSettings.value(QString("RadarTriggerPeriodUDP")));
    m_iniSettings->insert(QString("RadarTriggerPeriodUDP"), iniSettings.value(QString("RadarTriggerPeriodUDP")));

    m_iniSettings->insert(QString("RadarTriggerDelayValueUDP"), iniSettings.value(QString("RadarTriggerDelayValueUDP")));

    m_iniSettings->insert(QString("RadarADCSampligFrequencyUDP"), iniSettings.value(QString("RadarADCSampligFrequencyUDP")));

    m_iniSettings->insert(QString("RadarMiscZeroBlankMemComtrolUDP"), iniSettings.value(QString("RadarMiscZeroBlankMemComtrolUDP")));
    m_iniSettings->insert(QString("RadarMiscZeroTestACPUDP"), iniSettings.value(QString("RadarMiscZeroTestACPUDP")));
    m_iniSettings->insert(QString("RadarMiscZeroAutoTuneFineUDP"), iniSettings.value(QString("RadarMiscZeroAutoTuneFineUDP")));
    m_iniSettings->insert(QString("RadarMiscZeroAutoTuneRegUDP"), iniSettings.value(QString("RadarMiscZeroAutoTuneRegUDP")));
    m_iniSettings->insert(QString("RadarMiscZeroDivideACPInputUDP"), iniSettings.value(QString("RadarMiscZeroDivideACPInputUDP")));

    m_iniSettings->insert(QString("RadarMiscOneVideoSampleResUDP"), iniSettings.value(QString("RadarMiscOneVideoSampleResUDP")));
    m_iniSettings->insert(QString("RadarMiscOneAcpPeriodUDP"), iniSettings.value(QString("RadarMiscOneAcpPeriodUDP")));
    m_iniSettings->insert(QString("RadarMiscOneBlankingRamUDP"), iniSettings.value(QString("RadarMiscOneBlankingRamUDP")));
    m_iniSettings->insert(QString("RadarMiscOneBandwithUDP"), iniSettings.value(QString("RadarMiscOneBandwithUDP")));

    m_iniSettings->insert(QString("RadarHlSetupWidthUDP"), iniSettings.value(QString("RadarHlSetupWidthUDP")));
    m_iniSettings->insert(QString("RadarHlSetupInvertUDP"), iniSettings.value(QString("RadarHlSetupInvertUDP")));
    m_iniSettings->insert(QString("RadarHlSetupEdgeUDP"), iniSettings.value(QString("RadarHlSetupEdgeUDP")));
    m_iniSettings->insert(QString("RadarHlSetupDelayValueUDP"), iniSettings.value(QString("RadarHlSetupDelayValueUDP")));

    m_iniSettings->insert(QString("RadarEnableVideoAcquisitionUDP"), iniSettings.value(QString("RadarEnableVideoAcquisitionUDP")));
    m_iniSettings->insert(QString("RadarEnableDoubleBufferUDP"), iniSettings.value(QString("RadarEnableDoubleBufferUDP")));
    m_iniSettings->insert(QString("RadarEnableAutoTuneUDP"), iniSettings.value(QString("RadarEnableAutoTuneUDP")));
    m_iniSettings->insert(QString("RadarEnableDAConvertersUDP"), iniSettings.value(QString("RadarEnableDAConvertersUDP")));
    m_iniSettings->insert(QString("RadarEnableADConvertersUDP"), iniSettings.value(QString("RadarEnableADConvertersUDP")));

    m_iniSettings->insert(QString("RadarGateTuneDelayValueUDP"), iniSettings.value(QString("RadarGateTuneDelayValueUDP")));

    m_iniSettings->insert(QString("RadarAntSpeedControlUDP"), iniSettings.value(QString("RadarAntSpeedControlUDP")));
    m_iniSettings->insert(QString("RadarAntSpeedValueUDP"), iniSettings.value(QString("RadarAntSpeedValueUDP")));

    m_iniSettings->insert(QString("RadarMasterIPLowUDP"), iniSettings.value(QString("RadarMasterIPLowUDP")));

    m_iniSettings->insert(QString("RadarTuneCoarseUDP"), iniSettings.value(QString("RadarTuneCoarseUDP")));
    m_iniSettings->insert(QString("RadarTuneFineUDP"), iniSettings.value(QString("RadarTuneFineUDP")));
    m_iniSettings->insert(QString("RadarTuneRegUDP"), iniSettings.value(QString("RadarTuneRegUDP")));

    m_iniSettings->insert(QString("RadarDebugNoMasterModeUDP"), iniSettings.value(QString("RadarDebugNoMasterModeUDP")));
    m_iniSettings->insert(QString("RadarDebugTxTestModeUDP"), iniSettings.value(QString("RadarDebugTxTestModeUDP")));
    m_iniSettings->insert(QString("RadarDebugPowerUpModeUDP"), iniSettings.value(QString("RadarDebugPowerUpModeUDP")));

    m_iniSettings->insert(QString("RadarSetupTransceiverModeUDP"), iniSettings.value(QString("RadarSetupTransceiverModeUDP")));
    m_iniSettings->insert(QString("RadarSetupPulseModeUDP"), iniSettings.value(QString("RadarSetupPulseModeUDP")));
    m_iniSettings->insert(QString("RadarSetupEnableMotorUDP"), iniSettings.value(QString("RadarSetupEnableMotorUDP")));

    SlotGetAndSetDefaults();
}

QMap<QString, QString> RadarUdpControlUI::getIniSettings()
{
    QMap<QString, QString> iniSettings;

    iniSettings.insert(QString("RadarTriggerGeneratorStateUDP"), QString::number(m_radarTriggerSettings.getTriggerGenetatorState()));
    iniSettings.insert(QString("RadarTriggerPeriodUDP"), QString::number(m_radarTriggerSettings.getTriggerPeriod()));
    iniSettings.insert(QString("RadarTriggerPeriodUDP"), QString::number(m_radarTriggerSettings.getTriggerStaggerValue()));

    iniSettings.insert(QString("RadarTriggerDelayValueUDP"), QString::number(m_radarTriggerDelaySettings.m_triggerDelayValue));

    iniSettings.insert(QString("RadarADCSampligFrequencyUDP"), QString::number(m_radarADSamplingFrequencySettings.getRadarADFs()));

    iniSettings.insert(QString("RadarMiscZeroBlankMemComtrolUDP"), QString::number(m_radarMiscZeroSettings.getBlankMemControl()));
    iniSettings.insert(QString("RadarMiscZeroTestACPUDP"), QString::number(m_radarMiscZeroSettings.getTestACP()));
    iniSettings.insert(QString("RadarMiscZeroAutoTuneFineUDP"), QString::number(m_radarMiscZeroSettings.getAutotuneFine()));
    iniSettings.insert(QString("RadarMiscZeroAutoTuneRegUDP"), QString::number(m_radarMiscZeroSettings.getAutotuneReg()));
    iniSettings.insert(QString("RadarMiscZeroDivideACPInputUDP"), QString::number(m_radarMiscZeroSettings.getDivideACPInputSignal()));

    iniSettings.insert(QString("RadarMiscOneVideoSampleResUDP"), QString::number(m_radarMiscOneSettings.getVideoSampleRes()));
    iniSettings.insert(QString("RadarMiscOneAcpPeriodUDP"), QString::number(m_radarMiscOneSettings.getAcpPeriod()));
    iniSettings.insert(QString("RadarMiscOneBlankingRamUDP"), QString::number(m_radarMiscOneSettings.getBlankingRam()));
    iniSettings.insert(QString("RadarMiscOneBandwithUDP"), QString::number(m_radarMiscOneSettings.getBandwith()));

    iniSettings.insert(QString("RadarHlSetupWidthUDP"), QString::number(m_radarHlSetupSettings.getWidth()));
    iniSettings.insert(QString("RadarHlSetupInvertUDP"), QString::number(m_radarHlSetupSettings.getInvert()));
    iniSettings.insert(QString("RadarHlSetupEdgeUDP"), QString::number(m_radarHlSetupSettings.getEdge()));
    iniSettings.insert(QString("RadarHlSetupDelayValueUDP"), QString::number(m_radarHlSetupSettings.getDelayValue()));

    iniSettings.insert(QString("RadarEnableVideoAcquisitionUDP"), QString::number(m_radarEnableSettings.getVideoAcquisition()));
    iniSettings.insert(QString("RadarEnableDoubleBufferUDP"), QString::number(m_radarEnableSettings.getDoubleBuffer()));
    iniSettings.insert(QString("RadarEnableAutoTuneUDP"), QString::number(m_radarEnableSettings.getAutoTune()));
    iniSettings.insert(QString("RadarEnableDAConvertersUDP"), QString::number(m_radarEnableSettings.getDac()));
    iniSettings.insert(QString("RadarEnableADConvertersUDP"), QString::number(m_radarEnableSettings.getAdc()));

    iniSettings.insert(QString("RadarGateTuneDelayValueUDP"), QString::number(m_radarGateTuneDelaySettings.getTuneDelayValue()));

    iniSettings.insert(QString("RadarAntSpeedControlUDP"), QString::number(m_radarAntSpeedSettings.getSpeedControl()));
    iniSettings.insert(QString("RadarAntSpeedValueUDP"), QString::number(m_radarAntSpeedSettings.getSpeedValue()));

    iniSettings.insert(QString("RadarMasterIPLowUDP"), QString::number(m_radarMasterIPLowSettings.getMasterIPLowValue()));

    iniSettings.insert(QString("RadarTuneCoarseUDP"), QString::number(m_radarTuneCoarseSettings.m_coarseValue));
    iniSettings.insert(QString("RadarTuneFineUDP"), QString::number(m_radarTuneFineSettings.m_fineValue));
    iniSettings.insert(QString("RadarTuneRegUDP"), QString::number(m_radarTuneRegSettings.m_regValue));

    iniSettings.insert(QString("RadarDebugNoMasterModeUDP"), QString::number(m_radarDebugSettings.getNoMasterMode()));
    iniSettings.insert(QString("RadarDebugTxTestModeUDP"), QString::number(m_radarDebugSettings.getTxTestMode()));
    iniSettings.insert(QString("RadarDebugPowerUpModeUDP"), QString::number(m_radarDebugSettings.getPowerUpMode()));

    iniSettings.insert(QString("RadarSetupTransceiverModeUDP"), QString::number(m_radarSetupSetiings.getTransceiverMode()));
    iniSettings.insert(QString("RadarSetupPulseModeUDP"), QString::number(m_radarSetupSetiings.getPulseMode()));
    iniSettings.insert(QString("RadarSetupEnableMotorUDP"), QString::number(m_radarSetupSetiings.getEnableMotor()));

    IniReader::checkConfiguration(iniSettings, SettingsGroup::RadarEthernetSettings);

    m_iniSettings->insert(iniSettings);

    return iniSettings;
}

QWidget *RadarUdpControlUI::TimerWidget()
{
    QGroupBox *mainWidget = new QGroupBox(QString(tr("PowerUp Timer")));

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;

    m_timerCounter = new QLCDNumber;

    controlLayout->addWidget(m_timerCounter);
    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    controlLayout->addSpacerItem(spacer);

    m_timerStartCounter = new QPushButton(QString(tr("Start")));
    m_timerStopCounter = new QPushButton(QString(tr("Stop")));

    controlLayout->addWidget(m_timerStartCounter);
    controlLayout->addWidget(m_timerStopCounter);
    mainLayout->addLayout(controlLayout, 0, 0);

    connect(m_timerStartCounter, &QPushButton::clicked, this, &RadarUdpControlUI::timerStartCounterClicked);
    connect(m_timerStopCounter, &QPushButton::clicked, this, &RadarUdpControlUI::timerStopCounterClicked);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *RadarUdpControlUI::TriggerWidget()
{
    QGroupBox *triggerWidget = new QGroupBox;

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayout = new QHBoxLayout;

    triggerWidget->setTitle(QString(tr("Trigger")));

    QGridLayout *setButtonLayout = new QGridLayout;
    m_triggerSet = new QPushButton;
    m_triggerSet->setText(tr("Set"));
    setButtonLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 1);
    setButtonLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 1, 0);
    setButtonLayout->addWidget(m_triggerSet, 1, 1);

    QVBoxLayout *triggerEnableLayout = new QVBoxLayout;
    QLabel *triggerEnableLabel = new QLabel(tr("Enable"));
    triggerEnableLayout->addWidget(triggerEnableLabel);

    m_triggerEnable = new QComboBox;
    QStringList triggerEnable;
    triggerEnable.append(QString(tr("No")));
    triggerEnable.append(QString(tr("Yes")));
    m_triggerEnable->addItems(triggerEnable);
    m_triggerEnable->setCurrentIndex(m_radarTriggerSettings.getTriggerGenetatorState());
    triggerEnableLayout->addWidget(m_triggerEnable);

    QVBoxLayout *triggerStaggerLayout = new QVBoxLayout;
    QLabel *triggerStaggerLabel = new QLabel(tr("Stagger"));
    triggerStaggerLayout->addWidget(triggerStaggerLabel);

    m_triggerStaggerValue = new QSpinBox;
    m_triggerStaggerValue->setMaximum(15);
    m_triggerStaggerValue->setMinimum(0);
    m_triggerStaggerValue->setSingleStep(1);
    m_triggerStaggerValue->setValue(m_radarTriggerSettings.getTriggerStaggerValue());
    m_triggerStaggerValue->setSuffix(QString(tr("%")));
    m_triggerStaggerValue->setButtonSymbols(QSpinBox::NoButtons);
    triggerStaggerLayout->addWidget(m_triggerStaggerValue);

    QVBoxLayout *triggerPeriodLayout = new QVBoxLayout;
    QLabel *triggerPeriodLabel = new QLabel(tr("Period"));
    triggerPeriodLayout->addWidget(triggerPeriodLabel);

    m_triggerPeriod = new QComboBox;
    QStringList triggerPeriod;
    triggerPeriod.append(QString(tr("500 Hz")));
    triggerPeriod.append(QString(tr("800 Hz")));
    triggerPeriod.append(QString(tr("1600 Hz")));
    triggerPeriod.append(QString(tr("3200 Hz")));
    m_triggerPeriod->addItems(triggerPeriod);
    m_triggerPeriod->setCurrentIndex(m_radarTriggerSettings.getTriggerPeriod());
    triggerPeriodLayout->addWidget(m_triggerPeriod);

    settingsLayout->addLayout(triggerEnableLayout);
    settingsLayout->addLayout(triggerStaggerLayout);
    controlLayout->addLayout(triggerPeriodLayout);
    controlLayout->addLayout(setButtonLayout);

    mainLayout->addLayout(settingsLayout);
    mainLayout->addLayout(controlLayout);

    triggerWidget->setLayout(mainLayout);

    connect(m_triggerSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotTriggerSetClicked);

    return triggerWidget;
}

QWidget *RadarUdpControlUI::TriggerDelayWidget()
{
    QGroupBox *mainWidget = new QGroupBox(QString(tr("Trigger Delay")));

    QHBoxLayout *mainLayout = new QHBoxLayout;

    m_triggerDelay = new QSpinBox;
    m_triggerDelay->setMinimum(0);
    m_triggerDelay->setMaximum(65535);
    m_triggerDelay->setValue(m_radarTriggerDelaySettings.m_triggerDelayValue);
    m_triggerDelay->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_triggerDelay);

    m_triggerDelaySet = new QPushButton;
    m_triggerDelaySet->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_triggerDelaySet->setText(tr("Set"));

    mainLayout->addWidget(m_triggerDelaySet);

    mainWidget->setLayout(mainLayout);

    connect(m_triggerDelaySet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotTriggerDelaySetClicked);

    return mainWidget;
}

QWidget *RadarUdpControlUI::ADControlWidget()
{
    QGroupBox *mainWidget = new QGroupBox(tr("ADC Control"));
    QHBoxLayout *mainLayout = new QHBoxLayout;

    QVBoxLayout *adSamplingFrequencyLayout = new QVBoxLayout;
    QLabel *adSamplingFrequencyLabel = new QLabel(QString(tr("Sampling Frequency")));
    adSamplingFrequencyLayout->addWidget(adSamplingFrequencyLabel);

    m_ADSamplingFrequency = new QComboBox;
    QStringList adSamplingFrequencyList;
    adSamplingFrequencyList.append(QString(tr("50 MHz")));
    adSamplingFrequencyList.append(QString(tr("48.387 MHz")));
    adSamplingFrequencyList.append(QString(tr("52.273 MHz")));
    m_ADSamplingFrequency->addItems(adSamplingFrequencyList);
    m_ADSamplingFrequency->setCurrentIndex(m_radarADSamplingFrequencySettings.getRadarADFs());
    adSamplingFrequencyLayout->addWidget(m_ADSamplingFrequency);

    mainLayout->addLayout(adSamplingFrequencyLayout);

    QVBoxLayout *setLayout = new QVBoxLayout;
    setLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));

    m_ADSamplingFrequencySet = new QPushButton(QString(tr("Set")));
    connect(m_ADSamplingFrequencySet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotADSamplingFrequencySetClicked);
    setLayout->addWidget(m_ADSamplingFrequencySet);

    mainLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    mainLayout->addLayout(setLayout);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *RadarUdpControlUI::MiscZeroWidget()
{
    QGroupBox *miscZeroWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;

    miscZeroWidget->setTitle(QString(tr("Misc 0")));

    QVBoxLayout *setButtonLayout = new QVBoxLayout;
    setButtonLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    m_miscZeroSet = new QPushButton;
    m_miscZeroSet->setText(tr("Set"));
    setButtonLayout->addWidget(m_miscZeroSet);

    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    QVBoxLayout *miscZeroTestACPLayout = new QVBoxLayout;
    QLabel *miscZeroTestACPLabel = new QLabel(tr("Test ACP"));
    miscZeroTestACPLayout->addWidget(miscZeroTestACPLabel);

    m_miscZeroTestACP = new QComboBox;
    QStringList miscZeroTestACP;
    miscZeroTestACP.append(QString(tr("Normal operation")));
    miscZeroTestACP.append(QString(tr("ACP/ARP internal genaration")));
    m_miscZeroTestACP->addItems(miscZeroTestACP);
    m_miscZeroTestACP->setCurrentIndex(m_radarMiscZeroSettings.getTestACP());
    miscZeroTestACPLayout->addWidget(m_miscZeroTestACP);

    QVBoxLayout *miscZeroAutotuneFineLayout = new QVBoxLayout;
    QLabel *miscZeroAutotuneFineLabel = new QLabel(tr("Autotune Fine"));
    miscZeroAutotuneFineLayout->addWidget(miscZeroAutotuneFineLabel);

    controlLayout->addLayout(miscZeroTestACPLayout);
    controlLayout->addSpacerItem(spacer);
    controlLayout->addLayout(setButtonLayout);

    m_miscZeroAutoTineFine = new QComboBox;
    QStringList miscZeroAutotuneFine;
    miscZeroAutotuneFine.append(QString(tr("Disabled")));
    miscZeroAutotuneFine.append(QString(tr("Enabled")));
    m_miscZeroAutoTineFine->addItems(miscZeroAutotuneFine);
    m_miscZeroAutoTineFine->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneFine());
    miscZeroAutotuneFineLayout->addWidget(m_miscZeroAutoTineFine);

    QVBoxLayout *blankMemoryControlLayout = new QVBoxLayout;
    QLabel *blankMemoryControlLabel = new QLabel(tr("Blanking Memory"));
    blankMemoryControlLayout->addWidget(blankMemoryControlLabel);

    m_blankMemoryControl = new QComboBox;
    QStringList blankMemoryControl;
    blankMemoryControl.append(QString(tr("Serial Control")));
    blankMemoryControl.append(QString(tr("Ethernet Control")));
    m_blankMemoryControl->addItems(blankMemoryControl);
    m_blankMemoryControl->setCurrentIndex(m_radarMiscZeroSettings.getBlankMemControl());
    blankMemoryControlLayout->addWidget(m_blankMemoryControl);

    settingsLayoutOne->addLayout(miscZeroAutotuneFineLayout);
    settingsLayoutOne->addLayout(blankMemoryControlLayout);

    QVBoxLayout *miscZeroAutotuneRegLayout = new QVBoxLayout;
    QLabel *miscZeroAutotuneRegLabel = new QLabel(tr("Autotune Reg"));
    miscZeroAutotuneRegLayout->addWidget(miscZeroAutotuneRegLabel);

    m_miscZeroAutoTuneReg = new QComboBox;
    QStringList miscZeroAutotuneReg;
    miscZeroAutotuneReg.append(QString(tr("Disabled")));
    miscZeroAutotuneReg.append(QString(tr("Enabled")));
    m_miscZeroAutoTuneReg->addItems(miscZeroAutotuneReg);
    m_miscZeroAutoTuneReg->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneReg());
    miscZeroAutotuneRegLayout->addWidget(m_miscZeroAutoTuneReg);

    QVBoxLayout *miscZeroDivACPLayout = new QVBoxLayout;
    QLabel *miscZeroDivACPLabel = new QLabel(tr("ACP Signal Division"));
    miscZeroDivACPLayout->addWidget(miscZeroDivACPLabel);

    m_miscZeroDivACP= new QComboBox;
    QStringList miscZeroDivACP;
    miscZeroDivACP.append(QString(tr("Not devided")));
    miscZeroDivACP.append(QString(tr("Devideded by 2")));
    miscZeroDivACP.append(QString(tr("Devideded by 4")));
    miscZeroDivACP.append(QString(tr("Devideded by 8")));
    m_miscZeroDivACP->addItems(miscZeroDivACP);
    m_miscZeroDivACP->setCurrentIndex(m_radarMiscZeroSettings.getDivideACPInputSignal());
    miscZeroDivACPLayout->addWidget(m_miscZeroDivACP);

    settingsLayoutTwo->addLayout(miscZeroAutotuneRegLayout);
    settingsLayoutTwo->addLayout(miscZeroDivACPLayout);

    mainLayout->addLayout(controlLayout, 2, 0);
    mainLayout->addLayout(settingsLayoutOne, 1, 0);
    mainLayout->addLayout(settingsLayoutTwo, 0, 0);

    miscZeroWidget->setLayout(mainLayout);

    connect(m_miscZeroSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotMiscZeroSetClicked);

    return miscZeroWidget;
}

QWidget *RadarUdpControlUI::MiscOneWidget()
{
    QGroupBox *miscOneWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;

    miscOneWidget->setTitle(tr("Misc 1"));

    QVBoxLayout *setButtonLayout = new QVBoxLayout;
    setButtonLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    m_miscOneSet = new QPushButton;
    m_miscOneSet->setText(tr("Set"));
    setButtonLayout->addWidget(m_miscOneSet);

    QVBoxLayout *miscOneAcpPeriodLayout = new QVBoxLayout;
    QLabel *miscOneAcpPeriodLabel = new QLabel(tr("ACP Period"));
    miscOneAcpPeriodLayout->addWidget(miscOneAcpPeriodLabel);

    m_miscOneAcpPeriod = new QComboBox;
    QStringList miscOneAcpPeriod;
    miscOneAcpPeriod.append(QString(tr("2048 pulses per revolution")));
    miscOneAcpPeriod.append(QString(tr("4096 pulses per revolution")));
    m_miscOneAcpPeriod->addItems(miscOneAcpPeriod);
    m_miscOneAcpPeriod->setCurrentIndex(m_radarMiscOneSettings.getAcpPeriod());
    miscOneAcpPeriodLayout->addWidget(m_miscOneAcpPeriod);

    QVBoxLayout *miscOneBlackingRamLayout = new QVBoxLayout;
    QLabel *miscOneBlackingRamLabel = new QLabel(tr("Blanking RAM"));
    miscOneBlackingRamLayout->addWidget(miscOneBlackingRamLabel);

    m_miscOneBlackingRam = new QComboBox;
    QStringList miscOneBlackingRam;
    miscOneBlackingRam.append(QString(tr("Block from 0 to 511")));
    miscOneBlackingRam.append(QString(tr("Block from 512 to 1023")));
    m_miscOneBlackingRam->addItems(miscOneBlackingRam);
    m_miscOneBlackingRam->setCurrentIndex(m_radarMiscOneSettings.getBlankingRam());
    miscOneBlackingRamLayout->addWidget(m_miscOneBlackingRam);

    QVBoxLayout *miscOneBandwidthLayout = new QVBoxLayout;
    QLabel *miscOneBandwidthLabel = new QLabel(tr("Bandwith"));
    miscOneBandwidthLayout->addWidget(miscOneBandwidthLabel);

    m_miscOneBandwidth = new QComboBox;
    QStringList miscOneBandwidth;
    miscOneBandwidth.append(QString(tr("Wide Band")));
    miscOneBandwidth.append(QString(tr("Narrow Band")));
    m_miscOneBandwidth->addItems(miscOneBandwidth);
    m_miscOneBandwidth->setCurrentIndex(m_radarMiscOneSettings.getBandwith());
    miscOneBandwidthLayout->addWidget(m_miscOneBandwidth);

    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    controlLayout->addLayout(miscOneBandwidthLayout);
    controlLayout->addSpacerItem(spacer);
    controlLayout->addLayout(setButtonLayout);

    QVBoxLayout *videoSampleResLayout = new QVBoxLayout;
    QLabel *videoSampleResLabel = new QLabel(tr("Video Res"));
    videoSampleResLayout->addWidget(videoSampleResLabel);

    m_videoSampleRes = new QComboBox;
    QStringList videoSampleRes;
    videoSampleRes.append(QString(tr("Default")));
    videoSampleRes.append(QString(tr("Divide 2")));
    videoSampleRes.append(QString(tr("Divide 5")));
    videoSampleRes.append(QString(tr("Divide 10")));
    m_videoSampleRes->addItems(videoSampleRes);
    m_videoSampleRes->setCurrentIndex(m_radarMiscOneSettings.getVideoSampleRes());
    videoSampleResLayout->addWidget(m_videoSampleRes);

    settingsLayoutOne->addLayout(miscOneAcpPeriodLayout);
    settingsLayoutTwo->addLayout(videoSampleResLayout);
    settingsLayoutTwo->addLayout(miscOneBlackingRamLayout);

    mainLayout->addLayout(settingsLayoutOne, 0, 0);
    mainLayout->addLayout(settingsLayoutTwo, 1, 0);
    mainLayout->addLayout(controlLayout, 2, 0);

    miscOneWidget->setLayout(mainLayout);

    connect(m_miscOneSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotMiscOneSetClicked);

    return miscOneWidget;
}

QWidget *RadarUdpControlUI::HlSetupWidget()
{
    QGroupBox *hlSetupWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;

    hlSetupWidget->setTitle(tr("HL Setup"));

    QVBoxLayout *setButtonLayout = new QVBoxLayout;
    setButtonLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    m_hlSetupSet = new QPushButton;
    m_hlSetupSet->setText(tr("Set"));
    setButtonLayout->addWidget(m_hlSetupSet);

    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    controlLayout->addSpacerItem(spacer);
    controlLayout->addLayout(setButtonLayout);

    QVBoxLayout *hlSetupWidthLayout = new QVBoxLayout;
    QLabel *hlSetupWidthLabel = new QLabel(tr("Width"));
    hlSetupWidthLayout->addWidget(hlSetupWidthLabel);

    m_hlSetupWidth = new QComboBox;
    QStringList hlSetupWidth;
    hlSetupWidth.append(QString(tr("1/2 ACP Pulse")));
    hlSetupWidth.append(QString(tr("1 ACP Pulse")));
    hlSetupWidth.append(QString(tr("2 ACP Pulses")));
    hlSetupWidth.append(QString(tr("4 Acp Pulses")));
    m_hlSetupWidth->addItems(hlSetupWidth);
    m_hlSetupWidth->setCurrentIndex(m_radarHlSetupSettings.getWidth());
    hlSetupWidthLayout->addWidget(m_hlSetupWidth);

    settingsLayoutOne->addLayout(hlSetupWidthLayout);

    QVBoxLayout *hlSetupInvertLayout = new QVBoxLayout;
    QLabel *hlSetupInvertLabel = new QLabel(tr("Invert"));
    hlSetupInvertLayout->addWidget(hlSetupInvertLabel);

    m_hlSetupInvert = new QComboBox;
    QStringList hlSetupInvert;
    hlSetupInvert.append(QString(tr("Disabed")));
    hlSetupInvert.append(QString(tr("Enabled")));
    m_hlSetupInvert->addItems(hlSetupInvert);
    m_hlSetupInvert->setCurrentIndex(m_radarHlSetupSettings.getInvert());
    hlSetupInvertLayout->addWidget(m_hlSetupInvert);

    settingsLayoutOne->addLayout(hlSetupInvertLayout);

    QVBoxLayout *hlSetupEdgeLayout = new QVBoxLayout;
    QLabel *hlSetupEdgeLabel = new QLabel(tr("Edge"));
    hlSetupEdgeLayout->addWidget(hlSetupEdgeLabel);

    m_hlSetupEdge = new QComboBox;
    QStringList hlSetupEdge;
    hlSetupEdge.append(QString(tr("Falling")));
    hlSetupEdge.append(QString(tr("Rising")));
    m_hlSetupEdge->addItems(hlSetupEdge);
    m_hlSetupEdge->setCurrentIndex(m_radarHlSetupSettings.getEdge());
    hlSetupEdgeLayout->addWidget(m_hlSetupEdge);

    settingsLayoutTwo->addLayout(hlSetupEdgeLayout);

    QVBoxLayout *hlSetupDelayLayout = new QVBoxLayout;
    QLabel *hlSetupDelayLabel = new QLabel(tr("Delay"));
    hlSetupDelayLayout->addWidget(hlSetupDelayLabel);

    m_hlSetupDelay = new QSpinBox;
    m_hlSetupDelay->setValue(m_radarHlSetupSettings.getDelayValue());
    m_hlSetupDelay->setMinimum(0);
    m_hlSetupDelay->setMaximum(4095);
    hlSetupDelayLayout->addWidget(m_hlSetupDelay);

    settingsLayoutTwo->addLayout(hlSetupDelayLayout);

    mainLayout->addLayout(settingsLayoutOne, 1, 0);
    mainLayout->addLayout(settingsLayoutTwo, 2, 0);
    mainLayout->addLayout(controlLayout, 3, 0);

    hlSetupWidget->setLayout(mainLayout);

    connect(m_hlSetupSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotHlSetupSetClicked);

    return hlSetupWidget;
}

QWidget *RadarUdpControlUI::EnableWidget()
{
    QGroupBox *enableWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayout = new QHBoxLayout;

    enableWidget->setTitle(tr("Enable"));

    QVBoxLayout *setButtonLayout = new QVBoxLayout;
    setButtonLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    m_enableSet = new QPushButton;
    m_enableSet->setText(tr("Set"));
    setButtonLayout->addWidget(m_enableSet);

    QVBoxLayout *videoAcquisitionLayout = new QVBoxLayout;
    QLabel *videoAcquisitionLabel = new QLabel(tr("Video Aqcuis"));
    videoAcquisitionLayout->addWidget(videoAcquisitionLabel);

    m_videoAcquisition = new QComboBox;
    QStringList videoAcquisition;
    videoAcquisition.append(QString(tr("Disabled")));
    videoAcquisition.append(QString(tr("Enabled")));
    m_videoAcquisition->addItems(videoAcquisition);
    m_videoAcquisition->setCurrentIndex(m_radarEnableSettings.getVideoAcquisition());
    videoAcquisitionLayout->addWidget(m_videoAcquisition);

    QVBoxLayout *doubleBufferLayout = new QVBoxLayout;
    QLabel *doubleBufferLabel = new QLabel(tr("Double buffer"));
    doubleBufferLayout->addWidget(doubleBufferLabel);

    m_doubleBuffer = new QComboBox;
    QStringList doubleBuffer;
    doubleBuffer.append(QString(tr("Disabled")));
    doubleBuffer.append(QString(tr("Enabled")));
    m_doubleBuffer->addItems(doubleBuffer);
    m_doubleBuffer->setCurrentIndex(m_radarEnableSettings.getDoubleBuffer());
    doubleBufferLayout->addWidget(m_doubleBuffer);

    QVBoxLayout *enableAutoTuneLayout = new QVBoxLayout;
    QLabel *enableAutoTuneLabel = new QLabel(tr("Autotune"));
    enableAutoTuneLayout->addWidget(enableAutoTuneLabel);

    m_enableAutoTune = new QComboBox;
    QStringList enableAutoTune;
    enableAutoTune.append(QString(tr("Disabled")));
    enableAutoTune.append(QString(tr("Enabled")));
    m_enableAutoTune->addItems(enableAutoTune);
    m_enableAutoTune->setCurrentIndex(m_radarEnableSettings.getAutoTune());
    enableAutoTuneLayout->addWidget(m_enableAutoTune);

    QVBoxLayout *enableDALayout = new QVBoxLayout;
    QLabel *enableDALabel = new QLabel(tr("DAC"));
    enableDALayout->addWidget(enableDALabel);

    m_enableDA = new QComboBox;
    QStringList enableDA;
    enableDA.append(QString(tr("Disabled")));
    enableDA.append(QString(tr("Enabled")));
    m_enableDA->addItems(enableDA);
    m_enableDA->setCurrentIndex(m_radarEnableSettings.getDac());
    enableDALayout->addWidget(m_enableDA);

    QVBoxLayout *enableADLayout = new QVBoxLayout;
    QLabel *enableADLabel = new QLabel(tr("ADC"));
    enableADLayout->addWidget(enableADLabel);

    m_enableAD = new QComboBox;
    QStringList enableAD;
    enableAD.append(QString(tr("Disabled")));
    enableAD.append(QString(tr("Enabled")));
    m_enableAD->addItems(enableAD);
    m_enableAD->setCurrentIndex(m_radarEnableSettings.getAdc());
    enableADLayout->addWidget(m_enableAD);

    settingsLayout->addLayout(videoAcquisitionLayout);
    settingsLayout->addLayout(doubleBufferLayout);
    settingsLayout->addLayout(enableAutoTuneLayout);

    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    controlLayout->addLayout(enableADLayout);
    controlLayout->addLayout(enableDALayout);
    controlLayout->addSpacerItem(spacer);
    controlLayout->addLayout(setButtonLayout);

    mainLayout->addLayout(settingsLayout, 0, 0);
    mainLayout->addLayout(controlLayout, 1, 0);

    enableWidget->setLayout(mainLayout);

    connect(m_enableSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotEnableSetClicked);

    return enableWidget;
}

QWidget *RadarUdpControlUI::TuneCoarseWidget()
{
    QGroupBox *tuneCoarseWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;

    tuneCoarseWidget->setTitle(tr("Tune Coarse"));

    QVBoxLayout *tuneCoarseValueLayout = new QVBoxLayout;

    m_tuneCoarseValue = new QSpinBox;
    m_tuneCoarseValue->setValue(m_radarTuneCoarseSettings.m_coarseValue);
    m_tuneCoarseValue->setMinimum(0);
    m_tuneCoarseValue->setMaximum(255);
    m_tuneCoarseValue->setButtonSymbols(QSpinBox::NoButtons);
    tuneCoarseValueLayout->addWidget(m_tuneCoarseValue);

    controlLayout->addLayout(tuneCoarseValueLayout);

    m_tuneCoatseSet = new QPushButton;
    m_tuneCoatseSet->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_tuneCoatseSet->setText(tr("Set"));

    controlLayout->addWidget(m_tuneCoatseSet);

    mainLayout->addLayout(controlLayout, 0, 0);

    tuneCoarseWidget->setLayout(mainLayout);

    connect(m_tuneCoatseSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotTuneCoatseSetClicked);

    return tuneCoarseWidget;
}

QWidget *RadarUdpControlUI::TuneFineWidget()
{
    QGroupBox *tuneFineWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;

    tuneFineWidget->setTitle(tr("Tune Fine"));

    QVBoxLayout *tuneFineValueLayout = new QVBoxLayout;

    m_tuneFineValue = new QSpinBox;
    m_tuneFineValue->setValue(m_radarTuneFineSettings.m_fineValue);
    m_tuneFineValue->setMinimum(0);
    m_tuneFineValue->setMaximum(255);
    m_tuneFineValue->setButtonSymbols(QSpinBox::NoButtons);
    tuneFineValueLayout->addWidget(m_tuneFineValue);

    controlLayout->addLayout(tuneFineValueLayout);

    m_tuneFineSet = new QPushButton;
    m_tuneFineSet->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_tuneFineSet->setText(tr("Set"));

    controlLayout->addWidget(m_tuneFineSet);

    mainLayout->addLayout(controlLayout, 0, 0);

    tuneFineWidget->setLayout(mainLayout);

    connect(m_tuneFineSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotTuneFineSetClicked);

    return tuneFineWidget;
}

QWidget *RadarUdpControlUI::TuneRegWidget()
{
    QGroupBox *tuneRegWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;

    tuneRegWidget->setTitle(tr("Tune Reg"));

    QVBoxLayout *tuneRegValueLayout = new QVBoxLayout;

    m_tuneRegValue = new QSpinBox;
    m_tuneRegValue->setValue(m_radarTuneRegSettings.m_regValue);
    m_tuneRegValue->setMinimum(0);
    m_tuneRegValue->setMaximum(255);
    m_tuneRegValue->setButtonSymbols(QSpinBox::NoButtons);
    tuneRegValueLayout->addWidget(m_tuneRegValue);

    controlLayout->addLayout(tuneRegValueLayout);

    m_tuneRegSet = new QPushButton;
    m_tuneRegSet->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_tuneRegSet->setText(tr("Set"));

    controlLayout->addWidget(m_tuneRegSet);

    mainLayout->addLayout(settingsLayoutOne, 1, 0);
    mainLayout->addLayout(settingsLayoutTwo, 2, 0);
    mainLayout->addLayout(controlLayout, 0, 0);

    tuneRegWidget->setLayout(mainLayout);

    connect(m_tuneRegSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotTuneRegSetClicked);

    return tuneRegWidget;
}

QWidget *RadarUdpControlUI::SetupWidget()
{
    QGroupBox *setupWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayout = new QHBoxLayout;

    setupWidget->setTitle(tr("Setup"));

    m_setupSet = new QPushButton;
    m_setupSet->setText(tr("Set"));
    QVBoxLayout *setButtonLayout = new QVBoxLayout;
    setButtonLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    setButtonLayout->addWidget(m_setupSet);

    QVBoxLayout *setupTxLayout = new QVBoxLayout;
    QLabel *setupTxLabel = new QLabel(tr("Tranceiver"));
    setupTxLayout->addWidget(setupTxLabel);

    m_setupTx = new QComboBox;
    QStringList setupTx;
    setupTx.append(QString(tr("StandBy")));
    setupTx.append(QString(tr("Tx Enabled")));
    m_setupTx->addItems(setupTx);
    m_setupTx->setCurrentIndex(m_radarSetupSetiings.getTransceiverMode());
    setupTxLayout->addWidget(m_setupTx);

    settingsLayout->addLayout(setupTxLayout);

    QVBoxLayout *setupPulseWidthLayout = new QVBoxLayout;
    QLabel *setupPulseWidthLabel = new QLabel(tr("Pulse widgth"));
    setupPulseWidthLayout->addWidget(setupPulseWidthLabel);

    m_setupPulseWidth = new QComboBox;
    QStringList setupPulseWidth;
    setupPulseWidth.append(QString(tr("XL Pulse")));
    setupPulseWidth.append(QString(tr("L Pulse")));
    setupPulseWidth.append(QString(tr("M Pulse")));
    setupPulseWidth.append(QString(tr("S Pulse")));
    setupPulseWidth.append(QString(tr("XS Pulse")));
    m_setupPulseWidth->addItems(setupPulseWidth);
    m_setupPulseWidth->setCurrentIndex(m_radarSetupSetiings.getPulseMode());
    setupPulseWidthLayout->addWidget(m_setupPulseWidth);

    controlLayout->addLayout(setupPulseWidthLayout);
    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    controlLayout->addSpacerItem(spacer);
    controlLayout->addLayout(setButtonLayout);

    QVBoxLayout *setupMotorLayout = new QVBoxLayout;
    QLabel *setupMotorLabel = new QLabel(tr("Motor"));
    setupMotorLayout->addWidget(setupMotorLabel);

    m_setupMotor = new QComboBox;
    QStringList setupMotor;
    setupMotor.append(QString(tr("Disabled")));
    setupMotor.append(QString(tr("Enabled")));
    m_setupMotor->addItems(setupMotor);
    m_setupMotor->setCurrentIndex(m_radarSetupSetiings.getEnableMotor());
    setupMotorLayout->addWidget(m_setupMotor);

    settingsLayout->addLayout(setupMotorLayout);

    mainLayout->addLayout(settingsLayout, 0, 0);
    mainLayout->addLayout(controlLayout, 1, 0);

    setupWidget->setLayout(mainLayout);

    connect(m_setupSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotSetupSetClicked);

    return setupWidget;
}

QWidget *RadarUdpControlUI::GateTuneDelayWidget()
{
    QGroupBox *mainWidget = new QGroupBox(tr("Gate Tune"));
    QHBoxLayout *mainLayout = new QHBoxLayout;

    m_gateTuneDelayValue = new QSpinBox;
    m_gateTuneDelayValue->setMinimum(0);
    m_gateTuneDelayValue->setMaximum(255);
    m_gateTuneDelayValue->setValue(m_radarGateTuneDelaySettings.m_tuneDelay);
    m_gateTuneDelayValue->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_gateTuneDelayValue);

    m_gateTuneDelaySet = new QPushButton(QString(tr("Set")));
    connect(m_gateTuneDelaySet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotGateTuneDelaySetClicked);
    mainLayout->addWidget(m_gateTuneDelaySet);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *RadarUdpControlUI::AutotuneFineWidget()
{
    QGroupBox *autotuneFineWidget = new QGroupBox;

    QHBoxLayout *mainLayout = new QHBoxLayout;

    autotuneFineWidget->setTitle(tr("Autotune Fine"));

    m_autotuneFineValue = new QSpinBox;
    m_autotuneFineValue->setValue(0);
    m_autotuneFineValue->setMinimum(0);
    m_autotuneFineValue->setMaximum(255);
    m_autotuneFineValue->setReadOnly(true);
    m_autotuneFineValue->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_autotuneFineValue);

    autotuneFineWidget->setLayout(mainLayout);

    return autotuneFineWidget;
}

QWidget *RadarUdpControlUI::AutotuneRegWidget()
{
    QGroupBox *autotuneRegWidget = new QGroupBox;

    QHBoxLayout *mainLayout = new QHBoxLayout;

    autotuneRegWidget->setTitle(tr("Autotune Reg"));

    m_autotuneRegValue = new QSpinBox;
    m_autotuneRegValue->setValue(0);
    m_autotuneRegValue->setMinimum(0);
    m_autotuneRegValue->setMaximum(255);
    m_autotuneRegValue->setReadOnly(true);
    m_autotuneRegValue->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_autotuneRegValue);

    autotuneRegWidget->setLayout(mainLayout);

    return autotuneRegWidget;
}

QWidget *RadarUdpControlUI::AntSpeedWidget()
{
    QGroupBox *mainWidget = new QGroupBox(tr("Antena Speed"));
    QVBoxLayout *mainLayout = new QVBoxLayout;

    m_antSpeedSet = new QPushButton;
    m_antSpeedSet->setText(tr("Set"));
    QVBoxLayout *setButtonLayout = new QVBoxLayout;
    setButtonLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    setButtonLayout->addWidget(m_antSpeedSet);

    QHBoxLayout *bottomLayout = new QHBoxLayout;
    bottomLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    bottomLayout->addLayout(setButtonLayout);

    QHBoxLayout *topLayout = new QHBoxLayout;

    QVBoxLayout *antSpeedControlLayout = new QVBoxLayout;
    QLabel *antSpeedControlLabel = new QLabel(QString(tr("Control")));
    antSpeedControlLayout->addWidget(antSpeedControlLabel);

    QStringList antSpeedControlList;
    antSpeedControlList.append(QString("Half Step"));
    antSpeedControlList.append(QString("Full Step"));
    m_antSpeedControl = new QComboBox;
    m_antSpeedControl->addItems(antSpeedControlList);
    m_antSpeedControl->setCurrentIndex(m_radarAntSpeedSettings.getSpeedControl());
    antSpeedControlLayout->addWidget(m_antSpeedControl);

    QVBoxLayout *antSpeedValuelLayout = new QVBoxLayout;
    QLabel *antSpeedValueLabel = new QLabel(QString(tr("Control")));
    antSpeedValuelLayout->addWidget(antSpeedValueLabel);

    QStringList antSpeedValueList;
    antSpeedValueList.append(QString("33 RPM"));
    antSpeedValueList.append(QString("30 RPM"));
    antSpeedValueList.append(QString("27 RPM"));
    antSpeedValueList.append(QString("24 RPM"));
    m_antSpeedValue = new QComboBox;
    m_antSpeedValue->addItems(antSpeedValueList);
    m_antSpeedValue->setCurrentIndex(m_radarAntSpeedSettings.getSpeedValue());
    antSpeedValuelLayout->addWidget(m_antSpeedValue);

    topLayout->addLayout(antSpeedControlLayout);
    topLayout->addLayout(antSpeedValuelLayout);

    mainLayout->addLayout(topLayout); 
    mainLayout->addLayout(bottomLayout);

    mainWidget->setLayout(mainLayout);

    connect(m_antSpeedSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotAntSpeedSetClicked);

    return mainWidget;
}

QWidget *RadarUdpControlUI::MasterIPLowWidget()
{
    QGroupBox *mainWidget = new QGroupBox(tr("Master Ip Low"));

    QHBoxLayout *mainLayout = new QHBoxLayout;

    m_masterIPLow = new QSpinBox;
    m_masterIPLow->setMinimum(0);
    m_masterIPLow->setMaximum(255);
    m_masterIPLow->setValue(m_radarMasterIPLowSettings.m_masterIPLow);
    m_masterIPLow->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_masterIPLow);

    m_masterIPLowSet = new QPushButton;
    m_masterIPLowSet->setText(tr("Set"));

    mainLayout->addWidget(m_masterIPLowSet);

    mainWidget->setLayout(mainLayout);

    connect(m_masterIPLowSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotMasterIPLowSetClicked);


    return mainWidget;
}

QWidget *RadarUdpControlUI::MagIndicatorWidget()
{
    QGroupBox *magIndicatorWidget = new QGroupBox;

    QHBoxLayout *mainLayout = new QHBoxLayout;

    magIndicatorWidget->setTitle(tr("Mag Indicator"));

    m_magIndidcatorValue = new QSpinBox;
    m_magIndidcatorValue->setValue(0);
    m_magIndidcatorValue->setMinimum(0);
    m_magIndidcatorValue->setMaximum(255);
    m_magIndidcatorValue->setReadOnly(true);
    m_magIndidcatorValue->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_magIndidcatorValue);

    magIndicatorWidget->setLayout(mainLayout);

    return magIndicatorWidget;
}

QWidget *RadarUdpControlUI::TuneIndicatorWidget()
{
    QGroupBox *tuneIndicatorWidget = new QGroupBox;

    QHBoxLayout *mainLayout = new QHBoxLayout;

    tuneIndicatorWidget->setTitle(tr("Tune Indidcator"));

    m_tuneIndicatorValue = new QSpinBox;
    m_tuneIndicatorValue->setValue(0);
    m_tuneIndicatorValue->setMinimum(0);
    m_tuneIndicatorValue->setMaximum(255);
    m_tuneIndicatorValue->setReadOnly(true);
    m_tuneIndicatorValue->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_tuneIndicatorValue);

    tuneIndicatorWidget->setLayout(mainLayout);

    return tuneIndicatorWidget;
}

QWidget *RadarUdpControlUI::DebugWidget()
{
    QGroupBox *mainWidget = new QGroupBox(tr("Debug"));

    QVBoxLayout *mainLayout = new QVBoxLayout;

    m_debugSet = new QPushButton;
    m_debugSet->setText(tr("Set"));
    QVBoxLayout *setButtonLayout = new QVBoxLayout;
    setButtonLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    setButtonLayout->addWidget(m_debugSet);
    connect(m_debugSet, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotDebugSetClicked);

    QVBoxLayout *noMasterModeLayout = new QVBoxLayout;
    QLabel *noMasterModeLabel = new QLabel(QString(tr("Master Mode")));
    noMasterModeLayout->addWidget(noMasterModeLabel);

    QStringList noMasterModeList;
    noMasterModeList.append(QString(tr("Enabled")));
    noMasterModeList.append(QString(tr("Disabled")));   
    m_noMasterMode = new QComboBox;
    m_noMasterMode->addItems(noMasterModeList);
    m_noMasterMode->setCurrentIndex(m_radarDebugSettings.getNoMasterMode());
    noMasterModeLayout->addWidget(m_noMasterMode);

    QVBoxLayout *txTestModeLayout = new QVBoxLayout;
    QLabel *txTestModeLabel = new QLabel(QString(tr("Tx Test Mode")));
    txTestModeLayout->addWidget(txTestModeLabel);

    QStringList txTestModeList;
    txTestModeList.append(QString(tr("Disabled")));
    txTestModeList.append(QString(tr("Enabled")));
    m_txTestMode = new QComboBox;
    m_txTestMode->addItems(txTestModeList);
    m_txTestMode->setCurrentIndex(m_radarDebugSettings.getTxTestMode());
    txTestModeLayout->addWidget(m_txTestMode);

    QVBoxLayout *powerUpModeLayout = new QVBoxLayout;
    QLabel *powerUpModeLabel = new QLabel(QString(tr("Power Up Mode")));
    powerUpModeLayout->addWidget(powerUpModeLabel);

    QStringList powerUpModeList;
    powerUpModeList.append(QString(tr("Disabled")));
    powerUpModeList.append(QString(tr("SkipPowerUpTime")));
    m_powerUpMode = new QComboBox;
    m_powerUpMode->addItems(powerUpModeList);
    m_powerUpMode->setCurrentIndex(m_radarDebugSettings.getPowerUpMode());
    powerUpModeLayout->addWidget(m_powerUpMode);

    QHBoxLayout *topLayout = new QHBoxLayout;
    QHBoxLayout *bottomLayout = new QHBoxLayout;

    topLayout->addLayout(noMasterModeLayout);
    topLayout->addLayout(txTestModeLayout);
    bottomLayout->addLayout(powerUpModeLayout);
    bottomLayout->addLayout(setButtonLayout);

    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(bottomLayout);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *RadarUdpControlUI::StatusWidget()
{
    QGroupBox *statusWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;
    QHBoxLayout *settingsLayoutThree = new QHBoxLayout;

    statusWidget->setTitle(tr("Status"));

    QVBoxLayout *statusWarmupLayout = new QVBoxLayout;
    QLabel *statusWarmupLabel = new QLabel(tr("PowerUp"));
    statusWarmupLayout->addWidget(statusWarmupLabel);

    m_statusPowerUp = new QComboBox;
    QStringList statusWarmup;
    statusWarmup.append(QString(tr("PowerUp Mode")));
    statusWarmup.append(QString(tr("Ready to Transmit")));
    m_statusPowerUp->addItems(statusWarmup);
    m_statusPowerUp->setCurrentIndex(0);
    m_statusPowerUp->setEditable(false);
    statusWarmupLayout->addWidget(m_statusPowerUp);

    settingsLayoutOne->addLayout(statusWarmupLayout);

    QVBoxLayout *statusAutotuneRegLayout = new QVBoxLayout;
    QLabel *statusAutotuneRegLabel = new QLabel(tr("Autotune Reg"));
    statusAutotuneRegLayout->addWidget(statusAutotuneRegLabel);

    m_statusAutotuneReg = new QComboBox;
    QStringList statusAutotuneReg;
    statusAutotuneReg.append(QString(tr("Not Completed")));
    statusAutotuneReg.append(QString(tr("Completed")));
    m_statusAutotuneReg->addItems(statusAutotuneReg);
    m_statusAutotuneReg->setCurrentIndex(0);
    m_statusAutotuneReg->setEditable(false);
    statusAutotuneRegLayout->addWidget(m_statusAutotuneReg);

    settingsLayoutOne->addLayout(statusAutotuneRegLayout);

    QVBoxLayout *statusAutotuneFineLayout = new QVBoxLayout;
    QLabel *statusAutotuneFineLabel = new QLabel(tr("Autotune Fine"));
    statusAutotuneFineLayout->addWidget(statusAutotuneFineLabel);

    m_statusAutotuneFine = new QComboBox;
    QStringList statusAutotuneFine;
    statusAutotuneFine.append(QString(tr("Not Completed")));
    statusAutotuneFine.append(QString(tr("Completed")));
    m_statusAutotuneFine->addItems(statusAutotuneFine);
    m_statusAutotuneFine->setCurrentIndex(0);
    m_statusAutotuneFine->setEditable(false);
    statusAutotuneFineLayout->addWidget(m_statusAutotuneFine);

    settingsLayoutTwo->addLayout(statusAutotuneFineLayout);

    QVBoxLayout *statusAcpInputSignalLayout = new QVBoxLayout;
    QLabel *statusAcpInputSignalLabel = new QLabel(tr("ACP"));
    statusAcpInputSignalLayout->addWidget(statusAcpInputSignalLabel);

    m_statusAcpInputSignal = new QComboBox;
    QStringList statusAcpInputSignal;
    statusAcpInputSignal.append(QString(tr("Input Signal Ok")));
    statusAcpInputSignal.append(QString(tr("Input Signal Fail")));
    m_statusAcpInputSignal->addItems(statusAcpInputSignal);
    m_statusAcpInputSignal->setCurrentIndex(0);
    m_statusAcpInputSignal->setEditable(false);
    statusAcpInputSignalLayout->addWidget(m_statusAcpInputSignal);

    settingsLayoutTwo->addLayout(statusAcpInputSignalLayout);

    QVBoxLayout *statusHlInputSignalLayout = new QVBoxLayout;
    QLabel *statusHlInputSignalLabel = new QLabel(tr("HL"));
    statusHlInputSignalLayout->addWidget(statusHlInputSignalLabel);

    m_statusHlInputSignal = new QComboBox;
    QStringList statusHlInputSignal;
    statusHlInputSignal.append(QString(tr("Input Signal Ok")));
    statusHlInputSignal.append(QString(tr("Input Signal Fail")));
    m_statusHlInputSignal->addItems(statusHlInputSignal);
    m_statusHlInputSignal->setCurrentIndex(0);
    m_statusHlInputSignal->setEditable(false);
    statusHlInputSignalLayout->addWidget(m_statusHlInputSignal);

    settingsLayoutThree->addLayout(statusHlInputSignalLayout);

    QVBoxLayout *statusTriggerInpusSignalLayout = new QVBoxLayout;
    QLabel *statusTriggerInpusSignalLabel = new QLabel(tr("Trigger"));
    statusTriggerInpusSignalLayout->addWidget(statusTriggerInpusSignalLabel);

    m_statusTriggerInputSignal = new QComboBox;
    QStringList statusTriggerInpusSignal;
    statusTriggerInpusSignal.append(QString(tr("Input Signal Ok")));
    statusTriggerInpusSignal.append(QString(tr("Input Signal Fail")));
    m_statusTriggerInputSignal->addItems(statusTriggerInpusSignal);
    m_statusTriggerInputSignal->setCurrentIndex(0);
    m_statusTriggerInputSignal->setEditable(false);
    statusTriggerInpusSignalLayout->addWidget(m_statusTriggerInputSignal);

    settingsLayoutThree->addLayout(statusTriggerInpusSignalLayout);

    mainLayout->addLayout(settingsLayoutOne, 0, 0);
    mainLayout->addLayout(settingsLayoutTwo, 1, 0);
    mainLayout->addLayout(settingsLayoutThree, 2, 0);

    statusWidget->setLayout(mainLayout);

    return statusWidget;
}

QWidget *RadarUdpControlUI::StatusWidgetRegular()
{
    QGroupBox *mainWidget = new QGroupBox(tr("Status"));

    QVBoxLayout *triggerTxStatusLayout = new QVBoxLayout;
    QLabel *triggerTxStatusLabel = new QLabel(QString(tr("Tx")));
    triggerTxStatusLayout->addWidget(triggerTxStatusLabel);

    QStringList triggerTxStatusList;
    triggerTxStatusList.append(QString(tr("Stand By")));
    triggerTxStatusList.append(QString(tr("Transmit")));
    m_triggerTxStatus = new QComboBox;
    m_triggerTxStatus->addItems(triggerTxStatusList);
    m_triggerTxStatus->setCurrentIndex(0);
    triggerTxStatusLayout->addWidget(m_triggerTxStatus);

    QVBoxLayout *powerUpStatusLayout = new QVBoxLayout;
    QLabel *powerUpStatusLabel = new QLabel(QString(tr("System")));
    powerUpStatusLayout->addWidget(powerUpStatusLabel);

    QStringList powerUpStatusList;
    powerUpStatusList.append(QString(tr("PowerUp")));
    powerUpStatusList.append(QString(tr("Ready")));
    m_powerUpStatus = new QComboBox;
    m_powerUpStatus->addItems(powerUpStatusList);
    m_powerUpStatus->setCurrentIndex(0);
    powerUpStatusLayout->addWidget(m_powerUpStatus);

    QVBoxLayout *masterSlaveModeLayout = new QVBoxLayout;
    QLabel *masterSlaveModeLabel = new QLabel(QString(tr("Mode")));
    masterSlaveModeLayout->addWidget(masterSlaveModeLabel);

    QStringList masterSlaveModeList;
    masterSlaveModeList.append(QString(tr("Master")));
    masterSlaveModeList.append(QString(tr("Slave")));
    m_masterSlaveMode = new QComboBox;
    m_masterSlaveMode->addItems(masterSlaveModeList);
    m_masterSlaveMode->setCurrentIndex(0);
    masterSlaveModeLayout->addWidget(m_masterSlaveMode);

    QVBoxLayout *triggerStatusLayout = new QVBoxLayout;
    QLabel *triggerStatusLabel = new QLabel(QString(tr("Trigger")));
    triggerStatusLayout->addWidget(triggerStatusLabel);

    QStringList triggerStatusList;
    triggerStatusList.append(QString(tr("Ok")));
    triggerStatusList.append(QString(tr("Fail")));
    m_triggerStatus = new QComboBox;
    m_triggerStatus->addItems(triggerStatusList);
    m_triggerStatus->setCurrentIndex(0);
    triggerStatusLayout->addWidget(m_triggerStatus);

    QVBoxLayout *localTransmitionStatusLayout = new QVBoxLayout;
    QLabel *localTransmitionStatusLabel = new QLabel(QString(tr("Local Tx")));
    localTransmitionStatusLayout->addWidget(localTransmitionStatusLabel);

    QStringList localTransmitionStatusList;
    localTransmitionStatusList.append(QString(tr("Tx")));
    localTransmitionStatusList.append(QString(tr("Not Tx")));
    m_localTransmitionStatus = new QComboBox;
    m_localTransmitionStatus->addItems(localTransmitionStatusList);
    m_localTransmitionStatus->setCurrentIndex(0);
    localTransmitionStatusLayout->addWidget(m_localTransmitionStatus);

    QVBoxLayout *asimuthEncoderFailStatusLayout = new QVBoxLayout;
    QLabel *asimuthEncoderFailStatusLabel = new QLabel(QString(tr("ACP")));
    asimuthEncoderFailStatusLayout->addWidget(asimuthEncoderFailStatusLabel);

    QStringList asimuthEncoderFailStatusList;
    asimuthEncoderFailStatusList.append(QString(tr("Ok")));
    asimuthEncoderFailStatusList.append(QString(tr("Fail")));
    m_asimuthEncoderFailStatus = new QComboBox;
    m_asimuthEncoderFailStatus->addItems(asimuthEncoderFailStatusList);
    m_asimuthEncoderFailStatus->setCurrentIndex(0);
    asimuthEncoderFailStatusLayout->addWidget(m_asimuthEncoderFailStatus);

    QVBoxLayout *headLineDetectionStatusLayout = new QVBoxLayout;
    QLabel *headLineDetectionStatusLabel = new QLabel(QString(tr("HL")));
    headLineDetectionStatusLayout->addWidget(headLineDetectionStatusLabel);

    QStringList headLineDetectionStatusList;
    headLineDetectionStatusList.append(QString(tr("Ok")));
    headLineDetectionStatusList.append(QString(tr("Fail")));
    m_headLineDetectionStatus = new QComboBox;
    m_headLineDetectionStatus->addItems(headLineDetectionStatusList);
    m_headLineDetectionStatus->setCurrentIndex(0);
    headLineDetectionStatusLayout->addWidget(m_headLineDetectionStatus);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *topLayout = new QHBoxLayout;
    QHBoxLayout *middleLayout = new QHBoxLayout;
    QHBoxLayout *bottomLayout = new QHBoxLayout;

    topLayout->addLayout(triggerTxStatusLayout);
    topLayout->addLayout(powerUpStatusLayout);
    middleLayout->addLayout(masterSlaveModeLayout);
    middleLayout->addLayout(triggerStatusLayout);
    bottomLayout->addLayout(localTransmitionStatusLayout);
    bottomLayout->addLayout(asimuthEncoderFailStatusLayout);
    bottomLayout->addLayout(headLineDetectionStatusLayout);

    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(middleLayout);
    mainLayout->addLayout(bottomLayout);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *RadarUdpControlUI::GeneralWidget()
{
    QGroupBox *mainWidget = new QGroupBox(tr("Control"));

    QHBoxLayout *mainLayout = new QHBoxLayout;

    m_setAll = new QPushButton(QString(tr("Set All")));
    connect(m_setAll, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotSetAllClicked);
    mainLayout->addWidget(m_setAll);

    m_getAll = new QPushButton(QString(tr("Get All")));
    connect(m_getAll, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotGetAllClicked);
    mainLayout->addWidget(m_getAll);

    m_resetAll = new QPushButton(QString(tr("Reset")));
    connect(m_resetAll, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotResetAllClicked);
    mainLayout->addWidget(m_resetAll);

    m_defaultSettings = new QPushButton(QString(tr("Default")));
    connect(m_defaultSettings, &QPushButton::clicked, this, &RadarUdpControlUI::UISlotDefaultSettingsClicked);
    mainLayout->addWidget(m_defaultSettings);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

void RadarUdpControlUI::SlotStatusMessageRceived(const QByteArray &statusMessage)
{
    if (statusMessage.length() == 3) {
        if (m_onlineTimer->isActive()) {
            m_onlineTimer->stop();
        }
        m_onlineTimer->start(100);
        m_extEthernetConnectionLedIndicator->setState(LedState::LedOn);
        m_connected = true;
        m_reconnected = true;
        m_connectedSpecialCounter = m_connectedSpecialCounter < 100 ? m_connectedSpecialCounter + 1 : 100;
        switch (statusMessage[0]) {
        case 0x00 : {
            m_radarBuffer.clear();
            m_radarBuffer.append(statusMessage[1]);
            break;
        }
        case 0x01 : {
            if (m_radarBuffer.size() == 1) {
                m_radarBuffer.append(statusMessage[1]);
            }
            break;
        }
        case 0x02 : {
            if (m_radarBuffer.size() == 2) {
                m_radarBuffer.append(statusMessage[1]);
                m_radarTriggerSettingsReceived = RadarTriggerE(m_radarBuffer);
            }
            m_radarBuffer.clear();
            break;
        }
        case 0x03 : {
            m_radarBuffer.clear();
            m_radarBuffer.append(statusMessage[1]);
            break;
        }
        case 0x04 : {
            if (m_radarBuffer.size() == 1) {
                m_radarBuffer.append(statusMessage[1]);
                m_radarTriggerDelaySettingsReceived = RadarTriggerDelayE(m_radarBuffer);
            }
            m_radarBuffer.clear();
            break;
        }
        case 0x05 : {
            m_radarADSamplingFrequencySettingsReceived = RadarADControlE(statusMessage[1]);
            break;
        }
        case 0x06 : {
            m_radarMiscZeroSettingsReceived = RadarMiscZeroE(statusMessage[1]);
            break;
        }
        case 0x07 : {
            m_radarMiscOneSettingsReceived = RadarMiscOneE(statusMessage[1]);
            break;
        }
        case 0x08 : {
            m_radarBuffer.clear();
            m_radarBuffer.append(statusMessage[1]);
            break;
        }
        case 0x09 : {
            if (m_radarBuffer.size() == 1) {
                m_radarBuffer.append(statusMessage[1]);
                m_radarHlSetupSettingsReceived = RadarHlSetupE(m_radarBuffer);
            }
            m_radarBuffer.clear();
            break;
        }
        case 0x0A : {
            m_radarEnableSettingsReceived = RadarEnableE(statusMessage[1]);
            break;
        }
        case 0x0B : {
            m_radarTuneCoarseSettingsReceived = RadarTuneCoarseE(statusMessage[1]);
            break;
        }
        case 0x0C : {
            m_radarTuneFineSettingsReceived = RadarTuneFineE(statusMessage[1]);
            break;
        }
        case 0x0D : {
            m_radarTuneRegSettingsReceived = RadarTuneRegE(statusMessage[1]);
            break;
        }
        case 0x0F : {
            m_radarSetupSetiingsReceived = RadarSetupE(statusMessage[1]);
            break;
        }
        case 0x10 : {
            m_radarGateTuneDelaySettingsReceived = RadarGateTuneDelayE(statusMessage[1]);
            break;
        }
        case 0x11 : {
            m_autotuneFineValue->setValue(static_cast<quint8>(statusMessage[1]));
            break;
        }
        case 0x12 : {
            m_autotuneRegValue->setValue(static_cast<quint8>(statusMessage[1]));
            break;
        }
        case 0x13 : {
            m_radarAntSpeedSettingsReceived = RadarAntSpeedE(statusMessage[1]);
            break;
        }
        case 0x16 : {
            m_radarMasterIPLowSettingsReceived = RadarMasterIPLowE(statusMessage[1]);
            break;
        }
        case 0x1D : {
            m_magIndidcatorValue->setValue(static_cast<quint8>(statusMessage[1]));
            break;
        }
        case 0x1E : {
            m_tuneIndicatorValue->setValue(static_cast<quint8>(statusMessage[1]));
            break;
        }
        case 0x1F : {
            RadarStatusE status = RadarStatusE(statusMessage[1]);
            m_statusPowerUp->setCurrentIndex(status.getPowerUpComplete());
            m_statusAutotuneReg->setCurrentIndex(status.getRegAutoTune());
            m_statusAutotuneFine->setCurrentIndex(status.getFineAutoTune());
            m_statusAcpInputSignal->setCurrentIndex(status.getAcpFail());
            m_statusHlInputSignal->setCurrentIndex(status.getHlFail());
            m_statusTriggerInputSignal->setCurrentIndex(status.getTriggerFail());
            break;
        }
        case 0x20 : {
            if (((statusMessage[2] >> 6) & 0b1) == 1) {
                powerUpIsComplete(true);
                if (m_powerUpModeState) {
                    SlotGetAndSetDefaults();
                }
                m_powerUpModeState = false;
                if (!m_updatedOnce) {
                    m_powerUpModeState = true;
                }

            } else {
                powerUpIsComplete(false);
                timerCounterValue(statusMessage[1]);
                m_powerUpModeState = true;
            }
            break;
        }
        case 0x21 : {
            m_radarDebugSettingsReceived = RadarDebugE(statusMessage[1]);
            if (m_powerUpModeState) {
                SlotGetAndSetDefaults();
            }
            break;
        }
        }

        m_triggerTxStatus->setCurrentIndex(static_cast<bool>((statusMessage[2] >> 7) & 0b1));
        m_powerUpStatus->setCurrentIndex(static_cast<bool>((statusMessage[2] >> 6) & 0b1));;
        m_masterSlaveMode->setCurrentIndex(static_cast<bool>((statusMessage[2] >> 5) & 0b1));;
        m_triggerStatus->setCurrentIndex(static_cast<bool>((statusMessage[2] >> 3) & 0b1));;
        m_localTransmitionStatus->setCurrentIndex(static_cast<bool>((statusMessage[2] >> 2) & 0b1));;
        m_asimuthEncoderFailStatus->setCurrentIndex(static_cast<bool>((statusMessage[2] >> 1) & 0b1));;
        m_headLineDetectionStatus->setCurrentIndex(static_cast<bool>((statusMessage[2]) & 0b1));;
    }
}

void RadarUdpControlUI::timerStartCounterClicked()
{
    m_radarCommunicatorUdp->startWorkingThread();
    m_updateTimer->start(5000);
    m_autoreconnectionTimer->stop();
    if (m_reconnection) {
        emit SignalLogEvent(QString("Radar: Trying to reconnect"));
        m_reconnection = false;
    }
}

void RadarUdpControlUI::timerStopCounterClicked()
{
    m_radarCommunicatorUdp->stopWorkingThread();
    m_updateTimer->stop();
    m_autodisconnectionTimer->stop();
    if (!m_reconnected) {
        emit SignalLogEvent(QString("Radar: Disconnected (Auto)"));
        m_autoreconnectionTimer->start();
        m_reconnected = true;
        m_reconnection = true;
    } else {
        if (m_timerBeforeDisconnection->isActive()) {
            m_timerBeforeDisconnection->stop();
        }
    }
}

void RadarUdpControlUI::timerCounterValue(const int &number)
{
    m_timerCounter->display(static_cast<quint8>(number));
    m_extPowerUpTimer->setVisible(true);
    m_extPowerUpTimer->setValue(static_cast<quint8>(number));
    m_radarStatusLabel->setText(QString("Tx WarmUp"));
    m_radarStatusLedIndicator->setState(LedState::LedWait);
    m_reconnect->setVisible(false);
    m_extSkipWarmUpTimeButton->setVisible(true);
    m_startTx->setVisible(false);
    m_stopTx->setVisible(true);
//    m_extStartSwitch->setVisible(true);
}

void RadarUdpControlUI::powerUpIsComplete(const bool &powerUpStatus)
{
    SlotAllowSetup(powerUpStatus);
    if (powerUpStatus) {
        m_timerCounter->display("ready");
        m_extPowerUpTimer->setValue(0);
        m_extPowerUpTimer->setVisible(false);
        m_radarStatusLabel->setText(QString("Tx Ready"));
        m_radarStatusLedIndicator->setState(LedState::LedOn);
        m_reconnect->setVisible(false);
        m_extStartSwitch->setVisible(true);
        m_extSkipWarmUpTimeButton->setVisible(false);
        m_startTx->setVisible(false);
        m_stopTx->setVisible(true);
    }
    if (!m_logRadarConnectedOnce && m_connectedSpecialCounter > 99) {
        emit SignalLogEvent(QString("Radar: Connected"));
        m_logRadarConnectedOnce = true;
        m_logRadarDisconnectedOnce = false;
        m_logRadarNetworkErrorOnce = false;
    }
    emit SignalRadarIsWarmingUp(!powerUpStatus);
    if ((m_warmingUpStatus != powerUpStatus) && (m_connectedSpecialCounter > 99)) {
        if (powerUpStatus) {
            emit SignalLogEvent(QString("Radar: Warming Up Completed"));
        } else {
            emit SignalLogEvent(QString("Radar: Warming Up"));
        }
        m_warmingUpStatus = powerUpStatus;
    }
}

void RadarUdpControlUI::UISlotTriggerSetClicked()
{
    m_radarTriggerSettings.setTriggerGenetatorState(m_triggerEnable->currentIndex());
    m_radarTriggerSettings.setTriggerPeriod(m_triggerPeriod->currentIndex());
    m_radarTriggerSettings.setTriggerStaggerValue(m_triggerStaggerValue->value());
    QList<RadarAddressBytesE> addressList;
    QByteArray messageList;
    addressList.append(RadarAddressBytesE::Trigger_0);
    addressList.append(RadarAddressBytesE::Trigger_1);
    addressList.append(RadarAddressBytesE::Trigger_2);
    messageList = m_radarTriggerSettings.radarTriggerE();
    m_radarCommunicatorUdp->setRegisters(addressList, messageList);
}

void RadarUdpControlUI::UISlotTriggerDelaySetClicked()
{
    m_radarTriggerDelaySettings.setTriggerDelayValue(m_triggerDelay->value());
    QList<RadarAddressBytesE> addressList;
    QByteArray messageList;
    addressList.append(RadarAddressBytesE::TriggerDelay_0);
    addressList.append(RadarAddressBytesE::TriggerDelay_1);
    messageList = m_radarTriggerDelaySettings.radarTriggerDelay();
    m_radarCommunicatorUdp->setRegisters(addressList, messageList);
}

void RadarUdpControlUI::UISlotADSamplingFrequencySetClicked()
{
    m_radarADSamplingFrequencySettings.setRadarADFs(m_ADSamplingFrequency->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::ADControl, m_radarADSamplingFrequencySettings.radarADControl());
}

void RadarUdpControlUI::UISlotMiscZeroSetClicked()
{
    m_radarMiscZeroSettings.setAutotuneFine(m_miscZeroAutoTineFine->currentIndex());
    m_radarMiscZeroSettings.setAutotuneReg(m_miscZeroAutoTuneReg->currentIndex());
    m_radarMiscZeroSettings.setBlankMemControl(m_blankMemoryControl->currentIndex());
    m_radarMiscZeroSettings.setDivideACPInputSignal(m_miscZeroDivACP->currentIndex());
    m_radarMiscZeroSettings.setTestACP(m_miscZeroTestACP->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::Misc_0, m_radarMiscZeroSettings.radarMiscZero());
}

void RadarUdpControlUI::UISlotMiscOneSetClicked()
{
    m_radarMiscOneSettings.setAcpPeriod(m_miscOneAcpPeriod->currentIndex());
    m_radarMiscOneSettings.setBandwith(m_miscOneBandwidth->currentIndex());
    m_radarMiscOneSettings.setBlankingRam(m_miscOneBlackingRam->currentIndex());
    m_radarMiscOneSettings.setVideoSampleRes(m_videoSampleRes->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::Misc_1, m_radarMiscOneSettings.radarMiscOne());
}

void RadarUdpControlUI::UISlotHlSetupSetClicked()
{
    m_radarHlSetupSettings.setDelayValue(m_hlSetupDelay->value());
    m_radarHlSetupSettings.setEdge(m_hlSetupEdge->currentIndex());
    m_radarHlSetupSettings.setInvert(m_hlSetupInvert->currentIndex());
    m_radarHlSetupSettings.setWidth(m_hlSetupWidth->currentIndex());
    QList<RadarAddressBytesE> addressList;
    QByteArray messageList;
    addressList.append(RadarAddressBytesE::HLSetup_0);
    addressList.append(RadarAddressBytesE::HLSetup_1);
    messageList = m_radarHlSetupSettings.radarHlSetup();
    m_radarCommunicatorUdp->setRegisters(addressList, messageList);
}

void RadarUdpControlUI::UISlotEnableSetClicked()
{
    m_radarEnableSettings.setAdc(m_enableAD->currentIndex());
    m_radarEnableSettings.setAutoTune(m_enableAutoTune->currentIndex());
    m_radarEnableSettings.setDac(m_enableDA->currentIndex());
    m_radarEnableSettings.setDoubleBuffer(m_doubleBuffer->currentIndex());
    m_radarEnableSettings.setVideoAcquisition(m_videoAcquisition->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::Enable, m_radarEnableSettings.radarEnableE());
}

void RadarUdpControlUI::UISlotTuneCoatseSetClicked()
{
    m_radarTuneCoarseSettings.setCoarseValue(m_tuneCoarseValue->value());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::TuneCoarse, m_radarTuneCoarseSettings.radarTuneCoarseE());
}

void RadarUdpControlUI::UISlotTuneFineSetClicked()
{
    m_radarTuneFineSettings.setFineValue(m_tuneFineValue->value());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::TuneFine, m_radarTuneFineSettings.radarTuneFineE());
}

void RadarUdpControlUI::UISlotTuneRegSetClicked()
{
    m_radarTuneRegSettings.setRegValue(m_tuneRegValue->value());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::TuneReg, m_radarTuneRegSettings.radarTuneRegE());
}

void RadarUdpControlUI::UISlotSetupSetClicked()
{
    m_radarSetupSetiings.setTransceiverMode(m_setupTx->currentIndex());
    m_radarSetupSetiings.setEnableMotor(m_setupMotor->currentIndex());
    m_radarSetupSetiings.setPulseMode(m_setupPulseWidth->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::Setup, m_radarSetupSetiings.radarSetupE());
}

void RadarUdpControlUI::UISlotGateTuneDelaySetClicked()
{
    m_radarGateTuneDelaySettings.setTuneDelayValue(m_gateTuneDelayValue->value());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::GateTune, m_radarGateTuneDelaySettings.radarGateTuneDelayE());
}

void RadarUdpControlUI::UISlotAntSpeedSetClicked()
{
    m_radarAntSpeedSettings.setSpeedControl(m_antSpeedControl->currentIndex());
    m_radarAntSpeedSettings.setSpeedValue(m_antSpeedValue->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::AntSpeed, m_radarAntSpeedSettings.radarAntSpeedE());
}

void RadarUdpControlUI::UISlotMasterIPLowSetClicked()
{
    m_radarMasterIPLowSettings.setMasterIPLowValue(m_masterIPLow->value());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::MasterIP_Low, m_radarMasterIPLowSettings.radarMasterIPLowE());
}

void RadarUdpControlUI::UISlotDebugSetClicked()
{
    m_radarDebugSettings.setNoMasterMode(m_noMasterMode->currentIndex());
    m_radarDebugSettings.setPowerUpMode(m_powerUpMode->currentIndex());
    m_radarDebugSettings.setTxTestMode(m_txTestMode->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::Debug, m_radarDebugSettings.radarDebugE());
}

void RadarUdpControlUI::UISlotGetAllClicked()
{
    m_radarTriggerSettings = m_radarTriggerSettingsReceived;
    m_radarTriggerDelaySettings = m_radarTriggerDelaySettingsReceived;
    m_radarADSamplingFrequencySettings = m_radarADSamplingFrequencySettingsReceived;
    m_radarMiscZeroSettings = m_radarMiscZeroSettingsReceived;
    m_radarMiscOneSettings = m_radarMiscOneSettingsReceived;
    m_radarHlSetupSettings = m_radarHlSetupSettingsReceived;
    m_radarEnableSettings = m_radarEnableSettingsReceived;
    m_radarTuneCoarseSettings = m_radarTuneCoarseSettingsReceived;
    m_radarTuneFineSettings = m_radarTuneFineSettingsReceived;
    m_radarTuneRegSettings = m_radarTuneRegSettingsReceived;
    m_radarSetupSetiings = m_radarSetupSetiingsReceived;
    m_radarGateTuneDelaySettings = m_radarGateTuneDelaySettingsReceived;
    m_radarAntSpeedSettings = m_radarAntSpeedSettingsReceived;
    m_radarMasterIPLowSettings = m_radarMasterIPLowSettingsReceived;
    m_radarDebugSettings = m_radarDebugSettingsReceived;
    UISlotResetAllClicked();
}

void RadarUdpControlUI::UISlotSetAllClicked()
{
    UISlotTriggerSetClicked();
    UISlotTriggerDelaySetClicked();
    UISlotADSamplingFrequencySetClicked();
    UISlotMiscZeroSetClicked();
    UISlotMiscOneSetClicked();
    UISlotHlSetupSetClicked();
    UISlotEnableSetClicked();
    UISlotTuneCoatseSetClicked();
    UISlotTuneFineSetClicked();
    UISlotTuneRegSetClicked();
    UISlotSetupSetClicked();
    UISlotGateTuneDelaySetClicked();
    UISlotAntSpeedSetClicked();
    UISlotMasterIPLowSetClicked();
    UISlotDebugSetClicked();
}

void RadarUdpControlUI::UISlotResetAllClicked()
{
    UISlotTriggerWidgetUpdate();
    UISlotTriggerDelayUpdate();
    UISlotADControlWidgetUpdate();
    UISlotMiscZeroWidgetUpdate();
    UISlotMiscOneWidgetUpdate();
    UISlotHlSetupWidgetUpdate();
    UISlotEnableWidgetUpdate();
    UISlotTuneCoarseWidgetUpdate();
    UISlotTuneFineWidgetUpdate();
    UISlotTuneRegWidgetUpdate();
    UISlotSetupWidgetUpdate();
    UISlotGateTuneDelayWidgetUpdate();
    UISlotAntSpeedWidgetUpdate();
    UISlotMasterIPLowWidgetUpdate();
    UISlotDebugWidgetUpdate();
}

void RadarUdpControlUI::UISlotDefaultSettingsClicked()
{
    m_radarTriggerSettings = m_radarTriggerSettingsDefault;
    m_radarTriggerDelaySettings = m_radarTriggerDelaySettingsDefault;
    m_radarADSamplingFrequencySettings = m_radarADSamplingFrequencySettingsDefault;
    m_radarMiscZeroSettings = m_radarMiscZeroSettingsDefault;
    m_radarMiscOneSettings = m_radarMiscOneSettingsDefault;
    m_radarHlSetupSettings = m_radarHlSetupSettingsDefault;
    m_radarEnableSettings = m_radarEnableSettingsDefault;
    m_radarTuneCoarseSettings = m_radarTuneCoarseSettingsDefault;
    m_radarTuneFineSettings = m_radarTuneFineSettingsDefault;
    m_radarTuneRegSettings = m_radarTuneRegSettingsDefault;
    m_radarSetupSetiings = m_radarSetupSetiingsDefault;
    m_radarGateTuneDelaySettings = m_radarGateTuneDelaySettingsDefault;
    m_radarAntSpeedSettings = m_radarAntSpeedSettingsDefault;
    m_radarMasterIPLowSettings = m_radarMasterIPLowSettingsDefault;
    m_radarDebugSettings = m_radarDebugSettingsDefault;
    UISlotResetAllClicked();
}

void RadarUdpControlUI::UISlotTriggerWidgetUpdate()
{
    m_triggerEnable->setCurrentIndex(m_radarTriggerSettings.getTriggerGenetatorState());
    m_triggerStaggerValue->setValue(m_radarTriggerSettings.getTriggerStaggerValue());
    m_triggerPeriod->setCurrentIndex(m_radarTriggerSettings.getTriggerPeriod());
}

void RadarUdpControlUI::UISlotTriggerDelayUpdate()
{
    m_triggerDelay->setValue(m_radarTriggerDelaySettings.m_triggerDelayValue);
}

void RadarUdpControlUI::UISlotADControlWidgetUpdate()
{
    m_ADSamplingFrequency->setCurrentIndex(m_radarADSamplingFrequencySettings.getRadarADFs());
}

void RadarUdpControlUI::UISlotMiscZeroWidgetUpdate()
{
    m_blankMemoryControl->setCurrentIndex(m_radarMiscZeroSettings.getBlankMemControl());
    m_miscZeroTestACP->setCurrentIndex(m_radarMiscZeroSettings.getTestACP());
    m_miscZeroAutoTineFine->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneFine());
    m_miscZeroAutoTuneReg->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneReg());
    m_miscZeroDivACP->setCurrentIndex(m_radarMiscZeroSettings.getDivideACPInputSignal());
}

void RadarUdpControlUI::UISlotMiscOneWidgetUpdate()
{
    m_videoSampleRes->setCurrentIndex(m_radarMiscOneSettings.getVideoSampleRes());
    m_miscOneAcpPeriod->setCurrentIndex(m_radarMiscOneSettings.getAcpPeriod());
    m_miscOneBlackingRam->setCurrentIndex(m_radarMiscOneSettings.getBlankingRam());
    m_miscOneBandwidth->setCurrentIndex(m_radarMiscOneSettings.getBandwith());
}

void RadarUdpControlUI::UISlotHlSetupWidgetUpdate()
{
    m_hlSetupWidth->setCurrentIndex(m_radarHlSetupSettings.getWidth());
    m_hlSetupInvert->setCurrentIndex(m_radarHlSetupSettings.getInvert());
    m_hlSetupEdge->setCurrentIndex(m_radarHlSetupSettings.getEdge());
    m_hlSetupDelay->setValue(m_radarHlSetupSettings.getDelayValue());
}

void RadarUdpControlUI::UISlotEnableWidgetUpdate()
{
    m_videoAcquisition->setCurrentIndex(m_radarEnableSettings.getVideoAcquisition());
    m_doubleBuffer->setCurrentIndex(m_radarEnableSettings.getDoubleBuffer());
    m_enableAutoTune->setCurrentIndex(m_radarEnableSettings.getAutoTune());
    m_enableDA->setCurrentIndex(m_radarEnableSettings.getDac());
    m_enableAD->setCurrentIndex(m_radarEnableSettings.getAdc());
}

void RadarUdpControlUI::UISlotTuneCoarseWidgetUpdate()
{
    m_tuneCoarseValue->setValue(m_radarTuneCoarseSettings.m_coarseValue);
}

void RadarUdpControlUI::UISlotTuneFineWidgetUpdate()
{
    m_tuneFineValue->setValue(m_radarTuneFineSettings.m_fineValue);
}

void RadarUdpControlUI::UISlotTuneRegWidgetUpdate()
{
    m_tuneRegValue->setValue(m_radarTuneRegSettings.m_regValue);
}

void RadarUdpControlUI::UISlotSetupWidgetUpdate()
{
    m_setupTx->setCurrentIndex(m_radarSetupSetiings.getTransceiverMode());
    m_setupPulseWidth->setCurrentIndex(m_radarSetupSetiings.getPulseMode());
    m_setupMotor->setCurrentIndex(m_radarSetupSetiings.getEnableMotor());
}

void RadarUdpControlUI::UISlotGateTuneDelayWidgetUpdate()
{
    m_gateTuneDelayValue->setValue(m_radarGateTuneDelaySettings.m_tuneDelay);
}


void RadarUdpControlUI::UISlotAntSpeedWidgetUpdate()
{
    m_antSpeedControl->setCurrentIndex(m_radarAntSpeedSettings.getSpeedControl());
    m_antSpeedValue->setCurrentIndex(m_radarAntSpeedSettings.getSpeedValue());
}

void RadarUdpControlUI::UISlotMasterIPLowWidgetUpdate()
{
    m_masterIPLow->setValue(m_radarMasterIPLowSettings.m_masterIPLow);
}

void RadarUdpControlUI::UISlotDebugWidgetUpdate()
{
    m_noMasterMode->setCurrentIndex(m_radarDebugSettings.getNoMasterMode());
    m_txTestMode->setCurrentIndex(m_radarDebugSettings.getTxTestMode());
    m_powerUpMode->setCurrentIndex(m_radarDebugSettings.getPowerUpMode());
}

void RadarUdpControlUI::UISlotRestartWorkingThread()
{
    if (m_radarCommunicatorUdp->isWorking()) {
        m_radarCommunicatorUdp->stopWorkingThread();
        m_updateTimer->stop();
        m_radarCommunicatorUdp->msleep(100);
    }
    m_radarCommunicatorUdp->startWorkingThread();
    m_updateTimer->start(5000);
}

void RadarUdpControlUI::SlotAutoUpdate()
{
    UISlotGetAllClicked();
    m_updatedOnce = true;
    m_updateTimer->stop();
}

void RadarUdpControlUI::SlotStartAutoUpdate()
{

}

void RadarUdpControlUI::SlotStopAutoUpdate()
{

}

void RadarUdpControlUI::SlotStartTxClicked()
{
    m_radarSetupSetiings.setTransceiverMode(1);
    m_radarSetupSetiings.setEnableMotor(static_cast<bool>(!m_powerMotorCheckBox->isChecked()));
    UISlotSetupWidgetUpdate();
    UISlotSetupSetClicked();
    m_updateTimer->start(3000);
}

void RadarUdpControlUI::SlotStopTxClicked()
{
    m_radarSetupSetiings.setTransceiverMode(0);
    m_radarSetupSetiings.setEnableMotor(0);
    UISlotSetupWidgetUpdate();
    UISlotSetupSetClicked();
    m_updateTimer->start(3000);
}

void RadarUdpControlUI::OpenSettingsClicked()
{
    if (this->isVisible()) {
        this->close();
    } else {
        this->show();
    }
}

void RadarUdpControlUI::SlotDisconnect()
{
    if (m_reconnection) {
        m_radarStatusLabel->setText("Disconnected (Reconnection)");
    } else {
        m_radarStatusLabel->setText("Disconnected");
    }
    m_connected = false;
    m_radarStatusLedIndicator->setState(LedState::LedOff);
    m_reconnect->setVisible(true);
    m_extStartSwitch->setVisible(false);
    m_extSkipWarmUpTimeButton->setVisible(false);
    m_startTx->setVisible(true);
    m_stopTx->setVisible(false);
    if (!m_logRadarDisconnectedOnce) {
        emit SignalLogEvent(QString("Radar: Disconnected"));
        m_logRadarConnectedOnce = false;
        m_logRadarDisconnectedOnce = true;
        m_logRadarNetworkErrorOnce = false;
    }
}

void RadarUdpControlUI::SlotNetworkError()
{
    m_radarStatusLabel->setText("Network Error");
    m_radarStatusLedIndicator->setState(LedState::LedError);
    m_reconnect->setVisible(true);
    m_extStartSwitch->setVisible(false);
    m_extSkipWarmUpTimeButton->setVisible(false);
    m_startTx->setVisible(true);
    m_stopTx->setVisible(true);
    if (!m_logRadarNetworkErrorOnce) {
        emit SignalLogEvent(QString("Radar: Network Error"));
        m_logRadarConnectedOnce = false;
        m_logRadarDisconnectedOnce = false;
        m_logRadarNetworkErrorOnce = true;
    }
    if (!m_timerBeforeDisconnection->isActive() && !m_connected) {
        m_timerBeforeDisconnection->start();
    }
}

void RadarUdpControlUI::SlotPreDisconnection()
{
    m_timerBeforeDisconnection->stop();
    if (!m_connected) {
        m_reconnected = false;
        m_autodisconnectionTimer->start();
        emit SignalLogEvent(QString("Radar: Reconnrction failed. Restart the radar"));
    } else {
        m_reconnected = true;
        emit SignalLogEvent(QString("Radar: Reconnected"));
    }
}

void RadarUdpControlUI::SlotGetAndSetDefaults()
{
    /*---------------*/
    m_radarTriggerSettingsDefault.setTriggerGenetatorState(m_iniSettings->value("RadarTriggerGeneratorStateUDP").toInt() > -1
                                                         ? m_iniSettings->value("RadarTriggerGeneratorStateUDP").toInt()
                                                         : m_radarTriggerSettingsReceived.getTriggerGenetatorState());

    m_radarTriggerSettingsDefault.setTriggerPeriod(m_iniSettings->value("RadarTriggerPeriodUDP").toInt() > -1
                                                 ? m_iniSettings->value("RadarTriggerPeriodUDP").toInt()
                                                 : m_radarTriggerSettingsReceived.getTriggerPeriod());

    m_radarTriggerSettingsDefault.setTriggerStaggerValue(m_iniSettings->value("RadarTriggerStaggerValueUDP").toInt() > -1
                                                       ? m_iniSettings->value("RadarTriggerStaggerValueUDP").toInt()
                                                       : m_radarTriggerSettingsReceived.getTriggerStaggerValue());


    /*---------------*/
    m_radarTriggerDelaySettingsDefault.setTriggerDelayValue(m_iniSettings->value("RadarTriggerDelayValueUDP").toInt() > -1
                                                          ? m_iniSettings->value("RadarTriggerDelayValueUDP").toInt()
                                                          : m_radarTriggerDelaySettingsReceived.m_triggerDelayValue);


    /*---------------*/
    m_radarADSamplingFrequencySettingsDefault.setRadarADFs(m_iniSettings->value("RadarADCSampligFrequencyUDP").toInt() > -1
                                                       ? m_iniSettings->value("RadarADCSampligFrequencyUDP").toInt()
                                                       : m_radarADSamplingFrequencySettingsReceived.getRadarADFs());


    /*---------------*/
    m_radarMiscZeroSettingsDefault.setBlankMemControl(m_iniSettings->value("RadarMiscZeroBlankMemComtrolUDP").toInt() > -1
                                                    ? m_iniSettings->value("RadarMiscZeroBlankMemComtrolUDP").toInt()
                                                    : m_radarMiscZeroSettingsReceived.getBlankMemControl());

    m_radarMiscZeroSettingsDefault.setTestACP(m_iniSettings->value("RadarMiscZeroTestACPUDP").toInt() > -1
                                            ? m_iniSettings->value("RadarMiscZeroTestACPUDP").toInt()
                                            : m_radarMiscZeroSettingsReceived.getTestACP());

    m_radarMiscZeroSettingsDefault.setAutotuneFine(m_iniSettings->value("RadarMiscZeroAutoTuneFineUDP").toInt() > -1
                                                 ? m_iniSettings->value("RadarMiscZeroAutoTuneFineUDP").toInt()
                                                 : m_radarMiscZeroSettingsReceived.getAutotuneFine());

    m_radarMiscZeroSettingsDefault.setAutotuneReg(m_iniSettings->value("RadarMiscZeroAutoTuneRegUDP").toInt() > -1
                                                ? m_iniSettings->value("RadarMiscZeroAutoTuneRegUDP").toInt()
                                                : m_radarMiscZeroSettingsReceived.getAutotuneReg());

    m_radarMiscZeroSettingsDefault.setDivideACPInputSignal(m_iniSettings->value("RadarMiscZeroDivideACPInputUDP").toInt() > -1
                                                         ? m_iniSettings->value("RadarMiscZeroDivideACPInputUDP").toInt()
                                                         : m_radarMiscZeroSettingsReceived.getDivideACPInputSignal());


    /*---------------*/
    m_radarMiscOneSettingsDefault.setVideoSampleRes(m_iniSettings->value("RadarMiscOneVideoSampleResUDP").toInt() > -1
                                                  ? m_iniSettings->value("RadarMiscOneVideoSampleResUDP").toInt()
                                                  : m_radarMiscOneSettingsReceived.getVideoSampleRes());

    m_radarMiscOneSettingsDefault.setAcpPeriod(m_iniSettings->value("RadarMiscOneAcpPeriodUDP").toInt() > -1
                                             ? m_iniSettings->value("RadarMiscOneAcpPeriodUDP").toInt()
                                             : m_radarMiscOneSettingsReceived.getAcpPeriod());

    m_radarMiscOneSettingsDefault.setBlankingRam(m_iniSettings->value("RadarMiscOneBlankingRamUDP").toInt() > -1
                                               ? m_iniSettings->value("RadarMiscOneBlankingRamUDP").toInt()
                                               : m_radarMiscOneSettingsReceived.getBlankingRam());

    m_radarMiscOneSettingsDefault.setBandwith(m_iniSettings->value("RadarMiscOneBandwithUDP").toInt() > -1
                                            ? m_iniSettings->value("RadarMiscOneBandwithUDP").toInt()
                                            : m_radarMiscOneSettingsReceived.getBandwith());


    /*---------------*/
    m_radarHlSetupSettingsDefault.setWidth(m_iniSettings->value("RadarHlSetupWidthUDP").toInt() > -1
                                          ? m_iniSettings->value("RadarHlSetupWidthUDP").toInt()
                                          : m_radarHlSetupSettingsReceived.getWidth());

    m_radarHlSetupSettingsDefault.setInvert(m_iniSettings->value("RadarHlSetupInvertUDP").toInt() > -1
                                          ? m_iniSettings->value("RadarHlSetupInvertUDP").toInt()
                                          : m_radarHlSetupSettingsReceived.getInvert());

    m_radarHlSetupSettingsDefault.setEdge(m_iniSettings->value("RadarHlSetupEdgeUDP").toInt() > -1
                                        ? m_iniSettings->value("RadarHlSetupEdgeUDP").toInt()
                                        : m_radarHlSetupSettingsReceived.getEdge());

    m_radarHlSetupSettingsDefault.setDelayValue(m_iniSettings->value("RadarHlSetupDelayValueUDP").toInt() > -1
                                              ? m_iniSettings->value("RadarHlSetupDelayValueUDP").toInt()
                                              : m_radarHlSetupSettingsReceived.getDelayValue());


    /*---------------*/
    m_radarEnableSettingsDefault.setVideoAcquisition(m_iniSettings->value("RadarEnableVideoAcquisitionUDP").toInt() > -1
                                                   ? m_iniSettings->value("RadarEnableVideoAcquisitionUDP").toInt()
                                                   : m_radarEnableSettingsReceived.getVideoAcquisition());

    m_radarEnableSettingsDefault.setDoubleBuffer(m_iniSettings->value("RadarEnableDoubleBufferUDP").toInt() > -1
                                               ? m_iniSettings->value("RadarEnableDoubleBufferUDP").toInt()
                                               : m_radarEnableSettingsReceived.getDoubleBuffer());

    m_radarEnableSettingsDefault.setAutoTune(m_iniSettings->value("RadarEnableAutoTuneUDP").toInt() > -1
                                           ? m_iniSettings->value("RadarEnableAutoTuneUDP").toInt()
                                           : m_radarEnableSettingsReceived.getAutoTune());

    m_radarEnableSettingsDefault.setDac(m_iniSettings->value("RadarEnableDAConvertersUDP").toInt() > -1
                                      ? m_iniSettings->value("RadarEnableDAConvertersUDP").toInt()
                                      : m_radarEnableSettingsReceived.getDac());

    m_radarEnableSettingsDefault.setAdc(m_iniSettings->value("RadarEnableADConvertersUDP").toInt() > -1
                                      ? m_iniSettings->value("RadarEnableADConvertersUDP").toInt()
                                      : m_radarEnableSettingsReceived.getAdc());


    /*---------------*/
    m_radarGateTuneDelaySettingsDefault.setTuneDelayValue(m_iniSettings->value("RadarGateTuneDelayValueUDP").toInt() > -1
                                                        ? m_iniSettings->value("RadarGateTuneDelayValueUDP").toInt()
                                                        : m_radarGateTuneDelaySettingsReceived.getTuneDelayValue());


    /*---------------*/
    m_radarAntSpeedSettingsDefault.setSpeedControl(m_iniSettings->value("RadarAntSpeedControlUDP").toInt() > -1
                                                 ? m_iniSettings->value("RadarAntSpeedControlUDP").toInt()
                                                 : m_radarAntSpeedSettingsReceived.getSpeedControl());

    m_radarAntSpeedSettingsDefault.setSpeedValue(m_iniSettings->value("RadarAntSpeedValueUDP").toInt() > -1
                                               ? m_iniSettings->value("RadarAntSpeedValueUDP").toInt()
                                               : m_radarAntSpeedSettingsReceived.getSpeedValue());


    /*---------------*/
    m_radarMasterIPLowSettingsDefault.setMasterIPLowValue(m_iniSettings->value("RadarMasterIPLowUDP").toInt() > -1
                                                        ? m_iniSettings->value("RadarMasterIPLowUDP").toInt()
                                                        : m_radarMasterIPLowSettingsReceived.getMasterIPLowValue());


    /*---------------*/
    m_radarTuneCoarseSettingsDefault.setCoarseValue(m_iniSettings->value("RadarTuneCoarseUDP").toInt() > -1
                                                  ? m_iniSettings->value("RadarTuneCoarseUDP").toInt()
                                                  : m_radarTuneCoarseSettingsReceived.m_coarseValue);

    m_radarTuneFineSettingsDefault.setFineValue(m_iniSettings->value("RadarTuneFineUDP").toInt() > -1
                                              ? m_iniSettings->value("RadarTuneFineUDP").toInt()
                                              : m_radarTuneFineSettingsReceived.m_fineValue);

    m_radarTuneRegSettingsDefault.setRegValue(m_iniSettings->value("RadarTuneRegUDP").toInt() > -1
                                            ? m_iniSettings->value("RadarTuneRegUDP").toInt()
                                            : m_radarTuneRegSettingsReceived.m_regValue);


    /*---------------*/
    m_radarDebugSettingsDefault.setNoMasterMode(m_iniSettings->value("RadarDebugNoMasterModeUDP").toInt() > -1
                                              ? m_iniSettings->value("RadarDebugNoMasterModeUDP").toInt()
                                              : m_radarDebugSettingsReceived.getNoMasterMode());

    m_radarDebugSettingsDefault.setTxTestMode(m_iniSettings->value("RadarDebugTxTestModeUDP").toInt() > -1
                                            ? m_iniSettings->value("RadarDebugTxTestModeUDP").toInt()
                                            : m_radarDebugSettingsReceived.getTxTestMode());

    m_radarDebugSettingsDefault.setPowerUpMode(m_iniSettings->value("RadarDebugPowerUpModeUDP").toInt() > -1
                                             ? m_iniSettings->value("RadarDebugPowerUpModeUDP").toInt()
                                             : m_radarDebugSettingsReceived.getPowerUpMode());


    /*---------------*/
    m_radarSetupSetiingsDefault.setTransceiverMode(m_iniSettings->value("RadarSetupTransceiverModeUDP").toInt() > -1
                                                 ? m_iniSettings->value("RadarSetupTransceiverModeUDP").toInt()
                                                 : m_radarSetupSetiingsReceived.getTransceiverMode());

    m_radarSetupSetiingsDefault.setPulseMode(m_iniSettings->value("RadarSetupPulseModeUDP").toInt() > -1
                                           ? m_iniSettings->value("RadarSetupPulseModeUDP").toInt()
                                           : m_radarSetupSetiingsReceived.getPulseMode());

    m_radarSetupSetiingsDefault.setEnableMotor(m_iniSettings->value("RadarSetupEnableMotorUDP").toInt() > -1
                                             ? m_iniSettings->value("RadarSetupEnableMotorUDP").toInt()
                                             : m_radarSetupSetiingsReceived.getEnableMotor());


    UISlotDefaultSettingsClicked();
    UISlotSetAllClicked();
}

void RadarUdpControlUI::SlotAllowSetup(const bool &allow)
{
    m_setAll->setEnabled(allow);
    m_resetAll->setEnabled(allow);
    m_defaultSettings->setEnabled(allow);
}

void RadarUdpControlUI::SlotCheckStatus()
{
    if (m_radarSetupSetiingsReceived.getEnableMotor()) {
        m_powerMotorLedIndicator->setState(LedState::LedOn);
    } else {
        m_powerMotorLedIndicator->setState(LedState::LedOff);
    }

    if (m_radarSetupSetiingsReceived.getTransceiverMode()) {
        m_extStartSwitch->setText("Stop");
        m_extStartSwitchCheckState = true;
    } else {
        m_extStartSwitch->setText("Start");
        m_extStartSwitchCheckState = false;
    }

    if (!m_txDisabled && !m_blindZone) {
        if (m_radarSetupSetiingsReceived.getTransceiverMode()) {
            m_powerTxLedIndicator->setState(LedState::LedOn);
        } else {
            m_powerTxLedIndicator->setState(LedState::LedOff);
        }
    } else {
        m_powerTxLedIndicator->setState(LedState::LedOff);
    }

    bool half = false;
    if (m_radarEnableSettingsReceived.getAdc()) {
        half = true;
    }
    if (m_radarEnableSettingsReceived.getDac()) {
        if (half) {
            m_extAdcDacLedIndicator->setState(LedState::LedOn);
        } else {
            m_extAdcDacLedIndicator->setState(LedState::LedWait);
        }
    } else if (half) {
        m_extAdcDacLedIndicator->setState(LedState::LedWait);
    } else {
        m_extAdcDacLedIndicator->setState(LedState::LedError);
    }
    if (!m_extTuneCoarse->hasFocus()) {
        m_extTuneCoarse->setValue(m_radarTuneCoarseSettingsReceived.m_coarseValue);
        emit SignalSetTuning(m_radarTuneCoarseSettingsReceived.m_coarseValue);
    }
    if (!m_extTuneFine->hasFocus()) {
        m_extTuneFine->setValue(m_radarTuneFineSettingsReceived.m_fineValue);
    }

    m_extPowerIndicator->setValue(m_magIndidcatorValue->value());

}

void RadarUdpControlUI::SlotMotorControlCheckStateChanged()
{
    bool checkState = !m_powerMotorCheckBox->isChecked();
    if (m_radarSetupSetiings.getTransceiverMode()) {
        m_radarSetupSetiings.setEnableMotor(static_cast<bool>(checkState));
        UISlotSetupWidgetUpdate();
        UISlotSetupSetClicked();
        m_updateTimer->start(3000);
    }
}

void RadarUdpControlUI::SlotExtStartTxClicked()
{
    if (m_extStartSwitchCheckState) {
        SlotStopTxClicked();
        m_powerTxLedIndicator->setState(LedState::LedOff);
    } else {
        SlotStartTxClicked();
    }
    m_extStartSwitchCheckState =!m_extStartSwitchCheckState;
}

void RadarUdpControlUI::SlotRadarIsOffline()
{
    m_extEthernetConnectionLedIndicator->setState(LedState::LedError);
}

void RadarUdpControlUI::UISlotExtSetPulseWidthStateChanged()
{
    bool checkState = m_extSetPulseWidth->isChecked();
    if (checkState) {
        m_extPulseWidth->setEnabled(true);
    } else {
        m_extPulseWidth->setEnabled(false);
    }
}

void RadarUdpControlUI::UISlotExtInitClicked()
{
    UISlotGetAllClicked();
    SlotGetAndSetDefaults();
}

void RadarUdpControlUI::UISlotExtTuneCoarseEditingFinished()
{
    m_tuneCoarseValue->setValue(m_extTuneCoarse->value());
    UISlotTuneCoatseSetClicked();
    m_updateTimer->start(3000);
}

void RadarUdpControlUI::UISlotExtTuneFineEditingFinished()
{
    m_tuneFineValue->setValue(m_extTuneFine->value());
    UISlotTuneFineSetClicked();
    m_updateTimer->start(3000);
}

void RadarUdpControlUI::UISlotExtPulseWidthChanged(const int &index)
{
    m_setupPulseWidth->setCurrentIndex(index);
    UISlotSetupSetClicked();
    m_updateTimer->start(3000);
}

void RadarUdpControlUI::UISlotExtSkipWarmUpTimeClicked()
{
    m_radarDebugSettings.setNoMasterMode(m_noMasterMode->currentIndex());
    m_radarDebugSettings.setPowerUpMode(1);
    m_radarDebugSettings.setTxTestMode(m_txTestMode->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::Debug, m_radarDebugSettings.radarDebugE());
    m_radarDebugSettings.setNoMasterMode(m_noMasterMode->currentIndex());
    m_radarDebugSettings.setPowerUpMode(0);
    m_radarDebugSettings.setTxTestMode(m_txTestMode->currentIndex());
    m_radarCommunicatorUdp->setRegisters(RadarAddressBytesE::Debug, m_radarDebugSettings.radarDebugE());
    m_updateTimer->start(3000);
}

void RadarUdpControlUI::SlotStatusMessageReceived(const QString &message)
{
    emit SignalstatusMessage(message);
    m_statusMsgTimer->start();
}

void RadarUdpControlUI::SlotClearStatusMessage()
{
    emit SignalstatusMessage(QString(""));
    m_statusMsgTimer->stop();
}

void RadarUdpControlUI::SlotDataSavingState(const bool &savingState)
{
    m_extTuneCoarse->setEnabled(!savingState);
    m_extTuneFine->setEnabled(!savingState);
    if (m_extSetPulseWidth->isChecked()) {
        m_extSetPulseWidth->setChecked(!savingState);
    }
    m_extSetPulseWidth->setEnabled(!savingState);
}

void RadarUdpControlUI::SlotSetTrasmitterOn(const bool &isTransmitting)
{
    m_radarSetupSetiings.setTransceiverMode(static_cast<bool>(isTransmitting));
    UISlotSetupWidgetUpdate();
    UISlotSetupSetClicked();
    m_updateTimer->start(3000);
}

void RadarUdpControlUI::SlotSetAnttennaRotating(const bool &isRotating)
{
    m_radarSetupSetiings.setTransceiverMode(static_cast<bool>(isRotating));
    m_radarSetupSetiings.setEnableMotor(static_cast<bool>(isRotating));
    UISlotSetupWidgetUpdate();
    UISlotSetupSetClicked();
    m_updateTimer->start(3000);
}

void RadarUdpControlUI::SlotTxOff(const bool &off)
{
    m_txDisabled = off;
    if (off) {
        m_powerTxLedIndicator->setState(LedState::LedOff);
    }
}

void RadarUdpControlUI::SlotBlindZoneIsActive(const bool &active)
{
    m_blindZone = active;
    if (!m_txDisabled & active) {
        m_powerTxLedIndicator->setState(LedState::LedOff);
    }
}
