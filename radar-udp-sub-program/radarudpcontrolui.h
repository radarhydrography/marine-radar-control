﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef RADARUDPCONTROLUI_H
#define RADARUDPCONTROLUI_H

#include <QWidget>
#include <QPushButton>
#include <QLCDNumber>
#include <QGroupBox>
#include <QSpinBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QTimer>
#include <QLabel>
#include <QToolButton>

#include "radarcommunicatorudp.h"
#include "shared-libs/inireader.h"
#include "shared-libs/ledindicator.h"

class RadarUdpControlUI : public QWidget
{
    Q_OBJECT

    QString m_pcHostAddress;
    quint64 m_pcReadPort;
    quint64 m_pcWritePort;
    QString m_radarHostAddress;
    quint64 m_radarReadPort;
    quint64 m_radarWritePort;

    QByteArray m_radarBuffer;

    QTimer *m_updateTimer;
    QTimer *m_statusTimer;
    QTimer *m_onlineTimer;
    QTimer *m_statusMsgTimer;

    bool m_updatedOnce = false;

    RadarCommunicatorUdp *m_radarCommunicatorUdp;

    QMap<QString, QString> *m_iniSettings;

    bool m_powerUpModeState = true;
    bool m_warmingUpStatus = true;

    QTimer *m_timerBeforeDisconnection;
    QTimer *m_autodisconnectionTimer;
    QTimer *m_autoreconnectionTimer;
    bool m_reconnection = false;
    bool m_reconnected = false;
    bool m_connected = false;

    bool m_logRadarConnectedOnce = false;
    bool m_logRadarDisconnectedOnce = false;
    bool m_logRadarNetworkErrorOnce = false;
    int m_connectedSpecialCounter = 0;

public:

    RadarUdpControlUI(QMap<QString, QString> iniSettings = QMap<QString, QString>(), QWidget *parent = nullptr);
    ~RadarUdpControlUI();

    QWidget *m_externalControlWidget;
    QWidget *ExternalControlWidget();

    void exit();

    void setIniSettings(QMap<QString, QString> &iniSettings);
    QMap<QString, QString> getIniSettings();

private:

    QToolButton *m_startTx;
    QToolButton *m_stopTx;
    QPushButton *m_extStartSwitch;
    bool m_extStartSwitchCheckState = false;
    QToolButton *m_openSettings;
    QSpinBox *m_extPowerUpTimer;
    QLabel *m_radarStatusLabel;
    LedIndicator *m_radarStatusLedIndicator;
    QPushButton *m_reconnect;
    QPushButton *m_powerMotorCheckBox;
    LedIndicator *m_powerTxLedIndicator;
    LedIndicator *m_powerMotorLedIndicator;
    QSpinBox *m_extTuneCoarse;
    QSpinBox *m_extTuneFine;
    QSpinBox *m_extPowerIndicator;
    QComboBox *m_extPulseWidth;
    QPushButton *m_extSetPulseWidth;
    LedIndicator *m_extAdcDacLedIndicator;
    LedIndicator *m_extEthernetConnectionLedIndicator;
    QToolButton *m_extInitButton;
    QPushButton *m_extSkipWarmUpTimeButton;
    bool m_txDisabled = false;
    bool m_blindZone = false;

    QWidget *TimerWidget();
    QPushButton *m_timerStartCounter;
    QPushButton *m_timerStopCounter;
    QLCDNumber *m_timerCounter;

    QWidget *TriggerWidget();
    QComboBox *m_triggerEnable;
    QSpinBox *m_triggerStaggerValue;
    QComboBox *m_triggerPeriod;
    QPushButton *m_triggerSet;
    RadarTriggerE m_radarTriggerSettings;

    QWidget *TriggerDelayWidget();
    QSpinBox *m_triggerDelay;
    QPushButton *m_triggerDelaySet;
    RadarTriggerDelayE m_radarTriggerDelaySettings;

    QWidget *ADControlWidget();
    QComboBox *m_ADSamplingFrequency;
    QPushButton *m_ADSamplingFrequencySet;
    RadarADControlE m_radarADSamplingFrequencySettings;

    QWidget *MiscZeroWidget();
    QPushButton *m_miscZeroSet;
    QComboBox *m_blankMemoryControl;
    QComboBox *m_miscZeroTestACP;
    QComboBox *m_miscZeroAutoTineFine;
    QComboBox *m_miscZeroAutoTuneReg;
    QComboBox *m_miscZeroDivACP;
    RadarMiscZeroE m_radarMiscZeroSettings;

    QWidget *MiscOneWidget();
    QPushButton *m_miscOneSet;
    QComboBox *m_videoSampleRes;
    QComboBox *m_miscOneAcpPeriod;
    QComboBox *m_miscOneBlackingRam;
    QComboBox *m_miscOneBandwidth;
    RadarMiscOneE m_radarMiscOneSettings;

    QWidget *HlSetupWidget();
    QPushButton *m_hlSetupSet;
    QComboBox *m_hlSetupWidth;
    QComboBox *m_hlSetupInvert;
    QComboBox *m_hlSetupEdge;
    QSpinBox *m_hlSetupDelay;
    RadarHlSetupE m_radarHlSetupSettings;

    QWidget *EnableWidget();
    QPushButton *m_enableSet;
    QComboBox *m_videoAcquisition;
    QComboBox *m_doubleBuffer;
    QComboBox *m_enableAutoTune;
    QComboBox *m_enableDA;
    QComboBox *m_enableAD;
    RadarEnableE m_radarEnableSettings;

    QWidget *TuneCoarseWidget();
    QPushButton *m_tuneCoatseSet;
    QSpinBox *m_tuneCoarseValue;
    RadarTuneCoarseE m_radarTuneCoarseSettings;

    QWidget *TuneFineWidget();
    QPushButton *m_tuneFineSet;
    QSpinBox *m_tuneFineValue;
    RadarTuneFineE m_radarTuneFineSettings;

    QWidget *TuneRegWidget();
    QPushButton *m_tuneRegSet;
    QSpinBox *m_tuneRegValue;
    RadarTuneRegE m_radarTuneRegSettings;

    QWidget *SetupWidget();
    QPushButton *m_setupSet;
    QComboBox *m_setupTx;
    QComboBox *m_setupPulseWidth;
    QComboBox *m_setupMotor;
    RadarSetupE m_radarSetupSetiings;

    QWidget *GateTuneDelayWidget();
    QPushButton *m_gateTuneDelaySet;
    QSpinBox *m_gateTuneDelayValue;
    RadarGateTuneDelayE m_radarGateTuneDelaySettings;

    QWidget *AutotuneFineWidget();
    QSpinBox *m_autotuneFineValue;

    QWidget *AutotuneRegWidget();
    QSpinBox *m_autotuneRegValue;

    QWidget *AntSpeedWidget();
    QPushButton *m_antSpeedSet;
    QComboBox *m_antSpeedControl;
    QComboBox *m_antSpeedValue;
    RadarAntSpeedE m_radarAntSpeedSettings;

    QWidget *MasterIPLowWidget();
    QPushButton *m_masterIPLowSet;
    QSpinBox *m_masterIPLow;
    RadarMasterIPLowE m_radarMasterIPLowSettings;

    QWidget *MagIndicatorWidget();
    QSpinBox *m_magIndidcatorValue;

    QWidget *TuneIndicatorWidget();
    QSpinBox *m_tuneIndicatorValue;

    QWidget *DebugWidget();
    QPushButton *m_debugSet;
    QComboBox *m_noMasterMode;
    QComboBox *m_txTestMode;
    QComboBox *m_powerUpMode;
    RadarDebugE m_radarDebugSettings;

    QWidget *StatusWidget();
    QComboBox *m_statusPowerUp;
    QComboBox *m_statusAutotuneReg;
    QComboBox *m_statusAutotuneFine;
    QComboBox *m_statusAcpInputSignal;
    QComboBox *m_statusHlInputSignal;
    QComboBox *m_statusTriggerInputSignal;

    QWidget *StatusWidgetRegular();
    QComboBox *m_triggerTxStatus;
    QComboBox *m_powerUpStatus;
    QComboBox *m_masterSlaveMode;
    QComboBox *m_triggerStatus;
    QComboBox *m_localTransmitionStatus;
    QComboBox *m_asimuthEncoderFailStatus;
    QComboBox *m_headLineDetectionStatus;

    QWidget *GeneralWidget();
    QPushButton *m_getAll;
    QPushButton *m_setAll;
    QPushButton *m_resetAll;
    QPushButton *m_defaultSettings;

    RadarTriggerE m_radarTriggerSettingsReceived;
    RadarTriggerDelayE m_radarTriggerDelaySettingsReceived;
    RadarADControlE m_radarADSamplingFrequencySettingsReceived;
    RadarMiscZeroE m_radarMiscZeroSettingsReceived;
    RadarMiscOneE m_radarMiscOneSettingsReceived;
    RadarHlSetupE m_radarHlSetupSettingsReceived;
    RadarEnableE m_radarEnableSettingsReceived;
    RadarTuneCoarseE m_radarTuneCoarseSettingsReceived;
    RadarTuneFineE m_radarTuneFineSettingsReceived;
    RadarTuneRegE m_radarTuneRegSettingsReceived;
    RadarSetupE m_radarSetupSetiingsReceived;
    RadarGateTuneDelayE m_radarGateTuneDelaySettingsReceived;
    RadarAntSpeedE m_radarAntSpeedSettingsReceived;
    RadarMasterIPLowE m_radarMasterIPLowSettingsReceived;
    RadarDebugE m_radarDebugSettingsReceived;

    RadarTriggerE m_radarTriggerSettingsDefault;
    RadarTriggerDelayE m_radarTriggerDelaySettingsDefault;
    RadarADControlE m_radarADSamplingFrequencySettingsDefault;
    RadarMiscZeroE m_radarMiscZeroSettingsDefault;
    RadarMiscOneE m_radarMiscOneSettingsDefault;
    RadarHlSetupE m_radarHlSetupSettingsDefault;
    RadarEnableE m_radarEnableSettingsDefault;
    RadarTuneCoarseE m_radarTuneCoarseSettingsDefault;
    RadarTuneFineE m_radarTuneFineSettingsDefault;
    RadarTuneRegE m_radarTuneRegSettingsDefault;
    RadarSetupE m_radarSetupSetiingsDefault;
    RadarGateTuneDelayE m_radarGateTuneDelaySettingsDefault;
    RadarAntSpeedE m_radarAntSpeedSettingsDefault;
    RadarMasterIPLowE m_radarMasterIPLowSettingsDefault;
    RadarDebugE m_radarDebugSettingsDefault;

public slots:
    void SlotStatusMessageRceived(const QByteArray &statusMessage);
    void SlotTxOff(const bool &off);
    void SlotBlindZoneIsActive(const bool &active);
    void SlotDataSavingState(const bool &savingState);

    void SlotSetTrasmitterOn(const bool &isTransmitting);
    void SlotSetAnttennaRotating(const bool &isRotating);

private slots:
    void timerStartCounterClicked();
    void timerStopCounterClicked();
    void timerCounterValue(const int &number);
    void powerUpIsComplete(const bool &powerUpStatus);

    void UISlotTriggerSetClicked();
    void UISlotTriggerDelaySetClicked();
    void UISlotADSamplingFrequencySetClicked();
    void UISlotMiscZeroSetClicked();
    void UISlotMiscOneSetClicked();
    void UISlotHlSetupSetClicked();
    void UISlotEnableSetClicked();
    void UISlotTuneCoatseSetClicked();
    void UISlotTuneFineSetClicked();
    void UISlotTuneRegSetClicked();
    void UISlotSetupSetClicked();
    void UISlotGateTuneDelaySetClicked();
    void UISlotAntSpeedSetClicked();
    void UISlotMasterIPLowSetClicked();
    void UISlotDebugSetClicked();
    void UISlotGetAllClicked();
    void UISlotSetAllClicked();
    void UISlotResetAllClicked();
    void UISlotDefaultSettingsClicked();

    void UISlotTriggerWidgetUpdate();
    void UISlotTriggerDelayUpdate();
    void UISlotADControlWidgetUpdate();
    void UISlotMiscZeroWidgetUpdate();
    void UISlotMiscOneWidgetUpdate();
    void UISlotHlSetupWidgetUpdate();
    void UISlotEnableWidgetUpdate();
    void UISlotTuneCoarseWidgetUpdate();
    void UISlotTuneFineWidgetUpdate();
    void UISlotTuneRegWidgetUpdate();
    void UISlotSetupWidgetUpdate();
    void UISlotGateTuneDelayWidgetUpdate();
    void UISlotAntSpeedWidgetUpdate();
    void UISlotMasterIPLowWidgetUpdate();
    void UISlotDebugWidgetUpdate();
    void UISlotRestartWorkingThread();

    void SlotAutoUpdate();
    void SlotStartAutoUpdate();
    void SlotStopAutoUpdate();

    void SlotStartTxClicked();
    void SlotStopTxClicked();
    void OpenSettingsClicked();

    void SlotDisconnect();
    void SlotNetworkError();
    void SlotPreDisconnection();

    void SlotGetAndSetDefaults();
    void SlotAllowSetup(const bool &allow);

    void SlotCheckStatus();
    void SlotMotorControlCheckStateChanged();
    void SlotExtStartTxClicked();
    void SlotRadarIsOffline();
    void UISlotExtSetPulseWidthStateChanged();
    void UISlotExtInitClicked();
    void UISlotExtTuneCoarseEditingFinished();
    void UISlotExtTuneFineEditingFinished();
    void UISlotExtPulseWidthChanged(const int &index);
    void UISlotExtSkipWarmUpTimeClicked();

    void SlotStatusMessageReceived(const QString &message);
    void SlotClearStatusMessage();

signals:
    void SignalstatusMessage(const QString &message);
    void SignalSetTuning(const quint8 &tuning);
    void SignalRadarIsWarmingUp(const bool &radarIsWarmingUp);
    void SignalLogEvent(const QString &logEvent);
};
#endif // RADARUDPCONTROLUI_H
