/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "radarmessageudp.h"

RadarMessageUdp::RadarMessageUdp(QObject *parent) : QObject(parent)
{

}

RadarMessageUdp::RadarMessageUdp(const RadarMessageUdp &radarMessageUdp, QObject *parent) : QObject(parent)
{
    m_address = radarMessageUdp.m_address;
    m_data = radarMessageUdp.m_data;
}

RadarMessageUdp RadarMessageUdp::operator=(const RadarMessageUdp &radarMessageUdp)
{
    return radarMessageUdp;
}

RadarMessageUdp::RadarMessageUdp(const RadarHeaderE &header, const RadarAddressE &address, QObject *parent) :
    QObject(parent), m_header{header}, m_address{address}
{
    m_data = 0;
}

RadarMessageUdp::RadarMessageUdp(const RadarHeaderE &header, const RadarAddressE &address, const unsigned char &data, QObject *parent) :
    QObject(parent), m_header{header}, m_address{address}, m_data{data}
{

}

RadarMessageUdp::RadarMessageUdp(const RadarHeaderE &header, const RadarAddressE &address, const QByteArray &dataArray, QObject *parent) :
    QObject(parent), m_header{header}, m_address{address}, m_dataArray{dataArray}
{

}

QByteArray RadarMessageUdp::message(const RadarHeaderE &header, const QByteArray &data)
{
    QByteArray message;
    message.append(static_cast<quint8>(header));
    message.append(static_cast<int>(data.size()/2) + 1);
    message.append(data);
    message.append(RadarAddressE(RadarAddressBytesE::Update).radarAddressE());
    message.append(static_cast<quint8>(69));
    return message;
}

QByteArray RadarMessageUdp::message(const RadarHeaderE &header, const unsigned char &address, const unsigned char &data)
{
    QByteArray message;
    message.append(static_cast<quint8>(header));
    message.append(2);
    message.append(address);
    message.append(data);
    message.append(RadarAddressE(RadarAddressBytesE::Update).radarAddressE());
    message.append(static_cast<quint8>(69));
    return message;
}


QByteArray RadarMessageUdp::message()
{
    return message(m_header, m_address.radarAddressE(), m_data);
}

QByteArray RadarMessageUdp::updateMessage()
{
    QByteArray msg;
    msg.append(static_cast<quint8>(RadarHeaderE::Inquire));
    msg.append(static_cast<quint8>(0));
    return msg;
}

QByteArray RadarMessageUdp::complexMessage(const QList<RadarAddressBytesE> &addressList, const QByteArray &messageList)
{
    if (addressList.size() != messageList.size()) {
        return updateMessage();
    }
    QByteArray complexMessage;
    for (int i = 0; i < addressList.size(); i++) {
        complexMessage.append(static_cast<quint8>(addressList.at(i)));
        complexMessage.append(messageList.at(i));
    }
    return message(RadarHeaderE::WriteReg, complexMessage);
}

