﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>

#include "greenboard-udp-sub-program/greenboardcontrolui.h"
#include "radar-udp-sub-program/radarudpcontrolui.h"
#include "radar-rs-sub-program/radarserialcontrolui.h"
#include "graphics-module/realtimeplot.h"
#include "shared-libs/inireader.h"
#include "schedule-worker/scheduleworkerui.h"
#include "shared-libs/applicationlogger.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(const QMap<QString, QString> &iniSettings = IniReader::defaultConfiguration(), QWidget *parent = nullptr);
    ~MainWindow();

private:

    ApplicationLogger *m_applicationLogger;

    QTabWidget *m_mainTabWidget;

    // Object for Green Board control
    GreenBoardControlUI *m_greenBoardContol;

    RadarUdpControlUI *m_radarEthernetControl;
    QWidget *m_radarEthernetControlInterface;
    // Object for Radar control via RS 422
    RadarSerialControlUI *m_radarSerialControl;

    bool m_serialPortControl = 0;
    QWidget *m_radarSerialControlInterface;
    // Main Graphics Object
    RealTimePlot *m_mainPlot;

    QAction *m_darkModeSwitch;
    QAction *m_saveIniFile;
    QAction *m_openIniFile;
    QAction *m_tuner;
    QAction *m_calibration;

    ScheduleWorkerUI *m_scheduleWorkerUI;

    QStatusBar *m_statusbar;

    QMap<QString, QString> m_iniSettings;

    void setIniSettings(QMap<QString, QString> &iniSettings);
    QMap<QString, QString> getIniSettings();

    QDialog *SavingIniSettingsWidget();

    QMessageBox *m_openIniSettingsMessageBox;
    QCheckBox *m_openDefaultsRememberChoiseChechBox;
    bool m_openDefaultsRememberChoise = false;
    bool m_openDefaultsChoise = false;

    bool m_calibrationMode = false;

    QDialog *m_savingIniSettingsDialog;
    QPushButton *m_savingIniSettingsAccept;
    QPushButton *m_savingIniSettingsReject;
    QCheckBox *m_checkAllBasicsCheckBox;
    QCheckBox *m_greenBoardSettingsCheckBox;
    QCheckBox *m_radarSettingsCheckBox;
    QCheckBox *m_signalPlotCheckBox;
    QCheckBox *m_circuleDiagramCheckBox;
    QCheckBox *m_tuningPlotCheckBox;
    QCheckBox *m_calibrationPlotCheckBox;

    QCheckBox *m_checkAllDefaultsCheckBox;
    QComboBox *m_defaultPlotTypeComboBox;
    QComboBox *m_darkModeComboBox;
    QComboBox *m_calibrationModeComboBox;
    QComboBox *m_radarControlInterfaceComboBox;
    QCheckBox *m_defaultPlotTypeCheckBox;
    QCheckBox *m_darkModeCheckBox;
    QCheckBox *m_calibrationModeCheckBox;
    QCheckBox *m_radarControlInterfaceCheckBox;
    QCheckBox *m_freeDiskSpaceWarningCheckBox;
    QSpinBox *m_freeDiskSpaceWarningSpinBox;
    QCheckBox *m_freeDiskSpaceStopSavingCheckBox;
    QSpinBox *m_freeDiskSpaceStopSavingSpinBox;
    QCheckBox *m_continueSavingAfterPathChangedCheckBox;
    QComboBox *m_continueSavingAfterPathChangedComboBox;
    QCheckBox *m_motorControlAutoStartCheckBox;
    QComboBox *m_motorControlAutoStartComboBox;

    QPushButton *m_extendedValuesPushButton;
    QSpinBox *m_samplingFrequencyComboBox;
    QSpinBox *m_signalLengthComboBox;
    QSpinBox *m_adcBitrRateComboBox;
    QSpinBox *m_adcMaxValueComboBox;
    QCheckBox *m_samplingFrequencyCheckBox;
    QCheckBox *m_signalLengthCheckBox;
    QCheckBox *m_adcBitrRateCheckBox;
    QCheckBox *m_adcMaxValueCheckBox;

    QLabel *m_diagramSettingsLabel;
    QCheckBox *m_diagramAngleBitOffsetCheckBox;
    QSpinBox *m_diagramAngleBitOffsetSpinBox;
    QCheckBox *m_diagramRangeAvgCoefficientCheckBox;
    QSpinBox *m_diagramRangeAvgCoefficientSpinBox;
    QCheckBox *m_diagramMaxPlottingFrequencyCheckBox;
    QSpinBox *m_diagramMaxPlottingFrequencySpinBox;

    QLabel *m_calibrationSettingsLabel;
    QCheckBox *m_calibrationAngleBitOffsetCheckBox;
    QSpinBox *m_calibrationAngleBitOffsetSpinBox;
    QCheckBox *m_calibrationRangeAvgCoefficientCheckBox;
    QSpinBox *m_calibrationRangeAvgCoefficientSpinBox;
    QCheckBox *m_calibrationMaxPlottingFrequencyCheckBox;
    QSpinBox *m_calibrationMaxPlottingFrequencySpinBox;

    QCheckBox *m_greenBoardNetworkSettings;
    QLineEdit *m_pcHostAddress;
    QSpinBox *m_pcControlPort;
    QSpinBox *m_pcDataPort;
    QLineEdit *m_boardHostAddress;
    QSpinBox *m_boardControlPort;
    QSpinBox *m_boardDataPort;

    QCheckBox *m_radarBoardNetworkSettings;
    QLineEdit *m_radarPcHostAddress;
    QSpinBox *m_radarPcControlPort;
    QSpinBox *m_radarPcDataPort;
    QLineEdit *m_radarBoardHostAddress;
    QSpinBox *m_radarBoardControlPort;
    QSpinBox *m_radarBoardDataPort;

    QLabel *m_radarPcHostAddressLabel;
    QLabel *m_radarPcControlPortLabel;
    QLabel *m_radarPcDataPortLabel;
    QLabel *m_radarBoardHostAddressLabel;
    QLabel *m_radarBoardControlPortLabel;
    QLabel *m_radarBoardDataPortLabel;

    QCheckBox *m_radarBoardSerialSettings;
    QLineEdit *m_portNameLineEdit;
    QComboBox *m_baudRateComboBox;
    QComboBox *m_dataBitsComboBox;
    QComboBox *m_directionComboBox;
    QComboBox *m_flowControlComboBox;
    QComboBox *m_parityComboBox;
    QComboBox *m_stopBitsComboBox;

    QLabel *m_portNameLabel;
    QLabel *m_baudRateLabel;
    QLabel *m_dataBitsLabel;
    QLabel *m_flowControlLabel;
    QLabel *m_directionLabel;
    QLabel *m_parityLabel;
    QLabel *m_stopBitsLabel;

    QCheckBox *m_setConfigAsDefaultCheckBox;

    QMessageBox *m_openingIniSettingsDialog;
    QCheckBox *m_openingIniSettingsAskAgain;

public slots:
    void darkMode(const bool &enable);
    void darkModeSwitchClicked();
    void statusBarMessage(const QString &statusMessage);
    void SlotResizeWindow();

    void slotSetScheduleName(const QString &name);

    void slotScheduleWorkerStarted(const bool &isStarted);

    void slotSetAntennaRotating(const bool &isRotating);
    void slotSetTransmitionOn(const bool &isTransmitting);
    void slotSetDataAcquisitionOn(const bool &isSaving);

    void slotSetCurrentScheduleSettings(const ScheduleLine &scheduleLine);

    void slotExtendedValuesPushButtonClicked();

    void SlotLogEvent(const QString &logEvent);

private slots:

    void slotRadarControlInterfaceChanged(const int &index);

    void slotBasicSettingsChanged();
    void slotCheckAllBasicSetiings();

    void slotDefaultSettingsChanged();
    void slotDefaultAllBasicSetiings();

    void slotSaveIniConfiguration();
    void slotLoadIniConfiguration();

    void closeEvent(QCloseEvent *event);

    bool closeWindow();

signals:

    void SignalLogEvent(const QString &logEvent);

};

#endif // MAINWINDOW_H
