/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef RADARSTRUCTSSERIAL_H
#define RADARSTRUCTSSERIAL_H

#include <QBitArray>
#include <QByteArray>
#include <QDataStream>
#include <QtEndian>

#include <math.h>

enum struct RadarHeaderDirectionRS
{
    HostToSub               =   0b10100,
    SubToHost               =   0b01010,
};

enum struct RadarHeaderOperationRS
{
    WriteToReg              =   0b110,
    ReadFromReg             =   0b010,
    WriteToRAM              =   0b100,
    ReadFromRAM             =   0b000
};

struct RadarHeaderRS
{
    RadarHeaderDirectionRS m_direction;
    RadarHeaderOperationRS m_operation;

    RadarHeaderRS()
    {
        m_direction = headerDirectionRS(0);
        m_operation = headerOperationRS(0);
    }

    RadarHeaderRS(const unsigned char &headerByteArray)
    {
        m_direction = headerDirectionRS(headerByteArray);
        m_operation = headerOperationRS(headerByteArray);
    }

    RadarHeaderRS(const RadarHeaderDirectionRS &direction, const RadarHeaderOperationRS &operation)
    {
        m_direction = direction;
        m_operation = operation;
    }

    inline unsigned char radarHeaderRS()
    {
        return radarHeaderRS(m_direction, m_operation);
    }

    inline void setRadarHeaderDirection(const quint8 &number)
    {
        m_direction = num2radarHeaderDirection(number);
    }

    inline void setRadarHeaderOperation(const quint8 &number)
    {
        m_operation = num2radarGeaderOperation(number);
    }

    inline QString headerDirection()
    {
        return headerDirection(m_direction);
    }

    inline QString headerOperarion()
    {
        return headerOperarion(m_operation);
    }

    inline static RadarHeaderDirectionRS headerDirectionRS(const unsigned char &headerByteArray)
    {
        return static_cast<RadarHeaderDirectionRS>(((static_cast<quint8>((headerByteArray)) >> 3) & 0b11111));
    }

    inline static RadarHeaderOperationRS headerOperationRS(const unsigned char &headerByteArray)
    {
        return static_cast<RadarHeaderOperationRS>((static_cast<quint8>((headerByteArray)) & 0b111));
    }

    inline static unsigned char radarHeaderRS(const RadarHeaderDirectionRS &radarHeaderDirection,
                                            const RadarHeaderOperationRS &radarHeaderOperation)
    {
        return (static_cast<quint8>(radarHeaderDirection) << 3) | (static_cast<quint8>(radarHeaderOperation));
    }

    inline static QString headerDirection(const RadarHeaderDirectionRS &radarDirection)
    {
        switch (radarDirection) {
        case RadarHeaderDirectionRS::HostToSub    :   return QString("HostToSub");
        case RadarHeaderDirectionRS::SubToHost    :   return QString("SubToHost");
        default                                 :   return QString("Undfined");
        }
    }

    inline static QString headerOperarion(const RadarHeaderOperationRS &radarOperation)
    {
        switch (radarOperation) {
        case RadarHeaderOperationRS::WriteToReg   :   return QString("WriteToReg");
        case RadarHeaderOperationRS::ReadFromReg  :   return QString("ReadFromReg");
        case RadarHeaderOperationRS::WriteToRAM   :   return QString("WriteToRAM");
        case RadarHeaderOperationRS::ReadFromRAM  :   return QString("ReadFromRAM");
        default                                 :   return QString("Undfined");
        }
    }

    inline static QStringList headerDirectionList()
    {
        QStringList directionList;
        directionList.append(QString("HostToSub"));
        directionList.append(QString("SubToHost"));
        directionList.append(QString("Undfined"));
        return directionList;
    }

    inline static QStringList headerOprerationList()
    {
        QStringList operationList;
        operationList.append(QString("WriteToReg"));
        operationList.append(QString("ReadFromReg"));
        operationList.append(QString("WriteToRAM"));
        operationList.append(QString("ReadFromRAM"));
        return operationList;
    }

    inline static RadarHeaderDirectionRS num2radarHeaderDirection(const quint8 &number)
    {
        switch (number) {
        case 0: return RadarHeaderDirectionRS::HostToSub;
        case 1: return RadarHeaderDirectionRS::SubToHost;
        default: return static_cast<RadarHeaderDirectionRS>(2);
        }
    }

    inline static RadarHeaderOperationRS num2radarGeaderOperation(const quint8 &number)
    {
        switch (number) {
        case 0: return RadarHeaderOperationRS::WriteToReg;
        case 1: return RadarHeaderOperationRS::ReadFromReg;
        case 2: return RadarHeaderOperationRS::WriteToRAM;
        case 3: return RadarHeaderOperationRS::ReadFromRAM;
        default: return static_cast<RadarHeaderOperationRS>(1);
        }
    }
};

enum struct RadarAddressBytesRS
{
    Trigger_0               =   0x0,
    Trigger_1               =   0x1,
    Trigger_2               =   0x2,

    Misc_0                  =   0x6,
    Misc_1                  =   0x7,

    ARPSetup_0              =   0x8,
    ARPSetup_1              =   0x9,

    Enable                  =   0xA,

    TuneCoarse              =   0xB,
    TuneFine                =   0xC,
    TuneReg                 =   0xD,

    Setup                   =   0xF,

    AutoTuneFine            =   0x11,
    AutotuneReg             =   0x12,

    HWReg                   =   0x1C,

    MAGIndicator            =   0x1D,
    TuneIndicator           =   0x1E,

    Status                  =   0x1F,

    WarmupTimer             =   0x20,

    Version                 =   0x22,

    Update                  =   0xFF
};

struct RadarAddressRS
{
    RadarAddressBytesRS m_address;

    RadarAddressRS()
    {
        m_address = addressBytesRS(0);
    }

    RadarAddressRS(const unsigned char &address)
    {
        m_address = addressBytesRS(address);
    }

    RadarAddressRS(const RadarAddressBytesRS &address)
    {
        m_address = address;
    }

    inline unsigned char radarAddressRS()
    {
        return radarAddressRS(m_address);
    }

    inline void setRadarAddress(const quint8 &number)
    {
        m_address = num2radarAddress(number);
    }

    inline QString address()
    {
        return address(m_address);
    }

    inline static RadarAddressBytesRS addressBytesRS(const unsigned char &addressByteArray)
    {
        return static_cast<RadarAddressBytesRS>(static_cast<quint8>((addressByteArray)));
    }

    inline static unsigned char radarAddressRS(const RadarAddressBytesRS &radarAddress)
    {
        return (static_cast<quint8>(radarAddress));
    }

    inline static QString address(RadarAddressBytesRS radarAddress)
    {
        switch (radarAddress) {
        case RadarAddressBytesRS::Trigger_0       :   return QString("Trigger_0");
        case RadarAddressBytesRS::Trigger_1       :   return QString("Trigger_1");
        case RadarAddressBytesRS::Trigger_2       :   return QString("Trigger_2");
        case RadarAddressBytesRS::Misc_0          :   return QString("Misc_0");
        case RadarAddressBytesRS::Misc_1          :   return QString("Misc_1");
        case RadarAddressBytesRS::ARPSetup_0      :   return QString("ARPSetup_0");
        case RadarAddressBytesRS::ARPSetup_1      :   return QString("ARPSetup_1");
        case RadarAddressBytesRS::Enable          :   return QString("Enable");
        case RadarAddressBytesRS::TuneCoarse      :   return QString("TuneCoarse");
        case RadarAddressBytesRS::TuneFine        :   return QString("TuneFine");
        case RadarAddressBytesRS::TuneReg         :   return QString("TuneReg");
        case RadarAddressBytesRS::Setup           :   return QString("Setup");
        case RadarAddressBytesRS::MAGIndicator    :   return QString("MAGIndicator");
        case RadarAddressBytesRS::TuneIndicator   :   return QString("TuneIndicator");
        case RadarAddressBytesRS::Status          :   return QString("Status");
        case RadarAddressBytesRS::WarmupTimer     :   return QString("WarmupTimer");
        case RadarAddressBytesRS::Version         :   return QString("Version");
        case RadarAddressBytesRS::Update          :   return QString("Update");
        default                                 :   return QString("Undefined");
        }
    }

    inline static QStringList addressList()
    {
        QStringList addressList;
        addressList.append(QString("Trigger_0"));
        addressList.append(QString("Trigger_1"));
        addressList.append(QString("Trigger_2"));
        addressList.append(QString("Misc_0"));
        addressList.append(QString("Misc_1"));
        addressList.append(QString("ARPSetup_0"));
        addressList.append(QString("ARPSetup_1"));
        addressList.append(QString("Enable"));
        addressList.append(QString("TuneCoarse"));
        addressList.append(QString("TuneFine"));
        addressList.append(QString("TuneReg"));
        addressList.append(QString("Setup"));
        addressList.append(QString("MAGIndicator"));
        addressList.append(QString("TuneIndicator"));
        addressList.append(QString("Status"));
        addressList.append(QString("WarmupTimer"));
        addressList.append(QString("Version"));
        addressList.append(QString("Update"));
        addressList.append(QString("Undefined"));
        return addressList;
    }

    inline static RadarAddressBytesRS num2radarAddress(const quint8 &number)
    {
        switch (number) {
        case 0: return RadarAddressBytesRS::Trigger_0;
        case 1: return RadarAddressBytesRS::Trigger_1;
        case 2: return RadarAddressBytesRS::Trigger_2;
        case 3: return RadarAddressBytesRS::Misc_0;
        case 4: return RadarAddressBytesRS::Misc_1;
        case 5: return RadarAddressBytesRS::ARPSetup_0;
        case 6: return RadarAddressBytesRS::ARPSetup_1;
        case 7: return RadarAddressBytesRS::Enable;
        case 8: return RadarAddressBytesRS::TuneCoarse;
        case 9: return RadarAddressBytesRS::TuneFine;
        case 10: return RadarAddressBytesRS::TuneReg;
        case 11: return RadarAddressBytesRS::Setup;
        case 12: return RadarAddressBytesRS::MAGIndicator;
        case 13: return RadarAddressBytesRS::TuneIndicator;
        case 14: return RadarAddressBytesRS::Status;
        case 15: return RadarAddressBytesRS::WarmupTimer;
        case 16: return RadarAddressBytesRS::Version;
        case 17: return RadarAddressBytesRS::Update;
        default: return static_cast<RadarAddressBytesRS>(0x23);
        }
    }
};

enum struct RadarTriggerGeneratorStateRS
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarTriggerPeriodRS
{
    PRF3200Hz               =   0x03D08,
    PRF1600Hz               =   0x07A11,
    PRF800Hz                =   0x0D423,
    PRF500Hz                =   0x1869F
};

struct RadarTriggerRS
{
    RadarTriggerGeneratorStateRS m_triggerGeneratorState;
    quint8 m_staggerValue;
    RadarTriggerPeriodRS m_triggerPeriod;

    RadarTriggerRS()
    {
        m_triggerGeneratorState = RadarTriggerGeneratorStateRS::Disabled;
        m_staggerValue = 0;
        m_triggerPeriod = RadarTriggerPeriodRS::PRF500Hz;
    }
    RadarTriggerRS(const RadarTriggerRS &radarTriggerRS)
    {
        m_triggerGeneratorState = radarTriggerRS.m_triggerGeneratorState;
        m_staggerValue = radarTriggerRS.m_staggerValue;
        m_triggerPeriod = radarTriggerRS.m_triggerPeriod;
    }
    RadarTriggerRS(const QByteArray &triggerByteArray)
    {
        m_triggerGeneratorState = triggerGeneratorStateRS(triggerByteArray);
        m_staggerValue = staggerValueRS(triggerByteArray);
        m_triggerPeriod = triggerPeriodRS(triggerByteArray);
    }
    RadarTriggerRS(const QList<QByteArray> &triggerMessages)
    {
        QByteArray triggerByteArray;
        triggerByteArray.append(triggerMessages[0][2]);
        triggerByteArray.append(triggerMessages[1][2]);
        triggerByteArray.append(triggerMessages[2][2]);
        m_triggerGeneratorState = triggerGeneratorStateRS(triggerByteArray);
        m_staggerValue = staggerValueRS(triggerByteArray);
        m_triggerPeriod = triggerPeriodRS(triggerByteArray);
    }
    RadarTriggerRS operator=(const RadarTriggerRS &radarTriggerRS)
    {
        m_triggerGeneratorState = radarTriggerRS.m_triggerGeneratorState;
        m_staggerValue = radarTriggerRS.m_staggerValue;
        m_triggerPeriod = radarTriggerRS.m_triggerPeriod;
        return *this;
    }

    inline QByteArray radarTriggers()
    {
        return radarTriggers(m_triggerGeneratorState, m_staggerValue, m_triggerPeriod);
    }

    inline static RadarTriggerGeneratorStateRS triggerGeneratorStateRS(const QByteArray &triggerByteArray)
    {
        return static_cast<RadarTriggerGeneratorStateRS>(((static_cast<quint8>(triggerByteArray[2]) >> 7) & 0b1));
    }

    inline static quint8 staggerValueRS(const QByteArray &triggerByteArray)
    {
        return ((static_cast<quint8>(triggerByteArray[2]) >> 3) & 0b1111);
    }

    inline static RadarTriggerPeriodRS triggerPeriodRS(const QByteArray &triggerByteArray)
    {
        return static_cast<RadarTriggerPeriodRS>(((static_cast<quint8>(triggerByteArray[2]) & 0b1) << 16) |
                                                (static_cast<quint8>(triggerByteArray[1]) << 8) |
                                                 static_cast<quint8>(triggerByteArray[0]));
    }

    inline static QByteArray radarTriggers(const RadarTriggerGeneratorStateRS &triggerGeneratorState,
                                         const quint8 &staggerValue, const RadarTriggerPeriodRS &triggerPeriod)
    {
        quint32 triggers{0};

        triggers = static_cast<quint32>(triggerGeneratorState);
        triggers = triggers << 4;
        triggers |= (static_cast<quint8>(staggerValue) & 0b1111);
        triggers = triggers << 19;
        triggers |= static_cast<quint32>(triggerPeriod);

        QByteArray radartriggers;
        radartriggers.append(triggers);
        radartriggers.append(triggers >> 8);
        radartriggers.append(triggers >> 16);

        return radartriggers;
    }

    inline quint8 getTriggerGenetatorState()
    {
        return static_cast<quint8>(m_triggerGeneratorState);
    }

    inline quint8 getTriggerStaggerValue()
    {
        return m_staggerValue;
    }

    inline quint8 getTriggerPeriod()
    {
        switch (m_triggerPeriod) {
        case RadarTriggerPeriodRS::PRF500Hz : {
            return 0;
        }
        case RadarTriggerPeriodRS::PRF800Hz : {
            return 1;
        }
        case RadarTriggerPeriodRS::PRF1600Hz : {
            return 2;
        }
        case RadarTriggerPeriodRS::PRF3200Hz : {
            return 3;
        }
        }
        return 0;
    }

    inline void setTriggerGenetatorState(const quint8 &generatorStateIndex)
    {
        switch (generatorStateIndex) {
        case 0: {
            m_triggerGeneratorState = RadarTriggerGeneratorStateRS::Disabled;
            break;
        }
        case 1: {
            m_triggerGeneratorState = RadarTriggerGeneratorStateRS::Enabled;
            break;
        }
        default:
            m_triggerGeneratorState = RadarTriggerGeneratorStateRS::Disabled;
        }
    }

    inline void setTriggerStaggerValue(const quint8 &staggerValue)
    {
        m_staggerValue = 0b1111 & staggerValue;
    }

    inline void setTriggerPeriod(const quint8 &triggerPeriodIndex)
    {
        switch (triggerPeriodIndex) {
        case  0: {
            m_triggerPeriod = RadarTriggerPeriodRS::PRF500Hz;
            break;
        }
        case  1: {
            m_triggerPeriod = RadarTriggerPeriodRS::PRF800Hz;
            break;
        }
        case  2: {
            m_triggerPeriod = RadarTriggerPeriodRS::PRF1600Hz;
            break;
        }
        case  3: {
            m_triggerPeriod = RadarTriggerPeriodRS::PRF3200Hz;
            break;
        }
        default: {
            m_triggerPeriod = RadarTriggerPeriodRS::PRF500Hz;
        }
        }
    }
};

enum struct RadarMiscZeroTestACPRS
{
    NormalOperation         =   0b0,
    AcpArpInternalGen       =   0b1
};

enum struct RadarMiscZeroAutoTuneFineRS
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarMiscZeroAutoTuneRegRS
{
    NormalOperation         =   0b0,
    AutomaticSearching      =   0b1
};

enum struct RadarMiscZeroDivideACPInputRS
{
    NoDivide                =   0b00,
    DivideByTwo             =   0b01,
    DivideByFour            =   0b10,
    DivideByEight           =   0b11
};

struct RadarMiscZeroRS
{
    RadarMiscZeroTestACPRS m_testACP;
    RadarMiscZeroAutoTuneFineRS m_autoTuneFine;
    RadarMiscZeroAutoTuneRegRS m_autoTuneReg;
    RadarMiscZeroDivideACPInputRS m_divideACPInput;

    RadarMiscZeroRS()
    {
        m_testACP = testACPRS(0);
        m_autoTuneFine = autoTuneFineRS(0);
        m_autoTuneReg = autoTuneRegRS(0);
        m_divideACPInput = divideACPInputRS(0);
    }

    RadarMiscZeroRS(const unsigned char &miscZeroByteArray)
    {
        m_testACP = testACPRS(miscZeroByteArray);
        m_autoTuneFine = autoTuneFineRS(miscZeroByteArray);
        m_autoTuneReg = autoTuneRegRS(miscZeroByteArray);
        m_divideACPInput = divideACPInputRS(miscZeroByteArray);
    }

    inline unsigned char radarMiscZero()
    {
        return radarMiscZero(m_testACP, m_autoTuneFine, m_autoTuneReg, m_divideACPInput);
    }

    inline static RadarMiscZeroTestACPRS testACPRS(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroTestACPRS>(((static_cast<quint8>(miscZeroByteArray) >> 5) & 0b1));
    }

    inline static RadarMiscZeroAutoTuneFineRS autoTuneFineRS(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroAutoTuneFineRS>(((static_cast<quint8>(miscZeroByteArray) >> 3) & 0b1));
    }

    inline static RadarMiscZeroAutoTuneRegRS autoTuneRegRS(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroAutoTuneRegRS>(((static_cast<quint8>(miscZeroByteArray) >> 2) & 0b1));
    }

    inline static RadarMiscZeroDivideACPInputRS divideACPInputRS(const unsigned char &miscZeroByteArray)
    {
        return static_cast<RadarMiscZeroDivideACPInputRS>((static_cast<quint8>(miscZeroByteArray)&0b11));
    }

    inline static unsigned char radarMiscZero(const RadarMiscZeroTestACPRS &testACP,
                                            const RadarMiscZeroAutoTuneFineRS &autoTuneFine,
                                            const RadarMiscZeroAutoTuneRegRS &autoTuneReg,
                                            const RadarMiscZeroDivideACPInputRS &divideACPInput)
    {
        return (static_cast<quint8>(testACP) << 5) |
               (static_cast<quint8>(autoTuneFine) << 3) |
               (static_cast<quint8>(autoTuneReg) << 2) |
               (static_cast<quint8>(divideACPInput));
    }

    inline void setAutotuneReg(const quint8 &autotuneRegIndex)
    {
        switch (autotuneRegIndex) {
        case 0: {
            m_autoTuneReg = RadarMiscZeroAutoTuneRegRS::NormalOperation;
            break;
        }
        case 1: {
            m_autoTuneReg = RadarMiscZeroAutoTuneRegRS::AutomaticSearching;
            break;
        }
        default: {
            m_autoTuneReg = RadarMiscZeroAutoTuneRegRS::NormalOperation;
        }
        }
    }

    inline void setTestACP(const quint8 &testACPIndex)
    {
        switch (testACPIndex) {
        case 0: {
            m_testACP = RadarMiscZeroTestACPRS::NormalOperation;
            break;
        }
        case 1: {
            m_testACP = RadarMiscZeroTestACPRS::AcpArpInternalGen;
            break;
        }
        default: {
            m_testACP = RadarMiscZeroTestACPRS::NormalOperation;
        }
        }
    }

    inline void setAutotuneFine(const quint8 &autoTuneFineIndex)
    {
        switch (autoTuneFineIndex) {
        case 0: {
            m_autoTuneFine = RadarMiscZeroAutoTuneFineRS::Disabled;
            break;
        }
        case 1: {
            m_autoTuneFine = RadarMiscZeroAutoTuneFineRS::Enabled;
            break;
        }
        default: {
            m_autoTuneFine = RadarMiscZeroAutoTuneFineRS::Disabled;
        }
        }
    }

    inline void setDivideACPInputSignal(const quint8 &devideACPInputSignalIndex)
    {
        switch (devideACPInputSignalIndex) {
        case 0: {
            m_divideACPInput = RadarMiscZeroDivideACPInputRS::NoDivide;
            break;
        }
        case 1: {
            m_divideACPInput = RadarMiscZeroDivideACPInputRS::DivideByTwo;
            break;
        }
        case 2: {
            m_divideACPInput = RadarMiscZeroDivideACPInputRS::DivideByFour;
            break;
        }
        case 3: {
            m_divideACPInput = RadarMiscZeroDivideACPInputRS::DivideByEight;
            break;
        }
        default: {
            m_divideACPInput = RadarMiscZeroDivideACPInputRS::NoDivide;
        }
        }
    }

    inline quint8 getTestACP()
    {
        switch (m_testACP) {
        case RadarMiscZeroTestACPRS::NormalOperation : {
            return 0;
        }
        case RadarMiscZeroTestACPRS::AcpArpInternalGen : {
            return 1;
        }
        }
    }

    inline quint8 getAutotuneFine()
    {
        switch (m_autoTuneFine) {
        case RadarMiscZeroAutoTuneFineRS::Disabled : {
            return 0;
        }
        case RadarMiscZeroAutoTuneFineRS::Enabled : {
            return 1;
        }
        }
    }

    inline quint8 getAutotuneReg()
    {
        switch (m_autoTuneReg) {
        case RadarMiscZeroAutoTuneRegRS::NormalOperation : {
            return 0;
        }
        case RadarMiscZeroAutoTuneRegRS::AutomaticSearching : {
            return 1;
        }
        }
    }

    inline quint8 getDivideACPInputSignal()
    {
        switch (m_divideACPInput) {
        case RadarMiscZeroDivideACPInputRS::NoDivide : {
            return 0;
        }
        case RadarMiscZeroDivideACPInputRS::DivideByTwo : {
            return 1;
        }
        case RadarMiscZeroDivideACPInputRS::DivideByFour : {
            return 2;
        }
        case RadarMiscZeroDivideACPInputRS::DivideByEight : {
            return 3;
        }
        }
    }

};

enum struct RadarMiscOneAcpPeriodRS
{
    ACP2048Pulses           =   0b001,
    ACP4096Pulses           =   0b010
};

enum struct RadarMiscOneBlankingRamRS
{
    BlockFrom0to511         =   0b0,
    BlockFrom512to1023      =   0b1
};

enum struct RadarMiscOneBandwithRS
{
    WideBand                =   0b0,
    NarrowBand              =   0b1
};

struct RadarMiscOneRS
{
    RadarMiscOneAcpPeriodRS m_acpPeriod;
    RadarMiscOneBlankingRamRS m_blankingRam;
    RadarMiscOneBandwithRS m_bandwith;

    RadarMiscOneRS()
    {
        m_acpPeriod = acpPeriodRS(0);
        m_blankingRam = blankingRamRS(0);
        m_bandwith = bandwithRS(0);
    }

    RadarMiscOneRS(const unsigned char &miscOneByteArray)
    {
        m_acpPeriod = acpPeriodRS(miscOneByteArray);
        m_blankingRam = blankingRamRS(miscOneByteArray);
        m_bandwith = bandwithRS(miscOneByteArray);
    }

    inline unsigned char radarMiscOne()
    {
        return radarMiscOne(m_acpPeriod, m_blankingRam, m_bandwith);
    }

    inline static RadarMiscOneAcpPeriodRS acpPeriodRS(const unsigned char &miscOneByteArray)
    {
        return static_cast<RadarMiscOneAcpPeriodRS>(((static_cast<quint8>(miscOneByteArray) >> 3) & 0b111));
    }

    inline static RadarMiscOneBlankingRamRS blankingRamRS(const unsigned char &miscOneByteArray)
    {
        return static_cast<RadarMiscOneBlankingRamRS>(((static_cast<quint8>(miscOneByteArray) >> 2) & 0b1));
    }

    inline static RadarMiscOneBandwithRS bandwithRS(const unsigned char &miscOneByteArray)
    {
        return static_cast<RadarMiscOneBandwithRS>((static_cast<quint8>(miscOneByteArray) & 0b1));
    }

    inline static unsigned char radarMiscOne(const RadarMiscOneAcpPeriodRS &acpPeriod,
                                           const RadarMiscOneBlankingRamRS &blankingRam,
                                           const RadarMiscOneBandwithRS &bandwidth)
    {
        return (static_cast<quint8>(acpPeriod) << 3) |
               (static_cast<quint8>(blankingRam) << 2) |
               (static_cast<quint8>(bandwidth));
    }

    inline void setAcpPeriod(const quint8 &acpPeriodIndex)
    {
        switch (acpPeriodIndex) {
        case 0: {
            m_acpPeriod = RadarMiscOneAcpPeriodRS::ACP2048Pulses;
            break;
        }
        case 1: {
            m_acpPeriod = RadarMiscOneAcpPeriodRS::ACP4096Pulses;
            break;
        }
        default: {
            m_acpPeriod = RadarMiscOneAcpPeriodRS::ACP2048Pulses;
        }
        }
    }

    inline void setBlankingRam(const quint8 &blankingRamIndex)
    {
        switch (blankingRamIndex) {
        case 0: {
            m_blankingRam = RadarMiscOneBlankingRamRS::BlockFrom0to511;
            break;
        }
        case 1: {
            m_blankingRam = RadarMiscOneBlankingRamRS::BlockFrom512to1023;
            break;
        }
        default: {
            m_blankingRam = RadarMiscOneBlankingRamRS::BlockFrom0to511;
        }
        }
    }

    inline void setBandwith(const quint8 &bandwithIndex)
    {
        switch (bandwithIndex) {
        case 0: {
            m_bandwith = RadarMiscOneBandwithRS::WideBand;
            break;
        }
        case 1: {
            m_bandwith = RadarMiscOneBandwithRS::NarrowBand;
            break;
        }
        default: {
            m_bandwith = RadarMiscOneBandwithRS::WideBand;
        }
        }
    }

    inline quint8 getAcpPeriod()
    {
        switch (m_acpPeriod) {
        case RadarMiscOneAcpPeriodRS::ACP2048Pulses : {
            return 0;
        }
        case RadarMiscOneAcpPeriodRS::ACP4096Pulses : {
            return 1;
        }
        default: {
            return 0;
        }
        }
    }

    inline quint8 getBlankingRam()
    {
        switch (m_blankingRam) {
        case RadarMiscOneBlankingRamRS::BlockFrom0to511 : {
            return 0;
        }
        case RadarMiscOneBlankingRamRS::BlockFrom512to1023 : {
            return 1;
        }
        }
    }

    inline quint8 getBandwith()
    {
        switch (m_bandwith) {
        case RadarMiscOneBandwithRS::WideBand : {
            return 0;
        }
        case RadarMiscOneBandwithRS::NarrowBand : {
            return 1;
        }
        }
    }
};

enum struct RadarHlSetupWidthRS
{
    AcpHalfPulse            =   0b00,
    AcpOnePulse             =   0b01,
    AcpTwoPulses            =   0b10,
    AcpFourPulses           =   0b11
};

enum struct RadarHlSetupInvertRS
{
    SamePolarity            =   0b0,
    InversePolarity         =   0b1
};

enum struct RadarHlSetupEdgeRS
{
    FallingEdge             =   0b0,
    RisingEdge              =   0b1
};

struct RadarHlSetupRS
{
    RadarHlSetupWidthRS m_widgth;
    RadarHlSetupInvertRS m_invert;
    RadarHlSetupEdgeRS m_edge;
    quint16 m_delayValue;

    RadarHlSetupRS()
    {
        m_widgth = RadarHlSetupWidthRS(0);
        m_invert = RadarHlSetupInvertRS(0);
        m_edge = RadarHlSetupEdgeRS(0);
        m_delayValue = 0;
    }

    RadarHlSetupRS(const QByteArray &hlSetupByteArray)
    {
        m_widgth = widgthRS(hlSetupByteArray);
        m_invert = invertRS(hlSetupByteArray);
        m_edge = edgeRS(hlSetupByteArray);
        m_delayValue = delayValueRS(hlSetupByteArray);
    }

    inline QByteArray radarHlSetup()
    {
        return radarHlSetup(m_widgth, m_invert, m_edge, m_delayValue);
    }

    inline unsigned char radarHlSetupTwo()
    {
        return radarHlSetupTwo(m_widgth, m_invert, m_edge, m_delayValue);
    }

    inline unsigned char radarHlSetupOne()
    {
        return radarHlSetupOne(m_delayValue);
    }

    inline static RadarHlSetupWidthRS widgthRS(const QByteArray &hlSetupByteArray)
    {
        return static_cast<RadarHlSetupWidthRS>(((static_cast<quint8>(hlSetupByteArray[1]) >> 6) & 0b11));
    }

    inline static RadarHlSetupInvertRS invertRS(const QByteArray &hlSetupByteArray)
    {
        return static_cast<RadarHlSetupInvertRS>(((static_cast<quint8>(hlSetupByteArray[1]) >> 5) & 0b1));
    }

    inline static RadarHlSetupEdgeRS edgeRS(const QByteArray &hlSetupByteArray)
    {
        return static_cast<RadarHlSetupEdgeRS>(((static_cast<quint8>(hlSetupByteArray[1]) >> 4) & 0b1));
    }

    inline static quint16 delayValueRS(const QByteArray &hlSetupByteArray)
    {
        return static_cast<quint16>(((static_cast<quint8>(hlSetupByteArray[1]) & 0b1111) << 8) |
                                    (static_cast<quint8>(hlSetupByteArray[0])));
    }

    inline static QByteArray radarHlSetup(const RadarHlSetupWidthRS &widgth,
                                        const RadarHlSetupInvertRS &invert,
                                        const RadarHlSetupEdgeRS &edge,
                                        const quint16 &delayValue)
    {
        QByteArray hlSetup;
        hlSetup += static_cast<quint8>(delayValue);
        hlSetup += (static_cast<quint8>(widgth) << 6) | (static_cast<quint8>(invert) << 5) |
                   (static_cast<quint8>(edge) << 4) | (0b1111&(static_cast<quint8>(delayValue >> 8)));
        return hlSetup;
    }

    inline static unsigned char radarHlSetupTwo(const RadarHlSetupWidthRS &widgth,
                                              const RadarHlSetupInvertRS &invert,
                                              const RadarHlSetupEdgeRS &edge,
                                              const quint16 &delayValue)
    {
        return static_cast<unsigned char>((static_cast<quint8>(widgth) << 6) | (static_cast<quint8>(invert) << 5) |
                                          (static_cast<quint8>(edge) << 4) | (0b1111&(static_cast<quint8>(delayValue >> 8))));
    }

    inline static unsigned char radarHlSetupOne(const quint16 &delayValue)
    {
        return static_cast<unsigned char>(static_cast<quint8>(delayValue));
    }

    inline void setWidth(const quint8 &widgthIndex)
    {
        switch (widgthIndex) {
        case 0: {
            m_widgth = RadarHlSetupWidthRS::AcpHalfPulse;
            break;
        }
        case 1: {
            m_widgth = RadarHlSetupWidthRS::AcpOnePulse;
            break;
        }
        case 2: {
            m_widgth = RadarHlSetupWidthRS::AcpTwoPulses;
            break;
        }
        case 3: {
            m_widgth = RadarHlSetupWidthRS::AcpFourPulses;
            break;
        }
        default: {
            m_widgth = RadarHlSetupWidthRS::AcpHalfPulse;
        }
        }
    }

    inline void setInvert(const quint8 &invertIndex)
    {
        switch (invertIndex) {
        case 0: {
            m_invert = RadarHlSetupInvertRS::SamePolarity;
            break;
        }
        case 1: {
            m_invert = RadarHlSetupInvertRS::InversePolarity;
            break;
        }
        default: {
            m_invert = RadarHlSetupInvertRS::SamePolarity;
        }
        }
    }

    inline void setEdge(const quint8 &edgeIndex)
    {
        switch (edgeIndex) {
        case 0: {
            m_edge = RadarHlSetupEdgeRS::FallingEdge;
            break;
        }
        case 1: {
            m_edge = RadarHlSetupEdgeRS::RisingEdge;
            break;
        }
        default: {
            m_edge = RadarHlSetupEdgeRS::FallingEdge;
        }
        }
    }

    inline void setDelayValue(const quint16 &delayValue)
    {
        m_delayValue = delayValue;
    }

    inline quint8 getWidth()
    {
        switch (m_widgth) {
        case RadarHlSetupWidthRS::AcpHalfPulse : {
            return 0;
        }
        case RadarHlSetupWidthRS::AcpOnePulse : {
            return 1;
        }
        case RadarHlSetupWidthRS::AcpTwoPulses : {
            return 2;
        }
        case RadarHlSetupWidthRS::AcpFourPulses : {
            return 3;
        }
        }
    }

    inline quint8 getInvert()
    {
        switch (m_invert) {
        case RadarHlSetupInvertRS::SamePolarity : {
            return 0;
        }
        case RadarHlSetupInvertRS::InversePolarity : {
            return 1;
        }
        }
    }

    inline quint8 getEdge()
    {
        switch (m_edge) {
        case RadarHlSetupEdgeRS::FallingEdge : {
            return 0;
        }
        case RadarHlSetupEdgeRS::RisingEdge : {
            return 1;
        }
        }
    }

    inline quint16 getDelayValue()
    {
        return m_delayValue;
    }
};

enum struct RadarEnableAutoTuneRS
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarEnableDAConvertersRS
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

enum struct RadarEnableADConvertersRS
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

struct RadarEnableRS
{
    RadarEnableAutoTuneRS m_autoTune;
    RadarEnableDAConvertersRS m_dac;
    RadarEnableADConvertersRS m_adc;

    RadarEnableRS()
    {
        m_autoTune = autoTuneRS(0);
        m_dac = dacRS(0);
        m_adc = adcRS(0);
    }

    RadarEnableRS(const unsigned char &enableByteArray)
    {
        m_autoTune = autoTuneRS(enableByteArray);
        m_dac = dacRS(enableByteArray);
        m_adc = adcRS(enableByteArray);
    }

    inline unsigned char radarEnableRS()
    {
        return radarEnableRS(m_autoTune, m_dac, m_adc);
    }

    inline static RadarEnableAutoTuneRS autoTuneRS(const unsigned char &enableByteArray)
    {
        return static_cast<RadarEnableAutoTuneRS>(((static_cast<quint8>(enableByteArray) >> 2) & 0b1));
    }

    inline static RadarEnableDAConvertersRS dacRS(const unsigned char &enableByteArray)
    {
        return static_cast<RadarEnableDAConvertersRS>(((static_cast<quint8>(enableByteArray) >> 1) & 0b1));
    }

    inline static RadarEnableADConvertersRS adcRS(const unsigned char &enableByteArray)
    {
        return static_cast<RadarEnableADConvertersRS>((static_cast<quint8>(enableByteArray) & 0b1));
    }

    inline static unsigned char radarEnableRS(const RadarEnableAutoTuneRS &autoTune,
                                            const RadarEnableDAConvertersRS &dac,
                                            const RadarEnableADConvertersRS &adc)
    {
        return (static_cast<quint8>(autoTune) << 2) |
               (static_cast<quint8>(dac) << 1) |
               (static_cast<quint8>(adc));
    }

    inline void setAutoTune(const quint8 &autoTuneIndex)
    {
        switch (autoTuneIndex) {
        case 0: {
            m_autoTune = RadarEnableAutoTuneRS::Disabled;
            break;
        }
        case 1: {
            m_autoTune = RadarEnableAutoTuneRS::Enabled;
            break;
        }
        default: {
            m_autoTune = RadarEnableAutoTuneRS::Disabled;
        }
        }
    }

    inline void setDac(const quint8 &dacIndex)
    {
        switch (dacIndex) {
        case 0: {
            m_dac = RadarEnableDAConvertersRS::Disabled;
            break;
        }
        case 1: {
            m_dac = RadarEnableDAConvertersRS::Enabled;
            break;
        }
        default: {
            m_dac = RadarEnableDAConvertersRS::Disabled;
        }
        }
    }

    inline void setAdc(const quint8 &adcIndex)
    {
        switch (adcIndex) {
        case 0: {
            m_adc = RadarEnableADConvertersRS::Disabled;
            break;
        }
        case 1: {
            m_adc = RadarEnableADConvertersRS::Enabled;
            break;
        }
        default: {
            m_adc = RadarEnableADConvertersRS::Disabled;
        }
        }
    }

    inline quint8 getAutoTune()
    {
        switch (m_autoTune) {
        case RadarEnableAutoTuneRS::Disabled : {
            return 0;
        }
        case RadarEnableAutoTuneRS::Enabled : {
            return 1;
        }
        }
    }

    inline quint8 getDac()
    {
        switch (m_dac) {
        case RadarEnableDAConvertersRS::Disabled : {
            return 0;
        }
        case RadarEnableDAConvertersRS::Enabled : {
            return 1;
        }
        }
    }

    inline quint8 getAdc()
    {
        switch (m_adc) {
        case RadarEnableADConvertersRS::Disabled : {
            return 0;
        }
        case RadarEnableADConvertersRS::Enabled : {
            return 1;
        }
        }
    }
};

struct RadarTuneCoarseRS
{
    quint8 m_coarseValue;

    RadarTuneCoarseRS()
    {
        m_coarseValue = static_cast<quint8>(0);
    }

    RadarTuneCoarseRS(const unsigned char &coarseByteArray)
    {
        m_coarseValue = static_cast<quint8>(coarseByteArray);
    }

    inline unsigned char radarTuneCoarseRS()
    {
        return radarTuneCoarseRS(m_coarseValue);
    }

    inline static quint8 coarseRS(const unsigned char &coarseByteArray)
    {
        return static_cast<quint8>(coarseByteArray);
    }

    inline static unsigned char radarTuneCoarseRS(const quint8 &coarseValue)
    {
        return coarseValue;
    }

    inline void setCoarseValue(const quint8 &coarseValue)
    {
        m_coarseValue = coarseValue;
    }
};

struct RadarTuneFineRS
{
    quint8 m_fineValue;

    RadarTuneFineRS()
    {
        m_fineValue = static_cast<quint8>(0);
    }

    RadarTuneFineRS(const unsigned char &fineByteArray)
    {
        m_fineValue = static_cast<quint8>(fineByteArray);
    }

    inline unsigned char radarTuneFineRS()
    {
        return radarTuneFineRS(m_fineValue);
    }

    inline static quint8 fineRS(const unsigned char &fineByteArray)
    {
        return static_cast<quint8>(fineByteArray);
    }

    inline static unsigned char radarTuneFineRS(const quint8 &fineValue)
    {
        return fineValue;
    }

    inline void setFineValue(const quint8 &fineValue)
    {
        m_fineValue = fineValue;
    }

    inline quint8 getFineValue()
    {
        return m_fineValue;
    }
};

struct RadarTuneRegRS
{
    quint8 m_regValue;

    RadarTuneRegRS()
    {
        m_regValue = static_cast<quint8>(0);
    }

    RadarTuneRegRS(const unsigned char &regByteArray)
    {
        m_regValue = static_cast<quint8>(regByteArray);
    }

    inline unsigned char radarTuneRegRS()
    {
        return radarTuneRegRS(m_regValue);
    }

    inline static quint8 regRS(const unsigned char &regByteArray)
    {
        return static_cast<quint8>(regByteArray);
    }

    inline static unsigned char radarTuneRegRS(const quint8 &regValue)
    {
        return regValue;
    }

    inline void setegValue(const quint8 &regValue)
    {
        m_regValue = regValue;
    }

    inline quint8 getRegValue()
    {
        return m_regValue;
    }
};

enum struct RadarSetupTransceiverModeRS
{
    TransceiverStandBy      =   0b0,
    TransceiverTxMode       =   0b1
};

enum struct RadarSetupPulseModeRS
{
    ExtraLongPulse          =   0b00,
    LongPolse               =   0b01,
    MediumPulse             =   0b10,
    ShortPulse              =   0b11
};

enum struct RadarSetupEnableMotorRS
{
    Disabled                =   0b0,
    Enabled                 =   0b1
};

struct RadarSetupRS
{
    RadarSetupTransceiverModeRS m_transceiverMode;
    RadarSetupPulseModeRS m_pulseMode;
    RadarSetupEnableMotorRS m_enableMotor;

    RadarSetupRS()
    {
        m_transceiverMode = transceiverModeRS(0);
        m_pulseMode = pulseModeRS(0);
        m_enableMotor = enableMotorRS(0);
    }

    RadarSetupRS(const unsigned char &setupByteArray)
    {
        m_transceiverMode = transceiverModeRS(setupByteArray);
        m_pulseMode = pulseModeRS(setupByteArray);
        m_enableMotor = enableMotorRS(setupByteArray);
    }

    inline unsigned char radarSetupRS()
    {
        return radarSetupRS(m_transceiverMode, m_pulseMode, m_enableMotor);
    }

    inline static RadarSetupTransceiverModeRS transceiverModeRS(const unsigned char &setupByteArray)
    {
        return static_cast<RadarSetupTransceiverModeRS>(((static_cast<quint8>((setupByteArray)) >> 7) & 0b1));
    }
    inline static RadarSetupPulseModeRS pulseModeRS(const unsigned char &setupByteArray)
    {
        return static_cast<RadarSetupPulseModeRS>(((static_cast<quint8>((setupByteArray)) >> 5) & 0b11));
    }
    inline static RadarSetupEnableMotorRS enableMotorRS(const unsigned char &setupByteArray)
    {
        return static_cast<RadarSetupEnableMotorRS>(((static_cast<quint8>((setupByteArray)) >> 1) & 0b1));
    }

    inline static unsigned char radarSetupRS(const RadarSetupTransceiverModeRS &transceiverMode,
                                           const RadarSetupPulseModeRS &pulseMode,
                                           const RadarSetupEnableMotorRS &enableMotor)
    {
        return ((static_cast<quint8>(transceiverMode) << 7) |
               (static_cast<quint8>(pulseMode) << 5) |
               (static_cast<quint8>(enableMotor) << 1));
    }

    inline void setTransceiverMode(const quint8 &transceiverModeIndex)
    {
        switch (transceiverModeIndex) {
        case 0: {
            m_transceiverMode = RadarSetupTransceiverModeRS::TransceiverStandBy;
            break;
        }
        case 1: {
            m_transceiverMode = RadarSetupTransceiverModeRS::TransceiverTxMode;
            break;
        }
        default: {
            m_transceiverMode = RadarSetupTransceiverModeRS::TransceiverStandBy;
        }
        }
    }

    inline void setPulseMode(const quint8 &pulseModeIndex)
    {
        switch (pulseModeIndex) {
        case 0: {
            m_pulseMode = RadarSetupPulseModeRS::ExtraLongPulse;
            break;
        }
        case 1: {
            m_pulseMode = RadarSetupPulseModeRS::LongPolse;
            break;
        }
        case 2: {
            m_pulseMode = RadarSetupPulseModeRS::MediumPulse;
            break;
        }
        case 3: {
            m_pulseMode = RadarSetupPulseModeRS::ShortPulse;
            break;
        }
        default: {
            m_pulseMode = RadarSetupPulseModeRS::ExtraLongPulse;
        }
        }
    }

    inline void setEnableMotor(const quint8 &enableMotorIndex)
    {
        switch (enableMotorIndex) {
        case 0: {
            m_enableMotor = RadarSetupEnableMotorRS::Disabled;
            break;
        }
        case 1: {
            m_enableMotor = RadarSetupEnableMotorRS::Enabled;
            break;
        }
        default: {
            m_enableMotor = RadarSetupEnableMotorRS::Disabled;
        }
        }
    }

    inline quint8 getTransceiverMode()
    {
        switch (m_transceiverMode) {
        case RadarSetupTransceiverModeRS::TransceiverStandBy : {
            return 0;
        }
        case RadarSetupTransceiverModeRS::TransceiverTxMode : {
            return 1;
        }
        }
    }

    inline quint8 getPulseMode()
    {
        switch (m_pulseMode) {
        case RadarSetupPulseModeRS::ExtraLongPulse : {
            return 0;
        }
        case RadarSetupPulseModeRS::LongPolse : {
            return 1;
        }
        case RadarSetupPulseModeRS::MediumPulse : {
            return 2;
        }
        case RadarSetupPulseModeRS::ShortPulse : {
            return 3;
        }
        }
    }

    inline quint8 getEnableMotor()
    {
        switch (m_enableMotor) {
        case RadarSetupEnableMotorRS::Disabled : {
            return 0;
        }
        case RadarSetupEnableMotorRS::Enabled : {
            return 1;
        }
        }
    }
};

struct RadarAutoTuneFineRS
{
    quint8 m_fineAutoTuneValue;

    RadarAutoTuneFineRS();
    RadarAutoTuneFineRS(const unsigned char &fineAutoTuneByteArray)
    {
        m_fineAutoTuneValue = static_cast<quint8>(fineAutoTuneByteArray);
    }

    inline static quint8 regAutoTuneRS(const unsigned char &fineAutoTuneByteArray)
    {
        return static_cast<quint8>(fineAutoTuneByteArray);
    }

    inline quint8 getFineAutoTuneValue()
    {
        return m_fineAutoTuneValue;
    }
};

struct RadarAutoTuneRegRS
{
    quint8 m_regAutoTuneValue;

    RadarAutoTuneRegRS();
    RadarAutoTuneRegRS(const unsigned char &regAutoTuneByteArray)
    {
        m_regAutoTuneValue = static_cast<quint8>(regAutoTuneByteArray);
    }

    inline static quint8 regAutoTuneRS(const unsigned char &regAutoTuneByteArray)
    {
        return static_cast<quint8>(regAutoTuneByteArray);
    }

    inline quint8 getRegAutoTuneValue()
    {
        return m_regAutoTuneValue;
    }
};

struct RadarMagIndicatorRS
{
    quint8 m_magIndicatorValue;

    RadarMagIndicatorRS();
    RadarMagIndicatorRS(const unsigned char &magIndicatorByteArray)
    {
        m_magIndicatorValue = static_cast<quint8>(magIndicatorByteArray);
    }

    inline static quint8 regAutoTuneRS(const unsigned char &magIndicatorByteArray)
    {
        return static_cast<quint8>(magIndicatorByteArray);
    }

    inline quint8 getMagIndicatorValue()
    {
        return m_magIndicatorValue;
    }
};

struct RadarTuneIndicatorRS
{
    quint8 m_tuneIndicatorValue;

    RadarTuneIndicatorRS();
    RadarTuneIndicatorRS(const unsigned char &tuneIndicatorByteArray)
    {
        m_tuneIndicatorValue = static_cast<quint8>(tuneIndicatorByteArray);
    }

    inline static quint8 regAutoTuneRS(const unsigned char &tuneIndicatorByteArray)
    {
        return static_cast<quint8>(tuneIndicatorByteArray);
    }

    inline quint8 getTuneIndicatorValue()
    {
        return m_tuneIndicatorValue;
    }
};

enum struct RadarStatusWarmupCompleteRS
{
    WarmupMode              =   0b0,
    ReadyToTransmit         =   0b1
};

enum struct RadarStatusRegAutoTuneRS
{
    NotCompleted            =   0b0,
    Completed               =   0b1
};

enum struct RadarStatusFineAutoTuneRS
{
    NotCompleted            =   0b0,
    Completed               =   0b1
};

enum struct RadarStatusAcpFailRS
{
    AcpInputSignalOk        =   0b0,
    AcpInputSignalFail      =   0b1
};

enum struct RadarStatusHlFailRS
{
    HlInputSignalOk         =   0b0,
    HlInputSignalFail       =   0b1
};

enum struct RadarStatusTriggerFailRS
{
    TriggerInputSignalOk    =   0b0,
    TriggerInputSignalFail  =   0b1
};

struct RadarStatusRS
{
    RadarStatusWarmupCompleteRS m_warmupComplete;
    RadarStatusRegAutoTuneRS m_regAutiTune;
    RadarStatusFineAutoTuneRS m_fineAutoTune;
    RadarStatusAcpFailRS m_acpFail;
    RadarStatusHlFailRS m_hlFail;
    RadarStatusTriggerFailRS m_triggerFail;

    RadarStatusRS()
    {
        m_warmupComplete = warmupCompleteRS(0);
        m_regAutiTune = regAutoTuneRS(0);
        m_fineAutoTune = fineAutoTuneRS(0);
        m_acpFail = acpFailRS(0);
        m_hlFail = hlFailRS(0);
        m_triggerFail = triggerpFailRS(0);
    }

    RadarStatusRS(const unsigned char &radarStatusByteArray)
    {
        m_warmupComplete = warmupCompleteRS(radarStatusByteArray);
        m_regAutiTune = regAutoTuneRS(radarStatusByteArray);
        m_fineAutoTune = fineAutoTuneRS(radarStatusByteArray);
        m_acpFail = acpFailRS(radarStatusByteArray);
        m_hlFail = hlFailRS(radarStatusByteArray);
        m_triggerFail = triggerpFailRS(radarStatusByteArray);
    }

    inline static RadarStatusWarmupCompleteRS warmupCompleteRS(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusWarmupCompleteRS>(((static_cast<quint8>((radarStatusByteArray)) >> 7) & 0b1));
    }

    inline static RadarStatusRegAutoTuneRS regAutoTuneRS(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusRegAutoTuneRS>(((static_cast<quint8>((radarStatusByteArray)) >> 4) & 0b1));
    }

    inline static RadarStatusFineAutoTuneRS fineAutoTuneRS(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusFineAutoTuneRS>(((static_cast<quint8>((radarStatusByteArray)) >> 3) & 0b1));
    }

    inline static RadarStatusAcpFailRS acpFailRS(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusAcpFailRS>(((static_cast<quint8>((radarStatusByteArray)) >> 2) & 0b1));
    }

    inline static RadarStatusHlFailRS hlFailRS(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusHlFailRS>(((static_cast<quint8>((radarStatusByteArray)) >> 1) & 0b1));
    }

    inline static RadarStatusTriggerFailRS triggerpFailRS(const unsigned char &radarStatusByteArray)
    {
        return static_cast<RadarStatusTriggerFailRS>((static_cast<quint8>((radarStatusByteArray)) & 0b1));
    }

    inline quint8  getWarmupComplete()
    {
        switch (m_warmupComplete) {
        case RadarStatusWarmupCompleteRS::WarmupMode : {
            return 0;
        }
        case RadarStatusWarmupCompleteRS::ReadyToTransmit : {
            return 1;
        }
        }
    }

    inline quint8  getRegAutiTune()
    {
        switch (m_regAutiTune) {
        case RadarStatusRegAutoTuneRS::NotCompleted : {
            return 0;
        }
        case RadarStatusRegAutoTuneRS::Completed : {
            return 1;
        }
        }
    }

    inline quint8  getFineAutoTune()
    {
        switch (m_fineAutoTune) {
        case RadarStatusFineAutoTuneRS::NotCompleted : {
            return 0;
        }
        case RadarStatusFineAutoTuneRS::Completed : {
            return 1;
        }
        }
    }

    inline quint8  getAcpFail()
    {
        switch (m_acpFail) {
        case RadarStatusAcpFailRS::AcpInputSignalOk : {
            return 0;
        }
        case RadarStatusAcpFailRS::AcpInputSignalFail : {
            return 1;
        }
        }
    }

    inline quint8  getHlFail()
    {
        switch (m_hlFail) {
        case RadarStatusHlFailRS::HlInputSignalOk : {
            return 0;
        }
        case RadarStatusHlFailRS::HlInputSignalFail : {
            return 1;
        }
        }
    }

    inline quint8  getTriggerFail()
    {
        switch (m_triggerFail) {
        case RadarStatusTriggerFailRS::TriggerInputSignalOk : {
            return 0;
        }
        case RadarStatusTriggerFailRS::TriggerInputSignalFail : {
            return 1;
        }
        }
    }

};

struct RadarWarmupTimerRS
{
    quint8 m_warmupTimer;

    RadarWarmupTimerRS();
    RadarWarmupTimerRS(const unsigned char &warmupTimerByteArray)
    {
        m_warmupTimer = static_cast<quint8>((warmupTimerByteArray));
    }

    inline static quint8 warmupTimerRS(const unsigned char &warmupTimerByteArray)
    {
        return static_cast<quint8>((warmupTimerByteArray));
    }
};

struct RadarUpdateRS
{
    quint8 m_dummyValue = 69;

    RadarUpdateRS();

    inline unsigned char radarUpdateRS()
    {
        return radarUpdateRS(m_dummyValue);
    }

    inline static unsigned char radarUpdateRS(const quint8 &dummyValue)
    {
        return dummyValue;
    }
};

enum struct ErrorCodeHandlerRS
{
    NoError                 =   0,
    WrongHeader             =   -1,
    WrongChecksum           =   -2,
    WrongAddress            =   -3,
    WrongData               =   -4
};

#endif // RADARSTRUCTSSERIAL_H
