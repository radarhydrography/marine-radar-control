/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "serialportcommunicator.h"
#include <QDebug>

SerialPortCommunicator::SerialPortCommunicator(QObject *parent) :
    QThread(parent)
{
    m_mutex = new QMutex;
    m_serialPort = new QSerialPort;
}

SerialPortCommunicator::SerialPortCommunicator(const QString &portName, QObject *parent) :
    QThread(parent), m_portName{portName}
{
    m_mutex = new QMutex;
    m_serialPort = new QSerialPort;
}

SerialPortCommunicator::SerialPortCommunicator(const QString &portName, const SerialPortSettings &portSettings, QObject *parent) :
    QThread(parent), m_portName{portName}, m_portSettings{portSettings}
{
    m_mutex = new QMutex;
    m_serialPort = new QSerialPort;
}

SerialPortCommunicator::~SerialPortCommunicator()
{
    m_mutex->lock();
    m_quit = true;
    m_mutex->unlock();
    wait();
    delete m_serialPort;
}

void SerialPortCommunicator::startListen(const QString &portName, SerialPortSettings portSettings)
{
    const QMutexLocker locker(m_mutex);
    m_portName = portName;
    m_portSettings = portSettings;

    if (!isRunning()) {
        start();
    }
}

void SerialPortCommunicator::startListen()
{
    if (m_portName != "") {
        startListen(m_portName, m_portSettings);
    }
}

void SerialPortCommunicator::stopListen()
{
    m_mutex->lock();
    m_quit = true;
    m_mutex->unlock();
}

void SerialPortCommunicator::setPortParameters(const QString &portName, const SerialPortSettings &portSettings)
{
    m_portName = portName;
    m_portSettings = portSettings;
}

QByteArray SerialPortCommunicator::read()
{
    applyPortParameters();

        if (!m_serialPort->open(QIODevice::ReadOnly)) {
            emit error(tr("Can't open %1, error code %2")
                       .arg(m_portName).arg(m_serialPort->error()));
        }

        QByteArray readenData = m_serialPort->readAll();
        while (m_serialPort->waitForReadyRead(10)) {
            readenData += m_serialPort->readAll();
        }

    m_serialPort->close();

    emit error("");
    return readenData;
}

QByteArray SerialPortCommunicator::read(QSerialPort *serialPort)
{
    if (serialPort->open(QSerialPort::ReadOnly)) {

        QByteArray readenData = serialPort->readAll();
        while (serialPort->waitForReadyRead(50)) {
            readenData += serialPort->readAll();
        }

        serialPort->close();

        emit error("");
        return readenData;
    }

    emit error("SerialPort read error");
    return QByteArray("");
}

void SerialPortCommunicator::applyPortParameters()
{
    applyPortParamerets(m_serialPort);
}

void SerialPortCommunicator::applyPortParamerets(QSerialPort *serialPort)
{
    serialPort->setPortName(m_portName);

    serialPort->setBaudRate(m_portSettings.s_baudRate, m_portSettings.s_direction);
    serialPort->setDataBits(m_portSettings.s_dataBits);
    serialPort->setFlowControl(m_portSettings.s_flowControl);
    serialPort->setParity(m_portSettings.s_parity);
    serialPort->setStopBits(m_portSettings.s_stopBits);
}

bool SerialPortCommunicator::isRunning()
{
    m_mutex->lock();
    bool status = !m_quit;
    m_mutex->unlock();
    return status;
}

void SerialPortCommunicator::run()
{
    m_mutex->lock();

    QString currentPortName = m_portName;

    m_mutex->unlock();
    QSerialPort serial;

    serial.setBaudRate(m_portSettings.s_baudRate, m_portSettings.s_direction);
    serial.setDataBits(m_portSettings.s_dataBits);
    serial.setFlowControl(m_portSettings.s_flowControl);
    serial.setParity(m_portSettings.s_parity);
    serial.setStopBits(m_portSettings.s_stopBits);


    if (!m_quit) {
        emit poolStatus(true);
    }
    while (!m_quit) {
        serial.close();
        serial.setPortName(currentPortName);
        if (!serial.open(QIODevice::ReadOnly)) {
            emit error(tr("Can't open %1, error code %2")
                       .arg(m_portName).arg(serial.error()));
            m_mutex->lock();
            m_quit = true;
            m_mutex->unlock();
        }

        m_mutex->lock();

        // read request
        QByteArray readenData = serial.readAll();
        while (serial.waitForReadyRead(500)) {
            readenData += serial.readAll();
        }

        emit message(QString(readenData));
        emit rawMessage(readenData);

        m_mutex->unlock();

        msleep(10);
    }

    serial.close();
    if (m_quit) {
        emit poolStatus(false);
    }
    m_mutex->lock();
    m_quit = true;
    m_mutex->unlock();
}

void SerialPortCommunicator::handleBytesWritten(qint64 bytes)
{
    m_bytesWritten = bytes;
    if (m_bytesWritten == m_writeData.size()) {
        m_bytesWritten =0;
        emit handleWriteStatus(QString(QObject::tr("Data successfully sent to port %1")
                                                         .arg(m_serialPort->portName()) + ". Bytes written: " + QString::number(bytes)));
    }
}

void SerialPortCommunicator::handleError(QSerialPort::SerialPortError serialPortError)
{
    if (serialPortError == QSerialPort::WriteError) {
        emit handleWriteStatus(QString(QObject::tr("An I/O error occurred while writing"
                                                                     " the data to port %1, error: %2")
                                                         .arg(m_serialPort->portName(), m_serialPort->errorString())));
    }
}

void SerialPortCommunicator::write(const QByteArray &writeData)
{
    applyPortParameters();

    if (m_serialPort->open(QSerialPort::WriteOnly)) {

        m_writeData = writeData;

        const qint64 bytesWritten = m_serialPort->write(writeData);

        m_serialPort->waitForBytesWritten(50);

        if (bytesWritten == -1) {
            emit handleWriteStatus(QString(QObject::tr("Failed to write the data to port %1, error: %2")
                                           .arg(m_serialPort->portName(), m_serialPort->errorString())));
        } else if (bytesWritten != m_writeData.size()) {
            emit handleWriteStatus(QString(QObject::tr("Failed to write all the data to port %1, error: %2")
                                           .arg(m_serialPort->portName(), m_serialPort->errorString())));
        } else {
            handleBytesWritten(bytesWritten);
        }

        m_serialPort->close();

    }
}

QByteArray SerialPortCommunicator::readwrite(QSerialPort *serialPort, const QByteArray &message)
{
    if (serialPort->open(QSerialPort::ReadWrite)) {

        const qint64 bytesWritten = serialPort->write(message);

        serialPort->waitForBytesWritten(50);

        if (bytesWritten == -1) {
            emit error(QString(QObject::tr("Failed to write the data to port %1, error: %2")
                                           .arg(m_serialPort->portName(), m_serialPort->errorString())));
            return 0;
        } else if (bytesWritten != message.size()) {
            emit error(QString(QObject::tr("Failed to write all the data to port %1, error: %2")
                                           .arg(m_serialPort->portName(), m_serialPort->errorString())));
            return 0;
        }

        QByteArray readenData = serialPort->readAll();
        while (serialPort->waitForReadyRead(50)) {
            readenData += serialPort->readAll();
        }

        serialPort->close();

        emit error("");
        return readenData;
    }

    emit error("SerialPort read error");
    return QByteArray("");
}

QByteArray SerialPortCommunicator::readwrite(const QByteArray &message)
{
    applyPortParameters();

    if (m_serialPort->open(QSerialPort::ReadWrite)) {

        m_writeData = message;

        const qint64 bytesWritten = m_serialPort->write(message);

        m_serialPort->waitForBytesWritten(50);

        if (bytesWritten == -1) {
            emit error(QString(QObject::tr("Failed to write the data to port %1, error: %2")
                                           .arg(m_serialPort->portName(), m_serialPort->errorString())));
        } else if (bytesWritten != message.size()) {
            emit error(QString(QObject::tr("Failed to write all the data to port %1, error: %2")
                                           .arg(m_serialPort->portName(), m_serialPort->errorString())));
        } else {
            handleBytesWritten(bytesWritten);
        }

        QByteArray readenData = m_serialPort->readAll();
        while (m_serialPort->waitForReadyRead(50)) {
            readenData += m_serialPort->readAll();
        }

        m_serialPort->close();

        emit error("");
        return readenData;

    }

    emit error("SerialPort read error");
    return QByteArray("");
}


void SerialPortCommunicator::setPortParameters(QSerialPort *serialPort, const QString &portName, const SerialPortSettings &portSettings)
{
    serialPort->setPortName(portName);

    serialPort->setBaudRate(portSettings.s_baudRate, portSettings.s_direction);
    serialPort->setDataBits(portSettings.s_dataBits);
    serialPort->setFlowControl(portSettings.s_flowControl);
    serialPort->setParity(portSettings.s_parity);
    serialPort->setStopBits(portSettings.s_stopBits);
}
