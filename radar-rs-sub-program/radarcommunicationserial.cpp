/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "radarcommunicationserial.h"

#include <QList>
#include <QCoreApplication>
#include <QtCore>
#include "radarmessageserial.h"

RadarCommunicationSerial::RadarCommunicationSerial(QObject *parent):
    QThread(parent)
{
    m_mutex = new QMutex;

    m_serialPortCommunicator = new SerialPortCommunicator();
    m_message = new RadarMessageSerial;
    m_operationBuffer = new QList<RadarMessageSerial>;
    m_workingThreadEnable = false;

    connect(m_serialPortCommunicator, &SerialPortCommunicator::error, this, &RadarCommunicationSerial::SignalstatusMessage);
    connect(m_serialPortCommunicator, &SerialPortCommunicator::message, this, &RadarCommunicationSerial::SignalstatusMessage);
    connect(m_serialPortCommunicator, &SerialPortCommunicator::handleWriteStatus, this, &RadarCommunicationSerial::SignalstatusMessage);
}

RadarCommunicationSerial::~RadarCommunicationSerial()
{
    delete m_serialPortCommunicator;
    delete m_message;
    delete m_operationBuffer;
}

void RadarCommunicationSerial::setPortParameters(const QString &portName, const SerialPortSettings &portSettings)
{
    setPortName(portName);
    setPortSettings(portSettings);
}

void RadarCommunicationSerial::setPortName(const QString &portName)
{
    m_serialPortCommunicator->setPortName(portName);
}

void RadarCommunicationSerial::setPortSettings(const SerialPortSettings &portSettings)
{
    m_serialPortCommunicator->setPortSettings(portSettings);
}

void RadarCommunicationSerial::sendMessage(const QByteArray &message)
{
    m_serialPortCommunicator->write(message);
}

QByteArray RadarCommunicationSerial::receiveMessage()
{
    return m_serialPortCommunicator->read();
}

QByteArray RadarCommunicationSerial::sendReceiveMessage(const QByteArray &message)
{
    if (m_workingThreadEnable) {
        pauseWorkingThread();
    }
    QByteArray answer = m_serialPortCommunicator->readwrite(message);
    continueWorkingThread();
    return answer;
}

QList<QByteArray> RadarCommunicationSerial::sendReceiveMessage(const QList<QByteArray> &message)
{
    QList<QByteArray> answer;
    for (auto msgNum = 0; msgNum < message.length(); msgNum++) {
        QByteArray recanswer = sendReceiveMessage(message.at(msgNum));
        if (!recanswer.isEmpty()) {
            answer.append(recanswer);
        }
    }
    return answer;
}

void RadarCommunicationSerial::setRegister()
{
    setRegister(m_message->getAddress(), m_message->getData());
}

void RadarCommunicationSerial::setRegister(const RadarAddressRS &address, const unsigned char &data)
{
    RadarMessageSerial message(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::WriteToReg), address, data);
    this->sendReceiveMessage(message.message());
    this->sendReceiveMessage(RadarMessageSerial::updateMessage());
}

unsigned char RadarCommunicationSerial::getRegister()
{
    return getRegister(m_message->getAddress());
}

unsigned char RadarCommunicationSerial::getRegister(const RadarAddressRS &address)
{
    RadarMessageSerial message(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), address);
    QByteArray answer = sendReceiveMessage(message.message());
    if (!answer.isEmpty()) {
        return answer[2];
    } else {
        return 0;
    }
}

void RadarCommunicationSerial::setRegisters(const RadarAddressRS &address, const QByteArray &data)
{
    if (!data.isEmpty()) {
        QList<QByteArray> messageList;
        for (quint8 num = 0; num < data.size(); num++) {
            RadarMessageSerial message(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::WriteToReg), address, data[num]);
            messageList.append(message.message());
        }
    this->sendReceiveMessage(messageList);
    this->sendReceiveMessage(RadarMessageSerial::updateMessage());
    }
}

void RadarCommunicationSerial::setRegisters(const QList<QByteArray> &messageList)
{
    if (!messageList.isEmpty()) {
        this->sendReceiveMessage(messageList);
        this->sendReceiveMessage(RadarMessageSerial::updateMessage());
    }
}

QByteArray RadarCommunicationSerial::getRegisters(const QList<RadarAddressRS> &addressList)
{
    if (!addressList.isEmpty()) {
        QList<QByteArray> messageList;
        for (int num = 0; num < addressList.size(); num++) {
            messageList.append(RadarMessageSerial(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), addressList.at(num)).message());
        }
        return getRegisters(messageList);
    }
    return QByteArray();
}

QByteArray RadarCommunicationSerial::getRegisters(const QList<QByteArray> &messageList)
{
    if (!messageList.isEmpty()) {
        QList<QByteArray> answerList = this->sendReceiveMessage(messageList);
        QByteArray answer;
        if (answer.isEmpty()) {
            for (int num = 0; num < answerList.size(); num++) {
                answer.append(answerList.at(num).at(2));
            }
            return answer;
        }
    }
    return QByteArray();
}

void RadarCommunicationSerial::startWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = true;
    m_mutex->unlock();

    if (!isRunning()) {
        start();
    }
}

void RadarCommunicationSerial::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

void RadarCommunicationSerial::setOperaton(const RadarMessageSerial &message)
{
    m_mutex->lock();
    m_operationBuffer->append(message);
    m_mutex->unlock();
}

void RadarCommunicationSerial::pauseWorkingThread()
{
    m_mutex->lock();
    m_workingThreadPause = true;
    m_mutex->unlock();
    while (!m_readyToWrite) {
        msleep(10);
    }
}

void RadarCommunicationSerial::continueWorkingThread()
{
    m_mutex->lock();
    m_workingThreadPause = false;
    m_mutex->unlock();
}

void RadarCommunicationSerial::run()
{
    QSerialPort serialPort;
    SerialPortCommunicator::setPortParameters(&serialPort, m_serialPortCommunicator->getPortName(), m_serialPortCommunicator->getPortSettings());

    bool warmupComplete = false;

    RadarMessageSerial statusRequest(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), RadarAddressBytesRS::Status);

    if (m_workingThreadEnable) {
        emit poolStatus(true);
    }
    while (m_workingThreadEnable) {
        if (!m_workingThreadPause) {
            m_readyToWrite = false;
            if (!warmupComplete) {
                QByteArray warmupMessage = m_serialPortCommunicator->readwrite(&serialPort, RadarMessageSerial::warmupTimeRequest());
                if (!warmupMessage.isEmpty()) {
                    quint8 warmupTimeValue = static_cast<quint8>(RadarMessageSerial::warmapTimeValue(warmupMessage));
                    emit warmupTimer(warmupTimeValue);
                }
                QByteArray radarStatusMsg = m_serialPortCommunicator->readwrite(&serialPort, statusRequest.message());
                if (!radarStatusMsg.isEmpty()) {
                    RadarStatusRS status(radarStatusMsg[2]);
                    if (status.m_warmupComplete == RadarStatusWarmupCompleteRS::ReadyToTransmit) {
                        warmupComplete = true;
                        emit warmupIsComplete(warmupComplete);
                    } else {
                        warmupComplete = false;
                        emit warmupIsComplete(warmupComplete);
                    }
                }
                m_serialPortCommunicator->readwrite(&serialPort, m_message->updateMessage());
            } else {
                if (!m_operationBuffer->isEmpty()) {
                    for (int msgNum = 0; msgNum < m_operationBuffer->length(); msgNum++) {
                        RadarMessageSerial sendingMessage = m_operationBuffer->at(msgNum);
                        m_serialPortCommunicator->readwrite(&serialPort, sendingMessage.message());
                    }
                    m_serialPortCommunicator->readwrite(&serialPort, m_message->updateMessage());
                    m_operationBuffer->clear();
                } else {
                    m_serialPortCommunicator->readwrite(&serialPort, m_message->updateMessage());
                }
            }
        } else {
            m_readyToWrite = true;
        }
        msleep(100);
    }

    if (!m_workingThreadEnable) {
        emit poolStatus(false);
    }
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}
