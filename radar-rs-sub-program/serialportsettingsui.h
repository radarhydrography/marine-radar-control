/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SERIALPORTSETTINGSUI_H
#define SERIALPORTSETTINGSUI_H

#include <QWidget>
#include <QComboBox>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QTextEdit>
#include "structsserial.h"

class SerialPortSettingsUI : public QWidget
{
    Q_OBJECT

    QList<SerialPortParameters> m_portParameters;
    QList<SerialPortSettings> m_portSettings;
    QStringList m_portNames;

    QComboBox *m_portNameComboBox;
    QCheckBox *m_portNameCheckBox;
    QComboBox *m_baudRateComboBox;
    QCheckBox *m_baudRateCheckBox;
    QComboBox *m_dataBitsComboBox;
    QCheckBox *m_dataBitsCheckBox;
    QComboBox *m_directionComboBox;
    QCheckBox *m_directionCheckBox;
    QComboBox *m_flowControlComboBox;
    QCheckBox *m_flowControlCheckBox;
    QComboBox *m_parityComboBox;
    QCheckBox *m_parityCheckBox;
    QComboBox *m_stopBitsComboBox;
    QCheckBox *m_stopBitsCheckBox;

    QTextEdit *m_portParametersTextEdit;
    QPushButton *m_scanPushButton;
    QPushButton *m_setPushButton;
    QPushButton *m_resetPushButton;

    SerialPortSettings m_defaultSerialPortSettings;

public:
    explicit SerialPortSettingsUI(QWidget *parent = nullptr, SerialPortSettings defaultSerialPortSettings = SerialPortSettings());

private:

    void portNumerator();
    void showPortParameters(const int &index);

private slots:

    void SlotPortNameCheckBoxClicked();
    void SlotBaudRateCheckBoxClicked();
    void SlotDataBitsCheckBoxClicked();
    void SlotDirectionCheckBoxClicked();
    void SlotFlowControlCheckBoxClicked();
    void SlotParityCheckBoxClicked();
    void SlotStopBitsCheckBoxClicked();
    void SlotPortNameComboBoxIndexChanged(const int &index);

    void SlotScanPushButtonClicked();
    void SlotSetPushButtonClicked();
    void SlotResetPushButtonClicked();

signals:

    void SignalSetSerialPortParameters(const QString &serialPortName, const SerialPortSettings &serialPortSettings);

};

#endif // SERIALPORTSETTINGSUI_H
