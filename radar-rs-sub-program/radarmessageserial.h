/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef RADARMESSAGESERIAL_H
#define RADARMESSAGESERIAL_H

#include <QObject>
#include "radarstructsserial.h"

class RadarMessageSerial : public QObject
{
    Q_OBJECT

    RadarHeaderRS m_header;
    RadarAddressRS m_address;
    unsigned char m_data;
    QByteArray m_dataArray;

public:
    RadarMessageSerial(QObject *parent = nullptr);
    RadarMessageSerial(const RadarMessageSerial &radarMessageSerial, QObject *parent = nullptr);
    RadarMessageSerial operator=(const RadarMessageSerial &radarMessageSerial);
    RadarMessageSerial(const RadarHeaderRS &header, const RadarAddressRS &address, QObject *parent = nullptr);
    RadarMessageSerial(const RadarHeaderRS &header, const RadarAddressRS &address, const unsigned char &data, QObject *parent = nullptr);
    RadarMessageSerial(const RadarHeaderRS &header, const RadarAddressRS &address, const QByteArray &dataArray, QObject *parent = nullptr);

    void setHeader(const RadarHeaderRS &header)       {m_header = header;}
    void setAddress(const RadarAddressRS &address)    {m_address = address;}
    void setData(const unsigned char &data)         {m_data = data;}
    void setDataArray(const QByteArray &dataArray)  {m_dataArray = dataArray;}

    RadarHeaderRS     getHeader()     {return m_header;}
    RadarAddressRS    getAddress()    {return m_address;}
    unsigned char   getData()       {return m_data;}
    QByteArray      getDataArray()  {return m_dataArray;}

    static unsigned char checksum(const QByteArray &data);
    static unsigned char checksum(const unsigned char &header, const unsigned char &address, const unsigned char &data);
    static QByteArray message(const QByteArray &data);
    static QByteArray message(const unsigned char &header, const unsigned char &address, const unsigned char &data);
    QByteArray message();
    unsigned char checksum();

    static QByteArray warmupTimeRequest(const bool &check = false);
    static qint16 warmapTimeValue(const QByteArray &message);
    static QByteArray updateMessage();
    static QList<QByteArray> triggerMessage(RadarTriggerRS &radarTrigger);
    static QList<QByteArray> triggerCheck();
    static QList<QByteArray> hlMessage(RadarHlSetupRS &radarHlSetup);
    static QList<QByteArray> hlCheck();

    static ErrorCodeHandlerRS checkMessage(const QByteArray &message);
    static ErrorCodeHandlerRS checkMessage(const QByteArray &message,
                                           const RadarHeaderDirectionRS &direction,
                                           const RadarHeaderOperationRS &operation,
                                           const RadarAddressBytesRS &addressBytes);
    static ErrorCodeHandlerRS checkMessage(const QByteArray &recMessage, const QByteArray &compMessage);

signals:

};

#endif // RADARMESSAGESERIAL_H
