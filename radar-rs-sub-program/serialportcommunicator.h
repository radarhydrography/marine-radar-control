/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SERIALPORTCOMMUNICATOR_H
#define SERIALPORTCOMMUNICATOR_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <QTimer>
#include <QByteArray>
#include <QObject>
#include <QSerialPort>
#include <QString>
#include "structsserial.h"

class SerialPortCommunicator : public QThread
{
    Q_OBJECT

public:

    explicit SerialPortCommunicator(QObject *parent = nullptr);
    explicit SerialPortCommunicator(const QString &portName, QObject *parent = nullptr);
    explicit SerialPortCommunicator(const QString &portName, const SerialPortSettings &portSettings, QObject *parent = nullptr);
    ~SerialPortCommunicator();

    void write(const QByteArray &writeData);
    QByteArray readwrite(QSerialPort *serialPort, const QByteArray &message);
    QByteArray readwrite(const QByteArray &message);
    QByteArray readwrite(const QByteArray &message, const QString &portName, const SerialPortSettings &portSettings = SerialPortSettings());

    void setPortName(const QString &portName) {m_portName = portName;}
    void setPortParameters(const QString &portName, const SerialPortSettings &portSettings);
    static void setPortParameters(QSerialPort *serialPort, const QString &portName, const SerialPortSettings &portSettings);
    void setPortSettings(const SerialPortSettings &portSettings) {m_portSettings = portSettings;}
    QString getPortName() {return m_portName;}
    SerialPortSettings getPortSettings() {return m_portSettings;}

    void startListen(const QString &portName, SerialPortSettings portSettings = SerialPortSettings());
    void startListen();

    QByteArray read();
    QByteArray read(QSerialPort *serialPort);
    void applyPortParameters();
    void applyPortParamerets(QSerialPort *serialPort);

    bool isRunning();

public slots:
    void stopListen();
    void handleBytesWritten(qint64 bytes);
    void handleError(QSerialPort::SerialPortError serialPortError);

signals:
    void message(const QString &s);
    void rawMessage(const QByteArray &msg);
    void error(const QString &s);
    void poolStatus(bool threadStart);
    void handleWriteStatus(QString message);

private:
    void run() override;

    QString m_portName;
    SerialPortSettings m_portSettings;
    QSerialPort* m_serialPort = nullptr;
    QByteArray m_writeData;
    qint64 m_bytesWritten;
    QTimer m_timer;
    QByteArray m_readenData;
    qint64 m_bytesReaden;
    QMutex *m_mutex;
    bool m_quit = false;

};

#endif // SERIALPORTCOMMUNICATOR_H
