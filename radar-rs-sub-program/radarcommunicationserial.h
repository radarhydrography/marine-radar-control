﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef RADARCOMMUNICATIONSERIAL_H
#define RADARCOMMUNICATIONSERIAL_H

#include <QByteArray>
#include <QObject>
#include <QThread>
#include <QTime>
#include <QMutex>
#include <QList>
#include "radarstructsserial.h"
#include "radarmessageserial.h"
#include "serialportcommunicator.h"

class RadarCommunicationSerial : public QThread
{

    Q_OBJECT

    QMutex *m_mutex;
    bool m_workingThreadEnable;
    bool m_workingThreadPause;
    bool m_readyToWrite;

    RadarMessageSerial *m_message;
    SerialPortCommunicator *m_serialPortCommunicator;
    QList<RadarMessageSerial> *m_operationBuffer;


public:
    explicit RadarCommunicationSerial(QObject *parent = nullptr);
    ~RadarCommunicationSerial();

    void setPortParameters(const QString &portName, const SerialPortSettings &portSettings);
    void setPortName(const QString &portName);
    void setPortSettings(const SerialPortSettings &portSettings);
    void sendMessage(const QByteArray &message);
    QByteArray receiveMessage();
    QByteArray sendReceiveMessage(const QByteArray &message);
    QList<QByteArray> sendReceiveMessage(const QList<QByteArray> &message);

    void setRegister();
    void setRegister(const RadarAddressRS &address, const unsigned char &data);
    unsigned char getRegister();
    unsigned char getRegister(const RadarAddressRS &address);
    void setRegisters(const RadarAddressRS &address, const QByteArray &data);
    void setRegisters(const QList<QByteArray> &messageList);
    QByteArray getRegisters(const QList<RadarAddressRS> &addressList);
    QByteArray getRegisters(const QList<QByteArray> &messageList);

public slots:
    void startWorkingThread();
    void stopWorkingThread();

    void setOperaton(const RadarMessageSerial &message);

    void pauseWorkingThread();

    void continueWorkingThread();

signals:
    void sendMessageToRadar(const QByteArray &message);
    void textInterpTestSignal(const QString &message);
    void warmupTimer(const quint8 &warmpoTimerValue);
    void warmupIsComplete(const bool &complete);
    void radarStatus(const RadarStatusRS &status);
    void poolStatus(bool threadStart);
    void SignalstatusMessage(const QString &message);

private:
    void run() override;

};

#endif // RADARCOMMUNICATIONSERIAL_H
