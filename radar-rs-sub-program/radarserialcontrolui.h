﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef RADARSERIALCONTROLUI_H
#define RADARSERIALCONTROLUI_H

#include <QObject>
#include <QWidget>
#include <QGridLayout>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QLCDNumber>
#include <QMainWindow>
#include <QCheckBox>
#include <QGroupBox>
#include <QMenuBar>
#include <QStatusBar>
#include <QMenu>

#include "radarcommunicationserial.h"
#include "serialportsettingsui.h"
#include "shared-libs/inireader.h"

class RadarSerialControlUI : public QWidget
{
    Q_OBJECT

    QMap<QString, QString> *m_iniSettings;

public:
    RadarSerialControlUI(QMap<QString, QString> iniSettings = QMap<QString, QString>(), QWidget *parent = nullptr);
    ~RadarSerialControlUI();

    QWidget *m_externalControlWidget;
    QWidget *ExternalControlWidget();

    void exit();

private:

//    void setIniSettings(QMap<QString, QString> &iniSettings);
//    QMap<QString, QString> getIniSettings(QMap<QString, QString> &iniSettings);

    QWidget *TimerWidget();
    QPushButton *m_timerStartCounter;
    QPushButton *m_timerStopCounter;
    QLCDNumber *m_timerCounter;

    QWidget *TriggerWidget();
    QComboBox *m_triggerEnable;
    QSpinBox *m_triggerStaggerValue;
    QComboBox *m_triggerPeriod;
    QPushButton *m_triggerSet;
    QPushButton *m_triggerGet;
    RadarTriggerRS m_radarTriggerSettings;
    RadarTriggerRS m_radarTriggerSettingsDefault;

    QWidget *MiscZeroWidget();
    QPushButton *m_miscZeroSet;
    QPushButton *m_miscZeroGet;
    QComboBox *m_miscZeroTestACP;
    QComboBox *m_miscZeroAutoTineFine;
    QComboBox *m_miscZeroAutoTuneReg;
    QComboBox *m_miscZeroDivACP;
    RadarMiscZeroRS m_radarMiscZeroSettings;
    RadarMiscZeroRS m_radarMiscZeroSettingsDefault;

    QWidget *MiscOneWidget();
    QPushButton *m_miscOneSet;
    QPushButton *m_miscOneGet;
    QComboBox *m_miscOneAcpPeriod;
    QComboBox *m_miscOneBlackingRam;
    QComboBox *m_miscOneBandwidth;
    RadarMiscOneRS m_radarMiscOneSettings;
    RadarMiscOneRS m_radarMiscOneSettingsDefault;

    QWidget *HlSetupWidget();
    QPushButton *m_hlSetupSet;
    QPushButton *m_hlSetupGet;
    QComboBox *m_hlSetupWidth;
    QComboBox *m_hlSetupInvert;
    QComboBox *m_hlSetupEdge;
    QSpinBox *m_hlSetupDelay;
    RadarHlSetupRS m_radarHlSetupSettings;
    RadarHlSetupRS m_radarHlSetupSettingsDefault;

    QWidget *EnableWidget();
    QPushButton *m_enableSet;
    QPushButton *m_enableGet;
    QComboBox *m_enableAutoTune;
    QComboBox *m_enableDA;
    QComboBox *m_enableAD;
    RadarEnableRS m_radarEnableSettings;
    RadarEnableRS m_radarEnableSettingsDefault;

    QWidget *TuneCoarseWidget();
    QPushButton *m_tuneCoatseSet;
    QPushButton *m_tuneCoarseGet;
    QSpinBox *m_tuneCoarseValue;
    RadarTuneCoarseRS m_radarTuneCoarseSettings;
    RadarTuneCoarseRS m_radarTuneCoarseSettingsDefault;

    QWidget *TuneFineWidget();
    QPushButton *m_tuneFineSet;
    QPushButton *m_tuneFineGet;
    QSpinBox *m_tuneFineValue;
    RadarTuneFineRS m_radarTuneFineSettings;
    RadarTuneFineRS m_radarTuneFineSettingsDefault;

    QWidget *TuneRegWidget();
    QPushButton *m_tuneRegSet;
    QPushButton *m_tuneRegGet;
    QSpinBox *m_tuneRegValue;
    RadarTuneRegRS m_radarTuneRegSettings;
    RadarTuneRegRS m_radarTuneRegSettingsDefault;

    QWidget *SetupWidget();
    QPushButton *m_setupSet;
    QPushButton *m_setupGet;
    QComboBox *m_setupTx;
    QComboBox *m_setupPulseWidth;
    QComboBox *m_setupMotor;
    RadarSetupRS m_radarSetupSettings;
    RadarSetupRS m_radarSetupSettingsDefault;

    QWidget *IndicatorWidget();
    QPushButton *m_autotuneFineGet;
    QSpinBox *m_autotuneFineValue;
    QPushButton *m_autotuneRegGet;
    QSpinBox *m_autotuneRegValue;
    QPushButton *m_magIndicatorGet;
    QSpinBox *m_magIndidcatorValue;
    QPushButton *m_tuneIndicatorGet;
    QSpinBox *m_tuneIndicatorValue;

    QWidget *StatusWidget();
    QPushButton *m_statusGet;
    QComboBox *m_statusWarmup;
    QComboBox *m_statusAutotuneReg;
    QComboBox *m_statusAutotuneFine;
    QComboBox *m_statusAcpInputSignal;
    QComboBox *m_statusHlInputSignal;
    QComboBox *m_statusTriggerInputSignal;

    QWidget *SerialPortWidget();
    QLineEdit *m_portName;
    QPushButton *m_portSettings;
    QCheckBox *m_portManualCheck;
    QPushButton *m_setPort;
    QString m_portTip;

    QPushButton *m_startTx;
    QPushButton *m_stopTx;
    QPushButton *m_openSettings;

    QWidget *GeneralWidget();
    QPushButton *m_getAll;
    QPushButton *m_setAll;
    QPushButton *m_resetSettings;
    QPushButton *m_defaultSettings;

    RadarCommunicationSerial *m_radarCommunicationSerial;
    QString m_serialPortName;
    SerialPortSettings *m_serialPortSettings;
    SerialPortSettingsUI *m_serialPortSettingsUI;

private slots:
    void timerStartCounterClicked();
    void timerStopCounterClicked();
    void timerCounterValue(int number);
    void warmupIsComplete(bool warmupStatus);

    void triggerSetClicked();
    void triggerGetClicked();

    void miscZeroSetClicked();
    void miscZeroGetClicked();

    void miscOneSetClicked();
    void miscOneGetClicked();

    void hlSetupSetClicked();
    void hlSetupGetClicked();

    void enableSetClicked();
    void enableGetClicked();

    void tuneCoatseSetClicked();
    void tuneCoarseGetClicked();

    void tuneFineSetClicked();
    void tuneFineGetClicked();

    void tuneRegSetClicked();
    void tuneRegGetClicked();

    void setupSetClicked();
    void setupGetClicked();

    void autotuneFineGetClicked();

    void autotuneRegGetClicked();

    void magIndicatorGetClicked();

    void tuneIndicatorGetClicked();

    void statusGetClicked();

    void SlotStartTxClicked();
    void SlotStopTxClicked();

    void SlotPortSettingsClicked();
    void SlotPortManualCheckClicked();
    void SlotSetPortClicked();

    void SlotGetAndSetDefaults();
    void SlotAllowSet(const bool &allow);

public slots:

    void setTriggetState(RadarTriggerRS &triggerState);

    void OpenSettingsClicked();

    void SlotSetAll();
    void SlotGetAll();
    void SlotResetSettingsClicked();
    void SlotDefaultSettingsClicked();

    void SlotSetSerialPortParameters(const QString &serialPortName, const SerialPortSettings &serialPortSettings);

    void setFullPalette(const QPalette &palette);

signals:
    void SignalstatusMessage(const QString &message);

};

#endif // RADARSERIALCONTROLUI_H
