/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "radarmessageserial.h"

RadarMessageSerial::RadarMessageSerial(QObject *parent) : QObject(parent)
{

}

RadarMessageSerial::RadarMessageSerial(const RadarMessageSerial &radarMessageSerial, QObject *parent) : QObject(parent)
{
    m_header = radarMessageSerial.m_header;
    m_address = radarMessageSerial.m_address;
    m_data = radarMessageSerial.m_data;
}

RadarMessageSerial RadarMessageSerial::operator=(const RadarMessageSerial &radarMessageSerial)
{
    return radarMessageSerial;
}

RadarMessageSerial::RadarMessageSerial(const RadarHeaderRS &header, const RadarAddressRS &address, QObject *parent) :
    QObject(parent), m_header{header}, m_address{address}
{
    m_data = 0;
}

RadarMessageSerial::RadarMessageSerial(const RadarHeaderRS &header, const RadarAddressRS &address, const unsigned char &data, QObject *parent) :
    QObject(parent), m_header{header}, m_address{address}, m_data{data}
{

}

RadarMessageSerial::RadarMessageSerial(const RadarHeaderRS &header, const RadarAddressRS &address, const QByteArray &dataArray, QObject *parent) :
    QObject(parent), m_header{header}, m_address{address}, m_dataArray{dataArray}
{

}

unsigned char RadarMessageSerial::checksum(const QByteArray &data)
{
    unsigned char checksum{0};
    for (quint8 dataByteNumber = 0; dataByteNumber < 3; dataByteNumber++) {
        checksum ^= static_cast<unsigned char>(data[dataByteNumber]);
    }
    return checksum;
}

unsigned char RadarMessageSerial::checksum(const unsigned char &header, const unsigned char &address, const unsigned char &data)
{
    unsigned char checksum;
    checksum = header ^ address ^ data;
    return checksum;
}

QByteArray RadarMessageSerial::message(const QByteArray &data)
{
    QByteArray message;
    message = data;
    message.append(checksum(data));
    return message;
}

QByteArray RadarMessageSerial::message(const unsigned char &header, const unsigned char &address, const unsigned char &data)
{
    QByteArray message;
    message.append(header);
    message.append(address);
    message.append(data);
    message.append(checksum(header, address, data));
    return message;
}

QByteArray RadarMessageSerial::message()
{
    return message(m_header.radarHeaderRS(), m_address.radarAddressRS(), m_data);
}

unsigned char RadarMessageSerial::checksum()
{
    return checksum(m_header.radarHeaderRS(), m_address.radarAddressRS(), m_data);
}

QByteArray RadarMessageSerial::warmupTimeRequest(const bool &check)
{
    RadarMessageSerial message(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg),
                         RadarAddressRS(RadarAddressBytesRS::WarmupTimer));
    if (check) {
        message.m_header.m_direction = RadarHeaderDirectionRS::SubToHost;
//        message.m_header.m_operation = RadarHeaderOperationRS::WriteToReg;
    }
    message.setData(static_cast<unsigned char>(0));
    return message.message();
}

qint16 RadarMessageSerial::warmapTimeValue(const QByteArray &message)
{
    qint16 errorValue = static_cast<quint16>(checkMessage(message, warmupTimeRequest(true)));
    if (abs(errorValue)) {
        return errorValue;
    }
    return static_cast<quint16>(RadarWarmupTimerRS::warmupTimerRS(message[2]));
}

QByteArray RadarMessageSerial::updateMessage()
{
    RadarMessageSerial message(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::WriteToReg),
                             RadarAddressRS(RadarAddressBytesRS::Update));
    message.setData(static_cast<unsigned char>(static_cast<quint8>(rand()*255)));
    return message.message();
}

QList<QByteArray> RadarMessageSerial::triggerMessage(RadarTriggerRS &radarTrigger)
{
    QByteArray triggerMessage = radarTrigger.radarTriggers();
    RadarMessageSerial trigger0(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::WriteToReg), RadarAddressRS(RadarAddressBytesRS::Trigger_0), triggerMessage[0]);
    RadarMessageSerial trigger1(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::WriteToReg), RadarAddressRS(RadarAddressBytesRS::Trigger_1), triggerMessage[1]);
    RadarMessageSerial trigger2(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::WriteToReg), RadarAddressRS(RadarAddressBytesRS::Trigger_2), triggerMessage[2]);
    return {trigger0.message(), trigger1.message(), trigger2.message()};
}

QList<QByteArray> RadarMessageSerial::triggerCheck()
{
    RadarMessageSerial trigger0(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), RadarAddressRS(RadarAddressBytesRS::Trigger_0), static_cast<unsigned char>(0));
    RadarMessageSerial trigger1(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), RadarAddressRS(RadarAddressBytesRS::Trigger_1), static_cast<unsigned char>(0));
    RadarMessageSerial trigger2(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), RadarAddressRS(RadarAddressBytesRS::Trigger_2), static_cast<unsigned char>(0));
    return {trigger0.message(), trigger1.message(), trigger2.message()};
}

QList<QByteArray> RadarMessageSerial::hlMessage(RadarHlSetupRS &radarHlSetup)
{
    QByteArray hlMessage = radarHlSetup.radarHlSetup();
    RadarMessageSerial hlSetup0(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::WriteToReg), RadarAddressRS(RadarAddressBytesRS::ARPSetup_0), hlMessage[0]);
    RadarMessageSerial hlSetup1(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::WriteToReg), RadarAddressRS(RadarAddressBytesRS::ARPSetup_1), hlMessage[1]);
    return {hlSetup0.message(), hlSetup1.message()};
}

QList<QByteArray> RadarMessageSerial::hlCheck()
{
    RadarMessageSerial hl0(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), RadarAddressRS(RadarAddressBytesRS::ARPSetup_0), static_cast<unsigned char>(0));
    RadarMessageSerial hl1(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), RadarAddressRS(RadarAddressBytesRS::ARPSetup_1), static_cast<unsigned char>(0));
    return {hl0.message(), hl1.message()};
}

ErrorCodeHandlerRS RadarMessageSerial::checkMessage(const QByteArray &message, const RadarHeaderDirectionRS &direction, const RadarHeaderOperationRS &operation, const RadarAddressBytesRS &addressBytes)
{
    RadarMessageSerial msg((RadarHeaderRS(message[0])), (RadarAddressRS(message[1])));
    if (checksum(message) != message[3]) {
        return ErrorCodeHandlerRS::WrongChecksum;
    }
    if (msg.getHeader().m_direction != direction || msg.getHeader().m_operation != operation) {
        return ErrorCodeHandlerRS::WrongHeader;
    }
    if (msg.getAddress().m_address != addressBytes) {
        return ErrorCodeHandlerRS::WrongAddress;
    }
    return ErrorCodeHandlerRS::NoError;
}

ErrorCodeHandlerRS RadarMessageSerial::checkMessage(const QByteArray &recMessage, const QByteArray &compMessage)
{
//    if (checksum(recMessage) != recMessage[3]) {
//        return ErrorCodeHandlerRS::WrongChecksum;
//    }
    if (recMessage[0] != compMessage[0]) {
        return ErrorCodeHandlerRS::WrongHeader;
    }
    if (recMessage[1] != compMessage[1]) {
        return ErrorCodeHandlerRS::WrongAddress;
    }
    return ErrorCodeHandlerRS::NoError;
}

ErrorCodeHandlerRS RadarMessageSerial::checkMessage(const QByteArray &message)
{
    QList<quint8> headerNum;

    for (auto byteNum = 0; byteNum < message.size(); byteNum++) {

        RadarHeaderRS header(message[byteNum]);

        if ((header.m_direction == RadarHeaderDirectionRS::HostToSub ||
             header.m_direction == RadarHeaderDirectionRS::SubToHost ) &&
            (header.m_operation == RadarHeaderOperationRS::ReadFromRAM ||
             header.m_operation == RadarHeaderOperationRS::ReadFromReg ||
             header.m_operation == RadarHeaderOperationRS::WriteToRAM ||
             header.m_operation == RadarHeaderOperationRS::WriteToReg))
        {
            headerNum.append(byteNum);
        }
    }

    if (headerNum.isEmpty()) {
        return ErrorCodeHandlerRS::WrongHeader;
    }

    for (auto msgNum = 0; msgNum < headerNum.size(); msgNum++) {

        if (RadarMessageSerial::checksum(message[headerNum[msgNum]], message[headerNum[msgNum] + 1], message[headerNum[msgNum] + 2]) == message[headerNum[msgNum] + 3]) {
            return ErrorCodeHandlerRS::WrongChecksum;
        }

        RadarHeaderRS header(message[headerNum[msgNum]]);

        switch (header.m_direction) {
        case RadarHeaderDirectionRS::HostToSub : {
            break;
        }
        case RadarHeaderDirectionRS::SubToHost : {
            break;
        }
        default : {
            return ErrorCodeHandlerRS::WrongHeader;
        }
        }

        switch (header.m_operation) {
        case RadarHeaderOperationRS::WriteToReg : {
            break;
        }
        case RadarHeaderOperationRS::ReadFromReg : {
            break;
        }
        case RadarHeaderOperationRS::WriteToRAM : {
            break;
        }
        case RadarHeaderOperationRS::ReadFromRAM : {
            break;
        }
        default : {
            return ErrorCodeHandlerRS::WrongHeader;
        }
        }

        RadarAddressRS address(message[headerNum[msgNum] + 1]);

        switch (address.m_address) {
        case RadarAddressBytesRS::Trigger_0 : {
            break;
        }
        case RadarAddressBytesRS::Trigger_1 : {
            break;
        }
        case RadarAddressBytesRS::Trigger_2 : {
            break;
        }
        case RadarAddressBytesRS::Misc_0 : {
            break;
        }
        case RadarAddressBytesRS::Misc_1 : {
            break;
        }
        case RadarAddressBytesRS::ARPSetup_0 : {
            break;
        }
        case RadarAddressBytesRS::ARPSetup_1 : {
            break;
        }
        case RadarAddressBytesRS::Enable : {
            break;
        }
        case RadarAddressBytesRS::TuneCoarse : {
            break;
        }
        case RadarAddressBytesRS::TuneFine: {
            break;
        }
        case RadarAddressBytesRS::TuneReg : {
            break;
        }
        case RadarAddressBytesRS::Setup : {
            break;
        }
        case RadarAddressBytesRS::MAGIndicator : {
            break;
        }
        case RadarAddressBytesRS::TuneIndicator : {
            break;
        }
        case RadarAddressBytesRS::Status : {
            break;
        }
        case RadarAddressBytesRS::WarmupTimer : {
            break;
        }
        case RadarAddressBytesRS::Version : {
            break;
        }
        case RadarAddressBytesRS::Update : {
            break;
        }
        default:
            return ErrorCodeHandlerRS::WrongAddress;
        }

    }

    return ErrorCodeHandlerRS::NoError;
}
