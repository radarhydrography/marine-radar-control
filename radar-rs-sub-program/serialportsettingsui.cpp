/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "serialportsettingsui.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>

SerialPortSettingsUI::SerialPortSettingsUI(QWidget *parent, SerialPortSettings defaultSerialPortSettings) : QWidget(parent)
{
    m_defaultSerialPortSettings = defaultSerialPortSettings;

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *topLayout = new QHBoxLayout;

    QLabel *portNameLabel = new QLabel(QString(tr("Port")));
    topLayout->addWidget(portNameLabel);

    m_portNameComboBox = new QComboBox;
    m_portNameComboBox->setEditable(false);
    m_portNameComboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    connect(m_portNameComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SerialPortSettingsUI::SlotPortNameComboBoxIndexChanged);
    topLayout->addWidget(m_portNameComboBox);

    m_portNameCheckBox = new QCheckBox(QString(tr("Manual")));
    connect(m_portNameCheckBox, &QCheckBox::stateChanged, this, &SerialPortSettingsUI::SlotPortNameCheckBoxClicked);
    topLayout->addWidget(m_portNameCheckBox);

    m_setPushButton = new QPushButton(QString(tr("Set")));
    connect(m_setPushButton, &QPushButton::clicked, this, &SerialPortSettingsUI::SlotSetPushButtonClicked);
    topLayout->addWidget(m_setPushButton);

    m_resetPushButton = new QPushButton(QString(tr("Reset")));
    connect(m_resetPushButton, &QPushButton::clicked, this, &SerialPortSettingsUI::SlotResetPushButtonClicked);
    topLayout->addWidget(m_resetPushButton);

//    topLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    m_scanPushButton = new QPushButton(QString(tr("Scan")));
    connect(m_scanPushButton, &QPushButton::clicked, this, &SerialPortSettingsUI::SlotScanPushButtonClicked);
    topLayout->addWidget(m_scanPushButton);

    mainLayout->addLayout(topLayout);

    QHBoxLayout *bottomLayout = new QHBoxLayout;

    m_portParametersTextEdit = new QTextEdit;
    bottomLayout->addWidget(m_portParametersTextEdit);

    QGridLayout *bottomRightLayout = new QGridLayout;

    QLabel *baudRateLabel = new QLabel(QString(tr("Baud Rate")));
    m_baudRateComboBox = new QComboBox;
    m_baudRateCheckBox = new QCheckBox(QString(tr("")));

    QLabel *dataBitsLabel = new QLabel(QString(tr("Data Bits")));
    m_dataBitsComboBox = new QComboBox;
    m_dataBitsCheckBox = new QCheckBox(QString(tr("")));

    QLabel *directionLabel = new QLabel(QString(tr("Direction")));
    m_directionComboBox = new QComboBox;
    m_directionCheckBox = new QCheckBox(QString(tr("")));

    QLabel *flowControlLabel = new QLabel(QString(tr("Flow Control")));
    m_flowControlComboBox = new QComboBox;
    m_flowControlCheckBox = new QCheckBox(QString(tr("")));

    QLabel *parityLabel = new QLabel(QString(tr("Parity")));
    m_parityComboBox = new QComboBox;
    m_parityCheckBox = new QCheckBox(QString(tr("")));

    QLabel *stopBitsLabel = new QLabel(QString(tr("Stop Bits")));
    m_stopBitsComboBox = new QComboBox;
    m_stopBitsCheckBox = new QCheckBox(QString(tr("")));

    bottomRightLayout->addWidget(baudRateLabel, 0, 0);
    bottomRightLayout->addWidget(m_baudRateComboBox, 0, 2);
    bottomRightLayout->addWidget(m_baudRateCheckBox, 0, 1);
    bottomRightLayout->addWidget(dataBitsLabel, 1, 0);
    bottomRightLayout->addWidget(m_dataBitsComboBox, 1, 2);
    bottomRightLayout->addWidget(m_dataBitsCheckBox, 1, 1);
    bottomRightLayout->addWidget(directionLabel, 2, 0);
    bottomRightLayout->addWidget(m_directionComboBox, 2, 2);
    bottomRightLayout->addWidget(m_directionCheckBox, 2, 1);
    bottomRightLayout->addWidget(flowControlLabel, 3, 0);
    bottomRightLayout->addWidget(m_flowControlComboBox, 3, 2);
    bottomRightLayout->addWidget(m_flowControlCheckBox, 3, 1);
    bottomRightLayout->addWidget(parityLabel, 4, 0);
    bottomRightLayout->addWidget(m_parityComboBox, 4, 2);
    bottomRightLayout->addWidget(m_parityCheckBox, 4, 1);
    bottomRightLayout->addWidget(stopBitsLabel, 5, 0);
    bottomRightLayout->addWidget(m_stopBitsComboBox, 5, 2);
    bottomRightLayout->addWidget(m_stopBitsCheckBox, 5, 1);

    bottomLayout->addLayout(bottomRightLayout);
    mainLayout->addLayout(bottomLayout);

    SerialPortSettings::portSettingsDefault(m_baudRateComboBox, m_dataBitsComboBox, m_directionComboBox, m_flowControlComboBox, m_parityComboBox, m_stopBitsComboBox);
    connect(m_baudRateCheckBox, &QCheckBox::clicked, this, &SerialPortSettingsUI::SlotBaudRateCheckBoxClicked);
    connect(m_dataBitsCheckBox, &QCheckBox::clicked, this, &SerialPortSettingsUI::SlotDataBitsCheckBoxClicked);
    connect(m_directionCheckBox, &QCheckBox::clicked, this, &SerialPortSettingsUI::SlotDirectionCheckBoxClicked);
    connect(m_flowControlCheckBox, &QCheckBox::clicked, this, &SerialPortSettingsUI::SlotFlowControlCheckBoxClicked);
    connect(m_parityCheckBox, &QCheckBox::clicked, this, &SerialPortSettingsUI::SlotParityCheckBoxClicked);
    connect(m_stopBitsCheckBox, &QCheckBox::clicked, this, &SerialPortSettingsUI::SlotStopBitsCheckBoxClicked);

    portNumerator();

    this->setLayout(mainLayout);

    this->setWindowTitle("Serial Port Settings");
}

void SerialPortSettingsUI::portNumerator()
{
    m_portParameters.clear();
    m_portSettings.clear();

    const auto serialPortInfos = QSerialPortInfo::availablePorts();
    const QString blankString = "N/A";
    QString description;
    QString manufacturer;
    QString serialNumber;
    m_portNames.clear();

    for (const QSerialPortInfo &serialPortInfo : serialPortInfos) {
        description = serialPortInfo.description();
        manufacturer = serialPortInfo.manufacturer();
        serialNumber = serialPortInfo.serialNumber();
        QStringList currentPort;
        m_portNames << serialPortInfo.portName();
        m_portParameters.append(SerialPortParameters(serialPortInfo.portName(),
                                                     serialPortInfo.systemLocation(),
                                                    (!description.isEmpty() ? description : blankString),
                                                    (!manufacturer.isEmpty() ? manufacturer : blankString),
                                                    (!serialNumber.isEmpty() ? serialNumber : blankString),
                                                    (serialPortInfo.hasVendorIdentifier()
                                                        ? QByteArray::number(serialPortInfo.vendorIdentifier(), 16)
                                                        : blankString),
                                                    (serialPortInfo.hasProductIdentifier()
                                                        ? QByteArray::number(serialPortInfo.productIdentifier(), 16)
                                                        : blankString),
                                                    (serialPortInfo.isBusy() ? "Yes" : "No")));
        m_portSettings.append(SerialPortSettings());
    }
    m_portNameComboBox->clear();
    m_portNameComboBox->addItems(m_portNames);
    m_portNameComboBox->setCurrentIndex(0);
    showPortParameters(m_portNameComboBox->currentIndex());
}

void SerialPortSettingsUI::showPortParameters(const int &index)
{
    QString portEdit;
    portEdit.append("Port: " + m_portParameters[index].s_port + "\n");
    portEdit.append("Location: " + m_portParameters[index].s_location + "\n");
    portEdit.append("Description: " + m_portParameters[index].s_description + "\n");
    portEdit.append("Manufacturer: " + m_portParameters[index].s_manufacturer + "\n");
    portEdit.append("Serial number: " + m_portParameters[index].s_serialNumber + "\n");
    portEdit.append("Vendor Identifier: " + m_portParameters[index].s_vendorIdentifier + "\n");
    portEdit.append("Product Identifier: " + m_portParameters[index].s_productIdentifier + "\n");
    portEdit.append("Busy: " + m_portParameters[index].s_busy);

    m_portParametersTextEdit->setText(portEdit);
}

void SerialPortSettingsUI::SlotPortNameCheckBoxClicked()
{
    m_portNameComboBox->setEditable(m_portNameCheckBox->checkState());
}

void SerialPortSettingsUI::SlotBaudRateCheckBoxClicked()
{
    m_baudRateComboBox->setEnabled(m_baudRateCheckBox->checkState());
}

void SerialPortSettingsUI::SlotDataBitsCheckBoxClicked()
{
    m_dataBitsComboBox->setEnabled(m_dataBitsCheckBox->checkState());
}

void SerialPortSettingsUI::SlotDirectionCheckBoxClicked()
{
    m_directionComboBox->setEnabled(m_directionCheckBox->checkState());
}

void SerialPortSettingsUI::SlotFlowControlCheckBoxClicked()
{
    m_flowControlComboBox->setEnabled(m_flowControlCheckBox->checkState());
}

void SerialPortSettingsUI::SlotParityCheckBoxClicked()
{
    m_parityComboBox->setEnabled(m_parityCheckBox->checkState());
}

void SerialPortSettingsUI::SlotStopBitsCheckBoxClicked()
{
    m_stopBitsComboBox->setEnabled(m_stopBitsCheckBox->checkState());
}

void SerialPortSettingsUI::SlotPortNameComboBoxIndexChanged(const int &index)
{
    showPortParameters(index);
}

void SerialPortSettingsUI::SlotScanPushButtonClicked()
{
    disconnect(m_portNameComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SerialPortSettingsUI::SlotPortNameComboBoxIndexChanged);
    portNumerator();
    connect(m_portNameComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SerialPortSettingsUI::SlotPortNameComboBoxIndexChanged);
}

void SerialPortSettingsUI::SlotSetPushButtonClicked()
{
    int index = m_portNameComboBox->currentIndex();
    m_portSettings[index].s_baudRate = SerialPortSettings::cmb2num(*m_baudRateComboBox, SerialParameters::BaudRate);
    m_portSettings[index].s_dataBits = SerialPortSettings::mum2QSPnum(SerialPortSettings::cmb2num(*m_dataBitsComboBox, SerialParameters::DataBits), m_defaultSerialPortSettings.s_dataBits);
    m_portSettings[index].s_direction = SerialPortSettings::mum2QSPnum(SerialPortSettings::cmb2num(*m_directionComboBox, SerialParameters::Direction), m_defaultSerialPortSettings.s_direction);
    m_portSettings[index].s_flowControl = SerialPortSettings::mum2QSPnum(SerialPortSettings::cmb2num(*m_flowControlComboBox, SerialParameters::FlowControl), m_defaultSerialPortSettings.s_flowControl);
    m_portSettings[index].s_parity = SerialPortSettings::mum2QSPnum(SerialPortSettings::cmb2num(*m_parityComboBox, SerialParameters::Parity), m_defaultSerialPortSettings.s_parity);
    m_portSettings[index].s_stopBits = SerialPortSettings::mum2QSPnum(SerialPortSettings::cmb2num(*m_stopBitsComboBox, SerialParameters::StopBits), m_defaultSerialPortSettings.s_stopBits);
    emit SignalSetSerialPortParameters(m_portNameComboBox->currentText(), m_portSettings[index]);
}

void SerialPortSettingsUI::SlotResetPushButtonClicked()
{
    for (int index = 0; index < m_portSettings.size(); index++) {
        m_portSettings[index] = m_defaultSerialPortSettings;
    }
    m_baudRateCheckBox->setCheckState(Qt::Unchecked);
    m_baudRateComboBox->setEnabled(m_baudRateCheckBox->checkState());
    m_baudRateComboBox->setCurrentIndex(0);
    m_dataBitsCheckBox->setCheckState(Qt::Unchecked);
    m_dataBitsComboBox->setEnabled(m_dataBitsCheckBox->checkState());
    m_dataBitsComboBox->setCurrentIndex(0);
    m_directionCheckBox->setCheckState(Qt::Unchecked);
    m_directionComboBox->setEnabled(m_directionCheckBox->checkState());
    m_directionComboBox->setCurrentIndex(0);
    m_flowControlCheckBox->setCheckState(Qt::Unchecked);
    m_flowControlComboBox->setEnabled(m_flowControlCheckBox->checkState());
    m_flowControlComboBox->setCurrentIndex(0);
    m_parityCheckBox->setCheckState(Qt::Unchecked);
    m_parityComboBox->setEnabled(m_parityCheckBox->checkState());
    m_parityComboBox->setCurrentIndex(0);
    m_stopBitsCheckBox->setCheckState(Qt::Unchecked);
    m_stopBitsComboBox->setEnabled(m_stopBitsCheckBox->checkState());
    m_stopBitsComboBox->setCurrentIndex(0);
}
