/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "radarserialcontrolui.h"
#include <QLayout>
#include <initializer_list>
#include <QSerialPort>

//#define STANDALONE

#ifndef STANDALONE
#include "shared-libs/inireader.h"
#endif

RadarSerialControlUI::RadarSerialControlUI(QMap<QString, QString> iniSettings, QWidget *parent) : QWidget(parent)
{
    m_radarCommunicationSerial = new RadarCommunicationSerial;
    m_serialPortSettings = new SerialPortSettings;

#ifndef STANDALONE

    IniReader::checkConfiguration(iniSettings, SettingsGroup::RadarSerialSettings);

    m_iniSettings = new QMap<QString, QString>;
    m_iniSettings->insert(iniSettings);

    m_serialPortName = iniSettings["SerialPortName"];
    m_serialPortSettings->s_baudRate = SerialPortSettings::mum2QSPnum(iniSettings["SerialPortBaudRate"].toUInt(), QSerialPort::Baud9600);
    m_serialPortSettings->s_dataBits = SerialPortSettings::mum2QSPnum(iniSettings["SerialPortDataBits"].toUInt(), QSerialPort::Data8);
    m_serialPortSettings->s_stopBits = SerialPortSettings::mum2QSPnum(iniSettings["SerialPortStopBits"].toUInt(), QSerialPort::OneStop);
    m_serialPortSettings->s_parity = SerialPortSettings::mum2QSPnum(iniSettings["SerialPortParity"].toUInt(), QSerialPort::NoParity);
    m_serialPortSettings->s_flowControl = SerialPortSettings::mum2QSPnum(iniSettings["SerialPortFlowControl"].toUInt(), QSerialPort::NoFlowControl);
    m_serialPortSettings->s_direction = SerialPortSettings::mum2QSPnum(iniSettings["SerialPortDirection"].toUInt(), QSerialPort::AllDirections);

    m_radarTriggerSettings.m_triggerGeneratorState = RadarTriggerGeneratorStateRS::Disabled;
    m_radarTriggerSettings.m_triggerPeriod = RadarTriggerPeriodRS::PRF3200Hz;
    m_radarTriggerSettings.m_staggerValue = 0;

    m_radarMiscZeroSettings.m_testACP = RadarMiscZeroTestACPRS::NormalOperation;
    m_radarMiscZeroSettings.m_autoTuneFine = RadarMiscZeroAutoTuneFineRS::Disabled;
    m_radarMiscZeroSettings.m_autoTuneReg = RadarMiscZeroAutoTuneRegRS::NormalOperation;
    m_radarMiscZeroSettings.m_divideACPInput = RadarMiscZeroDivideACPInputRS::NoDivide;

    m_radarMiscOneSettings.m_acpPeriod = RadarMiscOneAcpPeriodRS::ACP2048Pulses;
    m_radarMiscOneSettings.m_blankingRam = RadarMiscOneBlankingRamRS::BlockFrom0to511;
    m_radarMiscOneSettings.m_bandwith = RadarMiscOneBandwithRS::WideBand;

    m_radarHlSetupSettings.m_widgth = RadarHlSetupWidthRS::AcpHalfPulse;
    m_radarHlSetupSettings.m_invert = RadarHlSetupInvertRS::SamePolarity;
    m_radarHlSetupSettings.m_edge = RadarHlSetupEdgeRS::FallingEdge;
    m_radarHlSetupSettings.m_delayValue = 0;

    m_radarEnableSettings.m_autoTune = RadarEnableAutoTuneRS::Disabled;
    m_radarEnableSettings.m_dac = RadarEnableDAConvertersRS::Enabled;
    m_radarEnableSettings.m_adc = RadarEnableADConvertersRS::Enabled;

    m_radarTuneCoarseSettings.m_coarseValue = 0;
    m_radarTuneFineSettings.m_fineValue = 0;
    m_radarTuneRegSettings.m_regValue = 0;

    m_radarSetupSettings.m_transceiverMode = RadarSetupTransceiverModeRS::TransceiverStandBy;
    m_radarSetupSettings.m_pulseMode = RadarSetupPulseModeRS::ShortPulse;
    m_radarSetupSettings.m_enableMotor = RadarSetupEnableMotorRS::Disabled;

    m_radarTriggerSettingsDefault = m_radarTriggerSettings;
    m_radarMiscZeroSettingsDefault = m_radarMiscZeroSettings;
    m_radarMiscOneSettingsDefault = m_radarMiscOneSettings;
    m_radarHlSetupSettingsDefault = m_radarHlSetupSettings;
    m_radarEnableSettingsDefault = m_radarEnableSettings;
    m_radarTuneCoarseSettingsDefault = m_radarTuneCoarseSettings;
    m_radarTuneFineSettingsDefault = m_radarTuneFineSettings;
    m_radarTuneRegSettingsDefault = m_radarTuneRegSettings;
    m_radarSetupSettingsDefault = m_radarSetupSettings;

#endif

#ifdef STANDALONE

    m_serialPortName = QString("/dev/ttyUSB0");
    m_serialPortSettings->s_baudRate = QSerialPort::Baud9600;
    m_serialPortSettings->s_dataBits = QSerialPort::Data8;
    m_serialPortSettings->s_stopBits = QSerialPort::OneStop;
    m_serialPortSettings->s_parity = QSerialPort::NoParity;
    m_serialPortSettings->s_flowControl = QSerialPort::NoFlowControl;
    m_serialPortSettings->s_direction = QSerialPort::AllDirections;

    m_radarTriggerSettings.m_triggerGeneratorState = RadarTriggerGeneratorStateRS::Disabled;
    m_radarTriggerSettings.m_triggerPeriod = RadarTriggerPeriodRS::PRF3200Hz;
    m_radarTriggerSettings.m_staggerValue = 0;

    m_radarMiscZeroSettings.m_testACP = RadarMiscZeroTestACPRS::NormalOperation;
    m_radarMiscZeroSettings.m_autoTuneFine = RadarMiscZeroAutoTuneFineRS::Disabled;
    m_radarMiscZeroSettings.m_autoTuneReg = RadarMiscZeroAutoTuneRegRS::NormalOperation;
    m_radarMiscZeroSettings.m_divideACPInput = RadarMiscZeroDivideACPInputRS::NoDivide;

    m_radarMiscOneSettings.m_acpPeriod = RadarMiscOneAcpPeriodRS::ACP2048Pulses;
    m_radarMiscOneSettings.m_blankingRam = RadarMiscOneBlankingRamRS::BlockFrom0to511;
    m_radarMiscOneSettings.m_bandwith = RadarMiscOneBandwithRS::WideBand;

    m_radarHlSetupSettings.m_widgth = RadarHlSetupWidthRS::AcpHalfPulse;
    m_radarHlSetupSettings.m_invert = RadarHlSetupInvertRS::SamePolarity;
    m_radarHlSetupSettings.m_edge = RadarHlSetupEdgeRS::FallingEdge;
    m_radarHlSetupSettings.m_delayValue = 0;

    m_radarEnableSettings.m_autoTune = RadarEnableAutoTuneRS::Disabled;
    m_radarEnableSettings.m_dac = RadarEnableDAConvertersRS::Enabled;
    m_radarEnableSettings.m_adc = RadarEnableADConvertersRS::Enabled;

    m_radarTuneCoarseSettings.m_coarseValue = 0;
    m_radarTuneFineSettings.m_fineValue = 0;
    m_radarTuneRegSettings.m_regValue = 0;

    m_radarSetupSettings.m_transceiverMode = RadarSetupTransceiverModeRS::TransceiverStandBy;
    m_radarSetupSettings.m_pulseMode = RadarSetupPulseModeRS::ShortPulse;
    m_radarSetupSettings.m_enableMotor = RadarSetupEnableMotorRS::Disabled;

#endif

    m_radarCommunicationSerial->setPortName(m_serialPortName);
    m_radarCommunicationSerial->setPortSettings(*m_serialPortSettings);

    connect(m_radarCommunicationSerial, &RadarCommunicationSerial::warmupTimer, this, &RadarSerialControlUI::timerCounterValue);
    connect(m_radarCommunicationSerial, &RadarCommunicationSerial::warmupIsComplete, this, &RadarSerialControlUI::warmupIsComplete);

    QWidget *triggerWidget = TriggerWidget();
    QWidget *miscZeroWidget = MiscZeroWidget();
    QWidget *miscOneWidget = MiscOneWidget();
    QWidget *hlSetupWidget = HlSetupWidget();
    QWidget *enableWidget = EnableWidget();
    QWidget *tuneCoarseWidget = TuneCoarseWidget();
    QWidget *tuneFineWidget = TuneFineWidget();
    QWidget *tuneRegWidget = TuneRegWidget();
    QWidget *setupWidget = SetupWidget();
    QWidget *indicatorWidget = IndicatorWidget();
    QWidget *statusWidget = StatusWidget();
    QWidget *serialPortWidget = SerialPortWidget();
    QWidget *generalWidget = GeneralWidget();

    QHBoxLayout *generalLayout = new QHBoxLayout;
    generalLayout->addWidget(generalWidget);
    generalLayout->addWidget(serialPortWidget);

    QVBoxLayout *tuneLayout = new QVBoxLayout;
    tuneLayout->addWidget(tuneCoarseWidget);
    tuneLayout->addWidget(tuneFineWidget);
    tuneLayout->addWidget(tuneRegWidget);

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addLayout(generalLayout, 0, 0, 1, 3);
    mainLayout->addLayout(tuneLayout, 1, 0);
    mainLayout->addWidget(statusWidget, 1, 1);
    mainLayout->addWidget(indicatorWidget, 1, 2);
    mainLayout->addWidget(triggerWidget, 2, 0);
    mainLayout->addWidget(enableWidget, 2, 1);
    mainLayout->addWidget(setupWidget, 2, 2);
    mainLayout->addWidget(hlSetupWidget, 3, 0);
    mainLayout->addWidget(miscZeroWidget, 3, 1);
    mainLayout->addWidget(miscOneWidget, 3, 2);

    setLayout(mainLayout);

    m_externalControlWidget = ExternalControlWidget();

    m_serialPortSettingsUI = new SerialPortSettingsUI(this, *m_serialPortSettings);
    m_serialPortSettingsUI->setWindowFlag(Qt::Window);
    connect(m_serialPortSettingsUI, &SerialPortSettingsUI::SignalSetSerialPortParameters, this, &RadarSerialControlUI::SlotSetSerialPortParameters);

    m_portName->setText(m_serialPortName);

    setWindowTitle("RADAR RS422 Control Pannel");

    connect(m_radarCommunicationSerial, &RadarCommunicationSerial::SignalstatusMessage, this, &RadarSerialControlUI::SignalstatusMessage);
}

RadarSerialControlUI::~RadarSerialControlUI()
{
}

QWidget *RadarSerialControlUI::ExternalControlWidget()
{
    QWidget *externalControlWidget = new QWidget;

    QGroupBox *controlGroupBox = new QGroupBox;
    controlGroupBox->setTitle("Serial Port Tx Control");

    QHBoxLayout *mainControlLayout = new QHBoxLayout;

    m_startTx = new QPushButton;
    m_startTx->setText("Start Tx");
    connect(m_startTx, &QPushButton::clicked, this, &RadarSerialControlUI::SlotStartTxClicked);
    mainControlLayout->addWidget(m_startTx);

    m_stopTx = new QPushButton;
    m_stopTx->setText("Stop Tx");
    connect(m_stopTx, &QPushButton::clicked, this, &RadarSerialControlUI::SlotStopTxClicked);
    mainControlLayout->addWidget(m_stopTx);

    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    mainControlLayout->addSpacerItem(spacer);

    m_openSettings = new QPushButton;
    m_openSettings->setText("Settings");
    mainControlLayout->addWidget(m_openSettings);

    controlGroupBox->setLayout(mainControlLayout);

    QVBoxLayout *mainLayout = new QVBoxLayout;

    QWidget *warmupTimerWidget = TimerWidget();

    mainLayout->addWidget(controlGroupBox);
    mainLayout->addWidget(warmupTimerWidget);

    externalControlWidget->setLayout(mainLayout);

//    connect(m_startRx, &QPushButton::clicked, this, &GreenBoardControlUI::SlotStartRxClicked);
//    connect(m_stopRx, &QPushButton::clicked, this, &GreenBoardControlUI::SlotStopRxClicked);
    connect(m_openSettings, &QPushButton::clicked, this, &RadarSerialControlUI::OpenSettingsClicked);

    return externalControlWidget;
}

void RadarSerialControlUI::exit()
{
    SlotStopTxClicked();
}

//void RadarSerialControlUI::setIniSettings(QMap<QString, QString> &iniSettings)
//{

//}

//QMap<QString, QString> RadarSerialControlUI::getIniSettings(QMap<QString, QString> &iniSettings)
//{

//}

QWidget* RadarSerialControlUI::TimerWidget()
{
    QGroupBox *timerWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;

    timerWidget->setTitle(QString(tr("Warmup timer")));

    m_timerCounter = new QLCDNumber;

    controlLayout->addWidget(m_timerCounter);
    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    controlLayout->addSpacerItem(spacer);

    m_timerStartCounter = new QPushButton;
    m_timerStartCounter->setText(tr("Start"));
    m_timerStopCounter = new QPushButton;
    m_timerStopCounter->setText(tr("Stop"));

    controlLayout->addWidget(m_timerStartCounter);
    controlLayout->addWidget(m_timerStopCounter);
    mainLayout->addLayout(controlLayout, 0, 0);

    timerWidget->setLayout(mainLayout);

    connect(m_timerStartCounter, &QPushButton::clicked, this, &RadarSerialControlUI::timerStartCounterClicked);
    connect(m_timerStopCounter, &QPushButton::clicked, this, &RadarSerialControlUI::timerStopCounterClicked);

    return timerWidget;
}

QWidget *RadarSerialControlUI::TriggerWidget()
{
    QGroupBox *triggerWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayout = new QHBoxLayout;

    triggerWidget->setTitle(QString(tr("Trigger")));

    m_triggerSet = new QPushButton;
    m_triggerSet->setText(tr("Set"));
    m_triggerGet = new QPushButton;
    m_triggerGet->setText(tr("Get"));

    QGridLayout *buttonsLayout = new QGridLayout;
    buttonsLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 0, 1, 2);
    buttonsLayout->addWidget(m_triggerSet, 1, 0);
    buttonsLayout->addWidget(m_triggerGet, 1, 1);
    controlLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    controlLayout->addLayout(buttonsLayout);

    QVBoxLayout *triggerEnableLayout = new QVBoxLayout;
    QLabel *triggerEnableLabel = new QLabel(tr("Enable"));
    triggerEnableLayout->addWidget(triggerEnableLabel);

    m_triggerEnable = new QComboBox;
    QStringList triggerEnable;
    triggerEnable.append(QString(tr("No")));
    triggerEnable.append(QString(tr("Yes")));
    m_triggerEnable->addItems(triggerEnable);
    m_triggerEnable->setCurrentIndex(m_radarTriggerSettings.getTriggerGenetatorState());
    triggerEnableLayout->addWidget(m_triggerEnable);

    QVBoxLayout *triggerStaggerLayout = new QVBoxLayout;
    QLabel *triggerStaggerLabel = new QLabel(tr("Stagger"));
    triggerStaggerLayout->addWidget(triggerStaggerLabel);

    m_triggerStaggerValue = new QSpinBox;
    m_triggerStaggerValue->setMaximum(15);
    m_triggerStaggerValue->setMinimum(0);
    m_triggerStaggerValue->setSingleStep(1);
    m_triggerStaggerValue->setValue(m_radarTriggerSettings.getTriggerStaggerValue());
    m_triggerStaggerValue->setSuffix(QString(tr("%")));
    triggerStaggerLayout->addWidget(m_triggerStaggerValue);

    QVBoxLayout *triggerPeriodLayout = new QVBoxLayout;
    QLabel *triggerPeriodLabel = new QLabel(tr("Period"));
    triggerPeriodLayout->addWidget(triggerPeriodLabel);

    m_triggerPeriod = new QComboBox;
    QStringList triggerPeriod;
    triggerPeriod.append(QString(tr("500 Hz")));
    triggerPeriod.append(QString(tr("800 Hz")));
    triggerPeriod.append(QString(tr("1600 Hz")));
    triggerPeriod.append(QString(tr("3200 Hz")));
    m_triggerPeriod->addItems(triggerPeriod);
    m_triggerPeriod->setCurrentIndex(m_radarTriggerSettings.getTriggerPeriod());
    triggerPeriodLayout->addWidget(m_triggerPeriod);

    settingsLayout->addLayout(triggerEnableLayout);
    settingsLayout->addLayout(triggerStaggerLayout);
    settingsLayout->addLayout(triggerPeriodLayout);

    mainLayout->addLayout(controlLayout, 1, 0);
    mainLayout->addLayout(settingsLayout, 0, 0);

    triggerWidget->setLayout(mainLayout);

    connect(m_triggerSet, &QPushButton::clicked, this, &RadarSerialControlUI::triggerSetClicked);
    connect(m_triggerGet, &QPushButton::clicked, this, &RadarSerialControlUI::triggerGetClicked);

    return triggerWidget;
}

QWidget *RadarSerialControlUI::MiscZeroWidget()
{
    QGroupBox *miscZeroWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;

    miscZeroWidget->setTitle(QString(tr("Misc 0")));

    QVBoxLayout *miscZeroTestACPLayout = new QVBoxLayout;
    QLabel *miscZeroTestACPLabel = new QLabel(tr("Test ACP"));
    miscZeroTestACPLayout->addWidget(miscZeroTestACPLabel);

    m_miscZeroTestACP = new QComboBox;
    QStringList miscZeroTestACP;
    miscZeroTestACP.append(QString(tr("Normal operation")));
    miscZeroTestACP.append(QString(tr("ACP/ARP internal genaration")));
    m_miscZeroTestACP->addItems(miscZeroTestACP);
    m_miscZeroTestACP->setCurrentIndex(m_radarMiscZeroSettings.getTestACP());
    miscZeroTestACPLayout->addWidget(m_miscZeroTestACP);

    QVBoxLayout *miscZeroAutotuneFineLayout = new QVBoxLayout;
    QLabel *miscZeroAutotuneFineLabel = new QLabel(tr("Autotune Fine"));
    miscZeroAutotuneFineLayout->addWidget(miscZeroAutotuneFineLabel);

    m_miscZeroAutoTineFine = new QComboBox;
    QStringList miscZeroAutotuneFine;
    miscZeroAutotuneFine.append(QString(tr("Disabled")));
    miscZeroAutotuneFine.append(QString(tr("Enabled")));
    m_miscZeroAutoTineFine->addItems(miscZeroAutotuneFine);
    m_miscZeroAutoTineFine->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneFine());
    miscZeroAutotuneFineLayout->addWidget(m_miscZeroAutoTineFine);

    settingsLayoutOne->addLayout(miscZeroTestACPLayout);
    controlLayout->addLayout(miscZeroAutotuneFineLayout);
    m_miscZeroSet = new QPushButton;
    m_miscZeroSet->setText(tr("Set"));
    m_miscZeroGet = new QPushButton;
    m_miscZeroGet->setText(tr("Get"));
    QGridLayout *buttonsLayout = new QGridLayout;
    buttonsLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 0, 1, 2);
    buttonsLayout->addWidget(m_miscZeroSet, 1, 0);
    buttonsLayout->addWidget(m_miscZeroGet, 1, 1);
    controlLayout->addLayout(buttonsLayout);

    QVBoxLayout *miscZeroAutotuneRegLayout = new QVBoxLayout;
    QLabel *miscZeroAutotuneRegLabel = new QLabel(tr("Autotune Reg"));
    miscZeroAutotuneRegLayout->addWidget(miscZeroAutotuneRegLabel);

    m_miscZeroAutoTuneReg = new QComboBox;
    QStringList miscZeroAutotuneReg;
    miscZeroAutotuneReg.append(QString(tr("Disabled")));
    miscZeroAutotuneReg.append(QString(tr("Enabled")));
    m_miscZeroAutoTuneReg->addItems(miscZeroAutotuneReg);
    m_miscZeroAutoTuneReg->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneReg());
    miscZeroAutotuneRegLayout->addWidget(m_miscZeroAutoTuneReg);

    QVBoxLayout *miscZeroDivACPLayout = new QVBoxLayout;
    QLabel *miscZeroDivACPLabel = new QLabel(tr("ACP Signal Division"));
    miscZeroDivACPLayout->addWidget(miscZeroDivACPLabel);

    m_miscZeroDivACP= new QComboBox;
    QStringList miscZeroDivACP;
    miscZeroDivACP.append(QString(tr("Not devided")));
    miscZeroDivACP.append(QString(tr("Devideded by 2")));
    miscZeroDivACP.append(QString(tr("Devideded by 4")));
    miscZeroDivACP.append(QString(tr("Devideded by 8")));
    m_miscZeroDivACP->addItems(miscZeroDivACP);
    m_miscZeroDivACP->setCurrentIndex(m_radarMiscZeroSettings.getDivideACPInputSignal());
    miscZeroDivACPLayout->addWidget(m_miscZeroDivACP);

    settingsLayoutTwo->addLayout(miscZeroAutotuneRegLayout);
    settingsLayoutTwo->addLayout(miscZeroDivACPLayout);

    mainLayout->addLayout(controlLayout, 2, 0);
    mainLayout->addLayout(settingsLayoutOne, 1, 0);
    mainLayout->addLayout(settingsLayoutTwo, 0, 0);

    miscZeroWidget->setLayout(mainLayout);

    connect(m_miscZeroSet, &QPushButton::clicked, this, &RadarSerialControlUI::miscZeroSetClicked);
    connect(m_miscZeroGet, &QPushButton::clicked, this, &RadarSerialControlUI::miscZeroGetClicked);

    return miscZeroWidget;
}

QWidget *RadarSerialControlUI::MiscOneWidget()
{
    QGroupBox *miscOneWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;

    miscOneWidget->setTitle(tr("Misc 1"));

    m_miscOneSet = new QPushButton;
    m_miscOneSet->setText(tr("Set"));
    m_miscOneGet = new QPushButton;
    m_miscOneGet->setText(tr("Get"));

    QVBoxLayout *miscOneAcpPeriodLayout = new QVBoxLayout;
    QLabel *miscOneAcpPeriodLabel = new QLabel(tr("ACP Period"));
    miscOneAcpPeriodLayout->addWidget(miscOneAcpPeriodLabel);

    m_miscOneAcpPeriod = new QComboBox;
    QStringList miscOneAcpPeriod;
    miscOneAcpPeriod.append(QString(tr("2048 pulses per revolution")));
    miscOneAcpPeriod.append(QString(tr("4096 pulses per revolution")));
    m_miscOneAcpPeriod->addItems(miscOneAcpPeriod);
    m_miscOneAcpPeriod->setCurrentIndex(m_radarMiscOneSettings.getAcpPeriod());
    miscOneAcpPeriodLayout->addWidget(m_miscOneAcpPeriod);

    QVBoxLayout *miscOneBlackingRamLayout = new QVBoxLayout;
    QLabel *miscOneBlackingRamLabel = new QLabel(tr("Blanking RAM"));
    miscOneBlackingRamLayout->addWidget(miscOneBlackingRamLabel);

    m_miscOneBlackingRam = new QComboBox;
    QStringList miscOneBlackingRam;
    miscOneBlackingRam.append(QString(tr("Block from 0 to 511")));
    miscOneBlackingRam.append(QString(tr("Block from 512 to 1023")));
    m_miscOneBlackingRam->addItems(miscOneBlackingRam);
    m_miscOneBlackingRam->setCurrentIndex(m_radarMiscOneSettings.getBlankingRam());
    miscOneBlackingRamLayout->addWidget(m_miscOneBlackingRam);

    QVBoxLayout *miscOneBandwidthLayout = new QVBoxLayout;
    QLabel *miscOneBandwidthLabel = new QLabel(tr("Bandwith"));
    miscOneBandwidthLayout->addWidget(miscOneBandwidthLabel);

    m_miscOneBandwidth = new QComboBox;
    QStringList miscOneBandwidth;
    miscOneBandwidth.append(QString(tr("Wide Band")));
    miscOneBandwidth.append(QString(tr("Narrow Band")));
    m_miscOneBandwidth->addItems(miscOneBandwidth);
    m_miscOneBandwidth->setCurrentIndex(m_radarMiscOneSettings.getBandwith());
    miscOneBandwidthLayout->addWidget(m_miscOneBandwidth);

    settingsLayoutOne->addLayout(miscOneAcpPeriodLayout);
    settingsLayoutTwo->addLayout(miscOneBlackingRamLayout);

    controlLayout->addLayout(miscOneBandwidthLayout);

    QGridLayout *buttonsLayout = new QGridLayout;
    buttonsLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 0, 1, 2);
    buttonsLayout->addWidget(m_miscOneSet, 1, 0);
    buttonsLayout->addWidget(m_miscOneGet, 1, 1);
    controlLayout->addLayout(buttonsLayout);

    mainLayout->addLayout(settingsLayoutOne, 0, 0);
    mainLayout->addLayout(settingsLayoutTwo, 1, 0);
    mainLayout->addLayout(controlLayout, 2, 0);

    miscOneWidget->setLayout(mainLayout);

    connect(m_miscOneSet, &QPushButton::clicked, this, &RadarSerialControlUI::miscOneSetClicked);
    connect(m_miscOneGet, &QPushButton::clicked, this, &RadarSerialControlUI::miscOneGetClicked);

    return miscOneWidget;
}

QWidget *RadarSerialControlUI::HlSetupWidget()
{
    QGroupBox *hlSetupWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;

    hlSetupWidget->setTitle(tr("HL Setup"));

    m_hlSetupSet = new QPushButton;
    m_hlSetupSet->setText(tr("Set"));
    m_hlSetupGet = new QPushButton;
    m_hlSetupGet->setText(tr("Get"));

    QGridLayout *buttonsLayout = new QGridLayout;
    buttonsLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 0, 1, 2);
    buttonsLayout->addWidget(m_hlSetupSet, 1, 0);
    buttonsLayout->addWidget(m_hlSetupGet, 1, 1);
    controlLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    controlLayout->addLayout(buttonsLayout);

    QVBoxLayout *hlSetupWidthLayout = new QVBoxLayout;
    QLabel *hlSetupWidthLabel = new QLabel(tr("Width"));
    hlSetupWidthLayout->addWidget(hlSetupWidthLabel);

    m_hlSetupWidth = new QComboBox;
    QStringList hlSetupWidth;
    hlSetupWidth.append(QString(tr("1/2 ACP Pulse")));
    hlSetupWidth.append(QString(tr("1 ACP Pulse")));
    hlSetupWidth.append(QString(tr("2 ACP Pulses")));
    hlSetupWidth.append(QString(tr("4 Acp Pulses")));
    m_hlSetupWidth->addItems(hlSetupWidth);
    m_hlSetupWidth->setCurrentIndex(m_radarHlSetupSettings.getWidth());
    hlSetupWidthLayout->addWidget(m_hlSetupWidth);

    settingsLayoutOne->addLayout(hlSetupWidthLayout);

    QVBoxLayout *hlSetupInvertLayout = new QVBoxLayout;
    QLabel *hlSetupInvertLabel = new QLabel(tr("Invert"));
    hlSetupInvertLayout->addWidget(hlSetupInvertLabel);

    m_hlSetupInvert = new QComboBox;
    QStringList hlSetupInvert;
    hlSetupInvert.append(QString(tr("Disabed")));
    hlSetupInvert.append(QString(tr("Enabled")));
    m_hlSetupInvert->addItems(hlSetupInvert);
    m_hlSetupInvert->setCurrentIndex(m_radarHlSetupSettings.getInvert());
    hlSetupInvertLayout->addWidget(m_hlSetupInvert);

    settingsLayoutOne->addLayout(hlSetupInvertLayout);

    QVBoxLayout *hlSetupEdgeLayout = new QVBoxLayout;
    QLabel *hlSetupEdgeLabel = new QLabel(tr("Edge"));
    hlSetupEdgeLayout->addWidget(hlSetupEdgeLabel);

    m_hlSetupEdge = new QComboBox;
    QStringList hlSetupEdge;
    hlSetupEdge.append(QString(tr("Falling")));
    hlSetupEdge.append(QString(tr("Rising")));
    m_hlSetupEdge->addItems(hlSetupEdge);
    m_hlSetupEdge->setCurrentIndex(m_radarHlSetupSettings.getEdge());
    hlSetupEdgeLayout->addWidget(m_hlSetupEdge);

    settingsLayoutTwo->addLayout(hlSetupEdgeLayout);

    QVBoxLayout *hlSetupDelayLayout = new QVBoxLayout;
    QLabel *hlSetupDelayLabel = new QLabel(tr("Delay"));
    hlSetupDelayLayout->addWidget(hlSetupDelayLabel);

    m_hlSetupDelay = new QSpinBox;
    m_hlSetupDelay->setValue(m_radarHlSetupSettings.getDelayValue());
    m_hlSetupDelay->setMinimum(0);
    m_hlSetupDelay->setMaximum(4095);
    hlSetupDelayLayout->addWidget(m_hlSetupDelay);

    settingsLayoutTwo->addLayout(hlSetupDelayLayout);

    mainLayout->addLayout(settingsLayoutOne, 1, 0);
    mainLayout->addLayout(settingsLayoutTwo, 2, 0);
    mainLayout->addLayout(controlLayout, 3, 0);

    hlSetupWidget->setLayout(mainLayout);

    connect(m_hlSetupSet, &QPushButton::clicked, this, &RadarSerialControlUI::hlSetupSetClicked);
    connect(m_hlSetupGet, &QPushButton::clicked, this, &RadarSerialControlUI::hlSetupGetClicked);

    return hlSetupWidget;
}

QWidget *RadarSerialControlUI::EnableWidget()
{
    QGroupBox *enableWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayout = new QHBoxLayout;

    enableWidget->setTitle(tr("Enable"));

    m_enableSet = new QPushButton;
    m_enableSet->setText(tr("Set"));
    m_enableGet = new QPushButton;
    m_enableGet->setText(tr("Get"));

    QVBoxLayout *enableAutoTuneLayout = new QVBoxLayout;
    QLabel *enableAutoTuneLabel = new QLabel(tr("Autotune"));
    enableAutoTuneLayout->addWidget(enableAutoTuneLabel);

    m_enableAutoTune = new QComboBox;
    QStringList enableAutoTune;
    enableAutoTune.append(QString(tr("Disabled")));
    enableAutoTune.append(QString(tr("Enabled")));
    m_enableAutoTune->addItems(enableAutoTune);
    m_enableAutoTune->setCurrentIndex(m_radarEnableSettings.getAutoTune());
    enableAutoTuneLayout->addWidget(m_enableAutoTune);

    controlLayout->addLayout(enableAutoTuneLayout);

    QGridLayout *buttonsLayout = new QGridLayout;
    buttonsLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 0, 1, 2);
    buttonsLayout->addWidget(m_enableSet, 1, 0);
    buttonsLayout->addWidget(m_enableGet, 1, 1);
    controlLayout->addLayout(buttonsLayout);

    QVBoxLayout *enableDALayout = new QVBoxLayout;
    QLabel *enableDALabel = new QLabel(tr("DAC"));
    enableDALayout->addWidget(enableDALabel);

    m_enableDA = new QComboBox;
    QStringList enableDA;
    enableDA.append(QString(tr("Disabled")));
    enableDA.append(QString(tr("Enabled")));
    m_enableDA->addItems(enableDA);
    m_enableDA->setCurrentIndex(m_radarEnableSettings.getDac());
    enableDALayout->addWidget(m_enableDA);

    settingsLayout->addLayout(enableDALayout);

    QVBoxLayout *enableADLayout = new QVBoxLayout;
    QLabel *enableADLabel = new QLabel(tr("ADC"));
    enableADLayout->addWidget(enableADLabel);

    m_enableAD = new QComboBox;
    QStringList enableAD;
    enableAD.append(QString(tr("Disabled")));
    enableAD.append(QString(tr("Enabled")));
    m_enableAD->addItems(enableAD);
    m_enableAD->setCurrentIndex(m_radarEnableSettings.getAdc());
    enableADLayout->addWidget(m_enableAD);

    settingsLayout->addLayout(enableADLayout);

    mainLayout->addLayout(settingsLayout, 0, 0);
    mainLayout->addLayout(controlLayout, 1, 0);

    enableWidget->setLayout(mainLayout);

    connect(m_enableSet, &QPushButton::clicked, this, &RadarSerialControlUI::enableSetClicked);
    connect(m_enableGet, &QPushButton::clicked, this, &RadarSerialControlUI::enableGetClicked);

    return enableWidget;
}

QWidget *RadarSerialControlUI::TuneCoarseWidget()
{
    QGroupBox *tuneCoarseWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;

    tuneCoarseWidget->setTitle(tr("Tune Coarse"));

    QVBoxLayout *tuneCoarseValueLayout = new QVBoxLayout;

    m_tuneCoarseValue = new QSpinBox;
    m_tuneCoarseValue->setValue(m_radarTuneCoarseSettings.m_coarseValue);
    m_tuneCoarseValue->setMinimum(0);
    m_tuneCoarseValue->setMaximum(255);
    tuneCoarseValueLayout->addWidget(m_tuneCoarseValue);

    controlLayout->addLayout(tuneCoarseValueLayout);

    m_tuneCoatseSet = new QPushButton;
    m_tuneCoatseSet->setText(tr("Set"));
    m_tuneCoarseGet = new QPushButton;
    m_tuneCoarseGet->setText(tr("Get"));

    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    controlLayout->addSpacerItem(spacer);
    controlLayout->addWidget(m_tuneCoatseSet);
    controlLayout->addWidget(m_tuneCoarseGet);

    mainLayout->addLayout(controlLayout, 0, 0);

    tuneCoarseWidget->setLayout(mainLayout);

    connect(m_tuneCoatseSet, &QPushButton::clicked, this, &RadarSerialControlUI::tuneCoatseSetClicked);
    connect(m_tuneCoarseGet, &QPushButton::clicked, this, &RadarSerialControlUI::tuneCoarseGetClicked);

    return tuneCoarseWidget;
}

QWidget *RadarSerialControlUI::TuneFineWidget()
{
    QGroupBox *tuneFineWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;

    tuneFineWidget->setTitle(tr("Tune Fine"));

    QVBoxLayout *tuneFineValueLayout = new QVBoxLayout;

    m_tuneFineValue = new QSpinBox;
    m_tuneFineValue->setValue(m_radarTuneFineSettings.m_fineValue);
    m_tuneFineValue->setMinimum(0);
    m_tuneFineValue->setMaximum(255);
    tuneFineValueLayout->addWidget(m_tuneFineValue);

    controlLayout->addLayout(tuneFineValueLayout);

    m_tuneFineSet = new QPushButton;
    m_tuneFineSet->setText(tr("Set"));
    m_tuneFineGet = new QPushButton;
    m_tuneFineGet->setText(tr("Get"));

    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    controlLayout->addSpacerItem(spacer);
    controlLayout->addWidget(m_tuneFineSet);
    controlLayout->addWidget(m_tuneFineGet);

    mainLayout->addLayout(controlLayout, 0, 0);

    tuneFineWidget->setLayout(mainLayout);

    connect(m_tuneFineSet, &QPushButton::clicked, this, &RadarSerialControlUI::tuneFineSetClicked);
    connect(m_tuneFineGet, &QPushButton::clicked, this, &RadarSerialControlUI::tuneFineGetClicked);

    return tuneFineWidget;
}

QWidget *RadarSerialControlUI::TuneRegWidget()
{
    QGroupBox *tuneRegWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;

    tuneRegWidget->setTitle(tr("Tune Reg"));

    QVBoxLayout *tuneRegValueLayout = new QVBoxLayout;

    m_tuneRegValue = new QSpinBox;
    m_tuneRegValue->setValue(m_radarTuneRegSettings.m_regValue);
    m_tuneRegValue->setMinimum(0);
    m_tuneRegValue->setMaximum(255);
    tuneRegValueLayout->addWidget(m_tuneRegValue);

    controlLayout->addLayout(tuneRegValueLayout);

    m_tuneRegSet = new QPushButton;
    m_tuneRegSet->setText(tr("Set"));
    m_tuneRegGet = new QPushButton;
    m_tuneRegGet->setText(tr("Get"));

    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    controlLayout->addSpacerItem(spacer);
    controlLayout->addWidget(m_tuneRegSet);
    controlLayout->addWidget(m_tuneRegGet);

    mainLayout->addLayout(settingsLayoutOne, 1, 0);
    mainLayout->addLayout(settingsLayoutTwo, 2, 0);
    mainLayout->addLayout(controlLayout, 0, 0);

    tuneRegWidget->setLayout(mainLayout);

    connect(m_tuneRegSet, &QPushButton::clicked, this, &RadarSerialControlUI::tuneRegSetClicked);
    connect(m_tuneRegGet, &QPushButton::clicked, this, &RadarSerialControlUI::tuneRegGetClicked);

    return tuneRegWidget;
}

QWidget *RadarSerialControlUI::SetupWidget()
{
    QGroupBox *setupWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayout = new QHBoxLayout;

    setupWidget->setTitle(tr("Setup"));

    m_setupSet = new QPushButton;
    m_setupSet->setText(tr("Set"));
    m_setupGet = new QPushButton;
    m_setupGet->setText(tr("Get"));

    QVBoxLayout *setupTxLayout = new QVBoxLayout;
    QLabel *setupTxLabel = new QLabel(tr("Tranceiver"));
    setupTxLayout->addWidget(setupTxLabel);

    m_setupTx = new QComboBox;
    QStringList setupTx;
    setupTx.append(QString(tr("StandBy")));
    setupTx.append(QString(tr("Tx Enabled")));
    m_setupTx->addItems(setupTx);
    m_setupTx->setCurrentIndex(m_radarSetupSettings.getTransceiverMode());
    setupTxLayout->addWidget(m_setupTx);

    settingsLayout->addLayout(setupTxLayout);

    QVBoxLayout *setupPulseWidthLayout = new QVBoxLayout;
    QLabel *setupPulseWidthLabel = new QLabel(tr("Pulse widgth"));
    setupPulseWidthLayout->addWidget(setupPulseWidthLabel);

    m_setupPulseWidth = new QComboBox;
    QStringList setupPulseWidth;
    setupPulseWidth.append(QString(tr("XL Pulse")));
    setupPulseWidth.append(QString(tr("L Pulse")));
    setupPulseWidth.append(QString(tr("M Pulse")));
    setupPulseWidth.append(QString(tr("S Pulse")));
    m_setupPulseWidth->addItems(setupPulseWidth);
    m_setupPulseWidth->setCurrentIndex(m_radarSetupSettings.getPulseMode());
    setupPulseWidthLayout->addWidget(m_setupPulseWidth);

    controlLayout->addLayout(setupPulseWidthLayout);

    QGridLayout *buttonsLayout = new QGridLayout;
    buttonsLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 0, 1, 2);
    buttonsLayout->addWidget(m_setupSet, 1, 0);
    buttonsLayout->addWidget(m_setupGet, 1, 1);
    controlLayout->addLayout(buttonsLayout);

    QVBoxLayout *setupMotorLayout = new QVBoxLayout;
    QLabel *setupMotorLabel = new QLabel(tr("Motor"));
    setupMotorLayout->addWidget(setupMotorLabel);

    m_setupMotor = new QComboBox;
    QStringList setupMotor;
    setupMotor.append(QString(tr("Disabled")));
    setupMotor.append(QString(tr("Enabled")));
    m_setupMotor->addItems(setupMotor);
    m_setupMotor->setCurrentIndex(m_radarSetupSettings.getEnableMotor());
    setupMotorLayout->addWidget(m_setupMotor);

    settingsLayout->addLayout(setupMotorLayout);

    mainLayout->addLayout(settingsLayout, 0, 0);
    mainLayout->addLayout(controlLayout, 1, 0);

    setupWidget->setLayout(mainLayout);

    connect(m_setupSet, &QPushButton::clicked, this, &RadarSerialControlUI::setupSetClicked);
    connect(m_setupGet, &QPushButton::clicked, this, &RadarSerialControlUI::setupGetClicked);

    return setupWidget;
}

QWidget *RadarSerialControlUI::IndicatorWidget()
{
    QGroupBox *mainWidget = new QGroupBox(QString("Indicators"));

    QVBoxLayout *mainLayout = new QVBoxLayout;

    QVBoxLayout *autotuneFineLayout = new QVBoxLayout;
    QHBoxLayout *autotuneFineControlLayout = new QHBoxLayout;

    autotuneFineLayout->addWidget(new QLabel(QString(tr("Autotune Fine"))));

    m_autotuneFineValue = new QSpinBox;
    m_autotuneFineValue->setValue(0);
    m_autotuneFineValue->setMinimum(0);
    m_autotuneFineValue->setMaximum(255);
    m_autotuneFineValue->setReadOnly(true);
    m_autotuneFineValue->setButtonSymbols(QSpinBox::NoButtons);
    autotuneFineControlLayout->addWidget(m_autotuneFineValue);

    m_autotuneFineGet = new QPushButton;
    m_autotuneFineGet->setText(tr("Get"));

    autotuneFineControlLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    autotuneFineControlLayout->addWidget(m_autotuneFineGet);

    autotuneFineLayout->addLayout(autotuneFineControlLayout);
    connect(m_autotuneFineGet, &QPushButton::clicked, this, &RadarSerialControlUI::autotuneFineGetClicked);

    QVBoxLayout *autotuneRegLayout = new QVBoxLayout;
    QHBoxLayout *autotuneRegControlLayout = new QHBoxLayout;

    autotuneRegLayout->addWidget(new QLabel(QString(tr("Autotune Reg"))));

    m_autotuneRegValue = new QSpinBox;
    m_autotuneRegValue->setValue(0);
    m_autotuneRegValue->setMinimum(0);
    m_autotuneRegValue->setMaximum(255);
    m_autotuneRegValue->setReadOnly(true);
    m_autotuneRegValue->setButtonSymbols(QSpinBox::NoButtons);
    autotuneRegControlLayout->addWidget(m_autotuneRegValue);

    m_autotuneRegGet = new QPushButton;
    m_autotuneRegGet->setText(tr("Get"));

    autotuneRegControlLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    autotuneRegControlLayout->addWidget(m_autotuneRegGet);

    autotuneRegLayout->addLayout(autotuneRegControlLayout);
    connect(m_autotuneRegGet, &QPushButton::clicked, this, &RadarSerialControlUI::autotuneRegGetClicked);

    QVBoxLayout *magIndicatorLayout = new QVBoxLayout;
    QHBoxLayout *magIndicatorControlLayout = new QHBoxLayout;

    magIndicatorLayout->addWidget(new QLabel(QString(tr("Mag Indicator"))));

    m_magIndidcatorValue = new QSpinBox;
    m_magIndidcatorValue->setValue(0);
    m_magIndidcatorValue->setMinimum(0);
    m_magIndidcatorValue->setMaximum(255);
    m_magIndidcatorValue->setReadOnly(true);
    m_magIndidcatorValue->setButtonSymbols(QSpinBox::NoButtons);
    magIndicatorControlLayout->addWidget(m_magIndidcatorValue);

    m_magIndicatorGet = new QPushButton;
    m_magIndicatorGet->setText(tr("Get"));

    magIndicatorControlLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    magIndicatorControlLayout->addWidget(m_magIndicatorGet);

    magIndicatorLayout->addLayout(magIndicatorControlLayout);
    connect(m_magIndicatorGet, &QPushButton::clicked, this, &RadarSerialControlUI::magIndicatorGetClicked);

    QVBoxLayout *tuneIndicatorLayout = new QVBoxLayout;
    QHBoxLayout *tuneIndicatorControlLayout = new QHBoxLayout;

    tuneIndicatorLayout->addWidget(new QLabel(QString(tr("Tune Indicator"))));

    m_tuneIndicatorValue = new QSpinBox;
    m_tuneIndicatorValue->setValue(0);
    m_tuneIndicatorValue->setMinimum(0);
    m_tuneIndicatorValue->setMaximum(255);
    m_tuneIndicatorValue->setReadOnly(true);
    m_tuneIndicatorValue->setButtonSymbols(QSpinBox::NoButtons);
    tuneIndicatorControlLayout->addWidget(m_tuneIndicatorValue);

    m_tuneIndicatorGet = new QPushButton;
    m_tuneIndicatorGet->setText(tr("Get"));

    tuneIndicatorControlLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    tuneIndicatorControlLayout->addWidget(m_tuneIndicatorGet);

    tuneIndicatorLayout->addLayout(tuneIndicatorControlLayout);
    connect(m_tuneIndicatorGet, &QPushButton::clicked, this, &RadarSerialControlUI::tuneIndicatorGetClicked);

    mainLayout->addLayout(autotuneFineLayout);
    mainLayout->addLayout(autotuneRegLayout);
    mainLayout->addLayout(magIndicatorLayout);
    mainLayout->addLayout(tuneIndicatorLayout);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *RadarSerialControlUI::StatusWidget()
{
    QGroupBox *statusWidget = new QGroupBox;

    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;
    QHBoxLayout *settingsLayoutOne = new QHBoxLayout;
    QHBoxLayout *settingsLayoutTwo = new QHBoxLayout;
    QHBoxLayout *settingsLayoutThree = new QHBoxLayout;

    statusWidget->setTitle(tr("Status"));

    m_statusGet = new QPushButton;
    m_statusGet->setText(tr("Get"));

    QVBoxLayout *buttonsLayout = new QVBoxLayout;
    buttonsLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    buttonsLayout->addWidget(m_statusGet);
    controlLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    controlLayout->addLayout(buttonsLayout);

    QVBoxLayout *statusWarmupLayout = new QVBoxLayout;
    QLabel *statusWarmupLabel = new QLabel(tr("Warmup"));
    statusWarmupLayout->addWidget(statusWarmupLabel);

    m_statusWarmup = new QComboBox;
    QStringList statusWarmup;
    statusWarmup.append(QString(tr("Warmup Mode")));
    statusWarmup.append(QString(tr("Ready to Transmit")));
    m_statusWarmup->addItems(statusWarmup);
    m_statusWarmup->setCurrentIndex(0);
    m_statusWarmup->setEditable(false);
    statusWarmupLayout->addWidget(m_statusWarmup);

    settingsLayoutOne->addLayout(statusWarmupLayout);

    QVBoxLayout *statusAutotuneRegLayout = new QVBoxLayout;
    QLabel *statusAutotuneRegLabel = new QLabel(tr("Autotune Reg"));
    statusAutotuneRegLayout->addWidget(statusAutotuneRegLabel);

    m_statusAutotuneReg = new QComboBox;
    QStringList statusAutotuneReg;
    statusAutotuneReg.append(QString(tr("Not Completed")));
    statusAutotuneReg.append(QString(tr("Completed")));
    m_statusAutotuneReg->addItems(statusAutotuneReg);
    m_statusAutotuneReg->setCurrentIndex(0);
    m_statusAutotuneReg->setEditable(false);
    statusAutotuneRegLayout->addWidget(m_statusAutotuneReg);

    settingsLayoutOne->addLayout(statusAutotuneRegLayout);

    QVBoxLayout *statusAutotuneFineLayout = new QVBoxLayout;
    QLabel *statusAutotuneFineLabel = new QLabel(tr("Autotune Fine"));
    statusAutotuneFineLayout->addWidget(statusAutotuneFineLabel);

    m_statusAutotuneFine = new QComboBox;
    QStringList statusAutotuneFine;
    statusAutotuneFine.append(QString(tr("Not Completed")));
    statusAutotuneFine.append(QString(tr("Completed")));
    m_statusAutotuneFine->addItems(statusAutotuneFine);
    m_statusAutotuneFine->setCurrentIndex(0);
    m_statusAutotuneFine->setEditable(false);
    statusAutotuneFineLayout->addWidget(m_statusAutotuneFine);

    settingsLayoutTwo->addLayout(statusAutotuneFineLayout);

    QVBoxLayout *statusAcpInputSignalLayout = new QVBoxLayout;
    QLabel *statusAcpInputSignalLabel = new QLabel(tr("ACP"));
    statusAcpInputSignalLayout->addWidget(statusAcpInputSignalLabel);

    m_statusAcpInputSignal = new QComboBox;
    QStringList statusAcpInputSignal;
    statusAcpInputSignal.append(QString(tr("Input Signal Ok")));
    statusAcpInputSignal.append(QString(tr("Input Signal Fail")));
    m_statusAcpInputSignal->addItems(statusAcpInputSignal);
    m_statusAcpInputSignal->setCurrentIndex(0);
    m_statusAcpInputSignal->setEditable(false);
    statusAcpInputSignalLayout->addWidget(m_statusAcpInputSignal);

    settingsLayoutTwo->addLayout(statusAcpInputSignalLayout);

    QVBoxLayout *statusHlInputSignalLayout = new QVBoxLayout;
    QLabel *statusHlInputSignalLabel = new QLabel(tr("HL"));
    statusHlInputSignalLayout->addWidget(statusHlInputSignalLabel);

    m_statusHlInputSignal = new QComboBox;
    QStringList statusHlInputSignal;
    statusHlInputSignal.append(QString(tr("Input Signal Ok")));
    statusHlInputSignal.append(QString(tr("Input Signal Fail")));
    m_statusHlInputSignal->addItems(statusHlInputSignal);
    m_statusHlInputSignal->setCurrentIndex(0);
    m_statusHlInputSignal->setEditable(false);
    statusHlInputSignalLayout->addWidget(m_statusHlInputSignal);

    settingsLayoutThree->addLayout(statusHlInputSignalLayout);

    QVBoxLayout *statusTriggerInpusSignalLayout = new QVBoxLayout;
    QLabel *statusTriggerInpusSignalLabel = new QLabel(tr("Trigger"));
    statusTriggerInpusSignalLayout->addWidget(statusTriggerInpusSignalLabel);

    m_statusTriggerInputSignal = new QComboBox;
    QStringList statusTriggerInpusSignal;
    statusTriggerInpusSignal.append(QString(tr("Input Signal Ok")));
    statusTriggerInpusSignal.append(QString(tr("Input Signal Fail")));
    m_statusTriggerInputSignal->addItems(statusTriggerInpusSignal);
    m_statusTriggerInputSignal->setCurrentIndex(0);
    m_statusTriggerInputSignal->setEditable(false);
    statusTriggerInpusSignalLayout->addWidget(m_statusTriggerInputSignal);

    settingsLayoutThree->addLayout(statusTriggerInpusSignalLayout);

    mainLayout->addLayout(settingsLayoutOne, 0, 0);
    mainLayout->addLayout(settingsLayoutTwo, 1, 0);
    mainLayout->addLayout(settingsLayoutThree, 2, 0);    
    mainLayout->addLayout(controlLayout, 3, 0);

    statusWidget->setLayout(mainLayout);

    connect(m_statusGet, &QPushButton::clicked, this, &RadarSerialControlUI::statusGetClicked);

    return statusWidget;
}

QWidget *RadarSerialControlUI::SerialPortWidget()
{
    QGroupBox *mainWidget = new QGroupBox(QString(tr("SerialPort Settings")));

    QHBoxLayout *mainlayout = new QHBoxLayout;

    QLabel *portLabel = new QLabel(QString(tr("Port Address")));
    mainlayout->addWidget(portLabel);

    m_portName = new QLineEdit;
    m_portName->setDisabled(true);
    m_portTip = m_serialPortSettings->getToolTip();
    m_portName->setToolTip(m_portTip);
    mainlayout->addWidget(m_portName);

    m_portManualCheck = new QCheckBox(QString(tr("Manual")));
    m_portManualCheck->setCheckState(Qt::Unchecked);
    connect(m_portManualCheck, &QCheckBox::clicked, this, &RadarSerialControlUI::SlotPortManualCheckClicked);
    mainlayout->addWidget(m_portManualCheck);

    QSpacerItem *spacer = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    mainlayout->addItem(spacer);

    m_portSettings = new QPushButton(QString(tr("Settings")));
    connect(m_portSettings, &QPushButton::clicked, this, &RadarSerialControlUI::SlotPortSettingsClicked);
    mainlayout->addWidget(m_portSettings);

    m_setPort = new QPushButton(QString(tr("Set")));
    m_setPort->setToolTip(m_portTip);
    connect(m_setPort, &QPushButton::clicked, this, &RadarSerialControlUI::SlotSetPortClicked);
    mainlayout->addWidget(m_setPort);

    mainWidget->setLayout(mainlayout);

    return mainWidget;
}

QWidget *RadarSerialControlUI::GeneralWidget()
{
    QGroupBox *mainWidget = new QGroupBox(QString(tr("General")));

    QHBoxLayout *mainLayout = new QHBoxLayout;

    m_setAll = new QPushButton(QString(tr("Set All")));
    connect(m_setAll, &QPushButton::clicked, this, &RadarSerialControlUI::SlotSetAll);
    mainLayout->addWidget(m_setAll);

    m_getAll = new QPushButton(QString(tr("Get All")));
    connect(m_getAll, &QPushButton::clicked, this, &RadarSerialControlUI::SlotGetAll);
    mainLayout->addWidget(m_getAll);

    m_resetSettings = new QPushButton(QString(tr("Reset")));
    connect(m_resetSettings, &QPushButton::clicked, this, &RadarSerialControlUI::SlotResetSettingsClicked);
    mainLayout->addWidget(m_resetSettings);

    m_defaultSettings = new QPushButton(QString(tr("Default")));
    connect(m_defaultSettings, &QPushButton::clicked, this, &RadarSerialControlUI::SlotDefaultSettingsClicked);
    mainLayout->addWidget(m_defaultSettings);

    mainWidget->setLayout(mainLayout);
    mainWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    return mainWidget;
}

void RadarSerialControlUI::timerStartCounterClicked()
{
    m_radarCommunicationSerial->startWorkingThread();
}

void RadarSerialControlUI::timerStopCounterClicked()
{
    m_radarCommunicationSerial->stopWorkingThread();
}

void RadarSerialControlUI::timerCounterValue(int number)
{
    m_timerCounter->display(number);
    if ((number % 50) == 0) {
        SlotGetAndSetDefaults();
    }
}

void RadarSerialControlUI::warmupIsComplete(bool warmupStatus)
{
    SlotAllowSet(warmupStatus);
    if (warmupStatus) {
        m_timerCounter->display("ready");
        SlotGetAndSetDefaults();
    }
}

void RadarSerialControlUI::triggerSetClicked()
{
    m_radarTriggerSettings.setTriggerGenetatorState(m_triggerEnable->currentIndex());
    m_radarTriggerSettings.setTriggerStaggerValue(m_triggerStaggerValue->value());
    m_radarTriggerSettings.setTriggerPeriod(m_triggerPeriod->currentIndex());
    m_radarCommunicationSerial->setRegisters(RadarMessageSerial::triggerMessage(m_radarTriggerSettings));
}

void RadarSerialControlUI::triggerGetClicked()
{
    QByteArray triggerStatus = m_radarCommunicationSerial->getRegisters(RadarMessageSerial::triggerCheck());
    if (!triggerStatus.isEmpty() && (triggerStatus.length() == 3)) {
        m_radarTriggerSettings = RadarTriggerRS(triggerStatus);
        m_triggerEnable->setCurrentIndex(m_radarTriggerSettings.getTriggerGenetatorState());
        m_triggerStaggerValue->setValue(m_radarTriggerSettings.getTriggerStaggerValue());
        m_triggerPeriod->setCurrentIndex(m_radarTriggerSettings.getTriggerPeriod());
    }
}

void RadarSerialControlUI::miscZeroSetClicked()
{
    m_radarMiscZeroSettings.setAutotuneFine(m_miscZeroAutoTineFine->currentIndex());
    m_radarMiscZeroSettings.setAutotuneReg(m_miscZeroAutoTuneReg->currentIndex());
    m_radarMiscZeroSettings.setTestACP(m_miscZeroTestACP->currentIndex());
    m_radarMiscZeroSettings.setDivideACPInputSignal(m_miscZeroDivACP->currentIndex());
    m_radarCommunicationSerial->setRegister(RadarAddressBytesRS::Misc_0, m_radarMiscZeroSettings.radarMiscZero());
}

void RadarSerialControlUI::miscZeroGetClicked()
{
    unsigned char answer = m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::Misc_0);
    m_radarMiscZeroSettings = RadarMiscZeroRS(answer);
    m_miscZeroAutoTineFine->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneFine());
    m_miscZeroAutoTuneReg->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneReg());
    m_miscZeroTestACP->setCurrentIndex(m_radarMiscZeroSettings.getTestACP());
    m_miscZeroDivACP->setCurrentIndex(m_radarMiscZeroSettings.getDivideACPInputSignal());
}

void RadarSerialControlUI::miscOneSetClicked()
{
    m_radarMiscOneSettings.setAcpPeriod(m_miscOneAcpPeriod->currentIndex());
    m_radarMiscOneSettings.setBlankingRam(m_miscOneBlackingRam->currentIndex());
    m_radarMiscOneSettings.setBandwith(m_miscOneBandwidth->currentIndex());
    m_radarCommunicationSerial->setRegister(RadarAddressBytesRS::Misc_1, m_radarMiscOneSettings.radarMiscOne());
}

void RadarSerialControlUI::miscOneGetClicked()
{
    unsigned char answer = m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::Misc_1);
    m_radarMiscOneSettings = RadarMiscOneRS(answer);
    m_miscOneAcpPeriod->setCurrentIndex(m_radarMiscOneSettings.getAcpPeriod());
    m_miscOneBlackingRam->setCurrentIndex(m_radarMiscOneSettings.getBlankingRam());
    m_miscOneBandwidth->setCurrentIndex(m_radarMiscOneSettings.getBandwith());
}

void RadarSerialControlUI::hlSetupSetClicked()
{
    m_radarHlSetupSettings.setWidth(m_hlSetupWidth->currentIndex());
    m_radarHlSetupSettings.setInvert(m_hlSetupInvert->currentIndex());
    m_radarHlSetupSettings.setEdge(m_hlSetupEdge->currentIndex());
    m_radarHlSetupSettings.setDelayValue(m_hlSetupDelay->value());
    m_radarCommunicationSerial->setRegisters(RadarMessageSerial::hlMessage(m_radarHlSetupSettings));
}

void RadarSerialControlUI::hlSetupGetClicked()
{
    QByteArray hlSetupStatus = m_radarCommunicationSerial->getRegisters(RadarMessageSerial::hlCheck());
    if (!hlSetupStatus.isEmpty() && hlSetupStatus.size() == 2) {
        m_radarHlSetupSettings = RadarHlSetupRS(hlSetupStatus);
        m_hlSetupWidth->setCurrentIndex(m_radarHlSetupSettings.getWidth());
        m_hlSetupInvert->setCurrentIndex(m_radarHlSetupSettings.getInvert());
        m_hlSetupEdge->setCurrentIndex(m_radarHlSetupSettings.getEdge());
        m_hlSetupDelay->setValue(m_radarHlSetupSettings.getDelayValue());
    }
}

void RadarSerialControlUI::enableSetClicked()
{
    m_radarEnableSettings.setAutoTune(m_enableAutoTune->currentIndex());
    m_radarEnableSettings.setDac(m_enableDA->currentIndex());
    m_radarEnableSettings.setAdc(m_enableAD->currentIndex());
    m_radarCommunicationSerial->setRegister(RadarAddressBytesRS::Enable, m_radarEnableSettings.radarEnableRS());
}

void RadarSerialControlUI::enableGetClicked()
{
    unsigned char answer = m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::Enable);
    m_radarEnableSettings = RadarEnableRS(answer);
    m_enableAutoTune->setCurrentIndex(m_radarEnableSettings.getAutoTune());
    m_enableDA->setCurrentIndex(m_radarEnableSettings.getDac());
    m_enableAD->setCurrentIndex(m_radarEnableSettings.getAdc());
}

void RadarSerialControlUI::tuneCoatseSetClicked()
{
    m_radarTuneCoarseSettings = RadarTuneCoarseRS(static_cast<quint8>(m_tuneCoarseValue->value()));
    m_radarCommunicationSerial->setRegister(RadarAddressBytesRS::TuneCoarse, m_radarTuneCoarseSettings.radarTuneCoarseRS());
}

void RadarSerialControlUI::tuneCoarseGetClicked()
{
    m_radarTuneCoarseSettings.m_coarseValue = m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::TuneCoarse);
    m_tuneCoarseValue->setValue(m_radarTuneCoarseSettings.m_coarseValue);
}

void RadarSerialControlUI::tuneFineSetClicked()
{
    m_radarTuneFineSettings = RadarTuneFineRS(static_cast<quint8>(m_tuneFineValue->value()));
    m_radarCommunicationSerial->setRegister(RadarAddressBytesRS::TuneFine, m_radarTuneFineSettings.radarTuneFineRS());
}

void RadarSerialControlUI::tuneFineGetClicked()
{
    m_radarTuneFineSettings.m_fineValue = m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::TuneFine);
    m_tuneFineValue->setValue(m_radarTuneFineSettings.m_fineValue);
}

void RadarSerialControlUI::tuneRegSetClicked()
{
    m_radarTuneRegSettings = RadarTuneRegRS(static_cast<quint8>(m_tuneRegValue->value()));
    m_radarCommunicationSerial->setRegister(RadarAddressBytesRS::TuneReg, m_radarTuneRegSettings.radarTuneRegRS());
}

void RadarSerialControlUI::tuneRegGetClicked()
{
    m_radarTuneRegSettings.m_regValue = m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::TuneReg);
    m_tuneRegValue->setValue(m_radarTuneRegSettings.m_regValue);
}

void RadarSerialControlUI::setupSetClicked()
{
    m_radarSetupSettings.setTransceiverMode(m_setupTx->currentIndex());
    m_radarSetupSettings.setPulseMode(m_setupPulseWidth->currentIndex());
    m_radarSetupSettings.setEnableMotor(m_setupMotor->currentIndex());
    m_radarCommunicationSerial->setRegister(RadarAddressBytesRS::Setup, m_radarSetupSettings.radarSetupRS());
}

void RadarSerialControlUI::setupGetClicked()
{
    unsigned char answer = m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::Setup);
    m_radarSetupSettings = RadarSetupRS(answer);
    m_setupTx->setCurrentIndex(m_radarSetupSettings.getTransceiverMode());
    m_setupPulseWidth->setCurrentIndex(m_radarSetupSettings.getPulseMode());
    m_setupMotor->setCurrentIndex(m_radarSetupSettings.getEnableMotor());
}

void RadarSerialControlUI::autotuneFineGetClicked()
{
    m_autotuneFineValue->setValue(m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::AutoTuneFine));
}

void RadarSerialControlUI::autotuneRegGetClicked()
{
    m_autotuneRegValue->setValue(m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::AutotuneReg));
}

void RadarSerialControlUI::magIndicatorGetClicked()
{
    m_magIndidcatorValue->setValue(m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::MAGIndicator));
}

void RadarSerialControlUI::tuneIndicatorGetClicked()
{
    m_tuneIndicatorValue->setValue(m_radarCommunicationSerial->getRegister(RadarAddressBytesRS::TuneIndicator));
}

void RadarSerialControlUI::statusGetClicked()
{
    RadarMessageSerial message(RadarHeaderRS(RadarHeaderDirectionRS::HostToSub, RadarHeaderOperationRS::ReadFromReg), RadarAddressBytesRS::Status);
    QByteArray radarStatus = m_radarCommunicationSerial->sendReceiveMessage(message.message());
    if (!radarStatus.isEmpty()) {
        RadarStatusRS status(radarStatus[2]);
        m_statusWarmup->setCurrentIndex(status.getWarmupComplete());
        m_statusAutotuneReg->setCurrentIndex(status.getRegAutiTune());
        m_statusAutotuneFine->setCurrentIndex(status.getFineAutoTune());
        m_statusAcpInputSignal->setCurrentIndex(status.getAcpFail());
        m_statusHlInputSignal->setCurrentIndex(status.getHlFail());
        m_statusTriggerInputSignal->setCurrentIndex(status.getTriggerFail());
    }
}

void RadarSerialControlUI::SlotStartTxClicked()
{
    m_radarSetupSettings.m_transceiverMode = RadarSetupTransceiverModeRS::TransceiverTxMode;
    m_radarSetupSettings.m_enableMotor = RadarSetupEnableMotorRS::Enabled;
    m_setupTx->setCurrentIndex(m_radarSetupSettings.getTransceiverMode());
    m_setupPulseWidth->setCurrentIndex(m_radarSetupSettings.getPulseMode());
    m_setupMotor->setCurrentIndex(m_radarSetupSettings.getEnableMotor());
    setupSetClicked();
    setupGetClicked();
}

void RadarSerialControlUI::SlotStopTxClicked()
{
    m_radarSetupSettings.m_transceiverMode = RadarSetupTransceiverModeRS::TransceiverStandBy;
    m_radarSetupSettings.m_enableMotor = RadarSetupEnableMotorRS::Disabled;
    m_setupTx->setCurrentIndex(m_radarSetupSettings.getTransceiverMode());
    m_setupPulseWidth->setCurrentIndex(m_radarSetupSettings.getPulseMode());
    m_setupMotor->setCurrentIndex(m_radarSetupSettings.getEnableMotor());
    setupSetClicked();
    setupGetClicked();
}

void RadarSerialControlUI::SlotPortSettingsClicked()
{
    m_serialPortSettingsUI->show();
}

void RadarSerialControlUI::SlotPortManualCheckClicked()
{
    m_portName->setEnabled(m_portManualCheck->checkState());
}

void RadarSerialControlUI::SlotSetPortClicked()
{
    if (m_radarCommunicationSerial->isRunning()) {
        m_radarCommunicationSerial->stopWorkingThread();
        m_radarCommunicationSerial->setPortName(m_serialPortName);
        m_radarCommunicationSerial->setPortSettings(*m_serialPortSettings);
        m_radarCommunicationSerial->startWorkingThread();
    } else {
        m_radarCommunicationSerial->setPortName(m_serialPortName);
        m_radarCommunicationSerial->setPortSettings(*m_serialPortSettings);
    }
    m_portTip = m_serialPortSettings->getToolTip();
    m_portName->setToolTip(m_portTip);
}

void RadarSerialControlUI::SlotGetAndSetDefaults()
{
    SlotGetAll();

    m_radarTriggerSettingsDefault.setTriggerGenetatorState(m_iniSettings->value("RadarTriggerGeneratorStateRS").toInt() > - 1
                                                         ? m_iniSettings->value("RadarTriggerGeneratorStateRS").toInt()
                                                         : m_radarTriggerSettings.getTriggerGenetatorState());

    m_radarTriggerSettingsDefault.setTriggerPeriod(m_iniSettings->value("RadarTriggerPeriodRS").toInt() > - 1
                                                 ? m_iniSettings->value("RadarTriggerPeriodRS").toInt()
                                                 : m_radarTriggerSettings.getTriggerPeriod());

    m_radarTriggerSettingsDefault.setTriggerStaggerValue(m_iniSettings->value("RadarTriggerStaggerValue").toInt() > - 1
                                                       ? m_iniSettings->value("RadarTriggerStaggerValue").toInt()
                                                       : m_radarTriggerSettings.getTriggerStaggerValue());



    m_radarMiscZeroSettingsDefault.setTestACP(m_iniSettings->value("RadarMiscZeroTestACPRS").toInt() > - 1
                                            ? m_iniSettings->value("RadarMiscZeroTestACPRS").toInt()
                                            : m_radarMiscZeroSettings.getTestACP());

    m_radarMiscZeroSettingsDefault.setAutotuneFine(m_iniSettings->value("RadarMiscZeroAutoTuneFineRS").toInt() > - 1
                                                 ? m_iniSettings->value("RadarMiscZeroAutoTuneFineRS").toInt()
                                                 : m_radarMiscZeroSettings.getAutotuneFine());

    m_radarMiscZeroSettingsDefault.setAutotuneReg(m_iniSettings->value("RadarMiscZeroAutoTuneRegRS").toInt() > - 1
                                                ? m_iniSettings->value("RadarMiscZeroAutoTuneRegRS").toInt()
                                                : m_radarMiscZeroSettings.getAutotuneReg());

    m_radarMiscZeroSettingsDefault.setDivideACPInputSignal(m_iniSettings->value("RadarMiscZeroDivideACPInputRS").toInt() > - 1
                                                         ? m_iniSettings->value("RadarMiscZeroDivideACPInputRS").toInt()
                                                         : m_radarMiscZeroSettings.getDivideACPInputSignal());



    m_radarMiscOneSettingsDefault.setAcpPeriod(m_iniSettings->value("RadarMiscOneAcpPeriodRS").toInt() > - 1
                                             ? m_iniSettings->value("RadarMiscOneAcpPeriodRS").toInt()
                                             : m_radarMiscOneSettings.getAcpPeriod());

    m_radarMiscOneSettingsDefault.setBlankingRam(m_iniSettings->value("RadarMiscOneBlankingRamRS").toInt() > - 1
                                               ? m_iniSettings->value("RadarMiscOneBlankingRamRS").toInt()
                                               : m_radarMiscOneSettings.getBlankingRam());

    m_radarMiscOneSettingsDefault.setBandwith(m_iniSettings->value("RadarMiscOneBandwithRS").toInt() > - 1
                                            ? m_iniSettings->value("RadarMiscOneBandwithRS").toInt()
                                            : m_radarMiscOneSettings.getBandwith());



    m_radarHlSetupSettingsDefault.setWidth(m_iniSettings->value("RadarHlSetupWidthRS").toInt() > - 1
                                          ? m_iniSettings->value("RadarHlSetupWidthRS").toInt()
                                          : m_radarHlSetupSettings.getWidth());

    m_radarHlSetupSettingsDefault.setInvert(m_iniSettings->value("RadarHlSetupInvertRS").toInt() > - 1
                                          ? m_iniSettings->value("RadarHlSetupInvertRS").toInt()
                                          : m_radarHlSetupSettings.getInvert());

    m_radarHlSetupSettingsDefault.setEdge(m_iniSettings->value("RadarHlSetupEdgeRS").toInt() > - 1
                                        ? m_iniSettings->value("RadarHlSetupEdgeRS").toInt()
                                        : m_radarHlSetupSettings.getEdge());

    m_radarHlSetupSettingsDefault.setDelayValue(m_iniSettings->value("RadarHlSetupDelayValue").toInt() > - 1
                                              ? m_iniSettings->value("RadarHlSetupDelayValue").toInt()
                                              : m_radarHlSetupSettings.getDelayValue());



    m_radarEnableSettingsDefault.setAutoTune(m_iniSettings->value("RadarEnableAutoTuneRS").toInt() > - 1
                                           ? m_iniSettings->value("RadarEnableAutoTuneRS").toInt()
                                           : m_radarEnableSettings.getAutoTune());

    m_radarEnableSettingsDefault.setDac(m_iniSettings->value("RadarEnableDAConvertersRS").toInt() > - 1
                                      ? m_iniSettings->value("RadarEnableDAConvertersRS").toInt()
                                      : m_radarEnableSettings.getDac());

    m_radarEnableSettingsDefault.setAdc(m_iniSettings->value("RadarEnableADConvertersRS").toInt() > - 1
                                      ? m_iniSettings->value("RadarEnableADConvertersRS").toInt()
                                      : m_radarEnableSettings.getAdc());



    m_radarTuneCoarseSettingsDefault.setCoarseValue(m_iniSettings->value("RadarTuneCoarseRS").toInt() > - 1
                                                  ? m_iniSettings->value("RadarTuneCoarseRS").toInt()
                                                  : m_radarTuneCoarseSettings.radarTuneCoarseRS());

    m_radarTuneFineSettingsDefault.setFineValue(m_iniSettings->value("RadarTuneFineRS").toInt() > - 1
                                              ? m_iniSettings->value("RadarTuneFineRS").toInt()
                                              : m_radarTuneFineSettings.radarTuneFineRS());

    m_radarTuneRegSettingsDefault.setegValue(m_iniSettings->value("RadarTuneRegRS").toInt() > - 1
                                           ? m_iniSettings->value("RadarTuneRegRS").toInt()
                                           : m_radarTuneRegSettings.radarTuneRegRS());



    m_radarSetupSettingsDefault.setTransceiverMode(m_iniSettings->value("RadarSetupTransceiverModeRS").toInt() > - 1
                                                 ? m_iniSettings->value("RadarSetupTransceiverModeRS").toInt()
                                                 : m_radarSetupSettings.getTransceiverMode());

    m_radarSetupSettingsDefault.setPulseMode(m_iniSettings->value("RadarSetupPulseModeRS").toInt() > - 1
                                           ? m_iniSettings->value("RadarSetupPulseModeRS").toInt()
                                           : m_radarSetupSettings.getPulseMode());

    m_radarSetupSettingsDefault.setEnableMotor(m_iniSettings->value("RadarSetupEnableMotorRS").toInt() > - 1
                                             ? m_iniSettings->value("RadarSetupEnableMotorRS").toInt()
                                             : m_radarSetupSettings.getEnableMotor());

    SlotDefaultSettingsClicked();
}

void RadarSerialControlUI::SlotAllowSet(const bool &allow)
{
    m_setAll->setEnabled(allow);
    m_resetSettings->setEnabled(allow);
    m_defaultSettings->setEnabled(allow);
}

void RadarSerialControlUI::SlotSetAll()
{
    triggerSetClicked();
    miscZeroSetClicked();
    miscOneSetClicked();
    hlSetupSetClicked();
    enableSetClicked();
    tuneCoatseSetClicked();
    tuneFineSetClicked();
    tuneRegSetClicked();
    setupSetClicked();
}

void RadarSerialControlUI::SlotGetAll()
{
    triggerGetClicked();
    miscZeroGetClicked();
    miscOneGetClicked();
    hlSetupGetClicked();
    enableGetClicked();
    tuneCoarseGetClicked();
    tuneFineGetClicked();
    tuneRegGetClicked();
    setupGetClicked();
    autotuneFineGetClicked();
    autotuneRegGetClicked();
    magIndicatorGetClicked();
    tuneIndicatorGetClicked();
    statusGetClicked();
}

void RadarSerialControlUI::SlotResetSettingsClicked()
{
    m_triggerEnable->setCurrentIndex(m_radarTriggerSettings.getTriggerGenetatorState());
    m_triggerStaggerValue->setValue(m_radarTriggerSettings.getTriggerStaggerValue());
    m_triggerPeriod->setCurrentIndex(m_radarTriggerSettings.getTriggerPeriod());

    m_miscZeroAutoTineFine->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneFine());
    m_miscZeroAutoTuneReg->setCurrentIndex(m_radarMiscZeroSettings.getAutotuneReg());
    m_miscZeroTestACP->setCurrentIndex(m_radarMiscZeroSettings.getTestACP());
    m_miscZeroDivACP->setCurrentIndex(m_radarMiscZeroSettings.getDivideACPInputSignal());

    m_miscOneAcpPeriod->setCurrentIndex(m_radarMiscOneSettings.getAcpPeriod());
    m_miscOneBlackingRam->setCurrentIndex(m_radarMiscOneSettings.getBlankingRam());
    m_miscOneBandwidth->setCurrentIndex(m_radarMiscOneSettings.getBandwith());

    m_hlSetupWidth->setCurrentIndex(m_radarHlSetupSettings.getWidth());
    m_hlSetupInvert->setCurrentIndex(m_radarHlSetupSettings.getInvert());
    m_hlSetupEdge->setCurrentIndex(m_radarHlSetupSettings.getEdge());
    m_hlSetupDelay->setValue(m_radarHlSetupSettings.getDelayValue());

    m_enableAutoTune->setCurrentIndex(m_radarEnableSettings.getAutoTune());
    m_enableDA->setCurrentIndex(m_radarEnableSettings.getDac());
    m_enableAD->setCurrentIndex(m_radarEnableSettings.getAdc());

    m_tuneCoarseValue->setValue(m_radarTuneCoarseSettings.m_coarseValue);
    m_tuneFineValue->setValue(m_radarTuneFineSettings.m_fineValue);
    m_tuneRegValue->setValue(m_radarTuneRegSettings.m_regValue);

    m_setupTx->setCurrentIndex(m_radarSetupSettings.getTransceiverMode());
    m_setupPulseWidth->setCurrentIndex(m_radarSetupSettings.getPulseMode());
    m_setupMotor->setCurrentIndex(m_radarSetupSettings.getEnableMotor());

    SlotSetAll();
    SlotGetAll();
}

void RadarSerialControlUI::SlotDefaultSettingsClicked()
{
    m_triggerEnable->setCurrentIndex(m_radarTriggerSettingsDefault.getTriggerGenetatorState());
    m_triggerStaggerValue->setValue(m_radarTriggerSettingsDefault.getTriggerStaggerValue());
    m_triggerPeriod->setCurrentIndex(m_radarTriggerSettingsDefault.getTriggerPeriod());

    m_miscZeroAutoTineFine->setCurrentIndex(m_radarMiscZeroSettingsDefault.getAutotuneFine());
    m_miscZeroAutoTuneReg->setCurrentIndex(m_radarMiscZeroSettingsDefault.getAutotuneReg());
    m_miscZeroTestACP->setCurrentIndex(m_radarMiscZeroSettingsDefault.getTestACP());
    m_miscZeroDivACP->setCurrentIndex(m_radarMiscZeroSettingsDefault.getDivideACPInputSignal());

    m_miscOneAcpPeriod->setCurrentIndex(m_radarMiscOneSettingsDefault.getAcpPeriod());
    m_miscOneBlackingRam->setCurrentIndex(m_radarMiscOneSettingsDefault.getBlankingRam());
    m_miscOneBandwidth->setCurrentIndex(m_radarMiscOneSettingsDefault.getBandwith());

    m_hlSetupWidth->setCurrentIndex(m_radarHlSetupSettingsDefault.getWidth());
    m_hlSetupInvert->setCurrentIndex(m_radarHlSetupSettingsDefault.getInvert());
    m_hlSetupEdge->setCurrentIndex(m_radarHlSetupSettingsDefault.getEdge());
    m_hlSetupDelay->setValue(m_radarHlSetupSettingsDefault.getDelayValue());

    m_enableAutoTune->setCurrentIndex(m_radarEnableSettingsDefault.getAutoTune());
    m_enableDA->setCurrentIndex(m_radarEnableSettingsDefault.getDac());
    m_enableAD->setCurrentIndex(m_radarEnableSettingsDefault.getAdc());

    m_tuneCoarseValue->setValue(m_radarTuneCoarseSettingsDefault.m_coarseValue);
    m_tuneFineValue->setValue(m_radarTuneFineSettingsDefault.m_fineValue);
    m_tuneRegValue->setValue(m_radarTuneRegSettingsDefault.m_regValue);

    m_setupTx->setCurrentIndex(m_radarSetupSettingsDefault.getTransceiverMode());
    m_setupPulseWidth->setCurrentIndex(m_radarSetupSettingsDefault.getPulseMode());
    m_setupMotor->setCurrentIndex(m_radarSetupSettingsDefault.getEnableMotor());

    SlotSetAll();
    SlotGetAll();
}

void RadarSerialControlUI::SlotSetSerialPortParameters(const QString &serialPortName, const SerialPortSettings &serialPortSettings)
{
    m_serialPortName = serialPortName;
    m_serialPortSettings->s_baudRate = serialPortSettings.s_baudRate;
    m_serialPortSettings->s_dataBits = serialPortSettings.s_dataBits;
    m_serialPortSettings->s_stopBits = serialPortSettings.s_stopBits;
    m_serialPortSettings->s_parity = serialPortSettings.s_parity;
    m_serialPortSettings->s_flowControl = serialPortSettings.s_flowControl;
    m_portName->setText(serialPortName);
    m_setPort->setToolTip(m_serialPortSettings->getToolTip());
}

void RadarSerialControlUI::setFullPalette(const QPalette &palette)
{
    setPalette(palette);
    m_serialPortSettingsUI->setPalette(palette);
}

void RadarSerialControlUI::setTriggetState(RadarTriggerRS &triggerState)
{
    m_triggerEnable->setCurrentIndex(triggerState.getTriggerGenetatorState());
    m_triggerStaggerValue->setValue(triggerState.getTriggerStaggerValue());
    m_triggerPeriod->setCurrentIndex(triggerState.getTriggerPeriod());
}

void RadarSerialControlUI::OpenSettingsClicked()
{
    if (this->isVisible()) {
        this->close();
    } else {
        this->show();
    }
}
