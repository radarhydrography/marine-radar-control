/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef STRUCTSSERIAL_H
#define STRUCTSSERIAL_H

#include <QString>
#include <QSerialPort>
#include <QComboBox>
#include <QList>
#include <QSerialPortInfo>

struct SerialPortParameters
{
    QString s_port;
    QString s_location;
    QString s_description;
    QString s_manufacturer;
    QString s_serialNumber;
    QString s_vendorIdentifier;
    QString s_productIdentifier;
    QString s_busy;

    QList<SerialPortParameters> s_portList;
    quint8 s_currenntIndex;

    SerialPortParameters() :
        s_port(QString("")), s_location(QString("")), s_description(QString("")), s_manufacturer(QString("")),
        s_serialNumber(QString("")), s_vendorIdentifier(QString("")), s_productIdentifier(QString("")), s_busy(QString("")) {};

    SerialPortParameters(const QString &por, const QString &loc, const QString &des, const QString &man,
                   const QString ser, const QString &ven, const QString &pro, const QString &bus) :
        s_port(por), s_location(loc), s_description(des), s_manufacturer(man),
        s_serialNumber(ser), s_vendorIdentifier(ven), s_productIdentifier(pro), s_busy(bus) {}

    SerialPortParameters(const SerialPortParameters &serialPortParameters)
    {
        s_port = serialPortParameters.s_port;
        s_location = serialPortParameters.s_location;
        s_description = serialPortParameters.s_description;
        s_manufacturer = serialPortParameters.s_manufacturer;
        s_serialNumber = serialPortParameters.s_serialNumber;
        s_vendorIdentifier = serialPortParameters.s_vendorIdentifier;
        s_productIdentifier = serialPortParameters.s_productIdentifier;
        s_busy = serialPortParameters.s_busy;
    }

    SerialPortParameters operator=(const SerialPortParameters &serialPortParameters)
    {
        return serialPortParameters;
    }

    inline static QList<SerialPortParameters> portList()
    {
        QList<SerialPortParameters> serialPortParameters;

        const auto serialPortInfos = QSerialPortInfo::availablePorts();
        const QString blankString = "N/A";
        QString description;
        QString manufacturer;
        QString serialNumber;

        for (const QSerialPortInfo &serialPortInfo : serialPortInfos) {
            description = serialPortInfo.description();
            manufacturer = serialPortInfo.manufacturer();
            serialNumber = serialPortInfo.serialNumber();
            QStringList currentPort;
            serialPortParameters.append(SerialPortParameters(serialPortInfo.portName(),
                                                 serialPortInfo.systemLocation(),
                                                 (!description.isEmpty() ? description : blankString),
                                                 (!manufacturer.isEmpty() ? manufacturer : blankString),
                                                 (!serialNumber.isEmpty() ? serialNumber : blankString),
                                                 (serialPortInfo.hasVendorIdentifier()
                                                      ? QByteArray::number(serialPortInfo.vendorIdentifier(), 16)
                                                      : blankString),
                                                 (serialPortInfo.hasProductIdentifier()
                                                      ? QByteArray::number(serialPortInfo.productIdentifier(), 16)
                                                      : blankString),
                                                 (serialPortInfo.isBusy() ? "Yes" : "No")));
        }

        return serialPortParameters;
    }

    inline void refresh()
    {
        s_portList = portList();
    }

    inline void setIndex(const quint8 &index)
    {
        if (!s_portList.isEmpty() && (index < s_portList.length())) {
            s_currenntIndex = index;
            s_port = s_portList[index].s_port;
            s_location = s_portList[index].s_location;
            s_description = s_portList[index].s_description;
            s_manufacturer = s_portList[index].s_manufacturer;
            s_serialNumber = s_portList[index].s_serialNumber;
            s_vendorIdentifier = s_portList[index].s_vendorIdentifier;
            s_productIdentifier = s_portList[index].s_productIdentifier;
            s_busy = s_portList[index].s_busy;
        }
    }

    inline static QString showPortParameters(const QList<SerialPortParameters> &serialPortList, const quint8 &index)
    {
        QString portParameters;
        portParameters.append("Port: " + serialPortList[index].s_port + "\n");
        portParameters.append("Location: " + serialPortList[index].s_location + "\n");
        portParameters.append("Description: " + serialPortList[index].s_description + "\n");
        portParameters.append("Manufacturer: " + serialPortList[index].s_manufacturer + "\n");
        portParameters.append("Serial number: " + serialPortList[index].s_serialNumber + "\n");
        portParameters.append("Vendor Identifier: " + serialPortList[index].s_vendorIdentifier + "\n");
        portParameters.append("Product Identifier: " + serialPortList[index].s_productIdentifier + "\n");
        portParameters.append("Busy: " + serialPortList[index].s_busy);

        return portParameters;
    }

    inline QString showPortParameters(quint8 &index)
    {
        return showPortParameters(s_portList, index);
    }

    inline QString showPortParameters()
    {
        return showPortParameters(s_portList, s_currenntIndex);
    }
};

enum class SerialParameters
{
    BaudRate,
    DataBits,
    Direction,
    FlowControl,
    Parity,
    StopBits
};

struct SerialPortSettings
{
    int s_baudRate = QSerialPort::Baud9600;
    QSerialPort::DataBits s_dataBits = QSerialPort::Data8;
    QSerialPort::Direction s_direction = QSerialPort::AllDirections;
    QSerialPort::FlowControl s_flowControl = QSerialPort::NoFlowControl;
    QSerialPort::Parity s_parity = QSerialPort::NoParity;
    QSerialPort::StopBits s_stopBits = QSerialPort::OneStop;

    QStringList s_baudRateList{"9600", "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200"};
    QStringList s_dataBitsList{"8", "5", "6", "7", "8"};
    QStringList s_directionList{"All", "Input", "Output", "All"};
    QStringList s_flowControlList{"No", "Hardware", "Software", "Unknown"};
    QStringList s_parityList{"No", "Even", "Odd", "Space", "Mark", "Unknown"};
    QStringList s_stopBitsList{"1", "1", "1.5", "2"};

    SerialPortSettings()
    {
        s_baudRate = QSerialPort::Baud9600;
        s_dataBits = QSerialPort::Data8;
        s_direction = QSerialPort::AllDirections;
        s_flowControl = QSerialPort::NoFlowControl;
        s_parity = QSerialPort::NoParity;
        s_stopBits = QSerialPort::OneStop;
    }

    inline static int cmb2num(const QComboBox &comboBox, SerialParameters serialParameter);
    inline static QSerialPort::BaudRate mum2QSPnum(const int &num, QSerialPort::BaudRate defaultBaudRate = QSerialPort::Baud9600);
    inline static QSerialPort::DataBits mum2QSPnum(const int &num, QSerialPort::DataBits defaultDataBits = QSerialPort::Data8);
    inline static QSerialPort::Direction mum2QSPnum(const int &num, QSerialPort::Direction defaultDirection = QSerialPort::AllDirections);
    inline static QSerialPort::FlowControl mum2QSPnum(const int &num, QSerialPort::FlowControl defaultFlowControl = QSerialPort::NoFlowControl);
    inline static QSerialPort::Parity mum2QSPnum(const int &num, QSerialPort::Parity defaultParity = QSerialPort::NoParity);
    inline static QSerialPort::StopBits mum2QSPnum(const int &num, QSerialPort::StopBits defaultStopBits = QSerialPort::OneStop);
    inline static SerialPortSettings portSettingsFromGUI(const QComboBox &comboBoxBaudRate,
                                                         const QComboBox &comboBoxDataBist,
                                                         const QComboBox &comboboxDirection,
                                                         const QComboBox &comboBoxFlowControl,
                                                         const QComboBox &comboBoxParity,
                                                         const QComboBox &comboBoxStopBits);
    inline static void portSettingsDefault(QComboBox *comboBoxBaudRate,
                                           QComboBox *comboBoxDataBist,
                                           QComboBox *comboboxDirection,
                                           QComboBox *comboBoxFlowControl,
                                           QComboBox *comboBoxParity,
                                           QComboBox *comboBoxStopBits);

    inline QString getToolTip() {
        QString toolTip;
        toolTip += QString("Baud Rate: ");
        int index;
        switch (s_baudRate) {
        case 0: {
            index = 0;
            break;
        }
        case QSerialPort::Baud1200: {
            index = 1;
            break;
        }
        case QSerialPort::Baud2400: {
            index = 2;
            break;
        }
        case QSerialPort::Baud4800: {
            index = 3;
            break;
        }
        case QSerialPort::Baud9600: {
            index = 4;
            break;
        }
        case QSerialPort::Baud19200: {
            index = 5;
            break;
        }
        case QSerialPort::Baud38400: {
            index = 6;
            break;
        }
        case QSerialPort::Baud57600: {
            index = 7;
            break;
        }
        case QSerialPort::Baud115200: {
            index = 8;
            break;
        }
        default: {
            index = 0;
            break;
        }
        }
        toolTip += s_baudRateList[index];

        toolTip += QString(", Data Bits: ");
        switch (s_dataBits) {
        case QSerialPort::Data5: {
            index = 1;
            break;
        }
        case QSerialPort::Data6: {
            index = 2;
            break;
        }
        case QSerialPort::Data7: {
            index = 3;
            break;
        }
        case QSerialPort::Data8: {
            index = 4;
            break;
        }
        default: {
            index = 0;
            break;
        }
        }
        toolTip += s_dataBitsList[index];

        toolTip += QString(", Direction: ");
        switch (s_direction) {
        case QSerialPort::Input: {
            index = 1;
            break;
        }
        case QSerialPort::Output: {
            index = 2;
            break;
        }
        case QSerialPort::AllDirections: {
            index = 3;
            break;
        }
        default: {
            index = 0;
            break;
        }
        }
        toolTip += s_directionList[index];

        toolTip += QString(", Flow Control: ");
        switch (s_flowControl) {
        case QSerialPort::NoFlowControl: {
            index = 0;
            break;
        }
        case QSerialPort::HardwareControl: {
            index = 1;
            break;
        }
        case QSerialPort::SoftwareControl: {
            index = 2;
            break;
        }
        case QSerialPort::UnknownFlowControl: {
            index = 3;
            break;
        }
        default: {
            index = 0;
            break;
        }
        }
        toolTip += s_flowControlList[index];

        toolTip += QString(", Parity: ");
        switch (s_parity) {
        case QSerialPort::NoParity: {
            index = 0;
            break;
        }
        case QSerialPort::EvenParity: {
            index = 1;
            break;
        }
        case QSerialPort::OddParity: {
            index = 2;
            break;
        }
        case QSerialPort::SpaceParity: {
            index = 3;
            break;
        }
        case QSerialPort::MarkParity: {
            index = 4;
            break;
        }
        case QSerialPort::UnknownParity: {
            index = 5;
            break;
        }
        default: {
            index = 0;
            break;
        }
        }
        toolTip += s_parityList[index];

        toolTip += QString(", Stop Bits: ");
        switch (s_stopBits) {
        case QSerialPort::OneStop: {
            index = 0;
            break;
        }
        case QSerialPort::OneAndHalfStop: {
            index = 1;
            break;
        }
        case QSerialPort::TwoStop: {
            index = 2;
            break;
        }
        case QSerialPort::UnknownStopBits: {
            index = 3;
            break;
        }
        default: {
            index = 0;
            break;
        }
        }
        toolTip += s_stopBitsList[index];

        return toolTip;
    }
};

int SerialPortSettings::cmb2num(const QComboBox &comboBox, SerialParameters serialParameter)
{
    int currentIndex = comboBox.currentIndex();
    switch (serialParameter) {
    case SerialParameters::BaudRate: {
        switch (currentIndex) {
        case 0: return -2;
        case 1: return QSerialPort::Baud1200;
        case 2: return QSerialPort::Baud2400;
        case 3: return QSerialPort::Baud4800;
        case 4: return QSerialPort::Baud9600;
        case 5: return QSerialPort::Baud19200;
        case 6: return QSerialPort::Baud38400;
        case 7: return QSerialPort::Baud57600;
        case 8: return QSerialPort::Baud115200;
        case 9: return QSerialPort::UnknownBaud;
        case 10: return comboBox.currentText().toInt();
        default: return -3;
        }
    }
    case SerialParameters::DataBits: {
        switch (currentIndex) {
        case 0: return -2;
        case 1: return QSerialPort::Data5;
        case 2: return QSerialPort::Data6;
        case 3: return QSerialPort::Data7;
        case 4: return QSerialPort::Data8;
        case 5: return QSerialPort::UnknownDataBits;
        case 6: return comboBox.currentText().toInt();
        default: return -3;
        }
    }
    case SerialParameters::Direction: {
        switch (currentIndex) {
        case 0: return -2;
        case 1: return QSerialPort::Input;
        case 2: return QSerialPort::Output;
        case 3: return QSerialPort::AllDirections;
        default: return -3;
        }
    }
    case SerialParameters::FlowControl: {
        switch (currentIndex) {
        case 0: return -2;
        case 1: return QSerialPort::NoFlowControl;
        case 2: return QSerialPort::HardwareControl;
        case 3: return QSerialPort::SoftwareControl;
        case 4: return QSerialPort::UnknownFlowControl;
        default: return -3;
        }
    }
    case SerialParameters::Parity: {
        switch (currentIndex) {
        case 0: return -2;
        case 1: return QSerialPort::NoParity;
        case 2: return QSerialPort::EvenParity;
        case 3: return QSerialPort::OddParity;
        case 4: return QSerialPort::SpaceParity;
        case 5: return QSerialPort::MarkParity;
        case 6: return QSerialPort::UnknownParity;
        default: return -3;
        }
    }
    case SerialParameters::StopBits: {
        switch (currentIndex) {
        case 0: return -2;
        case 1: return QSerialPort::OneStop;
        case 2: return QSerialPort::OneAndHalfStop;
        case 3: return QSerialPort::TwoStop;
        case 4: return QSerialPort::UnknownStopBits;
        default: return -3;
        }
    }
    default: return -4;
    }
}

QSerialPort::BaudRate SerialPortSettings::mum2QSPnum(const int &num, QSerialPort::BaudRate defaultBaudRate)
{
    switch (num) {
    case 0: return defaultBaudRate;
    case QSerialPort::Baud1200: return QSerialPort::Baud1200;
    case QSerialPort::Baud2400: return QSerialPort::Baud2400;
    case QSerialPort::Baud4800: return QSerialPort::Baud4800;
    case QSerialPort::Baud9600: return QSerialPort::Baud9600;
    case QSerialPort::Baud19200: return QSerialPort::Baud19200;
    case QSerialPort::Baud38400: return QSerialPort::Baud38400;
    case QSerialPort::Baud57600: return QSerialPort::Baud57600;
    case QSerialPort::Baud115200: return QSerialPort::Baud115200;
    case QSerialPort::UnknownBaud: return QSerialPort::UnknownBaud;
    default: return defaultBaudRate;
    }
}

QSerialPort::DataBits SerialPortSettings::mum2QSPnum(const int &num, QSerialPort::DataBits defaultDataBits)
{
    switch (num) {
    case 0: return defaultDataBits;
    case QSerialPort::Data5: return QSerialPort::Data5;
    case QSerialPort::Data6: return QSerialPort::Data6;
    case QSerialPort::Data7: return QSerialPort::Data7;
    case QSerialPort::Data8: return QSerialPort::Data8;
    case QSerialPort::UnknownDataBits: return QSerialPort::UnknownDataBits;
    default: return defaultDataBits;
    }
}

QSerialPort::Direction SerialPortSettings::mum2QSPnum(const int &num, QSerialPort::Direction defaultDirection)
{
    switch (num) {
    case 0: return defaultDirection;
    case QSerialPort::Input: return QSerialPort::Input;
    case QSerialPort::Output: return QSerialPort::Output;
    case QSerialPort::AllDirections: return QSerialPort::AllDirections;
    default: return defaultDirection;
    }
}

QSerialPort::FlowControl SerialPortSettings::mum2QSPnum(const int &num, QSerialPort::FlowControl defaultFlowControl)
{
    switch (num) {
    case QSerialPort::NoFlowControl: return QSerialPort::NoFlowControl;
    case QSerialPort::HardwareControl: return QSerialPort::HardwareControl;
    case QSerialPort::SoftwareControl: return QSerialPort::SoftwareControl;
    case QSerialPort::UnknownFlowControl: return QSerialPort::UnknownFlowControl;
    default: return defaultFlowControl;
    }
}

QSerialPort::Parity SerialPortSettings::mum2QSPnum(const int &num, QSerialPort::Parity defaultParity)
{
    switch (num) {
    case QSerialPort::NoParity: return QSerialPort::NoParity;
    case QSerialPort::EvenParity: return QSerialPort::EvenParity;
    case QSerialPort::OddParity: return QSerialPort::OddParity;
    case QSerialPort::SpaceParity: return QSerialPort::SpaceParity;
    case QSerialPort::MarkParity: return QSerialPort::MarkParity;
    case QSerialPort::UnknownParity: return QSerialPort::UnknownParity;
    default: return defaultParity;
    }
}

QSerialPort::StopBits SerialPortSettings::mum2QSPnum(const int &num, QSerialPort::StopBits defaultStopBits)
{
    switch (num) {
    case 0: return defaultStopBits;
    case QSerialPort::OneStop: return QSerialPort::OneStop;
    case QSerialPort::OneAndHalfStop: return QSerialPort::OneAndHalfStop;
    case QSerialPort::TwoStop: return QSerialPort::TwoStop;
    case QSerialPort::UnknownStopBits: return QSerialPort::UnknownStopBits;
    default: return defaultStopBits;
    }
}

SerialPortSettings SerialPortSettings::portSettingsFromGUI(const QComboBox &comboBoxBaudRate,
                                                           const QComboBox &comboBoxDataBist,
                                                           const QComboBox &comboboxDirection,
                                                           const QComboBox &comboBoxFlowControl,
                                                           const QComboBox &comboBoxParity,
                                                           const QComboBox &comboBoxStopBits)
{
    SerialPortSettings portSettings = SerialPortSettings();

    int baudRate = SerialPortSettings::cmb2num(comboBoxBaudRate, SerialParameters::BaudRate);
    int dataBits = SerialPortSettings::cmb2num(comboBoxDataBist, SerialParameters::DataBits);
    int direction = SerialPortSettings::cmb2num(comboboxDirection, SerialParameters::Direction);
    int flowControl = SerialPortSettings::cmb2num(comboBoxFlowControl, SerialParameters::FlowControl);
    int parity = SerialPortSettings::cmb2num(comboBoxParity, SerialParameters::Parity);
    int stopBits = SerialPortSettings::cmb2num(comboBoxStopBits, SerialParameters::StopBits);

    if (baudRate > -2) {
        portSettings.s_baudRate = baudRate;
    }

    if (dataBits > -2) {
        portSettings.s_dataBits = SerialPortSettings::mum2QSPnum(dataBits, QSerialPort::Data8);
    }

    if (direction > -2) {
        portSettings.s_direction = SerialPortSettings::mum2QSPnum(direction, QSerialPort::AllDirections);
    }

    if (flowControl > -2) {
        portSettings.s_flowControl = SerialPortSettings::mum2QSPnum(flowControl, QSerialPort::NoFlowControl);
    }

    if (parity > -2) {
        portSettings.s_parity = SerialPortSettings::mum2QSPnum(parity, QSerialPort::NoParity);
    }

    if (stopBits > -2) {
        portSettings.s_stopBits = SerialPortSettings::mum2QSPnum(stopBits, QSerialPort::OneStop);
    }

    return portSettings;
}

void SerialPortSettings::portSettingsDefault(QComboBox *comboBoxBaudRate,
                                             QComboBox *comboBoxDataBist,
                                             QComboBox *comboboxDirection,
                                             QComboBox *comboBoxFlowControl,
                                             QComboBox *comboBoxParity,
                                             QComboBox *comboBoxStopBits)
{
    QStringList baudRate{"Default", "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200", "Unknown", ""};
    QStringList dataBits{"Default", "5", "6", "7", "8", "Unknown"};
    QStringList direction{"Default", "Input", "Output", "All"};
    QStringList flowControl{"Default", "No", "Hardware", "Software", "Unknown"};
    QStringList parity{"Default", "No", "Even", "Odd", "Space", "Mark", "Unknown"};
    QStringList stopBits{"Default", "1", "1.5", "2", "Unknown"};

    comboBoxBaudRate->addItems(baudRate);
    comboBoxBaudRate->setCurrentIndex(0);
    comboBoxBaudRate->setEnabled(0);

    comboBoxDataBist->addItems(dataBits);
    comboBoxDataBist->setCurrentIndex(0);
    comboBoxDataBist->setEnabled(0);

    comboboxDirection->addItems(direction);
    comboboxDirection->setCurrentIndex(0);
    comboboxDirection->setEnabled(0);

    comboBoxFlowControl->addItems(flowControl);
    comboBoxFlowControl->setCurrentIndex(0);
    comboBoxFlowControl->setEnabled(0);

    comboBoxParity->addItems(parity);
    comboBoxParity->setCurrentIndex(0);
    comboBoxParity->setEnabled(0);

    comboBoxStopBits->addItems(stopBits);
    comboBoxStopBits->setCurrentIndex(0);
    comboBoxStopBits->setEnabled(0);
}

#endif // STRUCTSSERIAL_H
