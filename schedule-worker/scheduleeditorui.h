/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SCHEDULEEDITORUI_H
#define SCHEDULEEDITORUI_H

#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>
#include <QTimeEdit>
#include <QDialog>
#include <QPushButton>

#include "scheduleline.h"
#include "schedule.h"

class ScheduleEditorUI : public QDialog
{
    Q_OBJECT

    QLineEdit *w_taskNameLineEdit;
    QSpinBox *w_repetitionTimeSpinBox;
    QTimeEdit *w_beginTimeEdit;
    QTimeEdit *w_finishTimeEdit;
    QComboBox *w_radarModeComboBox;
    QSpinBox *w_rotationSpeed;
    QComboBox *w_sweepingModeComboBox;
    QDoubleSpinBox *w_angleOneDoubleSpinBox;
    QDoubleSpinBox *w_angleTwoDoubleSpinBox;
    QComboBox *w_triggerFrequencyComboBox;
    QComboBox *w_unAmpSignalLengthComboBox;
    QComboBox *w_ampSignalBeginComboBox;
    QComboBox *w_ampSignalEndComboBox;
    QComboBox *w_samplingFrequencyComboBox;
    QComboBox *w_maxFileSizeComboBox;
    QLineEdit *w_basicFileNameLineEdit;

    QPushButton *w_applyButton;
    QPushButton *w_cancelButton;

    ScheduleLine getSettingsFromGui();
    void setSettingsFromTaskLine(ScheduleLine &scheduleLine);

public:
    explicit ScheduleEditorUI(QWidget *parent = nullptr);

    bool createScheduleLine(Schedule &schedule);
    bool editSchedulLine(Schedule &schedule, int index);
    bool copyScheduleLine(Schedule &schedule, int index);

private slots:
    void slotRadarModeIndexIsChanged(const int &index);

signals:

};

#endif // SCHEDULEEDITORUI_H
