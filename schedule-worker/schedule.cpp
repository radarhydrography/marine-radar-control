/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "schedule.h"
#include <QTextStream>

Schedule::Schedule(QObject *parent) : QObject(parent)
{
    m_schedule = new QList<ScheduleLine>;

//    m_beginTimeSchedule = new QMap<QTime, int>();
//    m_finishTimeSchedule = new QMap<QTime, int>();
    m_fullTimeSchedule = new QMap<QPair<QTime, QTime>, int>;
}

Schedule::Schedule(const Schedule &schedule, QObject *parent) : QObject(parent),
    m_schedule(schedule.m_schedule), m_fileName(schedule.m_fileName)
{
    m_schedule = new QList<ScheduleLine>;

    this->m_schedule = schedule.m_schedule;
    this->m_fileName = schedule.m_fileName;

    m_fullTimeSchedule = new QMap<QPair<QTime, QTime>, int>;
//    m_beginTimeSchedule = new QMap<QTime, int>();
//    m_finishTimeSchedule = new QMap<QTime, int>();
    m_fullTimeSchedule = new QMap<QPair<QTime, QTime>, int>;
}

Schedule Schedule::operator=(const Schedule &schedule)
{
    this->m_schedule = schedule.m_schedule;
    this->m_fileName = schedule.m_fileName;
    return *this;
}

void Schedule::setSchedule(const Schedule &schedule)
{
    m_schedule->clear();
    m_schedule->append(*schedule.m_schedule);
    m_fileName = schedule.m_fileName;
    refreshSchedule();
}

Schedule Schedule::readScheduleFile(QString fileName)
{
    Schedule schedule;
    schedule.slotLoadSchedule(fileName);
    return schedule;
}

int Schedule::size()
{
    return m_schedule->size();
}

void Schedule::clear()
{
    m_schedule->clear();
    m_fileName = QString("");
}

ScheduleLine Schedule::getScheduleLine(const int &number)
{
    return m_schedule->at(number);
}

QStringList Schedule::getDetailedSchedule()
{
    m_scheduleAvgDataSavingSpeed = 0;
    QStringList detailedSchedule;
//    QList<QTime> beginTime = m_beginTimeSchedule->keys();
//    QList<QTime> finishTime = m_finishTimeSchedule->keys();
    QList<QPair<QTime, QTime>> scheduleTime = m_fullTimeSchedule->keys();

    int currentHour = 0;
    if (scheduleTime.size() > 0) {
        currentHour = scheduleTime.at(0).first.toString(QString("hh")).toInt();
    }

    for (int realTaskNumber = 0; realTaskNumber < m_fullTimeSchedule->size(); realTaskNumber++)
    {
        int taskNumber = m_fullTimeSchedule->value(scheduleTime.at(realTaskNumber));
        ScheduleLine realTask = m_schedule->at(taskNumber);
        if ((scheduleTime.at(realTaskNumber).first.toString(QString("hh")).toInt() > currentHour)){
            detailedSchedule.append(QString("------------"));
        }
        QString taskLine;
        taskLine += scheduleTime.at(realTaskNumber).first.toString(QString("hh:mm:ss"));
        taskLine += QString(" - ");
        taskLine += scheduleTime.at(realTaskNumber).second.toString(QString("hh:mm:ss"));
        taskLine += QString("\t");
        taskLine += realTask.slotGetTaskName();
        taskLine += QString(";");
        detailedSchedule.append(taskLine);

        currentHour = scheduleTime.at(realTaskNumber).first.toString(QString("hh")).toInt();

        m_scheduleAvgDataSavingSpeed += getCurrentScheduleLineAvgDataSavingSpeed(realTask);
    }

    return detailedSchedule;
}

double Schedule::getCurrentScheduleLineAvgDataSavingSpeed(ScheduleLine &scheduleLine)
{
    double avgSpeed = 0.0;

    int datagrams =  static_cast<int>(scheduleLine.slotGetAmpSignalBegin());
    datagrams = datagrams == 0 ? 0 : -1;
    datagrams = static_cast<int>(scheduleLine.slotGetAmpSignalEnd()) - datagrams;
    double triggerFreq = static_cast<double>(static_cast<int>(scheduleLine.slotGetTriggerFrequency()) + 1)*1000.0;
    double dataSpeed = triggerFreq*(32.0 + 1024.0 + 1024.0*static_cast<double>(datagrams));

    double time = scheduleLine.slotGetTimeBegin().secsTo(scheduleLine.slotGetTimeFinish());

    avgSpeed = (dataSpeed*time)/(24*3600);

    return avgSpeed;
}

void Schedule::addSingleTask(const ScheduleLine &singleTask)
{
    m_schedule->append(singleTask);
    refreshSchedule();
}

void Schedule::removeSingleTask(const int &taskNumber)
{
    m_schedule->removeAt(taskNumber);
    refreshSchedule();
}

void Schedule::editSingleTask(const ScheduleLine &newTask, const int &editedTaskNumber)
{
    removeSingleTask(editedTaskNumber);
    addSingleTask(newTask);
    refreshSchedule();
}

QString Schedule::getReadingErrors()
{
    return m_readingErrors;
}

void Schedule::clearSchedule()
{
    m_schedule->clear();
}

QString Schedule::getScheduleSavingString()
{
    QString scheduleSavingString;

    scheduleSavingString.append(QString("# Automatic generated schedule file \n\n"));

    for (int index = 0; index < m_schedule->size(); index++) {
        ScheduleLine currentScheduleLine = m_schedule->at(index);
        QStringList taskStringList = currentScheduleLine.generateTaskLine();
        for (int i = 0; i < taskStringList.size(); i++) {
            scheduleSavingString.append(taskStringList[i] + QString("\n"));
        }
        scheduleSavingString.append(QString("\n"));
    }

    return scheduleSavingString;
}

void Schedule::slotSetFileName(const QString &fileName)
{
    QFile scheduleFile(fileName);
    if (scheduleFile.open(QFile::ReadOnly)) {
        scheduleFile.close();
        m_fileName = fileName;
    } else {
        emit signalStatus(QString("Schedule file not found!"));
    }
}

void Schedule::slotLoadSchedule()
{
    m_schedule->clear();
    QFile scheduleFile(m_fileName);
    QList<QStringList> taskList;
    QStringList task;
    bool taskStarted = false;
    bool taskFinished = false;
    if (scheduleFile.open(QFile::ReadOnly)) {
        QByteArray fileByteArray(scheduleFile.readAll());
        scheduleFile.close();
        QTextStream fileTextStream(fileByteArray);
        QString line;
        do {
            line = fileTextStream.readLine();
            if (!(line == "" || line[0] == "#")) {
                if (line[0] == "{") {
                    taskStarted = true;
                    taskFinished = false;
                    line = fileTextStream.readLine();
                } else if (line[0] == "}")
                {
                    taskStarted = false;
                    taskFinished = true;
                }

                if (taskStarted) {
                    task.append(line);
                } else if (taskFinished) {
                    taskList.append(task);
                }
            }
        } while (!line.isNull());
    }

    for (int index = 0; index < taskList.size(); index++)
    {
        ScheduleLine scheduleLine;
        scheduleLine.slotReadScheduleTask(taskList[index]);
        m_schedule->append(scheduleLine);
    }

    refreshSchedule();
}

void Schedule::slotLoadSchedule(const QString &fileName)
{
    slotSetFileName(fileName);
    slotLoadSchedule();
}

bool Schedule::addScheduleTimeTask(ScheduleLine &scheduleLine, const int &taskNumber)
{
    return addScheduleTimeTask(scheduleLine.slotGetTimeBegin(), scheduleLine.slotGetTimeFinish(), taskNumber, scheduleLine.slotGetRepetitionTime());
}

bool Schedule::addScheduleTimeTask(const QTime &beginTime, const QTime &finishTime, const int &number, const int &repeatTime)
{
    bool error = false;
//    QMap<QTime, int> beginTimeSchedule;
//    QMap<QTime, int> finishTimeSchedule;

    QMap<QPair<QTime, QTime>, int> timeSchedule;

//    QList<QTime> beginTimesAlreadyExist = m_beginTimeSchedule->keys();
//    QList<QTime> finishTimesAlreadyExist = m_finishTimeSchedule->keys();

    QList<QPair<QTime, QTime>> timeScheduleExists = m_fullTimeSchedule->keys();

    if (repeatTime == 0) {
        timeSchedule.insert(QPair(QTime::fromString(QString("00:00:00"), QString("hh:mm:ss")), QTime::fromString(QString("00:00:00"), QString("hh:mm:ss"))), number);
    } else if (repeatTime == 24) {
        if (timeScheduleExists.size() > 0) {
            for (int beginTimeNumber = 0; beginTimeNumber < timeScheduleExists.size(); beginTimeNumber++) {
                if (((beginTime > timeScheduleExists.at(beginTimeNumber).first) && (beginTime < timeScheduleExists.at(beginTimeNumber).second)) ||
                        ((finishTime > timeScheduleExists.at(beginTimeNumber).first) && (finishTime < timeScheduleExists.at(beginTimeNumber).second))) {
                    error = true;
                } else {
                    timeSchedule.insert(QPair(beginTime, finishTime), number);
                }
            }
        } else {
            timeSchedule.insert(QPair(beginTime, finishTime), number);
        }
    } else {
        int taskBeginngingHour = beginTime.toString(QString("hh")).toInt();
        for (int currentTimeAdjustment = 0; currentTimeAdjustment + taskBeginngingHour < 24; currentTimeAdjustment += repeatTime) {
            if (timeScheduleExists.size() > 0) {
                for (int beginTimeNumber = 0; beginTimeNumber < timeScheduleExists.size(); beginTimeNumber++) {
                    if (((beginTime > timeScheduleExists.at(beginTimeNumber).first) && (beginTime < timeScheduleExists.at(beginTimeNumber).second)) ||
                            ((finishTime > timeScheduleExists.at(beginTimeNumber).first) && (finishTime < timeScheduleExists.at(beginTimeNumber).second))) {
                        error = true;
                    } else {
                        QTime begTime = beginTime;
                        begTime = begTime.addSecs(currentTimeAdjustment*60*60);
                        QTime finTime = finishTime;
                        finTime = finTime.addSecs(currentTimeAdjustment*60*60);
                        timeSchedule.insert(QPair(begTime, finTime), number);
                    }
                }
            } else {
                QTime begTime = beginTime;
                begTime = begTime.addSecs(currentTimeAdjustment*60*60);
                QTime finTime = finishTime;
                finTime = finTime.addSecs(currentTimeAdjustment*60*60);
                timeSchedule.insert(QPair(begTime, finTime), number);
            }
        }
    }

    m_fullTimeSchedule->insert(timeSchedule);
    return error;
}

void Schedule::sortSchedule()
{
    int change = 1;
    while (change > 0) {
        QList<QTime> beginningTime;

        for (int taskNumber = 0; taskNumber < m_schedule->size(); taskNumber++)
        {
            ScheduleLine currentTask = m_schedule->at(taskNumber);
            beginningTime.append(currentTask.slotGetTimeBegin());
        }

        change = 0;
        for (int taskNumber = 0; taskNumber < beginningTime.size() - 1; taskNumber++)
        {
            if (beginningTime[taskNumber] > beginningTime[taskNumber + 1])
            {
                ScheduleLine bufferTask = m_schedule->at(taskNumber);
                m_schedule->removeAt(taskNumber);
                m_schedule->insert(taskNumber + 1, bufferTask);
                change++;
            }
        }
    }
}

void Schedule::refreshSchedule()
{
    m_readingErrors = QString("");

    m_fullTimeSchedule->clear();

    sortSchedule();

    for (int taskIndex = 0; taskIndex < m_schedule->size(); taskIndex++)
    {
        ScheduleLine currentTask = m_schedule->at(taskIndex);
        if (addScheduleTimeTask(currentTask, taskIndex))
        {
            if (m_readingErrors != QString("")) {
                m_readingErrors += QString(",");
            }
            m_readingErrors += (QString(" "));
            m_readingErrors += currentTask.slotGetTaskName();
        }
    }
    if (m_readingErrors != QString("")) {
        m_readingErrors += QString(";");
    }
}
