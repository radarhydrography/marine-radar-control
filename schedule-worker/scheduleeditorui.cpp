/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "scheduleeditorui.h"

#include <QGridLayout>
#include <QLabel>
#include <QValidator>

ScheduleLine ScheduleEditorUI::getSettingsFromGui()
{
    ScheduleLine scheduleLine;

    scheduleLine.slotSetTaskName(w_taskNameLineEdit->text());
    scheduleLine.slotSetRepetitionTime(w_repetitionTimeSpinBox->value());
    scheduleLine.slotSetTimeBegin(w_beginTimeEdit->time());
    scheduleLine.slotSetTimeFinish(w_finishTimeEdit->time());

    switch (w_radarModeComboBox->currentIndex()) {
    case 0: {
        scheduleLine.slotSetRadarMode(RadarMode::Rotating);
        break;
    }
    case 1: {
        scheduleLine.slotSetRadarMode(RadarMode::Pointing);
        break;
    }
    case 2: {
        scheduleLine.slotSetRadarMode(RadarMode::Sweeping);
        break;
    }
    }

    scheduleLine.slotSetModeRotatingSpeed(w_rotationSpeed->value());
    scheduleLine.slotSetModeAngle(w_angleOneDoubleSpinBox->value(), w_angleTwoDoubleSpinBox->value());

    switch (w_sweepingModeComboBox->currentIndex()) {
    case 0: {
        scheduleLine.slotSetModeSweepingMode(SweepingMode::Short);
        break;
    }
    case 1: {
        scheduleLine.slotSetModeSweepingMode(SweepingMode::Long);
        break;
    }
    }

    switch (w_triggerFrequencyComboBox->currentIndex()) {
    case 0: {
        scheduleLine.slotSetTriggerFrequency(TriggerFrequency::T1kHz);
        break;
    }
    case 1: {
        scheduleLine.slotSetTriggerFrequency(TriggerFrequency::T2kHz);
        break;
    }
    case 2: {
        scheduleLine.slotSetTriggerFrequency(TriggerFrequency::T3kHz);
        break;
    }
    case 3: {
        scheduleLine.slotSetTriggerFrequency(TriggerFrequency::T4kHz);
        break;
    }
    }

    switch (w_unAmpSignalLengthComboBox->currentIndex()) {
    case 0: {
        scheduleLine.slotSetUnAmpSignalLength(UnAmpSignalLength::Length32);
        break;
    }
    case 1: {
        scheduleLine.slotSetUnAmpSignalLength(UnAmpSignalLength::Length64);
        break;
    }
    case 2: {
        scheduleLine.slotSetUnAmpSignalLength(UnAmpSignalLength::Length128);
        break;
    }
    case 3: {
        scheduleLine.slotSetUnAmpSignalLength(UnAmpSignalLength::Length256);
        break;
    }
    }

    switch (w_ampSignalBeginComboBox->currentIndex()) {
    case 0: {
        scheduleLine.slotSetAmpSignalBegin(AmpSignalOption::Datagram1);
        break;
    }
    case 1: {
        scheduleLine.slotSetAmpSignalBegin(AmpSignalOption::Datagram2);
        break;
    }
    case 2: {
        scheduleLine.slotSetAmpSignalBegin(AmpSignalOption::Datagram3);
        break;
    }
    case 3: {
        scheduleLine.slotSetAmpSignalBegin(AmpSignalOption::Datagram4);
        break;
    }
    case 4: {
        scheduleLine.slotSetAmpSignalBegin(AmpSignalOption::Datagram5);
        break;
    }
    case 5: {
        scheduleLine.slotSetAmpSignalBegin(AmpSignalOption::Datagram6);
        break;
    }
    case 6: {
        scheduleLine.slotSetAmpSignalBegin(AmpSignalOption::Datagram7);
        break;
    }
    case 7: {
        scheduleLine.slotSetAmpSignalBegin(AmpSignalOption::Datagram8);
        break;
    }
    }

    switch (w_ampSignalEndComboBox->currentIndex()) {
    case 0: {
        scheduleLine.slotSetAmpSignalEnd(AmpSignalOption::Datagram1);
        break;
    }
    case 1: {
        scheduleLine.slotSetAmpSignalEnd(AmpSignalOption::Datagram2);
        break;
    }
    case 2: {
        scheduleLine.slotSetAmpSignalEnd(AmpSignalOption::Datagram3);
        break;
    }
    case 3: {
        scheduleLine.slotSetAmpSignalEnd(AmpSignalOption::Datagram4);
        break;
    }
    case 4: {
        scheduleLine.slotSetAmpSignalEnd(AmpSignalOption::Datagram5);
        break;
    }
    case 5: {
        scheduleLine.slotSetAmpSignalEnd(AmpSignalOption::Datagram6);
        break;
    }
    case 6: {
        scheduleLine.slotSetAmpSignalEnd(AmpSignalOption::Datagram7);
        break;
    }
    case 7: {
        scheduleLine.slotSetAmpSignalEnd(AmpSignalOption::Datagram8);
        break;
    }
    }

    switch (w_samplingFrequencyComboBox->currentIndex()) {
    case 0: {
        scheduleLine.slotSetSamplingFrequency(SamplingFrequency::Fs1250kHz);
        break;
    }
    case 1: {
        scheduleLine.slotSetSamplingFrequency(SamplingFrequency::Fs2500kHz);
        break;
    }
    case 2: {
        scheduleLine.slotSetSamplingFrequency(SamplingFrequency::Fs5MHz);
        break;
    }
    case 3: {
        scheduleLine.slotSetSamplingFrequency(SamplingFrequency::Fs10MHz);
        break;
    }
    case 4: {
        scheduleLine.slotSetSamplingFrequency(SamplingFrequency::Fs20MHz);
        break;
    }
    case 5: {
        scheduleLine.slotSetSamplingFrequency(SamplingFrequency::Fs40MHz);
        break;
    }
    case 6: {
        scheduleLine.slotSetSamplingFrequency(SamplingFrequency::Fs80MHz);
        break;
    }
    }

    switch (w_maxFileSizeComboBox->currentIndex()) {
    case 0: {
        scheduleLine.slotSetFileSize(FileSize::Size128MB);
        break;
    }
    case 1: {
        scheduleLine.slotSetFileSize(FileSize::Size256MB);
        break;
    }
    case 2: {
        scheduleLine.slotSetFileSize(FileSize::Size512MB);
        break;
    }
    case 3: {
        scheduleLine.slotSetFileSize(FileSize::Size1024MB);
        break;
    }
    case 4: {
        scheduleLine.slotSetFileSize(FileSize::Size2048MB);
        break;
    }
    case 5: {
        scheduleLine.slotSetFileSize(FileSize::Size4096MB);
        break;
    }
    case 6: {
        scheduleLine.slotSetFileSize(FileSize::Size8192MB);
        break;
    }
    case 7: {
        scheduleLine.slotSetFileSize(FileSize::Size16384MB);
        break;
    }
    case 8: {
        scheduleLine.slotSetFileSize(FileSize::Size32768MB);
        break;
    }
    }

    scheduleLine.slotSetBasicFileName(w_basicFileNameLineEdit->text());

    return scheduleLine;
}

void ScheduleEditorUI::setSettingsFromTaskLine(ScheduleLine &scheduleLine)
{
    w_taskNameLineEdit->setText(scheduleLine.slotGetTaskName());
    w_repetitionTimeSpinBox->setValue(scheduleLine.slotGetRepetitionTime());
    w_beginTimeEdit->setTime(scheduleLine.slotGetTimeBegin());
    w_finishTimeEdit->setTime(scheduleLine.slotGetTimeFinish());

    w_radarModeComboBox->setCurrentIndex(static_cast<quint8>(scheduleLine.slotGetRadarMode()));

    w_rotationSpeed->setValue(scheduleLine.slotGetModeRotatingSpeed());
    w_angleOneDoubleSpinBox->setValue(scheduleLine.slotGetModeAngleOne());
    w_angleTwoDoubleSpinBox->setValue(scheduleLine.slotGetModeAngleTwo());

    w_sweepingModeComboBox->setCurrentIndex(static_cast<quint8>(scheduleLine.slotGetModeSweepingMode()));

    w_triggerFrequencyComboBox->setCurrentIndex(static_cast<quint8>(scheduleLine.slotGetTriggerFrequency()));

    w_unAmpSignalLengthComboBox->setCurrentIndex(static_cast<quint8>(scheduleLine.slotGetUnAmpSignalLength()));

    w_ampSignalBeginComboBox->setCurrentIndex(static_cast<quint8>(scheduleLine.slotGetAmpSignalBegin()));

    w_ampSignalEndComboBox->setCurrentIndex(static_cast<quint8>(scheduleLine.slotGetAmpSignalEnd()));

    w_samplingFrequencyComboBox->setCurrentIndex(static_cast<quint8>(scheduleLine.slotGetSamplingFrequency()));

    w_maxFileSizeComboBox->setCurrentIndex(static_cast<quint8>(scheduleLine.slotGetFileSize()));

    w_basicFileNameLineEdit->setText(scheduleLine.slotGetBasicFileName());
}

ScheduleEditorUI::ScheduleEditorUI(QWidget *parent) : QDialog(parent)
{
    QGridLayout *mainLayout = new QGridLayout;

    QFrame *line0 = new QFrame;
    line0->setFrameShape(QFrame::HLine);
    line0->setFrameShadow(QFrame::Sunken);

    QFrame *line1 = new QFrame;
    line1->setFrameShape(QFrame::HLine);
    line1->setFrameShadow(QFrame::Sunken);

    QFrame *line2 = new QFrame;
    line2->setFrameShape(QFrame::HLine);
    line2->setFrameShadow(QFrame::Sunken);

    mainLayout->addWidget(new QLabel(QString("Task Name")), 0, 0);
    mainLayout->addWidget(new QLabel(QString("Rep Time")), 1, 0);
    mainLayout->addWidget(new QLabel(QString("Time Begin")), 2, 0);
    mainLayout->addWidget(new QLabel(QString("Time Finish")), 2, 2);
    mainLayout->addWidget(line0, 3, 0, 1, 4);
    mainLayout->addWidget(new QLabel(QString("Radar Mode")), 4, 0);
    mainLayout->addWidget(new QLabel(QString("Ant Speed")), 5, 0);
    mainLayout->addWidget(new QLabel(QString("Angle One")), 5, 2);
    mainLayout->addWidget(new QLabel(QString("Angle Two")), 6, 2);
    mainLayout->addWidget(new QLabel(QString("Sweep Mode")), 6, 0);
    mainLayout->addWidget(line1, 7, 0, 1, 4);
    mainLayout->addWidget(new QLabel(QString("Trig Freq")), 8, 0);
    mainLayout->addWidget(new QLabel(QString("UnAmp Len")), 8, 2);
    mainLayout->addWidget(new QLabel(QString("Amp Begin")), 9, 0);
    mainLayout->addWidget(new QLabel(QString("Amp End")), 9, 2);
    mainLayout->addWidget(new QLabel(QString("Sampl Freq")), 10, 0);
    mainLayout->addWidget(new QLabel(QString("File Size")), 10, 2);
    mainLayout->addWidget(new QLabel(QString("File Name")), 11, 0);

    w_taskNameLineEdit = new QLineEdit;
    w_taskNameLineEdit->setValidator(new QRegExpValidator(QRegExp("^[A-Za-z0-9](\\S)+$")));

    w_repetitionTimeSpinBox = new QSpinBox;
    w_repetitionTimeSpinBox->setSingleStep(1);
    w_repetitionTimeSpinBox->setMinimum(0);
    w_repetitionTimeSpinBox->setMaximum(24);
    w_repetitionTimeSpinBox->setValue(24);

    w_beginTimeEdit = new QTimeEdit;
    w_beginTimeEdit->setDisplayFormat(QString("hh:mm:ss"));

    w_finishTimeEdit = new QTimeEdit;
    w_finishTimeEdit->setDisplayFormat(QString("hh:mm:ss"));

    w_radarModeComboBox = new QComboBox;
    QStringList radarModeStringList;
    radarModeStringList.append(QString("Rotating"));
    radarModeStringList.append(QString("Pointing"));
    radarModeStringList.append(QString("Sweeping"));
    w_radarModeComboBox->addItems(radarModeStringList);

    w_rotationSpeed = new QSpinBox;
    w_rotationSpeed->setMinimum(0);
    w_rotationSpeed->setMaximum(65535);
    w_rotationSpeed->setValue(0);

    w_angleOneDoubleSpinBox = new QDoubleSpinBox;
    w_angleOneDoubleSpinBox->setMinimum(0.0);
    w_angleOneDoubleSpinBox->setMaximum(360.0);
    w_angleOneDoubleSpinBox->setDecimals(1);
    w_angleOneDoubleSpinBox->setSingleStep(0.1);

    w_angleTwoDoubleSpinBox = new QDoubleSpinBox;
    w_angleTwoDoubleSpinBox->setMinimum(0.0);
    w_angleTwoDoubleSpinBox->setMaximum(360.0);
    w_angleTwoDoubleSpinBox->setDecimals(1);
    w_angleTwoDoubleSpinBox->setSingleStep(0.1);

    w_sweepingModeComboBox = new QComboBox;
    QStringList sweepingModeStringList;
    sweepingModeStringList.append(QString("Short"));
    sweepingModeStringList.append(QString("Long"));
    w_sweepingModeComboBox->addItems(sweepingModeStringList);

    slotRadarModeIndexIsChanged(w_radarModeComboBox->currentIndex());
    connect(w_radarModeComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &ScheduleEditorUI::slotRadarModeIndexIsChanged);

    w_triggerFrequencyComboBox = new QComboBox;
    QStringList triggerFrequencyStringList;
    triggerFrequencyStringList.append(QString("1 kHz"));
    triggerFrequencyStringList.append(QString("2 kHz"));
    triggerFrequencyStringList.append(QString("3 kHz"));
    triggerFrequencyStringList.append(QString("4 kHz"));
    w_triggerFrequencyComboBox->addItems(triggerFrequencyStringList);
    w_triggerFrequencyComboBox->setCurrentIndex(1);

    w_unAmpSignalLengthComboBox = new QComboBox;
    QStringList unAmpSignalLengthStringList;
    unAmpSignalLengthStringList.append(QString("32 Smp"));
    unAmpSignalLengthStringList.append(QString("64 Smp"));
    unAmpSignalLengthStringList.append(QString("128 Smp"));
    unAmpSignalLengthStringList.append(QString("256 Smp"));
    w_unAmpSignalLengthComboBox->addItems(unAmpSignalLengthStringList);

    w_ampSignalBeginComboBox = new QComboBox;
    QStringList ampSignalBeginStringList;
    ampSignalBeginStringList.append(QString("1 Dtg"));
    ampSignalBeginStringList.append(QString("2 Dtg"));
    ampSignalBeginStringList.append(QString("3 Dtg"));
    ampSignalBeginStringList.append(QString("4 Dtg"));
    ampSignalBeginStringList.append(QString("5 Dtg"));
    ampSignalBeginStringList.append(QString("6 Dtg"));
    ampSignalBeginStringList.append(QString("7 Dtg"));
    ampSignalBeginStringList.append(QString("8 Dtg"));
    w_ampSignalBeginComboBox->addItems(ampSignalBeginStringList);

    w_ampSignalEndComboBox = new QComboBox;
    QStringList ampSignalEndStringList;
    ampSignalEndStringList.append(QString("1 Dtg"));
    ampSignalEndStringList.append(QString("2 Dtg"));
    ampSignalEndStringList.append(QString("3 Dtg"));
    ampSignalEndStringList.append(QString("4 Dtg"));
    ampSignalEndStringList.append(QString("5 Dtg"));
    ampSignalEndStringList.append(QString("6 Dtg"));
    ampSignalEndStringList.append(QString("7 Dtg"));
    ampSignalEndStringList.append(QString("8 Dtg"));
    w_ampSignalEndComboBox->addItems(ampSignalEndStringList);
    w_ampSignalEndComboBox->setCurrentIndex(7);

    w_samplingFrequencyComboBox = new QComboBox;
    QStringList samplingFrequencyStringList;
    samplingFrequencyStringList.append(QString("1250 kHz"));
    samplingFrequencyStringList.append(QString("2500 kHz"));
    samplingFrequencyStringList.append(QString("5 MHz"));
    samplingFrequencyStringList.append(QString("10 MHz"));
    samplingFrequencyStringList.append(QString("20 MHz"));
    samplingFrequencyStringList.append(QString("40 MHz"));
    samplingFrequencyStringList.append(QString("80 MHz"));
    w_samplingFrequencyComboBox->addItems(samplingFrequencyStringList);
    w_samplingFrequencyComboBox->setCurrentIndex(6);

    w_maxFileSizeComboBox = new QComboBox;
    QStringList maxFileSizeStringList;
    maxFileSizeStringList.append(QString("128 MB"));
    maxFileSizeStringList.append(QString("256 MB"));
    maxFileSizeStringList.append(QString("512 MB"));
    maxFileSizeStringList.append(QString("1024 MB"));
    maxFileSizeStringList.append(QString("2048 MB"));
    maxFileSizeStringList.append(QString("4096 MB"));
    maxFileSizeStringList.append(QString("8192 MB"));
    maxFileSizeStringList.append(QString("16384 MB"));
    maxFileSizeStringList.append(QString("32768 MB"));
    w_maxFileSizeComboBox->addItems(maxFileSizeStringList);
    w_maxFileSizeComboBox->setCurrentIndex(5);

    w_basicFileNameLineEdit = new QLineEdit;
    w_basicFileNameLineEdit->setValidator(new QRegExpValidator(QRegExp("^[A-Za-z0-9](\\S)+$")));

    mainLayout->addWidget(w_taskNameLineEdit, 0, 1, 1, 3);
    mainLayout->addWidget(w_repetitionTimeSpinBox, 1, 1);
    mainLayout->addWidget(w_beginTimeEdit, 2, 1);
    mainLayout->addWidget(w_finishTimeEdit, 2, 3);
    mainLayout->addWidget(w_radarModeComboBox, 4, 1);
    mainLayout->addWidget(w_rotationSpeed, 5, 1);
    mainLayout->addWidget(w_angleOneDoubleSpinBox, 5, 3);
    mainLayout->addWidget(w_angleTwoDoubleSpinBox, 6, 3);
    mainLayout->addWidget(w_sweepingModeComboBox, 6, 1);
    mainLayout->addWidget(w_triggerFrequencyComboBox, 8, 1);
    mainLayout->addWidget(w_unAmpSignalLengthComboBox, 8, 3);
    mainLayout->addWidget(w_ampSignalBeginComboBox, 9, 1);
    mainLayout->addWidget(w_ampSignalEndComboBox, 9, 3);
    mainLayout->addWidget(w_samplingFrequencyComboBox, 10, 1);
    mainLayout->addWidget(w_maxFileSizeComboBox, 10, 3);
    mainLayout->addWidget(w_basicFileNameLineEdit, 11, 1, 1, 3);

    w_applyButton = new QPushButton(QString("Apply"));
    w_cancelButton = new QPushButton(QString("Cancel"));
    connect(w_applyButton, &QPushButton::clicked, this, &QDialog::accept);
    connect(w_cancelButton, &QPushButton::clicked, this, &QDialog::reject);

    QHBoxLayout *dialogLayout = new QHBoxLayout;
    dialogLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    dialogLayout->addWidget(w_applyButton);
    dialogLayout->addWidget(w_cancelButton);

    mainLayout->addLayout(dialogLayout, 12, 0, 1, 4);

    setLayout(mainLayout);
    setWindowFlag(Qt::Window);
    setWindowTitle(QString("Task Editor"));
}

bool ScheduleEditorUI::createScheduleLine(Schedule &schedule)
{
    if (exec())
    {
        ScheduleLine taskLine;
        taskLine = getSettingsFromGui();
        schedule.addSingleTask(taskLine);
        return true;
    }

    return false;
}

bool ScheduleEditorUI::editSchedulLine(Schedule &schedule, int index)
{
    ScheduleLine editedTaskLine = schedule.getScheduleLine(index);
    setSettingsFromTaskLine(editedTaskLine);

    if (exec())
    {
        editedTaskLine = getSettingsFromGui();
        schedule.editSingleTask(editedTaskLine, index);
        return true;
    }

    return false;
}

bool ScheduleEditorUI::copyScheduleLine(Schedule &schedule, int index)
{
    ScheduleLine editedTaskLine = schedule.getScheduleLine(index);
    setSettingsFromTaskLine(editedTaskLine);

    if (exec())
    {
        editedTaskLine = getSettingsFromGui();
        schedule.addSingleTask(editedTaskLine);
        return true;
    }

    return false;
}

void ScheduleEditorUI::slotRadarModeIndexIsChanged(const int &index)
{
    w_angleOneDoubleSpinBox->setEnabled(false);
    w_angleTwoDoubleSpinBox->setEnabled(false);
    w_sweepingModeComboBox->setEnabled(false);
    if (index > 0) {
        w_angleOneDoubleSpinBox->setEnabled(true);
        w_angleTwoDoubleSpinBox->setEnabled(true);
    }
    if (index > 1) {
        w_sweepingModeComboBox->setEnabled(true);
    }
}
