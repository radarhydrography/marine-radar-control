/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SCHEDULEWORKERUI_H
#define SCHEDULEWORKERUI_H

#include <QObject>
#include <QWidget>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QGroupBox>
#include <QListWidget>
#include <QToolButton>
#include <QCheckBox>
#include <QMessageBox>
#include <QProgressBar>
#include <QTimer>
#include <QHBoxLayout>

#include "schedule.h"
#include "scheduleeditorui.h"
#include "scheduleworker.h"

class ScheduleWorkerUI : public QWidget
{
    Q_OBJECT

    QMap<QString, QString> m_iniSettings;

public:

    explicit ScheduleWorkerUI(QMap<QString, QString> iniSettings = QMap<QString, QString>(), QWidget *parent = nullptr);

    QHBoxLayout *m_statusWidget;

    void setFullPalette(const QPalette &palette);

    void setDefaultSchedule(const QString &fileName, const bool &autostart = false);

private:

    Schedule *m_schedule;
    Schedule *m_scheduleOriginalState;
    QString m_filePath;

    QListWidget *w_scheduleListTable;
    QToolButton *w_saveSchedule;
    QToolButton *w_saveScheduleAs;
    QToolButton *w_editTask;
    QToolButton *w_newTask;
    QToolButton *w_deleteTask;
    QToolButton *w_copyTask;
    QToolButton *w_clearSchedule;
    QLineEdit *w_scheduleName;

    QListWidget *w_taskTextEdit;
    QLineEdit *w_schedulePathLineEdit;
    QToolButton *w_loadSchedulePushButton;
    QToolButton *w_closeSchedule;
    QToolButton *w_createNewSchedule;

    QToolButton *w_undoButton;
    QToolButton *w_redoButton;

    QToolButton *w_startSchedulePushButton;
    QToolButton *w_stopSchedulePushButton;
    QLabel *w_currentTimeLabel;
    QLabel *w_currentDateLabel;
    QLabel *w_currentStatusLabel;
    QProgressBar *w_currentTaskProgressBar;
    QTimer *m_clockTimer;

    QTabWidget *w_viewTabWidget;
    QListWidget *w_scheduleLogList;
    QToolButton *w_clearScheduleLogs;

    QListWidget *w_detailedScheduleList;

    ScheduleEditorUI *w_scheduleEditor;
    ScheduleWorker *m_scheduleWorker;

    const int m_scheduleInfoLines = 4;

    void readSchedule();

    void refreshSchedule();

    QStringList *m_scheduleList;
    QStringList *m_diskDataList;

    void refreshScheduleView();

    void refreshDiskDataView();

    bool m_fileIsAlreadyOpened = false;
    bool m_saveFileFirstTime = false;
    bool m_fileHasChanged = false;
    QCheckBox *w_saveFileFirstTimeAskAgain;
    QCheckBox *w_closeFileAskAgain;
    QCheckBox *w_closeFileSavingAskAgain;
    QCheckBox *w_deleteTaskAskAgain;
    QCheckBox *w_clearScheduleAskAgain;

    QMessageBox *w_fileSavingMsgBox;
    QMessageBox *w_fileClosingMsgBox;
    QMessageBox *w_fileClosingScheduleChangedMsgBox;
    QMessageBox *w_deleteTaskMsgBox;
    QMessageBox *w_clearScheduleMsgBox;

    QList<Schedule> *m_lastScheduleStatementsBuffer;
    int m_scheduleStatementsBufferSize = 10;
    int m_scheduleBufferIterator = 0;
    int m_scheduleBufferSavedStates = 0;

    void addLastStatement(Schedule schedule);
    void resetStatementBuffer();

    QHBoxLayout *scheduleWorkerStatusWidget();

    QString m_savingDirectory = QString("");

    double avgDataSavingSpeed = 0;

public slots:

    void slotAddLogLine(const QString &stringLine);
    void slotWorkingThreadStared(const bool &started);
    void slotSetWorkingStatus(const QString &status);
    void slotSetJobProgress(const int &progress);

    void SlotUpdateDirectoryFolder(const QString &directoryFolder);

private slots:

    void slotOpenFile();
    void slotOpenFile(const QString &filePath);
    void slotLoadTask(int index);
    void slotSaveSchedule();
    void slotSaveScheduleAs();
    void slotCreateNewSchedle();
    void slotCloseSchedule();

    void slotGetStarted();
    void slotStopWork();

    void slotUndoPressed();
    void slotRedoPressed();

    void slotDeleteTaskLine();
    void slotClearSchedule();
    void slotEditTaskLine();
    void slotCopyTaskLine();
    void slotCreateTaskLine();

    void slotLogClearButtonStateChanged(const int &index);
    void slotClearLogs();

    void slotSetFileOpenedStatement(const bool &isOpened);

    void slotScheduleChanged(const bool changed);

    void slotUpdateTime();

    void slotSetCurrentScheduleSettings(const int &scheduleLineNumber);

signals:

    void signalStartTx(const bool &start);
    void signalSetScheduleName(const QString &name);
    void signalScheduleHasEdited(const bool &edited);

    void signalSetAntennaRotating(const bool &isRotating);
    void signalSetTransmitionOn(const bool &isTransmitting);
    void signalSetDataAcquisitionOn(const bool &isSaving);
    void signalScheduleState(const bool &onSchedule);
    void signalSetCurrentScheduleSettings(const ScheduleLine &scheduleLine);

    void signalRadarIsWarmingUp(const bool &radarIsWarmingUp);

    void SignalLogEvent(const QString &logEvent);

    void SignalScheduleTimeCoefficientChanged(const double &coefficient);
};

#endif // SCHEDULEWORKERUI_H
