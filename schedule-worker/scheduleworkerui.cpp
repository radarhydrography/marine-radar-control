/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "scheduleworkerui.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFileDialog>
#include <QGroupBox>
#include <QStorageInfo>

#include "shared-libs/inireader.h"
#include "shared-libs/global.h"

ScheduleWorkerUI::ScheduleWorkerUI(QMap<QString, QString> iniSettings, QWidget *parent) : QWidget(parent)
{
    m_schedule = new Schedule;

    m_scheduleList = new QStringList;
    m_diskDataList = new QStringList;

    if (iniSettings.isEmpty()) {
        m_iniSettings[QString("icons")] = QString("resources/icons/");
    } else {
        m_iniSettings = iniSettings;
    }

    m_lastScheduleStatementsBuffer = new QList<Schedule>;
    for (int i = 0; i < m_scheduleStatementsBufferSize; i++) {
        m_lastScheduleStatementsBuffer->append(Schedule());
    }

    m_scheduleOriginalState = new Schedule;

    QVBoxLayout *scheduleWorkerLayout = new QVBoxLayout;

    w_loadSchedulePushButton = new QToolButton;
    w_loadSchedulePushButton->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("folder.svg")));
    w_loadSchedulePushButton->setToolTip(QString("Open Schedule"));
    w_undoButton = new QToolButton;
    w_redoButton = new QToolButton;
    w_undoButton->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-undo.svg")));
    w_undoButton->setToolTip(QString("Undo"));
    w_redoButton->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-redo.svg")));
    w_redoButton->setToolTip(QString("Redo"));
    w_scheduleListTable = new QListWidget;
    w_scheduleListTable->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred));
    QVBoxLayout *scheduleListLayout = new QVBoxLayout;

    w_scheduleName = new QLineEdit;
    w_scheduleName->setReadOnly(true);

    scheduleListLayout->addWidget(w_scheduleListTable);

    QHBoxLayout *scheduleEditorLayout = new QHBoxLayout;
    w_saveSchedule = new QToolButton;
    w_saveSchedule->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("document-save.svg")));
    w_saveSchedule->setToolTip(QString("Save Schedule"));
    w_saveScheduleAs = new QToolButton;
    w_saveScheduleAs->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("document-save-as.svg")));
    w_saveScheduleAs->setToolTip(QString("Save Schedule As"));
    w_newTask = new QToolButton;
    w_newTask->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("document-new.svg")));
    w_newTask->setToolTip(QString("New Task Line"));
    w_copyTask = new QToolButton;
    w_copyTask->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-copy.svg")));
    w_copyTask->setToolTip(QString("Copy Task Line"));
    w_deleteTask = new QToolButton;
    w_editTask = new QToolButton;
    w_editTask->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("document-edit.svg")));
    w_editTask->setToolTip(QString("Edit Task Line"));
    w_deleteTask->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-clear-all.svg")));
    w_deleteTask->setToolTip(QString("Delete Task Line"));
    w_clearSchedule = new QToolButton;
    w_clearSchedule->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-delete.svg")));
    w_clearSchedule->setToolTip(QString("Clear Schedule"));
    w_closeSchedule = new QToolButton;
    w_closeSchedule->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-clear.svg")));
    w_closeSchedule->setToolTip(QString("Close Schedule"));
    w_createNewSchedule = new QToolButton;
    w_createNewSchedule->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("appointment-new.svg")));
    w_createNewSchedule->setToolTip(QString("Create A New Schedule"));

    scheduleEditorLayout->addWidget(w_loadSchedulePushButton);
    scheduleEditorLayout->addWidget(w_saveSchedule);
    scheduleEditorLayout->addWidget(w_saveScheduleAs);
    scheduleEditorLayout->addWidget(w_createNewSchedule);
    scheduleEditorLayout->addWidget(w_closeSchedule);
    scheduleEditorLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    scheduleEditorLayout->addWidget(w_undoButton);
    scheduleEditorLayout->addWidget(w_redoButton);
    scheduleEditorLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    scheduleEditorLayout->addWidget(w_newTask);
    scheduleEditorLayout->addWidget(w_copyTask);
    scheduleEditorLayout->addWidget(w_editTask);
    scheduleEditorLayout->addWidget(w_deleteTask);
    scheduleEditorLayout->addWidget(w_clearSchedule);

    w_startSchedulePushButton = new QToolButton;
    w_stopSchedulePushButton = new QToolButton;
    w_startSchedulePushButton->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("play.svg")));
    w_startSchedulePushButton->setToolTip(QString("Get Started"));
    w_stopSchedulePushButton->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("stop.svg")));
    w_stopSchedulePushButton->setToolTip(QString("Stop Work"));

    w_viewTabWidget = new QTabWidget;
    w_viewTabWidget->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

    w_taskTextEdit = new QListWidget;
    w_viewTabWidget->addTab(w_taskTextEdit, QString("Task View"));

    w_detailedScheduleList = new QListWidget;
    w_viewTabWidget->addTab(w_detailedScheduleList, QString("Schedule View"));

    w_scheduleLogList = new QListWidget;
    w_viewTabWidget->addTab(w_scheduleLogList, QString("Schedule Log"));

    w_clearScheduleLogs = new QToolButton;
    w_clearScheduleLogs->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-clear.svg")));
    w_clearScheduleLogs->setToolTip(QString("Clear"));

    w_viewTabWidget->setCornerWidget(w_clearScheduleLogs, Qt::TopRightCorner);

    slotLogClearButtonStateChanged(w_viewTabWidget->currentIndex());

    scheduleWorkerLayout->addLayout(scheduleEditorLayout);
    scheduleWorkerLayout->addLayout(scheduleListLayout);
    scheduleWorkerLayout->addWidget(w_viewTabWidget);

    setLayout(scheduleWorkerLayout);

    m_statusWidget = scheduleWorkerStatusWidget();

    connect(w_scheduleListTable, &QListWidget::currentRowChanged, this, &ScheduleWorkerUI::slotLoadTask);

    connect(w_loadSchedulePushButton, &QToolButton::clicked, this, QOverload<>::of(&ScheduleWorkerUI::slotOpenFile));
    connect(w_saveSchedule, &QPushButton::clicked, this, &ScheduleWorkerUI::slotSaveSchedule);
    connect(w_saveScheduleAs, &QPushButton::clicked, this, &ScheduleWorkerUI::slotSaveScheduleAs);
    connect(w_createNewSchedule, &QPushButton::clicked, this, &ScheduleWorkerUI::slotCreateNewSchedle);
    connect(w_closeSchedule, &QPushButton::clicked, this, &ScheduleWorkerUI::slotCloseSchedule);

    connect(w_startSchedulePushButton, &QPushButton::clicked, this, &ScheduleWorkerUI::slotGetStarted);
    connect(w_stopSchedulePushButton, &QPushButton::clicked, this, &ScheduleWorkerUI::slotStopWork);

    connect(w_undoButton, &QPushButton::clicked, this, &ScheduleWorkerUI::slotUndoPressed);
    connect(w_redoButton, &QPushButton::clicked, this, &ScheduleWorkerUI::slotRedoPressed);

    connect(w_newTask, &QPushButton::clicked, this, &ScheduleWorkerUI::slotCreateTaskLine);
    connect(w_copyTask, &QPushButton::clicked, this, &ScheduleWorkerUI::slotCopyTaskLine);
    connect(w_editTask, &QPushButton::clicked, this, &ScheduleWorkerUI::slotEditTaskLine);
    connect(w_deleteTask, &QPushButton::clicked, this, &ScheduleWorkerUI::slotDeleteTaskLine);
    connect(w_clearSchedule, &QPushButton::clicked, this, &ScheduleWorkerUI::slotClearSchedule);

    connect(w_clearScheduleLogs, &QPushButton::clicked, this, &ScheduleWorkerUI::slotClearLogs);

    connect(w_viewTabWidget, &QTabWidget::currentChanged, this, &ScheduleWorkerUI::slotLogClearButtonStateChanged);

    w_scheduleEditor = new ScheduleEditorUI;

    slotSetFileOpenedStatement(false);

    w_saveFileFirstTimeAskAgain = new QCheckBox(QString("Ask me again next time"));
    w_saveFileFirstTimeAskAgain->setChecked(true);
    w_closeFileAskAgain = new QCheckBox(QString("Ask me again next time"));
    w_closeFileAskAgain->setChecked(true);
    w_closeFileSavingAskAgain = new QCheckBox(QString("Ask me again next time"));
    w_closeFileSavingAskAgain->setChecked(true);
    w_deleteTaskAskAgain = new QCheckBox(QString("Ask me again next time"));
    w_deleteTaskAskAgain->setChecked(true);
    w_clearScheduleAskAgain = new QCheckBox(QString("Ask me again next time"));
    w_clearScheduleAskAgain->setChecked(true);

    w_fileSavingMsgBox = new QMessageBox;
    w_fileSavingMsgBox->setWindowTitle(QString("Achtung!"));
    w_fileSavingMsgBox->setText("Shcedule has been changed");
    w_fileSavingMsgBox->setInformativeText("Do you want to save your changes at the same file?");
    w_fileSavingMsgBox->setStandardButtons(QMessageBox::Save | QMessageBox::Cancel | QMessageBox::SaveAll);
    w_fileSavingMsgBox->setCheckBox(w_saveFileFirstTimeAskAgain);

    w_fileClosingMsgBox = new QMessageBox;
    w_fileClosingMsgBox->setWindowTitle(QString("Achtung!"));
    w_fileClosingMsgBox->setText("Do you want to close the schedule?");
    w_fileClosingMsgBox->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    w_fileClosingMsgBox->setCheckBox(w_closeFileAskAgain);

    w_fileClosingScheduleChangedMsgBox = new QMessageBox;
    w_fileClosingScheduleChangedMsgBox->setWindowTitle(QString("Achtung!"));
    w_fileClosingScheduleChangedMsgBox->setText("Shcedule has been chenged");
    w_fileClosingScheduleChangedMsgBox->setInformativeText("Do you want to save your changes?");
    w_fileClosingScheduleChangedMsgBox->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel | QMessageBox::No);
    w_fileClosingScheduleChangedMsgBox->setCheckBox(w_closeFileSavingAskAgain);

    w_deleteTaskMsgBox = new QMessageBox;
    w_deleteTaskMsgBox->setWindowTitle(QString("Achtung!"));
    w_deleteTaskMsgBox->setText("Are you sure you want to delete this line: ");
    w_deleteTaskMsgBox->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    w_deleteTaskMsgBox->setCheckBox(w_deleteTaskAskAgain);

    w_clearScheduleMsgBox = new QMessageBox;
    w_clearScheduleMsgBox->setWindowTitle(QString("Achtung!"));
    w_clearScheduleMsgBox->setText("Are you sure you want to delete the schedule: ");
    w_clearScheduleMsgBox->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    w_clearScheduleMsgBox->setCheckBox(w_clearScheduleAskAgain);

    slotScheduleChanged(false);

    m_clockTimer = new QTimer;
    connect(m_clockTimer, &QTimer::timeout, this, &ScheduleWorkerUI::slotUpdateTime);
    m_clockTimer->start(100);

    m_scheduleWorker = new ScheduleWorker;
    connect(m_scheduleWorker, &ScheduleWorker::signalAddLogLine, this, &ScheduleWorkerUI::slotAddLogLine);
    connect(m_scheduleWorker, &ScheduleWorker::poolStatus, this, &ScheduleWorkerUI::slotWorkingThreadStared);
    connect(m_scheduleWorker, &ScheduleWorker::signalSetWorkingStatus, this, &ScheduleWorkerUI::slotSetWorkingStatus);
    connect(m_scheduleWorker, &ScheduleWorker::signalSetJobProgress, this, &ScheduleWorkerUI::slotSetJobProgress);

    w_startSchedulePushButton->setEnabled(false);
    w_stopSchedulePushButton->setEnabled(false);

    connect(m_scheduleWorker, &ScheduleWorker::signalSetAntennaRotating, this, &ScheduleWorkerUI::signalSetAntennaRotating);
    connect(m_scheduleWorker, &ScheduleWorker::signalSetTransmitionOn, this, &ScheduleWorkerUI::signalSetTransmitionOn);
    connect(m_scheduleWorker, &ScheduleWorker::signalSetDataAcquisitionOn, this, &ScheduleWorkerUI::signalSetDataAcquisitionOn);
    connect(m_scheduleWorker, &ScheduleWorker::signalSetCurrentScheduleSettings, this, &ScheduleWorkerUI::slotSetCurrentScheduleSettings);
    connect(this, &ScheduleWorkerUI::signalRadarIsWarmingUp, m_scheduleWorker, &ScheduleWorker::slotRadarIsWarmingUp);

    connect(m_scheduleWorker, &ScheduleWorker::SignalLogEvent, this, &ScheduleWorkerUI::SignalLogEvent);
}

void ScheduleWorkerUI::setFullPalette(const QPalette &palette)
{
    setPalette(palette);
    w_scheduleEditor->setPalette(palette);
}

void ScheduleWorkerUI::setDefaultSchedule(const QString &fileName, const bool &autostart)
{
    slotOpenFile(fileName);
    m_filePath = fileName;
    if (autostart) {
        slotGetStarted();
    }
}

void ScheduleWorkerUI::readSchedule()
{
    for (int taskNumber = 0; taskNumber < m_schedule->size(); taskNumber++)
    {
        ScheduleLine currentLine = m_schedule->getScheduleLine(taskNumber);
        QString currentTask;
        currentTask += QString::number(taskNumber + 1);
        currentTask += QString(".  ");
        currentTask += currentLine.slotGetTimeBegin().toString(QString("hh:mm:ss"));
        currentTask += QString(" - ");
        currentTask += currentLine.slotGetTimeFinish().toString(QString("hh:mm:ss"));
        currentTask += QString("\t(");
        currentTask += QString::number(currentLine.slotGetRepetitionTime());
        currentTask += QString("h)");
        currentTask += QString("\t");
        currentTask += currentLine.slotGetTaskName();
        currentTask += QString(";");
        m_scheduleList->append(currentTask);
    }
    if (m_schedule->getReadingErrors() != QString("")) {
        m_scheduleList->append(QString(""));
        m_scheduleList->append(QString("Conflict Tasks:") + m_schedule->getReadingErrors());
    }
}

void ScheduleWorkerUI::refreshSchedule()
{
    if (m_filePath != QString("")) {
        m_scheduleList->clear();
        m_scheduleList->append(QString("File Path:"));
        m_scheduleList->append(m_filePath);
        m_scheduleList->append(QString(""));
        m_scheduleList->append(QString("Schedule:"));
        QFileInfo file(m_filePath);
        emit signalSetScheduleName(file.fileName() + QString("*"));
        readSchedule();
        refreshScheduleView();
    }
    w_detailedScheduleList->clear();
    w_detailedScheduleList->addItems(m_schedule->getDetailedSchedule());
}

void ScheduleWorkerUI::refreshScheduleView()
{
    w_scheduleListTable->clear();
    w_scheduleListTable->addItems(*m_scheduleList);
    w_scheduleListTable->addItems(QStringList(""));
    w_scheduleListTable->setToolTip(m_filePath);
}

void ScheduleWorkerUI::refreshDiskDataView()
{
    for (int i = 0; i < m_diskDataList->size(); i++) {
        w_scheduleListTable->item(m_scheduleList->size() + i)->setText(m_diskDataList->at(i));
    }
}

void ScheduleWorkerUI::addLastStatement(Schedule schedule)
{
    if (m_scheduleBufferIterator > 0) {
        m_scheduleBufferIterator = 0;
        m_scheduleBufferSavedStates = 1;
    } else {
        m_scheduleBufferSavedStates = m_scheduleBufferSavedStates >= (m_scheduleStatementsBufferSize - 1) ? m_scheduleStatementsBufferSize : (m_scheduleBufferSavedStates + 1);
    }

    m_lastScheduleStatementsBuffer->removeAt(0);
    m_lastScheduleStatementsBuffer->append(schedule);

    w_undoButton->setEnabled(true);
    w_redoButton->setEnabled(false);
}

void ScheduleWorkerUI::resetStatementBuffer()
{
    slotScheduleChanged(false);
    m_scheduleBufferSavedStates = 0;
}

QHBoxLayout *ScheduleWorkerUI::scheduleWorkerStatusWidget()
{
//    QGroupBox *statusWidget = new QGroupBox(QString("Schedule Status"));
//    QWidget *statusWidget = new QWidget;

    w_currentTaskProgressBar = new QProgressBar;
    QFont font;
    QDateTime dateTime = QDateTime::currentDateTime();
    QString date = dateTime.toString(QString("dd.MM.yyyy  "));
    w_currentDateLabel = new QLabel;
    date = dateTime.toString(QString("dd.MM.yyyy  "));
    w_currentDateLabel->setText(date);
    font.setBold(true);
    font.setItalic(true);
    w_currentDateLabel->setFont(font);

    w_currentTimeLabel = new QLabel;
    date = dateTime.toString(QString("hh:mm:ss"));
    w_currentTimeLabel->setText(date);
    font.setBold(false);
    w_currentTimeLabel->setFont(font);
    w_currentStatusLabel = new QLabel(QString("Idle"));

    QHBoxLayout *progressLayout = new QHBoxLayout;
    progressLayout->addWidget(w_currentTimeLabel);
    progressLayout->addWidget(w_currentDateLabel);
    progressLayout->addWidget(w_currentTaskProgressBar);
    progressLayout->addWidget(new QLabel(QString("Status:")));
    progressLayout->addWidget(w_currentStatusLabel);
    progressLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    progressLayout->addWidget(w_startSchedulePushButton);
    progressLayout->addWidget(w_stopSchedulePushButton);

//    statusWidget->setLayout(progressLayout);

    return progressLayout;
}

void ScheduleWorkerUI::slotAddLogLine(const QString &stringLine)
{
    w_scheduleLogList->insertItem(0, QTime::currentTime().toString(QString("hh:mm:ss")) + QString(": ") + stringLine);
}

void ScheduleWorkerUI::slotWorkingThreadStared(const bool &started)
{
    w_startSchedulePushButton->setEnabled(!started);
    w_stopSchedulePushButton->setEnabled(started);

    w_saveSchedule->setEnabled(!started);
    w_saveScheduleAs->setEnabled(!started);
    w_editTask->setEnabled(!started);
    w_newTask->setEnabled(!started);
    w_deleteTask->setEnabled(!started);
    w_copyTask->setEnabled(!started);
    w_clearSchedule->setEnabled(!started);

    w_loadSchedulePushButton->setEnabled(!started);
    w_closeSchedule->setEnabled(!started);
    w_createNewSchedule->setEnabled(!started);

    w_undoButton->setEnabled(!started);
    w_redoButton->setEnabled(!started);

    w_clearScheduleLogs->setEnabled(!started);

    emit signalScheduleState(started);
}

void ScheduleWorkerUI::slotSetWorkingStatus(const QString &status)
{
    w_currentStatusLabel->setText(status);
}

void ScheduleWorkerUI::slotSetJobProgress(const int &progress)
{
    w_currentTaskProgressBar->setValue(progress);
}

void ScheduleWorkerUI::SlotUpdateDirectoryFolder(const QString &directoryFolder)
{
    if (m_fileIsAlreadyOpened) {

        m_savingDirectory = directoryFolder;

        QStorageInfo fileInfo;

        QString filePath = QString("");
        QDir dir;

        QString refreshFilePathToolTip = QString("Refresh Path From The List");

        filePath = directoryFolder;
        if (filePath != QString("") && dir.exists(filePath)) {
            fileInfo.setPath(filePath);
            double scheduleTimeAvalibale = m_schedule->m_scheduleAvgDataSavingSpeed;
            QString scheduleTimeAvaliableString;
            if (scheduleTimeAvalibale == 0.0) {
                scheduleTimeAvaliableString = QString("NaN");
            } else {
            scheduleTimeAvalibale = static_cast<double>(fileInfo.bytesFree()*(1.0 - m_iniSettings["FreeDiskSpaceStopSaving"].toDouble()/100.0))/scheduleTimeAvalibale;
            scheduleTimeAvaliableString = QString::number(static_cast<int>(scheduleTimeAvalibale/(60*60*24))) + QString("d ");
            scheduleTimeAvalibale = scheduleTimeAvalibale - static_cast<int>(scheduleTimeAvalibale/(60*60*24))*60*60*24;
            scheduleTimeAvaliableString += QString::number(static_cast<int>(scheduleTimeAvalibale/(60*60))) + QString("h ");
            scheduleTimeAvalibale = scheduleTimeAvalibale - static_cast<int>(scheduleTimeAvalibale/(60*60))*60*60;
            scheduleTimeAvaliableString += QString::number(static_cast<int>(scheduleTimeAvalibale/(60))) + QString("m ");
            scheduleTimeAvalibale = scheduleTimeAvalibale - static_cast<int>(scheduleTimeAvalibale/(60))*60;
            scheduleTimeAvaliableString += QString::number(static_cast<int>(scheduleTimeAvalibale)) + QString("s ");
            }

            QString savingDirectory = QString("\nSaving Directory: ") + filePath +
                    QString("\n") + scheduleTimeAvaliableString + QString("before hard drive is full") +
                    QString("\n") + QString::number(fileInfo.bytesFree()/(1024*1024*1024)) + QString("/") +
                    QString::number(fileInfo.bytesTotal()/(1024*1024*1024)) + QString(" GB available");
            refreshFilePathToolTip.append(QString("\n") + savingDirectory);
            m_diskDataList->clear();
            m_diskDataList->append(savingDirectory);
            if (!m_scheduleList->isEmpty()) {
                refreshDiskDataView();
            }
            emit SignalScheduleTimeCoefficientChanged(m_schedule->m_scheduleAvgDataSavingSpeed);
        }

    } else {
        m_diskDataList->clear();
        refreshScheduleView();
        emit SignalScheduleTimeCoefficientChanged(-1);
    }

}

void ScheduleWorkerUI::slotOpenFile()
{
    int ret = QMessageBox::No;

    if (m_fileHasChanged && w_closeFileSavingAskAgain) {
        ret = w_fileClosingScheduleChangedMsgBox->exec();
    }

    if (ret == QMessageBox::Yes) {
        slotSaveSchedule();
    }

    if (ret != QMessageBox::Cancel) {
        QString filePath = QFileDialog::getOpenFileName(this, QString("Open Schedule File"), QString(""), QString("*.sc"), nullptr, QFileDialog::DontUseNativeDialog);
        if (filePath != QString("")) {
            m_filePath = filePath;
        }
        slotOpenFile(m_filePath);
    }
}

void ScheduleWorkerUI::slotOpenFile(const QString &filePath)
{
    if (filePath != QString("")) {
        m_scheduleList->clear();
        m_scheduleList->append(QString("File Path:"));
        m_scheduleList->append(filePath);
        m_scheduleList->append(QString(""));
        m_scheduleList->append(QString("Schedule:"));
        QFileInfo file(filePath);
        emit signalSetScheduleName(file.fileName());
        m_schedule->slotLoadSchedule(filePath);
        readSchedule();
        slotSetFileOpenedStatement(true);

        m_fileIsAlreadyOpened = true;
        m_saveFileFirstTime = true;
        resetStatementBuffer();
        w_detailedScheduleList->clear();
        w_detailedScheduleList->addItems(m_schedule->getDetailedSchedule());

        refreshScheduleView();
    }
}

void ScheduleWorkerUI::slotLoadTask(int index)
{
    w_taskTextEdit->clear();
    if ((index >= m_scheduleInfoLines) && (index < m_schedule->size() + m_scheduleInfoLines)) {
        ScheduleLine task = m_schedule->getScheduleLine(index - m_scheduleInfoLines);
        w_taskTextEdit->addItems(task.generateScheduleLine());
    }
}

void ScheduleWorkerUI::slotSaveSchedule()
{
    int ret = QMessageBox::Save;
    if (m_saveFileFirstTime && w_saveFileFirstTimeAskAgain->isChecked() && m_fileHasChanged) {
        ret = w_fileSavingMsgBox->exec();
    }

    if ((m_filePath != QString("")) && (ret == QMessageBox::Save)) {
        QString savingString = m_schedule->getScheduleSavingString();
        QFile file(m_filePath);
        file.open(QFile::WriteOnly);
        file.write(savingString.toUtf8());
        file.close();
        QFileInfo fileInfo(m_filePath);
        emit signalSetScheduleName(fileInfo.fileName());
        slotOpenFile(m_filePath);
        m_saveFileFirstTime = false;
        m_fileIsAlreadyOpened = true;
        resetStatementBuffer();
    } else if (ret == QMessageBox::SaveAll) {
        slotSaveScheduleAs();
    }
}

void ScheduleWorkerUI::slotSaveScheduleAs()
{
    QString filePath = QFileDialog::getSaveFileName(this, QString("Create New Schedule File"), QString(".sc"), QString(".sc"), nullptr, QFileDialog::DontUseNativeDialog);
    if (filePath != QString("")) {
        m_filePath = filePath;
        m_filePath.remove(".sc");
        m_filePath += (".sc");
        QFile file((QString(m_filePath)));
        file.open(QFile::Append);
        QString fileHeader = QString("Schedule Automatic Generated File");
        file.write(fileHeader.toUtf8());
        if (file.isOpen()) {
            file.close();
        }
        slotOpenFile(m_filePath);
        m_saveFileFirstTime = false;
        m_fileIsAlreadyOpened = true;
        resetStatementBuffer();
    }
    slotSaveSchedule();
}

void ScheduleWorkerUI::slotCreateNewSchedle()
{
    int ret = QMessageBox::No;

    if (m_fileHasChanged && w_closeFileSavingAskAgain) {
        ret = w_fileClosingScheduleChangedMsgBox->exec();
    }

    if (ret == QMessageBox::Yes) {
        slotSaveSchedule();
    }

    if (ret != QMessageBox::Cancel) {
        QString filePath = QFileDialog::getSaveFileName(this, QString("Create New Schedule File"), QString(".sc"), QString(".sc"), nullptr, QFileDialog::DontUseNativeDialog);
        if (filePath != QString("")) {
            m_filePath = filePath;
        }
        if (m_filePath != QString("")) {
            m_filePath.remove(".sc");
            m_filePath += (".sc");
            QFile file((QString(m_filePath)));
            file.open(QFile::Append);
            QString fileHeader = QString("Schedule Automatic Generated File");
            file.write(fileHeader.toUtf8());
            if (file.isOpen()) {
                file.close();
                m_scheduleList->clear();
                m_scheduleList->append(QString("File Path:"));
                m_scheduleList->append(m_filePath);
                m_scheduleList->append(QString(""));
                m_scheduleList->append(QString("Schedule:"));
                QFileInfo fileInfo(m_filePath);
                emit signalSetScheduleName(fileInfo.fileName());
                m_schedule->slotLoadSchedule(m_filePath);
                readSchedule();
                slotSetFileOpenedStatement(true);
                m_fileIsAlreadyOpened = true;
                m_saveFileFirstTime = false;
                resetStatementBuffer();
                refreshScheduleView();
            }
        }
        w_detailedScheduleList->clear();
        w_detailedScheduleList->addItems(m_schedule->getDetailedSchedule());
    }
}

void ScheduleWorkerUI::slotCloseSchedule()
{
    int ret = QMessageBox::Yes;

    if (w_closeFileAskAgain->isChecked()) {
        ret = w_fileClosingMsgBox->exec();
    }

    if (m_fileHasChanged && w_closeFileSavingAskAgain && (ret != QMessageBox::Cancel)) {
        ret = w_fileClosingScheduleChangedMsgBox->exec();
        if (ret == QMessageBox::Yes) {
            slotSaveSchedule();
        }
    }

    if (ret != QMessageBox::Cancel) {
        m_fileIsAlreadyOpened = false;
        resetStatementBuffer();
        slotClearSchedule();
        m_scheduleList->clear();
        emit signalSetScheduleName(QString(""));
        slotSetFileOpenedStatement(false);
        m_fileHasChanged = false;
        m_schedule->clear();
        m_filePath = QString("");
        m_diskDataList->clear();
        refreshScheduleView();
    }
}

void ScheduleWorkerUI::slotGetStarted()
{
    m_scheduleWorker->startWorkingThread(*m_schedule);
}

void ScheduleWorkerUI::slotStopWork()
{
    m_scheduleWorker->stopWorkingThread();
}

void ScheduleWorkerUI::slotUndoPressed()
{
    m_scheduleBufferIterator++;
    w_undoButton->setEnabled(m_scheduleBufferIterator < m_scheduleBufferSavedStates);
    w_redoButton->setEnabled(m_scheduleBufferIterator > 0);

    if (m_scheduleBufferIterator == 1) {
        m_scheduleOriginalState->setSchedule(*m_schedule);
    }

//    if (m_scheduleBufferIterator == m_scheduleStatementsBufferSize) {
//        m_fileHasChanged = false;
//        QFileInfo fileInfo(m_filePath);
//        emit signalSetScheduleName(fileInfo.fileName());
//    } else {
//        m_fileHasChanged = false;
//        QFileInfo fileInfo(m_filePath);
//        emit signalSetScheduleName(fileInfo.fileName());
//    }

    Schedule currentSchedule = Schedule(m_lastScheduleStatementsBuffer->at(m_scheduleStatementsBufferSize - m_scheduleBufferIterator));
    m_schedule->setSchedule(currentSchedule);
    refreshSchedule();
}

void ScheduleWorkerUI::slotRedoPressed()
{
    m_scheduleBufferIterator--;
    w_undoButton->setEnabled(m_scheduleBufferIterator < m_scheduleBufferSavedStates);
    w_redoButton->setEnabled(m_scheduleBufferIterator > 0);

    if (m_scheduleBufferIterator == 0) {
        m_schedule->setSchedule(*m_scheduleOriginalState);
    } else {
        Schedule currentSchedule = Schedule(m_lastScheduleStatementsBuffer->at(m_scheduleStatementsBufferSize - m_scheduleBufferIterator));
        m_schedule->setSchedule(currentSchedule);
    }
    refreshSchedule();
}

void ScheduleWorkerUI::slotDeleteTaskLine()
{
    int index = w_scheduleListTable->currentRow() - m_scheduleInfoLines;
    if ((index >= 0) && (index < m_schedule->size())) {
        int ret = QMessageBox::Yes;
        if (w_deleteTaskAskAgain->isChecked()) {
            w_deleteTaskMsgBox->setInformativeText(w_scheduleListTable->item(w_scheduleListTable->currentRow())->text().remove(QString(";")) + QString("?"));
            ret = w_deleteTaskMsgBox->exec();
        }

        if (ret != QMessageBox::Cancel) {

            Schedule scheduleLastStatement;
            scheduleLastStatement.setSchedule(*m_schedule);
            addLastStatement(scheduleLastStatement);
            m_schedule->removeSingleTask(index);
            refreshSchedule();
            slotScheduleChanged(true);
        }
    }
}

void ScheduleWorkerUI::slotClearSchedule()
{
    int ret = QMessageBox::Yes;
    if (w_clearScheduleAskAgain->isChecked() && m_fileIsAlreadyOpened) {
        QFileInfo file(m_filePath);
        w_clearScheduleMsgBox->setInformativeText(file.fileName());
        ret = w_clearScheduleMsgBox->exec();
    }

    if (ret != QMessageBox::Cancel) {
        Schedule scheduleLastStatement;
        scheduleLastStatement.setSchedule(*m_schedule);
        addLastStatement(scheduleLastStatement);
        while (m_schedule->size()) {
            m_schedule->removeSingleTask(0);
            slotScheduleChanged(true);
        }
        refreshSchedule();
    }
}

void ScheduleWorkerUI::slotEditTaskLine()
{
    int index = w_scheduleListTable->currentRow() - m_scheduleInfoLines;
    if ((index >= 0) && (index < m_schedule->size())) {
        Schedule scheduleLastStatement;
        scheduleLastStatement.setSchedule(*m_schedule);
        if (w_scheduleEditor->editSchedulLine(*m_schedule, index)) {
            addLastStatement(scheduleLastStatement);
            refreshSchedule();
            slotScheduleChanged(true);
        }
    }
}

void ScheduleWorkerUI::slotCopyTaskLine()
{
    int index = w_scheduleListTable->currentRow() - m_scheduleInfoLines;
    if ((index >= 0) && (index < m_schedule->size())) {
        Schedule scheduleLastStatement;
        scheduleLastStatement.setSchedule(*m_schedule);
        if (w_scheduleEditor->copyScheduleLine(*m_schedule, index)) {
            addLastStatement(scheduleLastStatement);
            refreshSchedule();
            slotScheduleChanged(true);
        }
    }
}

void ScheduleWorkerUI::slotCreateTaskLine()
{
    Schedule scheduleLastStatement;
    scheduleLastStatement.setSchedule(*m_schedule);
    if (w_scheduleEditor->createScheduleLine(*m_schedule)) {
        addLastStatement(scheduleLastStatement);
        refreshSchedule();
        slotScheduleChanged(true);
    }
}

void ScheduleWorkerUI::slotLogClearButtonStateChanged(const int &index)
{
    bool visible = index == 2 ? true : false;
    w_clearScheduleLogs->setVisible(visible);
}

void ScheduleWorkerUI::slotClearLogs()
{
    w_scheduleLogList->clear();
}

void ScheduleWorkerUI::slotSetFileOpenedStatement(const bool &isOpened)
{
    w_newTask->setEnabled(isOpened);
    w_editTask->setEnabled(isOpened);
    w_copyTask->setEnabled(isOpened);
    w_deleteTask->setEnabled(isOpened);
    w_clearSchedule->setEnabled(isOpened);
    w_saveSchedule->setEnabled(isOpened);
    w_saveScheduleAs->setEnabled(isOpened);
    w_closeSchedule->setEnabled(isOpened);
    w_startSchedulePushButton->setEnabled(isOpened);
}

void ScheduleWorkerUI::slotScheduleChanged(const bool changed)
{
    m_fileHasChanged = changed;
    w_undoButton->setEnabled(changed);
    w_redoButton->setEnabled(false);
    emit signalScheduleHasEdited(changed);
}

void ScheduleWorkerUI::slotUpdateTime()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    w_currentDateLabel->setText(dateTime.toString(QString("dd.MM.yyyy  ")));
    w_currentTimeLabel->setText(dateTime.toString(QString("hh:mm:ss")));
}

void ScheduleWorkerUI::slotSetCurrentScheduleSettings(const int &scheduleLineNumber)
{
    emit signalSetCurrentScheduleSettings(m_schedule->getScheduleLine(scheduleLineNumber));
}
