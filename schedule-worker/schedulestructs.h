/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SCHEDULESTRUCTS_H
#define SCHEDULESTRUCTS_H

#include <QObject>

enum struct RadarMode
{
    Rotating        =   0,
    Pointing        =   1,
    Sweeping        =   2
};


//template <class T>
//struct UInt16Parameter
//{
//    quint16 m_uInt16Parameter;

//    UInt16Parameter()
//    {
//        m_uInt16Parameter = 0;
//    }

//    UInt16Parameter(const quint16 &uInt16Parameter)
//    {
//        m_uInt16Parameter = uInt16Parameter;
//    }

//    void setUInt16Parameter(const quint16 &uInt16Parameter)
//    {
//        m_uInt16Parameter = uInt16Parameter;
//    }

//    quint16 getUInt16Parameter()
//    {
//        return m_uInt16Parameter;
//    }
//};

struct RotatingSpeed
{

protected:

    quint16 m_rotatingSpeed;

public:

    RotatingSpeed()
    {
        m_rotatingSpeed = 0;
    }

    RotatingSpeed(const quint16 &speed)
    {
        m_rotatingSpeed = speed;
    }

    void setRotatingSpeed(const quint16 &speed)
    {
        m_rotatingSpeed = speed;
    }

    quint16 getRotatingSpeed()
    {
        return m_rotatingSpeed;
    }
};

struct RotatingParameters : public RotatingSpeed
{

public:

    RotatingParameters()
    {
        setRotatingSpeed(0);
    }

    RotatingParameters(const quint16 &rotatingSpeed)
    {
        setRotatingSpeed(rotatingSpeed);
    }
};

struct AngleOne
{

protected:

    quint16 m_AngleOne;

public:

    AngleOne()
    {
        m_AngleOne = 0;
    }

    AngleOne(const quint16 &angle)
    {
        m_AngleOne = angle;
    }

    void setAngleOne(const quint16 &angle)
    {
        m_AngleOne = angle;
    }

    quint16 getAngleOne()
    {
        return m_AngleOne;
    }
};

struct AngleTwo
{

protected:

    quint16 m_AngleTwo;

public:

    AngleTwo()
    {
        m_AngleTwo = 0;
    }

    AngleTwo(const quint16 &angle)
    {
        m_AngleTwo = angle;
    }

    void setAngleTwo(const quint16 &angle)
    {
        m_AngleTwo = angle;
    }

    quint16 getAngleTwo()
    {
        return m_AngleTwo;
    }
};

struct PointingParameters : public RotatingSpeed, AngleOne, AngleTwo
{

    PointingParameters()
    {
        setRotatingSpeed(0);
        setAngleOne(0);
        setAngleTwo(0);
    }

    PointingParameters(const quint16 &rotatingSpeed, const quint16 &angleOne, const quint16 &angleTwo)
    {
        setRotatingSpeed(rotatingSpeed);
        setAngleOne(angleOne);
        setAngleTwo(angleTwo);
    }
};

enum struct SweepingMode
{
    Short       =   0,
    Long        =   1
};

struct SweepingParameters : public RotatingSpeed, AngleOne, AngleTwo
{

protected:

    SweepingMode m_sweepingMode;

public:

    SweepingParameters()
    {
        setRotatingSpeed(0);
        setAngleOne(0);
        setAngleTwo(0);
    }

    SweepingParameters(const quint16 &rotatingSpeed, const quint16 &angleOne, const quint16 &angleTwo)
    {
        setRotatingSpeed(rotatingSpeed);
        setAngleOne(angleOne);
        setAngleTwo(angleTwo);
    }

    void setSweepingMode(const SweepingMode &sweepingMode)
    {
        m_sweepingMode = sweepingMode;
    }

    SweepingMode getSweepingMode()
    {
        return m_sweepingMode;
    }
};

struct ModeParameters
{

protected:

    RadarMode m_radarMode;

    RotatingParameters m_rotatingParameters;
    PointingParameters m_pointingParameters;
    SweepingParameters m_sweepingParameters;

public:

    ModeParameters()
    {
        m_radarMode = RadarMode::Rotating;
        m_rotatingParameters = RotatingParameters();
        m_pointingParameters = PointingParameters();
        m_sweepingParameters = SweepingParameters();
    }

    ModeParameters(const RotatingParameters &rotatingParameters)
    {
        m_radarMode = RadarMode::Rotating;
        m_rotatingParameters = rotatingParameters;
    }

    ModeParameters(const PointingParameters &pointingParameters)
    {
        m_radarMode = RadarMode::Pointing;
        m_pointingParameters = pointingParameters;
    }

    ModeParameters(const SweepingParameters &sweepingParameters)
    {
        m_radarMode = RadarMode::Sweeping;
        m_sweepingParameters = sweepingParameters;
    }

    void setRadarMode(const RadarMode &radarMode)
    {
        m_radarMode = radarMode;
    }

    void setRotatingParameters(const RotatingParameters &rotatingParameters)
    {
        m_rotatingParameters = rotatingParameters;
    }

    void setPointingParameters(const PointingParameters &pointingParameters)
    {
        m_pointingParameters = pointingParameters;
    }

    void setSweepingParameters(const SweepingParameters &sweepingParameters)
    {
        m_sweepingParameters = sweepingParameters;
    }

    RadarMode getRadarMode()
    {
         return m_radarMode;
    }

    RotatingParameters getRotatingParameters()
    {
        return m_rotatingParameters;
    }

    PointingParameters getPointingParameters()
    {
        return m_pointingParameters;
    }

    SweepingParameters getSweepingParameters()
    {
        return m_sweepingParameters;
    }
};

enum struct TriggerFrequency
{
    T1kHz           =   0,
    T2kHz           =   1,
    T3kHz           =   2,
    T4kHz           =   3
};

enum struct UnAmpSignalLength
{
    Length32            =   0,
    Length64            =   1,
    Length128           =   2,
    Length256           =   3
};

enum struct AmpSignalOption
{
    Datagram1           =   0,
    Datagram2           =   1,
    Datagram3           =   2,
    Datagram4           =   3,
    Datagram5           =   4,
    Datagram6           =   5,
    Datagram7           =   6,
    Datagram8           =   7
};

enum struct SamplingFrequency
{
    Fs1250kHz           =   0,
    Fs2500kHz           =   1,
    Fs5MHz              =   2,
    Fs10MHz             =   3,
    Fs20MHz             =   4,
    Fs40MHz             =   5,
    Fs80MHz             =   6
};

struct Range
{

protected:

    UnAmpSignalLength m_unAmpSignalLength;

    AmpSignalOption m_ampSignalBegin;
    AmpSignalOption m_ampSignalEnd;

    SamplingFrequency m_samplingFrequency;

public:

    Range()
    {
        m_unAmpSignalLength = UnAmpSignalLength::Length32;
        m_ampSignalBegin = AmpSignalOption::Datagram1;
        m_ampSignalEnd = AmpSignalOption::Datagram8;
        m_samplingFrequency = SamplingFrequency::Fs80MHz;
    }

    Range(const UnAmpSignalLength &unAmpSignalLength, const AmpSignalOption &ampSignalEnd = AmpSignalOption::Datagram8, const AmpSignalOption &ampSignalBegin = AmpSignalOption::Datagram1)
    {
        m_unAmpSignalLength = unAmpSignalLength;
        m_ampSignalBegin = ampSignalBegin;
        m_ampSignalEnd = ampSignalEnd;
        m_samplingFrequency = SamplingFrequency::Fs80MHz;
    }

    Range(const UnAmpSignalLength &unAmpSignalLength, const AmpSignalOption &ampSignalBegin, const AmpSignalOption &ampSignalEnd, const SamplingFrequency &samplingFrequency)
    {
        m_unAmpSignalLength = unAmpSignalLength;
        m_ampSignalBegin = ampSignalBegin;
        m_ampSignalEnd = ampSignalEnd;
        m_samplingFrequency = samplingFrequency;
    }

    void setUnAmpSignalLength (const UnAmpSignalLength &unAmpSignalLength)
    {
        m_unAmpSignalLength = unAmpSignalLength;
    }

    void setAmpSignalBegin (const AmpSignalOption &ampSignalBegin)
    {
        if (ampSignalBegin < m_ampSignalEnd) {
            m_ampSignalBegin = ampSignalBegin;
        } else {
            m_ampSignalBegin = m_ampSignalEnd;
        }
    }

    void setAmpSignalEnd (const AmpSignalOption &ampSignalEnd)
    {
        if (ampSignalEnd > m_ampSignalBegin) {
            m_ampSignalEnd = ampSignalEnd;
        } else {
            m_ampSignalEnd = m_ampSignalBegin;
        }
    }

    void setAmpSignalParameters (const AmpSignalOption &option1, const AmpSignalOption &option2)
    {
        if (option1 <= option2) {
            setAmpSignalBegin(option1);
            setAmpSignalEnd(option2);
        } else {
            setAmpSignalBegin(option2);
            setAmpSignalEnd(option1);
        }
    }

    void setSamplingFrequency (const SamplingFrequency &samplingFrequency)
    {
        m_samplingFrequency = samplingFrequency;
    }

    UnAmpSignalLength getUnAmpSignalLength()
    {
         return m_unAmpSignalLength;
    }

    AmpSignalOption getAmpSignalBegin()
    {
         return m_ampSignalBegin;
    }

    AmpSignalOption getAmpSignalEnd()
    {
         return m_ampSignalEnd;
    }

    SamplingFrequency getSamplingFrequency()
    {
         return m_samplingFrequency;
    }

    double getAmpSignalBeginValue()
    {
         return (getAmpSignalBeginLength()/static_cast<double>(static_cast<quint32>(m_samplingFrequency)))*(299792458.0/2.0);
    }

    double getAmpSignalEndValue()
    {
         return (getAmpSignalEndLength()/static_cast<double>(static_cast<quint32>(m_samplingFrequency)))*(299792458.0/2.0);
    }

    quint32 getAmpSignalBeginLength()
    {
         return 512 - static_cast<quint32>(m_unAmpSignalLength)*static_cast<quint32>(m_ampSignalBegin) > 0 ? 1 : 0 + 512*static_cast<quint32>(m_ampSignalBegin) - 1;
    }

    quint32 getAmpSignalEndLength()
    {
         return 512 - static_cast<quint32>(m_unAmpSignalLength) + 512*static_cast<quint32>(m_ampSignalEnd);
    }

    quint32 getAmpSignalLength()
    {
         return getAmpSignalEndLength() - getAmpSignalBeginLength();
    }

    double getAmpSignalLengthValue()
    {
         return (getAmpSignalLength()/static_cast<double>(static_cast<quint32>(m_samplingFrequency)))*(299792458.0/2.0);
    }

    double getUnAmpSignalLengthValue()
    {
         return (static_cast<double>(static_cast<quint32>(getUnAmpSignalLength())/static_cast<double>(static_cast<quint32>(m_samplingFrequency))))*(299792458.0/2.0);
    }

};

enum struct FileSize
{
    Size128MB           =   0,
    Size256MB           =   1,
    Size512MB           =   2,
    Size1024MB          =   3,
    Size2048MB          =   4,
    Size4096MB          =   5,
    Size8192MB          =   6,
    Size16384MB         =   7,
    Size32768MB         =   8,
};

struct RadarParameters : public Range
{

protected:

    TriggerFrequency m_triggerFrequency;
    FileSize m_fileSize;

public:

    RadarParameters()
    {
        m_triggerFrequency = TriggerFrequency::T1kHz;
        m_unAmpSignalLength = UnAmpSignalLength::Length32;
        m_ampSignalBegin = AmpSignalOption::Datagram1;
        m_ampSignalEnd = AmpSignalOption::Datagram8;
        m_samplingFrequency = SamplingFrequency::Fs80MHz;
        m_fileSize = FileSize::Size4096MB;
    }

    RadarParameters(const TriggerFrequency &triggerFrequency, const UnAmpSignalLength &unAmpSignalLength = UnAmpSignalLength::Length32, const AmpSignalOption &ampSignalBegin = AmpSignalOption::Datagram1,
                    const AmpSignalOption &ampSignalEnd = AmpSignalOption::Datagram8, const SamplingFrequency &samplingFrequency = SamplingFrequency::Fs80MHz, const FileSize &fileSize = FileSize::Size4096MB)
    {
        m_triggerFrequency = triggerFrequency;
        m_unAmpSignalLength = unAmpSignalLength;
        setAmpSignalParameters(ampSignalBegin, ampSignalEnd);
        m_samplingFrequency = samplingFrequency;
        m_fileSize = fileSize;
    }

    void setFileSize(const FileSize &fileSize)
    {
        m_fileSize = fileSize;
    }

    void setTriggerFrequency(const TriggerFrequency &frequency)
    {
        m_triggerFrequency = frequency;
    }

    FileSize getFileSize()
    {
        return m_fileSize;
    }

    quint32 getFileSizeValue(){
        return static_cast<quint32>(m_fileSize);
    }

    TriggerFrequency getTriggerFrequency()
    {
        return m_triggerFrequency;
    }
};

#endif // SCHEDULESTRUCTS_H
