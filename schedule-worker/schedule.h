/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <QObject>
#include <QFile>

#include "scheduleline.h"

class Schedule : public QObject
{
    Q_OBJECT

    QList<ScheduleLine> *m_schedule;

    QString m_fileName;

//    QMap<QTime, int> *m_beginTimeSchedule;
//    QMap<QTime, int> *m_finishTimeSchedule;

    QString m_readingErrors;

public:
    double m_scheduleAvgDataSavingSpeed = 0.0;

    QMap<QPair<QTime, QTime>, int> *m_fullTimeSchedule;

    Schedule(QObject *parent = nullptr);

    Schedule(const Schedule &schedule, QObject *parent = nullptr);

    Schedule operator=(const Schedule &schedule);

    void setSchedule(const Schedule &schedule);

    Schedule readScheduleFile(QString fileName);

    int size();
    void clear();
    ScheduleLine getScheduleLine(const int &number);
    QStringList getDetailedSchedule();

    double getCurrentScheduleLineAvgDataSavingSpeed(ScheduleLine &scheduleLine);

    void addSingleTask(const ScheduleLine &singleTask);
    void removeSingleTask(const int &taskNumber);
    void editSingleTask(const ScheduleLine &newTask, const int &editedTaskNumber);

    QString getReadingErrors();

    void clearSchedule();

    QString getScheduleSavingString();

public slots:

    void slotSetFileName(const QString &fileName);
    void slotLoadSchedule();
    void slotLoadSchedule(const QString &fileName);

private:

    bool addScheduleTimeTask(ScheduleLine &scheduleLine, const int &taskNumber);
    bool addScheduleTimeTask(const QTime &beginTime, const QTime &finishTime, const int &number = 0, const int &repeatTime = 24);
    void sortSchedule();
    void refreshSchedule();

signals:

    void signalStatus(const QString &error);

};

#endif // SCHEDULE_H
