/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SCHEDULEWORKER_H
#define SCHEDULEWORKER_H

#include <QThread>
#include <QObject>
#include <QMutex>
#include <QTime>
#include "shared-libs/inireader.h"

#include "schedule.h"

class ScheduleWorker : public QThread
{
    Q_OBJECT

    QMutex *m_mutex;
    bool m_workingThreadEnable;

    Schedule *m_workingSchedule;

    IniReader *m_radarSettings;

    bool m_radarIsWarmingUp = false;

public:

    explicit ScheduleWorker(QObject *parent = nullptr);

    void run() override;

    bool isWorking();

private:

    QPair<QTime, QTime> findNextTask();

public slots:

    void startWorkingThread(const Schedule &workingSchedule);
    void stopWorkingThread();
    void slotRadarIsWarmingUp(const bool &radarIsWarmingUp);

signals:

    void poolStatus(const bool &threadStart);

    void signalSetupTaskSettings(const Schedule &task);

    void signalAddLogLine(const QString &stringLine);
    void signalSetWorkingStatus(const QString &status);
    void signalSetJobProgress(const int &progress);

    void signalSetAntennaRotating(const bool &isRotating);
    void signalSetTransmitionOn(const bool &isTransmitting);
    void signalSetDataAcquisitionOn(const bool &isSaving);

    void signalSetCurrentScheduleSettings(const int &scheduleLineNumber);

    void SignalLogEvent(const QString &logEvent);
};

#endif // SCHEDULEWORKER_H
