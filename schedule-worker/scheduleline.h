﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SCHEDULELINE_H
#define SCHEDULELINE_H

#include <QTime>
#include <QObject>
#include <QString>
#include <QMap>

#include "schedulestructs.h"

class ScheduleLine : public QObject, ModeParameters, RadarParameters
{
    Q_OBJECT

    quint8 m_repetitionTime;

    QTime m_timeBegin;
    QTime m_timeFinish;

    QString m_taskName;

    QString m_basicFileName;

public:

    ScheduleLine(QObject *parent = nullptr);
    ScheduleLine(QString taskName, quint8 repetitionTime = 24, QTime timeBegin = QTime(), QTime timeFinish = QTime(),
                 RadarMode radarMode = RadarMode::Sweeping, RotatingSpeed rotatingSpeed = 0,
                 AngleOne angleOne = 0, AngleTwo angleTwo = 0, SweepingMode sweepingMode = SweepingMode::Short,
                 TriggerFrequency triggerFrequency = TriggerFrequency::T2kHz, UnAmpSignalLength unAmpSignalLength = UnAmpSignalLength::Length32,
                 AmpSignalOption ampSignalBegin = AmpSignalOption::Datagram1, AmpSignalOption ampSignalEnd = AmpSignalOption::Datagram8,
                 SamplingFrequency samplingFrequency = SamplingFrequency::Fs80MHz, FileSize fileSize = FileSize::Size4096MB, QString basicFileName = QString(""),
                 QObject *parent = nullptr);
    ScheduleLine(const ScheduleLine &scheduleLine, QObject *parent = nullptr);

    ScheduleLine operator=(const ScheduleLine &scheduleLine);

    ScheduleLine editScheduleLine(const ScheduleLine &scheduleLine);

public slots:

    void slotSetLine(QString taskName, quint8 repetitionTime = 24, QTime timeBegin = QTime(), QTime timeFinish = QTime(),
                     ModeParameters modeParameters = ModeParameters(), RadarParameters radarParameters = RadarParameters(), QString basicFileName = QString(""));
    void slotSetLine(QString taskName, quint8 repetitionTime, QTime timeBegin, QTime timeFinish,
                     RadarMode radarMode, RotatingSpeed rotatingSpeed,
                     AngleOne angleOne, AngleTwo angleTwo, SweepingMode sweepingMode,
                     TriggerFrequency triggerFrequency = TriggerFrequency::T2kHz, UnAmpSignalLength unAmpSignalLength = UnAmpSignalLength::Length32,
                                                     AmpSignalOption ampSignalBegin = AmpSignalOption::Datagram1, AmpSignalOption ampSignalEnd = AmpSignalOption::Datagram8,
                                                     SamplingFrequency samplingFrequency = SamplingFrequency::Fs80MHz, FileSize fileSize = FileSize::Size4096MB, QString basicFileName = QString(""));

    void slotSetRepetitionTime(quint8 repetitionTime = 0);
    void slotSetTime(QTime timeBegin, QTime timeFinish);
    void slotSetTimeBegin(QTime timeBegin);
    void slotSetTimeFinish(QTime timeFinish);


    void slotSetModeParameters(ModeParameters modeParameters);
    void slotSetRadarMode(RadarMode radarMode = RadarMode::Rotating);
    void slotSetModeParameters(RadarMode radarMode = RadarMode::Rotating, RotatingSpeed rotatingSpeed = 0);
    void slotSetModeParameters(RadarMode radarMode, RotatingSpeed rotatingSpeed, AngleOne angleOne, AngleTwo angleTwo);
    void slotSetModeParameters(RadarMode radarMode, RotatingSpeed rotatingSpeed, AngleOne angleOne, AngleTwo angleTwo, SweepingMode sweepingMode);

    void slotSetModeRotatingParameters(RotatingParameters rotatingParameters);
    void slotSetModeRotatingParameters(RotatingSpeed rotatingSpeed = 0);
    void slotSetModeRotatingSpeed(RotatingSpeed rotatingSpeed = 0);

    void slotSetModePointingParameters(PointingParameters pointingParameters);
    void slotSetModePointingParameters(RotatingSpeed rotatingSpeed = 0, AngleOne angleOne = 0, AngleTwo angleTwo = 0);
    void slotSetModeAngle(AngleOne angleOne = 0, AngleTwo angleTwo = 0);
    void slotSetModeAngleOne(AngleOne angleOne = 0);
    void slotSetModeAngleTwo(AngleTwo angleTwo = 0);

    void slotSetSweepingParameters(SweepingParameters sweepingParameters);
    void slotSetSweepingParameters(RotatingSpeed rotatingSpeed = 0, AngleOne angleOne = 0, AngleTwo angleTwo = 0, SweepingMode sweepingMode = SweepingMode::Short);
    void slotSetModeSweepingMode(SweepingMode sweepingMode = SweepingMode::Short);

    void slotSetRadarParameters(RadarParameters radarParameters);
    void slotSetRadarParameters(TriggerFrequency triggerFrequency, Range range, FileSize fileSize);
    void slotSetRadarParameters(TriggerFrequency triggerFrequency = TriggerFrequency::T2kHz, UnAmpSignalLength unAmpSignalLength = UnAmpSignalLength::Length32,
                                AmpSignalOption ampSignalBegin = AmpSignalOption::Datagram1, AmpSignalOption ampSignalEnd = AmpSignalOption::Datagram8,
                                SamplingFrequency samplingFrequency = SamplingFrequency::Fs80MHz, FileSize fileSize = FileSize::Size4096MB);

    void slotSetTriggerFrequency(TriggerFrequency triggerFrequency = TriggerFrequency::T2kHz);

    void slotSetRange(Range range);
    void slotSetRange(UnAmpSignalLength unAmpSignalLength = UnAmpSignalLength::Length32, AmpSignalOption ampSignalBegin = AmpSignalOption::Datagram1,
                      AmpSignalOption ampSignalEnd = AmpSignalOption::Datagram8, SamplingFrequency samplingFrequency = SamplingFrequency::Fs80MHz);
    void slotSetUnAmpSignalLength(UnAmpSignalLength unAmpSignalLength = UnAmpSignalLength::Length32);
    void slotSetAmpSignalLength(AmpSignalOption ampSignalBegin = AmpSignalOption::Datagram1, AmpSignalOption ampSignalEnd = AmpSignalOption::Datagram8);
    void slotSetAmpSignalBegin(AmpSignalOption ampSignalBegin = AmpSignalOption::Datagram1);
    void slotSetAmpSignalEnd(AmpSignalOption ampSignalEnd = AmpSignalOption::Datagram8);
    void slotSetSamplingFrequency(SamplingFrequency samplingFrequency = SamplingFrequency::Fs80MHz);
    void slotSetFileSize(FileSize fileSize = FileSize::Size4096MB);

    void slotSetBasicFileName(QString fileName = QString(""));
    void slotSetTaskName(QString taskName = QString(""));

    void slotReadScheduleTask(const QStringList &scheduleTask);

    quint8 slotGetRepetitionTime();
    QPair<QTime, QTime> slotGetTime();
    QTime slotGetTimeBegin();
    QTime slotGetTimeFinish();


    ModeParameters slotGetModeParameters();
    RadarMode slotGetRadarMode();

    RotatingParameters slotGetModeRotatingParameters();
    quint16 slotGetModeRotatingSpeed();

    PointingParameters slotGetModePointingParameters();
    QPair<quint16, quint16> slotGetModeAngle();
    quint16 slotGetModeAngleOne();
    quint16 slotGetModeAngleTwo();

    SweepingParameters slotGetSweepingParameters();
    SweepingMode slotGetModeSweepingMode();

    RadarParameters slotGetRadarParameters();

    TriggerFrequency slotGetTriggerFrequency();

    Range slotGetRange();
    UnAmpSignalLength slotGetUnAmpSignalLength();
    QPair<AmpSignalOption, AmpSignalOption> slotGetAmpSignalLength();
    AmpSignalOption slotGetAmpSignalBegin();
    AmpSignalOption slotGetAmpSignalEnd();
    SamplingFrequency slotGetSamplingFrequency();
    FileSize slotGetFileSize();

    QString slotGetBasicFileName();
    QString slotGetTaskName();

    QStringList generateScheduleLine();
    QStringList generateTaskLine();

private:

    QMap<QString, QString> defaultConfiguration();
    QMap<QString, QPair<long, long>> referenceValues();

    QString readScheduleData(QMap<QString, QString> &data);
};

#endif // SCHEDULELINE_H
