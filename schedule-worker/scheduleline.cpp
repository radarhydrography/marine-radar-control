/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "scheduleline.h"

ScheduleLine::ScheduleLine(QObject *parent) : QObject(parent)
{
    m_taskName = QString("");
    m_repetitionTime = 24;
    m_timeBegin = QTime();
    m_timeFinish = QTime();
    m_radarMode = RadarMode::Rotating;
    m_rotatingParameters = RotatingParameters();
    m_pointingParameters = PointingParameters();
    m_sweepingParameters = SweepingParameters();
    m_triggerFrequency = TriggerFrequency::T1kHz;
    m_unAmpSignalLength = UnAmpSignalLength::Length32;
    m_ampSignalBegin = AmpSignalOption::Datagram1;
    m_ampSignalEnd = AmpSignalOption::Datagram8;
    m_samplingFrequency = SamplingFrequency::Fs80MHz;
    m_fileSize = FileSize::Size4096MB;
    m_basicFileName = QString("");
}

ScheduleLine::ScheduleLine(QString taskName, quint8 repetitionTime, QTime timeBegin, QTime timeFinish, RadarMode radarMode, RotatingSpeed rotatingSpeed,
                           AngleOne angleOne, AngleTwo angleTwo, SweepingMode sweepingMode, TriggerFrequency triggerFrequency,
                           UnAmpSignalLength unAmpSignalLength, AmpSignalOption ampSignalBegin, AmpSignalOption ampSignalEnd,
                           SamplingFrequency samplingFrequency, FileSize fileSize, QString basicFileName, QObject *parent) : QObject(parent)
{
    m_taskName = taskName;
    m_repetitionTime = repetitionTime;
    m_timeBegin = timeBegin;
    m_timeFinish = timeFinish;
    m_radarMode = radarMode;
    m_rotatingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setAngleOne(angleOne.getAngleOne());
    m_pointingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_sweepingParameters.setAngleOne(angleOne.getAngleOne());
    m_sweepingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setSweepingMode(sweepingMode);
    m_triggerFrequency = triggerFrequency;
    m_unAmpSignalLength = unAmpSignalLength;
    setAmpSignalParameters(ampSignalBegin, ampSignalEnd);
    m_samplingFrequency = samplingFrequency;
    m_fileSize = fileSize;
    m_basicFileName = basicFileName;
}

ScheduleLine::ScheduleLine(const ScheduleLine &scheduleLine, QObject *parent) : QObject(parent)
{

    m_taskName = scheduleLine.m_taskName;
    m_repetitionTime = scheduleLine.m_repetitionTime;
    m_timeBegin = scheduleLine.m_timeBegin;
    m_timeFinish = scheduleLine.m_timeFinish;
    m_radarMode = scheduleLine.m_radarMode;
    m_rotatingParameters = scheduleLine.m_rotatingParameters;
    m_pointingParameters = scheduleLine.m_pointingParameters;
    m_sweepingParameters = scheduleLine.m_sweepingParameters;
    m_triggerFrequency = scheduleLine.m_triggerFrequency;
    m_unAmpSignalLength = scheduleLine.m_unAmpSignalLength;
    setAmpSignalParameters(scheduleLine.m_ampSignalBegin, scheduleLine.m_ampSignalEnd);
    m_samplingFrequency = scheduleLine.m_samplingFrequency;
    m_fileSize = scheduleLine.m_fileSize;
    m_basicFileName = scheduleLine.m_basicFileName;
}

ScheduleLine ScheduleLine::operator =(const ScheduleLine &scheduleLine)
{
    m_taskName = scheduleLine.m_taskName;
    m_repetitionTime = scheduleLine.m_repetitionTime;
    m_timeBegin = scheduleLine.m_timeBegin;
    m_timeFinish = scheduleLine.m_timeFinish;
    m_radarMode = scheduleLine.m_radarMode;
    m_rotatingParameters = scheduleLine.m_rotatingParameters;
    m_pointingParameters = scheduleLine.m_pointingParameters;
    m_sweepingParameters = scheduleLine.m_sweepingParameters;
    m_triggerFrequency = scheduleLine.m_triggerFrequency;
    m_unAmpSignalLength = scheduleLine.m_unAmpSignalLength;
    setAmpSignalParameters(scheduleLine.m_ampSignalBegin, scheduleLine.m_ampSignalEnd);
    m_samplingFrequency = scheduleLine.m_samplingFrequency;
    m_fileSize = scheduleLine.m_fileSize;
    m_basicFileName = scheduleLine.m_basicFileName;

    return *this;
}

ScheduleLine ScheduleLine::editScheduleLine(const ScheduleLine &scheduleLine)
{
    ScheduleLine out = scheduleLine;
    return scheduleLine;
}

void ScheduleLine::slotSetLine(QString taskName, quint8 repetitionTime, QTime timeBegin, QTime timeFinish, ModeParameters modeParameters, RadarParameters radarParameters, QString basicFileName)
{
    m_taskName = taskName;
    m_repetitionTime = repetitionTime;
    m_timeBegin = timeBegin;
    m_timeFinish = timeFinish;
    m_radarMode = modeParameters.getRadarMode();
    m_rotatingParameters = modeParameters.getRotatingParameters();
    m_pointingParameters = modeParameters.getPointingParameters();
    m_sweepingParameters = modeParameters.getSweepingParameters();
    m_triggerFrequency = radarParameters.getTriggerFrequency();
    m_unAmpSignalLength = radarParameters.getUnAmpSignalLength();
    setAmpSignalParameters(radarParameters.getAmpSignalBegin(), radarParameters.getAmpSignalEnd());
    m_samplingFrequency = radarParameters.getSamplingFrequency();
    m_fileSize = radarParameters.getFileSize();
    m_basicFileName = basicFileName;
}

void ScheduleLine::slotSetLine(QString taskName, quint8 repetitionTime, QTime timeBegin, QTime timeFinish, RadarMode radarMode,
                               RotatingSpeed rotatingSpeed, AngleOne angleOne, AngleTwo angleTwo, SweepingMode sweepingMode,
                               TriggerFrequency triggerFrequency, UnAmpSignalLength unAmpSignalLength, AmpSignalOption ampSignalBegin,
                               AmpSignalOption ampSignalEnd, SamplingFrequency samplingFrequency, FileSize fileSize, QString basicFileName)
{
    m_taskName = taskName;
    m_repetitionTime = repetitionTime;
    m_timeBegin = timeBegin;
    m_timeFinish = timeFinish;
    m_radarMode = radarMode;
    m_rotatingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setAngleOne(angleOne.getAngleOne());
    m_pointingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_sweepingParameters.setAngleOne(angleOne.getAngleOne());
    m_sweepingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setSweepingMode(sweepingMode);
    m_triggerFrequency = triggerFrequency;
    m_unAmpSignalLength = unAmpSignalLength;
    setAmpSignalParameters(ampSignalBegin, ampSignalEnd);
    m_samplingFrequency = samplingFrequency;
    m_fileSize = fileSize;
    m_basicFileName = basicFileName;
}

void ScheduleLine::slotSetRepetitionTime(quint8 repetitionTime)
{
    m_repetitionTime = repetitionTime;
}

void ScheduleLine::slotSetTime(QTime timeBegin, QTime timeFinish)
{
    m_timeBegin = timeBegin;
    m_timeFinish = timeFinish;
}

void ScheduleLine::slotSetTimeBegin(QTime timeBegin)
{
    m_timeBegin = timeBegin;
}

void ScheduleLine::slotSetTimeFinish(QTime timeFinish)
{
    m_timeFinish = timeFinish;
}

void ScheduleLine::slotSetModeParameters(ModeParameters modeParameters)
{
    m_radarMode = modeParameters.getRadarMode();
    m_rotatingParameters = modeParameters.getRotatingParameters();
    m_pointingParameters = modeParameters.getPointingParameters();
    m_sweepingParameters = modeParameters.getSweepingParameters();
}

void ScheduleLine::slotSetRadarMode(RadarMode radarMode)
{
    m_radarMode = radarMode;
}

void ScheduleLine::slotSetModeParameters(RadarMode radarMode, RotatingSpeed rotatingSpeed)
{
    m_radarMode = radarMode;
    m_rotatingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_sweepingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
}

void ScheduleLine::slotSetModeParameters(RadarMode radarMode, RotatingSpeed rotatingSpeed, AngleOne angleOne, AngleTwo angleTwo)
{
    m_radarMode = radarMode;
    m_rotatingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setAngleOne(angleOne.getAngleOne());
    m_pointingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_sweepingParameters.setAngleOne(angleOne.getAngleOne());
    m_sweepingParameters.setAngleTwo(angleTwo.getAngleTwo());
}

void ScheduleLine::slotSetModeParameters(RadarMode radarMode, RotatingSpeed rotatingSpeed, AngleOne angleOne, AngleTwo angleTwo, SweepingMode sweepingMode)
{
    m_radarMode = radarMode;
    m_rotatingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setAngleOne(angleOne.getAngleOne());
    m_pointingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_sweepingParameters.setAngleOne(angleOne.getAngleOne());
    m_sweepingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setSweepingMode(sweepingMode);
}

void ScheduleLine::slotSetModeRotatingParameters(RotatingParameters rotatingParameters)
{
    m_radarMode = RadarMode::Rotating;
    m_rotatingParameters = rotatingParameters;
}

void ScheduleLine::slotSetModeRotatingParameters(RotatingSpeed rotatingSpeed)
{
    m_radarMode = RadarMode::Rotating;
    m_rotatingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
}

void ScheduleLine::slotSetModeRotatingSpeed(RotatingSpeed rotatingSpeed)
{
    m_rotatingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_sweepingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
}

void ScheduleLine::slotSetModePointingParameters(PointingParameters pointingParameters)
{
    m_radarMode = RadarMode::Pointing;
    m_pointingParameters = pointingParameters;
}

void ScheduleLine::slotSetModePointingParameters(RotatingSpeed rotatingSpeed, AngleOne angleOne, AngleTwo angleTwo)
{
    m_radarMode = RadarMode::Pointing;
    m_pointingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_pointingParameters.setAngleOne(angleOne.getAngleOne());
    m_pointingParameters.setAngleTwo(angleTwo.getAngleTwo());
}

void ScheduleLine::slotSetModeAngle(AngleOne angleOne, AngleTwo angleTwo)
{
    m_pointingParameters.setAngleOne(angleOne.getAngleOne());
    m_pointingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setAngleOne(angleOne.getAngleOne());
    m_sweepingParameters.setAngleTwo(angleTwo.getAngleTwo());
}

void ScheduleLine::slotSetModeAngleOne(AngleOne angleOne)
{
    m_pointingParameters.setAngleOne(angleOne.getAngleOne());
    m_sweepingParameters.setAngleOne(angleOne.getAngleOne());
}

void ScheduleLine::slotSetModeAngleTwo(AngleTwo angleTwo)
{
    m_pointingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setAngleTwo(angleTwo.getAngleTwo());
}

void ScheduleLine::slotSetSweepingParameters(SweepingParameters sweepingParameters)
{
    m_radarMode = RadarMode::Sweeping;
    m_sweepingParameters = sweepingParameters;
}

void ScheduleLine::slotSetSweepingParameters(RotatingSpeed rotatingSpeed, AngleOne angleOne, AngleTwo angleTwo, SweepingMode sweepingMode)
{
    m_radarMode = RadarMode::Sweeping;
    m_sweepingParameters.setRotatingSpeed(rotatingSpeed.getRotatingSpeed());
    m_sweepingParameters.setAngleOne(angleOne.getAngleOne());
    m_sweepingParameters.setAngleTwo(angleTwo.getAngleTwo());
    m_sweepingParameters.setSweepingMode(sweepingMode);
}

void ScheduleLine::slotSetModeSweepingMode(SweepingMode sweepingMode)
{
    m_sweepingParameters.setSweepingMode(sweepingMode);
}

void ScheduleLine::slotSetRadarParameters(RadarParameters radarParameters)
{
    m_triggerFrequency = radarParameters.getTriggerFrequency();
    m_unAmpSignalLength = radarParameters.getUnAmpSignalLength();
    setAmpSignalParameters(radarParameters.getAmpSignalBegin(), radarParameters.getAmpSignalEnd());
    m_samplingFrequency = radarParameters.getSamplingFrequency();
    m_fileSize = radarParameters.getFileSize();
}

void ScheduleLine::slotSetRadarParameters(TriggerFrequency triggerFrequency, Range range, FileSize fileSize)
{
    m_triggerFrequency = triggerFrequency;
    m_unAmpSignalLength = range.getUnAmpSignalLength();
    setAmpSignalParameters(range.getAmpSignalBegin(), range.getAmpSignalEnd());
    m_samplingFrequency = range.getSamplingFrequency();
    m_fileSize = fileSize;
}

void ScheduleLine::slotSetRadarParameters(TriggerFrequency triggerFrequency, UnAmpSignalLength unAmpSignalLength, AmpSignalOption ampSignalBegin, AmpSignalOption ampSignalEnd, SamplingFrequency samplingFrequency, FileSize fileSize)
{
    m_triggerFrequency = triggerFrequency;
    m_unAmpSignalLength = unAmpSignalLength;
    setAmpSignalParameters(ampSignalBegin, ampSignalEnd);
    m_samplingFrequency = samplingFrequency;
    m_fileSize = fileSize;
}

void ScheduleLine::slotSetTriggerFrequency(TriggerFrequency triggerFrequency)
{
    m_triggerFrequency = triggerFrequency;
}

void ScheduleLine::slotSetRange(Range range)
{
    m_unAmpSignalLength = range.getUnAmpSignalLength();
    setAmpSignalParameters(range.getAmpSignalBegin(), range.getAmpSignalEnd());
    m_samplingFrequency = range.getSamplingFrequency();
}

void ScheduleLine::slotSetRange(UnAmpSignalLength unAmpSignalLength, AmpSignalOption ampSignalBegin, AmpSignalOption ampSignalEnd, SamplingFrequency samplingFrequency)
{
    m_unAmpSignalLength = unAmpSignalLength;
    setAmpSignalParameters(ampSignalBegin, ampSignalEnd);
    m_samplingFrequency = samplingFrequency;
}

void ScheduleLine::slotSetUnAmpSignalLength(UnAmpSignalLength unAmpSignalLength)
{
    m_unAmpSignalLength = unAmpSignalLength;
}

void ScheduleLine::slotSetAmpSignalLength(AmpSignalOption ampSignalBegin, AmpSignalOption ampSignalEnd)
{
    setAmpSignalParameters(ampSignalBegin, ampSignalEnd);
}

void ScheduleLine::slotSetAmpSignalBegin(AmpSignalOption ampSignalBegin)
{
    setAmpSignalBegin(ampSignalBegin);
}

void ScheduleLine::slotSetAmpSignalEnd(AmpSignalOption ampSignalEnd)
{
    setAmpSignalEnd(ampSignalEnd);
}

void ScheduleLine::slotSetSamplingFrequency(SamplingFrequency samplingFrequency)
{
    setSamplingFrequency(samplingFrequency);
}

void ScheduleLine::slotSetFileSize(FileSize fileSize)
{
    setFileSize(fileSize);
}

void ScheduleLine::slotSetBasicFileName(QString fileName)
{
    m_basicFileName = fileName;
}

void ScheduleLine::slotSetTaskName(QString taskName)
{
    m_taskName = taskName;
}

void ScheduleLine::slotReadScheduleTask(const QStringList &scheduleTask)
{
    QMap<QString, QString> data = defaultConfiguration();
    for (int index = 0; index < scheduleTask.size(); index++)
    {
        QString line;
        line.append(scheduleTask[index]);
        line.remove(' ');
        line.remove(';');
        if (!(line == "" || line[0] == "#")) {
            QStringList fileStringList = line.mid(0, line.indexOf("#")).split(QString("="));
            if (fileStringList.size() == 2 && fileStringList[1] != "" && data.contains(fileStringList[0])) {
                data[fileStringList[0]] = fileStringList[1];
            }
        }
    }

    readScheduleData(data);
}

quint8 ScheduleLine::slotGetRepetitionTime()
{
    return m_repetitionTime;
}

QPair<QTime, QTime> ScheduleLine::slotGetTime()
{
    return QPair<QTime, QTime>(m_timeBegin, m_timeFinish);
}

QTime ScheduleLine::slotGetTimeBegin()
{
    return m_timeBegin;
}

QTime ScheduleLine::slotGetTimeFinish()
{
    return m_timeFinish;
}

ModeParameters ScheduleLine::slotGetModeParameters()
{
    switch (getRadarMode()) {
    case RadarMode::Rotating : {
        return ModeParameters(getRotatingParameters());
    }
    case RadarMode::Pointing : {
        return ModeParameters(getPointingParameters());
    }
    case RadarMode::Sweeping : {
        return ModeParameters(getSweepingParameters());
    }
    }
}

RadarMode ScheduleLine::slotGetRadarMode()
{
    return getRadarMode();
}

RotatingParameters ScheduleLine::slotGetModeRotatingParameters()
{
    return getRotatingParameters();
}

quint16 ScheduleLine::slotGetModeRotatingSpeed()
{
    return m_rotatingParameters.getRotatingSpeed();
}

PointingParameters ScheduleLine::slotGetModePointingParameters()
{
    return getPointingParameters();
}

QPair<quint16, quint16> ScheduleLine::slotGetModeAngle()
{
    switch (getRadarMode()) {
    case RadarMode::Rotating : {
        return QPair<quint16, quint16>(m_pointingParameters.getAngleOne(), m_pointingParameters.getAngleTwo());
    }
    case RadarMode::Pointing : {
        return QPair<quint16, quint16>(m_pointingParameters.getAngleOne(), m_pointingParameters.getAngleTwo());
    }
    case RadarMode::Sweeping : {
        return QPair<quint16, quint16>(m_sweepingParameters.getAngleOne(), m_sweepingParameters.getAngleTwo());
    }
    }

}

quint16 ScheduleLine::slotGetModeAngleOne()
{
    switch (getRadarMode()) {
    case RadarMode::Rotating : {
        return m_pointingParameters.getAngleOne();
    }
    case RadarMode::Pointing : {
        return m_pointingParameters.getAngleOne();
    }
    case RadarMode::Sweeping : {
        return m_sweepingParameters.getAngleOne();
    }
    }
}

quint16 ScheduleLine::slotGetModeAngleTwo()
{
    switch (getRadarMode()) {
    case RadarMode::Rotating : {
        return m_pointingParameters.getAngleTwo();
    }
    case RadarMode::Pointing : {
        return m_pointingParameters.getAngleTwo();
    }
    case RadarMode::Sweeping : {
        return m_sweepingParameters.getAngleTwo();
    }
    }
}

SweepingParameters ScheduleLine::slotGetSweepingParameters()
{
    return getSweepingParameters();
}

SweepingMode ScheduleLine::slotGetModeSweepingMode()
{
    return m_sweepingParameters.getSweepingMode();
}

RadarParameters ScheduleLine::slotGetRadarParameters()
{
    return RadarParameters(getTriggerFrequency(), getUnAmpSignalLength(), getAmpSignalBegin(), getAmpSignalEnd(), getSamplingFrequency(), getFileSize());
}

TriggerFrequency ScheduleLine::slotGetTriggerFrequency()
{
    return getTriggerFrequency();
}

Range ScheduleLine::slotGetRange()
{
    return Range(getUnAmpSignalLength(), getAmpSignalBegin(), getAmpSignalEnd(), getSamplingFrequency());
}

UnAmpSignalLength ScheduleLine::slotGetUnAmpSignalLength()
{
    return getUnAmpSignalLength();
}

QPair<AmpSignalOption, AmpSignalOption> ScheduleLine::slotGetAmpSignalLength()
{
    return QPair<AmpSignalOption, AmpSignalOption>(getAmpSignalBegin(), getAmpSignalEnd());
}

AmpSignalOption ScheduleLine::slotGetAmpSignalBegin()
{
    return getAmpSignalBegin();
}

AmpSignalOption ScheduleLine::slotGetAmpSignalEnd()
{
    return getAmpSignalEnd();
}

SamplingFrequency ScheduleLine::slotGetSamplingFrequency()
{
    return getSamplingFrequency();
}

FileSize ScheduleLine::slotGetFileSize()
{
    return getFileSize();
}

QString ScheduleLine::slotGetBasicFileName()
{
    return m_basicFileName;
}

QString ScheduleLine::slotGetTaskName()
{
    return m_taskName;
}

QMap<QString, QString> ScheduleLine::defaultConfiguration()
{
    return QMap<QString, QString> ({
                                        {"TaskName", "default"}
                                       ,{"RepetitionTime", "69"}
                                       ,{"TimeBegin", "69:69:69"}
                                       ,{"TimeFinish", "69:69:69"}

                                       ,{"RadarMode", "69"}
                                       ,{"RotatingSpeed", "69"}
                                       ,{"AngleOne", "69"}
                                       ,{"AngleTwo", "69"}
                                       ,{"SweepingMode", "69"}

                                       ,{"TriggerFrequency", "69"}
                                       ,{"UnAmpSignalLength", "69"}
                                       ,{"AmpSignalBegin", "69"}
                                       ,{"AmpSignalEnd", "69"}
                                       ,{"SamplingFrequency", "69MHz"}

                                       ,{"MaxFileSize", "69"}

                                       ,{"BasicFileName", "default"}

                                       ,{"", ""}

                                   });
}

QMap<QString, QPair<long, long>> ScheduleLine::referenceValues()
{
    return QMap<QString, QPair<long, long>> (
                                                {
                                                     {"RepetitionTime", {0, 24}}

                                                    ,{"RadarMode", {0, 2}}
                                                    ,{"RotatingSpeed", {0, 0}}
                                                    ,{"AngleOne", {-360, 360}}
                                                    ,{"AngleTwo", {-360, 360}}
                                                    ,{"SweepingMode", {0, 1}}

                                                    ,{"TriggerFrequency", {1, 4}}
                                                    ,{"UnAmpSignalLength", {32, 256}}
                                                    ,{"AmpSignalBegin", {1, 8}}
                                                    ,{"AmpSignalEnd", {1, 8}}

                                                    ,{"MaxFileSize", {128, 32768}}

                                                    ,{"", {0, 0}}

                                                });
}

QString ScheduleLine::readScheduleData(QMap<QString, QString> &data)
{
    QString readingErrors = QString("Reading Errors:");

    m_timeBegin.currentTime();
    m_timeFinish.currentTime();

    m_taskName = data["TaskName"];
    if (m_taskName == "default") {
        readingErrors += " Task Name,";
    }
    m_repetitionTime = data["RepetitionTime"].toUInt();
    if (m_repetitionTime == 69) {
        readingErrors += " Repetition Time,";
    }
    m_basicFileName = data["BasicFileName"];
    if (m_basicFileName == "default") {
        readingErrors += " File Name,";
    }
    m_timeBegin = QTime::fromString(data["TimeBegin"], QString("hh:mm:ss"));
    if (m_timeBegin.toString(QString("hh:mm:ss")) != data["TimeBegin"]) {
        readingErrors += " TimeBegin,";
    }
    m_timeFinish = QTime::fromString(data["TimeFinish"], QString("hh:mm:ss"));
    if (m_timeFinish.toString(QString("hh:mm:ss")) != data["TimeFinish"]) {
        readingErrors += " TimeFinish,";
    }
    slotSetTime(m_timeBegin, m_timeFinish);
    if (data[""].toLong() == 0) {
        setRadarMode(RadarMode::Rotating);
    } else if (data["RadarMode"].toLong() == 1) {
        setRadarMode(RadarMode::Pointing);
    } else if (data["RadarMode"].toLong() == 2) {
        setRadarMode(RadarMode::Sweeping);
    } else {
        readingErrors += " RadarMode,";
    }

//    if (data["RotatingSpeed"].toLong() == 0) {

//    } else if (data[""].toLong() == 0) {

//    } else {
//        readingErrors += " RadarMode,";
//    }

//    if (data["AngleOne"].toLong() == 0) {

//    } else if (data[""].toLong() == 0) {

//    } else {
//        readingErrors += "AngleOne ,";
//    }

//    if (data["AngleTwo"].toLong() == 0) {

//    } else if (data[""].toLong() == 0) {

//    } else {
//        readingErrors += " AngleTwo,";
//    }

//    if (data["SweepingMode"].toLong() == 0) {

//    } else if (data[""].toLong() == 0) {

//    } else {
//        readingErrors += " SweepingMode,";
//    }

    if (data["TriggerFrequency"].toLong() == 1) {
        setTriggerFrequency(TriggerFrequency::T1kHz);
    } else if (data["TriggerFrequency"].toLong() == 2) {
        setTriggerFrequency(TriggerFrequency::T2kHz);
    } else if (data["TriggerFrequency"].toLong() == 3) {
        setTriggerFrequency(TriggerFrequency::T3kHz);
    } else if (data["TriggerFrequency"].toLong() == 4) {
        setTriggerFrequency(TriggerFrequency::T4kHz);
    } else {
        readingErrors += " TriggerFrequency,";
    }

    if (data["UnAmpSignalLength"].toLong() == 32) {
        setUnAmpSignalLength(UnAmpSignalLength::Length32);
    } else if (data["UnAmpSignalLength"].toLong() == 64) {
        setUnAmpSignalLength(UnAmpSignalLength::Length64);
    } else if (data["UnAmpSignalLength"].toLong() == 128) {
        setUnAmpSignalLength(UnAmpSignalLength::Length128);
    } else if (data["UnAmpSignalLength"].toLong() == 256) {
        setUnAmpSignalLength(UnAmpSignalLength::Length256);
    } else {
        readingErrors += " UnAmpSignalLength,";
    }

    if (data["AmpSignalBegin"].toLong() == 1) {
        setAmpSignalBegin(AmpSignalOption::Datagram1);
    } else if (data["AmpSignalBegin"].toLong() == 2) {
        setAmpSignalBegin(AmpSignalOption::Datagram2);
    } else if (data["AmpSignalBegin"].toLong() == 3) {
        setAmpSignalBegin(AmpSignalOption::Datagram3);
    } else if (data["AmpSignalBegin"].toLong() == 4) {
        setAmpSignalBegin(AmpSignalOption::Datagram4);
    } else if (data["AmpSignalBegin"].toLong() == 5) {
        setAmpSignalBegin(AmpSignalOption::Datagram5);
    } else if (data["AmpSignalBegin"].toLong() == 6) {
        setAmpSignalBegin(AmpSignalOption::Datagram6);
    } else if (data["AmpSignalBegin"].toLong() == 7) {
        setAmpSignalBegin(AmpSignalOption::Datagram7);
    } else if (data["AmpSignalBegin"].toLong() == 8) {
        setAmpSignalBegin(AmpSignalOption::Datagram8);
    } else {
        readingErrors += " AmpSignalBegin,";
    }

    if (data["AmpSignalEnd"].toLong() == 1) {
        setAmpSignalEnd(AmpSignalOption::Datagram1);
    } else if (data["AmpSignalEnd"].toLong() == 2) {
        setAmpSignalEnd(AmpSignalOption::Datagram2);
    } else if (data["AmpSignalEnd"].toLong() == 3) {
        setAmpSignalEnd(AmpSignalOption::Datagram3);
    } else if (data["AmpSignalEnd"].toLong() == 4) {
        setAmpSignalEnd(AmpSignalOption::Datagram4);
    } else if (data["AmpSignalEnd"].toLong() == 5) {
        setAmpSignalEnd(AmpSignalOption::Datagram5);
    } else if (data["AmpSignalEnd"].toLong() == 6) {
        setAmpSignalEnd(AmpSignalOption::Datagram6);
    } else if (data["AmpSignalEnd"].toLong() == 7) {
        setAmpSignalEnd(AmpSignalOption::Datagram7);
    } else if (data["AmpSignalEnd"].toLong() == 8) {
        setAmpSignalEnd(AmpSignalOption::Datagram8);
    } else {
        readingErrors += " AmpSignalEnd,";
    }

    if (data["SamplingFrequency"] == "80MHz") {
        setSamplingFrequency(SamplingFrequency::Fs80MHz);
    } else if (data["SamplingFrequency"] == "40MHz") {
        setSamplingFrequency(SamplingFrequency::Fs40MHz);
    } else if (data["SamplingFrequency"] == "20MHz") {
        setSamplingFrequency(SamplingFrequency::Fs20MHz);
    } else if (data["SamplingFrequency"] == "10MHz") {
        setSamplingFrequency(SamplingFrequency::Fs10MHz);
    } else if (data["SamplingFrequency"] == "5MHz") {
        setSamplingFrequency(SamplingFrequency::Fs5MHz);
    } else if (data["SamplingFrequency"] == "2500kHz") {
        setSamplingFrequency(SamplingFrequency::Fs2500kHz);
    } else if (data["SamplingFrequency"] == "1250kHz") {
        setSamplingFrequency(SamplingFrequency::Fs1250kHz);
    } else {
        readingErrors += " SamplingFrequency,";
    }

    if (data["MaxFileSize"].toLong() == 128) {
        setFileSize(FileSize::Size128MB);
    } else if (data["MaxFileSize"].toLong() == 256) {
        setFileSize(FileSize::Size256MB);
    } else if (data["MaxFileSize"].toLong() == 512) {
        setFileSize(FileSize::Size512MB);
    } else if (data["MaxFileSize"].toLong() == 1024) {
        setFileSize(FileSize::Size1024MB);
    } else if (data["MaxFileSize"].toLong() == 2048) {
        setFileSize(FileSize::Size2048MB);
    } else if (data["MaxFileSize"].toLong() == 4096) {
        setFileSize(FileSize::Size4096MB);
    } else if (data["MaxFileSize"].toLong() == 8192) {
        setFileSize(FileSize::Size8192MB);
    } else if (data["MaxFileSize"].toLong() == 16384) {
        setFileSize(FileSize::Size16384MB);
    } else if (data["MaxFileSize"].toLong() == 32768) {
        setFileSize(FileSize::Size32768MB);
    }  else {
        readingErrors += " MaxFileSize,";
    }

    if (readingErrors[readingErrors.size() - 1] == ",") {
        readingErrors.remove(readingErrors.size() - 1);
    } else {
        readingErrors = QString("No errors");
    }

    return readingErrors;
}

QStringList ScheduleLine::generateScheduleLine()
{
    QStringList schedileLine;
    QString textLine;

    textLine = QString("TaskName:\t\t");
    textLine += m_taskName;
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("RepetitionTime:\t");
    textLine += QString::number(m_repetitionTime);
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("TimeBegin:\t\t");
    textLine += m_timeBegin.toString(QString("hh:mm:ss"));
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("TimeFinish:\t\t");
    textLine += m_timeFinish.toString(QString("hh:mm:ss"));
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("RadarMode:\t");
    switch (m_radarMode) {
    case RadarMode::Rotating:
    {
        textLine += QString("Rotating");
        break;
    }
    case RadarMode::Pointing:
    {
        textLine += QString("Pointing");
        break;
    }
    case RadarMode::Sweeping:
    {
        textLine += QString("Sweeping");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("Rotation speed:\t");
    textLine += QString("n/a");
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("AngleOne:\t\t");
    textLine += QString("n/a");
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("AngleTwo:\t\t");
    textLine += QString("n/a");
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("SweepingMode:\t");
    textLine += QString("n/a");
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("TriggerFrequency:\t");
    switch (m_triggerFrequency) {
    case TriggerFrequency::T1kHz :
    {
        textLine += QString("1 kHz");
        break;
    }
    case TriggerFrequency::T2kHz :
    {
        textLine += QString("2 kHz");
        break;
    }
    case TriggerFrequency::T3kHz :
    {
        textLine += QString("3 kHz");
        break;
    }
    case TriggerFrequency::T4kHz :
    {
        textLine += QString("4 kHz");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("UnAmpSignalLength:\t");
    switch (m_unAmpSignalLength) {
    case UnAmpSignalLength::Length32 :
    {
        textLine += QString("32 Samples");
        break;
    }
    case UnAmpSignalLength::Length64 :
    {
        textLine += QString("64 Samples");
        break;
    }
    case UnAmpSignalLength::Length128 :
    {
        textLine += QString("128 Samples");
        break;
    }
    case UnAmpSignalLength::Length256 :
    {
        textLine += QString("256 Samples");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("AmpSignalBegin:\t");
    switch (m_ampSignalBegin) {
    case AmpSignalOption::Datagram1 :
    {
        textLine += QString("1st Datagram");
        break;
    }
    case AmpSignalOption::Datagram2 :
    {
        textLine += QString("2nd  Datagram");
        break;
    }
    case AmpSignalOption::Datagram3 :
    {
        textLine += QString("3rd Datagram");
        break;
    }
    case AmpSignalOption::Datagram4 :
    {
        textLine += QString("4th Datagram");
        break;
    }
    case AmpSignalOption::Datagram5 :
    {
        textLine += QString("5th Datagram");
        break;
    }
    case AmpSignalOption::Datagram6 :
    {
        textLine += QString("6th Datagram");
        break;
    }
    case AmpSignalOption::Datagram7 :
    {
        textLine += QString("7th Datagram");
        break;
    }
    case AmpSignalOption::Datagram8 :
    {
        textLine += QString("8th Datagram");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("AmpSignalEnd:\t");
    switch (m_ampSignalEnd) {
    case AmpSignalOption::Datagram1 :
    {
        textLine += QString("1st Datagram");
        break;
    }
    case AmpSignalOption::Datagram2 :
    {
        textLine += QString("2nd  Datagram");
        break;
    }
    case AmpSignalOption::Datagram3 :
    {
        textLine += QString("3rd Datagram");
        break;
    }
    case AmpSignalOption::Datagram4 :
    {
        textLine += QString("4th Datagram");
        break;
    }
    case AmpSignalOption::Datagram5 :
    {
        textLine += QString("5th Datagram");
        break;
    }
    case AmpSignalOption::Datagram6 :
    {
        textLine += QString("6th Datagram");
        break;
    }
    case AmpSignalOption::Datagram7 :
    {
        textLine += QString("7th Datagram");
        break;
    }
    case AmpSignalOption::Datagram8 :
    {
        textLine += QString("8th Datagram");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("SamplingFrequency:\t");
    switch (m_samplingFrequency) {
    case SamplingFrequency::Fs1250kHz :
    {
        textLine += QString("1250 kHz");
        break;
    }
    case SamplingFrequency::Fs2500kHz :
    {
        textLine += QString("2500 kHz");
        break;
    }
    case SamplingFrequency::Fs5MHz :
    {
        textLine += QString("5 MHz");
        break;
    }
    case SamplingFrequency::Fs10MHz :
    {
        textLine += QString("10 MHz");
        break;
    }
    case SamplingFrequency::Fs20MHz :
    {
        textLine += QString("20 MHz");
        break;
    }
    case SamplingFrequency::Fs40MHz :
    {
        textLine += QString("40 MHz");
        break;
    }
    case SamplingFrequency::Fs80MHz :
    {
        textLine += QString("80 MHz");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("MaxFileSize:\t");
    switch (m_fileSize) {
    case FileSize::Size128MB :
    {
        textLine += QString("128 MB");
        break;
    }
    case FileSize::Size256MB :
    {
        textLine += QString("256 MB");
        break;
    }
    case FileSize::Size512MB :
    {
        textLine += QString("512 MB");
        break;
    }
    case FileSize::Size1024MB :
    {
        textLine += QString("1024 MB");
        break;
    }
    case FileSize::Size2048MB :
    {
        textLine += QString("2048 MB");
        break;
    }
    case FileSize::Size4096MB :
    {
        textLine += QString("4096 MB");
        break;
    }
    case FileSize::Size8192MB :
    {
        textLine += QString("8192 MB");
        break;
    }
    case FileSize::Size16384MB :
    {
        textLine += QString("16384 MB");
        break;
    }
    case FileSize::Size32768MB :
    {
        textLine += QString("32768 MB");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("BasicFileName:\t");
    textLine += QString(m_basicFileName);
    textLine += QString(";");
    schedileLine.append(textLine);

    return schedileLine;
}

QStringList ScheduleLine::generateTaskLine()
{
    QStringList schedileLine;
    QString textLine;

    schedileLine.append(QString("{"));

    textLine = QString("TaskName = ");
    textLine += m_taskName;
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("RepetitionTime = ");
    textLine += QString::number(m_repetitionTime);
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("TimeBegin = ");
    textLine += m_timeBegin.toString(QString("hh:mm:ss"));
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("TimeFinish = ");
    textLine += m_timeFinish.toString(QString("hh:mm:ss"));
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("RadarMode = ");
    switch (m_radarMode) {
    case RadarMode::Rotating:
    {
        textLine += QString("0");
        break;
    }
    case RadarMode::Pointing:
    {
        textLine += QString("1");
        break;
    }
    case RadarMode::Sweeping:
    {
        textLine += QString("2");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("RotationSpeed = ");
    textLine += QString("0");
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("AngleOne = ");
    textLine += QString("0");
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("AngleTwo = ");
    textLine += QString("0");
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("SweepingMode = ");
    textLine += QString("0");
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("TriggerFrequency = ");
    switch (m_triggerFrequency) {
    case TriggerFrequency::T1kHz :
    {
        textLine += QString("1");
        break;
    }
    case TriggerFrequency::T2kHz :
    {
        textLine += QString("2");
        break;
    }
    case TriggerFrequency::T3kHz :
    {
        textLine += QString("3");
        break;
    }
    case TriggerFrequency::T4kHz :
    {
        textLine += QString("4");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("UnAmpSignalLength = ");
    switch (m_unAmpSignalLength) {
    case UnAmpSignalLength::Length32 :
    {
        textLine += QString("32");
        break;
    }
    case UnAmpSignalLength::Length64 :
    {
        textLine += QString("64");
        break;
    }
    case UnAmpSignalLength::Length128 :
    {
        textLine += QString("128");
        break;
    }
    case UnAmpSignalLength::Length256 :
    {
        textLine += QString("256");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("AmpSignalBegin = ");
    switch (m_ampSignalBegin) {
    case AmpSignalOption::Datagram1 :
    {
        textLine += QString("1");
        break;
    }
    case AmpSignalOption::Datagram2 :
    {
        textLine += QString("2");
        break;
    }
    case AmpSignalOption::Datagram3 :
    {
        textLine += QString("3");
        break;
    }
    case AmpSignalOption::Datagram4 :
    {
        textLine += QString("4");
        break;
    }
    case AmpSignalOption::Datagram5 :
    {
        textLine += QString("5");
        break;
    }
    case AmpSignalOption::Datagram6 :
    {
        textLine += QString("6");
        break;
    }
    case AmpSignalOption::Datagram7 :
    {
        textLine += QString("7");
        break;
    }
    case AmpSignalOption::Datagram8 :
    {
        textLine += QString("8");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("AmpSignalEnd = ");
    switch (m_ampSignalEnd) {
    case AmpSignalOption::Datagram1 :
    {
        textLine += QString("1");
        break;
    }
    case AmpSignalOption::Datagram2 :
    {
        textLine += QString("2");
        break;
    }
    case AmpSignalOption::Datagram3 :
    {
        textLine += QString("3");
        break;
    }
    case AmpSignalOption::Datagram4 :
    {
        textLine += QString("4");
        break;
    }
    case AmpSignalOption::Datagram5 :
    {
        textLine += QString("5");
        break;
    }
    case AmpSignalOption::Datagram6 :
    {
        textLine += QString("6");
        break;
    }
    case AmpSignalOption::Datagram7 :
    {
        textLine += QString("7");
        break;
    }
    case AmpSignalOption::Datagram8 :
    {
        textLine += QString("8");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("SamplingFrequency = ");
    switch (m_samplingFrequency) {
    case SamplingFrequency::Fs1250kHz :
    {
        textLine += QString("1250kHz");
        break;
    }
    case SamplingFrequency::Fs2500kHz :
    {
        textLine += QString("2500kHz");
        break;
    }
    case SamplingFrequency::Fs5MHz :
    {
        textLine += QString("5MHz");
        break;
    }
    case SamplingFrequency::Fs10MHz :
    {
        textLine += QString("10MHz");
        break;
    }
    case SamplingFrequency::Fs20MHz :
    {
        textLine += QString("20MHz");
        break;
    }
    case SamplingFrequency::Fs40MHz :
    {
        textLine += QString("40MHz");
        break;
    }
    case SamplingFrequency::Fs80MHz :
    {
        textLine += QString("80MHz");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("MaxFileSize = ");
    switch (m_fileSize) {
    case FileSize::Size128MB :
    {
        textLine += QString("128");
        break;
    }
    case FileSize::Size256MB :
    {
        textLine += QString("256");
        break;
    }
    case FileSize::Size512MB :
    {
        textLine += QString("512");
        break;
    }
    case FileSize::Size1024MB :
    {
        textLine += QString("1024");
        break;
    }
    case FileSize::Size2048MB :
    {
        textLine += QString("2048");
        break;
    }
    case FileSize::Size4096MB :
    {
        textLine += QString("4096");
        break;
    }
    case FileSize::Size8192MB :
    {
        textLine += QString("8192");
        break;
    }
    case FileSize::Size16384MB :
    {
        textLine += QString("16384");
        break;
    }
    case FileSize::Size32768MB :
    {
        textLine += QString("32768");
        break;
    }
    }
    textLine += QString(";");
    schedileLine.append(textLine);

    textLine = QString("BasicFileName = ");
    textLine += QString(m_basicFileName);
    textLine += QString(";");
    schedileLine.append(textLine);

    schedileLine.append(QString("}"));

    return schedileLine;
}
