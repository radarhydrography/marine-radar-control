/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "scheduleworker.h"

ScheduleWorker::ScheduleWorker(QObject *parent) : QThread(parent)
{
    m_mutex = new QMutex;
    m_workingSchedule = new Schedule;
}

void ScheduleWorker::run()
{
    if (m_workingThreadEnable) {
        emit poolStatus(true);
        emit signalAddLogLine(QString("Working thread started!"));
        emit signalSetWorkingStatus(QString("Job started"));
        emit SignalLogEvent(QString("Schedule: Schedule Worker Started"));
    }

    bool waitingForTask = false;
    bool onTheTask = false;

    quint8 secsFirstStage = 0;
    quint8 secsSecondStage = 3;

    QPair<QTime, QTime> currentTimeTask;

    while (m_workingThreadEnable) {

        if (m_radarIsWarmingUp) {
            emit signalSetWorkingStatus(QString("Warming Up!"));
            waitingForTask = false;
            onTheTask = false;
            msleep(100);
        } else {

            if (!waitingForTask & !onTheTask) {
                currentTimeTask = findNextTask();
                waitingForTask = true;
                if (QTime::currentTime() > currentTimeTask.second) {
                    emit signalAddLogLine(QString("Next Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" is tomorrow"));
                    emit SignalLogEvent(QString("Schedule: Next Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" is tomorrow"));
                } else {
                    emit signalAddLogLine(QString("Next Task is ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName());
                    emit signalSetCurrentScheduleSettings(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask));
                    emit SignalLogEvent(QString("Schedule: Next Task is ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName());
                }
                emit signalSetWorkingStatus(QString("On shcedule"));
                emit signalSetJobProgress(0);
                emit signalSetTransmitionOn(false);
            }

            //        if (waitingForTask) {
            //            emit signalSetTransmitionOn(false);
            //        }

            if (waitingForTask && QTime::currentTime() >= currentTimeTask.first && QTime::currentTime() < currentTimeTask.second) {
                emit signalAddLogLine(QString("Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" started"));
                emit SignalLogEvent(QString("Schedule: Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" started"));
                onTheTask = true;
                waitingForTask = false;
                emit signalSetWorkingStatus(m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName());
                emit signalSetJobProgress(0);
                emit signalSetDataAcquisitionOn(false);
                emit signalSetTransmitionOn(false);
                emit signalSetAntennaRotating(true);
            }

            if (onTheTask && QTime::currentTime() >= (currentTimeTask.first.addSecs(secsFirstStage))) {
                emit signalSetTransmitionOn(true);
            }

            if (onTheTask && QTime::currentTime() >= (currentTimeTask.first.addSecs(secsSecondStage))) {
                emit signalSetDataAcquisitionOn(true);
            }

            if (onTheTask && QTime::currentTime() >= currentTimeTask.second) {
                emit signalSetDataAcquisitionOn(false);
                emit signalSetTransmitionOn(false);
                emit signalSetAntennaRotating(false);
                emit signalAddLogLine(QString("Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" finished"));
                onTheTask = false;
                waitingForTask = false;
                emit signalSetWorkingStatus(QString("Waiting for a new task"));
                emit signalSetJobProgress(100);
            }

            if (onTheTask) {
                QTime cTime = QTime::currentTime();
                double progress = 100*(cTime.msecsSinceStartOfDay() - currentTimeTask.first.msecsSinceStartOfDay())/(currentTimeTask.second.msecsSinceStartOfDay() - currentTimeTask.first.msecsSinceStartOfDay());
                emit signalSetJobProgress(static_cast<int>(progress));
            }

            msleep(10);
        }
    }
    if (!m_workingThreadEnable) {
        if (onTheTask) {
            emit signalAddLogLine(QString("Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" was stopped in the middle"));
            emit SignalLogEvent(QString("Schedule: Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" was stopped in the middle"));
        }
        if (waitingForTask) {
            emit signalAddLogLine(QString("Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" and other were cancelled"));
            emit SignalLogEvent(QString("Schedule: Task ") + m_workingSchedule->getScheduleLine(m_workingSchedule->m_fullTimeSchedule->value(currentTimeTask)).slotGetTaskName() + QString(" and other were cancelled"));
        }
        emit signalSetDataAcquisitionOn(false);
        emit signalSetTransmitionOn(true);
        emit signalSetAntennaRotating(false);
        emit signalSetJobProgress(0);
        emit signalSetWorkingStatus(QString("Job finished"));
        emit poolStatus(false);
    }

    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
    emit signalAddLogLine(QString("Working thread stopped!"));
    emit SignalLogEvent(QString("Schedule: Working thread stopped!"));
    emit signalSetWorkingStatus(QString("Idle"));
}

bool ScheduleWorker::isWorking()
{
    m_mutex->lock();
    bool status = m_workingThreadEnable;
    m_mutex->unlock();
    return status;
}

QPair<QTime, QTime> ScheduleWorker::findNextTask()
{
    int number = 0;

    QList<QPair<QTime, QTime>> fullTimeList;
    fullTimeList.append(m_workingSchedule->m_fullTimeSchedule->keys());

    for (int index = 0; index < m_workingSchedule->m_fullTimeSchedule->size(); index++) {
        if (fullTimeList.at(index).first > QTime::currentTime()) {
            number = index;
            break;
        }
    }

    return fullTimeList[number];
}

void ScheduleWorker::startWorkingThread(const Schedule &workingSchedule)
{
    m_mutex->lock();
    m_workingSchedule->setSchedule(workingSchedule);
    m_workingThreadEnable = true;
    //    emit signalAddLogLine(QString("Starting schedule worker..."));
    m_mutex->unlock();

    if (!isRunning()) {
        start();
        //        setPriority(Priority::TimeCriticalPriority);
    }
}

void ScheduleWorker::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    //    emit signalAddLogLine(QString("Stopping schedule worker..."));
    m_mutex->unlock();
}

void ScheduleWorker::slotRadarIsWarmingUp(const bool &radarIsWarmingUp)
{
    bool radarWasWarmingUp;
    m_mutex->lock();
    radarWasWarmingUp = m_radarIsWarmingUp;
    m_radarIsWarmingUp = radarIsWarmingUp;
    m_mutex->unlock();
    if (radarWasWarmingUp != radarIsWarmingUp) {
        if (radarIsWarmingUp) {
            emit signalAddLogLine(QString("Radar Is Warming Up"));
        } else {
            emit signalAddLogLine(QString("Radar Is Ready To Work"));
        }
    }
}
