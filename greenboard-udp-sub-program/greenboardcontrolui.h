﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef GREENBOARDCONTROLUI_H
#define GREENBOARDCONTROLUI_H

#include <QObject>
#include <QWidget>
#include <QCheckBox>
#include <QComboBox>
#include <QSpinBox>
#include <QPushButton>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QToolButton>
#include <QDateTime>
#include <QHBoxLayout>

#include "shared-libs/ledindicator.h"
#include "greenboardcommunicator.h"

class GreenBoardControlUI : public QWidget
{
    Q_OBJECT

public:
    GreenBoardControlUI(QMap<QString, QString> iniSettings = QMap<QString, QString>(), QWidget *parent = nullptr);
    ~GreenBoardControlUI();

    QWidget *m_fileSavingWidget;
    QWidget *m_externalControlWidget;
    QVBoxLayout *m_informationWidget;
    QWidget *m_motorControlWidget;

    QTimer *m_automaticReconnectTimer;

    QWidget *ExternalControlWidget(const QString &defaulFileName = QString(""));

    void setIniSettings(QMap<QString, QString> &iniSettings, const GreenBoardIniSettingsGroup greenBoardIniSettingsGroup = GreenBoardIniSettingsGroup::FullInitialization);
    void resetIniSettings();
    QMap<QString, QString> getIniSettings();

    void setFullPalette(const QPalette &palette);

private:
    GreenBoardCommunicator *m_greenBoardNetworkCommunicator;

    QMap<QString, QString> m_iniSettings;
    QMap<QString, QString> m_scheduleIniSettings;

    bool m_onSchedule = false;
    bool m_defaultTriggerState = true;

    QToolButton *m_startRx;
    QToolButton *m_stopRx;
    QToolButton *m_openSettings;
    QToolButton *m_restartNetwork;
    QToolButton *m_stopNetwork;
    LedIndicator *m_connectionStatusLedIndicator;
    QLabel *m_connectionStatusLabel;
    LedIndicator *m_triggerOutputLedIndicator;
    LedIndicator *m_blindZoneLedIndicator;
    bool m_disconnected = false;
    bool m_networkError = false;

    QComboBox *m_extSamplingFrrequencyComboBox;
    QComboBox *m_extTriggerFrequencyComboBox;
    QPushButton *m_extTriggerOutCheckBox;
    QPushButton *m_extBlindZonePushButton;
    QComboBox *m_extAdcChannelAlengthComboBox;
    QSpinBox *m_extAdcDelaySpinBox;
    QDoubleSpinBox *m_extAngleOffsetSpinBox;
    QToolButton *m_initPushButton;

    QTimer *m_statusMsgTimer;

    QWidget *m_controlRegisterWidget;
    QWidget *m_controlRegisterWidget2;
    QWidget *m_blindZoneWidget;
    QWidget *m_adcDelayWidget;
    QWidget *m_angleOffsetWidget;
    QWidget *m_avgWidget;
    QGroupBox *m_networkSettingsWidget;
    QToolButton *m_motorControlMenuOpen;

    QGroupBox *NetworkSettingsWidget();
    QLineEdit *m_pcHostAddress;
    QSpinBox *m_pcControlPort;
    QSpinBox *m_pcDataPort;
    QLineEdit *m_boardHostAddress;
    QSpinBox *m_boardControlPort;
    QSpinBox *m_boardDataPort;
    QPushButton *m_networkSet;
    QPushButton *m_networkReset;
    QString m_pcHostAddressValue;
    int m_pcControlPortValue;
    int m_pcDataPortValue;
    QString m_boardHostAddressValue;
    int m_boardControlPortValue;
    int m_boardDataPortValue;

    QWidget *ControlRegisterWidget();
    QComboBox *m_rxEnable;
    QComboBox *m_adcClockSource;
    QComboBox *m_debugMode;
    QComboBox *m_decimation;
    QComboBox *m_startFrequency;
    QComboBox *m_startSignalSource;
    QPushButton *m_setControlSettings;
    QPushButton *m_initADC;
    QCheckBox *m_ledCheckBox;

    GreenBoardControl m_controlSetings;

    QWidget *ControlRegisterWidget2();
    QComboBox *m_startFrequency2;
    QComboBox *m_triggerEnable;
    QComboBox *m_strobeSize;
    QPushButton *m_setControlSettings2;

    GreenBoardControl2 m_controlSettings2;

    QWidget *BlindZoneWidget();
    QDoubleSpinBox *m_blindZoneOneFirst;
    QDoubleSpinBox *m_blindZoneOneSecond;
    QDoubleSpinBox *m_blindZoneTwoFirst;
    QDoubleSpinBox *m_blindZoneTwoSecond;
    QDoubleSpinBox *m_blindZoneThreeFirst;
    QDoubleSpinBox *m_blindZoneThreeSecond;
    QCheckBox *m_blindZoneOne;
    QCheckBox *m_blindZoneTwo;
    QCheckBox *m_blindZoneThree;
    QPushButton *m_setBlindZone;
    bool m_triggerWasEnabledBefore = true;
    bool m_blindZoneIsActive = false;

    QWidget *ADCDelayWidget();
    QLabel *m_adcDelayLabelA;
    QSpinBox *m_adcDelayA;
    QPushButton *m_setAdcDelayA;
    quint16 m_adcDelayValueA = 0;
    QLabel *m_adcDelayLabelB;
    QSpinBox *m_adcDelayB;
    QPushButton *m_setAdcDelayB;
    quint16 m_adcDelayValueB = 0;
    QCheckBox *m_synchronizeDelay;
    bool m_synchronizeDelayFlag;

    GreenBoardAdcDelay m_adcDelaySettingsA;
    GreenBoardAdcDelay m_adcDelaySettingsB;

    QWidget *AngleOffsetWidget();
    QDoubleSpinBox *m_angleOffset;
    QPushButton *m_setAngleOffset;
    QPushButton *m_resetAngleOffset;
    double m_angleOffsetValue;

    QVBoxLayout *InformationWidget();
    QDoubleSpinBox *m_angleSensorValue;
    QDoubleSpinBox *m_bytesReceived;
    QSpinBox *m_terabytesReceived;
    quint64 m_totalBytesReceived = 0;
    quint64 m_totalTeraBytesReceived = 0;
    LedIndicator *m_receivingLedIndicator;
    QTimer *m_receivingIndicatorTimer;
    QToolButton *m_infoOpenButton;

    QWidget *AvgWidget();
    QSpinBox *m_signalAvgCoefficient;
    quint16 m_signalAvgCoefficientValue;
    QPushButton *m_setSignalAvgCoefficient;
    QSpinBox *m_diagramAvgCoefficient;
    quint16 m_diagramCoefficientValue;
    QPushButton *m_setDiagramAvgCoefficient;
    QPushButton *m_resetAvgCoefficients;

    QLabel *m_errorsLabel;
    QLabel *m_receivedLabel;
    QLabel *m_counterLabel;
    QSpinBox *m_errorCounter;
    quint64 m_errorCounterValue = 0;
    QDoubleSpinBox *m_errorCoefCounter;
    QLabel *m_counterBeginningDate;
    QLabel *m_counterBeginningTime;
    QLabel *m_timeoutError;
    QToolButton *m_resetErrors;

//    QLabel *m_counterCurrentDate;
//    QLabel *m_counterCurrentTime;
//    QTimer *m_currentDateTimer;

    QWidget *FileSavingWidget(const QString &defaulFileName = QString(""));
    QLineEdit *m_fileName; 
    QLineEdit *m_filePath;
    QString m_defaultFileName;
    QToolButton *m_refreshFilePath;
    QToolButton *m_pathChangedAutoSaving;
    QToolButton *m_openFile;
    QToolButton *m_openFolder;
    QToolButton *m_savingFile;
    QDoubleSpinBox *m_bytesWritten;
    quint64 m_totalBytesWritten = 0;
    QDoubleSpinBox *m_freeDiskSpace;
    QString m_fileDateTimeInfo = QString("");
    QString m_fileSamplingFrequencyInfo = QString("");
    QString m_fileTriggeringFrequencyInfo = QString("");
    QString m_fileChannelLengthInfo = QString("");
    QString m_fileAngleOffsetInfo = QString("");
    QString m_fileTuningInfo = QString("");
    QWidget *m_suffixPreffixMenu;
    QToolButton *m_additionalFileNameMenu;
    QCheckBox *m_prefixCheckBox;
    QCheckBox *m_suffixCheckBox;
    LedIndicator *m_fileSavingLedIndicator;
    QStorageInfo *m_fileInfo;
    QComboBox *m_fileSizeComboBox;
    QComboBox *m_savingFirstDatagram;
    QComboBox *m_savingLastDatagram;
    QStringList *m_defaultFilePathStringList;
    bool m_savingState = false;
    bool m_freeDiskSpaceWarningState = false;
    bool m_freeDiskSpaceStopSavingState = false;
    double m_freeDiskSpaceWarning = 0;
    double m_freeDiskSpaceStopSaving;
    double m_scheduleTimeCoefficient = -1.0;
private slots:
    bool setNewFileDirectory();
    void SlotUpdateDiskList();
private:
    void setFileDirectory(const QString &filePath);
    void setFreeDiskSpaceStopSavingState();

    QWidget *SuffixPreffixMenu();
    QCheckBox *m_fileDateTimePrefixCheckBox;
    QCheckBox *m_fileSamplingFrequencyFrefixCheckBox;
    QCheckBox *m_fileTriggeringFrequencyFrefixCheckBox;
    QCheckBox *m_fileChannelLengthFrefixCheckBox;
    QCheckBox *m_fileAngleOffsetFrefixCheckBox;
    QCheckBox *m_fileTuningFrefixCheckBox;
    QCheckBox *m_fileDateTimeSuffixCheckBox;
    QCheckBox *m_fileSamplingFrequencySuffixCheckBox;
    QCheckBox *m_fileTriggeringFrequencySuffixCheckBox;
    QCheckBox *m_fileChannelLengthSuffixCheckBox;
    QCheckBox *m_fileAngleOffsetSuffixCheckBox;
    QCheckBox *m_fileTuningSuffixCheckBox;
    QLineEdit *m_fileNamePrefix;
    QLineEdit *m_fileNameSuffix;
    QTimer *m_fileInfoUpdateTimer;
    bool m_receivingIsEnable = false;

//    QWidget *MotorControlWidget();
    QWidget *SpiInterfaceTestWidget();
    QSpinBox *m_spiByte0;
    QSpinBox *m_spiByte1;
    QSpinBox *m_spiByte2;
    QPushButton *m_sendSpiMessage;

    QWidget *MotorControlWidget();
    QToolButton *m_motorRightRotation;
    QToolButton *m_motorLeftRotation;
    QSpinBox *m_motorRotationSpeed1;
    QSpinBox *m_motorRotationSpeed2;
    QToolButton *m_setMotorRotationSpeed;
    QToolButton *m_motorHoldPosition;
    QToolButton *m_rotateMotor;
    QToolButton *m_sweepAntenna;
    QToolButton *m_startMotorControlInterface;
    QToolButton *m_stopMotorControlInterface;
    QDoubleSpinBox *m_antennaPositionAngle1;
    QDoubleSpinBox *m_antennaPositionAngle2;
    QToolButton *m_motorPointing;
    QToolButton *m_motorAnglesMooving;
    QToolButton *m_motorStopEverithing;
    LedIndicator *m_motorMoovingIndicator;
    bool m_angleChanging = false;
    quint16 m_prevAngleValue = 0;
    QTimer *m_motorRotatingindicatorTimer;

    MotorControl m_motorControl;

    bool m_scheduleState = false;
    bool m_scheduleRemoteSavingIsActive = false;

    void UpdateControlSettings(const GreenBoardControl &controlSetings);
    void UpdateControlSettings();
    void UpdateAdcDelaySettings(const GreenBoardAdcDelay &adcDelaySettings);
    void UpdateAdcDelaySettings();

    void SetMotorControlMessage();

    void UISlotMotorRightRotationClicked();
    void UISlotMotorLeftRotationClicked();
    void UISlotRotateMotorClicked();
    void UISlotMotorHoldPositionClicked();
    void UISlotMotorPointingClicked();
    void UISlotMotorAnglesMoovingClicked();
    void UISlotSweepMotorClicked();
    void UISlotMotorStopEverithingClicked();
    void UISlotMotorSpeedClicked();
    void SlotMotorRotatingLedIndicatorUpdate();
    void UISlotStartMotorControlInterface();
    void UISlotStopMotorControlInterface();

    void slotMaxFileSizeIndexChanged(const int &index);
    void slotFirstSavingDatagramIndexChanged(const int &index);
    void slotLastSavingDatagramIndexChanged(const int &index);

    bool m_logBoardIsConnected = false;
    bool m_logBoardLostConnection = false;
    bool m_logTriggerOutIsTurnedOn = false;
    bool m_logDataSavingIsTurnedOn = false;

    QTimer *m_diskListUpdaterTimer;

public slots:
    void SlotSendSpiMessageClicked();
    void SlotSendSpiMessage(const QByteArray &spiMessage);
    void SlotSendSpiMessageList(const QList<QByteArray> &spiMessageList);
    void SlotRestartNetworkClicked();

    void LedCheckBoxClicked();
    void SetControlSettingsClicked();
    void InitADCClicked();
    void SetAdcDelayAClicked();
    void SetAdcDelayBClicked();
    void SynchronizeDelayStateChenged(const bool &checkState);
    void AdcDelayAValueChanged();
    void AngleValueChanged(const quint16 &value);
    void IncreaseErrorCounter(const quint8 &errors = 1);
    void ResetErrorCounter();
    void TimeoutError(const bool &error = false);
    void ResetErrorsClicked();
    void SlotBytesReceived(const quint64 &bytesReceived);
    void SlotBytesWritten(const quint64 &bytesWritten);
    void OpenFileClicked();
    void SavingFileClicked();
    void SlotDataSavingState(const bool &savingState);
    void SlotScheduleState(const bool &scheduleState);
    void SlotStartRxClicked();
    void SlotStopRxClicked();
    void SlotSetAngleOffsetClicked();
    void SlotResetAngleOffsetClicked();
    void OpenSettingsClicked();
    void SlotSetSignalAvgCoefficientClicked();
    void SlotSetDiagramAvgCoefficientClicked();
    void SlotResetAvgCoefficientsClicked();
    void SlotFreeDiskSpace(const qint64 &bytes);
    void SlotSetControlSettings2Clicked();

    void SlotNetworkSetClicked();
    void SlotNetworkResetClicked();

    void exit();

    void SlotTimeoutError(const bool &timeout);
    void SlotTransmitterDisabled(const bool &transmitterDisabled);
    void SlotSetBlindZone();

    void SlotBlindZoneOneCheckStateChanged(const bool &checkState);
    void SlotBlindZoneTwoCheckStateChanged(const bool &checkState);
    void SlotBlindZoneThreeCheckStateChanged(const bool &checkState);

    void SlotStatusMessageReceived(const QString &message);
    void SlotClearStatusMessage();

    void SlotExtSamplingFrequencyChanged(const int &index);
    void SlotExtTriggeringFrequencyChanged(const int &index);
    void SlotExtChannelAlengthChanged(const int &index);
    void SlotExtAngleOffsetChanged();
    void SlotExtTriggerOutCheckStateChanged();
    void SlotLostConnection();
    void SlotDisconnection();
    void SlotConnected();
    void SlotBlindZoneStateChanged();
    void SlotBlindZoneIsActive(const bool &active);

    void SlotOpenFileSettingsMenu();

    void SlotSetFileDateTimeInfo(const QString &text);
    void SlotSetFileSamplingFrequencyInfo(const QString &text);
    void SlotSetFileTriggeringFrequencyInfo(const QString &text);
    void SlotSetFileChannelLengthInfo(const QString &text);
    void SlotSetFileAngleOffsetInfo(const QString &text);
    void SlotSetFileTuningInfo(const QString &text);
    void SlotFilePathTextChanged(const QString &text);

    void SlotUpdateFileDateTimeInfo();
    void SlotUpdateFileSamplingFrequencyInfo();
    void SlotUpdateFileTriggeringFrequencyInfo();
    void SlotUpdateFileChannelLengthInfo();
    void SlotUpdateFileAngleOffsetInfo();
    void SlotUpdateFileTuningInfo(const quint8 &tuning);
    void SlotUpdateFileInfo();
    void SlotSavingLedIndicatorStateChange(const bool &state);
    void SlotExtOpenFolder();
    void SlotReceivingLedIndicatorTimerUpdate();
    void SlotOpenAdditionalInformationClicked();
    void SlotMotorControlOpen();

    void slotSetOnSchedule(const bool onSchedule);
    void slotSetTriggerOutputOn(const bool &isTransmitting);
    void slotSetDataSavingOn(const bool &isSaving);

    void SlotOpenBlindZoneWidgetClicked();

    void SlotScheduleTimeCoefficientChanged(const double &coefficient);

signals:
    void SignalStartDataSavingThread(const QString &fileName, const SavingOption &savingOption);
    void SignalStopDataSavingThread();
    void SignalHasRadarData(const QList<QByteArray> &radarData);
    void SignalAngleOffsetChanged(const quint16 &angleOffset);
    void SignalSetSignalAvgCoefficient(const quint16 &signalAvgCoefficient);
    void SignalSetDiagramAvgCoefficient(const quint16 &diagramAvgCoefficient);
    void SignalFullBeam(const QByteArray &fullBeam);
    void SignalstatusMessage(const QString &message);
    void SignalSamplingFrequency(const int &samplingFrequency);
    void SignalSetBlindZoneOne(const double &firstValue, const double &secondValue);
    void SignalSetBlindZoneTwo(const double &firstValue, const double &secondValue);
    void SignalSetBlindZoneThree(const double &firstValue, const double &secondValue);
    void SignalAdcChannelALengthChanged(const quint16 &length);
    void SignalTxOff(const bool &off);
    void SignalBlindZoneIsActive(const bool &active);
    void SignalDataSavingState(const bool &savingState);
    void SignalResize();

    void SignalLogEvent(const QString &logEvent);

    void SignalCurrentDataFolder(const QString &directory);
};

#endif // GREENBOARDCONTROLUI_H
