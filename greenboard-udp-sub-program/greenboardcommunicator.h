/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef GREENBOARDCOMMUNICATOR_H
#define GREENBOARDCOMMUNICATOR_H

#include <QObject>
#include <QThread>
#include <QtNetwork>
#include <QUdpSocket>
#include <QList>
#include <QByteArray>
#include <QTime>
#include <QTimer>
#include "GreenBoardStruct.h"
#include "datasavingthread.h"
#include "shared-libs/inireader.h"

class GreenBoardCommunicator : public QThread
{
    Q_OBJECT

private:
    QUdpSocket *m_udpControlSocket;
    QUdpSocket *m_udpDataSocket;
    QHostAddress m_pcHostAddress;
    quint64 m_pcControlPort;
    quint64 m_pcDataPort;
    QHostAddress m_boardHostAddress;
    quint64 m_boardControlPort;
    quint64 m_boardDataPort;
    quint16 m_signalStartFrequency = 0;
    quint64 m_errorsCounter = 0;
    quint16 m_angleSensorValue;
    QTimer *m_guiInformationUpdater;
    quint8 m_reconnectCounter = 0;
    QTimer *m_reconnectTimer;

    QPair<double, double> m_blindZoneOne = QPair<double, double>(0, 0);
    QPair<double, double> m_blindZoneTwo = QPair<double, double>(0, 0);
    QPair<double, double> m_blindZoneThree = QPair<double, double>(0, 0);
    bool m_blindZoneOneActivated = false;
    bool m_blindZoneTwoActivated = false;
    bool m_blindZoneThreeActivated = false;
    bool m_blind = false;
    bool m_transmitterWorkingState = true;

    quint16 m_angleOffset = 0;

    QMutex *m_mutex;
    bool m_workingThreadEnable;
    bool m_savingState;

    quint16 m_beamCounter = 0;

    DataSavingThread *m_dataSavingThread;
    QTimer *m_savingThreadCheckTimer;

    quint8 m_firstSavingDatagram = 0;
    quint8 m_lastSavingDatagram = 7;

    bool BlindZoneTrueCheck(const double &angleSensorValue);

    MotorControl m_motorControlStatement;
    QTimer *m_motorControlTimer;

public:
    explicit GreenBoardCommunicator(QMap<QString, QString> iniSettings = QMap<QString, QString>(), QObject *parent = nullptr);
    ~GreenBoardCommunicator();

    void run() override;

    void setPcHostAddress(const QString &hostAddress = QString("192.168.0.1"), const int &controlPort = 16416, const int &dataPort = 16386);
    void setBoardHostAddress(const QString &hostAddress = QString("192.168.0.2"), const int &controlPort = 16480, const int &dataPort = 16481);

    bool isWorking();

    QString CurrentTime();

private:

    void SlotSendSpiMessage(const QByteArray &spiMessage);
    void SlotSendSpiMessageList(const QList<QByteArray> &spiMessageList);

private slots:

    void SlotMotorControlTimeout();

public slots:

    qint8 sendControlMessage(const QByteArray &message);
    qint8 sendControlMessage(const QList<QByteArray> &messageList);

    void startWorkingThread();
    void stopWorkingThread();

    void startMotorControlInterface();
    void stopMotorControlInteface();

    void dataIsSaving(const bool &savingState);

    void SlotSetAngleOffset(const quint16 &angleOffset);

    void SlotSignalStartFrequencyChanged(const IntenernalStartSourceScale &frequency);

    void SlotSetBlindZoneOne(const double &firstValue, const double &secondValue);
    void SlotSetBlindZoneTwo(const double &firstValue, const double &secondValue);
    void SlotSetBlindZoneThree(const double &firstValue, const double &secondValue);

    void SlotUpdateGuiInformation();

    void SlotReconnectTimer();
    void SlotStartReconnectTimer();

    void SlotSavingThreadCheck();

    void slotSetFirstSavingDatagram(const quint8& firstSavingDatagram);
    void slotSetLastSavingDatagram(const quint8& lastSavingDatagram);
    void slotSetSavingDatagramDiapasone(quint8 &firstSavingDatagram, quint8 &lastSavingDatagram);

    void slotSetMaxFileSize(const qint64 &maxFileSize);

    void slotSetMotorControlStatement(const MotorControl &motorControl);

signals:
    void poolStatus(const bool &threadStart);
    void timeoutError(const bool &error);
    void errorsOccurred(const quint8 &errors);
    void signalPackagesLost(const quint8 &number);
    void signalAngleSensorValue(const quint16 &number);
    void signalBytesReceived(const quint64 &bytes);
    void signalBytesWritten(const quint64 &bytes);
    void startSavingThread(const QString &fileName, const SavingOption &savingOption);
    void stopSavingThread();
    void dataSavingState(const bool &savingState);
    void SignalFullBeam(const QByteArray &fullBeam);
    void SignalFullBeamToGui(const QByteArray &fullBeam);
    void signalFreeDiskSpace(const qint64 &bytes);
    void SignalTransmitterDisabled(const bool &transmitterDisabled);
    void SignalDataToSave(const QByteArray &data);
    void SignalstatusMessage(const QString &message);
    void SignalBlindZoneIsActive(const bool &active);

    void SignalDataIsSaving(const bool &state);
    void SignalLogEvent(const QString &logEvent);

    void signalMotorControlIntefaceIsWorking(const bool isWorking);
};

#endif // GREENBOARDCOMMUNICATOR_H
