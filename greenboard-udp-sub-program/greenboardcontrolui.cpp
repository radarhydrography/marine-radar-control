/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/
#include "greenboardcontrolui.h"

#include <QGridLayout>
#include <QUdpSocket>
#include <QString>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QLabel>
#include <QFileDialog>

#include "shared-libs/global.h"
#include "shared-libs/inireader.h"

GreenBoardControlUI::GreenBoardControlUI(QMap<QString, QString> iniSettings, QWidget *parent) : QWidget(parent)
{
    m_defaultFileName = QString("");

    setIniSettings(iniSettings, GreenBoardIniSettingsGroup::FirstInitialization);

    QGridLayout *mainLayout = new QGridLayout;
    QVBoxLayout *leftLayout = new QVBoxLayout;
    QVBoxLayout *rightLayout = new QVBoxLayout;

    m_controlRegisterWidget = ControlRegisterWidget();
    m_controlRegisterWidget2 = ControlRegisterWidget2();
    m_blindZoneWidget = BlindZoneWidget();
    m_adcDelayWidget = ADCDelayWidget();
    m_angleOffsetWidget = AngleOffsetWidget();
    m_avgWidget = AvgWidget();
    QWidget *spiTestWidget = SpiInterfaceTestWidget();
    m_networkSettingsWidget = NetworkSettingsWidget();

    leftLayout->addWidget(m_controlRegisterWidget);
    leftLayout->addWidget(spiTestWidget);

    rightLayout->addWidget(m_controlRegisterWidget2);
    rightLayout->addWidget(m_adcDelayWidget);

    mainLayout->addLayout(leftLayout, 0, 1);
    mainLayout->addLayout(rightLayout, 0, 2);
    mainLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), 1, 1, 1, 2);
    mainLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 0, 4, 1);
    mainLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 3, 4, 1);
    mainLayout->addWidget(m_angleOffsetWidget, 2, 1, 1, 2);
    mainLayout->addWidget(m_networkSettingsWidget, 3, 1, 1, 2);

    this->setLayout(mainLayout);
    setWindowTitle("GreenBoard Control Pannel");

    UpdateControlSettings();
    UpdateAdcDelaySettings();

    m_externalControlWidget = ExternalControlWidget(m_defaultFileName);
    m_informationWidget = InformationWidget();

    m_greenBoardNetworkCommunicator = new GreenBoardCommunicator(m_iniSettings, this);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::errorsOccurred, this, &GreenBoardControlUI::IncreaseErrorCounter);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::signalPackagesLost, this, &GreenBoardControlUI::IncreaseErrorCounter, Qt::AutoConnection);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::timeoutError, this, &GreenBoardControlUI::TimeoutError);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::signalAngleSensorValue, this, &GreenBoardControlUI::AngleValueChanged, Qt::AutoConnection);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::signalBytesReceived, this, &GreenBoardControlUI::SlotBytesReceived);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::signalBytesWritten, this, &GreenBoardControlUI::SlotBytesWritten);
    connect(this, &GreenBoardControlUI::SignalStartDataSavingThread, m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::startSavingThread);
    connect(this, &GreenBoardControlUI::SignalStopDataSavingThread, m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::stopSavingThread);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::dataSavingState, this, &GreenBoardControlUI::SlotDataSavingState);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SignalFullBeamToGui, this, &GreenBoardControlUI::SignalFullBeam);
    connect(this, &GreenBoardControlUI::SignalAngleOffsetChanged, m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SlotSetAngleOffset);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::timeoutError, this, &GreenBoardControlUI::SlotTimeoutError);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::signalFreeDiskSpace, this, &GreenBoardControlUI::SlotFreeDiskSpace);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SignalTransmitterDisabled, this, &GreenBoardControlUI::SlotTransmitterDisabled);
    connect(this, &GreenBoardControlUI::SignalSetBlindZoneOne, m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SlotSetBlindZoneOne);
    connect(this, &GreenBoardControlUI::SignalSetBlindZoneTwo, m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SlotSetBlindZoneTwo);
    connect(this, &GreenBoardControlUI::SignalSetBlindZoneThree, m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SlotSetBlindZoneThree);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SignalBlindZoneIsActive, this, &GreenBoardControlUI::SlotBlindZoneIsActive);

    m_statusMsgTimer = new QTimer;
    m_statusMsgTimer->setInterval(3000);
    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SignalstatusMessage, this, &GreenBoardControlUI::SlotStatusMessageReceived);
    connect(m_statusMsgTimer, &QTimer::timeout, this, &GreenBoardControlUI::SlotClearStatusMessage);

    connect(m_extAdcChannelAlengthComboBox, &QComboBox::currentTextChanged, this, &GreenBoardControlUI::SlotUpdateFileChannelLengthInfo);
    connect(m_extAngleOffsetSpinBox, &QSpinBox::editingFinished, this, &GreenBoardControlUI::SlotUpdateFileAngleOffsetInfo);
    connect(m_extSamplingFrrequencyComboBox, &QComboBox::currentTextChanged, this, &GreenBoardControlUI::SlotUpdateFileSamplingFrequencyInfo);
    connect(m_extTriggerFrequencyComboBox, &QComboBox::currentTextChanged, this, &GreenBoardControlUI::SlotUpdateFileTriggeringFrequencyInfo);

    connect(m_fileSizeComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &GreenBoardControlUI::slotMaxFileSizeIndexChanged);
    connect(m_savingFirstDatagram, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &GreenBoardControlUI::slotFirstSavingDatagramIndexChanged);
    connect(m_savingLastDatagram, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &GreenBoardControlUI::slotLastSavingDatagramIndexChanged);

    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SignalDataIsSaving, this, &GreenBoardControlUI::SlotSavingLedIndicatorStateChange);

    SlotSetAngleOffsetClicked();
//    SlotResetAvgCoefficientsClicked();
//    SlotSetSignalAvgCoefficientClicked();
    InitADCClicked();
    SetControlSettingsClicked();
    SlotSetControlSettings2Clicked();
    SetAdcDelayAClicked();
    SetAdcDelayBClicked();
    SlotNetworkResetClicked();

    setIniSettings(iniSettings, GreenBoardIniSettingsGroup::SecondInitialization);

    connect(m_greenBoardNetworkCommunicator, &GreenBoardCommunicator::SignalLogEvent, this, &GreenBoardControlUI::SignalLogEvent);

    this->setMaximumWidth(width());
}

GreenBoardControlUI::~GreenBoardControlUI()
{
    m_greenBoardNetworkCommunicator->~GreenBoardCommunicator();

    if (m_rxEnable->currentIndex() == 0) {
        m_greenBoardNetworkCommunicator->stopWorkingThread();
    }

    exit();
}

QWidget *GreenBoardControlUI::ControlRegisterWidget()
{
    QGroupBox *controlRegisterWidget = new QGroupBox;
    controlRegisterWidget->setTitle("Control Settings");

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *topLayout = new QHBoxLayout;
    QHBoxLayout *midLayout = new QHBoxLayout;
    QHBoxLayout *botLayout = new QHBoxLayout;

    QVBoxLayout *rxEnableLayout = new QVBoxLayout;
    QLabel *rxEnableLabel = new QLabel(tr("ADC Rx"));
    rxEnableLayout->addWidget(rxEnableLabel);

    m_rxEnable = new QComboBox;
    QStringList rxEnable;
    rxEnable.append(QString(tr("Disabled")));
    rxEnable.append(QString(tr("Enabled")));
    m_rxEnable->addItems(rxEnable);
    m_rxEnable->setCurrentIndex(0);
    rxEnableLayout->addWidget(m_rxEnable);

    QVBoxLayout *adcClockSourceLayout = new QVBoxLayout;
    QLabel *adcClockSourceLabel = new QLabel(tr("Clock Source"));
    adcClockSourceLayout->addWidget(adcClockSourceLabel);

    m_adcClockSource = new QComboBox;
    QStringList adcClockSource;
    adcClockSource.append(QString(tr("Internal")));
    adcClockSource.append(QString(tr("External")));
    m_adcClockSource->addItems(adcClockSource);
    m_adcClockSource->setCurrentIndex(0);
    adcClockSourceLayout->addWidget(m_adcClockSource);

    QVBoxLayout *debugModeLayout = new QVBoxLayout;
    QLabel *debugModeLabel = new QLabel(tr("Debug"));
    debugModeLayout->addWidget(debugModeLabel);

    m_debugMode = new QComboBox;
    QStringList debugMode;
    debugMode.append(QString(tr("Disabled")));
    debugMode.append(QString(tr("Enabled")));
    m_debugMode->addItems(debugMode);
    m_debugMode->setCurrentIndex(0);
    debugModeLayout->addWidget(m_debugMode);

    QVBoxLayout *decimationLayout = new QVBoxLayout;
    QLabel *decimationLabel = new QLabel(tr("Sampl Freq"));
    decimationLayout->addWidget(decimationLabel);

    m_decimation = new QComboBox;
    QStringList decimation;

//    decimation.append(QString(tr("1")));
//    decimation.append(QString(tr("2")));
//    decimation.append(QString(tr("4")));
//    decimation.append(QString(tr("8")));
//    decimation.append(QString(tr("16")));
//    decimation.append(QString(tr("32")));
//    decimation.append(QString(tr("64")));
//    decimation.append(QString(tr("128")));

    decimation.append(QString(tr("80 MHz")));
    decimation.append(QString(tr("40 MHz")));
    decimation.append(QString(tr("20 MHz")));
    decimation.append(QString(tr("10 MHz")));
    decimation.append(QString(tr("5 MHz")));
    decimation.append(QString(tr("2500 kHz")));
    decimation.append(QString(tr("1125 kHz")));
    decimation.append(QString(tr("562.5 kHz")));
    m_decimation->addItems(decimation);
    m_decimation->setCurrentIndex(0);
    decimationLayout->addWidget(m_decimation);

    QVBoxLayout *startFrequencyLayout = new QVBoxLayout;
    QLabel *startFrequencyLabel = new QLabel(tr("Trig Freq"));
    startFrequencyLayout->addWidget(startFrequencyLabel);

    m_startFrequency = new QComboBox;
    QStringList startFrequency;
    startFrequency.append(QString(tr("1 kHz")));
    startFrequency.append(QString(tr("2 kHz")));
    m_startFrequency->addItems(startFrequency);
    m_startFrequency->setCurrentIndex(0);
    startFrequencyLayout->addWidget(m_startFrequency);

    QVBoxLayout *startSignalSourceLayout = new QVBoxLayout;
    QLabel *startSignalSourceLabel = new QLabel(tr("Trig Src"));
    startSignalSourceLayout->addWidget(startSignalSourceLabel);

    m_startSignalSource = new QComboBox;
    QStringList startSignalSource;
    startSignalSource.append(QString(tr("Internal")));
    startSignalSource.append(QString(tr("External")));
    m_startSignalSource->addItems(startSignalSource);
    m_startSignalSource->setCurrentIndex(0);
    startSignalSourceLayout->addWidget(m_startSignalSource);

    m_ledCheckBox = new QCheckBox;
    m_ledCheckBox->setText("Test LED");
    m_ledCheckBox->setCheckState(Qt::Checked);
    connect(m_ledCheckBox, &QCheckBox::clicked, this, &GreenBoardControlUI::LedCheckBoxClicked);

    m_setControlSettings = new QPushButton;
    m_setControlSettings->setText("Set");
    connect(m_setControlSettings, &QPushButton::clicked, this, &GreenBoardControlUI::SetControlSettingsClicked);

    m_initADC = new QPushButton;
    m_initADC->setText("Init ADC");
    connect(m_initADC, &QPushButton::clicked, this, &GreenBoardControlUI::InitADCClicked);

    topLayout->addLayout(rxEnableLayout);
    topLayout->addLayout(adcClockSourceLayout);
    topLayout->addLayout(debugModeLayout);
    midLayout->addLayout(decimationLayout);
    midLayout->addLayout(startFrequencyLayout);
    midLayout->addLayout(startSignalSourceLayout);
    botLayout->addWidget(m_ledCheckBox);
    botLayout->addWidget(m_setControlSettings);
    botLayout->addWidget(m_initADC);

    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(midLayout);
    mainLayout->addLayout(botLayout);

    controlRegisterWidget->setLayout(mainLayout);

    SlotUpdateFileSamplingFrequencyInfo();

    return controlRegisterWidget;
}

QWidget *GreenBoardControlUI::ControlRegisterWidget2()
{
    QGroupBox *mainWidget = new QGroupBox(QString("Control Settings 2"));

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *topLayout = new QHBoxLayout;
    QHBoxLayout *bottomLayout = new QHBoxLayout;

    QVBoxLayout *startFrequencyLayout = new QVBoxLayout;
    startFrequencyLayout->addWidget(new QLabel(QString("Trig Freq")));
    m_startFrequency2 = new QComboBox;
    QStringList startFrequencyList;
    startFrequencyList.append(QString("1 kHz"));
    startFrequencyList.append(QString("2 kHz"));
    startFrequencyList.append(QString("3 kHz"));
    startFrequencyList.append(QString("4 kHz"));
    m_startFrequency2->addItems(startFrequencyList);
    startFrequencyLayout->addWidget(m_startFrequency2);
    m_startFrequency2->setCurrentIndex(m_controlSettings2.getFrequencyScaleIndex());

    QVBoxLayout *triggerEnableLayout = new QVBoxLayout;
    triggerEnableLayout->addWidget(new QLabel(QString("Trig Out")));
    m_triggerEnable = new QComboBox;
    QStringList triggerEnableList;
    triggerEnableList.append(QString("Enabled"));
    triggerEnableList.append(QString("Disabled"));
    m_triggerEnable->addItems(triggerEnableList);
    triggerEnableLayout->addWidget(m_triggerEnable);
    m_triggerEnable->setCurrentIndex(m_controlSettings2.getTriggerOut());

    QVBoxLayout *strobSizeLayout = new QVBoxLayout;
    strobSizeLayout->addWidget(new QLabel(QString("Strob Size")));
    m_strobeSize = new QComboBox;
    QStringList strobSizeList;
    strobSizeList.append(QString("A: 32 B: 2016"));
    strobSizeList.append(QString("A: 64 B: 1984"));
    strobSizeList.append(QString("A: 128 B: 1920"));
    strobSizeList.append(QString("A: 256 B: 1792"));
    m_strobeSize->addItems(strobSizeList);
    strobSizeLayout->addWidget(m_strobeSize);
    m_strobeSize->setCurrentIndex(m_controlSettings2.getStrobeSize());

    topLayout->addLayout(startFrequencyLayout);
    topLayout->addLayout(triggerEnableLayout);
    bottomLayout->addLayout(strobSizeLayout);
//    middleLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

    QGridLayout *buttonLayout = new QGridLayout;
    buttonLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 0, 1, 2);
    m_setControlSettings2 = new QPushButton(QString("Set"));
    connect(m_setControlSettings2, &QPushButton::clicked, this, &GreenBoardControlUI::SlotSetControlSettings2Clicked);
    buttonLayout->addWidget(m_setControlSettings2, 1, 1);
    bottomLayout->addLayout(buttonLayout);

    mainLayout->addLayout(topLayout);
//    mainLayout->addLayout(middleLayout);
    mainLayout->addLayout(bottomLayout);

    mainWidget->setLayout(mainLayout);

    SlotUpdateFileTriggeringFrequencyInfo();
    SlotUpdateFileChannelLengthInfo();

    return mainWidget;
}

QWidget *GreenBoardControlUI::BlindZoneWidget()
{
    QWidget *mainWidget = new QWidget(this);

    QGridLayout *mainLayout = new QGridLayout;

    m_blindZoneOne = new QCheckBox(QString("Sct 1"));
    m_blindZoneOne->setChecked(false);
    connect(m_blindZoneOne, &QCheckBox::stateChanged, this, &GreenBoardControlUI::SlotBlindZoneOneCheckStateChanged);

    m_blindZoneTwo = new QCheckBox(QString("Sct 2"));
    m_blindZoneTwo->setChecked(false);
    connect(m_blindZoneTwo, &QCheckBox::stateChanged, this, &GreenBoardControlUI::SlotBlindZoneTwoCheckStateChanged);

    m_blindZoneThree = new QCheckBox(QString("Sct 3"));
    m_blindZoneThree->setChecked(false);
    connect(m_blindZoneThree, &QCheckBox::stateChanged, this, &GreenBoardControlUI::SlotBlindZoneThreeCheckStateChanged);

    mainLayout->addWidget(m_blindZoneOne, 0, 1);
    mainLayout->addWidget(m_blindZoneTwo, 0, 2);
    mainLayout->addWidget(m_blindZoneThree, 0, 3);
    mainLayout->addWidget(new QLabel(QString("Limit 1")), 1, 0);
    mainLayout->addWidget(new QLabel(QString("Limit 2")), 2, 0);

    m_blindZoneOneFirst = new QDoubleSpinBox;
    m_blindZoneOneFirst->setMinimum(0);
    m_blindZoneOneFirst->setMaximum(360);
    m_blindZoneOneFirst->setValue(0.0);
    m_blindZoneOneFirst->setDecimals(2);
    m_blindZoneOneFirst->setSingleStep(0.01);
    m_blindZoneOneFirst->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_blindZoneOneFirst->setEnabled(false);

    m_blindZoneOneSecond = new QDoubleSpinBox;
    m_blindZoneOneSecond->setMinimum(0);
    m_blindZoneOneSecond->setMaximum(360);
    m_blindZoneOneSecond->setValue(0.0);
    m_blindZoneOneSecond->setDecimals(2);
    m_blindZoneOneSecond->setSingleStep(0.01);
    m_blindZoneOneSecond->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_blindZoneOneSecond->setEnabled(false);

    m_blindZoneTwoFirst = new QDoubleSpinBox;
    m_blindZoneTwoFirst->setMinimum(0);
    m_blindZoneTwoFirst->setMaximum(360);
    m_blindZoneTwoFirst->setValue(0.0);
    m_blindZoneTwoFirst->setDecimals(2);
    m_blindZoneTwoFirst->setSingleStep(0.01);
    m_blindZoneTwoFirst->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_blindZoneTwoFirst->setEnabled(false);

    m_blindZoneTwoSecond = new QDoubleSpinBox;
    m_blindZoneTwoSecond->setMinimum(0);
    m_blindZoneTwoSecond->setMaximum(360);
    m_blindZoneTwoSecond->setValue(0.0);
    m_blindZoneTwoSecond->setDecimals(2);
    m_blindZoneTwoSecond->setSingleStep(0.01);
    m_blindZoneTwoSecond->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_blindZoneTwoSecond->setEnabled(false);

    m_blindZoneThreeFirst = new QDoubleSpinBox;
    m_blindZoneThreeFirst->setMinimum(0);
    m_blindZoneThreeFirst->setMaximum(360);
    m_blindZoneThreeFirst->setValue(0.0);
    m_blindZoneThreeFirst->setDecimals(2);
    m_blindZoneThreeFirst->setSingleStep(0.01);
    m_blindZoneThreeFirst->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_blindZoneThreeFirst->setEnabled(false);

    m_blindZoneThreeSecond = new QDoubleSpinBox;
    m_blindZoneThreeSecond->setMinimum(0);
    m_blindZoneThreeSecond->setMaximum(360);
    m_blindZoneThreeSecond->setValue(0.0);
    m_blindZoneThreeSecond->setDecimals(2);
    m_blindZoneThreeSecond->setSingleStep(0.01);
    m_blindZoneThreeSecond->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_blindZoneThreeSecond->setEnabled(false);

    mainLayout->addWidget(m_blindZoneOneFirst, 1, 1);
    mainLayout->addWidget(m_blindZoneOneSecond, 2, 1);
    mainLayout->addWidget(m_blindZoneTwoFirst, 1, 2);
    mainLayout->addWidget(m_blindZoneTwoSecond, 2, 2);
    mainLayout->addWidget(m_blindZoneThreeFirst, 1, 3);
    mainLayout->addWidget(m_blindZoneThreeSecond, 2, 3);

    m_setBlindZone = new QPushButton(QString("Set"));
    connect(m_setBlindZone, &QPushButton::clicked, this, &GreenBoardControlUI::SlotSetBlindZone);
    QHBoxLayout *bottomLayout = new QHBoxLayout;
    bottomLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    bottomLayout->addWidget(m_setBlindZone);
    mainLayout->addLayout(bottomLayout, 3, 0, 1, 4);

    mainWidget->setLayout(mainLayout);

    mainWidget->setWindowTitle(QString("Blind Zone Settings"));
    mainWidget->setWindowFlag(Qt::Window);

    return mainWidget;
}

QWidget *GreenBoardControlUI::ADCDelayWidget()
{
    QGroupBox *adcDelayWidget = new QGroupBox;
    adcDelayWidget->setTitle("ADC Delay Settings");

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *controlLayout = new QHBoxLayout;

    QHBoxLayout *topLayout = new QHBoxLayout;
    m_adcDelayLabelA = new QLabel;
    m_adcDelayLabelA->setText(QString("ADC A"));
    topLayout->addWidget(m_adcDelayLabelA);

    m_adcDelayA = new QSpinBox;
    m_adcDelayA->setMaximum(65535);
    m_adcDelayA->setMinimum(0);
    m_adcDelayA->setValue(0);
    m_adcDelayA->setAlignment(Qt::AlignRight);
    topLayout->addWidget(m_adcDelayA);

    m_setAdcDelayA = new QPushButton;
    m_setAdcDelayA->setText("Set");
    topLayout->addWidget(m_setAdcDelayA);
    connect(m_setAdcDelayA, &QPushButton::clicked, this, &GreenBoardControlUI::SetAdcDelayAClicked);

    QHBoxLayout *bottomLayout = new QHBoxLayout;
    m_adcDelayLabelB = new QLabel;
    m_adcDelayLabelB->setText(QString("ADC B"));
    bottomLayout->addWidget(m_adcDelayLabelB);

    m_adcDelayB = new QSpinBox;
    m_adcDelayB->setMaximum(65535);
    m_adcDelayB->setMinimum(0);
    m_adcDelayB->setValue(0);
    m_adcDelayB->setAlignment(Qt::AlignRight);
    bottomLayout->addWidget(m_adcDelayB);

    m_setAdcDelayB = new QPushButton;
    m_setAdcDelayB->setText("Set");
    bottomLayout->addWidget(m_setAdcDelayB);
    connect(m_setAdcDelayB, &QPushButton::clicked, this, &GreenBoardControlUI::SetAdcDelayBClicked);

    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(bottomLayout);

    m_synchronizeDelay = new QCheckBox;
    m_synchronizeDelay->setText(QString("Synchronize"));
    connect(m_synchronizeDelay, &QCheckBox::stateChanged, this, &GreenBoardControlUI::SynchronizeDelayStateChenged);
    controlLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    controlLayout->addWidget(m_synchronizeDelay);
    m_synchronizeDelay->setChecked(m_synchronizeDelayFlag);

    mainLayout->addLayout(controlLayout);

    adcDelayWidget->setLayout(mainLayout);

    return adcDelayWidget;
}

QWidget *GreenBoardControlUI::AngleOffsetWidget()
{
    QGroupBox *mainWidget = new QGroupBox(QString(tr("Angle Offset")));
    QHBoxLayout *mainLayout = new QHBoxLayout;

    m_angleOffset = new QDoubleSpinBox;
    m_angleOffset->setMinimum(0);
    m_angleOffset->setMaximum(360.0);
    m_angleOffset->setDecimals(2);
    m_angleOffset->setSingleStep(0.1);
    m_angleOffset->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_angleOffset->setValue(m_angleOffsetValue);
    mainLayout->addWidget(m_angleOffset);

    QSpacerItem *spacer = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    mainLayout->addItem(spacer);

    m_setAngleOffset = new QPushButton(QString(tr("Set")));
    connect(m_setAngleOffset, &QPushButton::clicked, this, &GreenBoardControlUI::SlotSetAngleOffsetClicked);
    mainLayout->addWidget(m_setAngleOffset);

    m_resetAngleOffset = new QPushButton(QString(tr("Reset")));
    connect(m_resetAngleOffset, &QPushButton::clicked, this, &GreenBoardControlUI::SlotResetAngleOffsetClicked);
    mainLayout->addWidget(m_resetAngleOffset);

    mainWidget->setLayout(mainLayout);

    SlotUpdateFileAngleOffsetInfo();

    return mainWidget;
}

QVBoxLayout *GreenBoardControlUI::InformationWidget()
{
//    QGroupBox *informationWidget = new QGroupBox;
//    QWidget *informationWidget = new QWidget;
//    informationWidget->setTitle(QString(""));

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *topLayout = new QHBoxLayout;

    QLabel *angleSensorLabel = new QLabel;
    angleSensorLabel->setText(QString("Angle "));

    m_angleSensorValue = new QDoubleSpinBox;
    m_angleSensorValue->setReadOnly(true);
    m_angleSensorValue->setMinimum(0);
    m_angleSensorValue->setDecimals(1);
    m_angleSensorValue->setMaximum(361.0);
    m_angleSensorValue->setSuffix(QString("°"));
    m_angleSensorValue->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_angleSensorValue->setAlignment(Qt::AlignRight);    

    m_receivingLedIndicator = new LedIndicator;
    m_receivingLedIndicator->setState(LedState::LedOff);

    m_receivingIndicatorTimer = new QTimer;
    connect(m_receivingIndicatorTimer, &QTimer::timeout, this, &GreenBoardControlUI::SlotReceivingLedIndicatorTimerUpdate);
    m_receivingIndicatorTimer->start(1000);

    QLabel *bytesReceivedLabel = new QLabel;
    bytesReceivedLabel->setText(QString("Receiving"));

    m_errorCounter = new QSpinBox;
    m_errorCounter->setReadOnly(true);
    m_errorCounter->setMinimum(0);
    m_errorCounter->setMaximum(2147483647);
    m_errorCounter->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_errorCounter->setAlignment(Qt::AlignRight);

    m_terabytesReceived = new QSpinBox;
    m_terabytesReceived->setReadOnly(true);
    m_terabytesReceived->setMinimum(0);
    m_terabytesReceived->setMaximum(999);
    m_terabytesReceived->setSingleStep(1);
    m_terabytesReceived->setSuffix(QString(" TB"));
    m_terabytesReceived->setValue(0);
    m_terabytesReceived->setButtonSymbols(QSpinBox::NoButtons);
    m_terabytesReceived->setAlignment(Qt::AlignRight);
    m_terabytesReceived->setVisible(false);

    m_bytesReceived = new QDoubleSpinBox;
    m_bytesReceived->setReadOnly(true);
    m_bytesReceived->setMinimum(0);
    m_bytesReceived->setMaximum(10000.0);
    m_bytesReceived->setSuffix(QString(" B"));
    m_bytesReceived->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_bytesReceived->setAlignment(Qt::AlignRight);

    m_resetErrors = new QToolButton;
    m_resetErrors->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_resetErrors->setToolTip(QString("Refresh"));
    connect(m_resetErrors, &QPushButton::clicked, this, &GreenBoardControlUI::ResetErrorsClicked);

    m_infoOpenButton = new QToolButton;
    m_infoOpenButton->setText(QString("i"));
    QFont font;
    font.setItalic(true);
    font.setBold(true);
    m_infoOpenButton->setFont(font);
    m_infoOpenButton->setCheckable(true);
//    connect(m_infoOpenButton, &QPushButton::clicked, this, &GreenBoardControlUI::SlotOpenAdditionalInformationClicked);

//    topLayout->addWidget(m_infoOpenButton);

    QHBoxLayout *errorsLayout = new QHBoxLayout;
    m_receivedLabel = new QLabel(QString("Received"));

//    QHBoxLayout *currentTimeLayout = new QHBoxLayout;

    m_errorsLabel = new QLabel(QString("Lost Total"));

    m_errorCoefCounter = new QDoubleSpinBox;
    m_errorCoefCounter->setMaximum(9999);
    m_errorCoefCounter->setMinimum(0);
    m_errorCoefCounter->setSingleStep(0.001);
    m_errorCoefCounter->setDecimals(2);
    m_errorCoefCounter->setSuffix(QString("B"));
    m_errorCoefCounter->setValue(0);
    m_errorCoefCounter->setReadOnly(true);
    m_errorCoefCounter->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_errorCoefCounter->setAlignment(Qt::AlignRight);

    m_counterLabel = new QLabel(QString("Started:"));
    QFont lFont;
    lFont.setItalic(true);
    m_counterLabel->setFont(lFont);

    m_counterBeginningDate = new QLabel;
    QDateTime dateTime = QDateTime::currentDateTime();
    QString date = dateTime.toString(QString("dd.MM.yyyy  "));
    m_counterBeginningDate->setText(date);
    lFont.setBold(true);
    lFont.setItalic(true);
    m_counterBeginningDate->setFont(lFont);

    m_counterBeginningTime = new QLabel;
    date = dateTime.toString(QString("hh:mm:ss"));
    m_counterBeginningTime->setText(date);
    font.setBold(false);
    m_counterBeginningTime->setFont(font);

    topLayout->addWidget(m_receivingLedIndicator);
    topLayout->addWidget(bytesReceivedLabel);
    topLayout->addWidget(m_terabytesReceived);
    topLayout->addWidget(m_bytesReceived);
    topLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum));
    topLayout->addWidget(angleSensorLabel);
    topLayout->addWidget(m_angleSensorValue);

    errorsLayout->addWidget(m_errorsLabel);
    errorsLayout->addWidget(m_errorCounter);
    errorsLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    errorsLayout->addWidget(m_counterLabel);
    errorsLayout->addWidget(m_counterBeginningTime);
    errorsLayout->addWidget(m_counterBeginningDate);
    errorsLayout->addWidget(m_resetErrors);

    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(errorsLayout);
//    mainLayout->addLayout(currentTimeLayout);

//    informationWidget->setLayout(mainLayout);


//    SlotOpenAdditionalInformationClicked();

//    informationWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    return mainLayout;
}

QWidget *GreenBoardControlUI::AvgWidget()
{
    QGroupBox *mainWidget = new QGroupBox(QString(tr("Average Data")));
    QGridLayout *mainLayout = new QGridLayout;
    QHBoxLayout *leftLayout = new QHBoxLayout;
    QHBoxLayout *rightLayout = new QHBoxLayout;

    QLabel *avgSignalLabel = new QLabel(QString(tr("Average Signal")));
    mainLayout->addWidget(avgSignalLabel, 0, 0);

    QLabel *avgCircleLabel = new QLabel(QString(tr("Average Diagram")));
    mainLayout->addWidget(avgCircleLabel, 0, 1);

    m_signalAvgCoefficient = new QSpinBox;
    m_signalAvgCoefficient->setMinimum(1);
    m_signalAvgCoefficient->setMaximum(1000);
    m_signalAvgCoefficient->setValue(1);
    m_signalAvgCoefficient->setButtonSymbols(QSpinBox::NoButtons);
    leftLayout->addWidget(m_signalAvgCoefficient);

    m_setSignalAvgCoefficient = new QPushButton(QString(tr("Set")));
    connect(m_setSignalAvgCoefficient, &QPushButton::clicked, this, &GreenBoardControlUI::SlotSetSignalAvgCoefficientClicked);
    leftLayout->addWidget(m_setSignalAvgCoefficient);
    mainLayout->addLayout(leftLayout, 1, 0);

    m_diagramAvgCoefficient = new QSpinBox;
    m_diagramAvgCoefficient->setMinimum(1);
    m_diagramAvgCoefficient->setMaximum(1000);
    m_diagramAvgCoefficient->setValue(1);
    m_diagramAvgCoefficient->setButtonSymbols(QSpinBox::NoButtons);
    rightLayout->addWidget(m_diagramAvgCoefficient);

    m_setDiagramAvgCoefficient = new QPushButton(QString(tr("Set")));
    connect(m_setDiagramAvgCoefficient, &QPushButton::clicked, this, &GreenBoardControlUI::SlotSetDiagramAvgCoefficientClicked);
    rightLayout->addWidget(m_setDiagramAvgCoefficient);
    mainLayout->addLayout(rightLayout, 1, 1);

    QHBoxLayout *rightBottomLayout = new QHBoxLayout;

    m_resetAvgCoefficients = new QPushButton(QString(tr("Reset")));
    connect(m_resetAvgCoefficients, &QPushButton::clicked, this, &GreenBoardControlUI::SlotResetAvgCoefficientsClicked);
    rightBottomLayout->addWidget(m_resetAvgCoefficients);

    QSpacerItem *spacer = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    rightBottomLayout->addItem(spacer);

    mainLayout->addLayout(rightBottomLayout, 2, 0, 1, 2);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *GreenBoardControlUI::ExternalControlWidget(const QString &defaulFileName)
{
    QWidget *externalControlWidget = new QWidget;

    QGroupBox *controlGroupBox = new QGroupBox;
    controlGroupBox->setTitle("Coherence Board Control");

    QGridLayout *mainControlLayout = new QGridLayout;

    m_restartNetwork = new QToolButton;
    m_restartNetwork->setToolTip(QString("Connect"));
    m_restartNetwork->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("connect.svg")));
    connect(m_restartNetwork, &QPushButton::clicked, this, &GreenBoardControlUI::SlotRestartNetworkClicked);

    m_stopNetwork = new QToolButton;
    m_stopNetwork->setToolTip(QString("Disconnect"));
    m_stopNetwork->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("disconnect.svg")));
    connect(m_stopNetwork, &QPushButton::clicked, this, &GreenBoardControlUI::SlotStopRxClicked);

    m_motorControlMenuOpen = new QToolButton;
    m_motorControlMenuOpen->setText(QString("M"));
    m_motorControlMenuOpen->setCheckable(true);
    m_motorControlMenuOpen->setToolTip(QString("Open/Close Motor Control"));

    m_initPushButton = new QToolButton;
    m_initPushButton->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_initPushButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    m_initPushButton->setToolTip(QString("Reset GreenBoard to Default Settings"));

    m_openSettings = new QToolButton;
    m_openSettings->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("document-properties.svg")));
    m_openSettings->setToolTip(QString("Open GreenBoard Settings"));
    QHBoxLayout *settingsLayout = new QHBoxLayout;
    settingsLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    settingsLayout->addWidget(m_motorControlMenuOpen);
    settingsLayout->addWidget(m_initPushButton);
    settingsLayout->addWidget(m_openSettings);

    m_connectionStatusLedIndicator = new LedIndicator;
    m_connectionStatusLabel = new QLabel(QString("Not Connected"));
    m_triggerOutputLedIndicator = new LedIndicator;
    m_triggerOutputLedIndicator->setState(LedState::LedOn);
    m_extTriggerOutCheckBox = new QPushButton(QString("Trig Out"));
    m_extTriggerOutCheckBox->setCheckable(true);
    m_extTriggerOutCheckBox->setChecked(false);
    m_extTriggerOutCheckBox->setToolTip(QString("Enable/Disable Radar Pulse Output"));
    m_blindZoneLedIndicator = new LedIndicator;

    m_extBlindZonePushButton = new QPushButton("Blind Zone");
//    m_extBlindZonePushButton->setCheckable(true);
    m_extBlindZonePushButton->setChecked(false);
    m_extBlindZonePushButton->setToolTip(QString("Blind Zone Settings"));

    m_extSamplingFrrequencyComboBox = new QComboBox;
    QStringList freqList;
    freqList.append(QString("80 MHz"));
    freqList.append(QString("40 MHz"));
    freqList.append(QString("20 MHz"));
    freqList.append(QString("10 MHz"));
    freqList.append(QString("5 MHz"));
    freqList.append(QString("2500 kHz"));
    freqList.append(QString("1125 kHz"));
    freqList.append(QString("565.5kHz"));
    m_extSamplingFrrequencyComboBox->addItems(freqList);
    m_extSamplingFrrequencyComboBox->setCurrentIndex(m_controlSetings.getDecimationIndex());
    m_extSamplingFrrequencyComboBox->setToolTip(QString("Sampling Frequency"));

    m_extTriggerFrequencyComboBox = new QComboBox;
    QStringList trFreqList;
    trFreqList.append(QString("1 kHz"));
    trFreqList.append(QString("2 kHz"));
    trFreqList.append(QString("3 kHz"));
    trFreqList.append(QString("4 kHz"));
    m_extTriggerFrequencyComboBox->addItems(trFreqList);
    m_extTriggerFrequencyComboBox->setCurrentIndex(m_controlSettings2.getFrequencyScaleIndex());
    m_extTriggerFrequencyComboBox->setToolTip(QString("Radar Pulse Repetition Frequency"));

    m_extAdcChannelAlengthComboBox = new QComboBox;
    QStringList chAlengthList;
    chAlengthList.append(QString("32"));
    chAlengthList.append(QString("64"));
    chAlengthList.append(QString("128"));
    chAlengthList.append(QString("256"));
    m_extAdcChannelAlengthComboBox->addItems(chAlengthList);
    m_extAdcChannelAlengthComboBox->setCurrentIndex(m_controlSettings2.getStrobeSize());
    m_extAdcChannelAlengthComboBox->setToolTip(QString("Unamplified Channel Length (Samples)"));

    m_extAdcDelaySpinBox = new QSpinBox;
    m_extAdcDelaySpinBox->setMaximum(65535);
    m_extAdcDelaySpinBox->setMinimum(0);
    m_extAdcDelaySpinBox->setValue(0);
    m_extAdcDelaySpinBox->setAlignment(Qt::AlignRight);

    m_extAngleOffsetSpinBox = new QDoubleSpinBox;
    m_extAngleOffsetSpinBox->setMinimum(0);
    m_extAngleOffsetSpinBox->setMaximum(360.0);
    m_extAngleOffsetSpinBox->setDecimals(2);
    m_extAngleOffsetSpinBox->setSingleStep(0.1);
    m_extAngleOffsetSpinBox->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_extAngleOffsetSpinBox->setValue(m_angleOffsetValue);
    m_extAngleOffsetSpinBox->setToolTip(QString("Zero Angle Offset Value"));

    QFrame *line = new QFrame;
    line->setFrameShape(QFrame::VLine);
    line->setFrameShadow(QFrame::Sunken);

    QHBoxLayout *connectionStatusLayout = new QHBoxLayout;
    connectionStatusLayout->addWidget(m_connectionStatusLabel);
    connectionStatusLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    connectionStatusLayout->addWidget(m_restartNetwork);
    connectionStatusLayout->addWidget(m_stopNetwork);

    mainControlLayout->addWidget(m_connectionStatusLedIndicator, 0, 0);
    mainControlLayout->addLayout(connectionStatusLayout, 0, 1, 1, 5);
    mainControlLayout->addWidget(m_triggerOutputLedIndicator, 1, 0);
    mainControlLayout->addWidget(m_extTriggerOutCheckBox, 1, 1);
    mainControlLayout->addWidget(m_blindZoneLedIndicator, 2, 0);
    mainControlLayout->addWidget(m_extBlindZonePushButton, 2, 1);
    mainControlLayout->addWidget(line, 1, 2, 2, 1);
    mainControlLayout->addLayout(settingsLayout, 0, 6);

    mainControlLayout->addWidget(new QLabel(QString("Fs")), 1, 3);
    mainControlLayout->addWidget(m_extSamplingFrrequencyComboBox, 1, 4);
    mainControlLayout->addWidget(new QLabel(QString("Ft")), 2, 3);
    mainControlLayout->addWidget(m_extTriggerFrequencyComboBox, 2, 4);
    mainControlLayout->addWidget(new QLabel(QString("Ch A Length")), 1, 5);
    mainControlLayout->addWidget(m_extAdcChannelAlengthComboBox, 1, 6);
//    mainControlLayout->addWidget(new QLabel(QString("D")), 2, 3);
//    mainControlLayout->addWidget(m_extAdcDelaySpinBox, 2, 4);
    mainControlLayout->addWidget(new QLabel(QString("Angle Offset")), 2, 5);
    mainControlLayout->addWidget(m_extAngleOffsetSpinBox, 2, 6);

    connect(m_extSamplingFrrequencyComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &GreenBoardControlUI::SlotExtSamplingFrequencyChanged);
    connect(m_extTriggerFrequencyComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &GreenBoardControlUI::SlotExtTriggeringFrequencyChanged);
    connect(m_extAdcChannelAlengthComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &GreenBoardControlUI::SlotExtChannelAlengthChanged);
    connect(m_extAngleOffsetSpinBox, &QSpinBox::editingFinished, this, &GreenBoardControlUI::SlotExtAngleOffsetChanged);
    connect(m_extTriggerOutCheckBox, &QCheckBox::clicked, this, &GreenBoardControlUI::SlotExtTriggerOutCheckStateChanged);
    connect(m_blindZoneOne, &QCheckBox::stateChanged, this, &GreenBoardControlUI::SlotBlindZoneStateChanged);
    connect(m_blindZoneTwo, &QCheckBox::stateChanged, this, &GreenBoardControlUI::SlotBlindZoneStateChanged);
    connect(m_blindZoneThree, &QCheckBox::stateChanged, this, &GreenBoardControlUI::SlotBlindZoneStateChanged);
    connect(m_extBlindZonePushButton, &QPushButton::clicked, this, &GreenBoardControlUI::SlotOpenBlindZoneWidgetClicked);

    controlGroupBox->setLayout(mainControlLayout);

    m_motorControlWidget = MotorControlWidget();
    m_motorControlWidget->setVisible(false);
    connect(m_motorControlMenuOpen, &QPushButton::clicked, this, &GreenBoardControlUI::SlotMotorControlOpen);

    m_fileSavingWidget = FileSavingWidget(defaulFileName);

    QVBoxLayout *mainLayout = new QVBoxLayout;

    mainLayout->addWidget(controlGroupBox);
    mainLayout->addWidget(m_motorControlWidget);
    mainLayout->addWidget(m_fileSavingWidget);

    externalControlWidget->setLayout(mainLayout);

    connect(m_openSettings, &QPushButton::clicked, this, &GreenBoardControlUI::OpenSettingsClicked);

    m_automaticReconnectTimer = new QTimer;
    m_automaticReconnectTimer->setInterval(3000);
    connect(m_automaticReconnectTimer, &QTimer::timeout, this, &GreenBoardControlUI::SlotRestartNetworkClicked);

    return externalControlWidget;
}

void GreenBoardControlUI::setIniSettings(QMap<QString, QString> &iniSettings, const GreenBoardIniSettingsGroup greenBoardIniSettingsGroup)
{
    if (greenBoardIniSettingsGroup != GreenBoardIniSettingsGroup::ScheduleParameters) {
        m_iniSettings = iniSettings;
        IniReader::checkConfiguration(m_iniSettings, SettingsGroup::GreenBoardSettings);
        emit SignalLogEvent(QString("GreenBoard: Setup New Settings"));
    } else {
        emit SignalLogEvent(QString("GreenBoard: Setup Schedule Settings"));
        m_scheduleIniSettings = iniSettings;
        IniReader::checkConfiguration(m_scheduleIniSettings, SettingsGroup::GreenBoardScheduleSettings);
        m_controlSetings.m_frequencyScale = static_cast<IntenernalStartSourceScale>(m_scheduleIniSettings["BoardInrernalTriggerFrequency"].toUInt());
        m_controlSettings2.m_frequencyScale = static_cast<IntenernalStartSourceScale>(m_scheduleIniSettings["BoardTriggerFrequency"].toUInt());
        m_controlSettings2.m_strobeSize = static_cast<StrobeSize>(m_scheduleIniSettings["BoardAdcALength"].toUInt());
        m_controlSetings.m_decimation = static_cast<SignalDecimation>(m_scheduleIniSettings["BoardDecimation"].toUInt());
        m_fileSizeComboBox->setCurrentIndex(static_cast<quint16>(m_scheduleIniSettings["MaxFileSize"].toUInt()));
        m_savingFirstDatagram->setCurrentIndex(static_cast<quint16>(m_scheduleIniSettings["FirstSavingDatagram"].toUInt()));
        m_savingLastDatagram->setCurrentIndex(static_cast<quint16>(m_scheduleIniSettings["LastSavingDatagram"].toUInt()));
        m_defaultFileName = m_scheduleIniSettings["DefaultFileName"];
    }

    if (greenBoardIniSettingsGroup == GreenBoardIniSettingsGroup::FirstInitialization) {
        m_fileInfo = new QStorageInfo;

        m_defaultFilePathStringList = new QStringList;
        QDir dir;
        if ((m_iniSettings["dataFolder0"] != QString("")) && dir.exists(m_iniSettings["dataFolder0"])) {
            m_defaultFilePathStringList->append(m_iniSettings["dataFolder0"]);
        }
        if ((m_iniSettings["dataFolder1"] != QString("")) && dir.exists(m_iniSettings["dataFolder1"])) {
            m_defaultFilePathStringList->append(m_iniSettings["dataFolder1"]);
        }
        if ((m_iniSettings["dataFolder2"] != QString("")) && dir.exists(m_iniSettings["dataFolder2"])) {
            m_defaultFilePathStringList->append(m_iniSettings["dataFolder2"]);
        }
        if ((m_iniSettings["dataFolder3"] != QString("")) && dir.exists(m_iniSettings["dataFolder3"])) {
            m_defaultFilePathStringList->append(m_iniSettings["dataFolder3"]);
        }
        if ((m_iniSettings["dataFolder4"] != QString("")) && dir.exists(m_iniSettings["dataFolder4"])) {
            m_defaultFilePathStringList->append(m_iniSettings["dataFolder4"]);
        }
        if ((m_iniSettings["dataFolder5"] != QString("")) && dir.exists(m_iniSettings["dataFolder5"])) {
            m_defaultFilePathStringList->append(m_iniSettings["dataFolder5"]);
        }
        if ((m_iniSettings["dataFolder6"] != QString("")) && dir.exists(m_iniSettings["dataFolder6"])) {
            m_defaultFilePathStringList->append(m_iniSettings["dataFolder6"]);
        }
        if ((m_iniSettings["dataFolder7"] != QString("")) && dir.exists(m_iniSettings["dataFolder7"])) {
            m_defaultFilePathStringList->append(m_iniSettings["dataFolder7"]);
        }
    }

    if ((greenBoardIniSettingsGroup == GreenBoardIniSettingsGroup::FirstInitialization) || (greenBoardIniSettingsGroup == GreenBoardIniSettingsGroup::FullInitialization)) {

        m_controlSetings.m_rxEnable = static_cast<RxEnable>(m_iniSettings["BoardRxEnable"].toUInt());
        m_controlSetings.m_clockSignalSource = static_cast<ClockSignalSource>(m_iniSettings["BoardClockSignalSource"].toUInt());
        m_controlSetings.m_operationMode = static_cast<AdcOperationMode>(m_iniSettings["BoardAdcOperationMode"].toUInt());
        m_controlSetings.m_decimation = static_cast<SignalDecimation>(m_iniSettings["BoardDecimation"].toUInt());
        m_controlSetings.m_frequencyScale = static_cast<IntenernalStartSourceScale>(m_iniSettings["BoardInrernalTriggerFrequency"].toUInt());
        m_controlSetings.m_startSignalSource = static_cast<StartSignalSource>(m_iniSettings["BoardTriggerSignalSource"].toUInt());

        m_adcDelaySettingsA.m_adcNumber = 0;
        m_adcDelaySettingsA.m_delayValue = m_iniSettings["BoardAdcADelay"].toUInt();
        m_adcDelayValueA = m_adcDelaySettingsA.m_delayValue;
        m_adcDelaySettingsB.m_adcNumber = 1;
        m_adcDelaySettingsB.m_delayValue = m_iniSettings["BoardAdcBDelay"].toUInt();
        m_adcDelayValueB = m_adcDelaySettingsB.m_delayValue;
        m_synchronizeDelayFlag = static_cast<bool>(m_iniSettings["BoardAdcDelaySynchro"].toUInt());
        m_angleOffsetValue = m_iniSettings["BoardAngleOffset"].toDouble();
        m_signalAvgCoefficientValue = m_iniSettings["SignalAvgCoefficient"].toUInt();
        m_diagramCoefficientValue = 1;
        m_defaultFileName = m_iniSettings["DefaultFileName"];

        m_controlSettings2.m_frequencyScale = static_cast<IntenernalStartSourceScale>(m_iniSettings["BoardTriggerFrequency"].toUInt());
        m_controlSettings2.m_triggerOut = static_cast<TriggerOut>(m_iniSettings["BoardTriggerOff"].toUInt());
        m_controlSettings2.m_strobeSize = static_cast<StrobeSize>(m_iniSettings["BoardAdcALength"].toUInt());

        m_pcHostAddressValue = m_iniSettings["PcHostAddress"];
        m_pcControlPortValue = m_iniSettings["PcControlPort"].toUInt();
        m_pcDataPortValue = m_iniSettings["PcDataPort"].toUInt();

        m_boardHostAddressValue = m_iniSettings["BoardHostAddress"];
        m_boardControlPortValue = m_iniSettings["BoardControlPort"].toUInt();
        m_boardDataPortValue = m_iniSettings["BoardDataPort"].toUInt();
    }

    if ((greenBoardIniSettingsGroup == GreenBoardIniSettingsGroup::SecondInitialization) || (greenBoardIniSettingsGroup == GreenBoardIniSettingsGroup::FullInitialization)) {
        m_fileSizeComboBox->setCurrentIndex(static_cast<quint8>(m_iniSettings.value(QString("MaxFileSize")).toUInt()));
        m_savingFirstDatagram->setCurrentIndex(static_cast<quint8>(m_iniSettings.value(QString("FirstSavingDatagram")).toUInt()));
        m_savingLastDatagram->setCurrentIndex(static_cast<quint8>(m_iniSettings.value(QString("LastSavingDatagram")).toUInt()));

        m_pathChangedAutoSaving->setChecked(static_cast<bool>(m_iniSettings[QString("ContinueSavingAfterPathChanged")].toUInt()));

        m_blindZoneOneFirst->setValue(static_cast<double>(m_iniSettings.value(QString("BlindSectorOneBegin")).toDouble()));
        m_blindZoneOneSecond->setValue(static_cast<double>(m_iniSettings.value(QString("BlindSectorOneEnd")).toDouble()));
        m_blindZoneTwoFirst->setValue(static_cast<double>(m_iniSettings.value(QString("BlindSectorTwoBegin")).toDouble()));
        m_blindZoneTwoSecond->setValue(static_cast<double>(m_iniSettings.value(QString("BlindSectorTwoEnd")).toDouble()));
        m_blindZoneThreeFirst->setValue(static_cast<double>(m_iniSettings.value(QString("BlindSectorThreeBegin")).toDouble()));
        m_blindZoneThreeSecond->setValue(static_cast<double>(m_iniSettings.value(QString("BlindSectorThreeEnd")).toDouble()));
        m_blindZoneOne->setChecked(static_cast<bool>(m_iniSettings.value(QString("BlindSectorOneEnabled")).toUInt()));
        m_blindZoneTwo->setChecked(static_cast<bool>(m_iniSettings.value(QString("BlindSectorTwoEnabled")).toUInt()));
        m_blindZoneThree->setChecked(static_cast<bool>(m_iniSettings.value(QString("BlindSectorThreeEnabled")).toUInt()));
        m_setBlindZone->click();
    }

    if (greenBoardIniSettingsGroup != GreenBoardIniSettingsGroup::FirstInitialization) {
        m_angleOffset->setValue(m_angleOffsetValue);
        m_rxEnable->setCurrentIndex(m_controlSetings.getRxEnableIndex());
        m_adcClockSource->setCurrentIndex(m_controlSetings.getClockSignalSourceIndex());
        m_debugMode->setCurrentIndex(m_controlSetings.getOperationModeIndex());
        m_startFrequency->setCurrentIndex(m_controlSetings.getFrequencyScaleIndex());
        m_startSignalSource->setCurrentIndex(m_controlSetings.getStartSignalSourceIndex());
        m_decimation->setCurrentIndex(m_controlSetings.getDecimationIndex());

        m_startFrequency2->setCurrentIndex(m_controlSettings2.getFrequencyScaleIndex());
        m_strobeSize->setCurrentIndex(m_controlSettings2.getStrobeSize());

        m_extSamplingFrrequencyComboBox->setCurrentIndex(m_controlSetings.getDecimationIndex());
        m_extTriggerFrequencyComboBox->setCurrentIndex(m_controlSettings2.getFrequencyScaleIndex());;
        m_extTriggerOutCheckBox->setChecked(m_controlSettings2.getTriggerOut());
        m_extAdcChannelAlengthComboBox->setCurrentIndex(m_controlSettings2.getStrobeSize());
        m_extAngleOffsetSpinBox->setValue(m_angleOffsetValue);

        m_adcDelayA->setValue(m_adcDelaySettingsA.m_delayValue);
        m_adcDelayB->setValue(m_adcDelaySettingsB.m_delayValue);

        m_fileName->setText(m_defaultFileName);
        SetControlSettingsClicked();
        SlotSetControlSettings2Clicked();
        SlotSetAngleOffsetClicked();
        SetAdcDelayAClicked();
        SetAdcDelayBClicked();

        if (static_cast<bool>(m_iniSettings.value(QString("MotorControlAutoStart")).toUInt())) {
            UISlotStartMotorControlInterface();
        } else {
            UISlotStopMotorControlInterface();
        }
    }

}

void GreenBoardControlUI::resetIniSettings()
{
    emit SignalLogEvent(QString("GreenBoard: Setup Default Settings"));
    setIniSettings(m_iniSettings);
}

QMap<QString, QString> GreenBoardControlUI::getIniSettings()
{
    m_iniSettings["BoardRxEnable"] = QString::number(static_cast<quint16>(m_controlSetings.m_rxEnable));
    m_iniSettings["BoardClockSignalSource"] = QString::number(static_cast<quint16>(m_controlSetings.m_clockSignalSource));
    m_iniSettings["BoardAdcOperationMode"] = QString::number(static_cast<quint16>(m_controlSetings.m_operationMode));
    m_iniSettings["BoardDecimation"] = QString::number(static_cast<quint16>(m_controlSetings.m_decimation));
    m_iniSettings["BoardInrernalTriggerFrequency"] = QString::number(static_cast<quint16>(m_controlSetings.m_frequencyScale));
    m_iniSettings["BoardTriggerSignalSource"] = QString::number(static_cast<quint16>(m_controlSetings.m_startSignalSource));

    m_iniSettings["BoardAdcADelay"] = QString::number(static_cast<quint16>(m_adcDelaySettingsA.m_delayValue));
    m_iniSettings["BoardAdcBDelay"] = QString::number(static_cast<quint16>(m_adcDelaySettingsB.m_delayValue));
    m_iniSettings["BoardAdcDelaySynchro"] = QString::number(static_cast<quint16>(m_synchronizeDelayFlag ));
    m_iniSettings["BoardAngleOffset"] = QString::number(static_cast<double>(m_angleOffsetValue));
    m_iniSettings["SignalAvgCoefficient"] = QString::number(static_cast<quint16>(m_signalAvgCoefficientValue));
    m_iniSettings["DefaultFileName"] = m_defaultFileName;

    m_iniSettings["BoardTriggerFrequency"] = QString::number(static_cast<quint16>(m_controlSettings2.m_frequencyScale));
    m_iniSettings["BoardTriggerOff"] = QString::number(static_cast<quint16>(m_controlSettings2.m_triggerOut));
    m_iniSettings["BoardAdcALength"] = QString::number(static_cast<quint16>(m_controlSettings2.m_strobeSize));

    m_iniSettings["PcHostAddress"] = m_pcHostAddressValue;
    m_iniSettings["PcControlPort"] = QString::number(static_cast<int>(m_pcControlPortValue));
    m_iniSettings["PcDataPort"] = QString::number(static_cast<int>(m_pcDataPortValue));

    m_iniSettings["BoardHostAddress"] = m_boardHostAddressValue;
    m_iniSettings["BoardControlPort"] = QString::number(static_cast<int>(m_boardControlPortValue));
    m_iniSettings["BoardDataPort"] = QString::number(static_cast<int>(m_boardDataPortValue));

    m_iniSettings["MaxFileSize"] = QString::number(static_cast<quint16>(m_fileSizeComboBox->currentIndex()));
    m_iniSettings["FirstSavingDatagram"] = QString::number(static_cast<quint16>(m_savingFirstDatagram->currentIndex()));
    m_iniSettings["LastSavingDatagram"] = QString::number(static_cast<quint16>(m_savingLastDatagram->currentIndex()));

    m_iniSettings["BlindSectorOneBegin"] = QString::number(static_cast<double>(m_blindZoneOneFirst->value()));
    m_iniSettings["BlindSectorOneEnd"] = QString::number(static_cast<double>(m_blindZoneOneSecond->value()));
    m_iniSettings["BlindSectorTwoBegin"] = QString::number(static_cast<double>(m_blindZoneTwoFirst->value()));
    m_iniSettings["BlindSectorTwoEnd"] = QString::number(static_cast<double>(m_blindZoneTwoSecond->value()));
    m_iniSettings["BlindSectorThreeBegin"] = QString::number(static_cast<double>(m_blindZoneThreeFirst->value()));
    m_iniSettings["BlindSectorThreeEnd"] = QString::number(static_cast<double>(m_blindZoneThreeSecond->value()));
    m_iniSettings["BlindSectorOneEnabled"] = QString::number(static_cast<quint16>(m_blindZoneOne->isChecked()));
    m_iniSettings["BlindSectorTwoEnabled"] = QString::number(static_cast<quint16>(m_blindZoneTwo->isChecked()));
    m_iniSettings["BlindSectorThreeEnabled"] = QString::number(static_cast<quint16>(m_blindZoneThree->isChecked()));

    IniReader::checkConfiguration(m_iniSettings, SettingsGroup::GreenBoardSettings);
    QMap<QString, QString> iniSettings = m_iniSettings;

    return iniSettings;
}

void GreenBoardControlUI::setFullPalette(const QPalette &palette)
{
    setPalette(palette);
    m_blindZoneWidget->setPalette(palette);
    m_suffixPreffixMenu->setPalette(palette);
}

QGroupBox *GreenBoardControlUI::NetworkSettingsWidget()
{
    QGroupBox *mainWidget = new QGroupBox(tr("Network"));

    QGridLayout *mainLayout = new QGridLayout;

    QString IpRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    QRegularExpression IpRegex ("^" + IpRange
                                    + "(\\." + IpRange + ")"
                                    + "(\\." + IpRange + ")"
                                    + "(\\." + IpRange + ")$");
    QRegularExpressionValidator *ipValidator = new QRegularExpressionValidator(IpRegex, this);

    m_pcHostAddress = new QLineEdit;
    m_pcHostAddress->setValidator(ipValidator);
    m_pcHostAddress->setEnabled(false);
    QLabel *pcHostAddressLabel = new QLabel(QString(tr("PC Address")));
    m_pcControlPort = new QSpinBox;
    m_pcControlPort->setMinimum(0);
    m_pcControlPort->setMaximum(65535);
    m_pcControlPort->setButtonSymbols(QSpinBox::NoButtons);
    m_pcControlPort->setValue(0);
    m_pcControlPort->setEnabled(false);
    QLabel *pcControlPortLabel = new QLabel(QString(tr("Control Port")));
    m_pcDataPort = new QSpinBox;
    m_pcDataPort->setMinimum(0);
    m_pcDataPort->setMaximum(65535);
    m_pcDataPort->setButtonSymbols(QSpinBox::NoButtons);
    m_pcDataPort->setValue(0);
    m_pcDataPort->setEnabled(false);
    QLabel *pcDataPortLabel = new QLabel(QString(tr("Data Port")));
    m_boardHostAddress = new QLineEdit;
    m_boardHostAddress->setValidator(ipValidator);
    m_boardHostAddress->setEnabled(false);
    QLabel *boardHostAddressLabel = new QLabel(QString(tr("Board Address")));
    m_pcHostAddress->setMaximumWidth(120);
    m_boardHostAddress->setMaximumWidth(120);
    m_boardControlPort = new QSpinBox;
    m_boardControlPort->setMinimum(0);
    m_boardControlPort->setMaximum(65535);
    m_boardControlPort->setButtonSymbols(QSpinBox::NoButtons);
    m_boardControlPort->setValue(0);
    m_boardControlPort->setEnabled(false);
    QLabel *boardControlPortLabel = new QLabel(QString(tr("Control Port")));
    m_boardDataPort = new QSpinBox;
    m_boardDataPort->setMinimum(0);
    m_boardDataPort->setMaximum(65535);
    m_boardDataPort->setButtonSymbols(QSpinBox::NoButtons);
    m_boardDataPort->setValue(0);
    m_boardDataPort->setEnabled(false);
    QLabel *boardDataPortLabel = new QLabel(QString(tr("Data Port")));

    m_networkSet = new QPushButton(QString(tr(" Set ")));
    connect(m_networkSet, &QPushButton::clicked, this, &GreenBoardControlUI::SlotNetworkSetClicked);
    m_networkReset = new QPushButton(QString(tr("Reset")));
    connect(m_networkReset, &QPushButton::clicked, this, &GreenBoardControlUI::SlotNetworkResetClicked);

    QHBoxLayout *startStopLayout = new QHBoxLayout;
    m_startRx = new QToolButton;
    m_startRx->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("play.svg")));
    m_startRx->setToolTip(QString("Enstablish Connection"));
    m_stopRx = new QToolButton;
    m_stopRx->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("pause.svg")));
    m_stopRx->setToolTip(QString("Stop Dara Receiving"));
    startStopLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    startStopLayout->addWidget(m_stopRx);
    startStopLayout->addWidget(m_startRx);
    connect(m_startRx, &QPushButton::clicked, this, &GreenBoardControlUI::SlotStartRxClicked);
    connect(m_stopRx, &QPushButton::clicked, this, &GreenBoardControlUI::SlotStopRxClicked);

    mainLayout->addWidget(pcHostAddressLabel, 0, 0);
    mainLayout->addWidget(pcControlPortLabel, 0, 1);
    mainLayout->addWidget(pcDataPortLabel, 0, 2);
    mainLayout->addWidget(m_pcHostAddress, 1, 0);
    mainLayout->addWidget(m_pcControlPort, 1, 1);
    mainLayout->addWidget(m_pcDataPort, 1, 2);
    mainLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 3, 3, 1);
    mainLayout->addWidget(boardHostAddressLabel, 2, 0);
    mainLayout->addWidget(boardControlPortLabel, 2, 1);
    mainLayout->addWidget(boardDataPortLabel, 2, 2);
    mainLayout->addWidget(m_boardHostAddress, 3, 0);
    mainLayout->addWidget(m_boardControlPort, 3, 1);
    mainLayout->addWidget(m_boardDataPort, 3, 2);
    mainLayout->addLayout(startStopLayout, 0, 5);
//    mainLayout->addWidget(m_networkSet, 4, 1);
//    mainLayout->addWidget(m_networkReset, 4, 2);

//    QLabel *errorCounterLabel = new QLabel;
//    errorCounterLabel->setText(QString("Lost Datagrams"));
//    mainLayout->addWidget(errorCounterLabel, 2, 4);

    m_timeoutError = new QLabel;
    m_timeoutError->setText(QString("Timeout Error!"));
    m_timeoutError->setVisible(false);
    m_timeoutError->setAlignment(Qt::AlignRight);
    mainLayout->addWidget(m_timeoutError, 0, 4, 1, 2);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *GreenBoardControlUI::FileSavingWidget(const QString &defaulFileName)
{
    QGroupBox *fileSavingWidget = new QGroupBox;
    fileSavingWidget->setTitle(QString(tr("Data File")));

    QVBoxLayout *mainLayout = new QVBoxLayout;

    QHBoxLayout *topLayout = new QHBoxLayout;
    QHBoxLayout *middleLayout = new QHBoxLayout;

    QLabel *pathToFile = new QLabel;
    pathToFile->setText(QString(tr("Path:")));
    topLayout->addWidget(pathToFile);

    m_filePath = new QLineEdit;
    topLayout->addWidget(m_filePath);
    m_filePath->setToolTip(QString("Path to Data Acquisition Folder"));
    connect(m_filePath, &QLineEdit::textChanged, this, &GreenBoardControlUI::SlotFilePathTextChanged);

    m_fileName = new QLineEdit;
    m_fileName->setText(defaulFileName);

    m_pathChangedAutoSaving = new QToolButton;
    m_pathChangedAutoSaving->setCheckable(true);
    m_pathChangedAutoSaving->setChecked(false);
    m_pathChangedAutoSaving->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("emblem-synchronizing.svg")));
    m_pathChangedAutoSaving->setToolTip(QString("Continue Saving After Path Automatically Changed"));

    m_refreshFilePath = new QToolButton;
    m_refreshFilePath->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_refreshFilePath->setToolTip(QString("Refresh Path From The List"));
    connect(m_refreshFilePath, &QPushButton::clicked, this, &GreenBoardControlUI::setNewFileDirectory);

    m_openFile = new QToolButton;
    m_openFile->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("folder.svg")));
    m_openFile->setToolTip(QString("Set Path to Data Acquisition Folder"));
    connect(m_openFile, &QPushButton::clicked, this, &GreenBoardControlUI::OpenFileClicked);

    m_openFolder = new QToolButton;
    m_openFolder->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("folder-open.svg")));
    m_openFolder->setToolTip(QString("Open Data Acquisition Folder"));
    connect(m_openFolder, &QPushButton::clicked, this, &GreenBoardControlUI::SlotExtOpenFolder);

    m_savingFile = new QToolButton;
    m_savingFile->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("document-save.svg")));
    m_savingFile->setCheckable(true);
    m_savingFile->setChecked(false);
    m_savingFile->setToolTip(QString("Enable/Disable Data Acqusition"));
    connect(m_savingFile, &QCheckBox::clicked, this, &GreenBoardControlUI::SavingFileClicked);

    m_suffixPreffixMenu = SuffixPreffixMenu();

    m_additionalFileNameMenu = new QToolButton;
    m_additionalFileNameMenu->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("document-properties.svg")));
    m_additionalFileNameMenu->setToolTip(QString("File Name Parameters"));
    connect(m_additionalFileNameMenu, &QToolButton::clicked, this, &GreenBoardControlUI::SlotOpenFileSettingsMenu);
    topLayout->addWidget(m_pathChangedAutoSaving);
    topLayout->addWidget(m_refreshFilePath);
    topLayout->addWidget(m_openFile);
    topLayout->addWidget(m_openFolder);
    topLayout->addWidget(m_savingFile);

    m_prefixCheckBox = new QCheckBox(QString("Prefix"));
    m_prefixCheckBox->setToolTip(QString("Enable/Desable File Prefix"));
    m_suffixCheckBox = new QCheckBox(QString("Suffix"));
    m_suffixCheckBox->setToolTip(QString("Enable/Desable File Suffix"));
    middleLayout->addWidget(new QLabel(QString("Name:")));
    middleLayout->addWidget(m_fileName);
    middleLayout->addWidget(m_prefixCheckBox);
    middleLayout->addWidget(m_suffixCheckBox);
    m_prefixCheckBox->setChecked(true);
    m_suffixCheckBox->setChecked(true);
    middleLayout->addWidget(m_additionalFileNameMenu);

    QHBoxLayout *midBotomLayout = new QHBoxLayout;

    m_fileSizeComboBox = new QComboBox;
    QStringList fileSizeStringList;
    fileSizeStringList.append(QString("128 MB"));
    fileSizeStringList.append(QString("256 MB"));
    fileSizeStringList.append(QString("512 MB"));
    fileSizeStringList.append(QString("1024 MB"));
    fileSizeStringList.append(QString("2048 MB"));
    fileSizeStringList.append(QString("4096 MB"));
    fileSizeStringList.append(QString("8192 MB"));
    fileSizeStringList.append(QString("16384 MB"));
    fileSizeStringList.append(QString("32768 MB"));
    m_fileSizeComboBox->addItems(fileSizeStringList);
    m_fileSizeComboBox->setCurrentIndex(0);
    m_fileSizeComboBox->setToolTip(QString("Radar Data File Maximum Size"));

    m_savingFirstDatagram = new QComboBox;
    QStringList ampSignalBeginStringList;
    ampSignalBeginStringList.append(QString("1 Dtg"));
    ampSignalBeginStringList.append(QString("2 Dtg"));
    ampSignalBeginStringList.append(QString("3 Dtg"));
    ampSignalBeginStringList.append(QString("4 Dtg"));
    ampSignalBeginStringList.append(QString("5 Dtg"));
    ampSignalBeginStringList.append(QString("6 Dtg"));
    ampSignalBeginStringList.append(QString("7 Dtg"));
    ampSignalBeginStringList.append(QString("8 Dtg"));
    m_savingFirstDatagram->addItems(ampSignalBeginStringList);
    m_savingFirstDatagram->setToolTip(QString("Radar Data Recording Begin"));

    m_savingLastDatagram = new QComboBox;
    QStringList ampSignalEndStringList;
    ampSignalEndStringList.append(QString("1 Dtg"));
    ampSignalEndStringList.append(QString("2 Dtg"));
    ampSignalEndStringList.append(QString("3 Dtg"));
    ampSignalEndStringList.append(QString("4 Dtg"));
    ampSignalEndStringList.append(QString("5 Dtg"));
    ampSignalEndStringList.append(QString("6 Dtg"));
    ampSignalEndStringList.append(QString("7 Dtg"));
    ampSignalEndStringList.append(QString("8 Dtg"));
    m_savingLastDatagram->addItems(ampSignalEndStringList);
    m_savingLastDatagram->setCurrentIndex(7);
    m_savingLastDatagram->setToolTip(QString("Radar Data Recording End"));

    midBotomLayout->addWidget(new QLabel(QString("File Size")));
    midBotomLayout->addWidget(m_fileSizeComboBox);
    midBotomLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    midBotomLayout->addWidget(new QLabel(QString("First")));
    midBotomLayout->addWidget(m_savingFirstDatagram);
    midBotomLayout->addWidget(new QLabel(QString("Last")));
    midBotomLayout->addWidget(m_savingLastDatagram);


    QHBoxLayout *bottomLayout = new QHBoxLayout;

    m_fileSavingLedIndicator = new LedIndicator;
    m_fileSavingLedIndicator->setState(LedState::LedOff);
    bottomLayout->addWidget(m_fileSavingLedIndicator);

    bottomLayout->addWidget(new QLabel("Saving"));

    QFrame *line = new QFrame;
    line->setFrameShape(QFrame::VLine);
    line->setFrameShadow(QFrame::Sunken);
    bottomLayout->addWidget(line);

    bottomLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    QLabel *bytesWrittenLabel = new QLabel;
    bytesWrittenLabel->setText(QString("Written: "));
    bottomLayout->addWidget(bytesWrittenLabel);

    m_bytesWritten = new QDoubleSpinBox;
    m_bytesWritten->setReadOnly(true);
    m_bytesWritten->setMinimum(0);
    m_bytesWritten->setMaximum(10000.0);
    m_bytesWritten->setSuffix(QString(" B"));
    m_bytesWritten->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_bytesWritten->setAlignment(Qt::AlignRight);
    m_bytesWritten->setToolTip(QString("Current Data File Size"));
    bottomLayout->addWidget(m_bytesWritten);

    QLabel *freeDiskSpaceLabel = new QLabel;
    freeDiskSpaceLabel->setText(QString("Free: "));
    bottomLayout->addWidget(freeDiskSpaceLabel);

    m_freeDiskSpace = new QDoubleSpinBox;
    m_freeDiskSpace->setReadOnly(true);
    m_freeDiskSpace->setMinimum(-1.0);
    m_freeDiskSpace->setMaximum(10000.0);
    m_freeDiskSpace->setSuffix(QString(" B"));
    m_freeDiskSpace->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_freeDiskSpace->setAlignment(Qt::AlignRight);
    m_freeDiskSpace->setToolTip(QString("Space on Active Hard Drive Avalible"));
    bottomLayout->addWidget(m_freeDiskSpace);

    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(middleLayout);
    mainLayout->addLayout(midBotomLayout);
    mainLayout->addLayout(bottomLayout);

    fileSavingWidget->setLayout(mainLayout);

    if (m_defaultFilePathStringList->isEmpty()) {
        m_filePath->setText(g_tmpPath);
    } else {
        setNewFileDirectory();
    }

    m_diskListUpdaterTimer = new QTimer;
    m_diskListUpdaterTimer->setInterval(1000);
    connect(m_diskListUpdaterTimer, &QTimer::timeout, this, &GreenBoardControlUI::SlotUpdateDiskList);
    if (m_defaultFilePathStringList->size() > 1) {
        m_diskListUpdaterTimer->start();
    }

    return fileSavingWidget;
}

bool GreenBoardControlUI::setNewFileDirectory()
{
    bool directoryIsSet = false;

    if (m_savingState && m_savingFile->isEnabled()) {
        m_savingFile->click();
    }

    QString filePath = QString("");
    QDir dir;

    QString refreshFilePathToolTip = QString("Refresh Path From The List");

    int firstWarningIndex = -1;
    int directoryIndex = -1;
    int listSize = 0;
    for (int index = 0; index < m_defaultFilePathStringList->size(); index++) {
        filePath = m_defaultFilePathStringList->at(index);
        if (filePath != QString("") && dir.exists(filePath)) {
            listSize++;
            setFileDirectory(filePath);
            QString savingDirectory = QString::number(listSize) + QString(". [Free: ") +
                    QString::number(m_fileInfo->bytesFree()/(1024*1024*1024)) + QString("/") +
                    QString::number(m_fileInfo->bytesTotal()/(1024*1024*1024)) + QString(" GB") +
                    QString("]: ") + filePath;
            refreshFilePathToolTip.append(QString("\n") + savingDirectory);
            emit SignalLogEvent(QString("GreenBoard: Avalible Saving Directory: " + savingDirectory));
            if (!m_freeDiskSpaceWarningState && (directoryIndex == -1)) {
                directoryIndex = index;
                directoryIsSet = true;
                firstWarningIndex = -1;
            } else if (!m_freeDiskSpaceStopSavingState && firstWarningIndex == -1) {
                firstWarningIndex = index;
            }
            m_fileInfo->setPath(filePath);
        }
    }

    m_refreshFilePath->setToolTip(refreshFilePathToolTip);

    if (listSize > 1) {
        m_pathChangedAutoSaving->setVisible(true);
        m_refreshFilePath->setVisible(true);
    } else {
        m_pathChangedAutoSaving->setVisible(false);
        m_refreshFilePath->setVisible(false);
    }

    if (directoryIsSet) {
        filePath = m_defaultFilePathStringList->at(directoryIndex);
        setFileDirectory(filePath);
    } else if (firstWarningIndex > -1) {
        filePath = m_defaultFilePathStringList->at(firstWarningIndex);
        setFileDirectory(filePath);
        directoryIsSet = true;
    }
    if (directoryIsSet) {
        m_filePath->setText(filePath);
    } else {
        filePath = filePath == QString("") ? g_tmpPath : filePath;
        m_filePath->setText(filePath);
    }

    emit SignalLogEvent(QString("GreenBoard: Current Saving Directory: " + filePath));
    emit SignalCurrentDataFolder(filePath);

    return directoryIsSet;
}

void GreenBoardControlUI::SlotUpdateDiskList()
{
    QString filePath = QString("");
    QDir dir;

    QString refreshFilePathToolTip = QString("Refresh Path From The List");

    int datagrams =  static_cast<int>(m_savingFirstDatagram->currentIndex());
    datagrams = datagrams == 0 ? 0 : -1;
    datagrams = static_cast<int>(m_savingLastDatagram->currentIndex()) - datagrams;
    double triggerFreq = static_cast<double>(m_extTriggerFrequencyComboBox->currentIndex() + 1)*1000.0;
    double dataSpeed = triggerFreq*(32.0 + 1024.0 + 1024.0*static_cast<double>(datagrams));

    int listSize = 0;
    for (int index = 0; index < m_defaultFilePathStringList->size(); index++) {
        filePath = m_defaultFilePathStringList->at(index);
        if (filePath != QString("") && dir.exists(filePath)) {
            listSize++;
            m_fileInfo->setPath(filePath);
            double timeAvaliable = (m_fileInfo->bytesFree()*(1.0 - m_iniSettings["FreeDiskSpaceStopSaving"].toDouble()/100.0))/dataSpeed;
            QString timeAvaliableString = QString::number(static_cast<int>(timeAvaliable/(60*60*24))) + QString("d");
            timeAvaliable = timeAvaliable - static_cast<int>(timeAvaliable/(60*60*24))*60*60*24;
            timeAvaliableString += QString::number(static_cast<int>(timeAvaliable/(60*60))) + QString("h");
            timeAvaliable = timeAvaliable - static_cast<int>(timeAvaliable/(60*60))*60*60;
            timeAvaliableString += QString::number(static_cast<int>(timeAvaliable/(60))) + QString("m");
            timeAvaliable = timeAvaliable - static_cast<int>(timeAvaliable/(60))*60;
            timeAvaliableString += QString::number(static_cast<int>(timeAvaliable)) + QString("s");

            QString scheduleTimeAvaliableString;
            if (m_scheduleTimeCoefficient > 0) {
                double scheduleTimeAvalibale = (m_fileInfo->bytesFree()*(1.0 - m_iniSettings["FreeDiskSpaceStopSaving"].toDouble()/100.0))/m_scheduleTimeCoefficient;
                scheduleTimeAvaliableString = QString("/");
                scheduleTimeAvaliableString += QString::number(static_cast<int>(scheduleTimeAvalibale/(60*60*24))) + QString("d");
                scheduleTimeAvalibale = scheduleTimeAvalibale - static_cast<int>(scheduleTimeAvalibale/(60*60*24))*60*60*24;
                scheduleTimeAvaliableString += QString::number(static_cast<int>(scheduleTimeAvalibale/(60*60))) + QString("h");
                scheduleTimeAvalibale = scheduleTimeAvalibale - static_cast<int>(scheduleTimeAvalibale/(60*60))*60*60;
                scheduleTimeAvaliableString += QString::number(static_cast<int>(scheduleTimeAvalibale/(60))) + QString("m");
                scheduleTimeAvalibale = scheduleTimeAvalibale - static_cast<int>(scheduleTimeAvalibale/(60))*60;
                scheduleTimeAvaliableString += QString::number(static_cast<int>(scheduleTimeAvalibale)) + QString("s");
            }

            QString savingDirectory = QString::number(listSize) + QString(". [Free: ") +
                    QString::number(m_fileInfo->bytesFree()/(1024*1024*1024)) + QString("/") +
                    QString::number(m_fileInfo->bytesTotal()/(1024*1024*1024)) + QString(" GB") +
                    QString("]: ") + filePath + QString(" (") + timeAvaliableString +
                    scheduleTimeAvaliableString + QString(")");
            refreshFilePathToolTip.append(QString("\n") + savingDirectory);
            if (m_filePath->text() == filePath) {
                m_filePath->setToolTip(QString("Data Acquisition Folder") + QString("\n") + QString("[Free: ") +
                                       QString::number(m_fileInfo->bytesFree()/(1024*1024*1024)) + QString("/") +
                                       QString::number(m_fileInfo->bytesTotal()/(1024*1024*1024)) + QString(" GB") +
                                       QString("] ") + filePath + QString(" (") + timeAvaliableString +
                                       scheduleTimeAvaliableString + QString(")"));
            }
        }
    }


    m_refreshFilePath->setToolTip(refreshFilePathToolTip);
    emit SignalCurrentDataFolder(m_filePath->text());
}

void GreenBoardControlUI::setFileDirectory(const QString &filePath)
{
    m_fileInfo->setPath(filePath);
    m_freeDiskSpaceWarning = (static_cast<double>(m_iniSettings["FreeDiskSpaceWarning"].toInt())/100.0)*static_cast<double>(m_fileInfo->bytesTotal());
    m_freeDiskSpaceStopSaving = (static_cast<double>(m_iniSettings["FreeDiskSpaceStopSaving"].toInt())/100.0)*static_cast<double>(m_fileInfo->bytesTotal());
    m_freeDiskSpaceWarningState = (static_cast<double>(m_fileInfo->bytesFree()) <= m_freeDiskSpaceWarning) ? true : false;
    m_freeDiskSpaceStopSavingState = (static_cast<double>(m_fileInfo->bytesFree()) <= m_freeDiskSpaceStopSaving) ? true : false;
}

void GreenBoardControlUI::setFreeDiskSpaceStopSavingState()
{
    bool newDirectoryIsSet = false;
    if (m_savingFile->isEnabled() == m_freeDiskSpaceStopSavingState) {
        m_savingFile->setEnabled(!m_freeDiskSpaceStopSavingState);
        if (m_freeDiskSpaceStopSavingState) {
            bool savingState = m_savingState;
            if (m_savingState) {
                    m_savingFile->setChecked(false);
                    SavingFileClicked();
                }
            m_savingFile->setToolTip(QString("Out of hard drive space"));
            emit SignalLogEvent(QString("GreenBoard: Out of hard drive space"));
            emit SignalLogEvent(QString("GreenBoard: Saving Has Been Stopped"));
            newDirectoryIsSet = setNewFileDirectory();
            if (newDirectoryIsSet && savingState && m_pathChangedAutoSaving->isChecked()) {
                m_pathChangedAutoSaving->setToolTip(QString("Continue Saving After Path Automatically Changed: Enabled"));
                m_savingFile->setChecked(true);
                SavingFileClicked();
            } else if (!m_pathChangedAutoSaving->isChecked()) {
                m_pathChangedAutoSaving->setToolTip(QString("Continue Saving After Path Automatically Changed: Disabled"));
            }
        } else {
            m_savingFile->setToolTip(QString("Enable/Disable Data Acqusition"));
        }
    }
}

QWidget *GreenBoardControlUI::SuffixPreffixMenu()
{
    QWidget *suffixPreffixMenu = new QWidget (this);
    suffixPreffixMenu->setWindowFlags(Qt::Window);

    suffixPreffixMenu->setWindowTitle(QString("File Name Parameters"));
    setWindowFlag(Qt::Window);
    QGroupBox *prefixGroupBox = new QGroupBox(QString("Prefix Parameters"));
    QGroupBox *suffixGroupBox = new QGroupBox(QString("Suffix Parameters"));
    QVBoxLayout *prefixLayout = new QVBoxLayout;
    QVBoxLayout *suffixLayout = new QVBoxLayout;
    QHBoxLayout *fileNameParametersLayout = new QHBoxLayout;

    m_fileDateTimePrefixCheckBox = new QCheckBox(QString("Date And Time"));
    m_fileSamplingFrequencyFrefixCheckBox = new QCheckBox(QString("Sampling Frequency"));
    m_fileTriggeringFrequencyFrefixCheckBox = new QCheckBox(QString("Trigger Frequency"));
    m_fileChannelLengthFrefixCheckBox = new QCheckBox(QString("Channel Length"));
    m_fileAngleOffsetFrefixCheckBox = new QCheckBox(QString("Angle Offset"));
    m_fileTuningFrefixCheckBox = new QCheckBox(QString("Tuning"));
    prefixLayout->addWidget(m_fileDateTimePrefixCheckBox);
    prefixLayout->addWidget(m_fileSamplingFrequencyFrefixCheckBox);
    prefixLayout->addWidget(m_fileTriggeringFrequencyFrefixCheckBox);
    prefixLayout->addWidget(m_fileChannelLengthFrefixCheckBox);
    prefixLayout->addWidget(m_fileAngleOffsetFrefixCheckBox);
    prefixLayout->addWidget(m_fileTuningFrefixCheckBox);
    m_fileDateTimePrefixCheckBox->setChecked(true);

    m_fileDateTimeSuffixCheckBox = new QCheckBox(QString("Date And Time"));
    m_fileSamplingFrequencySuffixCheckBox = new QCheckBox(QString("Sampling Frequency"));
    m_fileTriggeringFrequencySuffixCheckBox = new QCheckBox(QString("Trigger Frequency"));
    m_fileChannelLengthSuffixCheckBox = new QCheckBox(QString("Channel Length"));
    m_fileAngleOffsetSuffixCheckBox = new QCheckBox(QString("Angle Offset"));
    m_fileTuningSuffixCheckBox = new QCheckBox(QString("Tuning"));
    suffixLayout->addWidget(m_fileDateTimeSuffixCheckBox);
    suffixLayout->addWidget(m_fileSamplingFrequencySuffixCheckBox);
    suffixLayout->addWidget(m_fileTriggeringFrequencySuffixCheckBox);
    suffixLayout->addWidget(m_fileChannelLengthSuffixCheckBox);
    suffixLayout->addWidget(m_fileAngleOffsetSuffixCheckBox);
    suffixLayout->addWidget(m_fileTuningSuffixCheckBox);
    m_fileSamplingFrequencySuffixCheckBox->setChecked(true);
    m_fileTriggeringFrequencySuffixCheckBox->setChecked(true);
    m_fileChannelLengthSuffixCheckBox->setChecked(true);
    m_fileAngleOffsetSuffixCheckBox->setChecked(true);
    m_fileTuningSuffixCheckBox->setChecked(true);

    m_fileNamePrefix = new QLineEdit;
    m_fileNamePrefix->setReadOnly(true);
    prefixLayout->addWidget(m_fileNamePrefix);
    m_fileNameSuffix = new QLineEdit;
    m_fileNameSuffix->setReadOnly(true);
    suffixLayout->addWidget(m_fileNameSuffix);

    prefixGroupBox->setLayout(prefixLayout);
    suffixGroupBox->setLayout(suffixLayout);
    fileNameParametersLayout->addWidget(prefixGroupBox);
    fileNameParametersLayout->addWidget(suffixGroupBox);
    suffixPreffixMenu->setLayout(fileNameParametersLayout);

    m_fileInfoUpdateTimer = new QTimer;
    connect(m_fileInfoUpdateTimer, &QTimer::timeout, this, &GreenBoardControlUI::SlotUpdateFileInfo);
    m_fileInfoUpdateTimer->start(1000);
    return suffixPreffixMenu;
}

QWidget *GreenBoardControlUI::SpiInterfaceTestWidget()
{
    QGroupBox *mainWidget = new QGroupBox(QString("SPI Control Test Interface"));

    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(new QLabel(QString("Byte 0")), 0, 0);
    m_spiByte0 = new QSpinBox;
    m_spiByte0->setMinimum(0);
    m_spiByte0->setMaximum(255);
    m_spiByte0->setValue(0);
    m_spiByte0->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_spiByte0, 1, 0);

    mainLayout->addWidget(new QLabel(QString("Byte 1")), 0, 1);
    m_spiByte1 = new QSpinBox;
    m_spiByte1->setMinimum(0);
    m_spiByte1->setMaximum(255);
    m_spiByte1->setValue(0);
    m_spiByte1->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_spiByte1, 1, 1);

    mainLayout->addWidget(new QLabel(QString("Byte 2")), 0, 2);
    m_spiByte2 = new QSpinBox;
    m_spiByte2->setMinimum(0);
    m_spiByte2->setMaximum(255);
    m_spiByte2->setValue(0);
    m_spiByte2->setButtonSymbols(QSpinBox::NoButtons);
    mainLayout->addWidget(m_spiByte2, 1, 2);

    mainLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 3);
    m_sendSpiMessage = new QPushButton(QString("Send"));
    connect(m_sendSpiMessage, &QPushButton::clicked, this, &GreenBoardControlUI::SlotSendSpiMessageClicked);
    mainLayout->addWidget(m_sendSpiMessage, 1, 4);

    mainWidget->setLayout(mainLayout);

    return mainWidget;
}

QWidget *GreenBoardControlUI::MotorControlWidget()
{
    QGroupBox *motorControlWidget = new QGroupBox(QString("Motor Control"));

    m_motorRightRotation = new QToolButton;
    m_motorRightRotation->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("object-rotate-right.svg")));
    m_motorRightRotation->setCheckable(true);
    m_motorRightRotation->setToolTip(QString("Сlockwise Direction"));

    m_motorLeftRotation = new QToolButton;
    m_motorLeftRotation->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("object-rotate-left.svg")));
    m_motorLeftRotation->setCheckable(true);
    m_motorLeftRotation->setToolTip(QString("Сounterclockwise Direction"));

    m_motorHoldPosition = new QToolButton;
    m_motorHoldPosition->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("system-lock-screen.svg")));
    m_motorHoldPosition->setCheckable(true);
    m_motorHoldPosition->setToolTip(QString("Forsed Hold Antena Position"));

    m_rotateMotor = new QToolButton;
    m_rotateMotor->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_rotateMotor->setCheckable(true);
    m_rotateMotor->setToolTip(QString("Rotate Antenna"));

    m_sweepAntenna = new QToolButton;
    m_sweepAntenna->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-undo-redo.svg")));
    m_sweepAntenna->setCheckable(true);
    m_sweepAntenna->setToolTip(QString("Sweep Antenna Between Angles"));

    m_antennaPositionAngle1 = new QDoubleSpinBox;
    m_antennaPositionAngle1->setMinimum(-360.0);
    m_antennaPositionAngle1->setMaximum(360.0);
    m_antennaPositionAngle1->setSingleStep(1);
    m_antennaPositionAngle1->setDecimals(2);
    m_antennaPositionAngle1->setSuffix(QString("°"));
    m_antennaPositionAngle1->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_antennaPositionAngle1->setToolTip(QString("Antenna Position Angle 1"));

    m_antennaPositionAngle2 = new QDoubleSpinBox;
    m_antennaPositionAngle2->setMinimum(-360.0);
    m_antennaPositionAngle2->setMaximum(360.0);
    m_antennaPositionAngle2->setSingleStep(1);
    m_antennaPositionAngle2->setDecimals(2);
    m_antennaPositionAngle2->setSuffix(QString("°"));
    m_antennaPositionAngle2->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_antennaPositionAngle2->setToolTip(QString("Antenna Position Angle 2"));

    m_motorPointing = new QToolButton;
    m_motorPointing->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-undo-mirrored.svg")));
    m_motorPointing->setCheckable(true);
    m_motorPointing->setToolTip(QString("Point Antena to Angle"));

    m_motorAnglesMooving = new QToolButton;
    m_motorAnglesMooving->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("emblem-synchronizing.svg")));
    m_motorAnglesMooving->setCheckable(true);
    m_motorAnglesMooving->setToolTip(QString("Rotate Antena Between Angles"));

    m_motorStopEverithing = new QToolButton;
    m_motorStopEverithing->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-clear.svg")));
    m_motorStopEverithing->setToolTip(QString("Cancel All Tasks"));

    m_motorRotationSpeed1 = new QSpinBox;
    m_motorRotationSpeed1->setMinimum(-32768);
    m_motorRotationSpeed1->setMaximum(32767);
    m_motorRotationSpeed1->setValue(1000);
    m_motorRotationSpeed1->setToolTip(QString("Rotation Speed 1"));

    m_motorRotationSpeed2 = new QSpinBox;
    m_motorRotationSpeed2->setMinimum(-32768);
    m_motorRotationSpeed2->setMaximum(32767);
    m_motorRotationSpeed2->setValue(1000);
    m_motorRotationSpeed2->setToolTip(QString("Rotation Speed 2"));

    m_setMotorRotationSpeed = new QToolButton;
    m_setMotorRotationSpeed->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("mail-forward.svg")));
    m_setMotorRotationSpeed->setToolTip(QString("Set Parameters"));
    m_motorMoovingIndicator = new LedIndicator;

    m_startMotorControlInterface = new QToolButton;
    m_startMotorControlInterface->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("connect.svg")));
    m_startMotorControlInterface->setToolTip(QString("Start SPI Motor Communication"));

    m_stopMotorControlInterface = new QToolButton;
    m_stopMotorControlInterface->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("disconnect.svg")));
    m_stopMotorControlInterface->setToolTip(QString("Stop SPI Motor Communication"));

    connect(m_motorRightRotation, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotMotorRightRotationClicked);
    connect(m_motorLeftRotation, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotMotorLeftRotationClicked);
    connect(m_motorHoldPosition, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotMotorHoldPositionClicked);
    connect(m_motorPointing, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotMotorPointingClicked);
    connect(m_motorAnglesMooving, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotMotorAnglesMoovingClicked);
    connect(m_motorStopEverithing, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotMotorStopEverithingClicked);
    connect(m_setMotorRotationSpeed, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotMotorSpeedClicked);
    connect(m_rotateMotor, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotRotateMotorClicked);
    connect(m_sweepAntenna, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotSweepMotorClicked);
    connect(m_startMotorControlInterface, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotStartMotorControlInterface);
    connect(m_stopMotorControlInterface, &QPushButton::clicked, this, &GreenBoardControlUI::UISlotStopMotorControlInterface);


    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *topLayout = new QHBoxLayout;
    QHBoxLayout *bottomLayout = new QHBoxLayout;
    topLayout->addWidget(m_motorLeftRotation);
    topLayout->addWidget(m_motorRightRotation);
    topLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    topLayout->addWidget(m_rotateMotor);
    topLayout->addWidget(m_motorHoldPosition);
    topLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    topLayout->addWidget(m_motorPointing);
    topLayout->addWidget(m_motorAnglesMooving);
    topLayout->addWidget(m_sweepAntenna);
    topLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    topLayout->addWidget(new QLabel(QString(tr("A1:"))));
    topLayout->addWidget(m_antennaPositionAngle1);
    topLayout->addWidget(new QLabel(QString(tr("A2:"))));
    topLayout->addWidget(m_antennaPositionAngle2);
    bottomLayout->addWidget(m_motorMoovingIndicator);
    bottomLayout->addWidget(new QLabel(QString("Mooving")));
    bottomLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    bottomLayout->addWidget(m_motorStopEverithing);
    bottomLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    bottomLayout->addWidget(m_startMotorControlInterface);
    bottomLayout->addWidget(m_stopMotorControlInterface);
    bottomLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    bottomLayout->addWidget(m_setMotorRotationSpeed);
    bottomLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    bottomLayout->addWidget(new QLabel(QString(tr("S1:"))));
    bottomLayout->addWidget(m_motorRotationSpeed1);
    bottomLayout->addWidget(new QLabel(QString(tr("S2:"))));
    bottomLayout->addWidget(m_motorRotationSpeed2);


    m_motorRotatingindicatorTimer = new QTimer;
    connect(m_motorRotatingindicatorTimer, &QTimer::timeout, this, &GreenBoardControlUI::SlotMotorRotatingLedIndicatorUpdate);
    m_motorRotatingindicatorTimer->start(100);

    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(bottomLayout);

    motorControlWidget->setLayout(mainLayout);

    m_motorRightRotation->setChecked(true);

    return motorControlWidget;
}

void GreenBoardControlUI::UpdateControlSettings(const GreenBoardControl &controlSetings)
{
    m_controlSetings = controlSetings;
    UpdateControlSettings();
}

void GreenBoardControlUI::UpdateControlSettings()
{
    m_rxEnable->setCurrentIndex(m_controlSetings.getRxEnableIndex());
    m_adcClockSource->setCurrentIndex(m_controlSetings.getClockSignalSourceIndex());
    m_debugMode->setCurrentIndex(m_controlSetings.getOperationModeIndex());
    if (m_decimation->currentIndex() != m_controlSetings.getDecimationIndex()) {
        emit SignalSamplingFrequency(static_cast<int>(8.0e7/pow(2, static_cast<int>(m_controlSetings.m_decimation))));
    }
    m_decimation->setCurrentIndex(m_controlSetings.getDecimationIndex());
    m_startFrequency->setCurrentIndex(m_controlSetings.getFrequencyScaleIndex());
    m_startSignalSource->setCurrentIndex(m_controlSetings.getStartSignalSourceIndex());
}

void GreenBoardControlUI::UpdateAdcDelaySettings(const GreenBoardAdcDelay &adcDelaySettings)
{
    switch (adcDelaySettings.m_adcNumber) {
    case 0: {
        m_adcDelaySettingsA = adcDelaySettings;
        UpdateAdcDelaySettings();
        break;
    }
    case 1: {
        m_adcDelaySettingsB = adcDelaySettings;
        UpdateAdcDelaySettings();
        break;
    }
    default: {
        break;
    }
    }
}

void GreenBoardControlUI::UpdateAdcDelaySettings()
{
    m_adcDelayA->setValue(m_adcDelaySettingsA.m_delayValue);
    m_adcDelayB->setValue(m_adcDelaySettingsB.m_delayValue);
}

void GreenBoardControlUI::SetMotorControlMessage()
{
    m_motorControl.setAngle1(m_antennaPositionAngle1->value());
    m_motorControl.setAngle2(m_antennaPositionAngle2->value());
    m_motorControl.setVelocity1(m_motorRotationSpeed1->value());
    m_motorControl.setVelocity1(m_motorRotationSpeed2->value());
    MotorDirection motorDirection;
    if (m_motorRightRotation->isChecked()) {
        motorDirection = MotorDirection::ClockWise;
    } else {
        motorDirection = MotorDirection::CounterClockWise;
    }
    m_motorControl.setDirection(motorDirection);
    //    SlotSendSpiMessageList(m_motorControl.getMessageList());
    m_greenBoardNetworkCommunicator->slotSetMotorControlStatement(m_motorControl);
}

void GreenBoardControlUI::UISlotMotorRightRotationClicked()
{
    m_motorLeftRotation->setChecked(false);
    m_motorRightRotation->setChecked(true);
}

void GreenBoardControlUI::UISlotMotorLeftRotationClicked()
{
    m_motorRightRotation->setChecked(false);
    m_motorLeftRotation->setChecked(true);
}

void GreenBoardControlUI::UISlotRotateMotorClicked()
{
    m_motorHoldPosition->setChecked(false);
    m_motorPointing->setChecked(false);
    m_motorAnglesMooving->setChecked(false);
    m_sweepAntenna->setChecked(false);

    m_motorRightRotation->setEnabled(!m_rotateMotor->isChecked());
    m_motorLeftRotation->setEnabled(!m_rotateMotor->isChecked());
    m_sweepAntenna->setEnabled(!m_rotateMotor->isChecked());
    m_motorHoldPosition->setEnabled(!m_rotateMotor->isChecked());
    m_motorAnglesMooving->setEnabled(!m_rotateMotor->isChecked());
    m_motorPointing->setEnabled(!m_rotateMotor->isChecked());

    m_motorControl.setMod(MotorModBytes::ContionusRotation);
    SetMotorControlMessage();
    if (!m_rotateMotor->isChecked()) {
        UISlotMotorStopEverithingClicked();
    }
}

void GreenBoardControlUI::UISlotMotorHoldPositionClicked()
{
    m_rotateMotor->setChecked(false);
    m_sweepAntenna->setChecked(false);
    m_motorPointing->setChecked(false);
    m_motorAnglesMooving->setChecked(false);

    m_motorRightRotation->setEnabled(!m_motorHoldPosition->isChecked());
    m_motorLeftRotation->setEnabled(!m_motorHoldPosition->isChecked());
    m_sweepAntenna->setEnabled(!m_motorHoldPosition->isChecked());
    m_motorPointing->setEnabled(!m_motorHoldPosition->isChecked());
    m_motorAnglesMooving->setEnabled(!m_motorHoldPosition->isChecked());
    m_motorPointing->setEnabled(!m_motorHoldPosition->isChecked());

    m_motorControl.setMod(MotorModBytes::Break);
    SetMotorControlMessage();
    if (!m_motorHoldPosition->isChecked()) {
        UISlotMotorStopEverithingClicked();
    }
}

void GreenBoardControlUI::UISlotMotorPointingClicked()
{
    m_rotateMotor->setChecked(false);
    m_sweepAntenna->setChecked(false);
    m_motorHoldPosition->setChecked(false);
    m_motorAnglesMooving->setChecked(false);

    m_motorRightRotation->setEnabled(!m_motorPointing->isChecked());
    m_motorLeftRotation->setEnabled(!m_motorPointing->isChecked());
    m_rotateMotor->setEnabled(!m_motorPointing->isChecked());
    m_sweepAntenna->setEnabled(!m_motorPointing->isChecked());
    m_motorHoldPosition->setEnabled(!m_motorPointing->isChecked());
    m_motorAnglesMooving->setEnabled(!m_motorPointing->isChecked());
    m_antennaPositionAngle1->setEnabled(!m_motorPointing->isChecked());
    m_antennaPositionAngle2->setEnabled(!m_motorPointing->isChecked());

    m_motorControl.setMod(MotorModBytes::RotateToAngle);
    SetMotorControlMessage();
    if (!m_motorPointing->isChecked()) {
        UISlotMotorStopEverithingClicked();
    }
}

void GreenBoardControlUI::UISlotMotorAnglesMoovingClicked()
{
    m_rotateMotor->setChecked(false);
    m_sweepAntenna->setChecked(false);
    m_motorHoldPosition->setChecked(false);
    m_motorPointing->setChecked(false);

    m_motorRightRotation->setEnabled(!m_motorAnglesMooving->isChecked());
    m_motorLeftRotation->setEnabled(!m_motorAnglesMooving->isChecked());
    m_rotateMotor->setEnabled(!m_motorAnglesMooving->isChecked());
    m_sweepAntenna->setEnabled(!m_motorAnglesMooving->isChecked());
    m_motorHoldPosition->setEnabled(!m_motorAnglesMooving->isChecked());
    m_antennaPositionAngle1->setEnabled(!m_motorAnglesMooving->isChecked());
    m_antennaPositionAngle2->setEnabled(!m_motorAnglesMooving->isChecked());
    m_motorPointing->setEnabled(!m_motorAnglesMooving->isChecked());

    m_motorControl.setMod(MotorModBytes::RotateDifferentSpeed);
    SetMotorControlMessage();
    if (!m_motorAnglesMooving->isChecked()) {
        UISlotMotorStopEverithingClicked();
    }
}

void GreenBoardControlUI::UISlotSweepMotorClicked()
{
    m_rotateMotor->setChecked(false);
    m_motorAnglesMooving->setChecked(false);
    m_motorHoldPosition->setChecked(false);
    m_motorPointing->setChecked(false);

    m_motorRightRotation->setEnabled(!m_sweepAntenna->isChecked());
    m_motorLeftRotation->setEnabled(!m_sweepAntenna->isChecked());
    m_rotateMotor->setEnabled(!m_sweepAntenna->isChecked());
    m_motorAnglesMooving->setEnabled(!m_sweepAntenna->isChecked());
    m_motorHoldPosition->setEnabled(!m_sweepAntenna->isChecked());
    m_antennaPositionAngle1->setEnabled(!m_sweepAntenna->isChecked());
    m_antennaPositionAngle2->setEnabled(!m_sweepAntenna->isChecked());
    m_motorPointing->setEnabled(!m_sweepAntenna->isChecked());

    m_motorControl.setMod(MotorModBytes::RotateBetwenAngles);
    SetMotorControlMessage();
    if (!m_sweepAntenna->isChecked()) {
        UISlotMotorStopEverithingClicked();
    }
}

void GreenBoardControlUI::UISlotMotorStopEverithingClicked()
{
    m_rotateMotor->setChecked(false);
    m_motorHoldPosition->setChecked(false);
    m_sweepAntenna->setChecked(false);
    m_motorPointing->setChecked(false);
    m_motorAnglesMooving->setChecked(false);

    m_motorRightRotation->setEnabled(true);
    m_motorLeftRotation->setEnabled(true);
    m_rotateMotor->setEnabled(true);
    m_motorHoldPosition->setEnabled(true);
    m_motorPointing->setEnabled(true);
    m_motorAnglesMooving->setEnabled(true);
    m_antennaPositionAngle1->setEnabled(true);
    m_antennaPositionAngle2->setEnabled(true);

    m_motorControl.setMod(MotorModBytes::PowerOff);
    SetMotorControlMessage();
}

void GreenBoardControlUI::UISlotMotorSpeedClicked()
{
    SetMotorControlMessage();
}

void GreenBoardControlUI::SlotMotorRotatingLedIndicatorUpdate()
{
    if (m_angleChanging) {
        m_motorMoovingIndicator->setState(LedState::LedOn);
        m_angleChanging = false;
    } else {
        m_motorMoovingIndicator->setState(LedState::LedOff);
    }
}

void GreenBoardControlUI::UISlotStartMotorControlInterface()
{
    m_greenBoardNetworkCommunicator->startMotorControlInterface();
    m_stopMotorControlInterface->setVisible(true);
    m_startMotorControlInterface->setVisible(false);
}

void GreenBoardControlUI::UISlotStopMotorControlInterface()
{
    m_greenBoardNetworkCommunicator->stopMotorControlInteface();
    m_startMotorControlInterface->setVisible(true);
    m_stopMotorControlInterface->setVisible(false);
}

void GreenBoardControlUI::slotMaxFileSizeIndexChanged(const int &index)
{
    qint64 maxFileSize = 4294967296;
    switch (index) {
    case 0 : {
        maxFileSize = 134217728;
        break;
    }
    case 1 : {
        maxFileSize = 268435456;
        break;
    }
    case 2 : {
        maxFileSize = 536870912;
        break;
    }
    case 3 : {
        maxFileSize = 1073741824;
        break;
    }
    case 4 : {
        maxFileSize = 2147483648;
        break;
    }
    case 5 : {
        maxFileSize = 4294967296;
        break;
    }
    case 6 : {
        maxFileSize = 8589934592;
        break;
    }
    case 7 : {
        maxFileSize = 17179869184;
        break;
    }
    case 8 : {
        maxFileSize = 34359738368;
        break;
    }
    case 9 : {
        maxFileSize = 68719476736;
        break;
    }
    default: {
        maxFileSize = 4294967296;
        break;
    }
    }

    m_greenBoardNetworkCommunicator->slotSetMaxFileSize(maxFileSize);
}

void GreenBoardControlUI::slotFirstSavingDatagramIndexChanged(const int &index)
{
    m_greenBoardNetworkCommunicator->slotSetFirstSavingDatagram(index);
}

void GreenBoardControlUI::slotLastSavingDatagramIndexChanged(const int &index)
{
    m_greenBoardNetworkCommunicator->slotSetLastSavingDatagram(index);
}

void GreenBoardControlUI::SlotSendSpiMessageClicked()
{
    QByteArray message;
    message.append(static_cast<quint8>(m_spiByte0->value()));
    message.append(static_cast<quint8>(m_spiByte1->value()));
    message.append(static_cast<quint8>(m_spiByte2->value()));

    SlotSendSpiMessage(message);
}

void GreenBoardControlUI::SlotSendSpiMessage(const QByteArray &spiMessage)
{
    if (spiMessage.size() == 3) {
        QByteArray message0, message1, message2;
        message0.append(static_cast<quint8>(GreenBoardAddressBytes::SPI_Byte_0));
        message0.append(spiMessage[0]);
        message1.append(static_cast<quint8>(GreenBoardAddressBytes::SPI_Byte_1));
        message1.append(spiMessage[1]);
        message2.append(static_cast<quint8>(GreenBoardAddressBytes::SPI_Byte_2));
        message2.append(spiMessage[2]);

        QList<QByteArray> messageList;
        messageList.append(message0);
        messageList.append(message1);
        messageList.append(message2);
        for (int i = 0; i < 3; i++) {
            m_greenBoardNetworkCommunicator->sendControlMessage(messageList);
        }
    }
}

void GreenBoardControlUI::SlotSendSpiMessageList(const QList<QByteArray> &spiMessageList)
{
    for (int i = 0; i < spiMessageList.size(); i++) {
        if (spiMessageList.at(i).size() == 3) {
            SlotSendSpiMessage(spiMessageList[i]);
        }
    }
}

void GreenBoardControlUI::SlotRestartNetworkClicked()
{
    m_greenBoardNetworkCommunicator->stopWorkingThread();
    m_greenBoardNetworkCommunicator->startWorkingThread();
    InitADCClicked();
    SlotStartRxClicked();
}

void GreenBoardControlUI::LedCheckBoxClicked()
{
    bool ledState = static_cast<bool>(m_ledCheckBox->checkState());
    GreenBoardLED led(!ledState);

    m_greenBoardNetworkCommunicator->sendControlMessage(led.greenBoardLED());
}

void GreenBoardControlUI::SetControlSettingsClicked()
{
    m_controlSetings.setRxEnable(m_rxEnable->currentIndex());
    m_controlSetings.setClockSignalSource(m_adcClockSource->currentIndex());
    m_controlSetings.setOperationMode(m_debugMode->currentIndex());
    m_controlSetings.setFrequencyScale(m_startFrequency->currentIndex());
    m_controlSetings.setStartSignalSource(m_startSignalSource->currentIndex());
    if (m_controlSetings.getDecimationIndex() != m_decimation->currentIndex()) {
        m_controlSetings.setDecimation(m_decimation->currentIndex());
        emit SignalSamplingFrequency(static_cast<int>(8.0e7/pow(2, static_cast<int>(m_controlSetings.m_decimation))));
    } else {
        m_controlSetings.setDecimation(m_decimation->currentIndex());
    }

    if (m_controlSetings.m_rxEnable == RxEnable::Enabled && !m_greenBoardNetworkCommunicator->isWorking()) {
        m_greenBoardNetworkCommunicator->startWorkingThread();
    }

    m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSetings.greenBoardControl());
}

void GreenBoardControlUI::InitADCClicked()
{
    m_greenBoardNetworkCommunicator->sendControlMessage(GreenBoardAdcControlWord::FullAdcInitMessage());
    SetControlSettingsClicked();
    SlotSetControlSettings2Clicked();
    SlotSetAngleOffsetClicked();
    SetAdcDelayAClicked();
    SetAdcDelayBClicked();
}

void GreenBoardControlUI::SetAdcDelayAClicked()
{
    if (m_synchronizeDelayFlag) {
        GreenBoardAdcDelay adcDelay;
        adcDelay.setAdcNumber(2);
        adcDelay.setDelayValue(m_adcDelayA->value());

        if (m_greenBoardNetworkCommunicator->sendControlMessage(adcDelay.greenBoardAdcDelay()) >= 0) {
            m_adcDelayValueA = m_adcDelayA->value();
            m_adcDelayValueB = m_adcDelayA->value();
            m_adcDelaySettingsA.setDelayValue(m_adcDelayA->value());
            m_adcDelaySettingsB.setDelayValue(m_adcDelayB->value());
        }
    } else {
        GreenBoardAdcDelay adcDelay;
        adcDelay.setAdcNumber(0);
        adcDelay.setDelayValue(m_adcDelayA->value());

        m_greenBoardNetworkCommunicator->sendControlMessage(adcDelay.greenBoardAdcDelay());

        if (m_greenBoardNetworkCommunicator->sendControlMessage(adcDelay.greenBoardAdcDelay()) >= 0) {
            m_adcDelayValueA = m_adcDelayA->value();
            m_adcDelaySettingsA.setDelayValue(m_adcDelayA->value());
        }
    }
}

void GreenBoardControlUI::SetAdcDelayBClicked()
{
    m_adcDelaySettingsB.setAdcNumber(1);
    m_adcDelaySettingsB.setDelayValue(m_adcDelayB->value());

    if (m_greenBoardNetworkCommunicator->sendControlMessage(m_adcDelaySettingsB.greenBoardAdcDelay()) >= 0) {
        m_adcDelayValueB = m_adcDelayB->value();
    }
}

void GreenBoardControlUI::SynchronizeDelayStateChenged(const bool &checkState)
{
    m_synchronizeDelayFlag = checkState;
    if (m_synchronizeDelayFlag) {
        if (m_adcDelayValueA == m_adcDelayValueB) {
            m_adcDelayA->setValue(m_adcDelayValueA);
            m_adcDelayB->setValue(m_adcDelayValueB);
        } else {
            m_adcDelayA->setValue(0);
            m_adcDelayB->setValue(0);
        }
        m_adcDelayLabelA->setText("ADC");
        m_adcDelayLabelB->setText("ADC");
        m_adcDelayB->setEnabled(false);
        m_adcDelayLabelB->setEnabled(false);
        m_setAdcDelayB->setEnabled(false);
        connect(m_adcDelayA, &QSpinBox::textChanged, this, &GreenBoardControlUI::AdcDelayAValueChanged);
    } else {
        m_adcDelayLabelA->setText("ADC A");
        m_adcDelayLabelB->setText("ADC B");
        disconnect(m_adcDelayA, &QSpinBox::textChanged, this, &GreenBoardControlUI::AdcDelayAValueChanged);
        m_adcDelayB->setEnabled(true);
        m_adcDelayLabelB->setEnabled(true);
        m_setAdcDelayB->setEnabled(true);
        m_adcDelayA->setValue(m_adcDelayValueA);
        m_adcDelayB->setValue(m_adcDelayValueB);
    }
}

void GreenBoardControlUI::AdcDelayAValueChanged()
{
    m_adcDelayB->setValue(m_adcDelayA->value());
}

void GreenBoardControlUI::AngleValueChanged(const quint16 &value)
{
    if (!m_disconnected) {
        if (value == g_defaultAngleSensorValue) {
            SlotLostConnection();
            m_networkError = true;
            m_receivingIsEnable = false;
        } else {
            SlotConnected();
            double val = (static_cast<double>(value)*360.0/g_maxAngleSensorValue + m_angleOffsetValue);
            val -= val > 360 ? 360 : 0;
            val += val < 0 ? 360 : 0;
            m_receivingIsEnable = true;
            m_angleSensorValue->setValue(val);
            m_angleChanging = m_prevAngleValue == value ? false : true;
            m_prevAngleValue = value;
            if (m_networkError) {
                m_networkError = false;
                if (m_triggerWasEnabledBefore) {
                    m_triggerOutputLedIndicator->setState(LedState::LedOn);
                }
            }
        }
    }
    if (m_logBoardLostConnection != m_networkError) {
        if (m_networkError) {
            emit SignalLogEvent(QString("GreenBoard: Network Error"));
        } else {
            emit SignalLogEvent(QString("GreenBoard: Reconnected"));
        }
        m_logBoardLostConnection = m_networkError;
    }
    if ((m_logBoardIsConnected == m_disconnected) && !m_networkError) {
        if (m_disconnected) {
            emit SignalLogEvent(QString("GreenBoard: Disconnected"));
        } else {
            emit SignalLogEvent(QString("GreenBoard: Connected"));
        }
        m_logBoardIsConnected = !m_disconnected;
    }
}

void GreenBoardControlUI::IncreaseErrorCounter(const quint8 &errors)
{
    m_errorCounter->setValue(m_errorCounter->value() + errors);
    m_errorCounterValue += errors;
}

void GreenBoardControlUI::ResetErrorCounter()
{
    m_errorCounter->setValue(0);
    m_errorCounterValue = 0;
}

void GreenBoardControlUI::TimeoutError(const bool &error)
{
    m_timeoutError->setVisible(error);
}

void GreenBoardControlUI::ResetErrorsClicked()
{
    m_timeoutError->setVisible(false);
    m_errorCounter->setValue(0);
    m_bytesReceived->setValue(0);
    m_totalBytesReceived = 0;
    m_terabytesReceived->setValue(0);
    m_totalTeraBytesReceived = 0;
    m_errorCoefCounter->setValue(0.0);
    m_errorCounter->setToolTip(QString("0.0 kB"));
    m_errorCounterValue = 0;
    QDateTime dateTime = QDateTime::currentDateTime();
    m_counterBeginningDate->setText(dateTime.toString(QString("dd.MM.yyyy  ")));
    m_counterBeginningTime->setText(dateTime.toString(QString("hh:mm:ss")));
}

void GreenBoardControlUI::SlotBytesReceived(const quint64 &bytesReceived)
{
    m_totalBytesReceived += bytesReceived;
    if (m_totalBytesReceived < 1024) {
        m_bytesReceived->setValue(m_totalBytesReceived);
        m_bytesReceived->setSuffix(QString(" B"));
    } if (m_totalBytesReceived > 1024 && m_totalBytesReceived < 1048576) {
        m_bytesReceived->setValue(m_totalBytesReceived/1024.0);
        m_bytesReceived->setSuffix(QString(" kB"));
    } if (m_totalBytesReceived > 1048576 && m_totalBytesReceived < 1073741824) {
        m_bytesReceived->setValue(m_totalBytesReceived/1048576.0);
        m_bytesReceived->setSuffix(QString(" MB"));
    } if (m_totalBytesReceived > 1073741824 && m_totalBytesReceived < 1048576.0*1048576.0) {
        m_bytesReceived->setValue(m_totalBytesReceived/1073741824.0);
        m_bytesReceived->setSuffix(QString(" GB"));
        m_bytesReceived->setDecimals(4);
    } if (m_totalBytesReceived > 1048576.0*1048576.0) {
        m_bytesReceived->setSuffix(QString(" B"));
        m_totalBytesReceived -= 1048576.0*1048576.0;
        m_totalTeraBytesReceived += 1;
        m_bytesReceived->setValue(m_totalBytesReceived);
        m_bytesReceived->setDecimals(2);
        m_terabytesReceived->setVisible(true);
        m_terabytesReceived->setValue(m_totalTeraBytesReceived);
    }
    double errorCoefCounterValue = m_errorCounterValue*1024;
    if (errorCoefCounterValue < 1024) {
        m_errorCoefCounter->setValue(errorCoefCounterValue);
        m_errorCoefCounter->setSuffix(QString(" B"));
        m_errorCounter->setToolTip(QString::number(errorCoefCounterValue) + QString(" B"));
    } if (errorCoefCounterValue > 1024 && errorCoefCounterValue < 1048576) {
        m_errorCoefCounter->setValue(errorCoefCounterValue/1024.0);
        m_errorCoefCounter->setSuffix(QString(" kB"));
        m_errorCounter->setToolTip(QString::number(errorCoefCounterValue/1024.0) + QString(" kB"));
    } if (errorCoefCounterValue > 1048576 && errorCoefCounterValue < 1073741824) {
        m_errorCoefCounter->setValue(errorCoefCounterValue/1048576.0);
        m_errorCoefCounter->setSuffix(QString(" MB"));
        m_errorCounter->setToolTip(QString::number(errorCoefCounterValue/1048576.0) + QString(" MB"));
    } if (errorCoefCounterValue > 1073741824 && errorCoefCounterValue < 1048576.0*1048576.0) {
        m_errorCoefCounter->setValue(errorCoefCounterValue/1073741824.0);
        m_errorCoefCounter->setSuffix(QString(" GB"));
        m_errorCounter->setToolTip(QString::number(errorCoefCounterValue/1073741824.0) + QString(" GB"));
        m_errorCoefCounter->setDecimals(4);
    } if (errorCoefCounterValue > 1048576.0*1048576.0) {
        m_errorCoefCounter->setSuffix(QString(" TB"));
        m_errorCounter->setToolTip(QString::number(errorCoefCounterValue/(1073741824.0*1024.0)) + QString(" TB"));
        m_errorCoefCounter->setDecimals(6);
        m_errorCoefCounter->setValue(errorCoefCounterValue/1048576.0*1048576.0);
    }
}

void GreenBoardControlUI::SlotBytesWritten(const quint64 &bytesWritten)
{
    m_totalBytesWritten += bytesWritten;
    if (bytesWritten < 1024) {
        m_bytesWritten->setValue(bytesWritten);
        m_bytesWritten->setSuffix(QString(" B"));
        m_bytesWritten->setDecimals(2);
    } if (bytesWritten > 1024 && bytesWritten < 1048576) {
        m_bytesWritten->setValue(bytesWritten/1024.0);
        m_bytesWritten->setSuffix(QString(" kB"));
        m_bytesWritten->setDecimals(2);
    } if (bytesWritten > 1048576 && bytesWritten < 1073741824) {
        m_bytesWritten->setValue(bytesWritten/1048576.0);
        m_bytesWritten->setSuffix(QString(" MB"));
        m_bytesWritten->setDecimals(2);
    } if (bytesWritten > 1073741824 && bytesWritten < 1048576.0*1048576.0) {
        m_bytesWritten->setValue(bytesWritten/1073741824.0);
        m_bytesWritten->setSuffix(QString(" GB"));
        m_bytesWritten->setDecimals(4);
    } if (bytesWritten > 1048576.0*1048576.0) {
        m_bytesWritten->setValue(bytesWritten/(1073741824.0*1024.0));
        m_bytesWritten->setSuffix(QString(" TB"));
        m_bytesWritten->setDecimals(8);
    }

    m_fileInfo->setPath(m_filePath->text());
    m_fileInfo->root();
    SlotFreeDiskSpace(m_fileInfo->bytesFree());
}

void GreenBoardControlUI::OpenFileClicked()
{
    QString filePath = QFileDialog::getExistingDirectory(this, QString("Data Folder"), QString(""), QFileDialog::DontUseNativeDialog);
    if (filePath != QString("")) {
        m_filePath->setText(filePath);
    }
}

void GreenBoardControlUI::SavingFileClicked()
{
    if (!m_freeDiskSpaceStopSavingState || m_savingState) {
        if ((m_savingFile->isChecked() || m_scheduleRemoteSavingIsActive) && !m_savingState) {
            QString fileName = m_filePath->text() + g_pathDevider + m_fileNamePrefix->text() + QTime::currentTime().toString(QString("hhmmss")) +  QString("_") + m_fileName->text() + m_fileNameSuffix->text();
            if (fileName == QString("")) {
                m_fileName->setStyleSheet("border: 1px red");
                m_savingFile->setChecked(false);
                m_savingState = false;
            } else {
                m_savingState = true;
                emit SignalStartDataSavingThread(fileName, SavingOption::DirectSaving);
            }
        } else {
            emit SignalStopDataSavingThread();
            m_savingState = false;
        }
    }
}

void GreenBoardControlUI::SlotDataSavingState(const bool &savingState)
{
    if (!m_scheduleState) {
        m_fileName->setEnabled(!savingState);
        m_filePath->setEnabled(!savingState);
        m_openFile->setEnabled(!savingState);
        m_prefixCheckBox->setEnabled(!savingState);
        m_suffixCheckBox->setEnabled(!savingState);
        m_extAngleOffsetSpinBox->setEnabled(!savingState);
        m_extAdcChannelAlengthComboBox->setEnabled(!savingState);
        m_extAdcDelaySpinBox->setEnabled(!savingState);
        m_extSamplingFrrequencyComboBox->setEnabled(!savingState);
        m_extTriggerFrequencyComboBox->setEnabled(!savingState);
        m_fileSizeComboBox->setEnabled(!savingState);
        m_savingFirstDatagram->setEnabled(!savingState);
        m_savingLastDatagram->setEnabled(!savingState);
        m_refreshFilePath->setEnabled(!savingState);
        m_pathChangedAutoSaving->setEnabled(!savingState);
        emit SignalDataSavingState(savingState);
    }
}

void GreenBoardControlUI::SlotScheduleState(const bool &scheduleState)
{
    m_fileName->setEnabled(!scheduleState);
    m_filePath->setEnabled(!scheduleState);
    m_openFile->setEnabled(!scheduleState);
    m_prefixCheckBox->setEnabled(!scheduleState);
    m_suffixCheckBox->setEnabled(!scheduleState);
    m_extAngleOffsetSpinBox->setEnabled(!scheduleState);
    m_extAdcChannelAlengthComboBox->setEnabled(!scheduleState);
    m_extAdcDelaySpinBox->setEnabled(!scheduleState);
    m_extSamplingFrrequencyComboBox->setEnabled(!scheduleState);
    m_extTriggerFrequencyComboBox->setEnabled(!scheduleState);
    m_fileSizeComboBox->setEnabled(!scheduleState);
    m_savingFirstDatagram->setEnabled(!scheduleState);
    m_savingLastDatagram->setEnabled(!scheduleState);
    m_savingFile->setEnabled(!scheduleState);
    m_refreshFilePath->setEnabled(!scheduleState);
    m_pathChangedAutoSaving->setEnabled(!scheduleState);
    emit SignalDataSavingState(scheduleState);
    m_scheduleState = scheduleState;
    if (!scheduleState) {
        resetIniSettings();
    }
    slotSetOnSchedule(scheduleState);
}

void GreenBoardControlUI::SlotStartRxClicked()
{
    m_controlSetings.m_rxEnable = RxEnable::Enabled;
    m_disconnected = false;
    UpdateControlSettings();
    SetControlSettingsClicked();
    if (m_networkError) {
        m_automaticReconnectTimer->start();
    }
}

void GreenBoardControlUI::SlotStopRxClicked()
{
    m_controlSetings.m_rxEnable = RxEnable::Disabled;
    m_disconnected = true;
    UpdateControlSettings();
    SetControlSettingsClicked();
    SlotDisconnection();
    m_automaticReconnectTimer->stop();
    if (m_networkError) {
        m_connectionStatusLabel->setText(QString("Network Error (Disconnected))"));
    }
}

void GreenBoardControlUI::SlotSetAngleOffsetClicked()
{
    m_angleOffsetValue = m_angleOffset->value();
    emit SignalAngleOffsetChanged(static_cast<quint16>((m_angleOffsetValue/360.0)*g_maxAngleSensorValue));
}

void GreenBoardControlUI::SlotResetAngleOffsetClicked()
{
    m_angleOffset->setValue(m_angleOffsetValue);
}

void GreenBoardControlUI::OpenSettingsClicked()
{
    if (isVisible()) {
        close();
    } else {
        show();
    }
}

void GreenBoardControlUI::SlotSetSignalAvgCoefficientClicked()
{
    m_signalAvgCoefficientValue = m_signalAvgCoefficient->value();
    emit SignalSetSignalAvgCoefficient(m_signalAvgCoefficientValue);
}

void GreenBoardControlUI::SlotSetDiagramAvgCoefficientClicked()
{
    m_diagramCoefficientValue = m_diagramAvgCoefficient->value();
    emit SignalSetDiagramAvgCoefficient(m_diagramCoefficientValue);
}

void GreenBoardControlUI::SlotResetAvgCoefficientsClicked()
{
    m_signalAvgCoefficient->setValue(m_signalAvgCoefficientValue);
    m_diagramAvgCoefficient->setValue(m_diagramCoefficientValue);
}

void GreenBoardControlUI::SlotFreeDiskSpace(const qint64 &bytes)
{
    if (bytes < 1024) {
        m_freeDiskSpace->setValue(bytes);
        m_freeDiskSpace->setSuffix(QString(" B"));
        m_freeDiskSpace->setDecimals(2);
    } if (bytes > 1024 && bytes < 1048576) {
        m_freeDiskSpace->setValue(bytes/1024.0);
        m_freeDiskSpace->setSuffix(QString(" kB"));
        m_freeDiskSpace->setDecimals(2);
    } if (bytes > 1048576 && bytes < 1073741824) {
        m_freeDiskSpace->setValue(bytes/1048576.0);
        m_freeDiskSpace->setSuffix(QString(" MB"));
        m_freeDiskSpace->setDecimals(2);
    } if (bytes > 1073741824 && bytes < 1048576.0*1048576.0) {
        m_freeDiskSpace->setValue(bytes/1073741824.0);
        m_freeDiskSpace->setSuffix(QString(" GB"));
        m_freeDiskSpace->setDecimals(4);
    } if (bytes > 1048576.0*1048576.0) {
        m_freeDiskSpace->setValue(bytes/(1073741824.0*1024.0));
        m_freeDiskSpace->setSuffix(QString(" TB"));
        m_freeDiskSpace->setDecimals(5);
    }

    m_freeDiskSpaceWarningState = (bytes <= m_freeDiskSpaceWarning);
    m_freeDiskSpaceStopSavingState = (bytes <= m_freeDiskSpaceStopSaving);
    setFreeDiskSpaceStopSavingState();
}

void GreenBoardControlUI::SlotSetControlSettings2Clicked()
{
    m_controlSettings2.setFrequencyScale(m_startFrequency2->currentIndex());
    m_controlSettings2.setTriggerOut(m_triggerEnable->currentIndex());
    m_controlSettings2.setStrobeSize(m_strobeSize->currentIndex());

    if (m_controlSettings2.m_triggerOut == TriggerOut::Disable) {
        m_triggerWasEnabledBefore = false;
    } else {
        m_triggerWasEnabledBefore = true;
        if (m_blindZoneIsActive) {
            m_triggerEnable->setCurrentIndex(1);
            m_controlSettings2.setTriggerOut(m_triggerEnable->currentIndex());
        }
    }

    m_greenBoardNetworkCommunicator->SlotSignalStartFrequencyChanged(m_controlSettings2.m_frequencyScale);

    if (m_controlSetings.m_rxEnable == RxEnable::Enabled) {
        if (!m_greenBoardNetworkCommunicator->isWorking()) {
            m_greenBoardNetworkCommunicator->startWorkingThread();
        }

        m_controlSetings.m_rxEnable = RxEnable::Disabled;
        m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSetings.greenBoardControl());

        m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSettings2.greenBoardControl2());

        m_controlSetings.m_rxEnable = RxEnable::Enabled;
        m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSetings.greenBoardControl());

    } else {

        m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSettings2.greenBoardControl2());

    }

    emit SignalAdcChannelALengthChanged(m_controlSettings2.getAdcChannelALength());
}

void GreenBoardControlUI::SlotNetworkSetClicked()
{
    m_pcHostAddressValue = m_pcHostAddress->text();
    m_pcControlPortValue = m_pcControlPort->value();
    m_pcDataPortValue = m_pcDataPort->value();
    m_boardHostAddressValue = m_boardHostAddress->text();
    m_boardControlPortValue = m_boardControlPort->value();
    m_boardDataPortValue = m_boardDataPort->value();
    if (m_greenBoardNetworkCommunicator->isWorking()) {
        m_greenBoardNetworkCommunicator->stopWorkingThread();
        m_greenBoardNetworkCommunicator->setPcHostAddress(m_pcHostAddressValue, m_pcControlPortValue, m_pcDataPortValue);
        m_greenBoardNetworkCommunicator->setBoardHostAddress(m_boardHostAddressValue, m_boardControlPortValue, m_boardDataPortValue);
        m_greenBoardNetworkCommunicator->startWorkingThread();
    } else {
        m_greenBoardNetworkCommunicator->setPcHostAddress(m_pcHostAddressValue, m_pcControlPortValue, m_pcDataPortValue);
        m_greenBoardNetworkCommunicator->setBoardHostAddress(m_boardHostAddressValue, m_boardControlPortValue, m_boardDataPortValue);
    }
}

void GreenBoardControlUI::SlotNetworkResetClicked()
{
    m_pcHostAddress->setText(m_pcHostAddressValue);
    m_pcControlPort->setValue(m_pcControlPortValue);
    m_pcDataPort->setValue(m_pcDataPortValue);
    m_boardHostAddress->setText(m_boardHostAddressValue);
    m_boardControlPort->setValue(m_boardControlPortValue);
    m_boardDataPort->setValue(m_boardDataPortValue);
}

void GreenBoardControlUI::exit()
{
    GreenBoardControl greenBoardControl;

    m_greenBoardNetworkCommunicator->sendControlMessage(greenBoardControl.greenBoardControl());
}

void GreenBoardControlUI::SlotTimeoutError(const bool &timeout)
{
    if (timeout) {
        emit SignalstatusMessage(QString("Network timeout error"));
    } else {
        emit SignalstatusMessage(QString(""));
    }
}

void GreenBoardControlUI::SlotTransmitterDisabled(const bool &transmitterDisabled)
{
    if (m_triggerWasEnabledBefore) {
        m_triggerEnable->setCurrentIndex(static_cast<quint8>(transmitterDisabled));
        m_controlSettings2.setTriggerOut(transmitterDisabled);

        if (m_controlSetings.m_rxEnable == RxEnable::Enabled) {
            if (!m_greenBoardNetworkCommunicator->isWorking()) {
                m_greenBoardNetworkCommunicator->startWorkingThread();
            }

            m_controlSetings.m_rxEnable = RxEnable::Disabled;
            m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSetings.greenBoardControl());

            m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSettings2.greenBoardControl2());

            m_controlSetings.m_rxEnable = RxEnable::Enabled;
            m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSetings.greenBoardControl());

        } else {

            m_greenBoardNetworkCommunicator->sendControlMessage(m_controlSettings2.greenBoardControl2());

        }
    }
}

void GreenBoardControlUI::SlotSetBlindZone()
{
    double firstValue = 0;
    double secondValue = 0;

    if (m_blindZoneOne->isChecked()) {

        firstValue = (static_cast<double>(m_blindZoneOneFirst->value()) - m_angleOffsetValue);
        firstValue -= firstValue > 360 ? 360 : 0;
        firstValue += firstValue < 0 ? 360 : 0;

        secondValue = (static_cast<double>(m_blindZoneOneSecond->value()) - m_angleOffsetValue);
        secondValue -= secondValue > 360 ? 360 : 0;
        secondValue += secondValue < 0 ? 360 : 0;

        emit SignalSetBlindZoneOne(firstValue, secondValue);

    }

    if (m_blindZoneTwo->isChecked()) {

        firstValue = (static_cast<double>(m_blindZoneTwoFirst->value()) - m_angleOffsetValue);
        firstValue -= firstValue > 360 ? 360 : 0;
        firstValue += firstValue < 0 ? 360 : 0;

        secondValue = (static_cast<double>(m_blindZoneTwoSecond->value()) - m_angleOffsetValue);
        secondValue -= secondValue > 360 ? 360 : 0;
        secondValue += secondValue < 0 ? 360 : 0;

        emit SignalSetBlindZoneTwo(firstValue, secondValue);

    }

    if (m_blindZoneThree->isChecked()) {

        firstValue = (static_cast<double>(m_blindZoneThreeFirst->value()) - m_angleOffsetValue);
        firstValue -= firstValue > 360 ? 360 : 0;
        firstValue += firstValue < 0 ? 360 : 0;

        secondValue = (static_cast<double>(m_blindZoneThreeSecond->value()) - m_angleOffsetValue);
        secondValue -= secondValue > 360 ? 360 : 0;
        secondValue += secondValue < 0 ? 360 : 0;

        emit SignalSetBlindZoneThree(firstValue, secondValue);

    }
}

void GreenBoardControlUI::SlotBlindZoneOneCheckStateChanged(const bool &checkState)
{
    m_blindZoneOneFirst->setEnabled(checkState);
    m_blindZoneOneSecond->setEnabled(checkState);
    if (!checkState) {
        emit SignalSetBlindZoneOne(0, 0);
    }
}

void GreenBoardControlUI::SlotBlindZoneTwoCheckStateChanged(const bool &checkState)
{
    m_blindZoneTwoFirst->setEnabled(checkState);
    m_blindZoneTwoSecond->setEnabled(checkState);
    if (!checkState) {
        emit SignalSetBlindZoneTwo(0, 0);
    }
}

void GreenBoardControlUI::SlotBlindZoneThreeCheckStateChanged(const bool &checkState)
{
    m_blindZoneThreeFirst->setEnabled(checkState);
    m_blindZoneThreeSecond->setEnabled(checkState);
    if (!checkState) {
        emit SignalSetBlindZoneThree(0, 0);
    }
}

void GreenBoardControlUI::SlotStatusMessageReceived(const QString &message)
{
    emit SignalstatusMessage(message);
    m_statusMsgTimer->start();
}

void GreenBoardControlUI::SlotClearStatusMessage()
{
    emit SignalstatusMessage(QString(""));
    m_statusMsgTimer->stop();
}

void GreenBoardControlUI::SlotExtSamplingFrequencyChanged(const int &index)
{
    m_decimation->setCurrentIndex(index);
    SetControlSettingsClicked();
}

void GreenBoardControlUI::SlotExtTriggeringFrequencyChanged(const int &index)
{
    m_startFrequency2->setCurrentIndex(index);
    SlotSetControlSettings2Clicked();
}

void GreenBoardControlUI::SlotExtChannelAlengthChanged(const int &index)
{
    m_strobeSize->setCurrentIndex(index);
    SlotSetControlSettings2Clicked();
}

void GreenBoardControlUI::SlotExtAngleOffsetChanged()
{
    m_angleOffset->setValue(m_extAngleOffsetSpinBox->value());
    SlotSetAngleOffsetClicked();
}

void GreenBoardControlUI::SlotExtTriggerOutCheckStateChanged()
{
    bool checkState = !m_extTriggerOutCheckBox->isChecked();
    if (!m_networkError) {
        m_triggerEnable->setCurrentIndex(static_cast<int>(!checkState));
        SlotSetControlSettings2Clicked();
        if (checkState) {
            m_triggerOutputLedIndicator->setState(LedState::LedOn);
            emit SignalTxOff(false);
        } else {
            m_triggerOutputLedIndicator->setState(LedState::LedOffRed);
            emit SignalTxOff(true);
        }
        if (m_blindZoneLedIndicator->getLedState() == LedState::LedOn && m_extTriggerOutCheckBox->isChecked()) {
            m_triggerOutputLedIndicator->setState(LedState::LedWait);
        }
        if (m_onSchedule) {
            if (m_extTriggerOutCheckBox->isChecked()) {
                m_triggerOutputLedIndicator->setState(LedState::LedWait);
            } else {
                m_triggerOutputLedIndicator->setState(LedState::LedOn);
            }
        }
    }
}

void GreenBoardControlUI::SlotLostConnection()
{
    m_connectionStatusLedIndicator->setState(LedState::LedError);
    if (m_automaticReconnectTimer->isActive()) {
        m_connectionStatusLabel->setText(QString("Network Error (Reconnection)"));
    } else {
        m_connectionStatusLabel->setText(QString("Network Error"));
    }
    m_restartNetwork->setVisible(true);
    m_stopNetwork->setVisible(true);
    m_triggerOutputLedIndicator->setState(LedState::LedOff);
    if (!m_automaticReconnectTimer->isActive()) {
        m_automaticReconnectTimer->start();
    }
}

void GreenBoardControlUI::SlotDisconnection()
{
    m_connectionStatusLedIndicator->setState(LedState::LedOff);
    m_connectionStatusLabel->setText(QString("Disconnected"));
    m_restartNetwork->setVisible(true);
    m_stopNetwork->setVisible(false);
}

void GreenBoardControlUI::SlotConnected()
{
    m_connectionStatusLedIndicator->setState(LedState::LedOn);
    m_connectionStatusLabel->setText(QString("Connected"));
    m_restartNetwork->setVisible(false);
    m_stopNetwork->setVisible(true);
    m_automaticReconnectTimer->stop();
}

void GreenBoardControlUI::SlotBlindZoneStateChanged()
{
    if (m_blindZoneOne->isChecked() || m_blindZoneTwo->isChecked() || m_blindZoneThree->isChecked()) {
        m_blindZoneLedIndicator->setState(LedState::LedWait);
    } else {
        m_blindZoneLedIndicator->setState(LedState::LedOff);
    }
}

void GreenBoardControlUI::SlotBlindZoneIsActive(const bool &active)
{
    m_blindZoneIsActive = active;
    if (active) {
        m_blindZoneLedIndicator->setState(LedState::LedOn);
        if (m_extTriggerOutCheckBox->isChecked()) {
            m_triggerOutputLedIndicator->setState(LedState::LedWait);
        } else {
            m_triggerOutputLedIndicator->setState(LedState::LedOffRed);
        }
    } else {
        if (m_blindZoneOne->isChecked() || m_blindZoneTwo->isChecked() || m_blindZoneThree->isChecked()) {
            m_blindZoneLedIndicator->setState(LedState::LedWait);
        } else {
            m_blindZoneLedIndicator->setState(LedState::LedOff);
        }
        if (m_extTriggerOutCheckBox->isChecked()) {
            m_triggerOutputLedIndicator->setState(LedState::LedOn);
        } else {
            m_triggerOutputLedIndicator->setState(LedState::LedOffRed);
        }
    }
    emit SignalBlindZoneIsActive(active);
}

void GreenBoardControlUI::SlotOpenFileSettingsMenu()
{
    if (m_suffixPreffixMenu->isVisible()) {
        m_suffixPreffixMenu->close();
    } else {
        m_suffixPreffixMenu->show();
    }
}

void GreenBoardControlUI::SlotSetFileDateTimeInfo(const QString &text)
{
    m_fileDateTimeInfo = text;
}

void GreenBoardControlUI::SlotSetFileSamplingFrequencyInfo(const QString &text)
{
    m_fileSamplingFrequencyInfo = text;
}

void GreenBoardControlUI::SlotSetFileTriggeringFrequencyInfo(const QString &text)
{
    m_fileTriggeringFrequencyInfo = text;
}

void GreenBoardControlUI::SlotSetFileChannelLengthInfo(const QString &text)
{
    m_fileChannelLengthInfo = text;
}

void GreenBoardControlUI::SlotSetFileAngleOffsetInfo(const QString &text)
{
    m_fileAngleOffsetInfo = text;
}

void GreenBoardControlUI::SlotSetFileTuningInfo(const QString &text)
{
    m_fileTuningInfo = text;
}

void GreenBoardControlUI::SlotFilePathTextChanged(const QString &text)
{
    QDir dir(m_filePath->text());
    if ((m_filePath->text() == QString("")) || !dir.exists()) {
        m_savingFile->setToolTip(QString("Directory doesn't exist"));
        m_savingFile->setEnabled(false);
    } else {
        m_savingFile->setToolTip(QString("Enable/Disable Data Acqusition"));
        m_savingFile->setEnabled(true);
        setFileDirectory(text);
    }
}

void GreenBoardControlUI::SlotUpdateFileDateTimeInfo()
{
    m_fileDateTimeInfo = QDateTime::currentDateTime().toString(QString("yyyyMMdd"));
}

void GreenBoardControlUI::SlotUpdateFileSamplingFrequencyInfo()
{
    switch (m_controlSetings.m_decimation) {
    case SignalDecimation::DecimationBy1 : {
        m_fileSamplingFrequencyInfo = QString("80MHz");
        break;
    }
    case SignalDecimation::DecimationBy2 : {
        m_fileSamplingFrequencyInfo = QString("40MHz");
        break;
    }
    case SignalDecimation::DecimationBy4 : {
        m_fileSamplingFrequencyInfo = QString("20MHz");
        break;
    }
    case SignalDecimation::DecimationBy8 : {
        m_fileSamplingFrequencyInfo = QString("10MHz");
        break;
    }
    case SignalDecimation::DecimationBy16 : {
        m_fileSamplingFrequencyInfo = QString("5MHz");
        break;
    }
    case SignalDecimation::DecimationBy32 : {
        m_fileSamplingFrequencyInfo = QString("2500kHz");
        break;
    }
    case SignalDecimation::DecimationBy64 : {
        m_fileSamplingFrequencyInfo = QString("1125kHz");
        break;
    }
    case SignalDecimation::DecimationBy128 : {
        m_fileSamplingFrequencyInfo = QString("562.5kHz");
        break;
    }
    }
}

void GreenBoardControlUI::SlotUpdateFileTriggeringFrequencyInfo()
{
    switch (m_controlSettings2.m_frequencyScale) {
    case IntenernalStartSourceScale::Frequency_1kHz : {
        m_fileTriggeringFrequencyInfo = QString("1kHz");
        break;
    }
    case IntenernalStartSourceScale::Frequency_2kHz : {
        m_fileTriggeringFrequencyInfo = QString("2kHz");
        break;
    }
    case IntenernalStartSourceScale::Frequency_3kHz : {
        m_fileTriggeringFrequencyInfo = QString("3kHz");
        break;
    }
    case IntenernalStartSourceScale::Frequency_4kHz : {
        m_fileTriggeringFrequencyInfo = QString("4kHz");
        break;
    }
    }
}

void GreenBoardControlUI::SlotUpdateFileChannelLengthInfo()
{
    switch (m_controlSettings2.m_strobeSize) {
    case StrobeSize::Strobe_A32_B2016 : {
        m_fileChannelLengthInfo = QString("A32_B2016");
        break;
    }
    case StrobeSize::Strobe_A64_B1984 : {
        m_fileChannelLengthInfo = QString("A64_B1984");
        break;
    }
    case StrobeSize::Strobe_A128_B1920 : {
        m_fileChannelLengthInfo = QString("A128_B1920");
        break;
    }
    case StrobeSize::Strobe_A256_B1792 : {
        m_fileChannelLengthInfo = QString("A256_B1792");
        break;
    }
    }
}

void GreenBoardControlUI::SlotUpdateFileAngleOffsetInfo()
{
    m_fileAngleOffsetInfo = QString("AO") + QString::number(m_angleOffsetValue);
    m_fileAngleOffsetInfo.replace(QString("."), QString("c"));
}

void GreenBoardControlUI::SlotUpdateFileTuningInfo(const quint8 &tuning)
{
    m_fileTuningInfo = QString("T") + QString::number(tuning);
}

void GreenBoardControlUI::SlotUpdateFileInfo()
{
    SlotUpdateFileDateTimeInfo();

    QString prefix = QString("");
    if (m_prefixCheckBox->isChecked()) {
        if (m_fileDateTimePrefixCheckBox->isChecked()) {
            prefix += m_fileDateTimeInfo + QString("_");
        }
        if (m_fileSamplingFrequencyFrefixCheckBox->isChecked()) {
            prefix += m_fileSamplingFrequencyInfo + QString("_");
        }
        if (m_fileTriggeringFrequencyFrefixCheckBox->isChecked()) {
            prefix += m_fileTriggeringFrequencyInfo + QString("_");
        }
        if (m_fileChannelLengthFrefixCheckBox->isChecked()) {
            prefix += m_fileChannelLengthInfo + QString("_");
        }
        if (m_fileAngleOffsetFrefixCheckBox->isChecked()) {
            prefix += m_fileAngleOffsetInfo + QString("_");
        }
        if (m_fileTuningFrefixCheckBox->isChecked()) {
            prefix += m_fileTuningInfo + QString("_");
        }
    }
    m_fileNamePrefix->setText(prefix);

    QString suffix = QString("");
    if (m_suffixCheckBox->isChecked()) {
        if (m_fileDateTimeSuffixCheckBox->isChecked()) {
            suffix += QString("_") + m_fileDateTimeInfo;
        }
        if (m_fileSamplingFrequencySuffixCheckBox->isChecked()) {
            suffix += QString("_") + m_fileSamplingFrequencyInfo;
        }
        if (m_fileTriggeringFrequencySuffixCheckBox->isChecked()) {
            suffix += QString("_") + m_fileTriggeringFrequencyInfo;
        }
        if (m_fileChannelLengthSuffixCheckBox->isChecked()) {
            suffix += QString("_") + m_fileChannelLengthInfo;
        }
        if (m_fileAngleOffsetSuffixCheckBox->isChecked()) {
            suffix += QString("_") + m_fileAngleOffsetInfo;
        }
        if (m_fileTuningSuffixCheckBox->isChecked()) {
            suffix += QString("_") + m_fileTuningInfo;
        }
    }
    m_fileNameSuffix->setText(suffix);

    m_fileName->setToolTip(m_fileNamePrefix->text() + m_fileName->text() + m_fileNameSuffix->text());
}

void GreenBoardControlUI::SlotSavingLedIndicatorStateChange(const bool &state)
{
    if (m_freeDiskSpaceStopSavingState) {
        m_fileSavingLedIndicator->setState(LedState::LedOffRed);
        m_fileSavingLedIndicator->setToolTip(QString("Out of hard drive space"));
        emit SignalstatusMessage(QString("Out of hard drive space"));
    } else {
        if (state) {
            if ((m_fileSavingLedIndicator->getLedState() == LedState::LedOn) || m_fileSavingLedIndicator->getLedState() == LedState::LedError) {
                m_fileSavingLedIndicator->setState(LedState::LedOff);
            } else if (m_freeDiskSpaceWarningState) {
                m_fileSavingLedIndicator->setState(LedState::LedError);
            } else {
                m_fileSavingLedIndicator->setState(LedState::LedOn);
            }
        } else {
            m_fileSavingLedIndicator->setState(LedState::LedOff);
        }
        if (m_freeDiskSpaceWarningState) {
            m_fileSavingLedIndicator->setToolTip(QString("Running out of hard drive space"));
            emit SignalstatusMessage(QString("Running out of hard drive space"));
        } else {
            m_fileSavingLedIndicator->setToolTip(QString(""));
        }
    }
}

void GreenBoardControlUI::SlotExtOpenFolder()
{
    const char* command = "gio open " + QByteArray(m_filePath->text().toLocal8Bit()) + QByteArray(QString("/").toLocal8Bit());
    system(command);
}

void GreenBoardControlUI::SlotReceivingLedIndicatorTimerUpdate()
{
    if (m_receivingIsEnable) {
        m_receivingLedIndicator->setState(m_receivingLedIndicator->getLedState() == LedState::LedOff ? LedState::LedOn : LedState::LedOff);
        m_receivingIsEnable = false;
    } else {
        m_receivingLedIndicator->setState(LedState::LedOff);
    } 
}

void GreenBoardControlUI::SlotOpenAdditionalInformationClicked()
{
    bool checkState = m_infoOpenButton->isChecked();
    m_receivedLabel->setVisible(checkState);
    m_bytesReceived->setVisible(checkState);
    m_counterBeginningTime->setVisible(checkState);
    m_counterBeginningDate->setVisible(checkState);
    m_errorsLabel->setVisible(checkState);
    m_errorCoefCounter->setVisible(checkState);
    m_counterLabel->setVisible(checkState);

}

void GreenBoardControlUI::SlotMotorControlOpen()
{
    m_motorControlWidget->setVisible(m_motorControlMenuOpen->isChecked());
}

void GreenBoardControlUI::slotSetOnSchedule(const bool onSchedule)
{
    m_onSchedule = onSchedule;
    SlotExtTriggerOutCheckStateChanged();
    if (m_onSchedule) {
        m_defaultTriggerState = !m_extTriggerOutCheckBox->isChecked();
        if (!m_extTriggerOutCheckBox->isChecked()) {
            m_extTriggerOutCheckBox->click();
        }
        emit SignalLogEvent(QString("GreenBoard: Schedule Is Activated"));
    } else {
        if (m_extTriggerOutCheckBox->isChecked() == m_defaultTriggerState) {
            m_extTriggerOutCheckBox->click();
        }
        emit SignalLogEvent(QString("GreenBoard: Schedule Is Inactivated"));
    }
}

void GreenBoardControlUI::slotSetTriggerOutputOn(const bool &isTransmitting)
{
    if (m_extTriggerOutCheckBox->isChecked() == isTransmitting) {
        m_extTriggerOutCheckBox->click();
    }
    if (m_logTriggerOutIsTurnedOn != isTransmitting) {
        if (isTransmitting) {
            emit SignalLogEvent(QString("GreenBoard: Trigger Output Is Activated By schedule"));
        } else {
            emit SignalLogEvent(QString("GreenBoard: Trigger Output Is Inactivated By schedule"));
        }
        m_logTriggerOutIsTurnedOn = isTransmitting;
    }
}

void GreenBoardControlUI::slotSetDataSavingOn(const bool &isSaving)
{
    bool savingStateBuffer = m_scheduleRemoteSavingIsActive;
    m_scheduleRemoteSavingIsActive = isSaving;
    if (savingStateBuffer != isSaving)
    {
        SavingFileClicked();
    }

    if (m_logDataSavingIsTurnedOn != isSaving) {
        if (isSaving) {
            emit SignalLogEvent(QString("GreenBoard: Data Saving Is Activated By schedule"));
        } else {
            emit SignalLogEvent(QString("GreenBoard: Data Saving Is Inactivated By schedule"));
        }
        m_logDataSavingIsTurnedOn = isSaving;
    }
}

void GreenBoardControlUI::SlotOpenBlindZoneWidgetClicked()
{
    if (m_blindZoneWidget->isVisible()) {
        m_blindZoneWidget->close();
        m_extBlindZonePushButton->setChecked(false);
    } else {
        m_blindZoneWidget->show();
        m_extBlindZonePushButton->setChecked(true);
    }
}

void GreenBoardControlUI::SlotScheduleTimeCoefficientChanged(const double &coefficient)
{
    m_scheduleTimeCoefficient = coefficient;
}
