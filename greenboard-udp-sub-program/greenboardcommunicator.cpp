/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/
#include "greenboardcommunicator.h"

#include <QDataStream>
#include <qiterator.h>
#include "shared-libs/global.h"

GreenBoardCommunicator::GreenBoardCommunicator(QMap<QString, QString> iniSettings, QObject *parent) : QThread(parent)
{
    m_mutex = new QMutex;

    IniReader::checkConfiguration(iniSettings);

    setPcHostAddress(iniSettings["PcHostAddress"], iniSettings["PcControlPort"].toUInt(), iniSettings["PcDataPort"].toUInt());
    setBoardHostAddress(iniSettings["BoardHostAddress"], iniSettings["BoardControlPort"].toUInt(), iniSettings["BoardDataPort"].toUInt());

    m_udpControlSocket = new QUdpSocket;

    m_udpControlSocket->abort();
    m_udpControlSocket->bind(m_pcHostAddress, m_pcControlPort);
    m_udpControlSocket->open(QUdpSocket::ReadWrite);

    GreenBoardLED led;
    led.m_enable = 0;
    sendControlMessage(led.greenBoardLED());

    m_dataSavingThread = new DataSavingThread;
    connect(m_dataSavingThread, &DataSavingThread::signalBytesWritten, this, &GreenBoardCommunicator::signalBytesWritten);
    connect(this, &GreenBoardCommunicator::startSavingThread, m_dataSavingThread, &DataSavingThread::startWorkingThread);
    connect(this, &GreenBoardCommunicator::stopSavingThread, m_dataSavingThread, &DataSavingThread::stopWorkingThread);
    connect(m_dataSavingThread, &DataSavingThread::poolStatus, this, &GreenBoardCommunicator::dataIsSaving);
    connect(m_dataSavingThread, &DataSavingThread::signalFreeDiskSpace, this, &GreenBoardCommunicator::signalFreeDiskSpace);
    m_savingState = false;

    m_angleOffset = 0;

    m_guiInformationUpdater = new QTimer;
    connect(m_guiInformationUpdater, &QTimer::timeout, this, &GreenBoardCommunicator::SlotUpdateGuiInformation);
    m_guiInformationUpdater->start(50);

    m_reconnectTimer = new QTimer;
    connect(m_reconnectTimer, &QTimer::timeout, this, &GreenBoardCommunicator::SlotReconnectTimer);
    m_reconnectTimer->setInterval(100);

    m_savingThreadCheckTimer = new QTimer;
    connect(m_savingThreadCheckTimer, &QTimer::timeout, this, &GreenBoardCommunicator::SlotSavingThreadCheck);
    m_savingThreadCheckTimer->start(1000);

    connect(this, &GreenBoardCommunicator::SignalDataToSave, m_dataSavingThread, &DataSavingThread::SlotDataToSave);
    connect(m_dataSavingThread, &DataSavingThread::SignalLogEvent, this, &GreenBoardCommunicator::SignalLogEvent);

    m_motorControlStatement.setMod(MotorModBytes::PowerOff);
    m_motorControlStatement.setVelocity1(1000);
    m_motorControlStatement.setVelocity2(1000);
    m_motorControlStatement.setAngle1(0);
    m_motorControlStatement.setAngle2(0);

    m_motorControlTimer = new QTimer;
    connect(m_motorControlTimer, &QTimer::timeout, this, &GreenBoardCommunicator::SlotMotorControlTimeout);
}

GreenBoardCommunicator::~GreenBoardCommunicator()
{
    GreenBoardLED led;
    led.m_enable = 0;
    sendControlMessage(led.greenBoardLED());

    if (m_workingThreadEnable) {

        stopWorkingThread();
        msleep(1);
    }

    m_dataSavingThread->stopWorkingThread();
    msleep(3);

    m_udpControlSocket->close();
}

bool GreenBoardCommunicator::BlindZoneTrueCheck(const double &angleSensorValue)
{
    if (m_blindZoneOneActivated) {
        if (m_blindZoneOne.first < m_blindZoneOne.second) {
            if ((angleSensorValue > m_blindZoneOne.first) && (angleSensorValue < m_blindZoneOne.second)) {
                return true;
            }
        } else {
            if ((angleSensorValue > m_blindZoneOne.second) && (angleSensorValue < m_blindZoneOne.first)) {
            } else {
                return true;
            }
        }
    }

    if (m_blindZoneTwoActivated) {
        if (m_blindZoneTwo.first < m_blindZoneTwo.second) {
            if ((angleSensorValue > m_blindZoneTwo.first) && (angleSensorValue < m_blindZoneTwo.second)) {
                return true;
            }
        } else {
            if ((angleSensorValue > m_blindZoneTwo.second) && (angleSensorValue < m_blindZoneTwo.first)) {
            } else {
                return true;
            }
        }
    }

    if (m_blindZoneThreeActivated) {
        if (m_blindZoneThree.first < m_blindZoneThree.second) {
            if ((angleSensorValue > m_blindZoneThree.first) && (angleSensorValue < m_blindZoneThree.second)) {
                return true;
            }
        } else {
            if ((angleSensorValue > m_blindZoneThree.second) && (angleSensorValue < m_blindZoneThree.first)) {
            } else {
                return true;
            }
        }
    }

    return false;
}

void GreenBoardCommunicator::run()
{
    if (m_workingThreadEnable) {
        emit poolStatus(true);
    }

    m_udpDataSocket = new QUdpSocket;

    m_udpDataSocket->abort();
    m_udpDataSocket->bind(m_pcHostAddress, m_pcDataPort);
    m_udpDataSocket->abort();
    m_udpDataSocket->bind(m_pcHostAddress, m_pcDataPort);
    m_udpDataSocket->close();
    m_udpDataSocket->bind(m_pcHostAddress, m_pcDataPort);
    m_udpDataSocket->open(QUdpSocket::ReadOnly);

    QByteArray receivedMessage;
    QByteArray fullBeam;
    QByteArray fullBeamDrawing;
    quint8 fullBeamCounter = 0;
    quint16 beamBeginNumber = 0;
    quint16 currentMessageNumber = 0;
    quint16 lastReceivedNumber = 0;
    quint16 newBeamFlag = 0;
    quint8 messageCounter = 0;
    quint16 angleSensorValue = 0;
    quint8 fullBeamSize = 8;
    quint8 sendDataToGuiCounter = 0;
    m_angleSensorValue = g_defaultAngleSensorValue;
    m_beamCounter = 0;

    QByteArray header;
    QByteArray zerosInTheHeader(12, 0);

//    struct timespec ts;
//    ts.tv_sec = 0;
//    ts.tv_nsec = 500;
//    nanosleep(&ts, nullptr);

    while (m_workingThreadEnable) {
        receivedMessage = m_udpDataSocket->receiveDatagram().data();
        if (!receivedMessage.isEmpty()) {
            // Bytes Received
            emit signalBytesReceived(receivedMessage.size());

            // Check if Empty
            if (receivedMessage.isEmpty()) {
                qDebug("Empty in Receiving Thread!");
            }

            memcpy(&angleSensorValue, receivedMessage.data(), 2);
            m_angleSensorValue = angleSensorValue;

            // Blind Zone
            if (m_blindZoneOneActivated || m_blindZoneTwoActivated || m_blindZoneThreeActivated) {
                if (BlindZoneTrueCheck(static_cast<double>(angleSensorValue)*360.0/g_maxAngleSensorValue)) {
                    m_blind = true;
                    if (m_transmitterWorkingState == true) {
                        emit SignalTransmitterDisabled(true);
                        emit SignalBlindZoneIsActive(true);
                        m_transmitterWorkingState = false;
                    }
                } else {
                    m_blind = false;
                    if (m_transmitterWorkingState == false) {
                        emit SignalTransmitterDisabled(false);
                        emit SignalBlindZoneIsActive(false);
                        m_transmitterWorkingState = true;
                    }
                }
            } else if (m_transmitterWorkingState == false) {
                emit SignalTransmitterDisabled(false);
                emit SignalBlindZoneIsActive(false);
                m_transmitterWorkingState = true;
            }

            // Received Data Processing
            memcpy(&currentMessageNumber, receivedMessage.data() + 4, 2);
            memcpy(&newBeamFlag, receivedMessage.data() + 6, 2);

            if (newBeamFlag == 0x55AA) {
                if (!fullBeam.isEmpty()) {
                    if (fullBeamCounter == fullBeamSize) {
                        m_mutex->lock();
                        if (!m_blind) {
//                            emit SignalFullBeam(fullBeam);
                            emit SignalDataToSave(fullBeam);
                            if (fullBeam.size() != ((m_lastSavingDatagram - m_firstSavingDatagram + 1 + (int)(bool)m_firstSavingDatagram)*1024 + 32)) {
                                emit SignalstatusMessage("Wrong Length!");
                            }
                        }
                        if (sendDataToGuiCounter >= m_signalStartFrequency) {
                            emit SignalFullBeamToGui(fullBeamDrawing);
                            sendDataToGuiCounter = 0;
                        } else {
                            sendDataToGuiCounter++;
                        }
                        m_mutex->unlock();
                    }
                    fullBeam.clear();
                    fullBeamDrawing.clear();
                    fullBeamCounter = 0;
                }
                if (messageCounter < fullBeamSize && messageCounter > 0) {
                    m_errorsCounter += fullBeamSize - messageCounter;
                }

                m_beamCounter += 1;

                header.clear();
                header.append((char*)&m_beamCounter, 2);
                angleSensorValue += m_angleOffset;
                header.append((char*)&(angleSensorValue), 2);
                header.append(CurrentTime().toLocal8Bit());
                header.append(zerosInTheHeader);

                fullBeam.append(header);
                fullBeamDrawing.append(header);
                receivedMessage.remove(0, 8);
                fullBeam.append(receivedMessage);
                fullBeamDrawing.append(receivedMessage);
                fullBeamCounter++;
                beamBeginNumber = currentMessageNumber;
                messageCounter = 0;
            } else if (messageCounter < fullBeamSize) {
                receivedMessage.remove(0, 8);
                if ((messageCounter >= m_firstSavingDatagram) && (messageCounter <= m_lastSavingDatagram)) {
                    fullBeam.append(receivedMessage);
                }
                fullBeamDrawing.append(receivedMessage);
                fullBeamCounter++;
            } else {
                if (!fullBeam.isEmpty()) {
                    if (fullBeamCounter == fullBeamSize) {
                        m_mutex->lock();
                        if (!m_blind) {
//                            emit SignalFullBeam(fullBeam);
                            emit SignalDataToSave(fullBeam);
                        }
                            if (sendDataToGuiCounter >= m_signalStartFrequency) {
                                emit SignalFullBeamToGui(fullBeamDrawing);
                                sendDataToGuiCounter = 0;
                            } else {
                                sendDataToGuiCounter++;
                            }
                        m_mutex->unlock();
                    }
                    fullBeam.clear();
                    fullBeamDrawing.clear();
                    fullBeamCounter = 0;
                }
                quint16 lostPackagesCounter = currentMessageNumber - lastReceivedNumber;
                m_beamCounter += ceil(lostPackagesCounter/fullBeamSize);
                m_errorsCounter += lostPackagesCounter;
                beamBeginNumber = beamBeginNumber + static_cast<quint16>(floor(static_cast<double>(currentMessageNumber - beamBeginNumber)/fullBeamSize));
                messageCounter = currentMessageNumber - beamBeginNumber;
//                receivedMessage.remove(0, 8);
//                fullBeam.append(receivedMessage);

            }
            lastReceivedNumber = currentMessageNumber;
            messageCounter++;
        }
//        usleep(1);
//        nanosleep(&ts, nullptr);

    }
    if (!m_workingThreadEnable) {
        emit poolStatus(false);
    }
    m_mutex->lock();
    m_udpDataSocket->close();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

void GreenBoardCommunicator::setPcHostAddress(const QString &hostAddress, const int &controlPort, const int &dataPort)
{
    m_pcHostAddress = QHostAddress(hostAddress);
    m_pcControlPort = controlPort;
    m_pcDataPort = dataPort;
}

void GreenBoardCommunicator::setBoardHostAddress(const QString &hostAddress, const int &controlPort, const int &dataPort)
{
    m_boardHostAddress = QHostAddress(hostAddress);
    m_boardControlPort = controlPort;
    m_boardDataPort = dataPort;
}

bool GreenBoardCommunicator::isWorking()
{
    m_mutex->lock();
    bool status = m_workingThreadEnable;
    m_mutex->unlock();
    return status;
}

QString GreenBoardCommunicator::CurrentTime()
{
    QString dateString = QDateTime::currentDateTime().toString(QString("yyyyddMMsszzzhhmm"));
    dateString.remove(dateString.length() - 5, 1);
    std::reverse(dateString.begin(), dateString.end());
    return  dateString;
}

void GreenBoardCommunicator::SlotSendSpiMessage(const QByteArray &spiMessage)
{
    if (spiMessage.size() == 3) {
        QByteArray message0, message1, message2;
        message0.append(static_cast<quint8>(GreenBoardAddressBytes::SPI_Byte_0));
        message0.append(spiMessage[0]);
        message1.append(static_cast<quint8>(GreenBoardAddressBytes::SPI_Byte_1));
        message1.append(spiMessage[1]);
        message2.append(static_cast<quint8>(GreenBoardAddressBytes::SPI_Byte_2));
        message2.append(spiMessage[2]);

        QList<QByteArray> messageList;
        messageList.append(message0);
        messageList.append(message1);
        messageList.append(message2);
        for (int i = 0; i < 1; i++) {
            this->sendControlMessage(messageList);
        }
    }
}

void GreenBoardCommunicator::SlotSendSpiMessageList(const QList<QByteArray> &spiMessageList)
{
    for (int i = 0; i < spiMessageList.size(); i++) {
        if (spiMessageList.at(i).size() == 3) {
            SlotSendSpiMessage(spiMessageList[i]);
        }
    }
}

void GreenBoardCommunicator::SlotMotorControlTimeout()
{
    this->SlotSendSpiMessage(m_motorControlStatement.getCurrentMessage());
    m_motorControlStatement.plusAddressCounter();
}

qint8 GreenBoardCommunicator::sendControlMessage(const QByteArray &message)
{
    emit timeoutError(false);
    if (message.isEmpty()) {
        return -2;
    }
    m_udpControlSocket->writeDatagram(message, m_boardHostAddress, m_boardControlPort);
    msleep(5);
    QByteArray answer = m_udpControlSocket->receiveDatagram().data();
    if (answer.size() != 48) {
        for (int i = 0; i < 5; i++) {
            m_udpControlSocket->writeDatagram(message, m_boardHostAddress, m_boardControlPort);
            answer = m_udpControlSocket->receiveDatagram().data();
            msleep(5);
            if (answer.size() == 48) {
                emit errorsOccurred(i + 1);
                return i + 1;
            }
        }
        emit timeoutError(true);
        return -1;
    } else {
        return 0;
    }
}

qint8 GreenBoardCommunicator::sendControlMessage(const QList<QByteArray> &messageList)
{
    qint8 errorCounter = 0;
    if (messageList.isEmpty()) {
        return -2;
    }
    for (int num = 0; num < messageList.size(); num++) {
        quint8 errorCode = sendControlMessage(messageList[num]);
        if (errorCode < 0) {
            return errorCode;
        } else {
            errorCounter += errorCode;
        }
        msleep(1);
    }
    return errorCounter;
}

void GreenBoardCommunicator::startWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = true;
    m_mutex->unlock();

    if (!isRunning()) {
        start();
//        setPriority(Priority::TimeCriticalPriority);
    }
}

void GreenBoardCommunicator::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

void GreenBoardCommunicator::startMotorControlInterface()
{
    m_motorControlTimer->start(100);
    emit signalMotorControlIntefaceIsWorking(true);
}

void GreenBoardCommunicator::stopMotorControlInteface()
{
    m_motorControlTimer->stop();
    emit signalMotorControlIntefaceIsWorking(false);
}

void GreenBoardCommunicator::dataIsSaving(const bool &savingState)
{
    m_mutex->lock();
    m_savingState = savingState;
    m_mutex->unlock();
    emit dataSavingState(savingState);
}

void GreenBoardCommunicator::SlotSetAngleOffset(const quint16 &angleOffset)
{
    m_mutex->lock();
    m_angleOffset = angleOffset;
    m_mutex->unlock();
}

void GreenBoardCommunicator::SlotSignalStartFrequencyChanged(const IntenernalStartSourceScale &frequency)
{
    m_mutex->lock();
    switch (frequency) {
    case IntenernalStartSourceScale::Frequency_1kHz : {
        m_signalStartFrequency = 0;
        break;
    }
    case IntenernalStartSourceScale::Frequency_2kHz : {
        m_signalStartFrequency = 1;
        break;
    }
    case IntenernalStartSourceScale::Frequency_3kHz : {
        m_signalStartFrequency = 2;
        break;
    }
    case IntenernalStartSourceScale::Frequency_4kHz : {
        m_signalStartFrequency = 3;
        break;
    }
    }
    m_mutex->unlock();
}

void GreenBoardCommunicator::SlotSetBlindZoneOne(const double &firstValue, const double &secondValue)
{
    m_mutex->lock();
    if (static_cast<qint32>(firstValue*100.0) == static_cast<qint32>(secondValue*100.0)) {
        m_blindZoneOneActivated = false;
    } else {
        m_blindZoneOne.first = firstValue;
        m_blindZoneOne.second = secondValue;
        m_blindZoneOneActivated = true;
    }
    m_mutex->unlock();
}

void GreenBoardCommunicator::SlotSetBlindZoneTwo(const double &firstValue, const double &secondValue)
{
    m_mutex->lock();
    if (static_cast<qint32>(firstValue*100.0) == static_cast<qint32>(secondValue*100.0)) {
        m_blindZoneTwoActivated = false;
    } else {
        m_blindZoneTwo.first = firstValue;
        m_blindZoneTwo.second = secondValue;
        m_blindZoneTwoActivated = true;
    }
    m_mutex->unlock();
}

void GreenBoardCommunicator::SlotSetBlindZoneThree(const double &firstValue, const double &secondValue)
{
    m_mutex->lock();
    if (static_cast<qint32>(firstValue*100.0) == static_cast<qint32>(secondValue*100.0)) {
        m_blindZoneThreeActivated = false;
    } else {
        m_blindZoneThree.first = firstValue;
        m_blindZoneThree.second = secondValue;
        m_blindZoneThreeActivated = true;
    }
    m_mutex->unlock();
}

void GreenBoardCommunicator::SlotUpdateGuiInformation()
{
    m_mutex->lock();
    quint16 angleSensorValue = m_angleSensorValue;
    quint64 errorsCounter = m_errorsCounter;
    m_errorsCounter = 0;
    m_angleSensorValue = g_defaultAngleSensorValue;
    m_mutex->unlock();
    emit signalAngleSensorValue(angleSensorValue);
    emit signalPackagesLost(errorsCounter);
}

void GreenBoardCommunicator::SlotReconnectTimer()
{
    m_reconnectTimer->stop();
    startWorkingThread();
}

void GreenBoardCommunicator::SlotStartReconnectTimer()
{
    m_reconnectTimer->start();
}

void GreenBoardCommunicator::SlotSavingThreadCheck()
{
    emit SignalDataIsSaving(m_dataSavingThread->isRunning());
}

void GreenBoardCommunicator::slotSetFirstSavingDatagram(const quint8 &firstSavingDatagram)
{
    m_mutex->lock();
    m_firstSavingDatagram = firstSavingDatagram;
    m_mutex->unlock();
}

void GreenBoardCommunicator::slotSetLastSavingDatagram(const quint8 &lastSavingDatagram)
{
    m_mutex->lock();
    m_lastSavingDatagram = lastSavingDatagram;
    m_mutex->unlock();
}

void GreenBoardCommunicator::slotSetSavingDatagramDiapasone(quint8 &firstSavingDatagram, quint8 &lastSavingDatagram)
{
    if (firstSavingDatagram > lastSavingDatagram)
    {
        quint8 buffer = firstSavingDatagram;
        firstSavingDatagram = lastSavingDatagram;
        lastSavingDatagram = buffer;
    }
    slotSetFirstSavingDatagram(firstSavingDatagram);
    slotSetLastSavingDatagram(lastSavingDatagram);
}

void GreenBoardCommunicator::slotSetMaxFileSize(const qint64 &maxFileSize)
{
    m_dataSavingThread->SlotSetFileMaxSize(maxFileSize);
}

void GreenBoardCommunicator::slotSetMotorControlStatement(const MotorControl &motorControl)
{
    m_mutex->lock();
    m_motorControlStatement = motorControl;
    m_mutex->unlock();
}
