/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "datasavingthread.h"

#include <QDateTime>
#include <QStorageInfo>

#include "shared-libs/inireader.h"
#include "shared-libs/global.h"

DataSavingThread::DataSavingThread(QMap<QString, QString> iniSettings, QObject *parent) : QThread(parent)
{
    m_mutex = new QMutex;

    IniReader::checkConfiguration(iniSettings);
    m_fileName = g_tmpPath + g_pathDevider + iniSettings["DefaultFileName"] + QString(".ds");
    if (iniSettings["StartSavingTreadAutomaticly"].toUInt() == 1) {
        startWorkingThread();
    }

    m_savingData = new QByteArray;

    m_dataSavedUpdater = new QTimer;
    connect(m_dataSavedUpdater, &QTimer::timeout, this, &DataSavingThread::SlotDataSavedUpdate);
    m_dataSavedUpdater->start(50);
}

void DataSavingThread::setFileName(const QString &fileName)
{
    if (!this->workingThreadState()) {
        if (m_fileName != fileName) {
            m_currentFileNameCounter = 0;
            m_dataSavedCounter = 0;
        }
        m_fileName = fileName;
    }
}

bool DataSavingThread::workingThreadState()
{
    m_mutex->lock();
    bool state = m_workingThreadEnable;
    m_mutex->unlock();
    return state;
}

void DataSavingThread::run()
{
    if (m_workingThreadEnable) {
        emit poolStatus(true);
        emit SignalLogEvent(QString("GreenBoard: Data Saving Thread Started"));
    }    
    while (m_workingThreadEnable) {
        m_mutex->lock();
        if (!m_savingData->isEmpty()) {
//            while (QFile::exists(QString(m_fileName.remove(".ds") + "_" + QString::number(m_currentFileNameCounter)) + ".ds")) {
//                m_currentFileNameCounter++;
//            }
            QFile file(QString(m_fileName.remove(".ds") + "_" + QString::number(m_currentFileNameCounter)) + ".ds");
            if (file.open(QFile::Append)) {
                if (m_dataSavedCounter == 0) {
                    m_dataSavedCounter += file.size();
                }
                m_dataSavedCounter += file.write(*m_savingData);
                if (file.size() >= m_fileMaxSize) {
                    m_currentFileNameCounter++;
                }
                file.close();
                m_savingData->clear();
            }
        }
        m_mutex->unlock();
        msleep(1);
//        emit getDataToSave();
    }
    if (!m_workingThreadEnable) {
        emit poolStatus(false);
        emit SignalLogEvent(QString("GreenBoard: Data Saving Thread Stopped"));
        emit SignalLogEvent(QString("GreenBoard: ") + QString::number(m_dataSavedCounter/(1024.0*1024.0*1024)) + QString(" GB of Data Saved"));
        QFileInfo fileInfo(m_fileName);
        QStorageInfo storageInfo(fileInfo.dir().path());
        emit SignalLogEvent(QString("GreenBoard: ") + QString::number(storageInfo.bytesAvailable()/(1024.0*1024.0*1024)) + QString("/")
                            + QString::number(storageInfo.bytesTotal()/(1024.0*1024.0*1024)) + QString(" GB of Free Space Avaliable"));
    }
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

void DataSavingThread::startWorkingThread(const QString &fileName, const SavingOption &savingOption)
{
    if (fileName != QString(""))
    {
        QString fileExtantion;
        switch (savingOption) {
        case SavingOption::DirectSaving : {
            fileExtantion = QString(".ds");
            break;
        }
        case SavingOption::SingleRotation : {
            fileExtantion = QString(".ss");
            break;
        }
        }

        setFileName(fileName + fileExtantion);

        emit SignalLogEvent(QString("GreenBoard: Data Saving Thread Is Starting. File Name: " + m_fileName));

        if (!workingThreadState()) {
            m_mutex->lock();
            m_workingThreadEnable = true;
            m_mutex->unlock();

            if (!isRunning()) {
                start();
                //            setPriority(Priority::NormalPriority);
            }
        }
    }
}

void DataSavingThread::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

void DataSavingThread::SlotDataToSave(const QByteArray &data)
{
    if (isRunning()) {
        m_mutex->lock();
        m_savingData->append(data);
        m_mutex->unlock();
    }
}

void DataSavingThread::SlotDataSavedUpdate()
{
    m_mutex->lock();
    quint64 dataSavedCounter = m_dataSavedCounter;
    m_mutex->unlock();
    emit signalBytesWritten(dataSavedCounter);
}

void DataSavingThread::SlotSetFileMaxSize(const qint64 &maxFileSize)
{
    m_mutex->lock();
    m_fileMaxSize = maxFileSize;
    m_mutex->unlock();
}
