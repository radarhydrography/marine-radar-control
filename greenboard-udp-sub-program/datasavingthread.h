/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef DATASAVINGTHREAD_H
#define DATASAVINGTHREAD_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QFile>
#include <QFileSystemModel>
#include <QStorageInfo>
#include <QTimer>

enum struct SavingOption
{
    DirectSaving        =   0,
    SingleRotation      =   1
};

class DataSavingThread : public QThread
{
    Q_OBJECT

private:

    QMutex *m_mutex;
    bool m_workingThreadEnable = false;
    QByteArray *m_savingData;

    quint16 m_beamCounter = 0;

    quint16 m_currentFileNameCounter = 0;

    QString m_fileName = QString("FileName.ds");

    QStorageInfo m_savingStorageInfo;

    quint64 m_dataSavedCounter = 0;
    quint64 m_freeStorageSpaceCounter = 0;

    QTimer *m_dataSavedUpdater;

    qint64 m_fileMaxSize = static_cast<qint64>(2048)*static_cast<qint64>(1024)*static_cast<qint64>(1024);

public:

    explicit DataSavingThread(QMap<QString, QString> iniSettings = QMap<QString, QString>(), QObject *parent = nullptr);

    void setFileName(const QString &fileName);
    bool workingThreadState();

    void run() override;

public slots:

//    void startWorkingThread();
    void startWorkingThread(const QString &fileName = QString("FileName"), const SavingOption &savingOption = SavingOption::DirectSaving);
    void stopWorkingThread();

    void SlotDataToSave(const QByteArray &data);

    void SlotDataSavedUpdate();

    void SlotSetFileMaxSize(const qint64 &maxFileSize);

signals:
    void poolStatus(const bool &threadStart);
    void getDataToSave();
    void signalBytesWritten(const quint64 &bytes);
    void signalFreeDiskSpace(const qint64 &bytes);

    void SignalLogEvent(const QString &logEvent);
};

#endif // DATASAVINGTHREAD_H
