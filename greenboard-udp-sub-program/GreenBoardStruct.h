/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef GREENBOARDSTRUCT_H
#define GREENBOARDSTRUCT_H

#include <QByteArray>
#include <QDataStream>

#include "shared-libs/global.h"

enum struct GreenBoardAddressBytes
{
    Control                             =   0x00,
    ADC_A_Delay_0                       =   0x01,
    ADC_A_Delay_1                       =   0x02,
    ADC_B_Delay_0                       =   0x03,
    ADC_B_Delay_1                       =   0x04,
    ADC_ControlWord_0                   =   0x05,
    ADC_ControlWord_1                   =   0x06,
    Control2                            =   0x07,
    LED                                 =   0x08,
    SPI_Byte_0                          =   0x09,
    SPI_Byte_1                          =   0x0A,
    SPI_Byte_2                          =   0x0B
};

struct GreenBoardAddress
{
    GreenBoardAddressBytes m_address;

    GreenBoardAddress()
    {
        m_address = GreenBoardAddressBytes::Control;
    }

    GreenBoardAddress(const unsigned char &address)
    {
        m_address = GreenBoardAddressBytes(address);
    }

    GreenBoardAddress(const GreenBoardAddressBytes &address)
    {
        m_address = address;
    }

    inline unsigned char greenBoardAddress()
    {
        return greenBoardAddress(m_address);
    }

    inline static unsigned char greenBoardAddress(const GreenBoardAddressBytes &address)
    {
        return static_cast<quint8>(address);
    }

    inline static GreenBoardAddressBytes addressBytes(const unsigned char &addressByte)
    {
        return static_cast<GreenBoardAddressBytes>(static_cast<quint8>(addressByte));
    }
};

enum struct RxEnable
{
    Disabled                        =   0b0,
    Enabled                         =   0b1
};

enum struct ClockSignalSource
{
    Internal                        =   0b0,
    External                        =   0b1
};

enum struct AdcOperationMode
{
    Normal                          =   0b0,
    Debug                           =   0b1
};

enum struct SignalDecimation
{
    DecimationBy1                   =   0b000,
    DecimationBy2                   =   0b001,
    DecimationBy4                   =   0b010,
    DecimationBy8                   =   0b011,
    DecimationBy16                  =   0b100,
    DecimationBy32                  =   0b101,
    DecimationBy64                  =   0b110,
    DecimationBy128                 =   0b111
};

enum struct IntenernalStartSourceScale
{
    Frequency_1kHz                  =   0b00,
    Frequency_2kHz                  =   0b01,
    Frequency_3kHz                  =   0b10,
    Frequency_4kHz                  =   0b11
};

enum struct StartSignalSource
{
    Internal                        =   0b0,
    External                        =   0b1
};

struct GreenBoardControl
{
    RxEnable m_rxEnable;
    ClockSignalSource m_clockSignalSource;
    AdcOperationMode m_operationMode;
    SignalDecimation m_decimation;
    IntenernalStartSourceScale m_frequencyScale;
    StartSignalSource m_startSignalSource;

    GreenBoardControl()
    {
        m_rxEnable = RxEnable(0);
        m_clockSignalSource = ClockSignalSource(0);
        m_operationMode = AdcOperationMode(0);
        m_decimation = SignalDecimation(0);
        m_frequencyScale = IntenernalStartSourceScale(0);
        m_startSignalSource = StartSignalSource(0);
    }

    GreenBoardControl(const RxEnable &rxEnable, const ClockSignalSource &clockSignalSource,
                      const AdcOperationMode &operationMode, const SignalDecimation &decimation,
                      const IntenernalStartSourceScale &frecuencyScale, const StartSignalSource &startSignalSource)
    {
        m_rxEnable = rxEnable;
        m_clockSignalSource = clockSignalSource;
        m_operationMode = operationMode;
        m_decimation = decimation;
        m_frequencyScale = frecuencyScale;
        m_startSignalSource = startSignalSource;
    }

    inline QByteArray greenBoardControl()
    {
        return greenBoardControl(m_rxEnable, m_clockSignalSource, m_operationMode, m_decimation, m_frequencyScale, m_startSignalSource);
    }

    inline static QByteArray greenBoardControl(const RxEnable &rxEnable, const ClockSignalSource &clockSignalSource,
                                               const AdcOperationMode &operationMode, const SignalDecimation &decimation,
                                               const IntenernalStartSourceScale &frecuencyScale, const StartSignalSource &startSignalSource)
    {
        QByteArray controlMessage;
        controlMessage.append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::Control));
        controlMessage.append(static_cast<unsigned char>((static_cast<quint8>(startSignalSource) << 7) | (static_cast<quint8>(frecuencyScale) << 6) |
                                                         (static_cast<quint8>(decimation) << 3) | (static_cast<quint8>(operationMode) << 2) |
                                                         (static_cast<quint8>(clockSignalSource) << 1) | static_cast<quint8>(rxEnable)));
        return controlMessage;
    }

    inline void setRxEnable(const quint8 &rxEnableIndex)
    {
        switch (rxEnableIndex) {
        case 0: {
            m_rxEnable = RxEnable::Disabled;
            break;
        }
        case 1: {
            m_rxEnable = RxEnable::Enabled;
            break;
        }
        default:
            m_rxEnable = RxEnable::Disabled;
        }
    }

    inline void setClockSignalSource(const quint8 &clockSignalSourceIndex)
    {
        switch (clockSignalSourceIndex) {
        case 0: {
            m_clockSignalSource = ClockSignalSource::Internal;
            break;
        }
        case 1: {
            m_clockSignalSource = ClockSignalSource::External;
            break;
        }
        default:
            m_clockSignalSource = ClockSignalSource::Internal;
        }
    }

    inline void setOperationMode(const quint8 &operationModeIndex)
    {
        switch (operationModeIndex) {
        case 0: {
            m_operationMode = AdcOperationMode::Normal;
            break;
        }
        case 1: {
            m_operationMode = AdcOperationMode::Debug;
            break;
        }
        default:
            m_operationMode = AdcOperationMode::Normal;
        }
    }

    inline void setDecimation(const quint8 &decimationIndex)
    {
        switch (decimationIndex) {
        case 0: {
            m_decimation = SignalDecimation::DecimationBy1;
            break;
        }
        case 1: {
            m_decimation = SignalDecimation::DecimationBy2;
            break;
        }
        case 2: {
            m_decimation = SignalDecimation::DecimationBy4;
            break;
        }
        case 3: {
            m_decimation = SignalDecimation::DecimationBy8;
            break;
        }
        case 4: {
            m_decimation = SignalDecimation::DecimationBy16;
            break;
        }
        case 5: {
            m_decimation = SignalDecimation::DecimationBy32;
            break;
        }
        case 6: {
            m_decimation = SignalDecimation::DecimationBy64;
            break;
        }
        case 7: {
            m_decimation = SignalDecimation::DecimationBy128;
            break;
        }
        default:
            m_decimation = SignalDecimation::DecimationBy1;
        }
    }

    inline void setFrequencyScale(const quint8 &frequencyScaleIndex)
    {
        switch (frequencyScaleIndex) {
        case 0: {
            m_frequencyScale = IntenernalStartSourceScale::Frequency_1kHz;
            break;
        }
        case 1: {
            m_frequencyScale = IntenernalStartSourceScale::Frequency_2kHz;
            break;
        }
        default:
            m_frequencyScale = IntenernalStartSourceScale::Frequency_1kHz;
        }
    }

    inline void setStartSignalSource(const quint8 &startSignalSourceIndex)
    {
        switch (startSignalSourceIndex) {
        case 0: {
            m_startSignalSource = StartSignalSource::Internal;
            break;
        }
        case 1: {
            m_startSignalSource = StartSignalSource::External;
            break;
        }
        default:
            m_startSignalSource = StartSignalSource::Internal;
        }
    }

    inline quint8 getRxEnableIndex()
    {
        switch (m_rxEnable) {
        case RxEnable::Disabled: {
            return 0;
        }
        case RxEnable::Enabled: {
            return 1;
        }
        default:
            return 0;
        }
    }

    inline quint8 getClockSignalSourceIndex()
    {
        switch (m_clockSignalSource) {
        case ClockSignalSource::Internal: {
            return 0;
        }
        case ClockSignalSource::External: {
            return 1;
        }
        default:
            return 0;
        }
    }

    inline quint8 getOperationModeIndex()
    {
        switch (m_operationMode) {
        case AdcOperationMode::Normal: {
            return 0;
        }
        case AdcOperationMode::Debug: {
            return 1;
        }
        default:
            return 0;
        }
    }

    inline quint8 getDecimationIndex()
    {
        switch (m_decimation) {
        case SignalDecimation::DecimationBy1: {
            return 0;
        }
        case SignalDecimation::DecimationBy2: {
            return 1;
            break;
        }
        case SignalDecimation::DecimationBy4: {
            return 2;
        }
        case SignalDecimation::DecimationBy8: {
            return 3;
        }
        case SignalDecimation::DecimationBy16: {
            return 4;
        }
        case SignalDecimation::DecimationBy32: {
            return 5;
        }
        case SignalDecimation::DecimationBy64: {
            return 6;
        }
        case SignalDecimation::DecimationBy128: {
            return 7;
        }
        default:
            return 0;
        }
    }

    inline quint8 getFrequencyScaleIndex()
    {
        switch (m_frequencyScale) {
        case IntenernalStartSourceScale::Frequency_1kHz: {
            return 0;
        }
        case IntenernalStartSourceScale::Frequency_2kHz: {
            return 1;
        }
        default:
            return 0;
        }
    }

    inline quint8 getStartSignalSourceIndex()
    {
        switch (m_startSignalSource) {
        case StartSignalSource::Internal: {
            return 0;
        }
        case StartSignalSource::External: {
            return 1;
        }
        default:
            return 0;
        }
    }
};

enum struct TriggerOut
{
    Enable                          =   0b0,
    Disable                         =   0b1
};

enum struct StrobeSize
{
    Strobe_A32_B2016                =   0b000,
    Strobe_A64_B1984                =   0b001,
    Strobe_A128_B1920               =   0b010,
    Strobe_A256_B1792               =   0b011,
};

struct GreenBoardControl2
{
    IntenernalStartSourceScale m_frequencyScale;
    TriggerOut m_triggerOut;
    StrobeSize m_strobeSize;

    GreenBoardControl2()
    {
        m_frequencyScale = IntenernalStartSourceScale(0);
        m_triggerOut = TriggerOut(0);
        m_strobeSize = StrobeSize(0);
    }

    GreenBoardControl2(const IntenernalStartSourceScale &frecuencyScale,
                       const TriggerOut &triggerOut, const StrobeSize &strobeSize)
    {
        m_frequencyScale = frecuencyScale;
        m_triggerOut = triggerOut;
        m_strobeSize = strobeSize;
    }

    inline QByteArray greenBoardControl2()
    {
        return greenBoardControl2(m_frequencyScale, m_triggerOut, m_strobeSize);
    }

    inline static QByteArray greenBoardControl2(const IntenernalStartSourceScale &frecuencyScale,
                                                const TriggerOut &triggerOut, const StrobeSize &strobeSize)
    {
        QByteArray controlMessage;
        controlMessage.append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::Control2));
        controlMessage.append(static_cast<unsigned char>((static_cast<quint8>(strobeSize) << 4) |
                                                         (static_cast<quint8>(triggerOut) << 3) |
                                                         (static_cast<quint8>(frecuencyScale))));
        return controlMessage;
    }

    inline void setFrequencyScale(const quint8 &frequencyScaleIndex)
    {
        switch (frequencyScaleIndex) {
        case 0: {
            m_frequencyScale = IntenernalStartSourceScale::Frequency_1kHz;
            break;
        }
        case 1: {
            m_frequencyScale = IntenernalStartSourceScale::Frequency_2kHz;
            break;
        }
        case 2: {
            m_frequencyScale = IntenernalStartSourceScale::Frequency_3kHz;
            break;
        }
        case 3: {
            m_frequencyScale = IntenernalStartSourceScale::Frequency_4kHz;
            break;
        }
        default:
            m_frequencyScale = IntenernalStartSourceScale::Frequency_1kHz;
        }
    }

    inline void setTriggerOut(const quint8 &triggerOutIndex)
    {
        switch (triggerOutIndex) {
        case 0: {
            m_triggerOut = TriggerOut::Enable;
            break;
        }
        case 1: {
            m_triggerOut = TriggerOut::Disable;
            break;
        }
        default:
            m_triggerOut = TriggerOut::Disable;
        }
    }

    inline void setStrobeSize(const quint8 &fstrobeSizeIndex)
    {
        switch (fstrobeSizeIndex) {
        case 0: {
            m_strobeSize = StrobeSize::Strobe_A32_B2016;
            break;
        }
        case 1: {
            m_strobeSize = StrobeSize::Strobe_A64_B1984;
            break;
        }
        case 2: {
            m_strobeSize = StrobeSize::Strobe_A128_B1920;
            break;
        }
        case 3: {
            m_strobeSize = StrobeSize::Strobe_A256_B1792;
            break;
        }
        default:
            m_strobeSize = StrobeSize::Strobe_A32_B2016;
        }
    }

    inline quint8 getFrequencyScaleIndex()
    {
        switch (m_frequencyScale) {
        case IntenernalStartSourceScale::Frequency_1kHz: {
            return 0;
        }
        case IntenernalStartSourceScale::Frequency_2kHz: {
            return 1;
        }
        case IntenernalStartSourceScale::Frequency_3kHz: {
            return 2;
        }
        case IntenernalStartSourceScale::Frequency_4kHz: {
            return 3;
        }
        default:
            return 0;
        }
    }

    inline quint8 getTriggerOut()
    {
        switch (m_triggerOut) {
        case TriggerOut::Enable: {
            return 0;
        }
        case TriggerOut::Disable: {
            return 1;
        }
        default:
            return 0;
        }
    }

    inline quint8 getStrobeSize()
    {
        switch (m_strobeSize) {
        case StrobeSize::Strobe_A32_B2016: {
            return 0;
        }
        case StrobeSize::Strobe_A64_B1984: {
            return 1;
        }
        case StrobeSize::Strobe_A128_B1920: {
            return 2;
        }
        case StrobeSize::Strobe_A256_B1792: {
            return 3;
        }
        default:
            return 0;
        }
    }

    inline quint16 getAdcChannelALength()
    {
        switch (m_strobeSize) {
        case StrobeSize::Strobe_A32_B2016: {
            return 32;
        }
        case StrobeSize::Strobe_A64_B1984: {
            return 64;
        }
        case StrobeSize::Strobe_A128_B1920: {
            return 128;
        }
        case StrobeSize::Strobe_A256_B1792: {
            return 256;
        }
        default:
            return 32;
        }
    }
};

struct GreenBoardAdcDelay
{
    quint8 m_adcNumber;
    quint16 m_delayValue;

    GreenBoardAdcDelay(const quint8 &adcNumber = 0)
    {
        m_delayValue = 0;
        m_adcNumber = adcNumber;
    }

    GreenBoardAdcDelay(const quint16 &delayValue, const quint8 &adcNumber = 0)
    {
        m_delayValue = delayValue;
        m_adcNumber = adcNumber;
    }

    inline QList<QByteArray> greenBoardAdcDelay()
    {
        return greenBoardAdcDelay(m_delayValue, m_adcNumber);
    }

    inline static QList<QByteArray> greenBoardAdcDelay(const quint16 &delayValue, const quint8 &adcNumber = 0)
    {
        QList<QByteArray> delayMessage;
        delayMessage.append(QByteArray());
        delayMessage.append(QByteArray());
        switch (adcNumber) {
        case 0: {
            delayMessage[0].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_A_Delay_0));
            delayMessage[1].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_A_Delay_1));
            break;
        }
        case 1: {
            delayMessage[0].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_B_Delay_0));
            delayMessage[1].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_B_Delay_1));
            break;
        }
        default : {
            delayMessage.append(QByteArray());
            delayMessage.append(QByteArray());
            delayMessage[0].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_A_Delay_0));
            delayMessage[1].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_A_Delay_1));
            delayMessage[2].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_B_Delay_0));
            delayMessage[3].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_B_Delay_1));
            delayMessage[2].append(static_cast<quint8>(delayValue & 0b11111111));
            delayMessage[3].append(static_cast<quint8>((delayValue >> 8) & 0b11111111));
        }
        }
        delayMessage[0].append(static_cast<quint8>(delayValue & 0b11111111));
        delayMessage[1].append(static_cast<quint8>((delayValue >> 8) & 0b11111111));
        return delayMessage;
    }

    inline void setAdcNumber(const quint8 &adcNumber)
    {
        m_adcNumber = adcNumber;
    }

    inline void setDelayValue(const quint16 &delayValue)
    {
        m_delayValue = delayValue;
    }
};

enum struct AdcControlWordAddressBytes
{
    DeviceIndex                 =   0x05,
    OutputMode                  =   0x14,
    OutputDelay                 =   0x17,
    VREFSelect                  =   0x18
};

enum struct AdcControlWordDataBytes
{
    VREFSelectRightValue        =   0x00,
    OutBDisabledStepOne         =   0x02,
    OutBDisabledStepThree       =   0x03,
    CMOSInterleavedOutputMode   =   0x21,
    OutputDelayRightValue       =   0x23,
    OutBDisabledStepTwo         =   0x31
};

struct GreenBoardAdcControlWord
{
    unsigned char m_addressByte;
    unsigned char m_dataByte;

    GreenBoardAdcControlWord()
    {
        m_addressByte = 0;
        m_dataByte = 0;
    }

    GreenBoardAdcControlWord(const AdcControlWordAddressBytes &address, const AdcControlWordDataBytes &data)
    {
        m_addressByte = static_cast<unsigned char>(address);
        m_dataByte = static_cast<unsigned char>(data);
    }

    inline QByteArray greenBoardAdcControlWord()
    {
        QByteArray bytes;
        bytes.append(m_addressByte);
        bytes.append(m_dataByte);
        return bytes;
    }

    inline static QByteArray greenBoardAdcControlWord(const AdcControlWordAddressBytes &address, const AdcControlWordDataBytes &data)
    {
        QByteArray bytes;
        bytes.append(static_cast<unsigned char>(address));
        bytes.append(static_cast<unsigned char>(data));
        return bytes;
    }

    inline QList<QByteArray> greenBoardAdcControlWordMessage()
    {
        QList<QByteArray> adcControlWordMessage;
        adcControlWordMessage.append(QByteArray());
        adcControlWordMessage.append(QByteArray());
        adcControlWordMessage[0].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_ControlWord_0));
        adcControlWordMessage[1].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_ControlWord_1));
        adcControlWordMessage[0].append(m_addressByte);
        adcControlWordMessage[1].append(m_dataByte);
        return adcControlWordMessage;
    }

    inline static QList<QByteArray> greenBoardAdcControlWordMessage(const AdcControlWordAddressBytes &address, const AdcControlWordDataBytes &data)
    {
        GreenBoardAdcControlWord adcControlWord(address, data);
        QList<QByteArray> adcControlWordMessage;
        adcControlWordMessage.append(QByteArray());
        adcControlWordMessage.append(QByteArray());
        adcControlWordMessage[0].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_ControlWord_0));
        adcControlWordMessage[1].append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::ADC_ControlWord_1));
        adcControlWordMessage[0].append(adcControlWord.m_addressByte);
        adcControlWordMessage[1].append(adcControlWord.m_dataByte);
        return adcControlWordMessage;
    }

    inline static QList<QByteArray> FullAdcInitMessage()
    {
        QList<QByteArray> adcInitMessage;
        adcInitMessage.append(SetCMOSInterleavedOutputMode());
        adcInitMessage.append(SetADCDataDelay());
        adcInitMessage.append(SetVREFSelect());
        adcInitMessage.append(SetOutBDisabledStepOne());
        adcInitMessage.append(SetOutBDisabledStepTwo());
        adcInitMessage.append(SetOutBDisabledStepThree());
        return adcInitMessage;
    }

    inline static QList<QByteArray> SetCMOSInterleavedOutputMode()
    {
        return greenBoardAdcControlWordMessage(AdcControlWordAddressBytes::OutputMode, AdcControlWordDataBytes::CMOSInterleavedOutputMode);
    }

    inline static QList<QByteArray> SetADCDataDelay()
    {
        return greenBoardAdcControlWordMessage(AdcControlWordAddressBytes::OutputDelay, AdcControlWordDataBytes::OutputDelayRightValue);
    }

    inline static QList<QByteArray> SetVREFSelect()
    {
        return greenBoardAdcControlWordMessage(AdcControlWordAddressBytes::VREFSelect, AdcControlWordDataBytes::VREFSelectRightValue);
    }

    inline static QList<QByteArray> SetOutBDisabledStepOne()
    {
        return greenBoardAdcControlWordMessage(AdcControlWordAddressBytes::DeviceIndex, AdcControlWordDataBytes::OutBDisabledStepOne);
    }

    inline static QList<QByteArray> SetOutBDisabledStepTwo()
    {
        return greenBoardAdcControlWordMessage(AdcControlWordAddressBytes::OutputMode, AdcControlWordDataBytes::OutBDisabledStepTwo);
    }

    inline static QList<QByteArray> SetOutBDisabledStepThree()
    {
        return greenBoardAdcControlWordMessage(AdcControlWordAddressBytes::DeviceIndex, AdcControlWordDataBytes::OutBDisabledStepThree);
    }
};

//struct GreenBoardLVTTLLines
//{
//    bool m_line0;
//    bool m_line1;
//    bool m_line2;

//    GreenBoardLVTTLLines()
//    {
//        m_line0 = 0;
//        m_line1 = 0;
//        m_line2 = 0;
//    }

//    GreenBoardLVTTLLines(const bool &line0, const bool &line1, const bool &line2)
//    {
//        m_line0 = line0;
//        m_line1 = line1;
//        m_line2 = line2;
//    }

//    inline QByteArray greenBoardLVTTLLines()
//    {
//        QByteArray message;
//        message.append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::LVTTL_OutLines));
//        message.append((static_cast<quint8>(m_line2) << 2) | (static_cast<quint8>(m_line1) < 1) | static_cast<quint8>(m_line0));
//        return message;
//    }

//    inline static QByteArray greenBoardLVTTLLines(const bool &line0, const bool &line1, const bool &line2)
//    {
//        QByteArray message;
//        message.append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::LVTTL_OutLines));
//        message.append((static_cast<quint8>(line2) << 2) | (static_cast<quint8>(line1) < 1) | static_cast<quint8>(line0));
//        return message;
//    }
//};

struct GreenBoardLED
{
    bool m_enable;

    GreenBoardLED()
    {
        m_enable = 0;
    }

    GreenBoardLED(const bool &enable)
    {
        m_enable = enable;
    }

    inline QByteArray greenBoardLED()
    {
        QByteArray message;
        message.append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::LED));
        message.append(static_cast<quint8>(m_enable));
        return message;
    }

    inline static QByteArray greenBoardLED(const bool &enable)
    {
        QByteArray message;
        message.append(GreenBoardAddress::greenBoardAddress(GreenBoardAddressBytes::LED));
        message.append(static_cast<quint8>(enable));
        return message;
    }
};

enum struct MotorSpiAddressBytes
{
    Control                     =   0x01,
    Undefined                   =   0x04,
    VelocityLow                 =   0x10,
    VelocityHigh                =   0x11,
    AngleActual                 =   0x20,
    AngleDest1                  =   0x30,
    AngleDest2                  =   0x31
};

enum struct MotorModBytes
{
    PowerOff                    =   0x00,
    ContionusRotation           =   0x01,
    RotateToAngle               =   0x02,
    RotateBetwenAngles          =   0x03,
    RotateDifferentSpeed        =   0x04,
    Break                       =   0x05
};

enum struct MotorDirection
{
    ClockWise                   =   0x0,
    CounterClockWise            =   0x1
};

struct MotorControl
{
    MotorDirection m_direction;
    MotorModBytes m_mod;
    qint16 m_velocity1;
    qint16 m_velocity2;
    quint16 m_angle1;
    quint16 m_angle2;

    MotorSpiAddressBytes m_currentSpiAddress;

    MotorControl()
    {
        m_direction = MotorDirection::ClockWise;
        m_mod = MotorModBytes::PowerOff;
        m_velocity1 = 10000;
        m_velocity2 = 10000;
        m_angle1 = 0;
        m_angle2 = 0;

        m_currentSpiAddress = MotorSpiAddressBytes::Control;
    }

    void setMod(const MotorModBytes &motorModBytes)
    {
        m_mod = motorModBytes;
    }

    void setDirection(const MotorDirection &motorDirection)
    {
        m_direction = motorDirection;
    }

    void setVelocity1(const qint32 &velocity)
    {
        m_velocity1 = velocity;
    }

    void setVelocity2(const qint32 &velocity)
    {
        m_velocity2 = velocity;
    }

    void setAngle1(const double &angle)
    {
        m_angle1 = static_cast<quint16>((angle/360.0)*g_maxAngleSensorValue);
    }

    void setAngle2(const double &angle)
    {
        m_angle2 = static_cast<quint16>((angle/360.0)*g_maxAngleSensorValue);
    }

    void plusAddressCounter()
    {
        switch (m_currentSpiAddress) {
        case MotorSpiAddressBytes::Control : {
            m_currentSpiAddress = MotorSpiAddressBytes::Undefined;
            break;
        }
        case MotorSpiAddressBytes::Undefined : {
            m_currentSpiAddress = MotorSpiAddressBytes::VelocityLow;
            break;
        }
        case MotorSpiAddressBytes::VelocityLow : {
            m_currentSpiAddress = MotorSpiAddressBytes::VelocityHigh;
            break;
        }
        case MotorSpiAddressBytes::VelocityHigh : {
            m_currentSpiAddress = MotorSpiAddressBytes::AngleDest1;
            break;
        }
        case MotorSpiAddressBytes::AngleDest1 : {
            m_currentSpiAddress = MotorSpiAddressBytes::AngleDest2;
            break;
        }
        case MotorSpiAddressBytes::AngleDest2 : {
            m_currentSpiAddress = MotorSpiAddressBytes::Control;
            break;
        }
        default : {
            m_currentSpiAddress = MotorSpiAddressBytes::VelocityLow;
            break;
        }
        }
    }

    void minusAddressCounter()
    {
        switch (m_currentSpiAddress) {
        case MotorSpiAddressBytes::Control : {
            m_currentSpiAddress = MotorSpiAddressBytes::Undefined;
            break;
        }
        case MotorSpiAddressBytes::Undefined : {
            m_currentSpiAddress = MotorSpiAddressBytes::AngleDest2;
            break;
        }
        case MotorSpiAddressBytes::VelocityLow : {
            m_currentSpiAddress = MotorSpiAddressBytes::Control;
            break;
        }
        case MotorSpiAddressBytes::VelocityHigh : {
            m_currentSpiAddress = MotorSpiAddressBytes::VelocityLow;
            break;
        }
        case MotorSpiAddressBytes::AngleDest1 : {
            m_currentSpiAddress = MotorSpiAddressBytes::VelocityHigh;
            break;
        }
        case MotorSpiAddressBytes::AngleDest2 : {
            m_currentSpiAddress = MotorSpiAddressBytes::AngleDest1;
            break;
        }
        default : {
            m_currentSpiAddress = MotorSpiAddressBytes::VelocityLow;
            break;
        }
        }
    }

    QByteArray getControlMessage()
    {
        QByteArray message;
        message.append(static_cast<quint8>(MotorSpiAddressBytes::Control));
        quint16 messageBytes = static_cast<quint16>(m_direction) << 3;
        messageBytes += static_cast<quint16>(m_mod);
        message.append(((messageBytes) & 0b1111111100000000) >> 8);
        message.append((messageBytes) & 0b11111111);
        return message;
    }

    QByteArray getUndefinedMessage()
    {
        QByteArray message;
        message.append(static_cast<quint8>(MotorSpiAddressBytes::Undefined));
        quint16 messageBytes = static_cast<quint16>(m_direction) << 3;
        messageBytes += static_cast<quint16>(m_mod);
        message.append(((messageBytes) & 0b1111111100000000) >> 8);
        message.append((messageBytes) & 0b11111111);
        return message;
    }

    QByteArray getVelocity1Message()
    {
        QByteArray message;
        message.append(static_cast<quint8>(MotorSpiAddressBytes::VelocityLow));
        quint16 messageBytes = static_cast<quint16>(m_velocity1);
        message.append(((messageBytes) & 0b1111111100000000) >> 8);
        message.append((messageBytes) & 0b11111111);
        return message;
    }

    QByteArray getVelocity2Message()
    {
        QByteArray message;
        message.append(static_cast<quint8>(MotorSpiAddressBytes::VelocityHigh));
        quint16 messageBytes = static_cast<quint16>(m_velocity2);
        message.append(((messageBytes) & 0b1111111100000000) >> 8);
        message.append((messageBytes) & 0b11111111);
        return message;
    }

    QByteArray getAngleDest1Message()
    {
        QByteArray message;
        message.append(static_cast<quint8>(MotorSpiAddressBytes::AngleDest1));
        quint16 messageBytes = static_cast<quint16>(m_angle1);
        message.append(((messageBytes) & 0b1111111100000000) >> 8);
        message.append((messageBytes) & 0b11111111);
        return message;
    }

    QByteArray getAngleDest2Message()
    {
        QByteArray message;
        message.append(static_cast<quint8>(MotorSpiAddressBytes::AngleDest2));
        quint16 messageBytes = static_cast<quint16>(m_angle2);
        message.append(((messageBytes) & 0b1111111100000000) >> 8);
        message.append((messageBytes) & 0b11111111);
        return message;
    }

    QByteArray getCurrentMessage()
    {
        switch (m_currentSpiAddress) {
        case MotorSpiAddressBytes::Control : {
            return getControlMessage();
        }
        case MotorSpiAddressBytes::Undefined : {
            return getUndefinedMessage();
        }
        case MotorSpiAddressBytes::VelocityLow : {
            return getVelocity1Message();
        }
        case MotorSpiAddressBytes::VelocityHigh : {
            return getVelocity2Message();
        }
        case MotorSpiAddressBytes::AngleDest1 : {
            return getAngleDest1Message();
        }
        case MotorSpiAddressBytes::AngleDest2 : {
            return getAngleDest2Message();
        }
        default : {
            return getControlMessage();
        }
        }
    }

    QList<QByteArray> getMessageList() {
        QList<QByteArray> messageList;
        messageList.append(getVelocity1Message());
        messageList.append(getVelocity2Message());
        messageList.append(getAngleDest1Message());
        messageList.append(getAngleDest2Message());
        messageList.append(getUndefinedMessage());
        messageList.append(getControlMessage());

        return messageList;
    }
};

enum struct GreenBoardErrorCode
{
    NoError                 =   0,
    AnswerMissed            =   1,
    Timeout                 =   2
};

enum struct GreenBoardIniSettingsGroup
{
    FirstInitialization     =   0,
    SecondInitialization    =   1,
    FullInitialization      =   2,
    ScheduleParameters      =   3
};

#endif // GREENBOARDSTRUCT_H
