/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "ledindicator.h"
#include <QPainter>

LedIndicator::LedIndicator(QWidget *parent) :
    QWidget(parent)
{
    setFixedSize(20, 20);
    ledState = LedState::LedOff;
    ledOnColor = Qt::green;
    ledOffColor = Qt::white;
    ledWaitColor = Qt::yellow;
    ledErrorColor = Qt::red;
    ledoffRedColor = Qt::red;
    ledOnPattern = Qt::SolidPattern;
    ledOffPattern = Qt::SolidPattern;
    ledWaitPattern = Qt::SolidPattern;
    ledErrorPattern = Qt::SolidPattern;
    ledoffRedPattern = Qt::SolidPattern;
    ledSize = 10;
}

void LedIndicator::paintEvent(QPaintEvent *) {
    QPainter p(this);
    switch (ledState) {
    case LedState::LedOff : {
        p.setBrush(QBrush(ledOffColor, ledOffPattern));
        break;
    }
    case LedState::LedOn : {
        p.setBrush(QBrush(ledOnColor, ledOnPattern));
        break;
    }
    case LedState::LedWait : {
        p.setBrush(QBrush(ledWaitColor, ledWaitPattern));
        break;
    }
    case LedState::LedError : {
        p.setBrush(QBrush(ledErrorColor, ledErrorPattern));
        break;
    } case LedState::LedOffRed : {
        p.setBrush(QBrush(ledoffRedColor, ledoffRedPattern));
        break;
    }
    default : {
        p.setBrush(QBrush(ledOffColor, ledOffPattern));
        break;
    }
    }
    p.drawEllipse(width()/2 - ledSize/2, height()/2 - ledSize/2, ledSize, ledSize);
}

void LedIndicator::switchLedIndicator() {
    switch (ledState) {
    case LedState::LedOff : {
        ledState = LedState::LedOn;
        break;
    }
    case LedState::LedOn : {
        ledState = LedState::LedOff;
        break;
    }
    default: {
        break;
    }
    }
    repaint();
}

void LedIndicator::setLedIndicatorState(LedState state)
{
    ledPastState = ledState;
    ledState = state;
    repaint();
}
void LedIndicator::setState(LedState state)
{
    ledPastState = ledState;
    ledState = state;
    repaint();
}
void LedIndicator::toggle()
{
    switch (ledState) {
    case LedState::LedOff : {
        ledState = LedState::LedOn;
        break;
    }
    case LedState::LedOn : {
        ledState = LedState::LedOff;
        break;
    }
    default: {
        break;
    }
    }
    repaint();
}

void LedIndicator::setOnColor(QColor onColor)
{
    ledOnColor=onColor;
    repaint();
}
void LedIndicator::setOffColor(QColor offColor)
{
    ledOffColor=offColor;
    repaint();
}

void LedIndicator::setWailColor(QColor waitColor)
{
    ledWaitColor = waitColor;
}

void LedIndicator::setErrorColor(QColor errorColor)
{
    ledErrorColor = errorColor;
}

void LedIndicator::setOffRed(QColor offRedColor)
{
    ledoffRedColor = offRedColor;
}

void LedIndicator::setOnPattern(Qt::BrushStyle onPattern)
{
    ledOnPattern=onPattern;
    repaint();
}
void LedIndicator::setOffPattern(Qt::BrushStyle offPattern)
{
    ledOffPattern=offPattern;
    repaint();
}

void LedIndicator::setWaitPattern(Qt::BrushStyle waitPattern)
{
    ledWaitPattern = waitPattern;
    repaint();
}

void LedIndicator::setErrorPattern(Qt::BrushStyle errorPattern)
{
    ledErrorPattern = errorPattern;
    repaint();
}

void LedIndicator::setOffRedPattern(Qt::BrushStyle offRedPattern)
{
    ledoffRedPattern = offRedPattern;
}

void LedIndicator::setLedSize(int size)
{
    ledSize=size;
    setFixedSize(size + 10, size + 10);
    repaint();
}
