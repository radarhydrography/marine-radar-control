/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef INIREADER_H
#define INIREADER_H

#include <QString>
#include <QMap>
#include <QFile>

enum struct SettingsGroup
{
    AllSettings,
    GreenBoardSettings,
    RadarEthernetSettings,
    RadarSerialSettings,
    PlotSettings,
    DiagramPlotSettings,
    SignalPlotSettings,
    CalibrationPlotSettings,
    TuningPlotSetting,
    GreenBoardScheduleSettings
};

enum struct GraphicModuleSetSettings
{
    InicializationMode,
    SetupMode
};

enum struct GraphicsModuleGetSettings
{
    GetSignalPlotSettings,
    GetDiagramSettings,
    GetCalibrationPlotSettings,
    GetTuningPlotSettings
};

class IniReader
{
    QString *m_defaultFileName;
    QMap<QString, QString> *m_data;

public:

    IniReader();

    void readConfigurationFile();

    static QMap<QString, QString> readConfigurationFile(const QString &name);
    static QMap<QString, QString> readBasicConfigurationFile(const QString &name);
    static QMap<QString, QString> defaultConfiguration();

    static void checkConfiguration(QMap<QString, QString> &config, const SettingsGroup &settingsGroup = SettingsGroup::AllSettings);
    static bool checkBasicConfiguration(QMap<QString, QString> &config);

    static void saveConfiguration(QMap<QString, QString> &config, const QString &fileName);
    static void saveBasicConfiguration(QMap<QString, QString> &config, const QString &fileName);
    static void saveRedirectionLink(const QString &fileName);

private:

    static QMap<QString, QPair<long, long> > referenceValues();
};

#endif // INIREADER_H
