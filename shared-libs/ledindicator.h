/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef LEDINDICATOR_H
#define LEDINDICATOR_H

#include <QWidget>

enum struct LedState
{
    LedOff      =   0,
    LedOn       =   1,
    LedWait     =   2,
    LedError    =   3,
    LedOffRed   =   4
};

class LedIndicator: public QWidget {
    Q_OBJECT
  public:
    LedIndicator(QWidget *parent = 0);
    void setState(LedState state);
    void toggle();
    void setOnColor(QColor onColor);
    void setOffColor(QColor offColor);
    void setWailColor(QColor waitColor);
    void setErrorColor(QColor errorColor);
    void setOffRed(QColor offRedColor);
    void setOnPattern(Qt::BrushStyle onPattern);
    void setOffPattern(Qt::BrushStyle offPattern);
    void setWaitPattern(Qt::BrushStyle waitPattern);
    void setErrorPattern(Qt::BrushStyle errorPattern);
    void setOffRedPattern(Qt::BrushStyle offRedPattern);
    void setLedSize(int size);
    LedState getLedState() {return ledState;}

  public slots:
    void switchLedIndicator();
    void setLedIndicatorState(LedState state);
  protected:
    void paintEvent(QPaintEvent *);
  private:
    QColor ledOnColor;
    QColor ledOffColor;
    QColor ledWaitColor;
    QColor ledErrorColor;
    QColor ledoffRedColor;
    Qt::BrushStyle ledOnPattern;
    Qt::BrushStyle ledOffPattern;
    Qt::BrushStyle ledWaitPattern;
    Qt::BrushStyle ledErrorPattern;
    Qt::BrushStyle ledoffRedPattern;
    int ledSize;
    LedState ledPastState;
    LedState ledState;
};

#endif // LEDINDICATOR_H
