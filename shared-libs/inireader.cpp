/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "inireader.h"
#include <QTextStream>
#include <QDir>
#include "shared-libs/global.h"

IniReader::IniReader()
{
    m_defaultFileName = new QString("settings.ini");

    m_data = new QMap<QString, QString>;
}

void IniReader::readConfigurationFile()
{
    QMap<QString, QString> data = readConfigurationFile(*m_defaultFileName);
    m_data = &data;
}

QMap<QString, QString> IniReader::readConfigurationFile(const QString &name)
{
    QFile file(name);
    QMap<QString, QString> data = defaultConfiguration();
    if (file.open(QFile::ReadOnly)) {
        QByteArray fileByteArray(file.readAll());
        file.close();
        QTextStream fileTextStream(fileByteArray);
        QString line;
        do {
            line = fileTextStream.readLine();
            line.remove(' ');
            line.remove(';');
            if (!(line == "" || line[0] == "#")) {
                QStringList fileStringList = line.mid(0, line.indexOf("#")).split(QString("="));
                if (fileStringList.size() == 2 && fileStringList[1] != "" && data.contains(fileStringList[0])) {
                    data[fileStringList[0]] = fileStringList[1];
                }
            }
        } while (!line.isNull());
    }
    return data;
}

QMap<QString, QString> IniReader::readBasicConfigurationFile(const QString &name)
{
    QFile file(name);
    QMap<QString, QString> data;
    if (file.open(QFile::ReadOnly)) {
        QByteArray fileByteArray(file.readAll());
        file.close();
        QTextStream fileTextStream(fileByteArray);
        QString line;
        do {
            line = fileTextStream.readLine();
            line.remove(';');
            if (!(line == "" || line[0] == "#")) {
                QStringList fileStringList = line.mid(0, line.indexOf("#")).split(QString("="));
                if (fileStringList.size() == 2 && fileStringList[1] != "" ) {
                    data[fileStringList[0]] = fileStringList[1];
                }
            }
        } while (!line.isNull());
    }

    // Just additional part to be sure that logs folder always exists and never full
    QDir dir(data[QString("logs")]);
    QString defaultLogsPath(QString(g_tmpPath + g_pathDevider + "logs"));
    if (data[QString("logs")] == QString("")) {
        data[QString("logs")] = defaultLogsPath;
    } else if (dir.exists()) {
        QStringList fileList;
        fileList.append(dir.entryList(QStringList("*.radarlog")));
        if (fileList.size() >= g_maxLogFilesNumber) {
            for (int fileNumber = 0; fileNumber <= (fileList.size() - g_maxLogFilesNumber); fileNumber++) {
                QString filePath = data[QString("logs")] + g_pathDevider + fileList[fileNumber];
                QFile::moveToTrash(filePath);
            }
        }
    }
    if (data[QString("logs")] == defaultLogsPath) {
        if (!dir.exists(defaultLogsPath)) {
            dir.mkdir(defaultLogsPath);
        }
    }

    return data;
}

QMap<QString, QString> IniReader::defaultConfiguration()
{
    return QMap<QString, QString> ({
                                        {"CalibrationMode", "0"}
                                       ,{"RadarControlInterface", "0"}
                                       ,{"DefaultPlotType", "0"}
                                       ,{"DarkMode", "0"}

                                       ,{"MaxAvgFactor", "1"}
                                       ,{"AngleBitOffset", "7"}
                                       ,{"RangeAvgCoef", "4"}
                                       ,{"MaxPlottingFrequency", "25"}
                                       ,{"Brightness", "100"}
                                       ,{"Contrast", "100"}
                                       ,{"Threshold", "0"}
                                       ,{"ColorMode", "0"}

                                       ,{"SignalPlotLogScale", "1"}
                                       ,{"SignalPlotAbs", "1"}
                                       ,{"SignalPlotRePlot", "0"}
                                       ,{"SignalPlotImPlot", "0"}
                                       ,{"SignalPlotLimitLowerLog", "-40.0"}
                                       ,{"SignalPlotLimitUpperLog", "0.0"}
                                       ,{"SignalPlotLimitLowerLin", "-1.0"}
                                       ,{"SignalPlotLimitUpperLin", "1.0"}

                                       ,{"TuningPlotEnanbled", "0"}
                                       ,{"TuningPlotAvgLine", "0"}
                                       ,{"TuningPlotAvgNumber", "50"}
                                       ,{"TuningSignalAvgNumber", "1"}
                                       ,{"TuningSignalAbs", "1"}
                                       ,{"TuningSignalRe", "1"}
                                       ,{"TuningSignalIm", "1"}
                                       ,{"TuningPlotStartSample", "8"}
                                       ,{"TuningPlotStopSample", "0"}
                                       ,{"TuningSignalShowLimits", "0"}
                                       ,{"TuningSignalAnalysisDelay", "0"}
                                       ,{"TuningSignalCustomAnalysisDelay", "0"}

                                       ,{"CalibrationMaxPlottingFrequency", "15"}
                                       ,{"CalibrationAvgFactor", "1"}
                                       ,{"CalibrationAngleBitOffset", "2"}
                                       ,{"CalibrationRangeAvgCoef", "1"}
                                       ,{"ExactNumberOfAvg", "0"}
                                       ,{"CalibrationDefaultAvgCoefficient", "10"}

                                       ,{"SamplingFrequency", "80000000"}
                                       ,{"LightSpeed", "299792458"}
                                       ,{"FrameLength", "2048"}
                                       ,{"AdcBitRateWithSign", "14"}
                                       ,{"AdcMaxValue", "11900"}

                                       ,{"PcHostAddress", "192.168.0.1"}
                                       ,{"PcControlPort", "16416"}
                                       ,{"PcDataPort", "16386"}
                                       ,{"BoardHostAddress", "192.168.0.2"}
                                       ,{"BoardControlPort", "16480"}
                                       ,{"BoardDataPort", "16481"}

                                       ,{"BoardRxEnable", "1"}
                                       ,{"BoardClockSignalSource", "0"}
                                       ,{"BoardAdcOperationMode", "0"}
                                       ,{"BoardDecimation", "0"}
                                       ,{"BoardInrernalTriggerFrequency", "1"}
                                       ,{"BoardTriggerSignalSource", "0"}
                                       ,{"BoardAdcADelay", "0"}
                                       ,{"BoardAdcBDelay", "0"}
                                       ,{"BoardAdcDelaySynchro", "1"}
                                       ,{"BoardAngleOffset", "0"}
                                       ,{"SignalAvgCoefficient", "1"}

                                       ,{"DefaultFileName", "FileName"}
                                       ,{"StartSavingTreadAutomaticly", "0"}

                                       ,{"BoardTriggerFrequency", "1"}
                                       ,{"BoardAdcALength", "0"}
                                       ,{"MaxFileSize", "5"}
                                       ,{"FirstSavingDatagram", "0"}
                                       ,{"LastSavingDatagram", "7"}

                                       ,{"MotorControlAutoStart", "0"}

                                       ,{"FreeDiskSpaceWarning", "10"}
                                       ,{"FreeDiskSpaceStopSaving", "5"}
                                       ,{"ContinueSavingAfterPathChanged", "0"}

                                       ,{"BlindSectorOneEnabled", "0"}
                                       ,{"BlindSectorOneBegin", "0.0"}
                                       ,{"BlindSectorOneEnd", "0.0"}
                                       ,{"BlindSectorTwoEnabled", "0"}
                                       ,{"BlindSectorTwoBegin", "0.0"}
                                       ,{"BlindSectorTwoEnd", "0.0"}
                                       ,{"BlindSectorThreeEnabled", "0"}
                                       ,{"BlindSectorThreeBegin", "0.0"}
                                       ,{"BlindSectorThreeEnd", "0.0"}

                                       ,{"HostAddrressPC", "192.168.42.255"}
                                       ,{"ReadPortPC", "5004"}
                                       ,{"WritePortPC", "5005"}
                                       ,{"HostAddressRADAR", "192.168.42.33"}
                                       ,{"ReadPortRADAR", "5004"}
                                       ,{"WritePortRADAR", "5005"}

                                       ,{"RadarTriggerGeneratorStateUDP", "-1"}
                                       ,{"RadarTriggerPeriodUDP", "-1"}
                                       ,{"RadarTriggerStaggerValueUDP", "-1"}

                                       ,{"RadarTriggerDelayValueUDP", "-1"}

                                       ,{"RadarADCSampligFrequencyUDP", "-1"}

                                       ,{"RadarMiscZeroBlankMemComtrolUDP", "-1"}
                                       ,{"RadarMiscZeroTestACPUDP", "-1"}
                                       ,{"RadarMiscZeroAutoTuneFineUDP", "-1"}
                                       ,{"RadarMiscZeroAutoTuneRegUDP", "-1"}
                                       ,{"RadarMiscZeroDivideACPInputUDP", "-1"}

                                       ,{"RadarMiscOneVideoSampleResUDP", "-1"}
                                       ,{"RadarMiscOneAcpPeriodUDP", "-1"}
                                       ,{"RadarMiscOneBlankingRamUDP", "-1"}
                                       ,{"RadarMiscOneBandwithUDP", "-1"}

                                       ,{"RadarHlSetupWidthUDP", "-1"}
                                       ,{"RadarHlSetupInvertUDP", "-1"}
                                       ,{"RadarHlSetupEdgeUDP", "-1"}
                                       ,{"RadarHlSetupDelayValueUDP", "-1"}

                                       ,{"RadarEnableVideoAcquisitionUDP", "-1"}
                                       ,{"RadarEnableDoubleBufferUDP", "-1"}
                                       ,{"RadarEnableAutoTuneUDP", "-1"}
                                       ,{"RadarEnableDAConvertersUDP", "-1"}
                                       ,{"RadarEnableADConvertersUDP", "-1"}

                                       ,{"RadarTuneCoarseUDP", "-1"}
                                       ,{"RadarTuneFineUDP", "-1"}
                                       ,{"RadarTuneRegUDP", "-1"}

                                       ,{"RadarSetupTransceiverModeUDP", "-1"}
                                       ,{"RadarSetupPulseModeUDP", "-1"}
                                       ,{"RadarSetupEnableMotorUDP", "-1"}

                                       ,{"RadarGateTuneDelayValueUDP", "-1"}

                                       ,{"RadarAntSpeedControlUDP", "-1"}
                                       ,{"RadarAntSpeedValueUDP", "-1"}

                                       ,{"RadarMasterIPLowUDP", "-1"}

                                       ,{"RadarDebugNoMasterModeUDP", "-1"}
                                       ,{"RadarDebugTxTestModeUDP", "-1"}
                                       ,{"RadarDebugPowerUpModeUDP", "-1"}

                                       ,{"SerialPortName", "/dev/ttyUSB0"}
                                       ,{"SerialPortBaudRate", "0"}
                                       ,{"SerialPortDataBits", "0"}
                                       ,{"SerialPortStopBits", "0"}
                                       ,{"SerialPortParity", "0"}
                                       ,{"SerialPortFlowControl", "0"}
                                       ,{"SerialPortDirection", "0"}

                                       ,{"RadarTriggerGeneratorStateRS", "-1"}
                                       ,{"RadarTriggerPeriodRS", "-1"}
                                       ,{"RadarTriggerStaggerValueRS", "-1"}

                                       ,{"RadarMiscZeroTestACPRS", "-1"}
                                       ,{"RadarMiscZeroAutoTuneFineRS", "-1"}
                                       ,{"RadarMiscZeroAutoTuneRegRS", "-1"}
                                       ,{"RadarMiscZeroDivideACPInputRS", "-1"}

                                       ,{"RadarMiscOneAcpPeriodRS", "-1"}
                                       ,{"RadarMiscOneBlankingRamRS", "-1"}
                                       ,{"RadarMiscOneBandwithRS", "-1"}

                                       ,{"RadarHlSetupWidthRS", "-1"}
                                       ,{"RadarHlSetupInvertRS", "-1"}
                                       ,{"RadarHlSetupEdgeRS", "-1"}
                                       ,{"RadarHlSetupDelayValueRS", "-1"}

                                       ,{"RadarEnableAutoTuneRS", "-1"}
                                       ,{"RadarEnableDAConvertersRS", "-1"}
                                       ,{"RadarEnableADConvertersRS", "-1"}

                                       ,{"RadarTuneCoarseRS", "-1"}
                                       ,{"RadarTuneFineRS", "-1"}
                                       ,{"RadarTuneRegRS", "-1"}

                                       ,{"RadarSetupTransceiverModeRS", "-1"}
                                       ,{"RadarSetupPulseModeRS", "-1"}
                                       ,{"RadarSetupEnableMotorRS", "-1"}

                                       ,{"icons", "resources/icons/"}
                                       ,{"tr", "resources/tr/en.tr"}
                                       ,{"basicDataFoler", "/tmp/Radar_Control/"}

                                       ,{"", ""}

                                   });
}

QMap<QString, QPair<long, long>> IniReader::referenceValues()
{
    return QMap<QString, QPair<long, long>> (
                                                {
                                                     {"CalibrationMode", {0, 1}}
                                                    ,{"RadarControlInterface", {0, 1}}
                                                    ,{"DefaultPlotType", {0, 3}}
                                                    ,{"DarkMode", {0, 1}}

                                                    ,{"MaxAvgFactor", {1, 999999999999999999}}
                                                    ,{"AngleBitOffset", {0, 15}}
                                                    ,{"RangeAvgCoef", {1, 1024}}
                                                    ,{"MaxPlottingFrequency", {1, 1000}}
                                                    ,{"Brightness", {0, 1000}}
                                                    ,{"Contrast", {0, 1000}}
                                                    ,{"Threshold", {0, 100}}
                                                    ,{"ColorMode", {0, 11}}

                                                    ,{"SignalPlotLogScale", {0, 1}}
                                                    ,{"SignalPlotAbs", {0, 1}}
                                                    ,{"SignalPlotRePlot", {0, 1}}
                                                    ,{"SignalPlotImPlot", {0, 1}}
                                                    ,{"SignalPlotLimitLowerLog", {-120, 30}}
                                                    ,{"SignalPlotLimitUpperLog", {-120, 30}}
                                                    ,{"SignalPlotLimitLowerLin", {-1, 1}}
                                                    ,{"SignalPlotLimitUpperLin", {-1, 1}}

                                                    ,{"TuningPlotEnanbled", {0, 1}}
                                                    ,{"TuningPlotAvgLine", {0, 1}}
                                                    ,{"TuningPlotAvgNumber", {1, 1000000}}
                                                    ,{"TuningSignalAvgNumber", {1, 1000000}}
                                                    ,{"TuningSignalAbs", {0, 1}}
                                                    ,{"TuningSignalRe", {0, 1}}
                                                    ,{"TuningSignalIm", {0, 1}}
                                                    ,{"TuningPlotStartSample", {0, 255}}
                                                    ,{"TuningPlotStopSample", {0, 255}}
                                                    ,{"TuningSignalShowLimits", {0, 1}}
                                                    ,{"TuningSignalAnalysisDelay", {0, 1}}
                                                    ,{"TuningSignalCustomAnalysisDelay", {0, 2016}}

                                                    ,{"CalibrationMaxPlottingFrequency", {1, 1000}}
                                                    ,{"CalibrationAvgFactor", {1, 999999999999999999}}
                                                    ,{"CalibrationAngleBitOffset", {0, 15}}
                                                    ,{"CalibrationRangeAvgCoef", {1, 1024}}
                                                    ,{"ExactNumberOfAvg", {0, 1}}
                                                    ,{"CalibrationDefaultAvgCoefficient", {1, 9999}}

                                                    ,{"SamplingFrequency", {20000000, 10000000000}}
                                                    ,{"LightSpeed", {299792458, 299792458}}
                                                    ,{"FrameLength", {1, 999999999999999999}}
                                                    ,{"AdcBitRateWithSign", {1, 127}}
                                                    ,{"AdcMaxValue", {1, 9999999999999}}

                                                    ,{"PcControlPort", {0, 10000000000}}
                                                    ,{"PcDataPort", {0, 10000000000}}
                                                    ,{"BoardControlPort", {0, 10000000000}}
                                                    ,{"BoardDataPort", {0, 10000000000}}

                                                    ,{"BoardRxEnable", {0, 1}}
                                                    ,{"BoardClockSignalSource", {0, 1}}
                                                    ,{"BoardAdcOperationMode", {0, 1}}
                                                    ,{"BoardDecimation", {0, 7}}
                                                    ,{"BoardInrernalTriggerFrequency", {0, 1}}
                                                    ,{"BoardTriggerSignalSource", {0, 1}}
                                                    ,{"BoardAdcADelay", {0, 65535}}
                                                    ,{"BoardAdcBDelay", {0, 65535}}
                                                    ,{"BoardAdcDelaySynchro", {0, 1}}
                                                    ,{"BoardAngleOffset", {0, 360}}
                                                    ,{"SignalAvgCoefficient", {1, 2000}}

                                                    ,{"StartSavingTreadAutomaticly", {0, 1}}

                                                    ,{"BoardTriggerFrequency", {0, 3}}
                                                    ,{"BoardAdcALength", {0, 3}}
                                                    ,{"MaxFileSize", {0, 8}}
                                                    ,{"FirstSavingDatagram", {0, 7}}
                                                    ,{"LastSavingDatagram", {0, 7}}

                                                    ,{"MotorControlAutoStart", {0, 1}}

                                                    ,{"FreeDiskSpaceWarning", {0, 100}}
                                                    ,{"FreeDiskSpaceStopSaving", {0, 100}}
                                                    ,{"ContinueSavingAfterPathChanged", {0, 100}}

                                                    ,{"BlindSectorOneEnabled", {0, 1}}
                                                    ,{"BlindSectorOneBegin", {0, 360}}
                                                    ,{"BlindSectorOneEnd", {0, 360}}
                                                    ,{"BlindSectorTwoEnabled", {0, 1}}
                                                    ,{"BlindSectorTwoBegin", {0, 360}}
                                                    ,{"BlindSectorTwoEnd", {0, 360}}
                                                    ,{"BlindSectorThreeEnabled", {0, 1}}
                                                    ,{"BlindSectorThreeBegin", {0, 360}}
                                                    ,{"BlindSectorThreeEnd", {0, 360}}

                                                    ,{"ReadPortPC", {0, 10000000000}}
                                                    ,{"WritePortPC", {0, 10000000000}}
                                                    ,{"ReadPortRADAR", {0, 10000000000}}
                                                    ,{"WritePortRADAR", {0, 10000000000}}

                                                    ,{"RadarTriggerGeneratorStateUDP", {-1, 1}}
                                                    ,{"RadarTriggerPeriodUDP", {-1, 99999}}
                                                    ,{"RadarTriggerStaggerValueUDP", {-1, 15}}

                                                    ,{"RadarTriggerDelayValueUDP", {-1, 65535}}

                                                    ,{"RadarADCSampligFrequencyUDP", {-1, 2}}

                                                    ,{"RadarMiscZeroBlankMemComtrolUDP", {-1, 1}}
                                                    ,{"RadarMiscZeroTestACPUDP", {-1, 1}}
                                                    ,{"RadarMiscZeroAutoTuneFineUDP", {-1, 1}}
                                                    ,{"RadarMiscZeroAutoTuneRegUDP", {-1, 1}}
                                                    ,{"RadarMiscZeroDivideACPInputUDP", {-1, 3}}

                                                    ,{"RadarMiscOneVideoSampleResUDP", {-1, 3}}
                                                    ,{"RadarMiscOneAcpPeriodUDP", {-1, 2}}
                                                    ,{"RadarMiscOneBlankingRamUDP", {-1, 1}}
                                                    ,{"RadarMiscOneBandwithUDP", {-1, 1}}

                                                    ,{"RadarHlSetupWidthUDP", {-1, 3}}
                                                    ,{"RadarHlSetupInvertUDP", {-1, 1}}
                                                    ,{"RadarHlSetupEdgeUDP", {-1, 1}}
                                                    ,{"RadarHlSetupDelayValueUDP", {-1, 4095}}

                                                    ,{"RadarMiscOneVideoSampleResUDP", {-1, 1}}
                                                    ,{"RadarMiscOneAcpPeriodUDP", {-1, 1}}
                                                    ,{"RadarEnableAutoTuneUDP", {-1, 1}}
                                                    ,{"RadarEnableDAConvertersUDP", {-1, 1}}
                                                    ,{"RadarEnableADConvertersUDP", {-1, 1}}

                                                    ,{"RadarTuneCoarseUDP", {-1, 255}}
                                                    ,{"RadarTuneFineUDP", {-1, 255}}
                                                    ,{"RadarTuneRegUDP", {-1, 255}}

                                                    ,{"RadarSetupTransceiverModeUDP", {-1, 1}}
                                                    ,{"RadarSetupPulseModeUDP", {-1, 3}}
                                                    ,{"RadarSetupEnableMotorUDP", {-1, 1}}

                                                    ,{"RadarGateTuneDelayValueUDP", {-1, 255}}

                                                    ,{"RadarAntSpeedControlUDP", {-1, 1}}
                                                    ,{"RadarAntSpeedValueUDP", {-1, 3}}

                                                    ,{"RadarMasterIPLowUDP", {-1, 255}}

                                                    ,{"RadarDebugNoMasterModeUDP", {-1, 1}}
                                                    ,{"RadarDebugTxTestModeUDP", {-1, 1}}
                                                    ,{"RadarDebugPowerUpModeUDP", {-1, 1}}

                                                    ,{"SerialPortBaudRate", {0, 7}}
                                                    ,{"SerialPortDataBits", {0, 4}}
                                                    ,{"SerialPortStopBits", {0, 3}}
                                                    ,{"SerialPortParity", {0, 4}}
                                                    ,{"SerialPortFlowControl", {0, 3}}
                                                    ,{"SerialPortDirection", {0, 3}}

                                                    ,{"RadarTriggerGeneratorStateRS", {-1, 1}}
                                                    ,{"RadarTriggerPeriodRS", {-1, 99999}}
                                                    ,{"RadarTriggerStaggerValueRS", {-1, 15}}

                                                    ,{"RadarMiscZeroTestACPRS", {-1, 1}}
                                                    ,{"RadarMiscZeroAutoTuneFineRS", {-1, 1}}
                                                    ,{"RadarMiscZeroAutoTuneRegRS", {-1, 1}}
                                                    ,{"RadarMiscZeroDivideACPInputRS", {-1, 3}}

                                                    ,{"RadarMiscOneAcpPeriodRS", {-1, 2}}
                                                    ,{"RadarMiscOneBlankingRamRS", {-1, 1}}
                                                    ,{"RadarMiscOneBandwithRS", {-1, 1}}

                                                    ,{"RadarHlSetupWidthRS", {-1, 3}}
                                                    ,{"RadarHlSetupInvertRS", {-1, 1}}
                                                    ,{"RadarHlSetupEdgeRS", {-1, 1}}
                                                    ,{"RadarHlSetupDelayValueRS", {-1, 4095}}

                                                    ,{"RadarEnableAutoTuneRS", {-1, 1}}
                                                    ,{"RadarEnableDAConvertersRS", {-1, 1}}
                                                    ,{"RadarEnableADConvertersRS", {-1, 1}}

                                                    ,{"RadarTuneCoarseRS", {-1, 255}}
                                                    ,{"RadarTuneFineRS", {-1, 255}}
                                                    ,{"RadarTuneRegRS", {-1, 255}}

                                                    ,{"RadarSetupTransceiverModeRS", {-1, 1}}
                                                    ,{"RadarSetupPulseModeRS", {-1, 3}}
                                                    ,{"RadarSetupEnableMotorRS", {-1, 1}}

                                                    ,{"", {0, 0}}

                                                });
}

void IniReader::checkConfiguration(QMap<QString, QString> &config, const SettingsGroup &settingsGroup)
{
    QMap<QString, QPair<long, long>> referenceMap = referenceValues();
    if (config.isEmpty()) {
        config = IniReader::defaultConfiguration();
    } else {
        QMap<QString, QString> defaultSettings = IniReader::defaultConfiguration();
        QStringList keyList = defaultSettings.keys();
        for (int i = 0; i < keyList.size(); i++) {
            if (!config.contains(keyList[i]))
            {
                config[keyList[i]] = defaultSettings[keyList[i]];
            } else {
                if (referenceMap.contains(keyList[i])) {
                    if (config[keyList[i]].toLong() < referenceMap[keyList[i]].first || config[keyList[i]].toLong() >referenceMap[keyList[i]].second) {
                        config[keyList[i]] = defaultSettings[keyList[i]];
                    }
                }
            }
        }
    }

    switch (settingsGroup) {
    case SettingsGroup::AllSettings : {
        break;
    }
    case SettingsGroup::GreenBoardSettings : {

        // circule diagram settings
        config.remove("MaxAvgFactor");
        config.remove("AngleBitOffset");
        config.remove("RangeAvgCoef");
        config.remove("MaxPlottingFrequency");
        config.remove("Brightness");
        config.remove("Contrast");
        config.remove("Threshold");
        config.remove("ColorMode");

        // signal plot settings
        config.remove("SignalPlotLogScale");
        config.remove("SignalPlotAbs");
        config.remove("SignalPlotRePlot");
        config.remove("SignalPlotImPlot");
        config.remove("SignalPlotLimitLower");
        config.remove("SignalPlotLimitUpper");

        // tuning plot settings
        config.remove("TuningPlotEnanbled");
        config.remove("TuningPlotAvgLine");
        config.remove("TuningPlotAvgNumber");
        config.remove("TuningSignalAvgNumber");
        config.remove("TuningSignalAbs");
        config.remove("TuningSignalRe");
        config.remove("TuningSignalIm");
        config.remove("TuningPlotStartSample");
        config.remove("TuningPlotStopSample");
        config.remove("TuningSignalShowLimits");
        config.remove("TuningSignalAnalysisDelay");
        config.remove("TuningSignalCustomAnalysisDelay");

        // calibration plot settings
        config.remove("CalibrationMaxPlottingFrequency");
        config.remove("CalibrationAvgFactor");
        config.remove("CalibrationAngleBitOffset");
        config.remove("CalibrationRangeAvgCoef");
        config.remove("ExactNumberOfAvg");
        config.remove("CalibrationDefaultAvgCoefficient");

        // radar ethernet settings
        config.remove("HostAddrressPC");
        config.remove("ReadPortPC");
        config.remove("WritePortPC");
        config.remove("HostAddressRADAR");
        config.remove("ReadPortRADAR");
        config.remove("WritePortRADAR");
        config.remove("RadarTriggerGeneratorStateUDP");
        config.remove("RadarTriggerPeriodUDP");
        config.remove("RadarTriggerStaggerValueUDP");
        config.remove("RadarTriggerDelayValueUDP");
        config.remove("RadarADCSampligFrequencyUDP");
        config.remove("RadarMiscZeroBlankMemComtrolUDP");
        config.remove("RadarMiscZeroTestACPUDP");
        config.remove("RadarMiscZeroAutoTuneFineUDP");
        config.remove("RadarMiscZeroAutoTuneRegUDP");
        config.remove("RadarMiscZeroDivideACPInputUDP");
        config.remove("RadarMiscOneVideoSampleResUDP");
        config.remove("RadarMiscOneAcpPeriodUDP");
        config.remove("RadarMiscOneBlankingRamUDP");
        config.remove("RadarMiscOneBandwithUDP");
        config.remove("RadarHlSetupWidthUDP");
        config.remove("RadarHlSetupInvertUDP");
        config.remove("RadarHlSetupEdgeUDP");
        config.remove("RadarHlSetupDelayValueUDP");
        config.remove("RadarEnableVideoAcquisitionUDP");
        config.remove("RadarEnableDoubleBufferUDP");
        config.remove("RadarEnableAutoTuneUDP");
        config.remove("RadarEnableDAConvertersUDP");
        config.remove("RadarEnableADConvertersUDP");
        config.remove("RadarTuneCoarseUDP");
        config.remove("RadarTuneFineUDP");
        config.remove("RadarTuneRegUDP");
        config.remove("RadarSetupTransceiverModeUDP");
        config.remove("RadarSetupPulseModeUDP");
        config.remove("RadarSetupEnableMotorUDP");
        config.remove("RadarGateTuneDelayValueUDP");
        config.remove("RadarAntSpeedControlUDP");
        config.remove("RadarAntSpeedValueUDP");
        config.remove("RadarMasterIPLowUDP");
        config.remove("RadarDebugNoMasterModeUDP");
        config.remove("RadarDebugTxTestModeUDP");
        config.remove("RadarDebugPowerUpModeUDP");

        // radar serial settings
        config.remove("SerialPortName");
        config.remove("SerialPortBaudRate");
        config.remove("SerialPortDataBits");
        config.remove("SerialPortStopBits");
        config.remove("SerialPortParity");
        config.remove("SerialPortFlowControl");
        config.remove("SerialPortDirection");
        config.remove("RadarTriggerGeneratorStateRS");
        config.remove("RadarTriggerPeriodRS");
        config.remove("RadarTriggerStaggerValueRS");
        config.remove("RadarMiscZeroTestACPRS");
        config.remove("RadarMiscZeroAutoTuneFineRS");
        config.remove("RadarMiscZeroAutoTuneRegRS");
        config.remove("RadarMiscZeroDivideACPInputRS");
        config.remove("RadarMiscOneAcpPeriodRS");
        config.remove("RadarMiscOneBlankingRamRS");
        config.remove("RadarMiscOneBandwithRS");
        config.remove("RadarHlSetupWidthRS");
        config.remove("RadarHlSetupInvertRS");
        config.remove("RadarHlSetupEdgeRS");
        config.remove("RadarHlSetupDelayValueRS");
        config.remove("RadarEnableAutoTuneRS");
        config.remove("RadarEnableDAConvertersRS");
        config.remove("RadarEnableADConvertersRS");
        config.remove("RadarTuneCoarseRS");
        config.remove("RadarTuneFineRS");
        config.remove("RadarTuneRegRS");
        config.remove("RadarSetupTransceiverModeRS");
        config.remove("RadarSetupPulseModeRS");
        config.remove("RadarSetupEnableMotorRS");
        break;
    }
    case SettingsGroup::RadarEthernetSettings : {
        // circule diagram settings
        config.remove("MaxAvgFactor");
        config.remove("AngleBitOffset");
        config.remove("RangeAvgCoef");
        config.remove("MaxPlottingFrequency");
        config.remove("Brightness");
        config.remove("Contrast");
        config.remove("Threshold");
        config.remove("ColorMode");

        // signal plot settings
        config.remove("SignalPlotLogScale");
        config.remove("SignalPlotAbs");
        config.remove("SignalPlotRePlot");
        config.remove("SignalPlotImPlot");
        config.remove("SignalPlotLimitLower");
        config.remove("SignalPlotLimitUpper");

        // tuning plot settings
        config.remove("TuningPlotEnanbled");
        config.remove("TuningPlotAvgLine");
        config.remove("TuningPlotAvgNumber");
        config.remove("TuningSignalAvgNumber");
        config.remove("TuningSignalAbs");
        config.remove("TuningSignalRe");
        config.remove("TuningSignalIm");
        config.remove("TuningPlotStartSample");
        config.remove("TuningPlotStopSample");
        config.remove("TuningSignalShowLimits");
        config.remove("TuningSignalAnalysisDelay");
        config.remove("TuningSignalCustomAnalysisDelay");

        // calibration plot settings
        config.remove("CalibrationMaxPlottingFrequency");
        config.remove("CalibrationAvgFactor");
        config.remove("CalibrationAngleBitOffset");
        config.remove("CalibrationRangeAvgCoef");
        config.remove("ExactNumberOfAvg");
        config.remove("CalibrationDefaultAvgCoefficient");

        // greenboard settings
        config.remove("PcHostAddress");
        config.remove("PcControlPort");
        config.remove("PcDataPort");
        config.remove("BoardHostAddress");
        config.remove("BoardControlPort");
        config.remove("BoardDataPort");
        config.remove("BoardRxEnable");
        config.remove("BoardClockSignalSource");
        config.remove("BoardAdcOperationMode");
        config.remove("BoardInrernalTriggerFrequency");
        config.remove("BoardTriggerSignalSource");
        config.remove("BoardAdcADelay");
        config.remove("BoardAdcBDelay");
        config.remove("BoardAdcDelaySynchro");
        config.remove("BoardAngleOffset");
        config.remove("SignalAvgCoefficient");
        config.remove("DefaultFileName");
        config.remove("StartSavingTreadAutomaticly");
        config.remove("MotorControlAutoStart");

        // additional part
        config.remove("BoardTriggerFrequency");
        config.remove("BoardAdcALength");
        config.remove("MaxFileSize");
        config.remove("FirstSavingDatagram");
        config.remove("LastSavingDatagram");
        config.remove("ContinueSavingAfterPathChanged");
        config.remove("BlindSectorOneEnabled");
        config.remove("BlindSectorOneBegin");
        config.remove("BlindSectorOneEnd");
        config.remove("BlindSectorTwoEnabled");
        config.remove("BlindSectorTwoBegin");
        config.remove("BlindSectorTwoEnd");
        config.remove("BlindSectorThreeEnabled");
        config.remove("BlindSectorThreeBegin");
        config.remove("BlindSectorThreeEnd");

        // radar serial settings
        config.remove("SerialPortName");
        config.remove("SerialPortBaudRate");
        config.remove("SerialPortDataBits");
        config.remove("SerialPortStopBits");
        config.remove("SerialPortParity");
        config.remove("SerialPortFlowControl");
        config.remove("SerialPortDirection");
        config.remove("RadarTriggerGeneratorStateRS");
        config.remove("RadarTriggerPeriodRS");
        config.remove("RadarTriggerStaggerValueRS");
        config.remove("RadarMiscZeroTestACPRS");
        config.remove("RadarMiscZeroAutoTuneFineRS");
        config.remove("RadarMiscZeroAutoTuneRegRS");
        config.remove("RadarMiscZeroDivideACPInputRS");
        config.remove("RadarMiscOneAcpPeriodRS");
        config.remove("RadarMiscOneBlankingRamRS");
        config.remove("RadarMiscOneBandwithRS");
        config.remove("RadarHlSetupWidthRS");
        config.remove("RadarHlSetupInvertRS");
        config.remove("RadarHlSetupEdgeRS");
        config.remove("RadarHlSetupDelayValueRS");
        config.remove("RadarEnableAutoTuneRS");
        config.remove("RadarEnableDAConvertersRS");
        config.remove("RadarEnableADConvertersRS");
        config.remove("RadarTuneCoarseRS");
        config.remove("RadarTuneFineRS");
        config.remove("RadarTuneRegRS");
        config.remove("RadarSetupTransceiverModeRS");
        config.remove("RadarSetupPulseModeRS");
        config.remove("RadarSetupEnableMotorRS");
        break;
    }
    case SettingsGroup::RadarSerialSettings : {
        // circule diagram settings
        config.remove("MaxAvgFactor");
        config.remove("AngleBitOffset");
        config.remove("RangeAvgCoef");
        config.remove("MaxPlottingFrequency");
        config.remove("Brightness");
        config.remove("Contrast");
        config.remove("Threshold");
        config.remove("ColorMode");

        // signal plot settings
        config.remove("SignalPlotLogScale");
        config.remove("SignalPlotAbs");
        config.remove("SignalPlotRePlot");
        config.remove("SignalPlotImPlot");
        config.remove("SignalPlotLimitLower");
        config.remove("SignalPlotLimitUpper");

        // tuning plot settings
        config.remove("TuningPlotEnanbled");
        config.remove("TuningPlotAvgLine");
        config.remove("TuningPlotAvgNumber");
        config.remove("TuningSignalAvgNumber");
        config.remove("TuningSignalAbs");
        config.remove("TuningSignalRe");
        config.remove("TuningSignalIm");
        config.remove("TuningPlotStartSample");
        config.remove("TuningPlotStopSample");
        config.remove("TuningSignalShowLimits");
        config.remove("TuningSignalAnalysisDelay");
        config.remove("TuningSignalCustomAnalysisDelay");

        // calibration plot settings
        config.remove("CalibrationMaxPlottingFrequency");
        config.remove("CalibrationAvgFactor");
        config.remove("CalibrationAngleBitOffset");
        config.remove("CalibrationRangeAvgCoef");
        config.remove("ExactNumberOfAvg");
        config.remove("CalibrationDefaultAvgCoefficient");

        // greenboard settings
        config.remove("PcHostAddress");
        config.remove("PcControlPort");
        config.remove("PcDataPort");
        config.remove("BoardHostAddress");
        config.remove("BoardControlPort");
        config.remove("BoardDataPort");
        config.remove("BoardRxEnable");
        config.remove("BoardClockSignalSource");
        config.remove("BoardAdcOperationMode");
        config.remove("BoardInrernalTriggerFrequency");
        config.remove("BoardTriggerSignalSource");
        config.remove("BoardAdcADelay");
        config.remove("BoardAdcBDelay");
        config.remove("BoardAdcDelaySynchro");
        config.remove("BoardAngleOffset");
        config.remove("SignalAvgCoefficient");
        config.remove("DefaultFileName");
        config.remove("StartSavingTreadAutomaticly");
        config.remove("MotorControlAutoStart");

        // additional part
        config.remove("BoardTriggerFrequency");
        config.remove("BoardAdcALength");
        config.remove("MaxFileSize");
        config.remove("FirstSavingDatagram");
        config.remove("LastSavingDatagram");
        config.remove("ContinueSavingAfterPathChanged");
        config.remove("BlindSectorOneEnabled");
        config.remove("BlindSectorOneBegin");
        config.remove("BlindSectorOneEnd");
        config.remove("BlindSectorTwoEnabled");
        config.remove("BlindSectorTwoBegin");
        config.remove("BlindSectorTwoEnd");
        config.remove("BlindSectorThreeEnabled");
        config.remove("BlindSectorThreeBegin");
        config.remove("BlindSectorThreeEnd");

        // radar ethernet settings
        config.remove("HostAddrressPC");
        config.remove("ReadPortPC");
        config.remove("WritePortPC");
        config.remove("HostAddressRADAR");
        config.remove("ReadPortRADAR");
        config.remove("WritePortRADAR");
        config.remove("RadarTriggerGeneratorStateUDP");
        config.remove("RadarTriggerPeriodUDP");
        config.remove("RadarTriggerStaggerValueUDP");
        config.remove("RadarTriggerDelayValueUDP");
        config.remove("RadarADCSampligFrequencyUDP");
        config.remove("RadarMiscZeroBlankMemComtrolUDP");
        config.remove("RadarMiscZeroTestACPUDP");
        config.remove("RadarMiscZeroAutoTuneFineUDP");
        config.remove("RadarMiscZeroAutoTuneRegUDP");
        config.remove("RadarMiscZeroDivideACPInputUDP");
        config.remove("RadarMiscOneVideoSampleResUDP");
        config.remove("RadarMiscOneAcpPeriodUDP");
        config.remove("RadarMiscOneBlankingRamUDP");
        config.remove("RadarMiscOneBandwithUDP");
        config.remove("RadarHlSetupWidthUDP");
        config.remove("RadarHlSetupInvertUDP");
        config.remove("RadarHlSetupEdgeUDP");
        config.remove("RadarHlSetupDelayValueUDP");
        config.remove("RadarEnableVideoAcquisitionUDP");
        config.remove("RadarEnableDoubleBufferUDP");
        config.remove("RadarEnableAutoTuneUDP");
        config.remove("RadarEnableDAConvertersUDP");
        config.remove("RadarEnableADConvertersUDP");
        config.remove("RadarTuneCoarseUDP");
        config.remove("RadarTuneFineUDP");
        config.remove("RadarTuneRegUDP");
        config.remove("RadarSetupTransceiverModeUDP");
        config.remove("RadarSetupPulseModeUDP");
        config.remove("RadarSetupEnableMotorUDP");
        config.remove("RadarGateTuneDelayValueUDP");
        config.remove("RadarAntSpeedControlUDP");
        config.remove("RadarAntSpeedValueUDP");
        config.remove("RadarMasterIPLowUDP");
        config.remove("RadarDebugNoMasterModeUDP");
        config.remove("RadarDebugTxTestModeUDP");
        config.remove("RadarDebugPowerUpModeUDP");

        break;
    }
    case SettingsGroup::PlotSettings : {
        // greenboard settings
        config.remove("PcHostAddress");
        config.remove("PcControlPort");
        config.remove("PcDataPort");
        config.remove("BoardHostAddress");
        config.remove("BoardControlPort");
        config.remove("BoardDataPort");
        config.remove("BoardRxEnable");
        config.remove("BoardClockSignalSource");
        config.remove("BoardAdcOperationMode");
        config.remove("BoardInrernalTriggerFrequency");
        config.remove("BoardTriggerSignalSource");
        config.remove("BoardAdcADelay");
        config.remove("BoardAdcBDelay");
        config.remove("BoardAdcDelaySynchro");
        config.remove("BoardAngleOffset");
        config.remove("SignalAvgCoefficient");
        config.remove("DefaultFileName");
        config.remove("StartSavingTreadAutomaticly");
        config.remove("MotorControlAutoStart");

        // additional part
        config.remove("BoardTriggerFrequency");
        config.remove("BoardAdcALength");
        config.remove("MaxFileSize");
        config.remove("FirstSavingDatagram");
        config.remove("LastSavingDatagram");
        config.remove("ContinueSavingAfterPathChanged");
        config.remove("BlindSectorOneEnabled");
        config.remove("BlindSectorOneBegin");
        config.remove("BlindSectorOneEnd");
        config.remove("BlindSectorTwoEnabled");
        config.remove("BlindSectorTwoBegin");
        config.remove("BlindSectorTwoEnd");
        config.remove("BlindSectorThreeEnabled");
        config.remove("BlindSectorThreeBegin");
        config.remove("BlindSectorThreeEnd");

        // radar ethernet settings
        config.remove("HostAddrressPC");
        config.remove("ReadPortPC");
        config.remove("WritePortPC");
        config.remove("HostAddressRADAR");
        config.remove("ReadPortRADAR");
        config.remove("WritePortRADAR");
        config.remove("RadarTriggerGeneratorStateUDP");
        config.remove("RadarTriggerPeriodUDP");
        config.remove("RadarTriggerStaggerValueUDP");
        config.remove("RadarTriggerDelayValueUDP");
        config.remove("RadarADCSampligFrequencyUDP");
        config.remove("RadarMiscZeroBlankMemComtrolUDP");
        config.remove("RadarMiscZeroTestACPUDP");
        config.remove("RadarMiscZeroAutoTuneFineUDP");
        config.remove("RadarMiscZeroAutoTuneRegUDP");
        config.remove("RadarMiscZeroDivideACPInputUDP");
        config.remove("RadarMiscOneVideoSampleResUDP");
        config.remove("RadarMiscOneAcpPeriodUDP");
        config.remove("RadarMiscOneBlankingRamUDP");
        config.remove("RadarMiscOneBandwithUDP");
        config.remove("RadarHlSetupWidthUDP");
        config.remove("RadarHlSetupInvertUDP");
        config.remove("RadarHlSetupEdgeUDP");
        config.remove("RadarHlSetupDelayValueUDP");
        config.remove("RadarEnableVideoAcquisitionUDP");
        config.remove("RadarEnableDoubleBufferUDP");
        config.remove("RadarEnableAutoTuneUDP");
        config.remove("RadarEnableDAConvertersUDP");
        config.remove("RadarEnableADConvertersUDP");
        config.remove("RadarTuneCoarseUDP");
        config.remove("RadarTuneFineUDP");
        config.remove("RadarTuneRegUDP");
        config.remove("RadarSetupTransceiverModeUDP");
        config.remove("RadarSetupPulseModeUDP");
        config.remove("RadarSetupEnableMotorUDP");
        config.remove("RadarGateTuneDelayValueUDP");
        config.remove("RadarAntSpeedControlUDP");
        config.remove("RadarAntSpeedValueUDP");
        config.remove("RadarMasterIPLowUDP");
        config.remove("RadarDebugNoMasterModeUDP");
        config.remove("RadarDebugTxTestModeUDP");
        config.remove("RadarDebugPowerUpModeUDP");

        // radar serial settings
        config.remove("SerialPortName");
        config.remove("SerialPortBaudRate");
        config.remove("SerialPortDataBits");
        config.remove("SerialPortStopBits");
        config.remove("SerialPortParity");
        config.remove("SerialPortFlowControl");
        config.remove("SerialPortDirection");
        config.remove("RadarTriggerGeneratorStateRS");
        config.remove("RadarTriggerPeriodRS");
        config.remove("RadarTriggerStaggerValueRS");
        config.remove("RadarMiscZeroTestACPRS");
        config.remove("RadarMiscZeroAutoTuneFineRS");
        config.remove("RadarMiscZeroAutoTuneRegRS");
        config.remove("RadarMiscZeroDivideACPInputRS");
        config.remove("RadarMiscOneAcpPeriodRS");
        config.remove("RadarMiscOneBlankingRamRS");
        config.remove("RadarMiscOneBandwithRS");
        config.remove("RadarHlSetupWidthRS");
        config.remove("RadarHlSetupInvertRS");
        config.remove("RadarHlSetupEdgeRS");
        config.remove("RadarHlSetupDelayValueRS");
        config.remove("RadarEnableAutoTuneRS");
        config.remove("RadarEnableDAConvertersRS");
        config.remove("RadarEnableADConvertersRS");
        config.remove("RadarTuneCoarseRS");
        config.remove("RadarTuneFineRS");
        config.remove("RadarTuneRegRS");
        config.remove("RadarSetupTransceiverModeRS");
        config.remove("RadarSetupPulseModeRS");
        config.remove("RadarSetupEnableMotorRS");
        break;
    }
    case SettingsGroup::DiagramPlotSettings : {
        // signal plot settings
        config.remove("SignalPlotLogScale");
        config.remove("SignalPlotAbs");
        config.remove("SignalPlotRePlot");
        config.remove("SignalPlotImPlot");
        config.remove("SignalPlotLimitLower");
        config.remove("SignalPlotLimitUpper");
        config.remove("Brightness");
        config.remove("Contrast");
        config.remove("Threshold");
        config.remove("ColorMode");

        // tuning plot settings
        config.remove("TuningPlotEnanbled");
        config.remove("TuningPlotAvgLine");
        config.remove("TuningPlotAvgNumber");
        config.remove("TuningSignalAvgNumber");
        config.remove("TuningSignalAbs");
        config.remove("TuningSignalRe");
        config.remove("TuningSignalIm");
        config.remove("TuningPlotStartSample");
        config.remove("TuningPlotStopSample");
        config.remove("TuningSignalShowLimits");
        config.remove("TuningSignalAnalysisDelay");
        config.remove("TuningSignalCustomAnalysisDelay");

        // calibration plot settings
        config.remove("CalibrationMaxPlottingFrequency");
        config.remove("CalibrationAvgFactor");
        config.remove("CalibrationAngleBitOffset");
        config.remove("CalibrationRangeAvgCoef");
        config.remove("ExactNumberOfAvg");
        config.remove("CalibrationDefaultAvgCoefficient");

        // greenboard settings
        config.remove("PcHostAddress");
        config.remove("PcControlPort");
        config.remove("PcDataPort");
        config.remove("BoardHostAddress");
        config.remove("BoardControlPort");
        config.remove("BoardDataPort");
        config.remove("BoardRxEnable");
        config.remove("BoardClockSignalSource");
        config.remove("BoardAdcOperationMode");
        config.remove("BoardInrernalTriggerFrequency");
        config.remove("BoardTriggerSignalSource");
        config.remove("BoardAdcADelay");
        config.remove("BoardAdcBDelay");
        config.remove("BoardAdcDelaySynchro");
        config.remove("BoardAngleOffset");
        config.remove("SignalAvgCoefficient");
        config.remove("DefaultFileName");
        config.remove("StartSavingTreadAutomaticly");
        config.remove("MotorControlAutoStart");

        // additional part
        config.remove("BoardTriggerFrequency");
        config.remove("BoardAdcALength");
        config.remove("MaxFileSize");
        config.remove("FirstSavingDatagram");
        config.remove("LastSavingDatagram");
        config.remove("ContinueSavingAfterPathChanged");
        config.remove("BlindSectorOneEnabled");
        config.remove("BlindSectorOneBegin");
        config.remove("BlindSectorOneEnd");
        config.remove("BlindSectorTwoEnabled");
        config.remove("BlindSectorTwoBegin");
        config.remove("BlindSectorTwoEnd");
        config.remove("BlindSectorThreeEnabled");
        config.remove("BlindSectorThreeBegin");
        config.remove("BlindSectorThreeEnd");

        // radar ethernet settings
        config.remove("HostAddrressPC");
        config.remove("ReadPortPC");
        config.remove("WritePortPC");
        config.remove("HostAddressRADAR");
        config.remove("ReadPortRADAR");
        config.remove("WritePortRADAR");
        config.remove("RadarTriggerGeneratorStateUDP");
        config.remove("RadarTriggerPeriodUDP");
        config.remove("RadarTriggerStaggerValueUDP");
        config.remove("RadarTriggerDelayValueUDP");
        config.remove("RadarADCSampligFrequencyUDP");
        config.remove("RadarMiscZeroBlankMemComtrolUDP");
        config.remove("RadarMiscZeroTestACPUDP");
        config.remove("RadarMiscZeroAutoTuneFineUDP");
        config.remove("RadarMiscZeroAutoTuneRegUDP");
        config.remove("RadarMiscZeroDivideACPInputUDP");
        config.remove("RadarMiscOneVideoSampleResUDP");
        config.remove("RadarMiscOneAcpPeriodUDP");
        config.remove("RadarMiscOneBlankingRamUDP");
        config.remove("RadarMiscOneBandwithUDP");
        config.remove("RadarHlSetupWidthUDP");
        config.remove("RadarHlSetupInvertUDP");
        config.remove("RadarHlSetupEdgeUDP");
        config.remove("RadarHlSetupDelayValueUDP");
        config.remove("RadarEnableVideoAcquisitionUDP");
        config.remove("RadarEnableDoubleBufferUDP");
        config.remove("RadarEnableAutoTuneUDP");
        config.remove("RadarEnableDAConvertersUDP");
        config.remove("RadarEnableADConvertersUDP");
        config.remove("RadarTuneCoarseUDP");
        config.remove("RadarTuneFineUDP");
        config.remove("RadarTuneRegUDP");
        config.remove("RadarSetupTransceiverModeUDP");
        config.remove("RadarSetupPulseModeUDP");
        config.remove("RadarSetupEnableMotorUDP");
        config.remove("RadarGateTuneDelayValueUDP");
        config.remove("RadarAntSpeedControlUDP");
        config.remove("RadarAntSpeedValueUDP");
        config.remove("RadarMasterIPLowUDP");
        config.remove("RadarDebugNoMasterModeUDP");
        config.remove("RadarDebugTxTestModeUDP");
        config.remove("RadarDebugPowerUpModeUDP");

        // radar serial settings
        config.remove("SerialPortName");
        config.remove("SerialPortBaudRate");
        config.remove("SerialPortDataBits");
        config.remove("SerialPortStopBits");
        config.remove("SerialPortParity");
        config.remove("SerialPortFlowControl");
        config.remove("SerialPortDirection");
        config.remove("RadarTriggerGeneratorStateRS");
        config.remove("RadarTriggerPeriodRS");
        config.remove("RadarTriggerStaggerValueRS");
        config.remove("RadarMiscZeroTestACPRS");
        config.remove("RadarMiscZeroAutoTuneFineRS");
        config.remove("RadarMiscZeroAutoTuneRegRS");
        config.remove("RadarMiscZeroDivideACPInputRS");
        config.remove("RadarMiscOneAcpPeriodRS");
        config.remove("RadarMiscOneBlankingRamRS");
        config.remove("RadarMiscOneBandwithRS");
        config.remove("RadarHlSetupWidthRS");
        config.remove("RadarHlSetupInvertRS");
        config.remove("RadarHlSetupEdgeRS");
        config.remove("RadarHlSetupDelayValueRS");
        config.remove("RadarEnableAutoTuneRS");
        config.remove("RadarEnableDAConvertersRS");
        config.remove("RadarEnableADConvertersRS");
        config.remove("RadarTuneCoarseRS");
        config.remove("RadarTuneFineRS");
        config.remove("RadarTuneRegRS");
        config.remove("RadarSetupTransceiverModeRS");
        config.remove("RadarSetupPulseModeRS");
        config.remove("RadarSetupEnableMotorRS");
        break;
    }
    case SettingsGroup::SignalPlotSettings : {
        // circule diagram settings
        config.remove("MaxAvgFactor");
        config.remove("AngleBitOffset");
        config.remove("RangeAvgCoef");
        config.remove("MaxPlottingFrequency");

        // tuning plot settings
        config.remove("TuningPlotEnanbled");
        config.remove("TuningPlotAvgLine");
        config.remove("TuningPlotAvgNumber");
        config.remove("TuningSignalAvgNumber");
        config.remove("TuningSignalAbs");
        config.remove("TuningSignalRe");
        config.remove("TuningSignalIm");
        config.remove("TuningPlotStartSample");
        config.remove("TuningPlotStopSample");
        config.remove("TuningSignalShowLimits");
        config.remove("TuningSignalAnalysisDelay");
        config.remove("TuningSignalCustomAnalysisDelay");

        // calibration plot settings
        config.remove("CalibrationMaxPlottingFrequency");
        config.remove("CalibrationAvgFactor");
        config.remove("CalibrationAngleBitOffset");
        config.remove("CalibrationRangeAvgCoef");
        config.remove("ExactNumberOfAvg");
        config.remove("CalibrationDefaultAvgCoefficient");

        // greenboard settings
        config.remove("PcHostAddress");
        config.remove("PcControlPort");
        config.remove("PcDataPort");
        config.remove("BoardHostAddress");
        config.remove("BoardControlPort");
        config.remove("BoardDataPort");
        config.remove("BoardRxEnable");
        config.remove("BoardClockSignalSource");
        config.remove("BoardAdcOperationMode");
        config.remove("BoardInrernalTriggerFrequency");
        config.remove("BoardTriggerSignalSource");
        config.remove("BoardAdcADelay");
        config.remove("BoardAdcBDelay");
        config.remove("BoardAdcDelaySynchro");
        config.remove("BoardAngleOffset");
        config.remove("SignalAvgCoefficient");
        config.remove("DefaultFileName");
        config.remove("StartSavingTreadAutomaticly");
        config.remove("MotorControlAutoStart");

        // additional part
        config.remove("BoardTriggerFrequency");
        config.remove("BoardAdcALength");
        config.remove("MaxFileSize");
        config.remove("FirstSavingDatagram");
        config.remove("LastSavingDatagram");
        config.remove("ContinueSavingAfterPathChanged");
        config.remove("BlindSectorOneEnabled");
        config.remove("BlindSectorOneBegin");
        config.remove("BlindSectorOneEnd");
        config.remove("BlindSectorTwoEnabled");
        config.remove("BlindSectorTwoBegin");
        config.remove("BlindSectorTwoEnd");
        config.remove("BlindSectorThreeEnabled");
        config.remove("BlindSectorThreeBegin");
        config.remove("BlindSectorThreeEnd");

        // radar ethernet settings
        config.remove("HostAddrressPC");
        config.remove("ReadPortPC");
        config.remove("WritePortPC");
        config.remove("HostAddressRADAR");
        config.remove("ReadPortRADAR");
        config.remove("WritePortRADAR");
        config.remove("RadarTriggerGeneratorStateUDP");
        config.remove("RadarTriggerPeriodUDP");
        config.remove("RadarTriggerStaggerValueUDP");
        config.remove("RadarTriggerDelayValueUDP");
        config.remove("RadarADCSampligFrequencyUDP");
        config.remove("RadarMiscZeroBlankMemComtrolUDP");
        config.remove("RadarMiscZeroTestACPUDP");
        config.remove("RadarMiscZeroAutoTuneFineUDP");
        config.remove("RadarMiscZeroAutoTuneRegUDP");
        config.remove("RadarMiscZeroDivideACPInputUDP");
        config.remove("RadarMiscOneVideoSampleResUDP");
        config.remove("RadarMiscOneAcpPeriodUDP");
        config.remove("RadarMiscOneBlankingRamUDP");
        config.remove("RadarMiscOneBandwithUDP");
        config.remove("RadarHlSetupWidthUDP");
        config.remove("RadarHlSetupInvertUDP");
        config.remove("RadarHlSetupEdgeUDP");
        config.remove("RadarHlSetupDelayValueUDP");
        config.remove("RadarEnableVideoAcquisitionUDP");
        config.remove("RadarEnableDoubleBufferUDP");
        config.remove("RadarEnableAutoTuneUDP");
        config.remove("RadarEnableDAConvertersUDP");
        config.remove("RadarEnableADConvertersUDP");
        config.remove("RadarTuneCoarseUDP");
        config.remove("RadarTuneFineUDP");
        config.remove("RadarTuneRegUDP");
        config.remove("RadarSetupTransceiverModeUDP");
        config.remove("RadarSetupPulseModeUDP");
        config.remove("RadarSetupEnableMotorUDP");
        config.remove("RadarGateTuneDelayValueUDP");
        config.remove("RadarAntSpeedControlUDP");
        config.remove("RadarAntSpeedValueUDP");
        config.remove("RadarMasterIPLowUDP");
        config.remove("RadarDebugNoMasterModeUDP");
        config.remove("RadarDebugTxTestModeUDP");
        config.remove("RadarDebugPowerUpModeUDP");

        // radar serial settings
        config.remove("SerialPortName");
        config.remove("SerialPortBaudRate");
        config.remove("SerialPortDataBits");
        config.remove("SerialPortStopBits");
        config.remove("SerialPortParity");
        config.remove("SerialPortFlowControl");
        config.remove("SerialPortDirection");
        config.remove("RadarTriggerGeneratorStateRS");
        config.remove("RadarTriggerPeriodRS");
        config.remove("RadarTriggerStaggerValueRS");
        config.remove("RadarMiscZeroTestACPRS");
        config.remove("RadarMiscZeroAutoTuneFineRS");
        config.remove("RadarMiscZeroAutoTuneRegRS");
        config.remove("RadarMiscZeroDivideACPInputRS");
        config.remove("RadarMiscOneAcpPeriodRS");
        config.remove("RadarMiscOneBlankingRamRS");
        config.remove("RadarMiscOneBandwithRS");
        config.remove("RadarHlSetupWidthRS");
        config.remove("RadarHlSetupInvertRS");
        config.remove("RadarHlSetupEdgeRS");
        config.remove("RadarHlSetupDelayValueRS");
        config.remove("RadarEnableAutoTuneRS");
        config.remove("RadarEnableDAConvertersRS");
        config.remove("RadarEnableADConvertersRS");
        config.remove("RadarTuneCoarseRS");
        config.remove("RadarTuneFineRS");
        config.remove("RadarTuneRegRS");
        config.remove("RadarSetupTransceiverModeRS");
        config.remove("RadarSetupPulseModeRS");
        config.remove("RadarSetupEnableMotorRS");
        break;
    }
    case SettingsGroup::CalibrationPlotSettings : {
        // circule diagram settings
        config.remove("MaxAvgFactor");
        config.remove("AngleBitOffset");
        config.remove("RangeAvgCoef");
        config.remove("MaxPlottingFrequency");
        config.remove("Brightness");
        config.remove("Contrast");
        config.remove("Threshold");
        config.remove("ColorMode");

        // signal plot settings
        config.remove("SignalPlotLogScale");
        config.remove("SignalPlotAbs");
        config.remove("SignalPlotRePlot");
        config.remove("SignalPlotImPlot");
        config.remove("SignalPlotLimitLower");
        config.remove("SignalPlotLimitUpper");

        // tuning plot settings
        config.remove("TuningPlotEnanbled");
        config.remove("TuningPlotAvgLine");
        config.remove("TuningPlotAvgNumber");
        config.remove("TuningSignalAvgNumber");
        config.remove("TuningSignalAbs");
        config.remove("TuningSignalRe");
        config.remove("TuningSignalIm");
        config.remove("TuningPlotStartSample");
        config.remove("TuningPlotStopSample");
        config.remove("TuningSignalShowLimits");
        config.remove("TuningSignalAnalysisDelay");
        config.remove("TuningSignalCustomAnalysisDelay");

        // greenboard settings
        config.remove("PcHostAddress");
        config.remove("PcControlPort");
        config.remove("PcDataPort");
        config.remove("BoardHostAddress");
        config.remove("BoardControlPort");
        config.remove("BoardDataPort");
        config.remove("BoardRxEnable");
        config.remove("BoardClockSignalSource");
        config.remove("BoardAdcOperationMode");
        config.remove("BoardInrernalTriggerFrequency");
        config.remove("BoardTriggerSignalSource");
        config.remove("BoardAdcADelay");
        config.remove("BoardAdcBDelay");
        config.remove("BoardAdcDelaySynchro");
        config.remove("BoardAngleOffset");
        config.remove("SignalAvgCoefficient");
        config.remove("DefaultFileName");
        config.remove("StartSavingTreadAutomaticly");
        config.remove("MotorControlAutoStart");

        // additional part
        config.remove("BoardTriggerFrequency");
        config.remove("BoardAdcALength");
        config.remove("MaxFileSize");
        config.remove("FirstSavingDatagram");
        config.remove("LastSavingDatagram");
        config.remove("ContinueSavingAfterPathChanged");
        config.remove("BlindSectorOneEnabled");
        config.remove("BlindSectorOneBegin");
        config.remove("BlindSectorOneEnd");
        config.remove("BlindSectorTwoEnabled");
        config.remove("BlindSectorTwoBegin");
        config.remove("BlindSectorTwoEnd");
        config.remove("BlindSectorThreeEnabled");
        config.remove("BlindSectorThreeBegin");
        config.remove("BlindSectorThreeEnd");

        // radar ethernet settings
        config.remove("HostAddrressPC");
        config.remove("ReadPortPC");
        config.remove("WritePortPC");
        config.remove("HostAddressRADAR");
        config.remove("ReadPortRADAR");
        config.remove("WritePortRADAR");
        config.remove("RadarTriggerGeneratorStateUDP");
        config.remove("RadarTriggerPeriodUDP");
        config.remove("RadarTriggerStaggerValueUDP");
        config.remove("RadarTriggerDelayValueUDP");
        config.remove("RadarADCSampligFrequencyUDP");
        config.remove("RadarMiscZeroBlankMemComtrolUDP");
        config.remove("RadarMiscZeroTestACPUDP");
        config.remove("RadarMiscZeroAutoTuneFineUDP");
        config.remove("RadarMiscZeroAutoTuneRegUDP");
        config.remove("RadarMiscZeroDivideACPInputUDP");
        config.remove("RadarMiscOneVideoSampleResUDP");
        config.remove("RadarMiscOneAcpPeriodUDP");
        config.remove("RadarMiscOneBlankingRamUDP");
        config.remove("RadarMiscOneBandwithUDP");
        config.remove("RadarHlSetupWidthUDP");
        config.remove("RadarHlSetupInvertUDP");
        config.remove("RadarHlSetupEdgeUDP");
        config.remove("RadarHlSetupDelayValueUDP");
        config.remove("RadarEnableVideoAcquisitionUDP");
        config.remove("RadarEnableDoubleBufferUDP");
        config.remove("RadarEnableAutoTuneUDP");
        config.remove("RadarEnableDAConvertersUDP");
        config.remove("RadarEnableADConvertersUDP");
        config.remove("RadarTuneCoarseUDP");
        config.remove("RadarTuneFineUDP");
        config.remove("RadarTuneRegUDP");
        config.remove("RadarSetupTransceiverModeUDP");
        config.remove("RadarSetupPulseModeUDP");
        config.remove("RadarSetupEnableMotorUDP");
        config.remove("RadarGateTuneDelayValueUDP");
        config.remove("RadarAntSpeedControlUDP");
        config.remove("RadarAntSpeedValueUDP");
        config.remove("RadarMasterIPLowUDP");
        config.remove("RadarDebugNoMasterModeUDP");
        config.remove("RadarDebugTxTestModeUDP");
        config.remove("RadarDebugPowerUpModeUDP");

        // radar serial settings
        config.remove("SerialPortName");
        config.remove("SerialPortBaudRate");
        config.remove("SerialPortDataBits");
        config.remove("SerialPortStopBits");
        config.remove("SerialPortParity");
        config.remove("SerialPortFlowControl");
        config.remove("SerialPortDirection");
        config.remove("RadarTriggerGeneratorStateRS");
        config.remove("RadarTriggerPeriodRS");
        config.remove("RadarTriggerStaggerValueRS");
        config.remove("RadarMiscZeroTestACPRS");
        config.remove("RadarMiscZeroAutoTuneFineRS");
        config.remove("RadarMiscZeroAutoTuneRegRS");
        config.remove("RadarMiscZeroDivideACPInputRS");
        config.remove("RadarMiscOneAcpPeriodRS");
        config.remove("RadarMiscOneBlankingRamRS");
        config.remove("RadarMiscOneBandwithRS");
        config.remove("RadarHlSetupWidthRS");
        config.remove("RadarHlSetupInvertRS");
        config.remove("RadarHlSetupEdgeRS");
        config.remove("RadarHlSetupDelayValueRS");
        config.remove("RadarEnableAutoTuneRS");
        config.remove("RadarEnableDAConvertersRS");
        config.remove("RadarEnableADConvertersRS");
        config.remove("RadarTuneCoarseRS");
        config.remove("RadarTuneFineRS");
        config.remove("RadarTuneRegRS");
        config.remove("RadarSetupTransceiverModeRS");
        config.remove("RadarSetupPulseModeRS");
        config.remove("RadarSetupEnableMotorRS");
        break;
    }
    case SettingsGroup::TuningPlotSetting : {
        // circule diagram settings
        config.remove("MaxAvgFactor");
        config.remove("AngleBitOffset");
        config.remove("RangeAvgCoef");
        config.remove("MaxPlottingFrequency");
        config.remove("Brightness");
        config.remove("Contrast");
        config.remove("Threshold");
        config.remove("ColorMode");

        // signal plot settings
        config.remove("SignalPlotLogScale");
        config.remove("SignalPlotAbs");
        config.remove("SignalPlotRePlot");
        config.remove("SignalPlotImPlot");
        config.remove("SignalPlotLimitLower");
        config.remove("SignalPlotLimitUpper");

        // calibration plot settings
        config.remove("CalibrationMaxPlottingFrequency");
        config.remove("CalibrationAvgFactor");
        config.remove("CalibrationAngleBitOffset");
        config.remove("CalibrationRangeAvgCoef");
        config.remove("ExactNumberOfAvg");
        config.remove("CalibrationDefaultAvgCoefficient");

        // greenboard settings
        config.remove("PcHostAddress");
        config.remove("PcControlPort");
        config.remove("PcDataPort");
        config.remove("BoardHostAddress");
        config.remove("BoardControlPort");
        config.remove("BoardDataPort");
        config.remove("BoardRxEnable");
        config.remove("BoardClockSignalSource");
        config.remove("BoardAdcOperationMode");
        config.remove("BoardInrernalTriggerFrequency");
        config.remove("BoardTriggerSignalSource");
        config.remove("BoardAdcADelay");
        config.remove("BoardAdcBDelay");
        config.remove("BoardAdcDelaySynchro");
        config.remove("BoardAngleOffset");
        config.remove("SignalAvgCoefficient");
        config.remove("DefaultFileName");
        config.remove("StartSavingTreadAutomaticly");
        config.remove("MotorControlAutoStart");

        // additional part
        config.remove("BoardTriggerFrequency");
        config.remove("BoardAdcALength");
        config.remove("MaxFileSize");
        config.remove("FirstSavingDatagram");
        config.remove("LastSavingDatagram");
        config.remove("ContinueSavingAfterPathChanged");
        config.remove("BlindSectorOneEnabled");
        config.remove("BlindSectorOneBegin");
        config.remove("BlindSectorOneEnd");
        config.remove("BlindSectorTwoEnabled");
        config.remove("BlindSectorTwoBegin");
        config.remove("BlindSectorTwoEnd");
        config.remove("BlindSectorThreeEnabled");
        config.remove("BlindSectorThreeBegin");
        config.remove("BlindSectorThreeEnd");

        // radar ethernet settings
        config.remove("HostAddrressPC");
        config.remove("ReadPortPC");
        config.remove("WritePortPC");
        config.remove("HostAddressRADAR");
        config.remove("ReadPortRADAR");
        config.remove("WritePortRADAR");
        config.remove("RadarTriggerGeneratorStateUDP");
        config.remove("RadarTriggerPeriodUDP");
        config.remove("RadarTriggerStaggerValueUDP");
        config.remove("RadarTriggerDelayValueUDP");
        config.remove("RadarADCSampligFrequencyUDP");
        config.remove("RadarMiscZeroBlankMemComtrolUDP");
        config.remove("RadarMiscZeroTestACPUDP");
        config.remove("RadarMiscZeroAutoTuneFineUDP");
        config.remove("RadarMiscZeroAutoTuneRegUDP");
        config.remove("RadarMiscZeroDivideACPInputUDP");
        config.remove("RadarMiscOneVideoSampleResUDP");
        config.remove("RadarMiscOneAcpPeriodUDP");
        config.remove("RadarMiscOneBlankingRamUDP");
        config.remove("RadarMiscOneBandwithUDP");
        config.remove("RadarHlSetupWidthUDP");
        config.remove("RadarHlSetupInvertUDP");
        config.remove("RadarHlSetupEdgeUDP");
        config.remove("RadarHlSetupDelayValueUDP");
        config.remove("RadarEnableVideoAcquisitionUDP");
        config.remove("RadarEnableDoubleBufferUDP");
        config.remove("RadarEnableAutoTuneUDP");
        config.remove("RadarEnableDAConvertersUDP");
        config.remove("RadarEnableADConvertersUDP");
        config.remove("RadarTuneCoarseUDP");
        config.remove("RadarTuneFineUDP");
        config.remove("RadarTuneRegUDP");
        config.remove("RadarSetupTransceiverModeUDP");
        config.remove("RadarSetupPulseModeUDP");
        config.remove("RadarSetupEnableMotorUDP");
        config.remove("RadarGateTuneDelayValueUDP");
        config.remove("RadarAntSpeedControlUDP");
        config.remove("RadarAntSpeedValueUDP");
        config.remove("RadarMasterIPLowUDP");
        config.remove("RadarDebugNoMasterModeUDP");
        config.remove("RadarDebugTxTestModeUDP");
        config.remove("RadarDebugPowerUpModeUDP");

        // radar serial settings
        config.remove("SerialPortName");
        config.remove("SerialPortBaudRate");
        config.remove("SerialPortDataBits");
        config.remove("SerialPortStopBits");
        config.remove("SerialPortParity");
        config.remove("SerialPortFlowControl");
        config.remove("SerialPortDirection");
        config.remove("RadarTriggerGeneratorStateRS");
        config.remove("RadarTriggerPeriodRS");
        config.remove("RadarTriggerStaggerValueRS");
        config.remove("RadarMiscZeroTestACPRS");
        config.remove("RadarMiscZeroAutoTuneFineRS");
        config.remove("RadarMiscZeroAutoTuneRegRS");
        config.remove("RadarMiscZeroDivideACPInputRS");
        config.remove("RadarMiscOneAcpPeriodRS");
        config.remove("RadarMiscOneBlankingRamRS");
        config.remove("RadarMiscOneBandwithRS");
        config.remove("RadarHlSetupWidthRS");
        config.remove("RadarHlSetupInvertRS");
        config.remove("RadarHlSetupEdgeRS");
        config.remove("RadarHlSetupDelayValueRS");
        config.remove("RadarEnableAutoTuneRS");
        config.remove("RadarEnableDAConvertersRS");
        config.remove("RadarEnableADConvertersRS");
        config.remove("RadarTuneCoarseRS");
        config.remove("RadarTuneFineRS");
        config.remove("RadarTuneRegRS");
        config.remove("RadarSetupTransceiverModeRS");
        config.remove("RadarSetupPulseModeRS");
        config.remove("RadarSetupEnableMotorRS");
        break;
    }
    case SettingsGroup::GreenBoardScheduleSettings : {
        // main settings
        config.remove("CalibrationMode");
        config.remove("RadarControlInterface");
        config.remove("DefaultPlotType");
        config.remove("DarkMode");

        // common settings
        config.remove("SamplingFrequency");
        config.remove("LightSpeed");
        config.remove("FrameLength");
        config.remove("AdcBitRateWithSign");
        config.remove("AdcMaxValue");


        // circule diagram settings
        config.remove("MaxAvgFactor");
        config.remove("AngleBitOffset");
        config.remove("RangeAvgCoef");
        config.remove("MaxPlottingFrequency");
        config.remove("Brightness");
        config.remove("Contrast");
        config.remove("Threshold");
        config.remove("ColorMode");

        // signal plot settings
        config.remove("SignalPlotLogScale");
        config.remove("SignalPlotAbs");
        config.remove("SignalPlotRePlot");
        config.remove("SignalPlotImPlot");
        config.remove("SignalPlotLimitLower");
        config.remove("SignalPlotLimitUpper");

        // tuning plot settings
        config.remove("TuningPlotEnanbled");
        config.remove("TuningPlotAvgLine");
        config.remove("TuningPlotAvgNumber");
        config.remove("TuningSignalAvgNumber");
        config.remove("TuningSignalAbs");
        config.remove("TuningSignalRe");
        config.remove("TuningSignalIm");
        config.remove("TuningPlotStartSample");
        config.remove("TuningPlotStopSample");
        config.remove("TuningSignalShowLimits");
        config.remove("TuningSignalAnalysisDelay");
        config.remove("TuningSignalCustomAnalysisDelay");

        // calibration plot settings
        config.remove("CalibrationMaxPlottingFrequency");
        config.remove("CalibrationAvgFactor");
        config.remove("CalibrationAngleBitOffset");
        config.remove("CalibrationRangeAvgCoef");
        config.remove("ExactNumberOfAvg");
        config.remove("CalibrationDefaultAvgCoefficient");

        // greenboard settings
        config.remove("PcHostAddress");
        config.remove("PcControlPort");
        config.remove("PcDataPort");
        config.remove("BoardHostAddress");
        config.remove("BoardControlPort");
        config.remove("BoardDataPort");
        config.remove("BoardRxEnable");
        config.remove("BoardClockSignalSource");
        config.remove("BoardAdcOperationMode");
        config.remove("BoardTriggerSignalSource");
        config.remove("BoardAdcADelay");
        config.remove("BoardAdcBDelay");
        config.remove("BoardAdcDelaySynchro");
        config.remove("BoardAngleOffset");
        config.remove("SignalAvgCoefficient");
        config.remove("StartSavingTreadAutomaticly");
        config.remove("MotorControlAutoStart");

        // additional part
        config.remove("ContinueSavingAfterPathChanged");
        config.remove("BlindSectorOneEnabled");
        config.remove("BlindSectorOneBegin");
        config.remove("BlindSectorOneEnd");
        config.remove("BlindSectorTwoEnabled");
        config.remove("BlindSectorTwoBegin");
        config.remove("BlindSectorTwoEnd");
        config.remove("BlindSectorThreeEnabled");
        config.remove("BlindSectorThreeBegin");
        config.remove("BlindSectorThreeEnd");

        // radar ethernet settings
        config.remove("HostAddrressPC");
        config.remove("ReadPortPC");
        config.remove("WritePortPC");
        config.remove("HostAddressRADAR");
        config.remove("ReadPortRADAR");
        config.remove("WritePortRADAR");
        config.remove("RadarTriggerGeneratorStateUDP");
        config.remove("RadarTriggerPeriodUDP");
        config.remove("RadarTriggerStaggerValueUDP");
        config.remove("RadarTriggerDelayValueUDP");
        config.remove("RadarADCSampligFrequencyUDP");
        config.remove("RadarMiscZeroBlankMemComtrolUDP");
        config.remove("RadarMiscZeroTestACPUDP");
        config.remove("RadarMiscZeroAutoTuneFineUDP");
        config.remove("RadarMiscZeroAutoTuneRegUDP");
        config.remove("RadarMiscZeroDivideACPInputUDP");
        config.remove("RadarMiscOneVideoSampleResUDP");
        config.remove("RadarMiscOneAcpPeriodUDP");
        config.remove("RadarMiscOneBlankingRamUDP");
        config.remove("RadarMiscOneBandwithUDP");
        config.remove("RadarHlSetupWidthUDP");
        config.remove("RadarHlSetupInvertUDP");
        config.remove("RadarHlSetupEdgeUDP");
        config.remove("RadarHlSetupDelayValueUDP");
        config.remove("RadarEnableVideoAcquisitionUDP");
        config.remove("RadarEnableDoubleBufferUDP");
        config.remove("RadarEnableAutoTuneUDP");
        config.remove("RadarEnableDAConvertersUDP");
        config.remove("RadarEnableADConvertersUDP");
        config.remove("RadarTuneCoarseUDP");
        config.remove("RadarTuneFineUDP");
        config.remove("RadarTuneRegUDP");
        config.remove("RadarSetupTransceiverModeUDP");
        config.remove("RadarSetupPulseModeUDP");
        config.remove("RadarSetupEnableMotorUDP");
        config.remove("RadarGateTuneDelayValueUDP");
        config.remove("RadarAntSpeedControlUDP");
        config.remove("RadarAntSpeedValueUDP");
        config.remove("RadarMasterIPLowUDP");
        config.remove("RadarDebugNoMasterModeUDP");
        config.remove("RadarDebugTxTestModeUDP");
        config.remove("RadarDebugPowerUpModeUDP");

        // radar serial settings
        config.remove("SerialPortName");
        config.remove("SerialPortBaudRate");
        config.remove("SerialPortDataBits");
        config.remove("SerialPortStopBits");
        config.remove("SerialPortParity");
        config.remove("SerialPortFlowControl");
        config.remove("SerialPortDirection");
        config.remove("RadarTriggerGeneratorStateRS");
        config.remove("RadarTriggerPeriodRS");
        config.remove("RadarTriggerStaggerValueRS");
        config.remove("RadarMiscZeroTestACPRS");
        config.remove("RadarMiscZeroAutoTuneFineRS");
        config.remove("RadarMiscZeroAutoTuneRegRS");
        config.remove("RadarMiscZeroDivideACPInputRS");
        config.remove("RadarMiscOneAcpPeriodRS");
        config.remove("RadarMiscOneBlankingRamRS");
        config.remove("RadarMiscOneBandwithRS");
        config.remove("RadarHlSetupWidthRS");
        config.remove("RadarHlSetupInvertRS");
        config.remove("RadarHlSetupEdgeRS");
        config.remove("RadarHlSetupDelayValueRS");
        config.remove("RadarEnableAutoTuneRS");
        config.remove("RadarEnableDAConvertersRS");
        config.remove("RadarEnableADConvertersRS");
        config.remove("RadarTuneCoarseRS");
        config.remove("RadarTuneFineRS");
        config.remove("RadarTuneRegRS");
        config.remove("RadarSetupTransceiverModeRS");
        config.remove("RadarSetupPulseModeRS");
        config.remove("RadarSetupEnableMotorRS");
        break;
    }
    }
}

bool IniReader::checkBasicConfiguration(QMap<QString, QString> &config)
{
    bool errorsAcqured = false;

    errorsAcqured = errorsAcqured || !QFile::exists(config[QString("config")]);
    if (config[QString("schedule")] != QString("")) {
        errorsAcqured = errorsAcqured || !QFile::exists(config[QString("schedule")]);
    }
    QDir dir;
    errorsAcqured = errorsAcqured || ((config[QString("logs")] != QString("")) && !dir.exists((config[QString("logs")])));
    if (config[QString("icons")] == QString("")) {
        errorsAcqured = true;
    } else {
        errorsAcqured = errorsAcqured || !dir.exists((config[QString("icons")]));
    }
//    errorsAcqured = errorsAcqured || QFile::exists(config[QString("tr")]);
    if (config[QString("dataFolder0")] == QString("") || !dir.exists(config[QString("dataFolder0")])) {
        config[QString("dataFolder0")] = QString("/tmp/Radar_Control/");
    } else {
        if (config[QString("dataFolder1")] != QString("")) {
            errorsAcqured = errorsAcqured || !dir.exists(config[QString("dataFolder1")]);
        }
        if (config[QString("dataFolder2")] != QString("")) {
            errorsAcqured = errorsAcqured || !dir.exists(config[QString("dataFolder2")]);
        }
        if (config[QString("dataFolder3")] != QString("")) {
            errorsAcqured = errorsAcqured || !dir.exists(config[QString("dataFolder3")]);
        }
        if (config[QString("dataFolder4")] != QString("")) {
            errorsAcqured = errorsAcqured || !dir.exists(config[QString("dataFolder4")]);
        }
        if (config[QString("dataFolder5")] != QString("")) {
            errorsAcqured = errorsAcqured || !dir.exists(config[QString("dataFolder5")]);
        }
        if (config[QString("dataFolder6")] != QString("")) {
            errorsAcqured = errorsAcqured || !dir.exists(config[QString("dataFolder6")]);
        }
        if (config[QString("dataFolder7")] != QString("")) {
            errorsAcqured = errorsAcqured || !dir.exists(config[QString("dataFolder7")]);
        }
    }

    return errorsAcqured;
}

void IniReader::saveConfiguration(QMap<QString, QString> &config, const QString &fileName)
{
    IniReader::checkConfiguration(config);

    QStringList outputText;

    outputText.append("\n# Ini File With Setting");
    outputText.append("\n");
    outputText.append("\nCalibrationMode = ");
    outputText.append(QString::number(config["CalibrationMode"].toInt()));
    outputText.append("\n");
    outputText.append("\n# RadarControlInterface = 0 (Ethernet) / 1 (SerialPort) [default: 0]");
    outputText.append("\nRadarControlInterface = ");
    outputText.append(QString::number(config["RadarControlInterface"].toInt()));
    outputText.append("\n");
    outputText.append("\nSamplingFrequency = ");
    outputText.append(QString::number(config["SamplingFrequency"].toInt()));
    outputText.append("\nFrameLength = ");
    outputText.append(QString::number(config["FrameLength"].toInt()));
    outputText.append("\nAdcBitRateWithSign = ");
    outputText.append(QString::number(config["AdcBitRateWithSign"].toInt()));
    outputText.append("\nAdcMaxValue = ");
    outputText.append(QString::number(config["AdcMaxValue"].toInt()));
    outputText.append("\n");
    outputText.append("\n# MainWindow settings");
    outputText.append("\n# DefaultPlotType = 0 (No Plots) / 1 (Signal Plot) / 2 (Both) / 3 (Polar Plot) [default : 1]");
    outputText.append("\n# DarkMode = 0 (Normal Theme) / 1 (Dark Theme) [default: 0]");
    outputText.append("\n");
    outputText.append("\nDefaultPlotType = ");
    outputText.append(QString::number(config["DefaultPlotType"].toInt()));
    outputText.append("\nDarkMode = ");
    outputText.append(QString::number(config["DarkMode"].toInt()));
    outputText.append("\n");
    outputText.append("\n# Main Plot settings");
    outputText.append("\n# MaxAvgFactor - how much signals must be avaraged in one beam > 0 [default: 1]");
    outputText.append("\n# AngleBitOffset - how much insignificant rank cut from angle value [default: 7]");
    outputText.append("\n# RangeAvgCoef [default: 4]");
    outputText.append("\n# AdcBitRateWithSign [default: 13]");
    outputText.append("\n# MaxPlottingFrequency [default: 25] # Hz");
    outputText.append("\n# Brightness = 0...1000 [default: 100]");
    outputText.append("\n# Contrast = 0...1000 [default: 100]");
    outputText.append("\n# Threshold = 0...100 [default: 0]");
    outputText.append("\n# ColorMode = 0 (Grayscale) / 1 (Hot) / 2 (Cold) / 3 (Night) / 4 (Candy) / 5 (Geography) /");
    outputText.append("\n#             6 (Ion) / 7 (Thermal) / 8 (Polar) / 9 (Spectrum) / 10 (Jet) / 11 (Hues) [default: 0]");
    outputText.append("\n#");
    outputText.append("\n# SignalPlotLogScale = 0/1 [default: 1]");
    outputText.append("\n# SignalPlotAbs = 0/1 [default: 1]");
    outputText.append("\n# SignalPlotRePlot = 0/1 [default: 0]");
    outputText.append("\n# SignalPlotImPlot = 0/1 [default: 0]");
    outputText.append("\n# SignalPlotLimitLower = -120.0...30.0 [default: -40.0]");
    outputText.append("\n# SignalPlotLimitUpper = -120.0...30.0 [default: 0.0]");
    outputText.append("\n#");
    outputText.append("\n# TuningPlotEnanbled = 0/1 [default: 0]");
    outputText.append("\n# TuningPlotAvgLine = 0/1 [default: 0]");
    outputText.append("\n# TuningPlotAvgNumber = 1...1000000 [default: 50]");
    outputText.append("\n# TuningSignalAvgNumber = 1...1000000 [default: 1]");
    outputText.append("\n# TuningSignalAbs = 0/1 [default: 1]");
    outputText.append("\n# TuningSignalRe = 0/1 [default: 1]");
    outputText.append("\n# TuningSignalIm = 0/1 [default: 1]");
    outputText.append("\n# TuningPlotStartSample = 0...31/63/127/255 [default: 8] - max border depends on the channel A size {be very careful}");
    outputText.append("\n# TuningPlotStopSample = 0...31/63/127/255 [default: 0] - lower than Start to have only one sample for analysis");
    outputText.append("\n# TuningSignalShowLimits = 0/1 [default: 0]");
    outputText.append("\n# TuningSignalAnalysisDelay = 0 (Channel A) / 1 (Channel B) [default: 0]");
    outputText.append("\n# TuningSignalCustomAnalysisDelay = 0...2016 [default: 0]");
    outputText.append("\n");
    outputText.append("\n#Circle Diagram");
    outputText.append("\nMaxAvgFactor = ");
    outputText.append(QString::number(config["MaxAvgFactor"].toInt()));
    outputText.append("\nAngleBitOffset = ");
    outputText.append(QString::number(config["AngleBitOffset"].toInt()));
    outputText.append("\nRangeAvgCoef = ");
    outputText.append(QString::number(config["RangeAvgCoef"].toInt()));
    outputText.append("\nMaxPlottingFrequency = ");
    outputText.append(QString::number(config["MaxPlottingFrequency"].toInt()));
    outputText.append("\n");
    outputText.append("\n#Signal Plot");
    outputText.append("\nSignalPlotLogScale = ");
    outputText.append(QString::number(config["SignalPlotLogScale"].toInt()));
    outputText.append("\nSignalPlotLimitLowerLog = ");
    outputText.append(QString::number(config["SignalPlotLimitLowerLog"].toDouble()));
    outputText.append("\nSignalPlotLimitUpperLog = ");
    outputText.append(QString::number(config["SignalPlotLimitUpperLog"].toDouble()));
    outputText.append("\nSignalPlotLimitLowerLin = ");
    outputText.append(QString::number(config["SignalPlotLimitLowerLin"].toDouble()));
    outputText.append("\nSignalPlotLimitUpperLin = ");
    outputText.append(QString::number(config["SignalPlotLimitUpperLin"].toDouble()));
    outputText.append("\n");
    outputText.append("\nSignalPlotAbs = ");
    outputText.append(QString::number(config["SignalPlotAbs"].toInt()));
    outputText.append("\nSignalPlotRePlot = ");
    outputText.append(QString::number(config["SignalPlotRePlot"].toInt()));
    outputText.append("\nSignalPlotImPlot = ");
    outputText.append(QString::number(config["SignalPlotImPlot"].toInt()));
    outputText.append("\n");
    outputText.append("\nBrightness = ");
    outputText.append(QString::number(config["Brightness"].toInt()));
    outputText.append("\nContrast = ");
    outputText.append(QString::number(config["Contrast"].toInt()));
    outputText.append("\nThreshold = ");
    outputText.append(QString::number(config["Threshold"].toInt()));
    outputText.append("\nColorMode = ");
    outputText.append(QString::number(config["ColorMode"].toInt()));
    outputText.append("\n");
    outputText.append("\n#Tuning Calibration Plot");
    outputText.append("\nTuningPlotEnanbled = ");
    outputText.append(QString::number(config["TuningPlotEnanbled"].toInt()));
    outputText.append("\nTuningPlotAvgLine = ");
    outputText.append(QString::number(config["TuningPlotAvgLine"].toInt()));
    outputText.append("\nTuningPlotAvgNumber = ");
    outputText.append(QString::number(config["TuningPlotAvgNumber"].toInt()));
    outputText.append("\nTuningSignalAvgNumber = ");
    outputText.append(QString::number(config["TuningSignalAvgNumber"].toInt()));
    outputText.append("\nTuningSignalAbs = ");
    outputText.append(QString::number(config["TuningSignalAbs"].toInt()));
    outputText.append("\nTuningSignalRe = ");
    outputText.append(QString::number(config["TuningSignalRe"].toInt()));
    outputText.append("\nTuningSignalIm = ");
    outputText.append(QString::number(config["TuningSignalIm"].toInt()));
    outputText.append("\nTuningPlotStartSample = ");
    outputText.append(QString::number(config["TuningPlotStartSample"].toInt()));
    outputText.append("\nTuningPlotStopSample = ");
    outputText.append(QString::number(config["TuningPlotStopSample"].toInt()));
    outputText.append("\nTuningSignalShowLimits = ");
    outputText.append(QString::number(config["TuningSignalShowLimits"].toInt()));
    outputText.append("\nTuningSignalAnalysisDelay = ");
    outputText.append(QString::number(config["TuningSignalAnalysisDelay"].toInt()));
    outputText.append("\nTuningSignalCustomAnalysisDelay = ");
    outputText.append(QString::number(config["TuningSignalCustomAnalysisDelay"].toInt()));
    outputText.append("\n");
    outputText.append("\n# CalibrationAvgFactor - how much signals must be avaraged in one beam > 0 [default: 1]");
    outputText.append("\n# CalibrationAngleBitOffset - how much insignificant rank cut from angle value [default: 7]");
    outputText.append("\n# CalibrationRangeAvgCoef [default: 4]");
    outputText.append("\n# ExactNumberOfAvg = 0 (Averaging every beam at least N times)");
    outputText.append("\n#                    1 (Averaging every beam exactly N times) [default: 0] ");
    outputText.append("\n# CalibrationDefaultAvgCoefficient = 1...9999 [default: 10]");
    outputText.append("\n# CalibrationMaxPlottingFrequency [default: 15] # Hz");
    outputText.append("\n");
    outputText.append("\nCalibrationMaxPlottingFrequency = ");
    outputText.append(QString::number(config["CalibrationMaxPlottingFrequency"].toInt()));
    outputText.append("\nCalibrationAvgFactor = ");
    outputText.append(QString::number(config["CalibrationAvgFactor"].toInt()));
    outputText.append("\nCalibrationAngleBitOffset = ");
    outputText.append(QString::number(config["CalibrationAngleBitOffset"].toInt()));
    outputText.append("\nCalibrationRangeAvgCoef = ");
    outputText.append(QString::number(config["CalibrationRangeAvgCoef"].toInt()));
    outputText.append("\nExactNumberOfAvg = ");
    outputText.append(QString::number(config["ExactNumberOfAvg"].toInt()));
    outputText.append("\nCalibrationDefaultAvgCoefficient = ");
    outputText.append(QString::number(config["CalibrationDefaultAvgCoefficient"].toInt()));
    outputText.append("\n");
    outputText.append("\n# GreenBoard Ethernet Settings");
    outputText.append("\n# PcHostAddress = [default: 192.168.0.1]");
    outputText.append("\n# PcControlPort = [default: 0x4020 = 16416]");
    outputText.append("\n# PcDataPort = [default: 0x4002 = 16386]");
    outputText.append("\n# BoardHostAddress = [default: 192.168.0.2]");
    outputText.append("\n# BoardControlPort = [default: 0x4060 = 16480]");
    outputText.append("\n# BoardDataPort = [default: 0x4061 = 16481]");
    outputText.append("\n");
    outputText.append("\nPcHostAddress = ");
    outputText.append(config["PcHostAddress"]);
    outputText.append("\nPcControlPort = ");
    outputText.append(config["PcControlPort"]);
    outputText.append("\nPcDataPort = ");
    outputText.append(config["PcDataPort"]);
    outputText.append("\nBoardHostAddress = ");
    outputText.append(config["BoardHostAddress"]);
    outputText.append("\nBoardControlPort = ");
    outputText.append(config["BoardControlPort"]);
    outputText.append("\nBoardDataPort = ");
    outputText.append(config["BoardDataPort"]);
    outputText.append("\n");
    outputText.append("\n# GreenBoard Main Settings");
    outputText.append("\n# BoardRxEnable = 0 (Disabled) / 1 (Enabled) [default: 0]");
    outputText.append("\n# BoardClockSignalSource = 0 (Inrernal) / 1 (External) [default: 0]");
    outputText.append("\n# BoardAdcOperationMode = 0 (Normal) / 1 (Debug) [default: 0]");
    outputText.append("\n# BoardDecimation = 0...6 (Decimation by 2^i: 1...128) [default: 0]");
    outputText.append("\n# BoardInrernalTriggerFrequency = 0 (1kHz) / 1 (2kHz) [default: 1]");
    outputText.append("\n# BoardTriggerSignalSource = 0 (Internal) / 1 (External) [default: 0]");
    outputText.append("\n# BoardAdcADelay = 0...65535 [default: 0] ");
    outputText.append("\n# BoardAdcBDelay = 0...65535 [default: 32]");
    outputText.append("\n# BoardAdcDelaySynchro = 0 (Separated Control) / 1 (Synchronyzed Control) [default: 1]");
    outputText.append("\n# BoardAngleOffset = 0.0...360.0 [default: 0]");
    outputText.append("\n# SignalAvgCoefficient = 1...2000 [default: 1]");
    outputText.append("\n# DefaultFileName = %path% [default: test.ds]");
    outputText.append("\n# StartSavingTreadAutomaticly = 0 (No) / 1 (Yes) [default: 0]");
    outputText.append("\n# BoardTriggerFrequency = 0 (1kHz) / 1 (2kHz) / 2 (3kHz) / 3 (4kHz) [default: 1]");
    outputText.append("\n# BoardTriggerOff = 0 (Enabled) / 1 (Disabled) [default: 0]");
    outputText.append("\n# BoardAdcALength = 0 (32) / 1 (64) / 2 (128) / 3 (256) [default: 0]");
    outputText.append("\n# MaxFileSize = 0 (128 Mb) / 1 (256 Mb) / 2 (512 Mb) / 3 (1024 Mb) / 4 (2048 Mb) / ");
    outputText.append("\n#               5 (4096 Mb) / 6 (8192 Mb) / 7 (16384 Mb) / 8 (32768 Mb) [default: 5]");
    outputText.append("\n# FirstSavingDatagram = 0...7 [default: 0]");
    outputText.append("\n# LastSavingDatagram = 0...7 [default: 7]");
    outputText.append("\n# MotorControlAutoStart = 0 (Disabled) / 1 (Enabled) [default: 0]");
    outputText.append("\n# BlindSector{Num}Enabled = 0 / 1 [default: 1]");
    outputText.append("\n# BlindSector{Num}Begin = 0.0...360.0 [default: 0.0]");
    outputText.append("\n# BlindSector{Num}End = 0.0...360.0 [default: 0.0]");
    outputText.append("\n# FreeDiskSpaceWarning = 0...100% of Disk Space [default: 10]");
    outputText.append("\n# FreeDiskSpaceStopSaving = 0...100% of Disk Space [default: 5]");
    outputText.append("\n# ContinueSavingAfterPathChanged = 0 (Disabled) / 1 (Enabled) [default: 0]");
    outputText.append("\n");
    outputText.append("\nBoardRxEnable = ");
    outputText.append(QString::number(config["BoardRxEnable"].toInt()));
    outputText.append("\nBoardClockSignalSource = ");
    outputText.append(QString::number(config["BoardClockSignalSource"].toInt()));
    outputText.append("\nBoardAdcOperationMode = ");
    outputText.append(QString::number(config["BoardAdcOperationMode"].toInt()));
    outputText.append("\nBoardDecimation = ");
    outputText.append(QString::number(config["BoardDecimation"].toInt()));
    outputText.append("\nBoardInrernalTriggerFrequency = ");
    outputText.append(QString::number(config["BoardInrernalTriggerFrequency"].toInt()));
    outputText.append("\nBoardTriggerSignalSource = ");
    outputText.append(QString::number(config["BoardTriggerSignalSource"].toInt()));
    outputText.append("\nBoardAdcADelay = ");
    outputText.append(QString::number(config["BoardAdcADelay"].toInt()));
    outputText.append("\nBoardAdcBDelay = ");
    outputText.append(QString::number(config["BoardAdcBDelay"].toInt()));
    outputText.append("\nBoardAdcDelaySynchro = ");
    outputText.append(QString::number(config["BoardAdcDelaySynchro"].toInt()));
    outputText.append("\nBoardAngleOffset = ");
    outputText.append(QString::number(config["BoardAngleOffset"].toDouble()));
    outputText.append("\nSignalAvgCoefficient = ");
    outputText.append(QString::number(config["SignalAvgCoefficient"].toInt()));
    outputText.append("\nDefaultFileName = ");
    outputText.append(config["DefaultFileName"]);
    outputText.append("\nStartSavingTreadAutomaticly = ");
    outputText.append(QString::number(config["StartSavingTreadAutomaticly"].toInt()));
    outputText.append("\nBoardTriggerFrequency = ");
    outputText.append(QString::number(config["BoardTriggerFrequency"].toInt()));
    outputText.append("\nBoardTriggerOff = ");
    outputText.append(QString::number(config["BoardTriggerOff"].toInt()));
    outputText.append("\nBoardAdcALength = ");
    outputText.append(QString::number(config["BoardAdcALength"].toInt()));
    outputText.append("\nMaxFileSize = ");
    outputText.append(QString::number(config["MaxFileSize"].toInt()));
    outputText.append("\nFirstSavingDatagram = ");
    outputText.append(QString::number(config["FirstSavingDatagram"].toInt()));
    outputText.append("\nLastSavingDatagram = ");
    outputText.append(QString::number(config["LastSavingDatagram"].toInt()));
    outputText.append("\nMotorControlAutoStart = ");
    outputText.append(QString::number(config["MotorControlAutoStart"].toInt()));
    outputText.append("\nFreeDiskSpaceWarning = ");
    outputText.append(QString::number(config["FreeDiskSpaceWarning"].toInt()));
    outputText.append("\nFreeDiskSpaceStopSaving = ");
    outputText.append(QString::number(config["FreeDiskSpaceStopSaving"].toInt()));
    outputText.append("\nContinueSavingAfterPathChanged = ");
    outputText.append(QString::number(config["ContinueSavingAfterPathChanged"].toInt()));
    outputText.append("\n");
    outputText.append("\nBlindSectorOneEnabled = ");
    outputText.append(QString::number(config["BlindSectorOneEnabled"].toInt()));
    outputText.append("\nBlindSectorOneBegin = ");
    outputText.append(QString::number(config["BlindSectorOneBegin"].toDouble()));
    outputText.append("\nBlindSectorOneEnd = ");
    outputText.append(QString::number(config["BlindSectorOneEnd"].toDouble()));
    outputText.append("\nBlindSectorTwoEnabled = ");
    outputText.append(QString::number(config["BlindSectorTwoEnabled"].toInt()));
    outputText.append("\nBlindSectorTwoBegin = ");
    outputText.append(QString::number(config["BlindSectorTwoBegin"].toDouble()));
    outputText.append("\nBlindSectorTwoEnd = ");
    outputText.append(QString::number(config["BlindSectorTwoEnd"].toDouble()));
    outputText.append("\nBlindSectorThreeEnabled = ");
    outputText.append(QString::number(config["BlindSectorThreeEnabled"].toInt()));
    outputText.append("\nBlindSectorThreeBegin = ");
    outputText.append(QString::number(config["BlindSectorThreeBegin"].toDouble()));
    outputText.append("\nBlindSectorThreeEnd = ");
    outputText.append(QString::number(config["BlindSectorThreeEnd"].toDouble()));
    outputText.append("\n");
    outputText.append("\n# EthernetRADAR Main Settings");
    outputText.append("\n");
    outputText.append("\n# EthernetSettings");
    outputText.append("\n# HostAddrressPC = [default: 192.168.42.255]");
    outputText.append("\n# ReadPortPC = [default: 5004]");
    outputText.append("\n# WritePortPC = [default: 5005]");
    outputText.append("\n# HostAddressRADAR = [default: 192.168.42.33]");
    outputText.append("\n# ReadPortRADAR = [default: 5004]");
    outputText.append("\n# WritePortRADAR = [default: 5005]");
    outputText.append("\n");
    outputText.append("\nHostAddrressPC = ");
    outputText.append(config["HostAddrressPC"]);
    outputText.append("\nReadPortPC = ");
    outputText.append(config["ReadPortPC"]);
    outputText.append("\nWritePortPC = ");
    outputText.append(config["WritePortPC"]);
    outputText.append("\nHostAddressRADAR = ");
    outputText.append(config["HostAddressRADAR"]);
    outputText.append("\nReadPortRADAR = ");
    outputText.append(config["ReadPortRADAR"]);
    outputText.append("\nWritePortRADAR = ");
    outputText.append(config["WritePortRADAR"]);
    outputText.append("\n");
    outputText.append("\n# RadarTriggerGeneratorStateUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarTriggerPeriodUDP = 0x03D08 = 15624 (PRF3200Hz) / 0x07A11 = 31249 (PRF1600Hz) / ");
    outputText.append("\n#                      0x0D423 = 54307 (PRF800Hz) / 0x1869F = 99999 (PRF500Hz) ");
    outputText.append("\n#                      [default: -1");
    outputText.append("\n#");
    outputText.append("\n# RadarTriggerStaggerValueUDP = 0...15 [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarTriggerDelayValueUDP = 0...65535 [default -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarADCSampligFrequencyUDP = 0 (ADSF50000kHz) / 1 (ADSF48387kHz) / ");
    outputText.append("\n#                             2 (ADSF52273kHz) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarMiscZeroBlankMemComtrolUDP = 0 (SerialControl) / 1 (EthernetControl) [default: -1]");
    outputText.append("\n# RadarMiscZeroTestACPUDP = 0 (NormalOperation) / 1 (AcpArpInternalGen) [default: -1]");
    outputText.append("\n# RadarMiscZeroAutoTuneFineUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarMiscZeroAutoTuneRegUDP = 0 (NormalOperation) / 1 (AutomaticSearching) [default: -1]");
    outputText.append("\n# RadarMiscZeroDivideACPInputUDP = 0 (NoDivide) / 1 (DivideByTwo) / 2 (DivideByFour) / ");
    outputText.append("\n#                               3 (DivideByEight) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarMiscOneVideoSampleResUDP = 0 (DefaultBS) / 1 (BSdivide2) / 2 (BSdivide5) /");
    outputText.append("\n#                               3 (BSdivide10) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarMiscOneAcpPeriodUDP = 1 (ACP2048Pulses) / 2 (ACP4096Pulses) [default: -1]");
    outputText.append("\n# RadarMiscOneBlankingRamUDP = 0 (BlockFrom0to511) / 1 (BlockFrom512to1023) [default: -1]");
    outputText.append("\n# RadarMiscOneBandwithUDP = 0 (WideBand) / 1 (NarrowBand) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarHlSetupWidthUDP = 0 (AcpHalfPulse) / 1 (AcpOnePulse) / 2 (AcpTwoPulses) / ");
    outputText.append("\n#                     3 (AcpFourPulses) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarHlSetupInvertUDP = 0 (SamePolarity) / 1 (InversePolarity) [default: -1]");
    outputText.append("\n# RadarHlSetupEdgeUDP = 0 (FallingEdge) / 1 (RisingEdge) [default: -1]");
    outputText.append("\n# RadarHlSetupDelayValueUDP = 0...4095 [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarEnableVideoAcquisitionUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarEnableDoubleBufferUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarEnableAutoTuneUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarEnableDAConvertersUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarEnableADConvertersUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarTuneCoarseUDP = 0...255 [default: -1]");
    outputText.append("\n# RadarTuneFineUDP = 0...255 [default: -1]");
    outputText.append("\n# RadarTuneRegUDP = 0...255 [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarSetupTransceiverModeUDP = 0 (TransceiverStandBy) / 1 (TransceiverTxMode) [default: -1]");
    outputText.append("\n# RadarSetupPulseModeUDP = 0 (ExtraLongPulse) / 1 (LongPolse) / 2 (MediumPulse) / ");
    outputText.append("\n#                        3 (ShortPulse) / 4 (ExtraShortPilse) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarSetupEnableMotorUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarGateTuneDelayValueUDP = 0...255 [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarAntSpeedControlUDP = 0 (HalfStepControlled) / 1 (FullStepControlled) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarAntSpeedValueUDP =  0 (Speed33RPM) / 1 (Speed30RPM) / 2 (Speed27RPM) / ");
    outputText.append("\n#                        3 (Speed24RPM) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarMasterIPLowUDP = 0...255 [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarDebugNoMasterModeUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarDebugTxTestModeUDP = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarDebugPowerUpModeUDP = 0 (Disabled) / 1 (SkipPowerUpTime) [default: -1]");
    outputText.append("\n");
    outputText.append("\nRadarTriggerGeneratorStateUDP = ");
    outputText.append(QString::number(config["RadarTriggerGeneratorStateUDP"].toInt()));
    outputText.append("\nRadarTriggerPeriodUDP = ");
    outputText.append(QString::number(config["RadarTriggerPeriodUDP"].toInt()));
    outputText.append("\nRadarTriggerStaggerValueUDP = ");
    outputText.append(QString::number(config["RadarTriggerStaggerValueUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarTriggerDelayValueUDP = ");
    outputText.append(QString::number(config["RadarTriggerDelayValueUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarADCSampligFrequencyUDP = ");
    outputText.append(QString::number(config["RadarADCSampligFrequencyUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarMiscZeroBlankMemComtrolUDP = ");
    outputText.append(QString::number(config["RadarMiscZeroBlankMemComtrolUDP"].toInt()));
    outputText.append("\nRadarMiscZeroTestACPUDP = ");
    outputText.append(QString::number(config["RadarMiscZeroTestACPUDP"].toInt()));
    outputText.append("\nRadarMiscZeroAutoTuneFineUDP = ");
    outputText.append(QString::number(config["RadarMiscZeroAutoTuneFineUDP"].toInt()));
    outputText.append("\nRadarMiscZeroAutoTuneRegUDP = ");
    outputText.append(QString::number(config["RadarMiscZeroAutoTuneRegUDP"].toInt()));
    outputText.append("\nRadarMiscZeroDivideACPInputUDP = ");
    outputText.append(QString::number(config["RadarMiscZeroDivideACPInputUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarMiscOneVideoSampleResUDP = ");
    outputText.append(QString::number(config["RadarMiscOneVideoSampleResUDP"].toInt()));
    outputText.append("\nRadarMiscOneAcpPeriodUDP = ");
    outputText.append(QString::number(config["RadarMiscOneAcpPeriodUDP"].toInt()));
    outputText.append("\nRadarMiscOneBlankingRamUDP = ");
    outputText.append(QString::number(config["RadarMiscOneBlankingRamUDP"].toInt()));
    outputText.append("\nRadarMiscOneBandwithUDP = ");
    outputText.append(QString::number(config["RadarMiscOneBandwithUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarHlSetupWidthUDP = ");
    outputText.append(QString::number(config["RadarHlSetupWidthUDP"].toInt()));
    outputText.append("\nRadarHlSetupInvertUDP = ");
    outputText.append(QString::number(config["RadarHlSetupInvertUDP"].toInt()));
    outputText.append("\nRadarHlSetupEdgeUDP = ");
    outputText.append(QString::number(config["RadarHlSetupEdgeUDP"].toInt()));
    outputText.append("\nRadarHlSetupDelayValueUDP = ");
    outputText.append(QString::number(config["RadarHlSetupDelayValueUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarEnableVideoAcquisitionUDP = ");
    outputText.append(QString::number(config["RadarEnableVideoAcquisitionUDP"].toInt()));
    outputText.append("\nRadarEnableDoubleBufferUDP = ");
    outputText.append(QString::number(config["RadarEnableDoubleBufferUDP"].toInt()));
    outputText.append("\nRadarEnableAutoTuneUDP = ");
    outputText.append(QString::number(config["RadarEnableAutoTuneUDP"].toInt()));
    outputText.append("\nRadarEnableDAConvertersUDP = ");
    outputText.append(QString::number(config["RadarEnableDAConvertersUDP"].toInt()));
    outputText.append("\nRadarEnableADConvertersUDP = ");
    outputText.append(QString::number(config["RadarEnableADConvertersUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarTuneCoarseUDP = ");
    outputText.append(QString::number(config["RadarTuneCoarseUDP"].toInt()));
    outputText.append("\nRadarTuneFineUDP = ");
    outputText.append(QString::number(config["RadarTuneFineUDP"].toInt()));
    outputText.append("\nRadarTuneRegUDP = ");
    outputText.append(QString::number(config["RadarTuneRegUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarSetupTransceiverModeUDP = ");
    outputText.append(QString::number(config["RadarSetupTransceiverModeUDP"].toInt()));
    outputText.append("\nRadarSetupPulseModeUDP = ");
    outputText.append(QString::number(config["RadarSetupPulseModeUDP"].toInt()));
    outputText.append("\nRadarSetupEnableMotorUDP = ");
    outputText.append(QString::number(config["RadarSetupEnableMotorUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarGateTuneDelayValueUDP = ");
    outputText.append(QString::number(config["RadarGateTuneDelayValueUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarAntSpeedControlUDP = ");
    outputText.append(QString::number(config["RadarAntSpeedControlUDP"].toInt()));
    outputText.append("\nRadarAntSpeedValueUDP = ");
    outputText.append(QString::number(config["RadarAntSpeedValueUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarMasterIPLowUDP = ");
    outputText.append(QString::number(config["RadarMasterIPLowUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarDebugNoMasterModeUDP = ");
    outputText.append(QString::number(config["RadarDebugNoMasterModeUDP"].toInt()));
    outputText.append("\nRadarDebugTxTestModeUDP = ");
    outputText.append(QString::number(config["RadarDebugTxTestModeUDP"].toInt()));
    outputText.append("\nRadarDebugPowerUpModeUDP = ");
    outputText.append(QString::number(config["RadarDebugPowerUpModeUDP"].toInt()));
    outputText.append("\n");
    outputText.append("\n# SerialRADAR Main Settings");
    outputText.append("\n");
    outputText.append("\n# SerialPortSettings");
    outputText.append("\n# SerialPortName = %portname% [default: /dev/ttyUSB0]");
    outputText.append("\n# SerialPortBaudRate = 0 (9600) / 1 (1200) / 2 (2400) / 3 (4800) / 4 (9600) / ");
    outputText.append("\n#                      5 (19200) / 6 (38400) / 7 (57600) [default: 0 or 4]");
    outputText.append("\n#");
    outputText.append("\n# SerialPortDataBits = 0 (8 Bits) / 1 (5 Bits) / 2 (6 Bits) / 3 (7 Bits) / ");
    outputText.append("\n#                      4 (8 Bits) [default: 0 or 4]");
    outputText.append("\n#");
    outputText.append("\n# SerialPortStopBits = 0 (1 Bit) / 1 (1 Bit) / 2 (1.5 Bits) / 3 (2 Bits) [default: 0 or 1]");
    outputText.append("\n# SerialPortParity = 0 (NoParity) / 1 (NoParity) / 2 (OddParity) / 3 (SpaceParity) / ");
    outputText.append("\n#                    4 (MarkParity) [default: 0 or 1]");
    outputText.append("\n#");
    outputText.append("\n# SerialPortFlowControl = 0 (NoFlowControl) / 1 (NoFlowControl) / 2 (HardwareControl) / ");
    outputText.append("\n#                         3 (SoftwareControl) [default: 0 or 1]");
    outputText.append("\n#");
    outputText.append("\n#SerialPortDirection = 0 (AllDirections) / 1 (Input) / 2 (Output) / 3 (AllDirections)");
    outputText.append("\n#                      [default: 0 or 3]");
    outputText.append("\n");
    outputText.append("\nSerialPortName = ");
    outputText.append(config["SerialPortName"]);
    outputText.append("\nSerialPortBaudRate = ");
    outputText.append(QString::number(config["SerialPortBaudRate"].toInt()));
    outputText.append("\nSerialPortDataBits = ");
    outputText.append(QString::number(config["SerialPortDataBits"].toInt()));
    outputText.append("\nSerialPortStopBits = ");
    outputText.append(QString::number(config["SerialPortStopBits"].toInt()));
    outputText.append("\nSerialPortParity = ");
    outputText.append(QString::number(config["SerialPortParity"].toInt()));
    outputText.append("\nSerialPortFlowControl = ");
    outputText.append(QString::number(config["SerialPortFlowControl"].toInt()));
    outputText.append("\nSerialPortDirection = ");
    outputText.append(QString::number(config["SerialPortDirection"].toInt()));
    outputText.append("\n");
    outputText.append("\n# RadarTriggerGeneratorStateRS = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarTriggerPeriodRS = 0 (PRF3200Hz) / 1 (PRF1600Hz) / ");
    outputText.append("\n#                      2 (PRF800Hz) / 3 (PRF500Hz) ");
    outputText.append("\n#                      [default: -1");
    outputText.append("\n#");
    outputText.append("\n# RadarTriggerStaggerValueRS = 0...15 [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarMiscZeroTestACPRS = 0 (NormalOperation) / 1 (AcpArpInternalGen) [default: -1]");
    outputText.append("\n# RadarMiscZeroAutoTuneFineRS = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarMiscZeroAutoTuneRegRS = 0 (NormalOperation) / 1 (AutomaticSearching) [default: -1]");
    outputText.append("\n# RadarMiscZeroDivideACPInputRS = 0 (NoDivide) / 1 (DivideByTwo) / 2 (DivideByFour) / ");
    outputText.append("\n#                               3 (DivideByEight) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarMiscOneAcpPeriodRS = 1 (ACP2048Pulses) / 2 (ACP4096Pulses) [default: -1]");
    outputText.append("\n# RadarMiscOneBlankingRamRS = 0 (BlockFrom0to511) / 1 (BlockFrom512to1023) [default: -1]");
    outputText.append("\n# RadarMiscOneBandwithRS = 0 (WideBand) / 1 (NarrowBand) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarHlSetupWidthRS = 0 (AcpHalfPulse) / 1 (AcpOnePulse) / 2 (AcpTwoPulses) / ");
    outputText.append("\n#                     3 (AcpFourPulses) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n# RadarHlSetupInvertRS = 0 (SamePolarity) / 1 (InversePolarity) [default: -1]");
    outputText.append("\n# RadarHlSetupEdgeRS = 0 (FallingEdge) / 1 (RisingEdge) [default: -1]");
    outputText.append("\n# RadarHlSetupDelayValueRS = 0...4095 [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarEnableAutoTuneRS = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarEnableDAConvertersRS = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n# RadarEnableADConvertersRS = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarTuneCoarseRS = 0...255 [default: -1]");
    outputText.append("\n# RadarTuneFineRS = 0...255 [default: -1]");
    outputText.append("\n# RadarTuneRegRS = 0...255 [default: -1]");
    outputText.append("\n#");
    outputText.append("\n#");
    outputText.append("\n# RadarSetupTransceiverModeRS = 0 (TransceiverStandBy) / 1 (TransceiverTxMode) [default: -1]");
    outputText.append("\n# RadarSetupPulseModeRS = 0 (ExtraLongPulse) / 1 (LongPolse) / 2 (MediumPulse) / ");
    outputText.append("\n#                       3 (ShortPulse) [default: -1]");
    outputText.append("\n");
    outputText.append("\n# RadarSetupEnableMotorRS = 0 (Disabled) / 1 (Enabled) [default: -1]");
    outputText.append("\n");
    outputText.append("\nRadarTriggerGeneratorStateRS = ");
    outputText.append(QString::number(config["RadarTriggerGeneratorStateRS"].toInt()));
    outputText.append("\nRadarTriggerPeriodRS = ");
    outputText.append(QString::number(config["RadarTriggerPeriodRS"].toInt()));
    outputText.append("\nRadarTriggerStaggerValueRS = ");
    outputText.append(QString::number(config["RadarTriggerStaggerValueRS"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarMiscZeroTestACPRS = ");
    outputText.append(QString::number(config["RadarMiscZeroTestACPRS"].toInt()));
    outputText.append("\nRadarMiscZeroAutoTuneFineRS = ");
    outputText.append(QString::number(config["RadarMiscZeroAutoTuneFineRS"].toInt()));
    outputText.append("\nRadarMiscZeroAutoTuneRegRS = ");
    outputText.append(QString::number(config["RadarMiscZeroAutoTuneRegRS"].toInt()));
    outputText.append("\nRadarMiscZeroDivideACPInputRS = ");
    outputText.append(QString::number(config["RadarMiscZeroDivideACPInputRS"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarMiscOneAcpPeriodRS = ");
    outputText.append(QString::number(config["RadarMiscOneAcpPeriodRS"].toInt()));
    outputText.append("\nRadarMiscOneBlankingRamRS = ");
    outputText.append(QString::number(config["RadarMiscOneBlankingRamRS"].toInt()));
    outputText.append("\nRadarMiscOneBandwithRS = ");
    outputText.append(QString::number(config["RadarMiscOneBandwithRS"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarHlSetupWidthRS = ");
    outputText.append(QString::number(config["RadarHlSetupWidthRS"].toInt()));
    outputText.append("\nRadarHlSetupInvertRS = ");
    outputText.append(QString::number(config["RadarHlSetupInvertRS"].toInt()));
    outputText.append("\nRadarHlSetupEdgeRS = ");
    outputText.append(QString::number(config["RadarHlSetupEdgeRS"].toInt()));
    outputText.append("\nRadarHlSetupDelayValueRS = ");
    outputText.append(QString::number(config["RadarHlSetupDelayValueRS"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarEnableAutoTuneRS = ");
    outputText.append(QString::number(config["RadarEnableAutoTuneRS"].toInt()));
    outputText.append("\nRadarEnableDAConvertersRS = ");
    outputText.append(QString::number(config["RadarEnableDAConvertersRS"].toInt()));
    outputText.append("\nRadarEnableADConvertersRS = ");
    outputText.append(QString::number(config["RadarEnableADConvertersRS"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarTuneCoarseRS = ");
    outputText.append(QString::number(config["RadarTuneCoarseRS"].toInt()));
    outputText.append("\nRadarTuneFineRS = ");
    outputText.append(QString::number(config["RadarTuneFineRS"].toInt()));
    outputText.append("\nRadarTuneRegRS = ");
    outputText.append(QString::number(config["RadarTuneRegRS"].toInt()));
    outputText.append("\n");
    outputText.append("\nRadarSetupTransceiverModeRS = ");
    outputText.append(QString::number(config["RadarSetupTransceiverModeRS"].toInt()));
    outputText.append("\nRadarSetupPulseModeRS = ");
    outputText.append(QString::number(config["RadarSetupPulseModeRS"].toInt()));
    outputText.append("\nRadarSetupEnableMotorRS = ");
    outputText.append(QString::number(config["RadarSetupEnableMotorRS"].toInt()));
    outputText.append("\n");

    QFile outFile(fileName);
    if (outFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream fileTextStreamOut(&outFile);
        for (int index = 0; index < outputText.size(); index++) {
            fileTextStreamOut << outputText[index];
        }
        outFile.close();
    }
}

void IniReader::saveBasicConfiguration(QMap<QString, QString> &config, const QString &fileName)
{
    QStringList outputText;

    outputText.append("config=");
    outputText.append(config["config"]);
    outputText.append(";");
    outputText.append("\nlogs=");
    outputText.append(config["logs"]);
    outputText.append(";");
    outputText.append("\nicons=");
    outputText.append(config["icons"]);
    outputText.append(";");
    outputText.append("\ntr=");
    outputText.append(config["tr"]);
    outputText.append(";");
    outputText.append("\nschedule=");
    outputText.append(config["schedule"]);
    outputText.append(";");
    outputText.append("\nautostart=");
    outputText.append(QString::number(config["autostart"].toUInt()));
    outputText.append(";");
    outputText.append("\ndataFolder0=");
    outputText.append(config["dataFolder0"]);
    outputText.append(";");
    outputText.append("\ndataFolder1=");
    outputText.append(config["dataFolder1"]);
    outputText.append(";");
    outputText.append("\ndataFolder2=");
    outputText.append(config["dataFolder2"]);
    outputText.append(";");
    outputText.append("\ndataFolder3=");
    outputText.append(config["dataFolder3"]);
    outputText.append(";");
    outputText.append("\ndataFolder4=");
    outputText.append(config["dataFolder4"]);
    outputText.append(";");
    outputText.append("\ndataFolder5=");
    outputText.append(config["dataFolder5"]);
    outputText.append(";");
    outputText.append("\ndataFolder6=");
    outputText.append(config["dataFolder6"]);
    outputText.append(";");
    outputText.append("\ndataFolder7=");
    outputText.append(config["dataFolder7"]);
    outputText.append(";");

    QFile outFile(fileName);
    if (outFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream fileTextStreamOut(&outFile);
        for (int index = 0; index < outputText.size(); index++) {
            fileTextStreamOut << outputText[index];
        }
        outFile.close();
    }
}

void IniReader::saveRedirectionLink(const QString &fileName)
{
    QStringList outputText;

    outputText.append("\nredirectionLink=");
    outputText.append(fileName);
    outputText.append(";");

    QString bsicDirectory = QString("resources/settings/radar.ini");
    QFile outFile(bsicDirectory);
    if (outFile.open(QIODevice::Append | QIODevice::Text)) {
        QTextStream fileTextStreamOut(&outFile);
        for (int index = 0; index < outputText.size(); index++) {
            fileTextStreamOut << outputText[index];
        }
        outFile.close();
    }
}
