/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "applicationlogger.h"

#include <QDateTime>
#include <QFile>
#include <QTextStream>

#include "shared-libs/global.h"

ApplicationLogger::ApplicationLogger(const QString &logDirectory, QObject *parent) : QThread(parent)
{
    m_mutex = new QMutex;

    m_logFileName = logDirectory + g_pathDevider + QDateTime::currentDateTime().toString(QString("yyyy_MM_dd__hh_mm_ss")) + QString("_RadarControl.radarlog");

    m_logFile = new QFile(m_logFileName);
    if (m_logFile->open(QFile::WriteOnly)) {
        QString fileHeader = QString("[") + QDateTime::currentDateTime().toString(QString("dd.MM.yyyy hh:mm:ss")) + QString("] Logger: Radar Log File Has Been Created");
        QTextStream fileTextStreamOut(m_logFile);
        fileTextStreamOut << fileHeader;
        m_logFile->close();

        m_logEventsList = new QStringList;
        startWorkingThread();
    }
}

ApplicationLogger::~ApplicationLogger()
{
    if (m_workingThreadEnable) {
        stopWorkingThread();
        msleep(10);
    }
}

void ApplicationLogger::run()
{
    if (m_workingThreadEnable) {
        emit poolStatus(true);
    }

    QStringList logEventsList;

    while (m_workingThreadEnable) {

        m_mutex->lock();
        logEventsList.append(*m_logEventsList);
        m_logEventsList->clear();
        m_mutex->unlock();

        if (!logEventsList.isEmpty()) {
            if (m_logFile->open(QFile::ReadOnly)) {
                QByteArray fileByteArray(m_logFile->readAll());
                m_logFile->close();
                QTextStream fileTextStream(fileByteArray);
                QString line;
                do {
                    line = fileTextStream.readLine();
                    if (line != QString("")) {
                        logEventsList.append(QString("\n") + line);
                    }
                } while (!line.isNull());
            }

            if (m_logFile->open(QIODevice::WriteOnly | QIODevice::Text)) {
                QTextStream fileTextStreamOut(m_logFile);
                for (int index = 0; index < logEventsList.size(); index++) {
                    fileTextStreamOut << logEventsList[index];
                }
                logEventsList.clear();
                m_logFile->close();
            }
        }

        msleep(10);
    }
    if (!m_workingThreadEnable) {
        emit poolStatus(false);
    }

    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();

}

bool ApplicationLogger::isWorking()
{
    m_mutex->lock();
    bool status = m_workingThreadEnable;
    m_mutex->unlock();
    return status;
}

void ApplicationLogger::startWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = true;
    m_mutex->unlock();

    if (!isRunning()) {
        start();
        //        setPriority(Priority::TimeCriticalPriority);
    }
}

void ApplicationLogger::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

void ApplicationLogger::SlotLogEvent(const QString &logEvent)
{
    QString loggingString =  QString("\n[") + QDateTime::currentDateTime().toString(QString("dd.MM.yyyy hh:mm:ss")) + QString("] ") + logEvent;
    m_mutex->lock();
    m_logEventsList->insert(0, loggingString);
    m_mutex->unlock();
}
