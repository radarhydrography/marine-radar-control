/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "mainwindow.h"

#include <QApplication>
#include <QFile>
#include "shared-libs/inireader.h"
#include "startdialog.h"
#include "shared-libs/global.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QDir tmpDir(g_tmpPath);
    tmpDir.mkdir(g_tmpPath);

    a.setApplicationName(QString("HYDROS"));
    a.setApplicationVersion(QString("1.0"));

    QString configFilePath = QString("resources/settings/radar.ini");
    QMap<QString, QString> basicSettings;
    basicSettings = IniReader::readBasicConfigurationFile(configFilePath);

    if (basicSettings[QString("redirectionLink")] != QString("")) {
        configFilePath = basicSettings[QString("redirectionLink")];
        basicSettings = IniReader::readBasicConfigurationFile(configFilePath);
    }

    if (basicSettings.isEmpty() || IniReader::checkBasicConfiguration(basicSettings)) {
        StartDialog startDialog;
        configFilePath = startDialog.stratDialog();
        if (configFilePath != QString("")) {
            basicSettings = IniReader::readBasicConfigurationFile(configFilePath);
        }
    }

    if (configFilePath != QString("")) {
        QMap<QString, QString> iniSettings;
        if (basicSettings["config"] == QString("")) {
            iniSettings = IniReader::defaultConfiguration();
        } else {
            iniSettings = IniReader::readConfigurationFile(QString(basicSettings["config"]));
            iniSettings.insert(basicSettings);
            iniSettings.insert(QString("LightSpeed"), QString::number(g_lightSpeed));
        }

        MainWindow mainWindow(iniSettings);
        mainWindow.setWindowIcon(QIcon(iniSettings.value(QString("icons")) + g_pathDevider + QString("hereon.png")));
        mainWindow.setWhatsThis(QString("HYDROS"));
        mainWindow.setWindowTitle("HYDROS - Hydrographic Radar Operational Software v1.0");
        mainWindow.show();
        return a.exec();
    } else {
        tmpDir.removeRecursively();
        return 0;
    }
}
