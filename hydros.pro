QT += core gui
QT += network
QT += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++17

SOURCES += \
    radar-rs-sub-program/radarcommunicationserial.cpp \
    radar-rs-sub-program/radarmessageserial.cpp \
    radar-rs-sub-program/radarserialcontrolui.cpp \
    radar-rs-sub-program/serialportcommunicator.cpp \
    radar-rs-sub-program/serialportsettingsui.cpp \
    radar-udp-sub-program/radarcommunicatorudp.cpp \
    radar-udp-sub-program/radarmessageudp.cpp \
    radar-udp-sub-program/radarudpcontrolui.cpp \
    graphics-module/calibrationplot.cpp \
    graphics-module/dataprocessingthread.cpp \
    graphics-module/diagramprocessingthread.cpp \
    graphics-module/realtimeplot.cpp \
    graphics-module/signalqualityplot.cpp \
    graphics-module/signalqualityprocessingthread.cpp \
    greenboard-udp-sub-program/datasavingthread.cpp \
    greenboard-udp-sub-program/greenboardcommunicator.cpp \
    greenboard-udp-sub-program/greenboardcontrolui.cpp \
    main.cpp \
    mainwindow.cpp \
    schedule-worker/scheduleeditorui.cpp \
    schedule-worker/scheduleworker.cpp \
    schedule-worker/scheduleworkerui.cpp \
    shared-libs/applicationlogger.cpp \
    shared-libs/inireader.cpp \
    shared-libs/ledindicator.cpp \
    shared-libs/qcustomplot.cpp \
    schedule-worker/schedule.cpp \
    schedule-worker/scheduleline.cpp \
    startdialog.cpp

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    radar-rs-sub-program/radarcommunicationserial.h \
    radar-rs-sub-program/radarmessageserial.h \
    radar-rs-sub-program/radarserialcontrolui.h \
    radar-rs-sub-program/radarstructsserial.h \
    radar-rs-sub-program/serialportcommunicator.h \
    radar-rs-sub-program/serialportsettingsui.h \
    radar-rs-sub-program/structsserial.h \
    radar-udp-sub-program/radarcommunicatorudp.h \
    radar-udp-sub-program/radarmessageudp.h \
    radar-udp-sub-program/radarstructsudp.h \
    radar-udp-sub-program/radarudpcontrolui.h \
    graphics-module/calibrationplot.h \
    graphics-module/dataprocessingthread.h \
    graphics-module/diagramprocessingthread.h \
    graphics-module/realtimeplot.h \
    graphics-module/signalqualityplot.h \
    graphics-module/signalqualityprocessingthread.h \
    greenboard-udp-sub-program/GreenBoardStruct.h \
    greenboard-udp-sub-program/datasavingthread.h \
    greenboard-udp-sub-program/greenboardcommunicator.h \
    greenboard-udp-sub-program/greenboardcontrolui.h \
    mainwindow.h \
    schedule-worker/scheduleeditorui.h \
    schedule-worker/scheduleworker.h \
    schedule-worker/scheduleworkerui.h \
    shared-libs/applicationlogger.h \
    shared-libs/global.h \
    shared-libs/inireader.h \
    shared-libs/ledindicator.h \
    shared-libs/qcustomplot.h \
    schedule-worker/schedule.h \
    schedule-worker/scheduleline.h \
    schedule-worker/schedulestructs.h \
    startdialog.h

DISTFILES += \
    resources/icons/achtung.svg \
    resources/icons/exit.svg \
    resources/icons/exit.svg) \
    resources/icons/executable.svg \
    resources/icons/executable.svg) \
    resources/icons/engineering.svg \
    resources/icons/engineering.svg) \
    resources/icons/system.svg \
    resources/icons/system.svg) \
    resources/icons/appointment-new.svg \
    resources/icons/appointment-new.svg) \
    resources/icons/document-edit.svg \
    resources/icons/document-new.svg \
    resources/icons/document-open.svg \
    resources/icons/document-open.svg) \
    resources/icons/document-properties.svg \
    resources/icons/document-properties.svg) \
    resources/icons/document-save-as.svg \
    resources/icons/document-save-as.svg) \
    resources/icons/document-save.svg \
    resources/icons/document-save.svg) \
    resources/icons/edit-clear-all.svg \
    resources/icons/edit-clear-all.svg) \
    resources/icons/edit-clear.svg \
    resources/icons/edit-clear.svg) \
    resources/icons/edit-copy.svg \
    resources/icons/edit-copy.svg) \
    resources/icons/edit-delete.svg \
    resources/icons/edit-delete.svg) \
    resources/icons/edit-redo.svg \
    resources/icons/edit-undo.svg \
    resources/icons/folder-drag-accept.svg \
    resources/icons/folder-drag-accept.svg) \
    resources/icons/folder-new.svg \
    resources/icons/folder-new.svg) \
    resources/icons/folder-open.svg \
    resources/icons/folder-open.svg) \
    resources/icons/folder.svg \
    resources/icons/folder.svg).svg) \
    resources/icons/folder-visiting.svg \
    resources/icons/folder-visiting.svg) \
    resources/icons/hereon.png \
    resources/icons/icon.png \
    resources/icons/pause.svg \
    resources/icons/pause.svg) \
    resources/icons/play.svg \
    resources/icons/connect.svg) \
    resources/icons/preferences-other.svg \
    resources/icons/preferences-other.svg) \
    resources/icons/stop.svg \
    resources/icons/disconnect.svg) \
    resources/icons/system-lock-screen.svg \
    resources/icons/system-lock-screen.svg) \
    resources/icons/tweaks-app.svg \
    resources/icons/tweaks-app.svg) \
    resources/icons/view-refresh.svg \
    resources/icons/view-refresh.svg) \
    resources/settings/radar.ini \
    resources/schedule.sc \
    resources/settings/settings.ini

