/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "mainwindow.h"
#include "shared-libs/global.h"
#include <QVBoxLayout>
#include <QDebug>
#include <QTabWidget>

MainWindow::MainWindow(const QMap<QString, QString> &iniSettings, QWidget *parent) : QMainWindow(parent)
{
    m_applicationLogger = new ApplicationLogger(iniSettings.value(QString("logs")), this);
    connect(this, &MainWindow::SignalLogEvent, m_applicationLogger, &ApplicationLogger::SlotLogEvent);

    m_iniSettings = iniSettings;
    IniReader::checkConfiguration(m_iniSettings);

    SlotLogEvent(QString("Main: Initial Configuration Loaded"));

    m_serialPortControl = static_cast<bool>(m_iniSettings["RadarControlInterface"].toUInt());

    // I will explain it ony once, but use everywere
    // It could be a bit different in different situations, but principe is the same every time

    // Create main layout for widget, for exaple this is horisontal
    // And idea is to content main plot widget at the left side and control settings at the right side
    QHBoxLayout *mainLayout = new QHBoxLayout;
    // Create additional layout for something
    // Ti contain all settings in vertival layout
    QVBoxLayout *rightLayout = new QVBoxLayout;

    QVBoxLayout *leftLayout = new QVBoxLayout;

    // Constructing object for Green Board control
    m_greenBoardContol = new GreenBoardControlUI(m_iniSettings, this);
    SlotLogEvent(QString("Main: Green Board Control Module Loaded"));
    connect(m_greenBoardContol, &GreenBoardControlUI::SignalLogEvent, this, &MainWindow::SlotLogEvent);
    // Make it openable in another window
    m_greenBoardContol->setWindowFlag(Qt::Window);
    // Create external control widget to put it to main widget
    QWidget *greenBoardControl = m_greenBoardContol->m_externalControlWidget;

    // Constructing object for Radar control via RS 422
    // The same widget features
    if (m_serialPortControl) {
        m_radarSerialControl = new RadarSerialControlUI(m_iniSettings, this);
        m_radarSerialControl->setWindowFlag(Qt::Window);
        m_radarSerialControlInterface = m_radarSerialControl->m_externalControlWidget;
        // Put one of control widget to right layout
        rightLayout->addWidget(m_radarSerialControlInterface);
        m_radarSerialControlInterface->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        SlotLogEvent(QString("Main: Radar Serial Control Module Loaded"));
    } else {
        m_radarEthernetControl = new RadarUdpControlUI(m_iniSettings, this);
        m_radarEthernetControl->setWindowFlag(Qt::Window);
        m_radarEthernetControlInterface = m_radarEthernetControl->m_externalControlWidget;
        rightLayout->addWidget(m_radarEthernetControlInterface);
        m_radarEthernetControlInterface->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        connect(m_greenBoardContol, &GreenBoardControlUI::SignalDataSavingState, m_radarEthernetControl, &RadarUdpControlUI::SlotDataSavingState);
        SlotLogEvent(QString("Main: Radar Ethernet Control Module Loaded"));
        connect(m_radarEthernetControl, &RadarUdpControlUI::SignalLogEvent, this, &MainWindow::SlotLogEvent);
    }

    // And another one
    rightLayout->addWidget(greenBoardControl);
    greenBoardControl->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    // Thats for playing with the size policy
    m_mainTabWidget = new QTabWidget;
    QWidget *controlWidget = new QWidget;

    controlWidget->setLayout(rightLayout);
    controlWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    controlWidget->setMaximumWidth(460);

    // Constructing main Graphics Object
    m_mainPlot = new RealTimePlot(iniSettings);
    SlotLogEvent(QString("Main: Graphics Module Loaded"));

    m_scheduleWorkerUI = new ScheduleWorkerUI(iniSettings);
    SlotLogEvent(QString("Main: Schedule Worker Module Loaded"));
    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::SignalLogEvent, this, &MainWindow::SlotLogEvent);

//    rightLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
    QGridLayout *bottomLayout = new QGridLayout;
//    QVBoxLayout *bwLayout = new QVBoxLayout;

    QFrame *lineLeft = new QFrame;
    lineLeft->setFrameShape(QFrame::VLine);
    lineLeft->setFrameShadow(QFrame::Sunken);

    QFrame *lineRight = new QFrame;
    lineRight->setFrameShape(QFrame::VLine);
    lineRight->setFrameShadow(QFrame::Sunken);

    QFrame *lineTop = new QFrame;
    lineTop->setFrameShape(QFrame::HLine);
    lineTop->setFrameShadow(QFrame::Sunken);

    QFrame *lineBottom = new QFrame;
    lineBottom->setFrameShape(QFrame::HLine);
    lineBottom->setFrameShadow(QFrame::Sunken);

    QFrame *lineMidTop = new QFrame;
    lineMidTop->setFrameShape(QFrame::HLine);
    lineMidTop->setFrameShadow(QFrame::Sunken);

    QFrame *lineMidBottom = new QFrame;
    lineMidBottom->setFrameShape(QFrame::HLine);
    lineMidBottom->setFrameShadow(QFrame::Sunken);

    bottomLayout->addWidget(lineLeft, 0, 0, 7, 1);
    bottomLayout->addWidget(lineRight, 0, 2, 7, 1);

    bottomLayout->addWidget(lineTop, 0, 0, 1, 3);
    bottomLayout->addWidget(lineMidTop, 2, 0, 1, 3);
    bottomLayout->addWidget(lineMidBottom, 4, 0, 1, 3);
    bottomLayout->addWidget(lineBottom, 6, 0, 1, 3);

    bottomLayout->addLayout(m_mainPlot->m_externalControlWidget, 1, 1);
    bottomLayout->addLayout(m_scheduleWorkerUI->m_statusWidget, 3, 1);
    bottomLayout->addLayout(m_greenBoardContol->m_informationWidget, 5, 1);
//    bottomWidget->setLayout(bwLayout);
//    bottomWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
//    rightLayout->addWidget(bottomWidget);
    rightLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding));

    // And a contol widget too
    m_mainTabWidget->addTab(controlWidget, QString(tr("Radar Control")));
    m_mainTabWidget->addTab(m_scheduleWorkerUI, QString(tr("Schedule")));
    leftLayout->addWidget(m_mainTabWidget);
    m_mainTabWidget->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred));
    QWidget *bottomWidget = new QWidget;
    int maxH = 480;
    controlWidget->setMaximumWidth(maxH);
    controlWidget->setMinimumWidth(maxH);
    bottomWidget->setMaximumWidth(maxH);
    bottomWidget->setMinimumWidth(maxH);
    bottomWidget->setLayout(bottomLayout);
    leftLayout->addWidget(bottomWidget);
    mainLayout->addLayout(leftLayout);
    m_mainTabWidget->setMaximumWidth(maxH);


    // Put it into the main layout
    mainLayout->addWidget(m_mainPlot);
//    m_mainPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    // Create new widget to contain everything
    QWidget *mainWidget = new QWidget;
    // Put everything inside
    mainWidget->setLayout(mainLayout);

    // Get this widget as main
    this->setCentralWidget(mainWidget);

    QMenuBar *menubar = new QMenuBar(this);
    QMenu *settingsMenu = new QMenu(QString("Settings"));

    m_openIniFile = new QAction(QString("Open Configuration"));
    m_saveIniFile = new QAction(QString("Save Configuration"));

    settingsMenu->addAction(m_openIniFile);
    settingsMenu->addAction(m_saveIniFile);

    settingsMenu->addSeparator();

    if (static_cast<bool>(m_iniSettings["CalibrationMode"].toUInt())) {
        m_calibration = new QAction(QString("Calibration Tool"));
        connect(m_calibration, &QAction::triggered, m_mainPlot, &RealTimePlot::SignalCalibrationClicked);
        settingsMenu->addAction(m_calibration);
    }

    settingsMenu->addSeparator();

    connect(m_openIniFile, &QAction::triggered, this, &MainWindow::slotLoadIniConfiguration);
    connect(m_saveIniFile, &QAction::triggered, this, &MainWindow::slotSaveIniConfiguration);

    m_darkModeSwitch = new QAction(QString("Night Mode"));
    m_darkModeSwitch->setCheckable(true);
    connect(m_darkModeSwitch, &QAction::triggered, this, &MainWindow::darkModeSwitchClicked);
    settingsMenu->addAction(m_darkModeSwitch);

//    m_tuner = new QAction(QString("Tune Fine Tuning Tool"));
//    connect(m_tuner, &QAction::triggered, m_mainPlot, &RealTimePlot::SignalTunerClicked);
//    settingsMenu->addAction(m_tuner);

    menubar->addMenu(settingsMenu);

    setMenuBar(menubar);

    m_statusbar = new QStatusBar(this);
    setStatusBar(m_statusbar);

    connect(m_mainPlot, &RealTimePlot::SignalResize, this, &MainWindow::SlotResizeWindow);

    m_mainPlot->SlotPlotTypeChanged(m_iniSettings["DefaultPlotType"].toUInt());
    // Connect event of changing angle offset to signal in Main Graphics module
    connect(m_greenBoardContol, &GreenBoardControlUI::SignalAngleOffsetChanged, m_mainPlot, &RealTimePlot::SignalAngleOffsetChanged);
    // Connect the event of collecting full beam (4 datagrams) to Main Plot for data processing and plotting
    connect(m_greenBoardContol, &GreenBoardControlUI::SignalFullBeam, m_mainPlot, &RealTimePlot::SignalFullBeam);
    // Connect event of changing average coefficient offset to signal in Main Graphics module
    connect(m_greenBoardContol, &GreenBoardControlUI::SignalSetSignalAvgCoefficient, m_mainPlot, &RealTimePlot::SignalSetSignalAvgCoefficient);
    connect(m_greenBoardContol, &GreenBoardControlUI::SignalAdcChannelALengthChanged, m_mainPlot, &RealTimePlot::SignalAdcChannelALengthChanged);

    m_greenBoardContol->InitADCClicked();
    m_greenBoardContol->SlotRestartNetworkClicked();

    if (m_serialPortControl) {
        connect(m_radarSerialControl, &RadarSerialControlUI::SignalstatusMessage, this, &MainWindow::statusBarMessage);
    } else {
        connect(m_radarEthernetControl, &RadarUdpControlUI::SignalstatusMessage, this, &MainWindow::statusBarMessage);
        connect(m_greenBoardContol, &GreenBoardControlUI::SignalTxOff, m_radarEthernetControl, &RadarUdpControlUI::SlotTxOff);
        connect(m_greenBoardContol, &GreenBoardControlUI::SignalBlindZoneIsActive, m_radarEthernetControl, &RadarUdpControlUI::SlotBlindZoneIsActive);
    }

    connect(m_greenBoardContol, &GreenBoardControlUI::SignalstatusMessage, this, &MainWindow::statusBarMessage);
    connect(m_greenBoardContol, &GreenBoardControlUI::SignalSamplingFrequency, m_mainPlot, &RealTimePlot::SlotSamplingFrequencyChanged);
    connect(m_mainPlot, &RealTimePlot::SignalStatusBarMessage, this, &MainWindow::statusBarMessage); 
    connect(m_radarEthernetControl, &RadarUdpControlUI::SignalSetTuning, m_greenBoardContol, &GreenBoardControlUI::SlotUpdateFileTuningInfo);

    darkMode(m_iniSettings["DarkMode"].toUInt());
    m_darkModeSwitch->setChecked(m_iniSettings["DarkMode"].toUInt());

    connect(m_greenBoardContol, &GreenBoardControlUI::SignalResize, this, &MainWindow::SlotResizeWindow);
    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::signalSetScheduleName, this, &MainWindow::slotSetScheduleName);

    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::signalSetAntennaRotating, this, &MainWindow::slotSetAntennaRotating);
    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::signalSetTransmitionOn, this, &MainWindow::slotSetTransmitionOn);
    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::signalSetDataAcquisitionOn, this, &MainWindow::slotSetDataAcquisitionOn);
    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::signalScheduleState, m_greenBoardContol, &GreenBoardControlUI::SlotScheduleState);
    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::signalScheduleState, this, &MainWindow::slotScheduleWorkerStarted);
    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::signalSetCurrentScheduleSettings, this, &MainWindow::slotSetCurrentScheduleSettings);
    connect(m_radarEthernetControl, &RadarUdpControlUI::SignalRadarIsWarmingUp, m_scheduleWorkerUI, &ScheduleWorkerUI::signalRadarIsWarmingUp);

    if (m_iniSettings["schedule"] != QString("") || QFile::exists(m_iniSettings["schedule"])) {
        bool autostart = m_iniSettings["autostart"].toUInt() == 1 ? true : false;
        m_scheduleWorkerUI->setDefaultSchedule(m_iniSettings["schedule"], autostart);
    }
    connect(m_greenBoardContol, &GreenBoardControlUI::SignalCurrentDataFolder, m_scheduleWorkerUI, &ScheduleWorkerUI::SlotUpdateDirectoryFolder);
    connect(m_scheduleWorkerUI, &ScheduleWorkerUI::SignalScheduleTimeCoefficientChanged, m_greenBoardContol, &GreenBoardControlUI::SlotScheduleTimeCoefficientChanged);

    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    m_savingIniSettingsDialog = SavingIniSettingsWidget();

    m_openDefaultsRememberChoiseChechBox = new QCheckBox(QString("Remember my choise"));
    m_openIniSettingsMessageBox = new QMessageBox;
    m_openIniSettingsMessageBox->setWindowTitle(QString("Ini Settings Default"));
    m_openIniSettingsMessageBox->setText(QString("Do you want to set settings as default?"));
    m_openIniSettingsMessageBox->setCheckBox(m_openDefaultsRememberChoiseChechBox);
    m_openIniSettingsMessageBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    m_calibrationMode = static_cast<bool>(m_iniSettings["CalibrationMode"].toUInt());

    SlotLogEvent(QString("Main: Loading Completed"));
}

MainWindow::~MainWindow()
{
    m_greenBoardContol->exit();
    if (m_serialPortControl) {
        m_radarSerialControl->exit();
    } else {
        m_radarEthernetControl->exit();
    }
    QDir tmpDir(g_tmpPath);
    tmpDir.removeRecursively();
    SlotLogEvent(QString("Main: Exiting Application"));
    QThread::msleep(50);
    m_applicationLogger->~ApplicationLogger();
}

void MainWindow::setIniSettings(QMap<QString, QString> &iniSettings)
{
    IniReader::checkConfiguration(iniSettings);
    m_iniSettings = iniSettings;

    m_greenBoardContol->setIniSettings(iniSettings);
    if (m_serialPortControl) {

    } else {
        m_radarEthernetControl->setIniSettings(iniSettings);
    }

    m_mainPlot->setIniSettings(iniSettings, GraphicModuleSetSettings::SetupMode);
}

QMap<QString, QString> MainWindow::getIniSettings()
{
    QMap<QString, QString> iniSettings;

    if (m_greenBoardSettingsCheckBox->isChecked()) {
        iniSettings.insert(m_greenBoardContol->getIniSettings());
    }

    if (m_radarSettingsCheckBox->isChecked()) {
        if (m_serialPortControl) {

        } else {
            iniSettings.insert(m_radarEthernetControl->getIniSettings());
        }
    }

    if (m_signalPlotCheckBox->isChecked()) {
        iniSettings.insert(m_mainPlot->getIniSettings(GraphicsModuleGetSettings::GetSignalPlotSettings));
    }

    if (m_circuleDiagramCheckBox->isChecked()) {
        iniSettings.insert(m_mainPlot->getIniSettings(GraphicsModuleGetSettings::GetDiagramSettings));
    }

    if (m_tuningPlotCheckBox->isChecked()) {
        iniSettings.insert(m_mainPlot->getIniSettings(GraphicsModuleGetSettings::GetTuningPlotSettings));
    }

    if (m_calibrationPlotCheckBox->isChecked() && m_calibrationMode) {
        iniSettings.insert(m_mainPlot->getIniSettings(GraphicsModuleGetSettings::GetCalibrationPlotSettings));
    }

    return iniSettings;
}

QDialog *MainWindow::SavingIniSettingsWidget()
{
    QDialog *savingIniSettingsDialog = new QDialog;
    m_greenBoardSettingsCheckBox = new QCheckBox(QString("Green Board Settings"));
    m_radarSettingsCheckBox = new QCheckBox(QString("RADAR Radar Settings"));
    m_signalPlotCheckBox = new QCheckBox(QString("Signal Plot Settings"));
    m_circuleDiagramCheckBox = new QCheckBox(QString("Circle Diagram Settings"));
    m_tuningPlotCheckBox = new QCheckBox(QString("Tuning Plot Settings"));
    m_calibrationPlotCheckBox = new QCheckBox(QString("Calibration Plot Settings"));
    m_greenBoardSettingsCheckBox->setChecked(false);
    m_radarSettingsCheckBox->setChecked(false);
    m_signalPlotCheckBox->setChecked(false);
    m_circuleDiagramCheckBox->setChecked(false);
    m_tuningPlotCheckBox->setChecked(false);
    m_calibrationPlotCheckBox->setChecked(false);

    m_defaultPlotTypeComboBox = new QComboBox;
    QStringList defaultPlotTypeStringList;
    defaultPlotTypeStringList.append(QString("No plots"));
    defaultPlotTypeStringList.append(QString("Signal"));
    defaultPlotTypeStringList.append(QString("All plots"));
    defaultPlotTypeStringList.append(QString("Circular"));
    m_defaultPlotTypeComboBox->addItems(defaultPlotTypeStringList);
    m_darkModeComboBox = new QComboBox;
    QStringList darkModeStringList;
    darkModeStringList.append(QString("Normal mode"));
    darkModeStringList.append(QString("Dark theme"));
    m_darkModeComboBox->addItems(darkModeStringList);
    m_calibrationModeComboBox = new QComboBox;
    QStringList calibrationModeStringList;
    calibrationModeStringList.append(QString("Disabled"));
    calibrationModeStringList.append(QString("Enabled"));
    m_calibrationModeComboBox->addItems(calibrationModeStringList);
    m_radarControlInterfaceComboBox = new QComboBox;
    QStringList radarControlInterfaceStringList;
    radarControlInterfaceStringList.append(QString("Ethernet"));
    radarControlInterfaceStringList.append(QString("Serial"));
    m_radarControlInterfaceComboBox->addItems(radarControlInterfaceStringList);
    m_freeDiskSpaceWarningCheckBox = new QCheckBox(QString("Free Disk Space Warning"));
    m_freeDiskSpaceWarningSpinBox = new QSpinBox;
    m_freeDiskSpaceWarningSpinBox->setMinimum(0);
    m_freeDiskSpaceWarningSpinBox->setMaximum(100);
    m_freeDiskSpaceWarningSpinBox->setSuffix(QString("%"));
    m_freeDiskSpaceWarningSpinBox->setAlignment(Qt::AlignRight);
    connect(m_freeDiskSpaceWarningCheckBox, &QCheckBox::stateChanged, m_freeDiskSpaceWarningSpinBox, &QWidget::setEnabled);
    m_freeDiskSpaceStopSavingCheckBox = new QCheckBox(QString("Free Disk Space Stop Saving"));
    m_freeDiskSpaceStopSavingSpinBox = new QSpinBox;
    m_freeDiskSpaceStopSavingSpinBox->setMinimum(0);
    m_freeDiskSpaceStopSavingSpinBox->setMaximum(100);
    m_freeDiskSpaceStopSavingSpinBox->setSuffix(QString("%"));
    m_freeDiskSpaceStopSavingSpinBox->setAlignment(Qt::AlignRight);
    connect(m_freeDiskSpaceStopSavingCheckBox, &QCheckBox::stateChanged, m_freeDiskSpaceStopSavingSpinBox, &QWidget::setEnabled);
    m_continueSavingAfterPathChangedCheckBox = new QCheckBox(QString("Continue Saving After Hard Drive Has Been Changed Automatically"));
    m_continueSavingAfterPathChangedComboBox = new QComboBox;
    QStringList continueSavingAfterPathChangedStringList;
    continueSavingAfterPathChangedStringList.append(QString("Disabled"));
    continueSavingAfterPathChangedStringList.append(QString("Enabled"));
    m_continueSavingAfterPathChangedComboBox->addItems(continueSavingAfterPathChangedStringList);
    connect(m_continueSavingAfterPathChangedCheckBox, &QCheckBox::stateChanged, m_continueSavingAfterPathChangedComboBox, &QWidget::setEnabled);
    m_motorControlAutoStartCheckBox = new QCheckBox(QString("Motor Control AutoStart"));
    m_motorControlAutoStartComboBox = new QComboBox;
    QStringList motorControlAutoStartStringList;
    motorControlAutoStartStringList.append(QString("Disabled"));
    motorControlAutoStartStringList.append(QString("Enabled"));
    m_motorControlAutoStartComboBox->addItems(motorControlAutoStartStringList);
    connect(m_motorControlAutoStartCheckBox, &QCheckBox::stateChanged, m_motorControlAutoStartComboBox, &QWidget::setEnabled);

    m_defaultPlotTypeComboBox->setEnabled(false);
    m_darkModeComboBox->setEnabled(false);
    m_calibrationModeComboBox->setEnabled(false);
    m_radarControlInterfaceComboBox->setEnabled(false);
    m_freeDiskSpaceWarningSpinBox->setEnabled(false);
    m_freeDiskSpaceStopSavingSpinBox->setEnabled(false);
    m_continueSavingAfterPathChangedComboBox->setEnabled(false);
    m_motorControlAutoStartComboBox->setEnabled(false);

    m_defaultPlotTypeCheckBox = new QCheckBox(QString("Default Plot Type"));
    m_darkModeCheckBox = new QCheckBox(QString("Default Color Mode"));
    m_calibrationModeCheckBox = new QCheckBox(QString("Calibration Mode"));
    m_radarControlInterfaceCheckBox = new QCheckBox(QString("Radar Control Interface"));
    connect(m_defaultPlotTypeCheckBox, &QCheckBox::stateChanged, m_defaultPlotTypeComboBox, &QComboBox::setEnabled);
    connect(m_darkModeCheckBox, &QCheckBox::stateChanged, m_darkModeComboBox, &QComboBox::setEnabled);
    connect(m_calibrationModeCheckBox, &QCheckBox::stateChanged, m_calibrationModeComboBox, &QComboBox::setEnabled);
    connect(m_radarControlInterfaceCheckBox, &QCheckBox::stateChanged, m_radarControlInterfaceComboBox, &QComboBox::setEnabled);
    m_defaultPlotTypeCheckBox->setChecked(false);
    m_darkModeCheckBox->setChecked(false);
    m_calibrationModeCheckBox->setChecked(false);
    m_radarControlInterfaceCheckBox->setChecked(false);

    QHBoxLayout *defaultPlotTypeLayout = new QHBoxLayout;
    defaultPlotTypeLayout->addWidget(m_defaultPlotTypeCheckBox);
    defaultPlotTypeLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    defaultPlotTypeLayout->addWidget(m_defaultPlotTypeComboBox);
    QHBoxLayout *darkModeLayout = new QHBoxLayout;
    darkModeLayout->addWidget(m_darkModeCheckBox);
    darkModeLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    darkModeLayout->addWidget(m_darkModeComboBox);
    QHBoxLayout *calibrationModeLayout = new QHBoxLayout;
    calibrationModeLayout->addWidget(m_calibrationModeCheckBox);
    calibrationModeLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    calibrationModeLayout->addWidget(m_calibrationModeComboBox);
    QHBoxLayout *radarControlInterfaceLayout = new QHBoxLayout;
    radarControlInterfaceLayout->addWidget(m_radarControlInterfaceCheckBox);
    radarControlInterfaceLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    radarControlInterfaceLayout->addWidget(m_radarControlInterfaceComboBox);
    QHBoxLayout *freeDiskSpaceWarningLayout = new QHBoxLayout;
    freeDiskSpaceWarningLayout->addWidget(m_freeDiskSpaceWarningCheckBox);
    freeDiskSpaceWarningLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    freeDiskSpaceWarningLayout->addWidget(m_freeDiskSpaceWarningSpinBox);
    QHBoxLayout *freeDiskSpaceStopSavingLayout = new QHBoxLayout;
    freeDiskSpaceStopSavingLayout->addWidget(m_freeDiskSpaceStopSavingCheckBox);
    freeDiskSpaceStopSavingLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    freeDiskSpaceStopSavingLayout->addWidget(m_freeDiskSpaceStopSavingSpinBox);
    QHBoxLayout *continueSavingAfterPathChangedLayout = new QHBoxLayout;
    continueSavingAfterPathChangedLayout->addWidget(m_continueSavingAfterPathChangedCheckBox);
    continueSavingAfterPathChangedLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    continueSavingAfterPathChangedLayout->addWidget(m_continueSavingAfterPathChangedComboBox);
    QHBoxLayout *motorControlAutoStartLayout = new QHBoxLayout;
    motorControlAutoStartLayout->addWidget(m_motorControlAutoStartCheckBox);
    motorControlAutoStartLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    motorControlAutoStartLayout->addWidget(m_motorControlAutoStartComboBox);

    m_extendedValuesPushButton = new QPushButton(QString("Advanced parameters"));
    m_extendedValuesPushButton->setCheckable(true);
    connect(m_extendedValuesPushButton, &QPushButton::clicked, this, &MainWindow::slotExtendedValuesPushButtonClicked);
    QHBoxLayout *extendedValuesLayout = new QHBoxLayout;
    extendedValuesLayout->addWidget(m_extendedValuesPushButton);
    extendedValuesLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

    m_samplingFrequencyComboBox = new QSpinBox;
    m_samplingFrequencyComboBox->setMinimum(1);
    m_samplingFrequencyComboBox->setMaximum(2147483647);
    m_signalLengthComboBox = new QSpinBox;
    m_signalLengthComboBox->setMinimum(1);
    m_signalLengthComboBox->setMaximum(2147483647);
    m_adcBitrRateComboBox = new QSpinBox;
    m_adcBitrRateComboBox->setMinimum(1);
    m_adcBitrRateComboBox->setMaximum(2147483647);
    m_adcMaxValueComboBox = new QSpinBox;
    m_adcMaxValueComboBox->setMinimum(1);
    m_adcMaxValueComboBox->setMaximum(2147483647);

    m_samplingFrequencyCheckBox = new QCheckBox(QString("Sampling Frequency"));
    m_signalLengthCheckBox = new QCheckBox(QString("Signal Length"));
    m_adcBitrRateCheckBox = new QCheckBox(QString("ADC BitRate"));
    m_adcMaxValueCheckBox = new QCheckBox(QString("ADC Max Value"));
    connect(m_samplingFrequencyCheckBox, &QCheckBox::stateChanged, m_samplingFrequencyComboBox, &QSpinBox::setEnabled);
    connect(m_signalLengthCheckBox, &QCheckBox::stateChanged, m_signalLengthComboBox, &QSpinBox::setEnabled);
    connect(m_adcBitrRateCheckBox, &QCheckBox::stateChanged, m_adcBitrRateComboBox, &QSpinBox::setEnabled);
    connect(m_adcMaxValueCheckBox, &QCheckBox::stateChanged, m_adcMaxValueComboBox, &QSpinBox::setEnabled);
    m_samplingFrequencyCheckBox->setChecked(false);
    m_signalLengthCheckBox->setChecked(false);
    m_adcBitrRateCheckBox->setChecked(false);
    m_adcMaxValueCheckBox->setChecked(false);

    m_greenBoardNetworkSettings = new QCheckBox(QString("Green Board Network Settings"));
    m_greenBoardNetworkSettings->setChecked(false);
    m_greenBoardNetworkSettings->setEnabled(false);

    QString IpRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    QRegularExpression IpRegex ("^" + IpRange
                                    + "(\\." + IpRange + ")"
                                    + "(\\." + IpRange + ")"
                                    + "(\\." + IpRange + ")$");
    QRegularExpressionValidator *ipValidator = new QRegularExpressionValidator(IpRegex, this);

    m_pcHostAddress = new QLineEdit;
    m_pcHostAddress->setValidator(ipValidator);
    m_pcHostAddress->setEnabled(false);
    QLabel *pcHostAddressLabel = new QLabel(QString(tr("PC Address")));
    pcHostAddressLabel->setEnabled(false);
    m_pcControlPort = new QSpinBox;
    m_pcControlPort->setMinimum(0);
    m_pcControlPort->setMaximum(65535);
    m_pcControlPort->setButtonSymbols(QSpinBox::NoButtons);
    m_pcControlPort->setValue(0);
    m_pcControlPort->setEnabled(false);
    QLabel *pcControlPortLabel = new QLabel(QString(tr("Control Port")));
    pcControlPortLabel->setEnabled(false);
    m_pcDataPort = new QSpinBox;
    m_pcDataPort->setMinimum(0);
    m_pcDataPort->setMaximum(65535);
    m_pcDataPort->setButtonSymbols(QSpinBox::NoButtons);
    m_pcDataPort->setValue(0);
    m_pcDataPort->setEnabled(false);
    QLabel *pcDataPortLabel = new QLabel(QString(tr("Data Port")));
    pcDataPortLabel->setEnabled(false);
    m_boardHostAddress = new QLineEdit;
    m_boardHostAddress->setValidator(ipValidator);
    m_boardHostAddress->setEnabled(false);
    QLabel *boardHostAddressLabel = new QLabel(QString(tr("Board Address")));
    boardHostAddressLabel->setEnabled(false);
    m_pcHostAddress->setMaximumWidth(120);
    m_boardHostAddress->setMaximumWidth(120);
    m_boardControlPort = new QSpinBox;
    m_boardControlPort->setMinimum(0);
    m_boardControlPort->setMaximum(65535);
    m_boardControlPort->setButtonSymbols(QSpinBox::NoButtons);
    m_boardControlPort->setValue(0);
    m_boardControlPort->setEnabled(false);
    QLabel *boardControlPortLabel = new QLabel(QString(tr("Control Port")));
    boardControlPortLabel->setEnabled(false);
    m_boardDataPort = new QSpinBox;
    m_boardDataPort->setMinimum(0);
    m_boardDataPort->setMaximum(65535);
    m_boardDataPort->setButtonSymbols(QSpinBox::NoButtons);
    m_boardDataPort->setValue(0);
    m_boardDataPort->setEnabled(false);
    QLabel *boardDataPortLabel = new QLabel(QString(tr("Data Port")));
    boardDataPortLabel->setEnabled(false);

    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, m_pcHostAddress, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, pcHostAddressLabel, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, m_pcControlPort, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, pcControlPortLabel, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, m_pcDataPort, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, pcDataPortLabel, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, m_boardHostAddress, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, boardHostAddressLabel, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, m_boardControlPort, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, boardControlPortLabel, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, m_boardDataPort, &QWidget::setEnabled);
    connect(m_greenBoardNetworkSettings, &QCheckBox::stateChanged, boardDataPortLabel, &QWidget::setEnabled);

    QGridLayout *greenBoardNetSettingsLayout = new QGridLayout;

    greenBoardNetSettingsLayout->addWidget(m_greenBoardNetworkSettings, 0, 0, 1, 2);
    greenBoardNetSettingsLayout->addWidget(pcHostAddressLabel, 1, 0);
    greenBoardNetSettingsLayout->addWidget(pcControlPortLabel, 1, 1);
    greenBoardNetSettingsLayout->addWidget(pcDataPortLabel, 1, 2);
    greenBoardNetSettingsLayout->addWidget(m_pcHostAddress, 2, 0);
    greenBoardNetSettingsLayout->addWidget(m_pcControlPort, 2, 1);
    greenBoardNetSettingsLayout->addWidget(m_pcDataPort, 2, 2);
    greenBoardNetSettingsLayout->addWidget(boardHostAddressLabel, 3, 0);
    greenBoardNetSettingsLayout->addWidget(boardControlPortLabel, 3, 1);
    greenBoardNetSettingsLayout->addWidget(boardDataPortLabel, 3, 2);
    greenBoardNetSettingsLayout->addWidget(m_boardHostAddress, 4, 0);
    greenBoardNetSettingsLayout->addWidget(m_boardControlPort, 4, 1);
    greenBoardNetSettingsLayout->addWidget(m_boardDataPort, 4, 2);

    m_radarBoardNetworkSettings = new QCheckBox(QString("Radar Network Settings"));
    m_radarBoardNetworkSettings->setChecked(false);
    m_radarBoardNetworkSettings->setEnabled(false);

    m_radarPcHostAddress = new QLineEdit;
    m_radarPcHostAddress->setValidator(ipValidator);
    m_radarPcHostAddress->setEnabled(false);
    m_radarPcHostAddressLabel = new QLabel(QString(tr("PC Address")));
    m_radarPcHostAddressLabel->setEnabled(false);
    m_radarPcControlPort = new QSpinBox;
    m_radarPcControlPort->setMinimum(0);
    m_radarPcControlPort->setMaximum(65535);
    m_radarPcControlPort->setButtonSymbols(QSpinBox::NoButtons);
    m_radarPcControlPort->setValue(0);
    m_radarPcControlPort->setEnabled(false);
    m_radarPcControlPortLabel = new QLabel(QString(tr("Control Port")));
    m_radarPcControlPortLabel->setEnabled(false);
    m_radarPcDataPort = new QSpinBox;
    m_radarPcDataPort->setMinimum(0);
    m_radarPcDataPort->setMaximum(65535);
    m_radarPcDataPort->setButtonSymbols(QSpinBox::NoButtons);
    m_radarPcDataPort->setValue(0);
    m_radarPcDataPort->setEnabled(false);
    m_radarPcDataPortLabel = new QLabel(QString(tr("Data Port")));
    m_radarPcDataPortLabel->setEnabled(false);
    m_radarBoardHostAddress = new QLineEdit;
    m_radarBoardHostAddress->setValidator(ipValidator);
    m_radarBoardHostAddress->setEnabled(false);
    m_radarBoardHostAddressLabel = new QLabel(QString(tr("Radar Address")));
    m_radarBoardHostAddressLabel->setEnabled(false);
    m_radarPcHostAddress->setMaximumWidth(120);
    m_radarBoardHostAddress->setMaximumWidth(120);
    m_radarBoardControlPort = new QSpinBox;
    m_radarBoardControlPort->setMinimum(0);
    m_radarBoardControlPort->setMaximum(65535);
    m_radarBoardControlPort->setButtonSymbols(QSpinBox::NoButtons);
    m_radarBoardControlPort->setValue(0);
    m_radarBoardControlPort->setEnabled(false);
    m_radarBoardControlPortLabel = new QLabel(QString(tr("Control Port")));
    m_radarBoardControlPortLabel->setEnabled(false);
    m_radarBoardDataPort = new QSpinBox;
    m_radarBoardDataPort->setMinimum(0);
    m_radarBoardDataPort->setMaximum(65535);
    m_radarBoardDataPort->setButtonSymbols(QSpinBox::NoButtons);
    m_radarBoardDataPort->setValue(0);
    m_radarBoardDataPort->setEnabled(false);
    m_radarBoardDataPortLabel = new QLabel(QString(tr("Data Port")));
    m_radarBoardDataPortLabel->setEnabled(false);

    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarPcHostAddress, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarPcHostAddressLabel, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarPcControlPort, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarPcControlPortLabel, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarPcDataPort, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarPcDataPortLabel, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarBoardHostAddress, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarBoardHostAddressLabel, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarBoardControlPort, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarBoardControlPortLabel, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarBoardDataPort, &QWidget::setEnabled);
    connect(m_radarBoardNetworkSettings, &QCheckBox::stateChanged, m_radarBoardDataPortLabel, &QWidget::setEnabled);

    QGridLayout *radarBoardNetSettingsLayout = new QGridLayout;

    radarBoardNetSettingsLayout->addWidget(m_radarBoardNetworkSettings, 0, 0, 1, 2);
    radarBoardNetSettingsLayout->addWidget(m_radarPcHostAddressLabel, 1, 0);
    radarBoardNetSettingsLayout->addWidget(m_radarPcControlPortLabel, 1, 1);
    radarBoardNetSettingsLayout->addWidget(m_radarPcDataPortLabel, 1, 2);
    radarBoardNetSettingsLayout->addWidget(m_radarPcHostAddress, 2, 0);
    radarBoardNetSettingsLayout->addWidget(m_radarPcControlPort, 2, 1);
    radarBoardNetSettingsLayout->addWidget(m_radarPcDataPort, 2, 2);
    radarBoardNetSettingsLayout->addWidget(m_radarBoardHostAddressLabel, 3, 0);
    radarBoardNetSettingsLayout->addWidget(m_radarBoardControlPortLabel, 3, 1);
    radarBoardNetSettingsLayout->addWidget(m_radarBoardDataPortLabel, 3, 2);
    radarBoardNetSettingsLayout->addWidget(m_radarBoardHostAddress, 4, 0);
    radarBoardNetSettingsLayout->addWidget(m_radarBoardControlPort, 4, 1);
    radarBoardNetSettingsLayout->addWidget(m_radarBoardDataPort, 4, 2);

    m_radarBoardSerialSettings = new QCheckBox(QString("Radar Serial Port Settings"));
    m_radarBoardSerialSettings->setEnabled(false);
    m_radarBoardSerialSettings->setChecked(false);

    m_portNameLabel = new QLabel(QString(tr("Port Name")));
    m_portNameLabel->setEnabled(false);
    m_portNameLineEdit = new QLineEdit;
    m_baudRateLabel = new QLabel(QString(tr("Baud Rate")));
    m_baudRateLabel->setEnabled(false);
    m_baudRateComboBox = new QComboBox;
    m_dataBitsLabel = new QLabel(QString(tr("Data Bits")));
    m_dataBitsLabel->setEnabled(false);
    m_dataBitsComboBox = new QComboBox;
    m_directionLabel = new QLabel(QString(tr("Direction")));
    m_directionLabel->setEnabled(false);
    m_directionComboBox = new QComboBox;
    m_flowControlLabel = new QLabel(QString(tr("Flow Control")));
    m_flowControlLabel->setEnabled(false);
    m_flowControlComboBox = new QComboBox;
    m_parityLabel = new QLabel(QString(tr("Parity")));
    m_parityLabel->setEnabled(false);
    m_parityComboBox = new QComboBox;
    m_stopBitsLabel = new QLabel(QString(tr("Stop Bits")));
    m_stopBitsLabel->setEnabled(false);
    m_stopBitsComboBox = new QComboBox;

    QStringList baudRate{"Default", "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200"};
    QStringList dataBits{"Default", "5", "6", "7", "8"};
    QStringList direction{"Default", "Input", "Output", "All"};
    QStringList flowControl{"Default", "No", "Hardware", "Software"};
    QStringList parity{"Default", "No", "Even", "Odd", "Space", "Mark"};
    QStringList stopBits{"Default", "1", "1.5", "2"};
    m_portNameLineEdit->setEnabled(false);
    m_baudRateComboBox->addItems(baudRate);
    m_baudRateComboBox->setCurrentIndex(0);
    m_baudRateComboBox->setEnabled(false);
    m_dataBitsComboBox->addItems(dataBits);
    m_dataBitsComboBox->setCurrentIndex(0);
    m_dataBitsComboBox->setEnabled(false);
    m_directionComboBox->addItems(direction);
    m_directionComboBox->setCurrentIndex(0);
    m_directionComboBox->setEnabled(false);
    m_flowControlComboBox->addItems(flowControl);
    m_flowControlComboBox->setCurrentIndex(0);
    m_flowControlComboBox->setEnabled(false);
    m_parityComboBox->addItems(parity);
    m_parityComboBox->setCurrentIndex(0);
    m_parityComboBox->setEnabled(false);
    m_stopBitsComboBox->addItems(stopBits);
    m_stopBitsComboBox->setCurrentIndex(0);
    m_stopBitsComboBox->setEnabled(false);

    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_portNameLineEdit, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_baudRateComboBox, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_dataBitsComboBox, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_directionComboBox, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_flowControlComboBox, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_parityComboBox, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_stopBitsComboBox, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_portNameLabel, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_baudRateLabel, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_dataBitsLabel, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_directionLabel, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_flowControlLabel, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_parityLabel, &QWidget::setEnabled);
    connect(m_radarBoardSerialSettings, &QCheckBox::stateChanged, m_stopBitsLabel, &QWidget::setEnabled);

    radarBoardNetSettingsLayout->addWidget(m_radarBoardSerialSettings, 5, 0, 1, 4);
    radarBoardNetSettingsLayout->addWidget(m_portNameLabel, 6, 0, 1, 2);
    radarBoardNetSettingsLayout->addWidget(m_baudRateLabel, 6, 2);
    radarBoardNetSettingsLayout->addWidget(m_dataBitsLabel, 6, 3);
    radarBoardNetSettingsLayout->addWidget(m_portNameLineEdit, 7, 0, 1, 2);
    radarBoardNetSettingsLayout->addWidget(m_baudRateComboBox, 7, 2);
    radarBoardNetSettingsLayout->addWidget(m_dataBitsComboBox, 7, 3);
    radarBoardNetSettingsLayout->addWidget(m_directionLabel, 8, 0);
    radarBoardNetSettingsLayout->addWidget(m_flowControlLabel, 8, 1);
    radarBoardNetSettingsLayout->addWidget(m_parityLabel, 8, 2);
    radarBoardNetSettingsLayout->addWidget(m_stopBitsLabel, 8, 3);
    radarBoardNetSettingsLayout->addWidget(m_directionComboBox, 9, 0);
    radarBoardNetSettingsLayout->addWidget(m_flowControlComboBox, 9, 1);
    radarBoardNetSettingsLayout->addWidget(m_parityComboBox, 9, 2);
    radarBoardNetSettingsLayout->addWidget(m_stopBitsComboBox, 9, 3);

    connect(m_radarControlInterfaceComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::slotRadarControlInterfaceChanged);

    QHBoxLayout *samplingFrequencyLayout = new QHBoxLayout;
    samplingFrequencyLayout->addWidget(m_samplingFrequencyCheckBox);
    samplingFrequencyLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    samplingFrequencyLayout->addWidget(m_samplingFrequencyComboBox);
    QHBoxLayout *signalLengthLayout = new QHBoxLayout;
    signalLengthLayout->addWidget(m_signalLengthCheckBox);
    signalLengthLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    signalLengthLayout->addWidget(m_signalLengthComboBox);
    QHBoxLayout *adcBitrRateLayout = new QHBoxLayout;
    adcBitrRateLayout->addWidget(m_adcBitrRateCheckBox);
    adcBitrRateLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    adcBitrRateLayout->addWidget(m_adcBitrRateComboBox);
    QHBoxLayout *adcMaxValueLayout = new QHBoxLayout;
    adcMaxValueLayout->addWidget(m_adcMaxValueCheckBox);
    adcMaxValueLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    adcMaxValueLayout->addWidget(m_adcMaxValueComboBox);

    m_samplingFrequencyComboBox->setEnabled(false);
    m_signalLengthComboBox->setEnabled(false);
    m_adcBitrRateComboBox->setEnabled(false);
    m_adcMaxValueComboBox->setEnabled(false);
    m_samplingFrequencyCheckBox->setEnabled(false);
    m_signalLengthCheckBox->setEnabled(false);
    m_adcBitrRateCheckBox->setEnabled(false);
    m_adcMaxValueCheckBox->setEnabled(false);

    m_savingIniSettingsAccept = new QPushButton(QString("Save"));
    m_savingIniSettingsReject = new QPushButton(QString("Cancel"));
    m_setConfigAsDefaultCheckBox = new QCheckBox(QString("Use as Default"));

    QHBoxLayout *buttsLayout = new QHBoxLayout;
    buttsLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    buttsLayout->addWidget(m_setConfigAsDefaultCheckBox);
    buttsLayout->addWidget(m_savingIniSettingsAccept);
    buttsLayout->addWidget(m_savingIniSettingsReject);

    connect(m_savingIniSettingsAccept, &QPushButton::clicked, savingIniSettingsDialog, &QDialog::accept);
    connect(m_savingIniSettingsReject, &QPushButton::clicked, savingIniSettingsDialog, &QDialog::reject);

    m_diagramSettingsLabel = new QLabel(QString("Diagram Plot Settings"));
    m_diagramAngleBitOffsetCheckBox = new QCheckBox(QString("Angle Bit Offset"));
    m_diagramAngleBitOffsetSpinBox = new QSpinBox;
    m_diagramAngleBitOffsetSpinBox->setMinimum(0);
    m_diagramAngleBitOffsetSpinBox->setMaximum(15);
    m_diagramRangeAvgCoefficientCheckBox = new QCheckBox(QString("Range Avg Coefficient (2^i)"));
    m_diagramRangeAvgCoefficientSpinBox = new QSpinBox;
    m_diagramRangeAvgCoefficientSpinBox->setMinimum(1);
    m_diagramRangeAvgCoefficientSpinBox->setMaximum(64);
    m_diagramMaxPlottingFrequencyCheckBox = new QCheckBox(QString("Max Refreshing Frequency"));
    m_diagramMaxPlottingFrequencySpinBox = new QSpinBox;
    m_diagramMaxPlottingFrequencySpinBox->setMinimum(1);
    m_diagramMaxPlottingFrequencySpinBox->setMaximum(1000);

    m_diagramAngleBitOffsetSpinBox->setEnabled(false);
    m_diagramRangeAvgCoefficientSpinBox->setEnabled(false);
    m_diagramMaxPlottingFrequencySpinBox->setEnabled(false);

    connect(m_diagramAngleBitOffsetCheckBox, &QCheckBox::stateChanged, m_diagramAngleBitOffsetSpinBox, &QWidget::setEnabled);
    connect(m_diagramRangeAvgCoefficientCheckBox, &QCheckBox::stateChanged, m_diagramRangeAvgCoefficientSpinBox, &QWidget::setEnabled);
    connect(m_diagramMaxPlottingFrequencyCheckBox, &QCheckBox::stateChanged, m_diagramMaxPlottingFrequencySpinBox, &QWidget::setEnabled);

    QHBoxLayout *diagramAngleBitOffsetLayout = new QHBoxLayout;
    diagramAngleBitOffsetLayout->addWidget(m_diagramAngleBitOffsetCheckBox);
    diagramAngleBitOffsetLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    diagramAngleBitOffsetLayout->addWidget(m_diagramAngleBitOffsetSpinBox);
    QHBoxLayout *diagramRangeAvgCoefficientLayout = new QHBoxLayout;
    diagramRangeAvgCoefficientLayout->addWidget(m_diagramRangeAvgCoefficientCheckBox);
    diagramRangeAvgCoefficientLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    diagramRangeAvgCoefficientLayout->addWidget(m_diagramRangeAvgCoefficientSpinBox);
    QHBoxLayout *diagramMaxPlottingFrequencLayout = new QHBoxLayout;
    diagramMaxPlottingFrequencLayout->addWidget(m_diagramMaxPlottingFrequencyCheckBox);
    diagramMaxPlottingFrequencLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    diagramMaxPlottingFrequencLayout->addWidget(m_diagramMaxPlottingFrequencySpinBox);

    m_calibrationSettingsLabel = new QLabel(QString("Calibration Plot Settings"));
    m_calibrationAngleBitOffsetCheckBox = new QCheckBox(QString("Angle Bit Offset"));
    m_calibrationAngleBitOffsetSpinBox = new QSpinBox;
    m_calibrationAngleBitOffsetSpinBox->setMinimum(0);
    m_calibrationAngleBitOffsetSpinBox->setMaximum(15);
    m_calibrationRangeAvgCoefficientCheckBox = new QCheckBox(QString("Range Avg Coefficient (2^i)"));
    m_calibrationRangeAvgCoefficientSpinBox = new QSpinBox;
    m_calibrationRangeAvgCoefficientSpinBox->setMinimum(1);
    m_calibrationRangeAvgCoefficientSpinBox->setMaximum(64);
    m_calibrationMaxPlottingFrequencyCheckBox = new QCheckBox(QString("Max Refreshing Frequency"));
    m_calibrationMaxPlottingFrequencySpinBox = new QSpinBox;
    m_calibrationMaxPlottingFrequencySpinBox->setMinimum(1);
    m_calibrationMaxPlottingFrequencySpinBox->setMaximum(1000);

    m_calibrationAngleBitOffsetSpinBox->setEnabled(false);
    m_calibrationRangeAvgCoefficientSpinBox->setEnabled(false);
    m_calibrationMaxPlottingFrequencySpinBox->setEnabled(false);

    connect(m_calibrationAngleBitOffsetCheckBox, &QCheckBox::stateChanged, m_calibrationAngleBitOffsetSpinBox, &QWidget::setEnabled);
    connect(m_calibrationRangeAvgCoefficientCheckBox, &QCheckBox::stateChanged, m_calibrationRangeAvgCoefficientSpinBox, &QWidget::setEnabled);
    connect(m_calibrationMaxPlottingFrequencyCheckBox, &QCheckBox::stateChanged, m_calibrationMaxPlottingFrequencySpinBox, &QWidget::setEnabled);

    QHBoxLayout *calibrationAngleBitOffsetLayout = new QHBoxLayout;
    calibrationAngleBitOffsetLayout->addWidget(m_calibrationAngleBitOffsetCheckBox);
    calibrationAngleBitOffsetLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    calibrationAngleBitOffsetLayout->addWidget(m_calibrationAngleBitOffsetSpinBox);
    QHBoxLayout *calibrationRangeAvgCoefficientLayout = new QHBoxLayout;
    calibrationRangeAvgCoefficientLayout->addWidget(m_calibrationRangeAvgCoefficientCheckBox);
    calibrationRangeAvgCoefficientLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    calibrationRangeAvgCoefficientLayout->addWidget(m_calibrationRangeAvgCoefficientSpinBox);
    QHBoxLayout *calibrationMaxPlottingFrequencLayout = new QHBoxLayout;
    calibrationMaxPlottingFrequencLayout->addWidget(m_calibrationMaxPlottingFrequencyCheckBox);
    calibrationMaxPlottingFrequencLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    calibrationMaxPlottingFrequencLayout->addWidget(m_calibrationMaxPlottingFrequencySpinBox);

    m_calibrationAngleBitOffsetCheckBox->setEnabled(false);
    m_diagramSettingsLabel->setEnabled(false);
    m_diagramAngleBitOffsetCheckBox->setEnabled(false);
    m_diagramRangeAvgCoefficientCheckBox->setEnabled(false);
    m_diagramMaxPlottingFrequencyCheckBox->setEnabled(false);
    m_calibrationSettingsLabel->setEnabled(false);
    m_calibrationAngleBitOffsetCheckBox->setEnabled(false);
    m_calibrationRangeAvgCoefficientCheckBox->setEnabled(false);
    m_calibrationMaxPlottingFrequencyCheckBox->setEnabled(false);

    QFrame *deviderLine = new QFrame;
    deviderLine->setFrameShape(QFrame::HLine);
    deviderLine->setFrameShadow(QFrame::Sunken);

    m_checkAllBasicsCheckBox = new QCheckBox(QString("                        "));
    m_checkAllDefaultsCheckBox = new QCheckBox(QString("                        "));

    QHBoxLayout *allBasicsLeyout = new QHBoxLayout;
    allBasicsLeyout->addWidget(m_checkAllBasicsCheckBox);
    allBasicsLeyout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    allBasicsLeyout->addWidget(new QLabel(QString("Do you want to save current configuration?")));
    allBasicsLeyout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    allBasicsLeyout->addWidget(new QLabel(QString("                            ")));

    QHBoxLayout *allDefaultsLeyout = new QHBoxLayout;
    allDefaultsLeyout->addWidget(m_checkAllDefaultsCheckBox);
    allDefaultsLeyout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    allDefaultsLeyout->addWidget(new QLabel(QString("Next settings are appliable after restart only!")));
    allDefaultsLeyout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    allDefaultsLeyout->addWidget(new QLabel(QString("                            ")));

    connect(m_checkAllBasicsCheckBox, &QCheckBox::clicked, this, &MainWindow::slotCheckAllBasicSetiings);
    connect(m_greenBoardSettingsCheckBox, &QCheckBox::clicked, this, &MainWindow::slotBasicSettingsChanged);
    connect(m_radarSettingsCheckBox, &QCheckBox::clicked, this, &MainWindow::slotBasicSettingsChanged);
    connect(m_signalPlotCheckBox, &QCheckBox::clicked, this, &MainWindow::slotBasicSettingsChanged);
    connect(m_circuleDiagramCheckBox, &QCheckBox::clicked, this, &MainWindow::slotBasicSettingsChanged);
    connect(m_tuningPlotCheckBox, &QCheckBox::clicked, this, &MainWindow::slotBasicSettingsChanged);
    connect(m_calibrationPlotCheckBox, &QCheckBox::clicked, this, &MainWindow::slotBasicSettingsChanged);

    connect(m_checkAllDefaultsCheckBox, &QCheckBox::clicked, this, &MainWindow::slotDefaultAllBasicSetiings);
    connect(m_defaultPlotTypeCheckBox, &QCheckBox::clicked, this, &MainWindow::slotDefaultSettingsChanged);
    connect(m_darkModeCheckBox, &QCheckBox::clicked, this, &MainWindow::slotDefaultSettingsChanged);
    connect(m_calibrationModeCheckBox, &QCheckBox::clicked, this, &MainWindow::slotDefaultSettingsChanged);
    connect(m_radarControlInterfaceCheckBox, &QCheckBox::clicked, this, &MainWindow::slotDefaultSettingsChanged);
    connect(m_freeDiskSpaceWarningCheckBox, &QCheckBox::clicked, this, &MainWindow::slotDefaultSettingsChanged);
    connect(m_freeDiskSpaceStopSavingCheckBox, &QCheckBox::clicked, this, &MainWindow::slotDefaultSettingsChanged);
    connect(m_continueSavingAfterPathChangedCheckBox, &QCheckBox::clicked, this, &MainWindow::slotDefaultSettingsChanged);

    QGridLayout *savingIniSettingsLayout = new QGridLayout;
    savingIniSettingsLayout->addLayout(allBasicsLeyout, 0, 0, 1, 5);
    savingIniSettingsLayout->addItem(new QSpacerItem(10, 10), 1, 0, 1, 5);
    savingIniSettingsLayout->addWidget(m_greenBoardSettingsCheckBox, 2, 0, 1, 2);
    savingIniSettingsLayout->addWidget(m_radarSettingsCheckBox, 2, 3, 1, 2);
    savingIniSettingsLayout->addWidget(m_signalPlotCheckBox, 3, 0, 1, 2);
    savingIniSettingsLayout->addWidget(m_circuleDiagramCheckBox, 3, 3, 1, 2);
    savingIniSettingsLayout->addWidget(m_tuningPlotCheckBox, 4, 0, 1, 2);
    savingIniSettingsLayout->addWidget(m_calibrationPlotCheckBox, 4, 3, 1, 2);
    savingIniSettingsLayout->addItem(new QSpacerItem(10, 10), 5, 0, 1, 5);
    savingIniSettingsLayout->addLayout(allDefaultsLeyout, 6, 0, 1, 5);
    savingIniSettingsLayout->addItem(new QSpacerItem(10, 10), 7, 0, 1, 5);
    savingIniSettingsLayout->addLayout(defaultPlotTypeLayout, 8, 0, 1, 2);
    savingIniSettingsLayout->addLayout(darkModeLayout, 8, 3, 1, 2);
    savingIniSettingsLayout->addLayout(calibrationModeLayout, 9, 0, 1, 2);
    savingIniSettingsLayout->addLayout(radarControlInterfaceLayout, 9, 3, 1, 2);
    savingIniSettingsLayout->addLayout(freeDiskSpaceWarningLayout, 10, 0, 1, 2);
    savingIniSettingsLayout->addLayout(freeDiskSpaceStopSavingLayout, 10, 3, 1, 2);
    savingIniSettingsLayout->addLayout(continueSavingAfterPathChangedLayout, 11, 0, 1, 5);
    savingIniSettingsLayout->addLayout(motorControlAutoStartLayout, 12, 0, 1, 2);
    savingIniSettingsLayout->addLayout(extendedValuesLayout, 13, 0, 1, 5);
    savingIniSettingsLayout->addWidget(deviderLine, 14, 0, 1, 5);
    savingIniSettingsLayout->addLayout(samplingFrequencyLayout, 15, 0, 1, 2);
    savingIniSettingsLayout->addLayout(signalLengthLayout, 15, 3, 1, 2);
    savingIniSettingsLayout->addLayout(adcBitrRateLayout, 16, 0, 1, 2);
    savingIniSettingsLayout->addLayout(adcMaxValueLayout, 16, 3, 1, 2);
    savingIniSettingsLayout->addWidget(m_diagramSettingsLabel, 17, 0, 1, 2);
    savingIniSettingsLayout->addLayout(diagramAngleBitOffsetLayout, 18, 0, 1, 2);
    savingIniSettingsLayout->addLayout(diagramRangeAvgCoefficientLayout, 19, 0, 1, 2);
    savingIniSettingsLayout->addLayout(diagramMaxPlottingFrequencLayout, 20, 0, 1, 2);
    savingIniSettingsLayout->addWidget(m_calibrationSettingsLabel, 17, 3, 1, 2);
    savingIniSettingsLayout->addLayout(calibrationAngleBitOffsetLayout, 18, 3, 1, 2);
    savingIniSettingsLayout->addLayout(calibrationRangeAvgCoefficientLayout, 19, 3, 1, 2);
    savingIniSettingsLayout->addLayout(calibrationMaxPlottingFrequencLayout, 20, 3, 1, 2);
    savingIniSettingsLayout->addLayout(greenBoardNetSettingsLayout, 21, 0, 1, 2);
    savingIniSettingsLayout->addLayout(radarBoardNetSettingsLayout, 21, 3, 1, 2);
    savingIniSettingsLayout->addLayout(buttsLayout, 22, 0, 1, 5);

    savingIniSettingsDialog->setWindowTitle(QString("Ini File Manager"));
    savingIniSettingsDialog->setLayout(savingIniSettingsLayout);

    m_openingIniSettingsDialog = new QMessageBox;
    m_openingIniSettingsAskAgain = new QCheckBox(QString("Do not ask again"));
    m_openingIniSettingsDialog->setWindowTitle(QString("Open ini settings"));
    m_openingIniSettingsDialog->setText("Do you want to set up new settings?");
    m_openingIniSettingsDialog->setCheckBox(m_openingIniSettingsAskAgain);
    m_openingIniSettingsDialog->setStandardButtons(QMessageBox::Cancel | QMessageBox::Open);

    return savingIniSettingsDialog;
}

void MainWindow::darkMode(const bool &enable)
{
    if (enable) {
        QPalette darkPalette;
        darkPalette.setColor(QPalette::Window, QColor(53,53,53));
        darkPalette.setColor(QPalette::WindowText, QColor(250,250,250));
        darkPalette.setColor(QPalette::Base, QColor(25,25,25));
        darkPalette.setColor(QPalette::AlternateBase, QColor(53,53,53));
        darkPalette.setColor(QPalette::ToolTipBase, QColor(53,53,53));
        darkPalette.setColor(QPalette::ToolTipText, QColor(220,220,220));
        darkPalette.setColor(QPalette::Text, QColor(220,220,220));
        darkPalette.setColor(QPalette::Button, QColor(53,53,53));
        darkPalette.setColor(QPalette::ButtonText, QColor(250,250,250));
        darkPalette.setColor(QPalette::BrightText, Qt::red);
        darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));

        darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
        darkPalette.setColor(QPalette::HighlightedText, Qt::black);
        setPalette(darkPalette);
        m_greenBoardContol->setFullPalette(darkPalette);
        m_scheduleWorkerUI->setFullPalette(darkPalette);
        if (m_serialPortControl) {
            m_radarSerialControl->setFullPalette(darkPalette);
        } else {
            m_radarEthernetControl->setPalette(darkPalette);
        }
        m_mainPlot->setFullPalette(darkPalette);
    } else {
        QPalette whitePalette;
        setPalette(whitePalette);
        m_greenBoardContol->setPalette(whitePalette);
        if (m_serialPortControl) {
            m_radarSerialControl->setFullPalette(whitePalette);
        } else {
            m_radarEthernetControl->setPalette(whitePalette);
        }
        m_mainPlot->setFullPalette(whitePalette);
    }
}

void MainWindow::darkModeSwitchClicked()
{
    if (m_darkModeSwitch->isChecked()) {
        darkMode(true);
        SlotLogEvent(QString("Main: Dark Mode Enabled"));
    } else {
        darkMode(false);
        SlotLogEvent(QString("Main: Light Mode Enabled"));
    }
}

void MainWindow::statusBarMessage(const QString &statusMessage)
{
    m_statusbar->showMessage(statusMessage);
}

void MainWindow::SlotResizeWindow()
{
    resize(QSize(800, 800));
    adjustSize();
    resize(QSize(0, 0));
    adjustSize();
}

void MainWindow::slotSetScheduleName(const QString &name)
{
    QString schedule = QString("Schedule");
    if (name != QString("")) {
        schedule += QString(" - ");
    }
    m_mainTabWidget->setTabText(1, schedule + name);
}

void MainWindow::slotScheduleWorkerStarted(const bool &isStarted)
{
    m_saveIniFile->setEnabled(!isStarted);
    m_openIniFile->setEnabled(!isStarted);
}

void MainWindow::slotSetAntennaRotating(const bool &isRotating)
{
    m_radarEthernetControl->SlotSetTrasmitterOn(isRotating);
    m_radarEthernetControl->SlotSetAnttennaRotating(isRotating);
}

void MainWindow::slotSetTransmitionOn(const bool &isTransmitting)
{
    m_greenBoardContol->slotSetTriggerOutputOn(isTransmitting);
}

void MainWindow::slotSetDataAcquisitionOn(const bool &isSaving)
{
    m_greenBoardContol->slotSetDataSavingOn(isSaving);
}

void MainWindow::slotSetCurrentScheduleSettings(const ScheduleLine &scheduleLine)
{
    ScheduleLine currentSettings = scheduleLine;
    QMap<QString, QString> m_iniSettings;

    switch (currentSettings.slotGetTriggerFrequency()) {
    case TriggerFrequency::T1kHz : {
        m_iniSettings["BoardInrernalTriggerFrequency"] = QString("0");
        m_iniSettings["BoardTriggerFrequency"] = QString("0");
        break;
    }
    case TriggerFrequency::T2kHz : {
        m_iniSettings["BoardInrernalTriggerFrequency"] = QString("1");
        m_iniSettings["BoardTriggerFrequency"] = QString("1");
        break;
    }
    case TriggerFrequency::T3kHz : {
        m_iniSettings["BoardInrernalTriggerFrequency"] = QString("1");
        m_iniSettings["BoardTriggerFrequency"] = QString("2");
        break;
    }
    case TriggerFrequency::T4kHz : {
        m_iniSettings["BoardInrernalTriggerFrequency"] = QString("1");
        m_iniSettings["BoardTriggerFrequency"] = QString("3");
        break;
    }
    }

    switch (currentSettings.slotGetUnAmpSignalLength()) {
    case UnAmpSignalLength::Length32 : {
        m_iniSettings["BoardAdcALength"] = QString("0");
        break;
    }
    case UnAmpSignalLength::Length64 : {
        m_iniSettings["BoardAdcALength"] = QString("1");
        break;
    }
    case UnAmpSignalLength::Length128 : {
        m_iniSettings["BoardAdcALength"] = QString("2");
        break;
    }
    case UnAmpSignalLength::Length256 : {
        m_iniSettings["BoardAdcALength"] = QString("3");
        break;
    }
    }

    switch (currentSettings.slotGetSamplingFrequency()) {
    case SamplingFrequency::Fs1250kHz : {
        m_iniSettings["BoardDecimation"] = QString("6");
        break;
    }
    case SamplingFrequency::Fs2500kHz : {
        m_iniSettings["BoardDecimation"] = QString("5");
        break;
    }
    case SamplingFrequency::Fs5MHz : {
        m_iniSettings["BoardDecimation"] = QString("4");
        break;
    }
    case SamplingFrequency::Fs10MHz : {
        m_iniSettings["BoardDecimation"] = QString("3");
        break;
    }
    case SamplingFrequency::Fs20MHz : {
        m_iniSettings["BoardDecimation"] = QString("2");
        break;
    }
    case SamplingFrequency::Fs40MHz : {
        m_iniSettings["BoardDecimation"] = QString("1");
        break;
    }
    case SamplingFrequency::Fs80MHz : {
        m_iniSettings["BoardDecimation"] = QString("0");
        break;
    }
    }

    switch (currentSettings.slotGetFileSize()) {
    case FileSize::Size128MB : {
        m_iniSettings["MaxFileSize"] = QString("0");
        break;
    }
    case FileSize::Size256MB : {
        m_iniSettings["MaxFileSize"] = QString("1");
        break;
    }
    case FileSize::Size512MB : {
        m_iniSettings["MaxFileSize"] = QString("2");
        break;
    }
    case FileSize::Size1024MB : {
        m_iniSettings["MaxFileSize"] = QString("3");
        break;
    }
    case FileSize::Size2048MB : {
        m_iniSettings["MaxFileSize"] = QString("4");
        break;
    }
    case FileSize::Size4096MB : {
        m_iniSettings["MaxFileSize"] = QString("5");
        break;
    }
    case FileSize::Size8192MB : {
        m_iniSettings["MaxFileSize"] = QString("6");
        break;
    }
    case FileSize::Size16384MB : {
        m_iniSettings["MaxFileSize"] = QString("7");
        break;
    }
    case FileSize::Size32768MB : {
        m_iniSettings["MaxFileSize"] = QString("8");
        break;
    }
    }

    switch (currentSettings.slotGetAmpSignalBegin()) {
    case AmpSignalOption::Datagram1 : {
        m_iniSettings["FirstSavingDatagram"] = QString("0");
        break;
    }
    case AmpSignalOption::Datagram2 : {
        m_iniSettings["FirstSavingDatagram"] = QString("1");
        break;
    }
    case AmpSignalOption::Datagram3 : {
        m_iniSettings["FirstSavingDatagram"] = QString("2");
        break;
    }
    case AmpSignalOption::Datagram4 : {
        m_iniSettings["FirstSavingDatagram"] = QString("3");
        break;
    }
    case AmpSignalOption::Datagram5 : {
        m_iniSettings["FirstSavingDatagram"] = QString("4");
        break;
    }
    case AmpSignalOption::Datagram6 : {
        m_iniSettings["FirstSavingDatagram"] = QString("5");
        break;
    }
    case AmpSignalOption::Datagram7 : {
        m_iniSettings["FirstSavingDatagram"] = QString("6");
        break;
    }
    case AmpSignalOption::Datagram8 : {
        m_iniSettings["FirstSavingDatagram"] = QString("7");
        break;
    }
    }

    switch (currentSettings.slotGetAmpSignalEnd()) {
    case AmpSignalOption::Datagram1 : {
        m_iniSettings["LastSavingDatagram"] = QString("0");
        break;
    }
    case AmpSignalOption::Datagram2 : {
        m_iniSettings["LastSavingDatagram"] = QString("1");
        break;
    }
    case AmpSignalOption::Datagram3 : {
        m_iniSettings["LastSavingDatagram"] = QString("2");
        break;
    }
    case AmpSignalOption::Datagram4 : {
        m_iniSettings["LastSavingDatagram"] = QString("3");
        break;
    }
    case AmpSignalOption::Datagram5 : {
        m_iniSettings["LastSavingDatagram"] = QString("4");
        break;
    }
    case AmpSignalOption::Datagram6 : {
        m_iniSettings["LastSavingDatagram"] = QString("5");
        break;
    }
    case AmpSignalOption::Datagram7 : {
        m_iniSettings["LastSavingDatagram"] = QString("6");
        break;
    }
    case AmpSignalOption::Datagram8 : {
        m_iniSettings["LastSavingDatagram"] = QString("7");
        break;
    }
    }

    m_iniSettings["DefaultFileName"] = currentSettings.slotGetBasicFileName();

    m_greenBoardContol->setIniSettings(m_iniSettings, GreenBoardIniSettingsGroup::ScheduleParameters);
}

void MainWindow::slotExtendedValuesPushButtonClicked()
{
    bool checkState = m_extendedValuesPushButton->isChecked();

    if (checkState) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(QString("Advanced parameters editing"));
        msgBox.setText(QString("Are you sure you want to change advanced application settings?"));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Yes) {
            m_samplingFrequencyCheckBox->setEnabled(checkState);
            m_signalLengthCheckBox->setEnabled(checkState);
            m_adcBitrRateCheckBox->setEnabled(checkState);
            m_adcMaxValueCheckBox->setEnabled(checkState);
            m_greenBoardNetworkSettings->setEnabled(checkState);
            m_radarBoardNetworkSettings->setEnabled(checkState);
            m_radarBoardSerialSettings->setEnabled(checkState);
            m_diagramAngleBitOffsetCheckBox->setEnabled(checkState);
            m_calibrationAngleBitOffsetCheckBox->setEnabled(checkState);
            m_diagramSettingsLabel->setEnabled(checkState);
            m_diagramAngleBitOffsetCheckBox->setEnabled(checkState);
            m_diagramRangeAvgCoefficientCheckBox->setEnabled(checkState);
            m_diagramMaxPlottingFrequencyCheckBox->setEnabled(checkState);
            m_calibrationSettingsLabel->setEnabled(checkState);
            m_calibrationAngleBitOffsetCheckBox->setEnabled(checkState);
            m_calibrationRangeAvgCoefficientCheckBox->setEnabled(checkState);
            m_calibrationMaxPlottingFrequencyCheckBox->setEnabled(checkState);
        } else {
            m_extendedValuesPushButton->click();
            checkState = false;
        }
    }

    m_samplingFrequencyCheckBox->setEnabled(checkState);
    m_signalLengthCheckBox->setEnabled(checkState);
    m_adcBitrRateCheckBox->setEnabled(checkState);
    m_adcMaxValueCheckBox->setEnabled(checkState);
    m_greenBoardNetworkSettings->setEnabled(checkState);
    m_radarBoardNetworkSettings->setEnabled(checkState);
    m_radarBoardSerialSettings->setEnabled(checkState);
    m_diagramAngleBitOffsetCheckBox->setEnabled(checkState);
    m_calibrationAngleBitOffsetCheckBox->setEnabled(checkState);
    m_diagramSettingsLabel->setEnabled(checkState);
    m_diagramAngleBitOffsetCheckBox->setEnabled(checkState);
    m_diagramRangeAvgCoefficientCheckBox->setEnabled(checkState);
    m_diagramMaxPlottingFrequencyCheckBox->setEnabled(checkState);
    m_calibrationSettingsLabel->setEnabled(checkState);
    m_calibrationAngleBitOffsetCheckBox->setEnabled(checkState);
    m_calibrationRangeAvgCoefficientCheckBox->setEnabled(checkState);
    m_calibrationMaxPlottingFrequencyCheckBox->setEnabled(checkState);

    if (!checkState) {
        if (checkState != m_greenBoardNetworkSettings->isChecked()) {
            m_greenBoardNetworkSettings->setChecked(checkState);
        }
        if (checkState != m_radarBoardNetworkSettings->isChecked()) {
            m_radarBoardNetworkSettings->setChecked(checkState);
        }
        if (checkState != m_radarBoardSerialSettings->isChecked()) {
            m_radarBoardSerialSettings->setChecked(checkState);
        }
    }
}

void MainWindow::SlotLogEvent(const QString &logEvent)
{
    m_applicationLogger->SlotLogEvent(logEvent);
}

void MainWindow::slotRadarControlInterfaceChanged(const int &index)
{
    m_radarBoardNetworkSettings->setVisible(false);
    m_radarPcHostAddress->setVisible(false);
    m_radarPcControlPort->setVisible(false);
    m_radarPcDataPort->setVisible(false);
    m_radarBoardHostAddress->setVisible(false);
    m_radarBoardControlPort->setVisible(false);
    m_radarBoardDataPort->setVisible(false);

    m_radarPcHostAddressLabel->setVisible(false);
    m_radarPcControlPortLabel->setVisible(false);
    m_radarPcDataPortLabel->setVisible(false);
    m_radarBoardHostAddressLabel->setVisible(false);
    m_radarBoardControlPortLabel->setVisible(false);
    m_radarBoardDataPortLabel->setVisible(false);

    m_radarBoardSerialSettings->setVisible(false);
    m_portNameLineEdit->setVisible(false);
    m_baudRateComboBox->setVisible(false);
    m_dataBitsComboBox->setVisible(false);
    m_directionComboBox->setVisible(false);
    m_flowControlComboBox->setVisible(false);
    m_parityComboBox->setVisible(false);
    m_stopBitsComboBox->setVisible(false);

    m_portNameLabel->setVisible(false);
    m_baudRateLabel->setVisible(false);
    m_dataBitsLabel->setVisible(false);
    m_flowControlLabel->setVisible(false);
    m_directionLabel->setVisible(false);
    m_parityLabel->setVisible(false);
    m_stopBitsLabel->setVisible(false);

    if (static_cast<bool>(index)) {
        m_radarBoardSerialSettings->setVisible(true);
        m_portNameLineEdit->setVisible(true);
        m_baudRateComboBox->setVisible(true);
        m_dataBitsComboBox->setVisible(true);
        m_directionComboBox->setVisible(true);
        m_flowControlComboBox->setVisible(true);
        m_parityComboBox->setVisible(true);
        m_stopBitsComboBox->setVisible(true);

        m_portNameLabel->setVisible(true);
        m_baudRateLabel->setVisible(true);
        m_dataBitsLabel->setVisible(true);
        m_flowControlLabel->setVisible(true);
        m_directionLabel->setVisible(true);
        m_parityLabel->setVisible(true);
        m_stopBitsLabel->setVisible(true);
    } else {
        m_radarBoardNetworkSettings->setVisible(true);
        m_radarPcHostAddress->setVisible(true);
        m_radarPcControlPort->setVisible(true);
        m_radarPcDataPort->setVisible(true);
        m_radarBoardHostAddress->setVisible(true);
        m_radarBoardControlPort->setVisible(true);
        m_radarBoardDataPort->setVisible(true);

        m_radarPcHostAddressLabel->setVisible(true);
        m_radarPcControlPortLabel->setVisible(true);
        m_radarPcDataPortLabel->setVisible(true);
        m_radarBoardHostAddressLabel->setVisible(true);
        m_radarBoardControlPortLabel->setVisible(true);
        m_radarBoardDataPortLabel->setVisible(true);
    }
}

void MainWindow::slotBasicSettingsChanged()
{
    bool severalChecked = m_greenBoardSettingsCheckBox->isChecked() ||
            m_radarSettingsCheckBox->isChecked() ||
            m_signalPlotCheckBox->isChecked() ||
            m_circuleDiagramCheckBox->isChecked() ||
            m_tuningPlotCheckBox->isChecked() ||
            m_calibrationPlotCheckBox->isChecked();

    bool allChecked = m_greenBoardSettingsCheckBox->isChecked() &&
            m_radarSettingsCheckBox->isChecked() &&
            m_signalPlotCheckBox->isChecked() &&
            m_circuleDiagramCheckBox->isChecked() &&
            m_tuningPlotCheckBox->isChecked() &&
            m_calibrationPlotCheckBox->isChecked();

    if (severalChecked && !allChecked) {
        m_checkAllBasicsCheckBox->setCheckState(Qt::PartiallyChecked);
    } else {
        m_checkAllBasicsCheckBox->setChecked(allChecked);
    }

    m_checkAllBasicsCheckBox->repaint();
}

void MainWindow::slotCheckAllBasicSetiings()
{
    if (m_checkAllBasicsCheckBox->checkState() == Qt::PartiallyChecked || m_checkAllBasicsCheckBox->checkState() == Qt::Checked) {
        m_checkAllBasicsCheckBox->setChecked(true);
        m_greenBoardSettingsCheckBox->setChecked(true);
        m_radarSettingsCheckBox->setChecked(true);
        m_signalPlotCheckBox->setChecked(true);
        m_circuleDiagramCheckBox->setChecked(true);
        m_tuningPlotCheckBox->setChecked(true);
        m_calibrationPlotCheckBox->setChecked(true);
    } else {
        m_greenBoardSettingsCheckBox->setChecked(false);
        m_radarSettingsCheckBox->setChecked(false);
        m_signalPlotCheckBox->setChecked(false);
        m_circuleDiagramCheckBox->setChecked(false);
        m_tuningPlotCheckBox->setChecked(false);
        m_calibrationPlotCheckBox->setChecked(false);
    }

    m_checkAllBasicsCheckBox->repaint();
}

void MainWindow::slotDefaultSettingsChanged()
{

    bool severalChecked = m_defaultPlotTypeCheckBox->isChecked() ||
            m_darkModeCheckBox->isChecked() ||
            m_calibrationModeCheckBox->isChecked() ||
            m_radarControlInterfaceCheckBox->isChecked() ||
            m_freeDiskSpaceWarningCheckBox->isChecked() ||
            m_freeDiskSpaceStopSavingCheckBox->isChecked() ||
            m_continueSavingAfterPathChangedCheckBox->isChecked();

    bool allChecked = m_defaultPlotTypeCheckBox->isChecked() &&
            m_darkModeCheckBox->isChecked() &&
            m_calibrationModeCheckBox->isChecked() &&
            m_radarControlInterfaceCheckBox->isChecked() &&
            m_freeDiskSpaceWarningCheckBox->isChecked() &&
            m_freeDiskSpaceStopSavingCheckBox->isChecked() &&
            m_continueSavingAfterPathChangedCheckBox->isChecked();

    if (severalChecked && !allChecked) {
        m_checkAllDefaultsCheckBox->setCheckState(Qt::PartiallyChecked);
    } else {
        m_checkAllDefaultsCheckBox->setChecked(allChecked);
    }

    m_checkAllDefaultsCheckBox->repaint();
}

void MainWindow::slotDefaultAllBasicSetiings()
{
    if (m_checkAllDefaultsCheckBox->checkState() == Qt::PartiallyChecked || m_checkAllDefaultsCheckBox->checkState() == Qt::Checked) {
        m_checkAllDefaultsCheckBox->setChecked(true);
        m_defaultPlotTypeCheckBox->setChecked(true);
        m_darkModeCheckBox->setChecked(true);
        m_calibrationModeCheckBox->setChecked(true);
        m_radarControlInterfaceCheckBox->setChecked(true);
        m_freeDiskSpaceWarningCheckBox->setChecked(true);
        m_freeDiskSpaceStopSavingCheckBox->setChecked(true);
        m_continueSavingAfterPathChangedCheckBox->setChecked(true);
    } else {
        m_defaultPlotTypeCheckBox->setChecked(false);
        m_darkModeCheckBox->setChecked(false);
        m_calibrationModeCheckBox->setChecked(false);
        m_radarControlInterfaceCheckBox->setChecked(false);
        m_freeDiskSpaceWarningCheckBox->setChecked(false);
        m_freeDiskSpaceStopSavingCheckBox->setChecked(false);
        m_continueSavingAfterPathChangedCheckBox->setChecked(false);
    }

    m_checkAllDefaultsCheckBox->repaint();
}

void MainWindow::slotSaveIniConfiguration()
{
    m_defaultPlotTypeComboBox->setCurrentIndex(m_iniSettings["DefaultPlotType"].toUInt());
    m_darkModeComboBox->setCurrentIndex(m_iniSettings["DarkMode"].toUInt());
    m_calibrationModeComboBox->setCurrentIndex(m_iniSettings["CalibrationMode"].toUInt());
    m_radarControlInterfaceComboBox->setCurrentIndex(m_iniSettings["RadarControlInterface"].toUInt());
    m_freeDiskSpaceWarningSpinBox->setValue(m_iniSettings["FreeDiskSpaceWarning"].toUInt());
    m_freeDiskSpaceStopSavingSpinBox->setValue(m_iniSettings["FreeDiskSpaceStopSaving"].toUInt());;
    m_continueSavingAfterPathChangedComboBox->setCurrentIndex(m_iniSettings["ContinueSavingAfterPathChanged"].toUInt());
    m_motorControlAutoStartComboBox->setCurrentIndex(m_iniSettings["MotorControlAutoStart"].toUInt());

    m_samplingFrequencyComboBox->setValue(m_iniSettings["SamplingFrequency"].toUInt());
    m_signalLengthComboBox->setValue(m_iniSettings["FrameLength"].toUInt());
    m_adcBitrRateComboBox->setValue(m_iniSettings["AdcBitRateWithSign"].toUInt());
    m_adcMaxValueComboBox->setValue(m_iniSettings["AdcMaxValue"].toUInt());

    m_pcHostAddress->setText(m_iniSettings["PcHostAddress"]);
    m_pcControlPort->setValue(m_iniSettings["PcControlPort"].toUInt());
    m_pcDataPort->setValue(m_iniSettings["PcDataPort"].toUInt());
    m_boardHostAddress->setText(m_iniSettings["BoardHostAddress"]);
    m_boardControlPort->setValue(m_iniSettings["BoardControlPort"].toUInt());
    m_boardDataPort->setValue(m_iniSettings["BoardDataPort"].toUInt());

    m_radarPcHostAddress->setText(m_iniSettings["HostAddrressPC"]);
    m_radarPcControlPort->setValue(m_iniSettings["ReadPortPC"].toUInt());
    m_radarPcDataPort->setValue(m_iniSettings["WritePortPC"].toUInt());
    m_radarBoardHostAddress->setText(m_iniSettings["HostAddressRADAR"]);
    m_radarBoardControlPort->setValue(m_iniSettings["ReadPortRADAR"].toUInt());
    m_radarBoardDataPort->setValue(m_iniSettings["WritePortRADAR"].toUInt());

    m_portNameLineEdit->setText(m_iniSettings["SerialPortName"]);
    m_baudRateComboBox->setCurrentIndex(m_iniSettings["SerialPortBaudRate"].toUInt());
    m_dataBitsComboBox->setCurrentIndex(m_iniSettings["SerialPortDataBits"].toUInt());
    m_directionComboBox->setCurrentIndex(m_iniSettings["SerialPortDirection"].toUInt());
    m_flowControlComboBox->setCurrentIndex(m_iniSettings["SerialPortFlowControl"].toUInt());
    m_parityComboBox->setCurrentIndex(m_iniSettings["SerialPortParity"].toUInt());
    m_stopBitsComboBox->setCurrentIndex(m_iniSettings["SerialPortStopBits"].toUInt());

    m_diagramAngleBitOffsetSpinBox->setValue(m_iniSettings["AngleBitOffset"].toUInt());
    m_diagramRangeAvgCoefficientSpinBox->setValue(m_iniSettings["RangeAvgCoef"].toUInt());
    m_diagramMaxPlottingFrequencySpinBox->setValue(m_iniSettings["MaxPlottingFrequency"].toUInt());

    m_calibrationAngleBitOffsetSpinBox->setValue(m_iniSettings["CalibrationAngleBitOffset"].toUInt());
    m_calibrationRangeAvgCoefficientSpinBox->setValue(m_iniSettings["CalibrationRangeAvgCoef"].toUInt());
    m_calibrationMaxPlottingFrequencySpinBox->setValue(m_iniSettings["CalibrationMaxPlottingFrequency"].toUInt());

    slotRadarControlInterfaceChanged(static_cast<int>(m_serialPortControl));

    int ret = m_savingIniSettingsDialog->exec();

    if (ret) {
        QString fileSavingName = QFileDialog::getSaveFileName(this, QString("Saving Ini Settings File"), QString(""), QString("*.ini"), nullptr, QFileDialog::DontUseNativeDialog);
        if (fileSavingName != QString("")) {
            fileSavingName.remove(".ini");
            fileSavingName += QString(".ini");
            QMap<QString, QString> iniSettings;

            iniSettings = getIniSettings();

            if (m_defaultPlotTypeCheckBox->isChecked()) {
                iniSettings.insert(QString("DefaultPlotType"), QString::number(m_defaultPlotTypeComboBox->currentIndex()));
            }
            if (m_darkModeCheckBox->isChecked()) {
                iniSettings.insert(QString("DarkMode"), QString::number(m_darkModeComboBox->currentIndex()));
            }
            if (m_calibrationModeCheckBox->isChecked()) {
                iniSettings.insert(QString("CalibrationMode"), QString::number(m_calibrationModeComboBox->currentIndex()));
            }
            if (m_radarControlInterfaceCheckBox->isChecked()) {
                iniSettings.insert(QString("RadarControlInterface"), QString::number(m_radarControlInterfaceComboBox->currentIndex()));
            }
            if (m_freeDiskSpaceWarningCheckBox->isChecked()) {
                iniSettings.insert(QString("FreeDiskSpaceWarning"), QString::number(m_freeDiskSpaceWarningSpinBox->value()));
            }
            if (m_freeDiskSpaceStopSavingCheckBox->isChecked()) {
                iniSettings.insert(QString("FreeDiskSpaceStopSaving"), QString::number(m_freeDiskSpaceStopSavingSpinBox->value()));
            }
            if (m_continueSavingAfterPathChangedCheckBox->isChecked()) {
                iniSettings.insert(QString("ContinueSavingAfterPathChanged"), QString::number(m_continueSavingAfterPathChangedComboBox->currentIndex()));
            }
            if (m_motorControlAutoStartCheckBox->isChecked()) {
                iniSettings.insert(QString("MotorControlAutoStart"), QString::number(m_motorControlAutoStartComboBox->currentIndex()));
            }

            if (m_extendedValuesPushButton->isChecked()) {

                if (m_samplingFrequencyCheckBox->isChecked()) {
                    iniSettings.insert(QString("SamplingFrequency"), QString::number(m_samplingFrequencyComboBox->value()));
                }
                if (m_signalLengthCheckBox->isChecked()) {
                    iniSettings.insert(QString("FrameLength"), QString::number(m_signalLengthComboBox->value()));
                }
                if (m_adcBitrRateCheckBox->isChecked()) {
                    iniSettings.insert(QString("AdcBitRateWithSign"), QString::number(m_adcBitrRateComboBox->value()));
                }
                if (m_adcMaxValueCheckBox->isChecked()) {
                    iniSettings.insert(QString("AdcMaxValue"), QString::number(m_adcMaxValueComboBox->value()));
                }

                if (m_diagramAngleBitOffsetCheckBox->isChecked()) {
                    iniSettings.insert(QString("AngleBitOffset"), QString::number(m_diagramAngleBitOffsetSpinBox->value()));
                }
                if (m_diagramRangeAvgCoefficientCheckBox->isChecked()) {
                    iniSettings.insert(QString("RangeAvgCoef"), QString::number(m_diagramRangeAvgCoefficientSpinBox->value()));
                }
                if (m_diagramMaxPlottingFrequencyCheckBox->isChecked()) {
                    iniSettings.insert(QString("MaxPlottingFrequency"), QString::number(m_diagramMaxPlottingFrequencySpinBox->value()));
                }

                if (m_calibrationAngleBitOffsetCheckBox->isChecked()) {
                    iniSettings.insert(QString("CalibrationAngleBitOffset"), QString::number(m_calibrationAngleBitOffsetSpinBox->value()));
                }
                if (m_calibrationRangeAvgCoefficientCheckBox->isChecked()) {
                    iniSettings.insert(QString("CalibrationRangeAvgCoef"), QString::number(m_calibrationRangeAvgCoefficientSpinBox->value()));
                }
                if (m_calibrationMaxPlottingFrequencyCheckBox->isChecked()) {
                    iniSettings.insert(QString("CalibrationMaxPlottingFrequency"), QString::number(m_calibrationMaxPlottingFrequencySpinBox->value()));
                }


                if (m_greenBoardNetworkSettings->isChecked()) {
                    iniSettings.insert(QString("PcHostAddress"), QString(m_pcHostAddress->text()));
                    iniSettings.insert(QString("PcControlPort"), QString::number(m_pcControlPort->value()));
                    iniSettings.insert(QString("PcDataPort"), QString::number(m_pcDataPort->value()));
                    iniSettings.insert(QString("BoardHostAddress"), QString(m_boardHostAddress->text()));
                    iniSettings.insert(QString("BoardControlPort"), QString::number(m_boardControlPort->value()));
                    iniSettings.insert(QString("BoardDataPort"), QString::number(m_boardDataPort->value()));
                }

                if (m_radarSerialControl) {
                    if (m_radarBoardSerialSettings->isChecked()) {
                        iniSettings.insert(QString("SerialPortName"), QString(m_portNameLineEdit->text()));
                        iniSettings.insert(QString("SerialPortBaudRate"), QString::number(m_baudRateComboBox->currentIndex()));
                        iniSettings.insert(QString("SerialPortDataBits"), QString::number(m_dataBitsComboBox->currentIndex()));
                        iniSettings.insert(QString("SerialPortDirection"), QString::number(m_directionComboBox->currentIndex()));
                        iniSettings.insert(QString("SerialPortFlowControl"), QString::number(m_flowControlComboBox->currentIndex()));
                        iniSettings.insert(QString("SerialPortParity"), QString::number(m_parityComboBox->currentIndex()));
                        iniSettings.insert(QString("SerialPortStopBits"), QString::number(m_stopBitsComboBox->currentIndex()));
                    }
                } else {
                    if (m_radarBoardNetworkSettings->isChecked()) {
                        iniSettings.insert(QString("HostAddrressPC"), QString(m_radarPcHostAddress->text()));
                        iniSettings.insert(QString("ReadPortPC"), QString::number(m_radarPcControlPort->value()));
                        iniSettings.insert(QString("WritePortPC"), QString::number(m_radarPcDataPort->value()));
                        iniSettings.insert(QString("HostAddressRADAR"), QString(m_radarBoardHostAddress->text()));
                        iniSettings.insert(QString("ReadPortRADAR"), QString::number(m_radarBoardControlPort->value()));
                        iniSettings.insert(QString("WritePortRADAR"), QString::number(m_radarBoardDataPort->value()));
                    }

                }
            }

            setIniSettings(iniSettings);
            IniReader::saveConfiguration(m_iniSettings, fileSavingName);

            SlotLogEvent(QString("Main: New Configuration Saved"));

            if (m_setConfigAsDefaultCheckBox->isChecked()) {
                QMap<QString, QString> basicSettings;
                basicSettings = IniReader::readBasicConfigurationFile(QString("resources/settings/radar.ini"));
                basicSettings.insert(QString("config"), fileSavingName);

                IniReader::saveBasicConfiguration(basicSettings, QString("resources/settings/radar.ini"));

                SlotLogEvent(QString("Main: New Configuration Set Up As Default"));
            }
        }
    }
}

void MainWindow::slotLoadIniConfiguration()
{
    QString fileLoading;
    fileLoading = QFileDialog::getOpenFileName(this, QString("Saving Ini Settings File"), QString(""), QString("*.ini"), nullptr, QFileDialog::DontUseNativeDialog);

    if (fileLoading != QString("")) {
        int ret;
        if (m_openingIniSettingsAskAgain->isChecked()) {
            ret = QMessageBox::Open;
        } else {
            m_openingIniSettingsAskAgain->setChecked(true);
            ret = m_openingIniSettingsDialog->exec();
        }
        if (ret != QMessageBox::Cancel) {
            QMap<QString, QString> iniSettings;
            iniSettings = IniReader::readConfigurationFile(fileLoading);
            SlotLogEvent(QString("Main: New Configuration Opened"));
            setIniSettings(iniSettings);

            int ret = QMessageBox::No;

            if (!m_openDefaultsRememberChoise) {
                ret = m_openIniSettingsMessageBox->exec();
            }

            if ((ret == QMessageBox::Yes) || (m_openDefaultsRememberChoise && m_openDefaultsChoise)) {

                if (!m_openDefaultsRememberChoise) {
                    m_openDefaultsRememberChoise = m_openDefaultsRememberChoiseChechBox->isChecked();
                    m_openDefaultsChoise = true;
                }

                QMap<QString, QString> basicSettings;
                basicSettings = IniReader::readBasicConfigurationFile(QString("resources/settings/radar.ini"));
                basicSettings.insert(QString("config"), fileLoading);

                if (basicSettings[QString("redirectionLink")] != QString("")) {
                    QString realPath = basicSettings[QString("redirectionLink")];
                    basicSettings = IniReader::readBasicConfigurationFile(realPath);
                    IniReader::saveBasicConfiguration(basicSettings, realPath);
                } else {
                    IniReader::saveBasicConfiguration(basicSettings, QString("resources/settings/radar.ini"));
                }

                SlotLogEvent(QString("Main: New Configuration Set Up As Default"));
            } else if (!m_openDefaultsRememberChoise) {
                m_openDefaultsRememberChoise = m_openDefaultsRememberChoiseChechBox->isChecked();
                m_openDefaultsChoise = false;
            }
        }
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (MainWindow::closeWindow())
            event->accept();
        else
            event->ignore();
}

bool MainWindow::closeWindow()
{
    QMessageBox::StandardButton answer = QMessageBox::question(
                this,
                tr("Close the Application"),
                tr("Do you want to close the application?"),
                QMessageBox::Yes | QMessageBox::No
                );

    return answer == QMessageBox::Yes;
}
