/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef DATAPROCESSINGTHREAD_H
#define DATAPROCESSINGTHREAD_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QDataStream>
#include "shared-libs/inireader.h"
#include "shared-libs/qcustomplot.h"

class DataProcessingThread : public QThread
{
    Q_OBJECT

    // Classic variables when you work with threads
    QMutex *m_mutex;
    bool m_workingThreadEnable = false;

    // Vectors for containing current anf averaging plotting data
    QVector<double> *m_distance;
    QByteArray *m_absSignal, *m_realSignal, *m_imagSignal;
    QByteArray *m_absSignalLin, *m_realSignalLin, *m_imagSignalLin;

    // Angle offset variable
    quint16 m_angleOffset = 0;

    // Raw data container
    QList<QByteArray> *m_dataToProcess;

    // Variables for signal averaging
    quint16 m_avgCounter = 0; // Counter
    quint16 m_signalAvgCoefficient = 1; // Upper limit

    QCustomPlot *m_signalPlot;

    // Timer for limit number of replots (against freezing)
    QTimer *m_plotTimer;
    // Time between replot (1/MaxFPS)
    quint16 m_plotTimerDelay;

    int m_beamLength = 2048;
    double m_samplingFrequency = 80000000;
    double m_lightSpeed = 299792458;

    quint16 m_adcChannelALength = 32;

    QMap<QString, QString> m_iniSettings;

    bool m_logScale = false;

    double m_lowerSignalLimit;
    double m_upperSignalLimit;

public:
    explicit DataProcessingThread(QCustomPlot *signalPlot, QMap<QString, QString> iniSettings = IniReader::defaultConfiguration(), QObject *parent = nullptr);
    ~DataProcessingThread();

    // Ha-ha, classic...
    void run() override;

    void setIniSettings(QMap<QString, QString> &iniSettings);

private:

    // Mehtod for perocessing raw data
    quint16 dataProcessing(const QByteArray &radarData);

private slots:

    void SlotDrawData();

public slots:
    // Clearly
    void startWorkingThread();
    void stopWorkingThread();

    // Slot for saving new raw data in buffer
    void SlotRadarData(const QByteArray &radarData);
    // Slot for changing angle offset from gui or another sourse of corresponding signal
    void SlotSetAngleOffset(const quint16 &angleOffset);

    void SlotSamplingFrequencyChanged(const int &samplingFrequency);

    void SlotClearImage();

    void SlotAdcChannelALengthChanged(const quint16 &length);

    void SlotSignalLogStateChanged(const bool &checkState);

    void SlotSetSignalLimits(const double &lowerLimit, const double &upperLimit);

signals:
    //Clearly
    void poolStatus(const bool &status);
    // Signal to send processed raw data ready to draw to the signal diagram
    void SignalDataToDraw(const QByteArray &absData, const quint16 &angleValue = 0);

    void SignalToCheck(const QByteArray &realValues, const QByteArray &imagValues, const QByteArray &absValues);
};

#endif // DATAPROCESSINGTHREAD_H
