/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "signalqualityplot.h"

#include <QVBoxLayout>
#include "shared-libs/inireader.h"
#include "shared-libs/global.h"

SignalQualityPlot::SignalQualityPlot(QMap<QString, QString> iniSettings, QWidget *parent) : QWidget(parent)
{
    m_channelAlength = 32*(iniSettings["BoardAdcALength"].toInt() + 1);
    m_beamLength = iniSettings["FrameLength"].toInt();

    m_signalQualityPlot = new QCustomPlot;
    // Adjust main parameters
    int size = 226;
    m_signalQualityPlot->setMinimumSize(size + 11, size);
    m_signalQualityPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    m_signalQualityPlot->axisRect()->setupFullAxesBox(true);
    m_signalQualityPlot->addGraph();
    m_signalQualityPlot->graph(0)->setPen(QPen(Qt::black));
    m_signalQualityPlot->addGraph();
    m_signalQualityPlot->graph(1)->setPen(QPen(QBrush(Qt::red), 3));
    m_signalQualityPlot->graph(1)->setScatterStyle(QCPScatterStyle::ScatterShape::ssDot);
    m_signalQualityPlot->graph(1)->setLineStyle(QCPGraph::LineStyle::lsNone);
    m_signalQualityPlot->xAxis->setRange(-1, 1);
    m_signalQualityPlot->yAxis->setRange(-1, 1);
    m_signalQualityPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_signalQualityPlot->xAxis->setLabel(QString("Amplitude, V"));
    m_signalQualityPlot->yAxis->setLabel(QString("Amplitude, V"));

    m_signalAvgPlot = new QCustomPlot;
    m_signalAvgPlot->setMinimumSize(250, size);
    m_signalAvgPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    m_signalAvgPlot->axisRect()->setupFullAxesBox(true);
    m_signalAvgPlot->addGraph();
    m_signalAvgPlot->graph(0)->setPen(QPen(Qt::black));
    m_signalAvgPlot->addGraph();
    m_signalAvgPlot->graph(1)->setPen(QPen(Qt::black));
    m_signalAvgPlot->addGraph();
    m_signalAvgPlot->graph(2)->setPen(QPen(Qt::black));
    m_signalAvgPlot->addGraph();
    m_signalAvgPlot->graph(3)->setPen(QPen(Qt::red));
    m_signalAvgPlot->addGraph();
    m_signalAvgPlot->graph(4)->setPen(QPen(Qt::blue));
    m_signalAvgPlot->xAxis->setRange(0, m_channelAlength);
    m_signalAvgPlot->yAxis->setRange(-1, 1);
    m_signalAvgPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_signalAvgPlot->xAxis->setLabel(QString("Sample Number"));
    m_signalAvgPlot->yAxis->setLabel(QString("Amplitude, V"));

    m_processingThread = new SignalQualityProcessingThread(m_signalQualityPlot, m_signalAvgPlot);
    connect(this, &SignalQualityPlot::SignalDataReceived, m_processingThread, &SignalQualityProcessingThread::SlotSignalDataReceived);

    QVBoxLayout *signalAvgPlotControlLayout = new QVBoxLayout;
    QHBoxLayout *signalAvgPlotControlLayout0 = new QHBoxLayout;
    QHBoxLayout *signalAvgPlotControlLayout1 = new QHBoxLayout;

    m_signalAvgNumber = new QSpinBox;
    m_signalAvgNumber->setToolTip("Numbers of Signals Avereging for Signal Plot");
    m_signalAvgNumber->setMinimum(1);
    m_signalAvgNumber->setMaximum(1000000);
    m_signalAvgNumber->setValue(1);
    m_signalAvgNumber->setButtonSymbols(QSpinBox::NoButtons);

    m_showAbs = new QCheckBox(QString("Abs"));
    m_showAbs->setToolTip("Show Absolute Singal");
    m_showAbs->setChecked(false);
    connect(m_showAbs, &QCheckBox::stateChanged, m_processingThread, &SignalQualityProcessingThread::SlotShowAbs);
    m_showAbs->setChecked(true);

    m_showReal = new QCheckBox(QString("Re"));
    m_showReal->setToolTip("Show Real Singal");
    m_showReal->setChecked(false);
    connect(m_showReal, &QCheckBox::stateChanged, m_processingThread, &SignalQualityProcessingThread::SlotShowReal);
    m_showReal->setChecked(true);

    m_showImag = new QCheckBox(QString("Im"));
    m_showImag->setToolTip("Show Imaginary Singal");
    m_showImag->setChecked(false);
    connect(m_showImag, &QCheckBox::stateChanged, m_processingThread, &SignalQualityProcessingThread::SlotShowImag);
    m_showImag->setChecked(true);

    m_holdSignal = new QCheckBox(QString("Hold"));
    m_holdSignal->setToolTip("Freeze Signal Plot");
    connect(m_holdSignal, &QCheckBox::stateChanged, m_processingThread, &SignalQualityProcessingThread::SlotHold);
    m_holdSignal->setChecked(false);

    m_setSignalAvgNumber = new QPushButton(QString("Set"));
    connect(m_setSignalAvgNumber, &QPushButton::clicked, this, &SignalQualityPlot::SlotSetAvgClicked);

    m_signalRescale = new QPushButton(QString("Rescale"));
    connect(m_signalRescale, &QPushButton::clicked, this, &SignalQualityPlot::SlotRescaleAvgClicked);

    m_signalReset = new QPushButton(QString("Reset"));
    connect(m_signalReset, &QPushButton::clicked, m_processingThread, &SignalQualityProcessingThread::SlotResetAvgClicked);

    signalAvgPlotControlLayout0->addWidget(new QLabel(QString("Avg:")));
    signalAvgPlotControlLayout0->addWidget(m_signalAvgNumber);
    signalAvgPlotControlLayout0->addWidget(m_setSignalAvgNumber);
    signalAvgPlotControlLayout0->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));
    signalAvgPlotControlLayout0->addWidget(m_signalRescale);
    signalAvgPlotControlLayout0->addWidget(m_signalReset);
    signalAvgPlotControlLayout0->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    signalAvgPlotControlLayout1->addWidget(m_showAbs);
    signalAvgPlotControlLayout1->addWidget(m_showReal);
    signalAvgPlotControlLayout1->addWidget(m_showImag);
    signalAvgPlotControlLayout1->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    signalAvgPlotControlLayout->addLayout(signalAvgPlotControlLayout0);
    signalAvgPlotControlLayout->addLayout(signalAvgPlotControlLayout1);

    QHBoxLayout *signaQualityPlotControlLayout = new QHBoxLayout;

    signaQualityPlotControlLayout->addWidget(new QLabel("Avg:"));

    m_windowSize = new QSpinBox;
    m_windowSize->setToolTip("Numbers of Signals Avereging for Vector Diagram");
    m_windowSize->setMinimum(1);
    m_windowSize->setMaximum(1000000);
    m_windowSize->setValue(50);
    m_windowSize->setButtonSymbols(QSpinBox::NoButtons);
    signaQualityPlotControlLayout->addWidget(m_windowSize);

    m_set = new QPushButton(QString("Set"));
    connect(m_set, &QPushButton::clicked, this, &SignalQualityPlot::SlotSetClicked);
    signaQualityPlotControlLayout->addWidget(m_set);

    signaQualityPlotControlLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));

    m_avgLine = new QCheckBox(QString("Avg"));
    m_avgLine->setToolTip("Show Diagram Average Circle");
    connect(m_avgLine, &QCheckBox::stateChanged, m_processingThread, &SignalQualityProcessingThread::SlotDrawAvgLine);

    m_rescale = new QPushButton(QString("Rescale"));
    connect(m_rescale, &QPushButton::clicked, this, &SignalQualityPlot::SlotRescaleClicked);
    signaQualityPlotControlLayout->addWidget(m_rescale);

    m_reset = new QPushButton(QString("Reset"));
    connect(m_reset, &QPushButton::clicked, m_processingThread, &SignalQualityProcessingThread::SlotResetClicked);
    signaQualityPlotControlLayout->addWidget(m_reset);

    signaQualityPlotControlLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));

    QVBoxLayout *controlLayout = new QVBoxLayout;
    QHBoxLayout *controlLayout0 = new QHBoxLayout;
    QHBoxLayout *controlLayout1 = new QHBoxLayout;
    QHBoxLayout *controlLayout2 = new QHBoxLayout;

    controlLayout0->addWidget(new QLabel("Limits:"));

    m_ceilNumber = new QSpinBox;
    m_ceilNumber->setToolTip(QString("Set First Analizing Sample"));
    m_ceilNumber->setMinimum(1);
    m_ceilNumber->setMaximum(m_channelAlength);
    m_ceilNumber->setValue(8);
    m_ceilNumber->setButtonSymbols(QSpinBox::NoButtons);
    controlLayout0->addWidget(m_ceilNumber);

    m_ceilNumberSecond = new QSpinBox;
    m_ceilNumberSecond->setToolTip(QString("Set Last Analizing Sample"));
    m_ceilNumberSecond->setMinimum(0);
    m_ceilNumberSecond->setMaximum(m_channelAlength);
    m_ceilNumberSecond->setValue(0);
    m_ceilNumberSecond->setButtonSymbols(QSpinBox::NoButtons);
    controlLayout0->addWidget(m_ceilNumberSecond);

    m_setCeilNumber = new QPushButton(QString("Set"));
    connect(m_setCeilNumber, &QPushButton::clicked, this, &SignalQualityPlot::SlotSetCeilsClicked);
    controlLayout0->addWidget(m_setCeilNumber);

    m_visibleLimits = new QCheckBox(QString("Limits"));
    m_visibleLimits->setToolTip(QString("Show Analizing Sample/Window"));
    connect(m_visibleLimits, &QCheckBox::stateChanged, m_processingThread, &SignalQualityProcessingThread::SlotSetLimitsVivible);

//    m_adcA = new QRadioButton(QString("ADC A"));
//    m_adcB = new QRadioButton(QString("ADC B"));
//    controlLayout->addWidget(m_adcA);
//    controlLayout->addWidget(m_adcB);
//    m_adcA->setChecked(true);

    controlLayout1->addWidget(new QLabel(QString("Delay")));
    m_adcChannel = new QComboBox;
    m_adcChannel->setToolTip(QString("Signal Analisys Delay Switch"));
    QStringList adcChannelList;
    adcChannelList.append(QString("ADC A"));
    adcChannelList.append(QString("ADC B"));
    adcChannelList.append(QString("Manual"));
    m_adcChannel->addItems(adcChannelList);
    connect(m_adcChannel, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SignalQualityPlot::SlotAdcChannelChanged);
    controlLayout1->addWidget(m_adcChannel);

    m_analisysPositionOffset = new QSpinBox;
    m_analisysPositionOffset->setToolTip(QString("Delay Value (Samples)"));
    m_analisysPositionOffset->setMinimum(0);
    m_analisysPositionOffset->setMaximum(m_beamLength - m_channelAlength);
    m_analisysPositionOffset->setValue(0);
    m_analisysPositionOffset->setButtonSymbols(QSpinBox::NoButtons);
    m_analisysPositionOffset->setEnabled(false);
    controlLayout1->addWidget(m_analisysPositionOffset);

    controlLayout1->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));

    m_setOffset = new QPushButton("Set");
    m_setOffset->setEnabled(false);
    connect(m_setOffset, &QPushButton::clicked, this, &SignalQualityPlot::SlotSetOffsetClicked);
    controlLayout1->addWidget(m_setOffset);

    controlLayout2->addWidget(new QLabel(QString("Ch A Size")));
    m_channelAlengthChoise = new QComboBox;
    m_channelAlengthChoise->setToolTip(QString("Channel A Size"));
    QStringList channelASizeList;
    channelASizeList.append(QString("32"));
    channelASizeList.append(QString("64"));
    channelASizeList.append(QString("128"));
    channelASizeList.append(QString("256"));
    m_channelAlengthChoise->addItems(channelASizeList);
    m_channelAlengthChoise->setCurrentIndex(0);
    connect(m_channelAlengthChoise, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SignalQualityPlot::SlotSetChannelALengthChanged);
    controlLayout2->addWidget(m_channelAlengthChoise);

    controlLayout2->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding));

    m_progressBar = new QProgressBar;
    m_progressBar->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    connect(m_processingThread, &SignalQualityProcessingThread::SignalProgressValue, this, &SignalQualityPlot::SlotProgressStatus);
//    controlLayout->addWidget(m_progressBar);

    m_start = new QPushButton(QString("Start"));
    connect(m_start, &QPushButton::clicked, this, &SignalQualityPlot::SlotStartClicked);
    controlLayout2->addWidget(m_start);

    m_stop = new QPushButton(QString("Stop"));
    connect(m_stop, &QPushButton::clicked, this, &SignalQualityPlot::SlotStopClicked);
    controlLayout2->addWidget(m_stop);

    controlLayout->addLayout(controlLayout0);
    controlLayout->addLayout(controlLayout1);
    controlLayout->addLayout(controlLayout2);
//    controlLayout->addLayout(controlLayout3);

    QVBoxLayout *leftLayout = new QVBoxLayout;
//    leftLayout->addWidget(m_signalQualityPlot);
    leftLayout->addLayout(signaQualityPlotControlLayout);

    QVBoxLayout *rightLayout = new QVBoxLayout;
//    rightLayout->addWidget(m_signalAvgPlot);
    rightLayout->addLayout(signalAvgPlotControlLayout);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QGroupBox *calibrationGroupBox = new QGroupBox("Calibration Plot");
    calibrationGroupBox->setLayout(leftLayout);
    QGroupBox *signalGroupBox = new QGroupBox("Signal Plot");
    signalGroupBox->setLayout(rightLayout);
    QGroupBox *commonGroupBox = new QGroupBox;
    commonGroupBox->setLayout(controlLayout);
    mainLayout->addWidget(calibrationGroupBox);
    mainLayout->addWidget(signalGroupBox);
    mainLayout->addWidget(commonGroupBox);

    m_settings = new QToolButton;
    m_settings->setIcon(QIcon(iniSettings.value(QString("icons")) + g_pathDevider + QString("document-properties.svg")));
    connect(m_settings, &QToolButton::clicked, this, &SignalQualityPlot::SlotOpenSettings);

    m_plotWidget = new QWidget;
    QVBoxLayout *widgetLayout = new QVBoxLayout;
    QHBoxLayout *plotLayout = new QHBoxLayout;
    plotLayout->addWidget(m_signalQualityPlot);
    plotLayout->addWidget(m_signalAvgPlot);

    m_avgAmplitude = new QDoubleSpinBox;
    m_avgAmplitude->setMinimum(0);
    m_avgAmplitude->setMaximum(1);
    m_avgAmplitude->setSingleStep(0.001);
    m_avgAmplitude->setDecimals(3);
    m_avgAmplitude->setButtonSymbols(QDoubleSpinBox::NoButtons);

    m_avgAmplitudeSD = new QDoubleSpinBox;
    m_avgAmplitudeSD->setMinimum(0);
    m_avgAmplitudeSD->setMaximum(1);
    m_avgAmplitudeSD->setSingleStep(0.001);
    m_avgAmplitudeSD->setDecimals(3);
    m_avgAmplitudeSD->setButtonSymbols(QDoubleSpinBox::NoButtons);

    m_circleCoefficient = new QDoubleSpinBox;
    m_circleCoefficient->setMinimum(0);
    m_circleCoefficient->setMaximum(1);
    m_circleCoefficient->setSingleStep(0.001);
    m_circleCoefficient->setDecimals(3);
    m_circleCoefficient->setButtonSymbols(QDoubleSpinBox::NoButtons);

    m_complexEqualityCoefficient = new QDoubleSpinBox;
    m_complexEqualityCoefficient->setMinimum(0);
    m_complexEqualityCoefficient->setMaximum(1);
    m_complexEqualityCoefficient->setSingleStep(0.001);
    m_complexEqualityCoefficient->setDecimals(3);
    m_complexEqualityCoefficient->setButtonSymbols(QDoubleSpinBox::NoButtons);

    m_refresh = new QToolButton;
    m_refresh->setIcon(QIcon(iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    connect(m_refresh, &QPushButton::clicked, m_processingThread, &SignalQualityProcessingThread::SlotResetAvgClicked);
    connect(m_refresh, &QPushButton::clicked, m_processingThread, &SignalQualityProcessingThread::SlotResetClicked);

    QHBoxLayout *widgetBottomLayout = new QHBoxLayout;
    widgetBottomLayout->addWidget(m_avgLine);
    widgetBottomLayout->addWidget(m_visibleLimits);
    widgetBottomLayout->addWidget(m_holdSignal);
    widgetBottomLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
    widgetBottomLayout->addWidget(new QLabel(QString("A")));
    widgetBottomLayout->addWidget(m_avgAmplitude);
    widgetBottomLayout->addWidget(new QLabel(QString("SD")));
    widgetBottomLayout->addWidget(m_avgAmplitudeSD);
    widgetBottomLayout->addWidget(new QLabel(QString("C")));
    widgetBottomLayout->addWidget(m_circleCoefficient);
    widgetBottomLayout->addWidget(new QLabel(QString("CE")));
    widgetBottomLayout->addWidget(m_complexEqualityCoefficient);
    widgetBottomLayout->addWidget(m_refresh);
    widgetBottomLayout->addWidget(m_settings);

    widgetLayout->addLayout(plotLayout);
    widgetLayout->addLayout(widgetBottomLayout);

    m_plotWidget->setLayout(widgetLayout);

    m_progressBar->setVisible(false);

    setFullPalette(QPalette());

    SlotSetClicked();

    setLayout(mainLayout);

    m_replotTimer = new QTimer;
    connect(m_replotTimer, &QTimer::timeout, this, &SignalQualityPlot::SlotReplot);
    m_replotTimer->start(m_replotTime);

    connect(m_processingThread, &SignalQualityProcessingThread::SignalSetAvgAmplitude, this, &SignalQualityPlot::SlotSetAvgAmplitude);
    connect(m_processingThread, &SignalQualityProcessingThread::SignalSetAvgAmplitudeSD, this, &SignalQualityPlot::SlotSetAvgAmplitudeSD);
    connect(m_processingThread, &SignalQualityProcessingThread::SignalSetCircleCoefficient, this, &SignalQualityPlot::SlotSetCircleCoefficient);
    connect(m_processingThread, &SignalQualityProcessingThread::SignalSetComplexEqualityCoefficient, this, &SignalQualityPlot::SlotSetComplexEqualityCoefficient);

    setWindowTitle(QString("Tuning Calibration Tool Settings"));

    setIniSettings(iniSettings);
}

SignalQualityPlot::~SignalQualityPlot()
{
    m_processingThread->stopWorkingThread();
    m_processingThread->wait();
}

void SignalQualityPlot::setFullPalette(const QPalette &palette)
{
    setPalette(palette);

    m_signalQualityPlot->setPalette(palette);
    m_signalQualityPlot->setBackground(palette.window());

    m_signalQualityPlot->xAxis->setBasePen(QPen(palette.text(), 1));
    m_signalQualityPlot->xAxis->setTickPen(QPen(palette.text(), 1));
    m_signalQualityPlot->xAxis->setSubTickPen(QPen(palette.text(), 1));
    m_signalQualityPlot->xAxis->setTickLabelColor(palette.text().color());
    m_signalQualityPlot->xAxis->setLabelColor(palette.text().color());

    m_signalQualityPlot->yAxis->setBasePen(QPen(palette.text(), 1));
    m_signalQualityPlot->yAxis->setTickPen(QPen(palette.text(), 1));
    m_signalQualityPlot->yAxis->setSubTickPen(QPen(palette.text(), 1));
    m_signalQualityPlot->yAxis->setTickLabelColor(palette.text().color());
    m_signalQualityPlot->yAxis->setLabelColor(palette.text().color());

    m_signalQualityPlot->xAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalQualityPlot->yAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalQualityPlot->xAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalQualityPlot->yAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalQualityPlot->xAxis->grid()->setSubGridVisible(true);
    m_signalQualityPlot->yAxis->grid()->setSubGridVisible(true);
    m_signalQualityPlot->xAxis->grid()->setZeroLinePen(QPen(palette.text(), 1));
    m_signalQualityPlot->yAxis->grid()->setZeroLinePen(QPen(palette.text(), 1));

    m_signalQualityPlot->xAxis2->setBasePen(QPen(palette.text(), 1));
    m_signalQualityPlot->xAxis2->setTickPen(QPen(palette.text(), 1));
    m_signalQualityPlot->xAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_signalQualityPlot->xAxis2->setTickLabelColor(palette.text().color());
    m_signalQualityPlot->xAxis2->setLabelColor(palette.text().color());

    m_signalQualityPlot->yAxis2->setBasePen(QPen(palette.text(), 1));
    m_signalQualityPlot->yAxis2->setTickPen(QPen(palette.text(), 1));
    m_signalQualityPlot->yAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_signalQualityPlot->yAxis2->setTickLabelColor(palette.text().color());
    m_signalQualityPlot->yAxis2->setLabelColor(palette.text().color());

    m_signalQualityPlot->graph(0)->setPen(QPen(palette.text(), 1));

    m_signalQualityPlot->replot();

    m_signalAvgPlot->setPalette(palette);
    m_signalAvgPlot->setBackground(palette.window());

    m_signalAvgPlot->xAxis->setBasePen(QPen(palette.text(), 1));
    m_signalAvgPlot->xAxis->setTickPen(QPen(palette.text(), 1));
    m_signalAvgPlot->xAxis->setSubTickPen(QPen(palette.text(), 1));
    m_signalAvgPlot->xAxis->setTickLabelColor(palette.text().color());
    m_signalAvgPlot->xAxis->setLabelColor(palette.text().color());

    m_signalAvgPlot->yAxis->setBasePen(QPen(palette.text(), 1));
    m_signalAvgPlot->yAxis->setTickPen(QPen(palette.text(), 1));
    m_signalAvgPlot->yAxis->setSubTickPen(QPen(palette.text(), 1));
    m_signalAvgPlot->yAxis->setTickLabelColor(palette.text().color());
    m_signalAvgPlot->yAxis->setLabelColor(palette.text().color());

    m_signalAvgPlot->xAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalAvgPlot->yAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalAvgPlot->xAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalAvgPlot->yAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalAvgPlot->xAxis->grid()->setSubGridVisible(true);
    m_signalAvgPlot->yAxis->grid()->setSubGridVisible(true);
    m_signalAvgPlot->xAxis->grid()->setZeroLinePen(QPen(palette.text(), 1));
    m_signalAvgPlot->yAxis->grid()->setZeroLinePen(QPen(palette.text(), 1));


    m_signalAvgPlot->xAxis2->setBasePen(QPen(palette.text(), 1));
    m_signalAvgPlot->xAxis2->setTickPen(QPen(palette.text(), 1));
    m_signalAvgPlot->xAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_signalAvgPlot->xAxis2->setTickLabelColor(palette.text().color());
    m_signalAvgPlot->xAxis2->setLabelColor(palette.text().color());

    m_signalAvgPlot->yAxis2->setBasePen(QPen(palette.text(), 1));
    m_signalAvgPlot->yAxis2->setTickPen(QPen(palette.text(), 1));
    m_signalAvgPlot->yAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_signalAvgPlot->yAxis2->setTickLabelColor(palette.text().color());
    m_signalAvgPlot->yAxis2->setLabelColor(palette.text().color());

    m_signalAvgPlot->graph(2)->setPen(QPen(palette.text(), 1));
    m_signalAvgPlot->graph(1)->setPen(QPen(palette.text(), 1));
    m_signalAvgPlot->graph(0)->setPen(QPen(palette.text(), 1));

    m_signalAvgPlot->replot();
}

void SignalQualityPlot::setIniSettings(QMap<QString, QString> &iniSettings)
{
    m_iniSettings = iniSettings;
    IniReader::checkConfiguration(m_iniSettings, SettingsGroup::TuningPlotSetting);

    m_windowSize->setValue(m_iniSettings["TuningPlotAvgNumber"].toInt());
    m_signalAvgNumber->setValue(m_iniSettings["TuningSignalAvgNumber"].toInt());
    if (m_showAbs->isChecked() != static_cast<bool>(m_iniSettings["TuningSignalAbs"].toInt()))
    {
        m_showAbs->click();
    }
    if (m_showReal->isChecked() != static_cast<bool>(m_iniSettings["TuningSignalRe"].toInt()))
    {
        m_showReal->click();
    }
    if (m_showImag->isChecked() != static_cast<bool>(m_iniSettings["TuningSignalIm"].toInt()))
    {
        m_showImag->click();
    }
    m_ceilNumber->setValue(m_iniSettings["TuningPlotStartSample"].toInt());
    m_ceilNumberSecond->setValue(m_iniSettings["TuningPlotStopSample"].toInt());
    if (m_visibleLimits->isChecked() != static_cast<bool>(m_iniSettings["TuningSignalShowLimits"].toInt()))
    {
        m_visibleLimits->click();
    }
    if (m_avgLine->isChecked() != static_cast<bool>(m_iniSettings["TuningPlotAvgLine"].toInt()))
    {
        m_avgLine->click();
    }
    m_adcChannel->setCurrentIndex(m_iniSettings["TuningSignalAnalysisDelay"].toInt());
    if (m_adcChannel->currentIndex() == 2)
    {
        m_analisysPositionOffsetValue = m_iniSettings["TuningSignalCustomAnalysisDelay"].toInt();
        m_analisysPositionOffset->setValue(m_analisysPositionOffsetValue);
    }

    SlotSetClicked();
    SlotSetCeilsClicked();
    SlotSetAvgClicked();
    SlotSetOffsetClicked();
}

QMap<QString, QString> SignalQualityPlot::getIniSettings()
{
    QMap<QString, QString> iniSettings;

    iniSettings.insert(QString("TuningPlotAvgNumber"), QString::number(m_windowSize->value()));
    iniSettings.insert(QString("TuningSignalAvgNumber"), QString::number(m_signalAvgNumber->value()));
    iniSettings.insert(QString("TuningSignalAbs"), QString::number(m_showAbs->isChecked()));
    iniSettings.insert(QString("TuningSignalRe"), QString::number(m_showReal->isChecked()));
    iniSettings.insert(QString("TuningSignalIm"), QString::number(m_showImag->isChecked()));
    iniSettings.insert(QString("TuningPlotStartSample"), QString::number(m_ceilNumber->value()));
    iniSettings.insert(QString("TuningPlotStopSample"), QString::number(m_ceilNumberSecond->value()));
    iniSettings.insert(QString("TuningSignalShowLimits"), QString::number(m_visibleLimits->isChecked()));
    iniSettings.insert(QString("TuningPlotAvgLine"), QString::number(m_avgLine->isChecked()));
    iniSettings.insert(QString("TuningSignalAnalysisDelay"), QString::number(m_adcChannel->currentIndex()));

    if (m_adcChannel->currentIndex() == 2)
    {
        iniSettings.insert(QString("TuningSignalCustomAnalysisDelay"), QString::number(m_analisysPositionOffset->value()));
    } else {
        iniSettings.insert(QString("TuningSignalCustomAnalysisDelay"), QString::number(0));
    }

    m_iniSettings.insert(iniSettings);

    return iniSettings;
}

void SignalQualityPlot::SlotProgressStatus(const quint8 &progress)
{
    m_progressBar->setValue(progress);
    if (progress < 100) {
        m_progressBar->setVisible(true);
    } else {
        m_progressBar->setVisible(false);
    }
}

void SignalQualityPlot::resizeEvent(QResizeEvent *event)
{
    bool workingThreadState = m_processingThread->isWorking();
    if (workingThreadState) {
        m_processingThread->stopWorkingThread();
    }
    if (m_signalQualityPlot->height() > this->width()) {
        int size = m_signalAvgPlot->height() < m_signalAvgPlot->width() ? m_signalAvgPlot->height() : m_signalAvgPlot->width();
        m_signalQualityPlot->resize(size + 11, size);
    } else {
        int size = m_signalAvgPlot->height() < m_signalAvgPlot->width() ? m_signalAvgPlot->height() : m_signalAvgPlot->width();
        m_signalQualityPlot->resize(size + 11, size);
    }
    if (workingThreadState) {
        m_processingThread->startWorkingThread();
    }
}

void SignalQualityPlot::SlotStartClicked()
{
    m_processingThread->startWorkingThread();
}

void SignalQualityPlot::SlotStopClicked()
{
    m_processingThread->stopWorkingThread();
}

void SignalQualityPlot::SlotRescaleClicked()
{
    m_signalQualityPlot->rescaleAxes();
    m_signalQualityPlot->replot();
}

void SignalQualityPlot::SlotRescaleAvgClicked()
{
    m_signalAvgPlot->graph(1)->setData(QVector<double>(), QVector<double>());
    m_signalAvgPlot->graph(2)->setData(QVector<double>(), QVector<double>());
    m_signalAvgPlot->rescaleAxes();
    SlotSetCeilsClicked();
}

void SignalQualityPlot::SlotSetClicked()
{
    m_processingThread->SlotChangeWindowSize(m_windowSize->value());
}

void SignalQualityPlot::SlotSetCeilsClicked()
{
    m_processingThread->SlotSetCeilNumber(m_ceilNumber->value(), m_ceilNumberSecond->value());
}

void SignalQualityPlot::SlotSetAvgClicked()
{
    m_processingThread->SlotSetWindowAvgSize(m_signalAvgNumber->value());
}

void SignalQualityPlot::SlotSetOffsetClicked()
{
    m_analisysPositionOffsetValue = m_analisysPositionOffset->value();
    m_processingThread->SlotSetAdcSamplingBias(m_analisysPositionOffsetValue);
}

void SignalQualityPlot::SlotSetChannelALengthChanged(const int &index)
{
    switch (index) {
    case 0 : {
        m_channelAlength = 32;
        break;
    }
    case 1 : {
        m_channelAlength = 64;
        break;
    }
    case 2 : {
        m_channelAlength = 128;
        break;
    }
    case 3 : {
        m_channelAlength = 256;
        break;
    }
    default: {
        m_channelAlength = 32;
    }
    }

    m_analisysPositionOffset->setMaximum(m_beamLength - m_channelAlength);
    m_ceilNumber->setMaximum(m_channelAlength);
    m_ceilNumberSecond->setMaximum(m_channelAlength);
    m_processingThread->SlotSizeOfChennelAChanged(m_channelAlength);
}

void SignalQualityPlot::SlotOpenSettings()
{
    if (isVisible()) {
        close();
    } else {
        show();
    }
}

void SignalQualityPlot::SlotSetAvgAmplitude(const double &value)
{
    m_avgAmplitude->setValue(value);
}

void SignalQualityPlot::SlotSetAvgAmplitudeSD(const double &value)
{
    m_avgAmplitudeSD->setValue(value);
}

void SignalQualityPlot::SlotSetCircleCoefficient(const double &value)
{
    m_circleCoefficient->setValue(value);
}

void SignalQualityPlot::SlotSetComplexEqualityCoefficient(const double &value)
{
    m_complexEqualityCoefficient->setValue(value);
}

void SignalQualityPlot::SlotAdcChannelChanged(const int &index)
{
    switch (index) {
    case 0: {
        m_analisysPositionOffset->setValue(0);
        m_analisysPositionOffset->setEnabled(false);
        m_setOffset->setEnabled(false);
        SlotSetOffsetClicked();
        break;
    }
    case 1: {
        m_analisysPositionOffset->setValue(m_channelAlength);
        m_analisysPositionOffset->setEnabled(false);
        m_setOffset->setEnabled(false);
        SlotSetOffsetClicked();
        break;
    }
    case 2: {
        m_analisysPositionOffset->setEnabled(true);
        m_setOffset->setEnabled(true);
        break;
    }
    default: {
        m_processingThread->SlotSetAdcSamplingBias(0);
    }
    }
}

void SignalQualityPlot::SlotReplot()
{
    m_signalQualityPlot->replot();
    m_signalAvgPlot->replot();
    m_replotTimer->start(m_replotTime);
}
