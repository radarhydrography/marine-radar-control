﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SIGNALQUALITYPROCESSINGTHREAD_H
#define SIGNALQUALITYPROCESSINGTHREAD_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include "shared-libs/qcustomplot.h"

class SignalQualityProcessingThread : public QThread
{
    Q_OBJECT

    // Classic variables when you work with threads
    QMutex *m_mutex;
    bool m_workingThreadEnable = false;

    QCustomPlot *m_signalQualityPlot;
    QCustomPlot *m_signalAvgPlot;
    QCPCurve *m_avgCicrleCurve;

    quint32 m_windowSize = 50;
    quint32 m_bufferCounter = 0;
    quint32 m_windowAvgSize = 1;
    quint32 m_avgBufferCounter = 0;
    qint16 m_ceilNumFirst = 7;
    qint16 m_ceilNumSecond = 0;
    bool m_avgCirclePlot = false;

    QByteArray *m_realValuesBuffer, *m_imagValuesBuffer, *m_absValuesBuffer;
    QByteArray *m_avgReValuesBuffer, *m_avgImValuesBuffer, *m_avgAbsValuesBuffer;
    QByteArray *m_avgAbsData, *m_avgReData, *m_avgImData;
    QVector<double> *m_avgSignalAxesX;
    quint16 m_sizeOfChannelA = 32;
    bool m_showReal = false;
    bool m_showImag = false;
    bool m_showAbs = false;
    bool m_hold = false;
    bool m_visibleLimits = false;

    double m_avgR;

    quint16 m_adcSamplingBias = 0;

public:
    explicit SignalQualityProcessingThread(QCustomPlot *signalQualityPlot, QCustomPlot *signalAvgPlot, QObject *parent = nullptr);
    ~SignalQualityProcessingThread();

    // Clearly
    void startWorkingThread();
    void stopWorkingThread();
    bool isWorking();

private:
    void run() override;
    void avgCircle(const double &rad);

    void avgProcessing(QByteArray &avgSignal, const QByteArray &inputBuffer, const quint32 &windowSize);

    QByteArray bubbleSorting(const QByteArray &byteArray);

    void bubbleSorting(const QByteArray &byteArray, QByteArray &maxArray, QByteArray &minArray, const int outputSize);

    void complexSorting(const QByteArray &inputMasterByteArray, const QByteArray inputSlaveByteArray, QByteArray &outSlaveArray);

    void complexSorting(const QByteArray &realByteArray, const QByteArray imagByteArray, QByteArray &reOutArray, QByteArray &imOutArray, int outArraySize);

public slots:
    void SlotSignalDataReceived(const QByteArray &realValues, const QByteArray &imagValues, const QByteArray &absValues);
    void SlotDrawAvgLine(const bool &checkState);
    void SlotChangeWindowSize(const quint32 &value);
    void SlotSetCeilNumber(const qint16 &ceilNumFirst, const qint16 &ceilNumSecond = 0);
    void SlotSetWindowAvgSize(const quint32 &value);
    void SlotResetClicked();
    void SlotResetAvgClicked();

    void SlotShowAbs(const bool &show);
    void SlotShowReal(const bool &show);
    void SlotShowImag(const bool &show);
    void SlotHold(const bool &hold);

    void SlotSetAdcSamplingBias(const quint16 &bias);

    void SlotSizeOfChennelAChanged(const quint16 &size);

    void SlotSetLimitsVivible(const bool &visible);

signals:
    // Clearly
    void poolStatus(const bool &status);
    void SignalProgressValue(const quint8 &progress);

    void SignalSetAvgAmplitude(const double &value);
    void SignalSetAvgAmplitudeSD(const double &value);
    void SignalSetCircleCoefficient(const double &value);
    void SignalSetComplexEqualityCoefficient(const double &value);
};

#endif // SIGNALQUALITYPROCESSINGTHREAD_H
