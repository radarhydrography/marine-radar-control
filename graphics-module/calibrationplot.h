/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef CALIBRATIONPLOT_H
#define CALIBRATIONPLOT_H

#include <QWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QProgressBar>
#include <QCheckBox>

#include "shared-libs/inireader.h"
#include "shared-libs/qcustomplot.h"
#include "graphics-module/diagramprocessingthread.h"

class CalibrationPlot : public QWidget
{
    Q_OBJECT

    QCustomPlot *m_calibrationPlot;
    QCPColorMap *m_colorMap;

    DiagramProcessingThread *m_diagramProcessingThread;

    QPushButton *m_rescalePushButton;
    QDoubleSpinBox *m_coordinateX;
    QDoubleSpinBox *m_coordinateY;
    double m_coordinateXValue = 0.0;
    double m_coordinateYValue = 0.0;
    QDoubleSpinBox *m_distance;
    QDoubleSpinBox *m_angle;
    QSpinBox *m_avgDiagramCoefficient;
    quint16 m_avgDiagramCoefficientValue;
    QPushButton *m_startButton;
    QPushButton *m_stopButton;
    QDoubleSpinBox *m_angleOffset;
    QPushButton *m_setAngleOffset;
    double m_calibrationAngleOffsetValue = 0;

    QCheckBox *m_exactlyCalibrationNumber;
    bool m_exactlyCalibrationNumberValue;

    QProgressBar *m_calibrationProgressBar;

    QTimer *m_plotTimer;
    QTimer *m_statusTimer;

    bool m_showRealTimeCoordinates = false;
    bool m_rightFlag = false;

    QMap<QString, QString> m_iniSettings;

public:
    explicit CalibrationPlot(QWidget *parent = nullptr, QMap<QString, QString> iniSettings = IniReader::defaultConfiguration());
    ~CalibrationPlot();

    void setIniSettings(QMap<QString, QString> &iniSettings, const GraphicModuleSetSettings graphicModuleSetSettings = GraphicModuleSetSettings::InicializationMode);
    QMap<QString, QString> getIniSettings();

private:

public slots:

    void SlotDiagramToDraw();

    void SlotResetImageClicked();

    void resizeEvent(QResizeEvent *event) override;

    void SlotStartCalibration();

    void SlotCalibrationStarted();
    void SlotCalibrationFinished();

    void SlotRescalePlot();

    void SlotClearStatusMessage();

    void SlotSetFullPalette(const QPalette &palette);

    void SlotGetCoursorCoordinates(QMouseEvent *event);

    void SlotMouseButtonPressed(QMouseEvent *event);
    void SlotMouseButtonReleased(QMouseEvent *event);
    void SlotLeftMouseButtonDoubleClicked(QMouseEvent *event);

    void SlotSetAngleOffsetClicked();

signals:

    void SignalSignalToDiagram(const QByteArray &signal, const quint16 &angleValue);
    void SignalSamplingFrequencyChanged(const int &samplingFrequency);
    void SignalStatusBarMessage(const QString &statusMessage);
    void SignalAdcChannelALengthChanged(const quint16 &length);

};

#endif // CALIBRATIONPLOT_H
