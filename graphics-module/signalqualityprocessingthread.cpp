/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "signalqualityprocessingthread.h"
#include <math.h>

SignalQualityProcessingThread::SignalQualityProcessingThread(QCustomPlot *signalQualityPlot, QCustomPlot *signalAvgPlot, QObject *parent) : QThread(parent)
{
    m_mutex = new QMutex;

    m_signalQualityPlot = signalQualityPlot;
    m_signalAvgPlot = signalAvgPlot;
    m_realValuesBuffer = new QByteArray;
    m_imagValuesBuffer = new QByteArray;
    m_absValuesBuffer = new QByteArray;
    m_avgReValuesBuffer = new QByteArray;
    m_avgImValuesBuffer = new QByteArray;
    m_avgAbsValuesBuffer = new QByteArray;
    m_avgSignalAxesX = new QVector<double>;

    m_avgAbsData = new QByteArray;
    m_avgReData = new QByteArray;
    m_avgImData = new QByteArray;

    m_realValuesBuffer->resize(m_windowSize*sizeof (double));
    m_imagValuesBuffer->resize(m_windowSize*sizeof (double));
    m_absValuesBuffer->resize(m_windowSize*sizeof (double));

    m_avgReValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));
    m_avgImValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));
    m_avgAbsValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));

    m_avgAbsData->resize(m_sizeOfChannelA*sizeof (double));
    m_avgReData->resize(m_sizeOfChannelA*sizeof (double));
    m_avgImData->resize(m_sizeOfChannelA*sizeof (double));

    for (int i = 1; i < m_sizeOfChannelA + 1; i++) {
        m_avgSignalAxesX->append(i);
    }

    m_avgCicrleCurve = new QCPCurve(m_signalQualityPlot->xAxis, m_signalQualityPlot->yAxis);

    SlotResetClicked();
    SlotResetAvgClicked();
}

SignalQualityProcessingThread::~SignalQualityProcessingThread()
{

}

void SignalQualityProcessingThread::startWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = true;
    m_mutex->unlock();

    if (!isRunning()) {
        start();
    }
}

void SignalQualityProcessingThread::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

bool SignalQualityProcessingThread::isWorking()
{
    m_mutex->lock();
    bool working = m_workingThreadEnable;
    m_mutex->unlock();
    return working;
}

void SignalQualityProcessingThread::run()
{
    if (m_workingThreadEnable) {
        emit poolStatus(true);
    }

    double absValue;

    SlotResetClicked();

    int testSize = 25, sampleSize = 10, cSampleSize = 5;
    QByteArray maxValues, minValues, maxValuesSample, minValuesSample;
    maxValues.resize(testSize*sampleSize*sizeof (double));
    minValues.resize(testSize*sampleSize*sizeof (double));
    QByteArray realValues, imagValues, realValuesSample, imagValuesSample;
    realValues.resize(testSize*cSampleSize*sizeof (double));
    imagValues.resize(testSize*cSampleSize*sizeof (double));
    int currentTestNumber = 0;

    int avgMeasurePeriod = 25, currentStep = 0;

    while (m_workingThreadEnable) {
        m_mutex->lock();
        m_avgR = 0.0;
        for (quint32 signalNumber = 0; signalNumber < m_windowSize; signalNumber++) {
            memcpy(&absValue, m_absValuesBuffer->data() + signalNumber*sizeof (double), sizeof (double));
            m_avgR += absValue/m_windowSize;
            memcpy(&((m_signalQualityPlot->graph(1)->data()->begin() + signalNumber)->key), m_realValuesBuffer->data() + signalNumber*sizeof (double), sizeof (double));
            memcpy(&((m_signalQualityPlot->graph(1)->data()->begin() + signalNumber)->value), m_imagValuesBuffer->data() + signalNumber*sizeof (double), sizeof (double));
        }

        double avgRSD = 0;
        double minValue = 1, maxValue = 0, realValue, imagValue;
        for (quint32 signalNumber = 0; signalNumber < m_windowSize; signalNumber++) {
            memcpy(&absValue, m_absValuesBuffer->data() + signalNumber*sizeof (double), sizeof (double));
            avgRSD += pow(absValue - m_avgR, 2);
//            minValue = absValue < minValue ? absValue : minValue;
//            maxValue = absValue > maxValue ? absValue : maxValue;
        }
        bubbleSorting(*m_absValuesBuffer, maxValuesSample, minValuesSample, sampleSize);
        memcpy(maxValues.data() + currentTestNumber*sampleSize*sizeof (double), maxValuesSample.data(), sizeof (double)*sampleSize);
        memcpy(minValues.data() + currentTestNumber*sampleSize*sizeof (double), minValuesSample.data(), sizeof (double)*sampleSize);
        complexSorting(*m_realValuesBuffer, *m_imagValuesBuffer, realValuesSample, imagValuesSample, cSampleSize);
        memcpy(realValues.data() + currentTestNumber*cSampleSize*sizeof (double), realValuesSample.data(), sizeof (double)*cSampleSize);
        memcpy(imagValues.data() + currentTestNumber*cSampleSize*sizeof (double), imagValuesSample.data(), sizeof (double)*cSampleSize);

        avgRSD = sqrt(avgRSD/(m_windowSize - 1));

        if (m_avgCirclePlot) {
            avgCircle(m_avgR);
        }

        if (currentTestNumber >= testSize - 1) {
            double avgMin = 0, avgMax = 0, avgRe = 0, avgIm = 0;
            for (int valueNumber = 0; valueNumber < testSize*sampleSize; valueNumber++) {
                memcpy(&maxValue, maxValues.data() + valueNumber*sizeof (double), sizeof (double));
                memcpy(&minValue, minValues.data() + valueNumber*sizeof (double), sizeof (double));
                avgMax += maxValue/(testSize*sampleSize);
                avgMin += minValue/(testSize*sampleSize);
            }

            for (int valueNumber = 0; valueNumber < testSize*cSampleSize; valueNumber++) {
                memcpy(&realValue, realValues.data() + valueNumber*sizeof (double), sizeof (double));
                memcpy(&imagValue, imagValues.data() + valueNumber*sizeof (double), sizeof (double));
                avgRe += abs(realValue)/(testSize*cSampleSize);
                avgIm += abs(imagValue)/(testSize*cSampleSize);
            }
            emit SignalSetCircleCoefficient(avgMin/avgMax);
            emit SignalSetComplexEqualityCoefficient(avgRe/avgIm);
            currentTestNumber = 0;
        } else {
            currentTestNumber++;
        }

        if (currentStep >= avgMeasurePeriod) {
            emit SignalSetAvgAmplitude(m_avgR);
            emit SignalSetAvgAmplitudeSD(avgRSD);
        } else {
            currentStep++;
        }

        if (!m_hold) {
            if (m_windowAvgSize > 1) {
                avgProcessing(*m_avgAbsData, *m_avgAbsValuesBuffer, m_windowAvgSize);
                avgProcessing(*m_avgReData, *m_avgReValuesBuffer, m_windowAvgSize);
                avgProcessing(*m_avgImData, *m_avgImValuesBuffer, m_windowAvgSize);
            } else {
                memcpy(m_avgAbsData->data(), m_avgAbsValuesBuffer->data(), m_sizeOfChannelA*sizeof (double));
                memcpy(m_avgReData->data(), m_avgReValuesBuffer->data(), m_sizeOfChannelA*sizeof (double));
                memcpy(m_avgImData->data(), m_avgImValuesBuffer->data(), m_sizeOfChannelA*sizeof (double));
            }


            for (int i = 0; i < m_sizeOfChannelA; i++) {
                if (m_showAbs) {
                    memcpy(&(m_signalAvgPlot->graph(0)->data()->begin() + i)->value, m_avgAbsData->data() + i*sizeof (double), sizeof (double));
                }
                if (m_showReal) {
                    memcpy(&(m_signalAvgPlot->graph(3)->data()->begin() + i)->value, m_avgReData->data() + i*sizeof (double), sizeof (double));

                }
                if (m_showImag) {
                    memcpy(&(m_signalAvgPlot->graph(4)->data()->begin() + i)->value, m_avgImData->data() + i*sizeof (double), sizeof (double));

                }
            }
        }

        m_mutex->unlock();
        msleep(50);
    }

    if (!m_workingThreadEnable) {
        emit poolStatus(false);
    }
}

void SignalQualityProcessingThread::avgCircle(const double &rad)
{
    double vectorSize = 200;
    double delta = (2*rad)/vectorSize;
    QVector<double> xVector;
    QVector<double> yVector;
    for (int xCeil = 0; xCeil < vectorSize; xCeil++) {
        for (int yCeil = 0; yCeil < vectorSize; yCeil++) {
            xVector.append(-rad + delta*xCeil);
            yVector.append(sqrt(abs(pow(rad, 2) - pow(-rad + delta*xCeil, 2))));
        }
    }
    for (int xCeil = 0; xCeil < vectorSize; xCeil++) {
        for (int yCeil = 0; yCeil < vectorSize; yCeil++) {
            xVector.append(rad - delta*xCeil);
            yVector.append(-sqrt(abs(pow(rad, 2) - pow(rad - delta*xCeil, 2))));
        }
    }
    xVector.append(-rad);
    yVector.append(sqrt(abs(pow(rad, 2) - pow(-rad, 2))));
    m_avgCicrleCurve->setData(xVector, yVector);
}

void SignalQualityProcessingThread::avgProcessing(QByteArray &avgSignal, const QByteArray &inputBuffer, const quint32 &windowSize)
{
    quint32 signalSize = (inputBuffer.size()/sizeof (double))/windowSize;
    if (avgSignal.size() != signalSize*sizeof (double)) {
        avgSignal.resize(signalSize*sizeof (double));
    }
    double sampleValue, sampleBuffer;
    for (quint32 sampleNumber = 0; sampleNumber < signalSize; sampleNumber++) {
        sampleValue = 0;
        for (quint32 signalNumber = 0; signalNumber < windowSize; signalNumber++) {
            memcpy(&sampleBuffer, inputBuffer.data() + sampleNumber*sizeof (double) + signalSize*signalNumber*sizeof (double), sizeof (double));
            sampleValue += sampleBuffer/windowSize;
        }
        memcpy(avgSignal.data() + sampleNumber*sizeof (double), &sampleValue, sizeof (double));
    }
}

QByteArray SignalQualityProcessingThread::bubbleSorting(const QByteArray &byteArray)
{
    QByteArray inputArray;
    inputArray.append(byteArray);
    inputArray.append(m_windowSize*sizeof (double) - inputArray.size(), 0);
    bool continueFlag = true;
    while (continueFlag) {
        double minValue, buffer;
        continueFlag = false;
        for (int currentIndex = 0; currentIndex < static_cast<int>(inputArray.size()/sizeof (double)) - 1; currentIndex++) {
            memcpy(&minValue, inputArray.data() + currentIndex*sizeof (double), sizeof (double));
            memcpy(&buffer, inputArray.data() + (currentIndex + 1)*sizeof (double), sizeof (double));
            if (minValue < buffer) {
                memcpy(inputArray.data() + currentIndex*sizeof (double), &buffer, sizeof (double));
                memcpy(inputArray.data() + (currentIndex + 1)*sizeof (double), &minValue, sizeof (double));
                continueFlag = true;
            }
        }
    }
    return inputArray;
}

void SignalQualityProcessingThread::bubbleSorting(const QByteArray &byteArray, QByteArray &maxArray, QByteArray &minArray, const int outputSize)
{
    QByteArray inputArray = bubbleSorting(byteArray);
    maxArray.clear();
    maxArray.resize(outputSize*sizeof (double));
    minArray.clear();
    minArray.resize(outputSize*sizeof (double));
    int offset = (inputArray.size() - outputSize*sizeof (double) - 2*sizeof (double));
    memcpy(maxArray.data(), inputArray.data() + sizeof (double), outputSize*sizeof (double));
    memcpy(minArray.data(), inputArray.data() + offset, outputSize*sizeof (double));
}

void SignalQualityProcessingThread::complexSorting(const QByteArray &inputMasterByteArray, const QByteArray inputSlaveByteArray, QByteArray &outSlaveArray)
{
    QByteArray inputMasterArray;
    outSlaveArray.clear();
    inputMasterArray.append(inputMasterByteArray);
    inputMasterArray.append(m_windowSize*sizeof (double) - inputMasterArray.size(), 0);
    outSlaveArray.append(inputSlaveByteArray);
    outSlaveArray.append(m_windowSize*sizeof (double) - outSlaveArray.size(), 0);

    bool continueFlag = true;
    while (continueFlag) {
        double minValue, buffer;
        double outMinValue, outBuffer;
        continueFlag = false;
        for (int currentIndex = 0; currentIndex < static_cast<int>(inputMasterArray.size()/sizeof (double)) - 1; currentIndex++) {
            memcpy(&minValue, inputMasterArray.data() + currentIndex*sizeof (double), sizeof (double));
            memcpy(&buffer, inputMasterArray.data() + (currentIndex + 1)*sizeof (double), sizeof (double));

            memcpy(&outMinValue, outSlaveArray.data() + currentIndex*sizeof (double), sizeof (double));
            memcpy(&outBuffer, outSlaveArray.data() + (currentIndex + 1)*sizeof (double), sizeof (double));
            if (abs(minValue) < abs(buffer)) {
                memcpy(inputMasterArray.data() + currentIndex*sizeof (double), &buffer, sizeof (double));
                memcpy(inputMasterArray.data() + (currentIndex + 1)*sizeof (double), &minValue, sizeof (double));

                memcpy(outSlaveArray.data() + currentIndex*sizeof (double), &outBuffer, sizeof (double));
                memcpy(outSlaveArray.data() + (currentIndex + 1)*sizeof (double), &outMinValue, sizeof (double));
                continueFlag = true;
            }
        }
    }
}

void SignalQualityProcessingThread::complexSorting(const QByteArray &realByteArray, const QByteArray imagByteArray, QByteArray &reOutArray, QByteArray &imOutArray, int outArraySize)
{
    QByteArray realArray;
    QByteArray imagArray;
    complexSorting(realByteArray, imagByteArray, imagArray);
    complexSorting(imagByteArray, realByteArray, realArray);

    reOutArray.clear();
    imOutArray.clear();
    reOutArray.resize(outArraySize*sizeof (double));
    imOutArray.resize(outArraySize*sizeof (double));

    int offset = realArray.size() - (outArraySize - 2)*sizeof (double);

    memcpy(reOutArray.data(), realArray.data() + offset, outArraySize*sizeof (double));
    memcpy(imOutArray.data(), imagArray.data() + offset, outArraySize*sizeof (double));
}


void SignalQualityProcessingThread::SlotSignalDataReceived(const QByteArray &realValues, const QByteArray &imagValues, const QByteArray &absValues)
{
    m_mutex->lock();
    if (m_workingThreadEnable) {
        if (m_ceilNumSecond <= m_ceilNumFirst) {
            memcpy(m_realValuesBuffer->data() + m_bufferCounter*sizeof (double), realValues.data() + (m_ceilNumFirst + m_adcSamplingBias)*sizeof (double), sizeof (double));
            memcpy(m_imagValuesBuffer->data() + m_bufferCounter*sizeof (double), imagValues.data() + (m_ceilNumFirst + m_adcSamplingBias)*sizeof (double), sizeof (double));
            memcpy(m_absValuesBuffer->data() + m_bufferCounter*sizeof (double), absValues.data() + (m_ceilNumFirst + m_adcSamplingBias)*sizeof (double), sizeof (double));
            m_bufferCounter = m_bufferCounter + 1 >= m_windowSize ? 0 : m_bufferCounter + 1;
        } else {
            for (int num = m_ceilNumFirst; num < m_ceilNumSecond; num++) {
                memcpy(m_realValuesBuffer->data() + m_bufferCounter*sizeof (double), realValues.data() + (num + m_adcSamplingBias)*sizeof (double), sizeof (double));
                memcpy(m_imagValuesBuffer->data() + m_bufferCounter*sizeof (double), imagValues.data() + (num + m_adcSamplingBias)*sizeof (double), sizeof (double));
                memcpy(m_absValuesBuffer->data() + m_bufferCounter*sizeof (double), absValues.data() + (num + m_adcSamplingBias)*sizeof (double), sizeof (double));
                m_bufferCounter = m_bufferCounter + 1 >= m_windowSize ? 0 : m_bufferCounter + 1;
            }
        }

        memcpy(m_avgReValuesBuffer->data() + m_avgBufferCounter*m_sizeOfChannelA*sizeof(double), realValues.data() + m_adcSamplingBias*sizeof (double), m_sizeOfChannelA*sizeof (double));
        memcpy(m_avgImValuesBuffer->data() + m_avgBufferCounter*m_sizeOfChannelA*sizeof(double), imagValues.data() + m_adcSamplingBias*sizeof (double), m_sizeOfChannelA*sizeof (double));
        memcpy(m_avgAbsValuesBuffer->data() + m_avgBufferCounter*m_sizeOfChannelA*sizeof(double), absValues.data() + m_adcSamplingBias*sizeof (double), m_sizeOfChannelA*sizeof (double));
        m_avgBufferCounter = m_avgBufferCounter + 1 >= m_windowAvgSize ? 0 : m_avgBufferCounter + 1;

    }
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotDrawAvgLine(const bool &checkState)
{
    m_mutex->lock();
    m_avgCirclePlot = checkState;
    if (!checkState) {
        m_avgCicrleCurve->setData(QVector<double>(), QVector<double>());
    }
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotChangeWindowSize(const quint32 &value)
{
    m_mutex->lock();
    m_windowSize = value;
    m_realValuesBuffer->clear();
    m_realValuesBuffer->resize(m_windowSize*sizeof (double));
    m_imagValuesBuffer->clear();
    m_imagValuesBuffer->resize(m_windowSize*sizeof (double));
    m_absValuesBuffer->clear();
    m_absValuesBuffer->resize(m_windowSize*sizeof (double));
    m_bufferCounter = 0;
    m_mutex->unlock();
    SlotResetClicked();
}

void SignalQualityProcessingThread::SlotSetCeilNumber(const qint16 &ceilNumFirst, const qint16 &ceilNumSecond)
{
    m_mutex->lock();
    m_ceilNumFirst = (ceilNumFirst - 1) < 0 ? 0 : ceilNumFirst - 1;
    m_ceilNumSecond = (ceilNumSecond - 1) < 0  ? 0 : ceilNumSecond - 1;
    if (m_visibleLimits) {
        QVector<double> yValues;
        yValues.append(-1);
        yValues.append(1);
        if (m_ceilNumSecond <= m_ceilNumFirst) {
            m_signalAvgPlot->graph(1)->setData(QVector<double>(2, ceilNumFirst), yValues);
            m_signalAvgPlot->graph(2)->setData(QVector<double>(), QVector<double>());
        } else {
            m_signalAvgPlot->graph(1)->setData(QVector<double>(2, ceilNumFirst), yValues);
            m_signalAvgPlot->graph(2)->setData(QVector<double>(2, ceilNumSecond), yValues);
        }
    } else {
        m_signalAvgPlot->graph(1)->setData(QVector<double>(), QVector<double>());
        m_signalAvgPlot->graph(2)->setData(QVector<double>(), QVector<double>());
    }
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotSetWindowAvgSize(const quint32 &value)
{
    m_mutex->lock();
    m_windowAvgSize = value;
    m_avgBufferCounter = 0;
    m_avgReValuesBuffer->clear();
    m_avgReValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));
    m_avgImValuesBuffer->clear();
    m_avgImValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));
    m_avgAbsValuesBuffer->clear();
    m_avgAbsValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotResetClicked()
{
    m_mutex->lock();
    m_signalQualityPlot->xAxis->setRange(-1, 1);
    m_signalQualityPlot->yAxis->setRange(-1, 1);
    m_signalQualityPlot->graph(0)->setData(QVector<double>(m_windowSize, 0),  QVector<double>(m_windowSize, 0));
    m_signalQualityPlot->graph(1)->setData(QVector<double>(m_windowSize, 0),  QVector<double>(m_windowSize, 0));
    m_avgCicrleCurve->setData(QVector<double>(), QVector<double>());
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotResetAvgClicked()
{
    m_mutex->lock();
    m_signalAvgPlot->xAxis->setRange(0, m_sizeOfChannelA);
    m_signalAvgPlot->yAxis->setRange(-1, 1);
    m_signalAvgPlot->graph(0)->setData(*m_avgSignalAxesX,  QVector<double>(m_avgSignalAxesX->size(), 0));
    m_signalAvgPlot->graph(3)->setData(*m_avgSignalAxesX,  QVector<double>(m_avgSignalAxesX->size(), 0));
    m_signalAvgPlot->graph(4)->setData(*m_avgSignalAxesX,  QVector<double>(m_avgSignalAxesX->size(), 0));
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotShowAbs(const bool &show)
{
    m_mutex->lock();
    m_showAbs = show;
    if (!m_showAbs) {
        m_signalAvgPlot->graph(0)->setData(QVector<double>(),  QVector<double>());
    } else {
        m_signalAvgPlot->graph(0)->setData(*m_avgSignalAxesX,  QVector<double>(m_avgSignalAxesX->size(), 0));
    }
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotShowReal(const bool &show)
{
    m_mutex->lock();
    m_showReal = show;
    if (!m_showReal) {
        m_signalAvgPlot->graph(3)->setData(QVector<double>(),  QVector<double>());
    } else {
        m_signalAvgPlot->graph(3)->setData(*m_avgSignalAxesX,  QVector<double>(m_avgSignalAxesX->size(), 0));
    }
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotShowImag(const bool &show)
{
    m_mutex->lock();
    m_showImag = show;
    if (!m_showImag) {
        m_signalAvgPlot->graph(4)->setData(QVector<double>(),  QVector<double>());
    } else {
        m_signalAvgPlot->graph(4)->setData(*m_avgSignalAxesX,  QVector<double>(m_avgSignalAxesX->size(), 0));
    }
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotHold(const bool &hold)
{
    m_mutex->lock();
    m_hold = hold;
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotSetAdcSamplingBias(const quint16 &bias)
{
    m_mutex->lock();
    m_adcSamplingBias = bias;
    m_mutex->unlock();
}

void SignalQualityProcessingThread::SlotSizeOfChennelAChanged(const quint16 &size)
{
    m_mutex->lock();
    m_sizeOfChannelA = size;
    m_avgReValuesBuffer->clear();
    m_avgReValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));
    m_avgImValuesBuffer->clear();
    m_avgImValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));
    m_avgAbsValuesBuffer->clear();
    m_avgAbsValuesBuffer->resize(m_windowAvgSize*m_sizeOfChannelA*sizeof (double));
    m_avgAbsData->clear();
    m_avgAbsData->resize(m_sizeOfChannelA*sizeof (double));
    m_avgReData->clear();
    m_avgReData->resize(m_sizeOfChannelA*sizeof (double));
    m_avgImData->clear();
    m_avgImData->resize(m_sizeOfChannelA*sizeof (double));
    m_avgSignalAxesX->clear();
    for (int i = 1; i < m_sizeOfChannelA + 1; i++) {
        m_avgSignalAxesX->append(i);
    }
    m_mutex->unlock();
    SlotChangeWindowSize(m_windowSize);
    SlotResetClicked();
    SlotResetAvgClicked();
}

void SignalQualityProcessingThread::SlotSetLimitsVivible(const bool &visible)
{
    m_mutex->lock();
    m_visibleLimits = visible;
    m_mutex->unlock();
    SlotSetCeilNumber(m_ceilNumFirst + 1, m_ceilNumSecond + 1);
}
