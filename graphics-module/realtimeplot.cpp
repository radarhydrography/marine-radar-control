/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "realtimeplot.h"

#include <QLabel>
#include <list>
#include "shared-libs/inireader.h"
#include "shared-libs/global.h"

RealTimePlot::RealTimePlot(QMap<QString, QString> iniSettings, QWidget *parent) : QWidget(parent)
{
    setIniSettings(iniSettings);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QVBoxLayout *signalPlotLayout = new QVBoxLayout;
    m_plotsLayout = new QHBoxLayout;

    // Construct a signal plot
    m_signalPlot = new QCustomPlot;
    // Adjust main parameters
    m_signalPlot->setMinimumSize(500, 200);
    m_signalPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    m_signalPlot->axisRect()->setupFullAxesBox(true);
    m_signalPlot->addGraph();
    m_signalPlot->graph(0)->setPen(QPen(Qt::red));
    m_signalPlot->addGraph();
    m_signalPlot->graph(1)->setPen(QPen(Qt::blue));
    m_signalPlot->addGraph();
    m_beamLength = m_iniSettings["FrameLength"].toInt();
    m_samplingFrequency = m_iniSettings[QString("SamplingFrequency")].toDouble()/pow(2, m_iniSettings["BoardDecimation"].toDouble());
    m_lightSpeed = g_lightSpeed;
    m_signalPlot->xAxis->setRange(0, (m_beamLength/(2*m_samplingFrequency))*m_lightSpeed); // in meters
    m_signalPlot->yAxis->setRange(-45, 0); // just for begin, it's rescalable
    signalPlotLayout->addWidget(m_signalPlot);
    m_signalPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_signalPlot->xAxis->setLabel(QString("Distance, m"));
    m_signalPlot->yAxis->setLabel(QString("Amplitude, dB"));

    // Constructing object for processing signal data in another thread
    m_dataProcessingThread = new DataProcessingThread(m_signalPlot, iniSettings);

    // Connect Signal with raw signal data to data processing thread
    connect(this, &RealTimePlot::SignalFullBeam, m_dataProcessingThread, &DataProcessingThread::SlotRadarData);
    // Connect Signal with processed raw ready to draw data to Main Graph module
    connect(m_dataProcessingThread, &DataProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDataToDraw);
    // Clear to understand
    connect(this, &RealTimePlot::SignalAngleOffsetChanged, m_dataProcessingThread, &DataProcessingThread::SlotSetAngleOffset);

    // Construct a circle diagram
    m_polarPlot = new QCustomPlot;
    m_polarPlot->setMinimumSize(200, 200);
    m_polarPlot->xAxis->setLabel(QString("Distance, m"));
    m_polarPlot->yAxis->setLabel(QString("Distance, m"));

    // Allow rescaling by dragging/zooming
    m_polarPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    m_polarPlot->axisRect()->setupFullAxesBox(true);

    QSizePolicy polarPlotSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    polarPlotSizePolicy.setWidthForHeight(true);
    m_polarPlot->setSizePolicy(polarPlotSizePolicy);

    m_polarPlotWidget = new QGroupBox;
    QVBoxLayout *polarLayout = new QVBoxLayout;
    polarLayout->addWidget(m_polarPlot);
    QHBoxLayout *polarControlLayout = new QHBoxLayout;

    m_rescalePolarPlot = new QToolButton;
    m_resetPolarPlot = new QToolButton;

    m_rescalePolarPlot->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("zoom-fit-best.svg")));
    m_rescalePolarPlot->setToolTip(QString("Rescale Plot"));
    m_resetPolarPlot->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_resetPolarPlot->setToolTip(QString("Reset Plot"));

    connect(m_rescalePolarPlot, &QPushButton::clicked, this, &RealTimePlot::SlotRescalePolarPlot);

    m_polarIntensity = new QSpinBox;
    m_polarIntensity->setToolTip(QString("Image Intensity"));
    m_polarBrightness = new QSpinBox;
    m_polarBrightness->setToolTip(QString("Image Brightness"));
    m_polarThreshold = new QSpinBox;
    m_polarThreshold->setToolTip(QString("Signal Threshold"));
    m_polarIntensityDial = new QDial;
    m_polarIntensityDial->setToolTip(QString("Image Intensity"));
    m_polarBrightnessDial = new QDial;
    m_polarBrightnessDial->setToolTip(QString("Image Brightness"));
    m_polarThresholdDial = new QDial;
    m_polarThresholdDial->setToolTip(QString("Signal Threshold"));

    m_polarIntensity->setMinimum(0);
    m_polarIntensity->setMaximum(1000);
    m_polarIntensity->setValue(m_contrast);
    m_polarIntensity->setAlignment(Qt::AlignRight);
    m_polarIntensity->setButtonSymbols(QSpinBox::NoButtons);
    m_polarIntensityDial->setMinimum(0);
    m_polarIntensityDial->setMaximum(1000);
    m_polarIntensityDial->setValue(100);
    connect(m_polarIntensityDial, &QDial::valueChanged, m_polarIntensity, &QSpinBox::setValue);
    connect(m_polarIntensity, QOverload<int>::of(&QSpinBox::valueChanged), m_polarIntensityDial, &QDial::setValue);

    m_polarBrightness->setMinimum(0);
    m_polarBrightness->setMaximum(1000);
    m_polarBrightness->setValue(m_brightness);
    m_polarBrightness->setAlignment(Qt::AlignRight);
    m_polarBrightness->setButtonSymbols(QSpinBox::NoButtons);
    m_polarBrightnessDial->setMinimum(0);
    m_polarBrightnessDial->setMaximum(1000);
    m_polarBrightnessDial->setValue(100);
    connect(m_polarBrightnessDial, &QDial::valueChanged, m_polarBrightness, &QSpinBox::setValue);
    connect(m_polarBrightness, QOverload<int>::of(&QSpinBox::valueChanged), m_polarBrightnessDial, &QDial::setValue);

    m_polarThreshold->setMinimum(0);
    m_polarThreshold->setMaximum(100);
    m_polarThreshold->setValue(m_threshold);
    m_polarThreshold->setAlignment(Qt::AlignRight);
    m_polarThreshold->setButtonSymbols(QSpinBox::NoButtons);
    m_polarThresholdDial->setMinimum(0);
    m_polarThresholdDial->setMaximum(100);
    m_polarThresholdDial->setValue(0);
    connect(m_polarThresholdDial, &QDial::valueChanged, m_polarThreshold, &QSpinBox::setValue);
    connect(m_polarThreshold, QOverload<int>::of(&QSpinBox::valueChanged), m_polarThresholdDial, &QDial::setValue);

    connect(m_polarIntensity, QOverload<int>::of(&QSpinBox::valueChanged), this, &RealTimePlot::SLotSetIntensityCoefficient);
    connect(m_polarBrightness, QOverload<int>::of(&QSpinBox::valueChanged), this, &RealTimePlot::SLotSetBrightnessCoefficient);
    connect(m_polarThreshold, QOverload<int>::of(&QSpinBox::valueChanged), this, &RealTimePlot::SLotSetThreshold);

    m_polarIntensityDial->setMaximumSize(30, 30);
    m_polarBrightnessDial->setMaximumSize(30, 30);
    m_polarThresholdDial->setMaximumSize(30, 30);

    m_colorGradient = new QComboBox;
    m_colorGradient->setToolTip(QString("Picture Color Gradient"));
    QStringList colorGradientList;
    colorGradientList.append(QString("Grayscale"));
    colorGradientList.append(QString("Hot"));
    colorGradientList.append(QString("Cold"));
    colorGradientList.append(QString("Night"));
    colorGradientList.append(QString("Candy"));
    colorGradientList.append(QString("Geography"));
    colorGradientList.append(QString("Ion"));
    colorGradientList.append(QString("Thermal"));
    colorGradientList.append(QString("Polar"));
    colorGradientList.append(QString("Spectrum"));
    colorGradientList.append(QString("Jet"));
    colorGradientList.append(QString("Hues"));
    m_colorGradient->addItems(colorGradientList);
    m_colorGradient->setCurrentIndex(m_colorMode);
    connect(m_colorGradient, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &RealTimePlot::SlotSetColorGradient);

    QFrame *line0 = new QFrame;
    line0->setFrameShape(QFrame::VLine);
    line0->setFrameShadow(QFrame::Sunken);

    QFrame *line3 = new QFrame;
    line3->setFrameShape(QFrame::VLine);
    line3->setFrameShadow(QFrame::Sunken);

    QFrame *line4 = new QFrame;
    line4->setFrameShape(QFrame::VLine);
    line4->setFrameShadow(QFrame::Sunken);

    m_resetPolarSettings = new QToolButton;
    m_resetPolarSettings->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_resetPolarSettings->setToolTip(QString("Restore Default Values"));
    connect(m_resetPolarSettings, &QPushButton::clicked, this, &RealTimePlot::SlotResetPolarSettings);

    polarControlLayout->addWidget(new QLabel(QString("Brt")));
    polarControlLayout->addWidget(m_polarBrightness);
    polarControlLayout->addWidget(m_polarBrightnessDial);
    polarControlLayout->addWidget(new QLabel(QString("Ctr")));
    polarControlLayout->addWidget(m_polarIntensity);
    polarControlLayout->addWidget(m_polarIntensityDial);
    polarControlLayout->addWidget(new QLabel(QString("Trh")));
    polarControlLayout->addWidget(m_polarThreshold);
    polarControlLayout->addWidget(m_polarThresholdDial);
    polarControlLayout->addWidget(m_resetPolarSettings);
    polarControlLayout->addWidget(line3);
    polarControlLayout->addWidget(m_colorGradient);
    polarControlLayout->addWidget(line0);
    polarControlLayout->addWidget(m_rescalePolarPlot);
    polarControlLayout->addWidget(m_resetPolarPlot);

//    polarControlLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

    QHBoxLayout *middlePolarLayout = new QHBoxLayout;

    QLabel *xLabel = new QLabel(QString("X:"));
    middlePolarLayout->addWidget(xLabel);

    m_coordinateX = new QDoubleSpinBox;
    m_coordinateX->setToolTip(QString("Coordinate X"));
    m_coordinateX->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_coordinateX->setDecimals(3);
    m_coordinateX->setMinimum(-999999);
    m_coordinateX->setMaximum(999999);
    m_coordinateX->setReadOnly(true);
    m_coordinateX->setAlignment(Qt::AlignRight);
    m_coordinateX->setSuffix(QString(" m"));
    middlePolarLayout->addWidget(m_coordinateX);

    QLabel *yLabel = new QLabel(QString("Y:"));
    middlePolarLayout->addWidget(yLabel);

    m_coordinateY = new QDoubleSpinBox;
    m_coordinateY->setToolTip(QString("Coordinate Y"));
    m_coordinateY->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_coordinateY->setDecimals(3);
    m_coordinateY->setMinimum(-999999);
    m_coordinateY->setMaximum(999999);
    m_coordinateY->setReadOnly(true);
    m_coordinateY->setAlignment(Qt::AlignRight);
    m_coordinateY->setSuffix(QString(" m"));
    middlePolarLayout->addWidget(m_coordinateY);

    QLabel *dLabel = new QLabel(QString(" D:"));
    middlePolarLayout->addWidget(dLabel);

    m_distance = new QDoubleSpinBox;
    m_distance->setToolTip(QString("Distance"));
    m_distance->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_distance->setDecimals(3);
    m_distance->setMinimum(-999999);
    m_distance->setMaximum(999999);
    m_distance->setReadOnly(true);
    m_distance->setAlignment(Qt::AlignRight);
    m_distance->setSuffix(QString(" m"));
    middlePolarLayout->addWidget(m_distance);

    QLabel *aLabel = new QLabel(QString("φ:"));
    middlePolarLayout->addWidget(aLabel);

    m_angle = new QDoubleSpinBox;
    m_angle->setToolTip(QString("Angle"));
    m_angle->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_angle->setDecimals(3);
    m_angle->setMinimum(0);
    m_angle->setMaximum(360);
    m_angle->setReadOnly(true);
    m_angle->setAlignment(Qt::AlignRight);
    m_angle->setSuffix(QString(" °"));
    middlePolarLayout->addWidget(m_angle);

    m_resetPolarPlotPointer = new QToolButton;
    m_resetPolarPlotPointer->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_resetPolarPlotPointer->setToolTip(QString("Reset"));
    connect(m_resetPolarPlotPointer, &QPushButton::clicked, this, &RealTimePlot::SlotResetPolarPlotPointer);
    middlePolarLayout->addWidget(line4);
    middlePolarLayout->addWidget(m_resetPolarPlotPointer);

//    middlePolarLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

    QGridLayout *additionalPolarLyout = new QGridLayout;
    additionalPolarLyout->addLayout(polarControlLayout, 0, 0);
    additionalPolarLyout->addLayout(middlePolarLayout, 1, 0);
    additionalPolarLyout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding), 0, 1, 2, 1);

    polarLayout->addLayout(additionalPolarLyout);
    m_polarPlotWidget->setLayout(polarLayout);
    m_polarPlotWidget->setVisible(false);

    m_signalQualityPlot = new SignalQualityPlot(iniSettings, this);
    m_signalQualityPlot->setWindowFlag(Qt::Window);
    connect(this, &RealTimePlot::SignalTunerClicked, m_signalQualityPlot, &SignalQualityPlot::show);
    connect(m_dataProcessingThread, &DataProcessingThread::SignalToCheck, this, &RealTimePlot::SlotDataToCheck);

    m_signalPlotWidget = new QGroupBox;

    // Signal plots settings
    m_realData = new QCheckBox(QString(tr("Re")));
    m_realData->setToolTip(QString("Show Real Signal"));
    m_realData->setChecked(m_realSignalShowState);
    connect(m_realData, &QCheckBox::clicked, this, &RealTimePlot::SlotRealDataClicked);

    m_imagData = new QCheckBox(QString(tr("Im")));
    m_imagData->setToolTip(QString("Show Imaginary Signal"));
    m_imagData->setChecked(m_imagSignalShowState);
    connect(m_imagData, &QCheckBox::clicked, this, &RealTimePlot::SlotImagDataClicked);

    m_absData = new QCheckBox(QString(tr("Abs")));
    m_absData->setToolTip(QString("Show Absolute Signal"));
    m_absData->setChecked(m_absSignalShowState);
    connect(m_absData, &QCheckBox::clicked, this, &RealTimePlot::SlotAbsDataClicked);

    m_logScale = new QCheckBox(QString(tr("Log")));
    m_logScale->setToolTip(QString("Switch Signal to Logarithmic Scale"));
    m_logScale->setChecked(m_logScaleState);
    connect(m_logScale, &QCheckBox::clicked, this, &RealTimePlot::SlotSignalLogStateChanged);
    m_logScale->setChecked(m_logScaleState); // !!!

    m_syncScale = new QCheckBox(QString(tr("")));
    m_syncScale->setToolTip(QString("Syncronize Limits"));
    m_syncScale->setChecked(true);
    connect(m_syncScale, &QCheckBox::clicked, this, &RealTimePlot::SlotSignalLinSyncStateChanged);
    m_syncScale->setVisible(!m_logScaleState);

    m_resetSignalPlot = new QToolButton;
    m_rescaleSignalPlot = new QToolButton;

    connect(m_rescaleSignalPlot, &QPushButton::clicked, this, &RealTimePlot::SlotResetImageClicked);
    connect(m_resetSignalPlot, &QPushButton::clicked, this, &RealTimePlot::SlotClearImageClicked);


    m_rescaleSignalPlot->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("zoom-fit-best.svg")));
    m_rescaleSignalPlot->setToolTip(QString("Rescale Plot"));
//    connect(m_resetSignalPlot, &QPushButton::clicked, this, &RealTimePlot::);
    m_resetSignalPlot->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
    m_resetSignalPlot->setToolTip(QString("Reset Plot"));
//    connect(m_rescaleSignalPlot, &QPushButton::clicked, this, &RealTimePlot::);

    m_signalUpperLimit = new QDoubleSpinBox;
    m_signalUpperLimit->setToolTip(QString("Upper Limit"));
    if (m_logScaleState) {
        m_signalUpperLimit->setMinimum(-40);
        m_signalUpperLimit->setMaximum(30);
        m_signalUpperLimit->setSingleStep(1);
        m_signalUpperLimit->setDecimals(1);
        m_signalUpperLimit->setValue(0);
    } else {
        m_signalUpperLimit->setMinimum(-1);
        m_signalUpperLimit->setMaximum(1);
        m_signalUpperLimit->setSingleStep(0.01);
        m_signalUpperLimit->setDecimals(3);
        m_signalUpperLimit->setValue(1);
    }
    m_signalUpperLimit->setAlignment(Qt::AlignRight);
    m_signalUpperLimit->setButtonSymbols(QDoubleSpinBox::PlusMinus);

    m_signalLowerLimit = new QDoubleSpinBox;
    m_signalLowerLimit->setToolTip(QString("Lower Limit"));
    if (m_logScaleState) {
        m_signalLowerLimit->setMinimum(-120);
        m_signalLowerLimit->setMaximum(0);
        m_signalLowerLimit->setSingleStep(1);
        m_signalLowerLimit->setDecimals(1);
        m_signalLowerLimit->setValue(-40);
    } else {
        m_signalLowerLimit->setMinimum(-1);
        m_signalLowerLimit->setMaximum(1);
        m_signalLowerLimit->setSingleStep(0.01);
        m_signalLowerLimit->setDecimals(3);
        m_signalLowerLimit->setValue(-1);
    }
    m_signalLowerLimit->setAlignment(Qt::AlignRight);
    m_signalLowerLimit->setButtonSymbols(QDoubleSpinBox::PlusMinus);

    connect(m_signalUpperLimit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &RealTimePlot::SlotSignalUpperLimitChanged);
    connect(m_signalLowerLimit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &RealTimePlot::SlotSignalLowerLimitChanged);

    QFrame *line1 = new QFrame;
    line1->setFrameShape(QFrame::VLine);
    line1->setFrameShadow(QFrame::Sunken);

    QFrame *line2 = new QFrame;
    line2->setFrameShape(QFrame::VLine);
    line2->setFrameShadow(QFrame::Sunken);

    QFrame *line5 = new QFrame;
    line5->setFrameShape(QFrame::VLine);
    line5->setFrameShadow(QFrame::Sunken);

    QHBoxLayout *signalSettingsLayout = new QHBoxLayout;
    signalSettingsLayout->addWidget(m_absData);
    signalSettingsLayout->addWidget(m_realData);
    signalSettingsLayout->addWidget(m_imagData);
    signalSettingsLayout->addWidget(line5);
    signalSettingsLayout->addWidget(m_logScale);
    signalSettingsLayout->addWidget(line1);
    signalSettingsLayout->addWidget(new QLabel(QString("Lim:")));
    signalSettingsLayout->addWidget(m_signalLowerLimit);
    signalSettingsLayout->addWidget(m_signalUpperLimit);
    signalSettingsLayout->addWidget(m_syncScale);
    signalSettingsLayout->addWidget(line2);
    signalSettingsLayout->addWidget(m_rescaleSignalPlot);
    signalSettingsLayout->addWidget(m_resetSignalPlot);
    signalSettingsLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
    signalPlotLayout->addLayout(signalSettingsLayout);
    signalPlotLayout->addWidget(m_signalQualityPlot->m_plotWidget);
    m_signalPlotWidget->setLayout(signalPlotLayout);
    m_signalPlotWidget->setVisible(false);

    m_plotsLayout->addWidget(m_signalPlotWidget);
    m_plotsLayout->addWidget(m_polarPlotWidget);
    m_signalQualityPlot->m_plotWidget->setVisible(false);

    m_externalControlWidget = new QGridLayout;

    // Create a plot type control widget
    m_plotType = new QComboBox;
    m_plotType->setToolTip(QString("Plot Type Switch"));
    QStringList *plotTypeList = new QStringList;
    plotTypeList->append(QString(tr("No Plots")));
    plotTypeList->append(QString(tr("Signal")));
    plotTypeList->append(QString(tr("All Plots")));
    plotTypeList->append(QString(tr("Circular")));
    m_plotType->addItems(*plotTypeList);
    m_plotType->setCurrentIndex(iniSettings["DefaultPlotType"].toUInt()); // Start with full
    // Wierd connect but &QComboBox::currentIndexChanged doesn't wort...
    connect(m_plotType, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &RealTimePlot::SlotPlotTypeChanged);

    // Rescale button
//    m_resetImage = new QToolButton;
//    m_resetImage->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("view-refresh.svg")));
//    connect(m_resetImage, &QPushButton::clicked, this, &RealTimePlot::SlotResetImageClicked);

//    m_clearImage = new QToolButton;
//    m_clearImage->setIcon(QIcon(m_iniSettings.value(QString("icons")) + g_pathDevider + QString("edit-delete.svg")));
//    connect(m_clearImage, &QPushButton::clicked, this, &RealTimePlot::SlotClearImageClicked);

    mainLayout->addLayout(m_plotsLayout);
//    mainLayout->addLayout(bottomLayout);
    setLayout(mainLayout);

    m_signalQualityCheckBox = new QPushButton(QString("Tuning Plot"));
    m_signalQualityCheckBox->setToolTip(QString("Show Tuning Plot"));
    m_signalQualityCheckBox->setCheckable(true);
    m_signalQualityCheckBox->setChecked(true);
    connect(m_signalQualityCheckBox, &QPushButton::clicked, this, &RealTimePlot::SlotSignalQualytyCheckState);
    m_signalQualityCheckBox->setChecked(false);

    m_externalControlWidget->addWidget(m_plotType, 0, 0);
    m_externalControlWidget->addWidget(m_signalQualityCheckBox, 0, 1);
    m_externalControlWidget->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 2, 1, 1);

//    m_externalControlWidget = new QGroupBox(QString("Graphics"));
//    m_externalControlWidget = new QWidget;
//    m_externalControlWidget->setLayout(bottomLayout);

    m_calibrationMode = static_cast<bool>(iniSettings["CalibrationMode"].toUInt());
    if (m_calibrationMode) {
        m_calibrationPlot = new CalibrationPlot(this, iniSettings);
        m_calibrationPlot->setWindowFlag(Qt::Window);
        connect(this, &RealTimePlot::SignalSignalToDiagram, m_calibrationPlot, &CalibrationPlot::SignalSignalToDiagram);
        connect(this, &RealTimePlot::SignalResize, m_calibrationPlot, &CalibrationPlot::SlotResetImageClicked);
        connect(this, &RealTimePlot::SignalCalibrationClicked, m_calibrationPlot, &QWidget::show);
        connect(this, &RealTimePlot::SignalSamplingFrequencyChanged, m_calibrationPlot, &CalibrationPlot::SignalSamplingFrequencyChanged);
        connect(m_calibrationPlot, &CalibrationPlot::SignalStatusBarMessage, this, &RealTimePlot::SignalStatusBarMessage);
        connect(this, &RealTimePlot::SignalAdcChannelALengthChanged, m_calibrationPlot, &CalibrationPlot::SignalAdcChannelALengthChanged);
    }

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    // Create color map object for polar plot diagram to make a signal level visible as intency
    m_colorMap = new QCPColorMap(m_polarPlot->xAxis, m_polarPlot->yAxis);

    // Construct object for processing and making circle diagram in another thread
    // m_colorMap pointer goes direct to this thread and changing from directly without sending a plenty of data through connect
    int calibrationMode = iniSettings["CalibrationMode"].toInt();
    iniSettings["CalibrationMode"] = QString::number(0);
    m_diagramProcessingThread = new DiagramProcessingThread(m_colorMap, iniSettings);
    iniSettings["CalibrationMode"] = QString::number(calibrationMode);

    // Connect signals to corresponding slots
    connect(this, &RealTimePlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
    connect(m_diagramProcessingThread, &DiagramProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDiagramToDraw);
    connect(this, &RealTimePlot::SignalSamplingFrequencyChanged, m_dataProcessingThread, &DataProcessingThread::SlotSamplingFrequencyChanged);
    connect(this, &RealTimePlot::SignalSamplingFrequencyChanged, m_diagramProcessingThread, &DiagramProcessingThread::SlotSamplingFrequencyChanged);
    connect(m_resetPolarPlot, &QPushButton::clicked, m_diagramProcessingThread, &DiagramProcessingThread::SlotClearImage);

    m_plotTimer = new QTimer;
    connect(m_plotTimer, &QTimer::timeout, this, &RealTimePlot::SlotRescalePlots);

    m_signalPlot->graph(0)->setVisible(m_realData->checkState());
    m_signalPlot->graph(1)->setVisible(m_imagData->checkState());
    m_signalPlot->graph(2)->setVisible(m_absData->checkState());

    // Show diagram layout
    SlotPlotTypeChanged(0);
    SlotSetColorGradient(m_colorMode);

    connect(this, &RealTimePlot::SignalAdcChannelALengthChanged, m_dataProcessingThread, &DataProcessingThread::SlotAdcChannelALengthChanged);
    connect(this, &RealTimePlot::SignalAdcChannelALengthChanged, m_diagramProcessingThread, &DiagramProcessingThread::SlotAdcChannelALengthChanged);
    connect(this, &RealTimePlot::SignalAdcChannelALengthChanged, this, &RealTimePlot::SlotTimeReplot);

    connect(this, &RealTimePlot::SignalSetIntensityCoefficient, m_diagramProcessingThread, &DiagramProcessingThread::SLotSetIntensityCoefficient);
    connect(this, &RealTimePlot::SignalSetBrightnessCoefficient, m_diagramProcessingThread, &DiagramProcessingThread::SLotSetBrightnessCoefficient);
    connect(this, &RealTimePlot::SignalSetThreshold, m_diagramProcessingThread, &DiagramProcessingThread::SLotSetThreshold);

    m_polarPlot->addGraph();
    m_polarPlot->graph(0)->setPen(QPen(Qt::red));
    m_polarPlot->addGraph();
    m_polarPlot->graph(1)->setPen(QPen(Qt::red));
    m_polarPlot->addGraph();
    m_polarPlot->graph(2)->setPen(QPen(Qt::red));

    connect(m_polarPlot, &QCustomPlot::mouseDoubleClick, this, &RealTimePlot::SlotLeftMouseButtonDoubleClicked);
    connect(m_polarPlot, &QCustomPlot::mousePress, this, &RealTimePlot::SlotMouseButtonPressed);
    connect(m_polarPlot, &QCustomPlot::mouseRelease, this, &RealTimePlot::SlotMouseButtonReleased);
    connect(m_polarPlot, &QCustomPlot::mouseMove, this, &RealTimePlot::SlotGetCoursorCoordinates);

    connect(this, &RealTimePlot::SignalSignalLogStateChanged, m_dataProcessingThread, &DataProcessingThread::SlotSignalLogStateChanged);

    setFullPalette(QPalette());

    if (static_cast<bool>(iniSettings["TuningPlotEnanbled"].toInt())) {
        m_signalQualityCheckBox->click();
    }

    m_plotTimer->start(1000);

    m_defaultPlotDelayTimer = new QTimer;
    m_defaultPlotDelayTimer->setInterval(1500);
    connect(m_defaultPlotDelayTimer, &QTimer::timeout, this, &RealTimePlot::SlotSetDefaultPlot);
}

RealTimePlot::~RealTimePlot()
{
    if (m_calibrationMode) {
        m_calibrationPlot->~CalibrationPlot();
    }
    m_signalQualityPlot->~SignalQualityPlot();
}

void RealTimePlot::setFullPalette(const QPalette &palette)
{
    setPalette(palette);

    m_signalPlot->setPalette(palette);
    m_signalPlot->setBackground(palette.window());

    m_signalPlot->xAxis->setBasePen(QPen(palette.text(), 1));
    m_signalPlot->xAxis->setTickPen(QPen(palette.text(), 1));
    m_signalPlot->xAxis->setSubTickPen(QPen(palette.text(), 1));
    m_signalPlot->xAxis->setTickLabelColor(palette.text().color());
    m_signalPlot->xAxis->setLabelColor(palette.text().color());

    m_signalPlot->yAxis->setBasePen(QPen(palette.text(), 1));
    m_signalPlot->yAxis->setTickPen(QPen(palette.text(), 1));
    m_signalPlot->yAxis->setSubTickPen(QPen(palette.text(), 1));
    m_signalPlot->yAxis->setTickLabelColor(palette.text().color());
    m_signalPlot->yAxis->setLabelColor(palette.text().color());

    m_signalPlot->xAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalPlot->yAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalPlot->xAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalPlot->yAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_signalPlot->xAxis->grid()->setSubGridVisible(true);
    m_signalPlot->yAxis->grid()->setSubGridVisible(true);
    m_signalPlot->xAxis->grid()->setZeroLinePen(QPen(Qt::red));
    m_signalPlot->yAxis->grid()->setZeroLinePen(QPen(Qt::red));

    m_signalPlot->xAxis2->setBasePen(QPen(palette.text(), 1));
    m_signalPlot->xAxis2->setTickPen(QPen(palette.text(), 1));
    m_signalPlot->xAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_signalPlot->xAxis2->setTickLabelColor(palette.text().color());
    m_signalPlot->xAxis2->setLabelColor(palette.text().color());

    m_signalPlot->yAxis2->setBasePen(QPen(palette.text(), 1));
    m_signalPlot->yAxis2->setTickPen(QPen(palette.text(), 1));
    m_signalPlot->yAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_signalPlot->yAxis2->setTickLabelColor(palette.text().color());
    m_signalPlot->yAxis2->setLabelColor(palette.text().color());

    m_signalPlot->graph(2)->setPen(QPen(palette.text(), 1));

    m_signalPlot->replot();

    m_polarPlot->setPalette(palette);
    m_polarPlot->setBackground(palette.window());

    m_polarPlot->xAxis->setBasePen(QPen(palette.text(), 1));
    m_polarPlot->xAxis->setTickPen(QPen(palette.text(), 1));
    m_polarPlot->xAxis->setSubTickPen(QPen(palette.text(), 1));
    m_polarPlot->xAxis->setTickLabelColor(palette.text().color());
    m_polarPlot->xAxis->setLabelColor(palette.text().color());

    m_polarPlot->yAxis->setBasePen(QPen(palette.text(), 1));
    m_polarPlot->yAxis->setTickPen(QPen(palette.text(), 1));
    m_polarPlot->yAxis->setSubTickPen(QPen(palette.text(), 1));
    m_polarPlot->yAxis->setTickLabelColor(palette.text().color());
    m_polarPlot->yAxis->setLabelColor(palette.text().color());

    m_polarPlot->xAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_polarPlot->yAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_polarPlot->xAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_polarPlot->yAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_polarPlot->xAxis->grid()->setSubGridVisible(true);
    m_polarPlot->yAxis->grid()->setSubGridVisible(true);
    m_polarPlot->xAxis->grid()->setZeroLinePen(QPen(Qt::red));
    m_polarPlot->yAxis->grid()->setZeroLinePen(QPen(Qt::red));

    m_polarPlot->xAxis2->setBasePen(QPen(palette.text(), 1));
    m_polarPlot->xAxis2->setTickPen(QPen(palette.text(), 1));
    m_polarPlot->xAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_polarPlot->xAxis2->setTickLabelColor(palette.text().color());
    m_polarPlot->xAxis2->setLabelColor(palette.text().color());

    m_polarPlot->yAxis2->setBasePen(QPen(palette.text(), 1));
    m_polarPlot->yAxis2->setTickPen(QPen(palette.text(), 1));
    m_polarPlot->yAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_polarPlot->yAxis2->setTickLabelColor(palette.text().color());
    m_polarPlot->yAxis2->setLabelColor(palette.text().color());

    m_polarPlot->replot();

    if (m_calibrationMode) {
        m_calibrationPlot->SlotSetFullPalette(palette);
    }

    m_signalQualityPlot->setFullPalette(palette);
}

void RealTimePlot::setIniSettings(QMap<QString, QString> &iniSettings, const GraphicModuleSetSettings graphicModuleSetSettings)
{
    m_iniSettings = iniSettings;
    IniReader::checkConfiguration(m_iniSettings, SettingsGroup::SignalPlotSettings);

    m_logScaleState = static_cast<bool>(m_iniSettings.value("SignalPlotLogScale").toUInt());

    m_signalLogLowerLimit = m_iniSettings.value("SignalPlotLimitLowerLog").toDouble();
    m_signalLogUpperLimit = m_iniSettings.value("SignalPlotLimitUpperLog").toDouble();
    m_signalLinLowerLimit = m_iniSettings.value("SignalPlotLimitLowerLin").toDouble();
    m_signalLinUpperLimit = m_iniSettings.value("SignalPlotLimitUpperLin").toDouble();

    m_absSignalShowState = static_cast<bool>(m_iniSettings.value("SignalPlotAbs").toUInt());
    m_realSignalShowState = static_cast<bool>(m_iniSettings.value("SignalPlotRePlot").toUInt());
    m_imagSignalShowState = static_cast<bool>(m_iniSettings.value("SignalPlotImPlot").toUInt());

    m_brightness = m_iniSettings.value("Brightness").toUInt();
    m_contrast = m_iniSettings.value("Contrast").toUInt();
    m_threshold = m_iniSettings.value("Threshold").toUInt();
    m_colorMode = m_iniSettings.value("ColorMode").toUInt();

    if (graphicModuleSetSettings == GraphicModuleSetSettings::SetupMode) {

        if (m_logScale->isChecked() != m_logScaleState)
        {
            m_logScale->click();
        }

        if (m_logScale) {
            m_signalUpperLimit->setValue(m_signalLogUpperLimit);
            m_signalLowerLimit->setValue(m_signalLogLowerLimit);
        } else {
            m_signalUpperLimit->setValue(m_signalLinUpperLimit);
            m_signalLowerLimit->setValue(m_signalLinLowerLimit);
        }

        if (m_absData->isChecked() != m_absSignalShowState)
        {
            m_absData->click();
        }
        if (m_realData->isChecked() != m_realSignalShowState)
        {
            m_realData->click();
        }
        if (m_imagData->isChecked() != m_imagSignalShowState)
        {
            m_imagData->click();
        }

        m_polarBrightness->setValue(m_brightness);
        m_polarIntensity->setValue(m_contrast);
        m_polarThreshold->setValue(m_threshold);
        m_colorGradient->setCurrentIndex(m_colorMode);

        if (m_calibrationMode) {
            m_calibrationPlot->setIniSettings(iniSettings);
        }

        m_signalQualityPlot->setIniSettings(iniSettings);
    }
}

QMap<QString, QString> RealTimePlot::getIniSettings(const GraphicsModuleGetSettings getSettings)
{
    QMap<QString, QString> iniSettings;    

    switch (getSettings) {
    case GraphicsModuleGetSettings::GetSignalPlotSettings : {
        iniSettings.insert(QString("SignalPlotLimitLowerLog"), QString::number(m_signalLogLowerLimit));
        iniSettings.insert(QString("SignalPlotLimitUpperLog"), QString::number(m_signalLogUpperLimit));
        iniSettings.insert(QString("SignalPlotLimitLowerLin"), QString::number(m_signalLinLowerLimit));
        iniSettings.insert(QString("SignalPlotLimitUpperLin"), QString::number(m_signalLinUpperLimit));

        iniSettings.insert(QString("SignalPlotAbs"), QString::number(m_absSignalShowState));
        iniSettings.insert(QString("SignalPlotRePlot"), QString::number(m_realSignalShowState));
        iniSettings.insert(QString("SignalPlotImPlot"), QString::number(m_imagSignalShowState));

        m_iniSettings.insert(iniSettings);
        break;
    }
    case GraphicsModuleGetSettings::GetDiagramSettings : {
        iniSettings.insert(QString("Brightness"), QString::number(m_brightness));
        iniSettings.insert(QString("Contrast"), QString::number(m_contrast));
        iniSettings.insert(QString("Threshold"), QString::number(m_threshold));
        iniSettings.insert(QString("ColorMode"), QString::number(m_colorMode));

        m_iniSettings.insert(iniSettings);
        break;
    }
    case GraphicsModuleGetSettings::GetCalibrationPlotSettings : {
        iniSettings = m_calibrationPlot->getIniSettings();
        break;
    }
    case GraphicsModuleGetSettings::GetTuningPlotSettings : {
        iniSettings = m_signalQualityPlot->getIniSettings();
        break;
    }
    }

    return iniSettings;
}

void RealTimePlot::SlotSignalQualytyCheckState()
{
    m_signalQualityPlot->m_plotWidget->setVisible(m_signalQualityCheckBox->isChecked());
    if (m_signalQualityCheckBox->isChecked()) {
        m_signalQualityPlot->SlotStartClicked();
//        m_plotsLayout->removeWidget(m_signalQualityPlot->m_plotWidget);
//        m_plotsLayout->addWidget(m_signalQualityPlot->m_plotWidget, 1, 0);
    } else {
        m_signalQualityPlot->SlotStopClicked();
//        m_plotsLayout->removeWidget(m_signalQualityPlot->m_plotWidget);
    }
}

void RealTimePlot::SlotSetDefaultPlot()
{
    SlotPlotTypeChanged(m_iniSettings["DefaultPlotType"].toUInt());
}


void RealTimePlot::SlotPlotTypeChanged(const int &index)
{
    // Optimal way to change diagram layout
    switch (index) {
    case 0: {
        setVisible(false);
        m_polarPlotWidget->setVisible(false);
        m_signalPlotWidget->setVisible(false);
        m_signalQualityPlot->m_plotWidget->setVisible(false);
        m_signalQualityCheckBox->setVisible(false);
        disconnect(this, &RealTimePlot::SignalFullBeam, m_dataProcessingThread, &DataProcessingThread::SlotRadarData);
        disconnect(m_dataProcessingThread, &DataProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDataToDraw);
        disconnect(this, &RealTimePlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
        disconnect(m_diagramProcessingThread, &DiagramProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDiagramToDraw);
        m_diagramProcessingThread->stopWorkingThread();
        m_dataProcessingThread->stopWorkingThread();
        emit SignalResize();
        m_plotPreviousIndex = 0;
        break;
    }
    case 1: {
        setVisible(true);
        m_polarPlotWidget->setVisible(false);
        m_signalPlotWidget->setVisible(false);
        m_signalQualityPlot->m_plotWidget->setVisible(false);
        m_signalQualityCheckBox->setVisible(true);
        if (m_signalQualityCheckBox->isChecked()) {
            m_signalPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        } else {
            m_signalPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        }
//        if (m_plotPreviousIndex) {
//            m_plotsLayout->removeWidget(m_signalPlotWidget);
//            if (m_signalQualityCheckBox->isChecked()) {
//                m_plotsLayout->removeWidget(m_signalQualityPlot->m_plotWidget);
//            }
//            m_plotsLayout->removeWidget(m_polarPlot);
//        }
//        m_plotsLayout->addWidget(m_signalPlotWidget, 0, 0);
//        if (m_signalQualityCheckBox->isChecked()) {
//            m_plotsLayout->addWidget(m_signalQualityPlot->m_plotWidget, 1, 0);
//        }
//        m_plotsLayout->addWidget(m_polarPlot, 0, 1, 2, 1);
        m_signalQualityPlot->m_plotWidget->setVisible(m_signalQualityCheckBox->isChecked());
        m_signalPlotWidget->setVisible(true);
        disconnect(this, &RealTimePlot::SignalFullBeam, m_dataProcessingThread, &DataProcessingThread::SlotRadarData);
        disconnect(m_dataProcessingThread, &DataProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDataToDraw);
        connect(this, &RealTimePlot::SignalFullBeam, m_dataProcessingThread, &DataProcessingThread::SlotRadarData);
        connect(m_dataProcessingThread, &DataProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDataToDraw);
        disconnect(this, &RealTimePlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
        disconnect(m_diagramProcessingThread, &DiagramProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDiagramToDraw);
        m_diagramProcessingThread->stopWorkingThread();
        m_dataProcessingThread->startWorkingThread();
        m_plotPreviousIndex = 1;
        break;
    }
    case 2: {
        setVisible(true);
        m_polarPlotWidget->setVisible(false);
        m_signalPlotWidget->setVisible(false);
        m_signalQualityPlot->m_plotWidget->setVisible(false);
        m_signalQualityCheckBox->setVisible(true);
//        if (m_plotPreviousIndex) {
//            m_plotsLayout->removeWidget(m_signalPlotWidget);
//            if (m_signalQualityCheckBox->isChecked()) {
//                m_plotsLayout->addWidget(m_signalQualityPlot->m_plotWidget, 1, 0);
//            }
//            m_plotsLayout->removeWidget(m_polarPlot);
//        }
//        m_plotsLayout->addWidget(m_signalPlotWidget, 0, 0);
//        if (m_signalQualityCheckBox->isChecked()) {
//            m_plotsLayout->addWidget(m_signalQualityPlot->m_plotWidget, 1, 0);
//        }
//        m_plotsLayout->addWidget(m_polarPlot, 0, 1, 2, 1);
        int size = m_signalPlotWidget->height() > m_signalPlotWidget->width() ? m_signalPlotWidget->width() : m_signalPlotWidget->height();
        m_polarPlot->setMinimumSize(size, size - 25);
        m_signalQualityPlot->m_plotWidget->setVisible(m_signalQualityCheckBox->isChecked());
        m_polarPlotWidget->setVisible(true);
        m_signalPlotWidget->setVisible(true);
        disconnect(this, &RealTimePlot::SignalFullBeam, m_dataProcessingThread, &DataProcessingThread::SlotRadarData);
        disconnect(m_dataProcessingThread, &DataProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDataToDraw);
        connect(this, &RealTimePlot::SignalFullBeam, m_dataProcessingThread, &DataProcessingThread::SlotRadarData);
        connect(m_dataProcessingThread, &DataProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDataToDraw);
        disconnect(this, &RealTimePlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
        disconnect(m_diagramProcessingThread, &DiagramProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDiagramToDraw);
        connect(this, &RealTimePlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
        connect(m_diagramProcessingThread, &DiagramProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDiagramToDraw);
        m_diagramProcessingThread->startWorkingThread();
        m_dataProcessingThread->startWorkingThread();
        m_plotPreviousIndex = 2;
        break;
    }
    case 3: {
        setVisible(true);
        m_signalPlotWidget->setVisible(false);
        m_polarPlotWidget->setVisible(false);
        m_signalQualityPlot->m_plotWidget->setVisible(false);
        m_signalQualityCheckBox->setVisible(false);
        if (m_signalQualityCheckBox->isChecked()) {
            m_signalPlot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        } else {
            m_signalPlot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        }
//        if (m_plotPreviousIndex) {
//            m_plotsLayout->removeWidget(m_signalPlotWidget);
//            if (m_signalQualityCheckBox->isChecked()) {
//                m_plotsLayout->removeWidget(m_signalQualityPlot->m_plotWidget);
//            }
//            m_plotsLayout->removeWidget(m_polarPlot);
//        }
//        m_plotsLayout->addWidget(m_signalPlotWidget, 0, 0);
//        if (m_signalQualityCheckBox->isChecked()) {
//            m_plotsLayout->addWidget(m_signalQualityPlot->m_plotWidget, 1, 0, 1, 2);
//        }
//        m_plotsLayout->addWidget(m_polarPlot, 0, 1);
        m_polarPlot->setMinimumSize(550, 550 - 25);
        m_polarPlotWidget->setVisible(true);
        disconnect(this, &RealTimePlot::SignalFullBeam, m_dataProcessingThread, &DataProcessingThread::SlotRadarData);
        disconnect(m_dataProcessingThread, &DataProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDataToDraw);
        connect(this, &RealTimePlot::SignalFullBeam, m_dataProcessingThread, &DataProcessingThread::SlotRadarData);
        connect(m_dataProcessingThread, &DataProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDataToDraw);
        disconnect(this, &RealTimePlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
        disconnect(m_diagramProcessingThread, &DiagramProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDiagramToDraw);
        connect(this, &RealTimePlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
        connect(m_diagramProcessingThread, &DiagramProcessingThread::SignalDataToDraw, this, &RealTimePlot::SlotDiagramToDraw);
        m_diagramProcessingThread->startWorkingThread();
        m_dataProcessingThread->startWorkingThread();
        m_plotPreviousIndex = 3;
        break;
    }
    default: {
        m_signalPlot->setVisible(true);
        m_polarPlotWidget->setVisible(true);
    }
    }
    emit SignalResize();
}

void RealTimePlot::SlotResetImageClicked()
{
//    m_signalPlot->yAxis->setRange(-45, 0); // Rescale will manage it
//    m_polarPlot->rescaleAxes(); // In case of mindless zoom
//    m_colorMap->rescaleDataRange();
//    m_colorMap->setDataRange(QCPRange(-1, 0));
//    m_colorMap->data()->recalculateDataBounds(); // In case of average intencity changing
//    m_colorMap->data()->dataBounds();
    m_signalPlot->rescaleAxes(); // In case of mindless zoom
//    m_signalPlot->yAxis->setRange(-40, m_signalPlot->yAxis->range().upper >= 0 ? m_signalPlot->yAxis->range().upper : 0);
    m_signalPlot->yAxis->setRange(m_signalLowerLimit->value(), m_signalUpperLimit->value());

    m_signalPlot->replot();
//    m_polarPlot->replot();
}

void RealTimePlot::SlotDataToDraw(const QByteArray &absData, const quint16 &angleValue)
{
    // Send data to circle diagram processing
    // You can send anything you want, but default is abs data
    emit SignalSignalToDiagram(absData, angleValue);
}

void RealTimePlot::SlotDataToCheck(const QByteArray &realValues, const QByteArray &imagValues, const QByteArray &absValues)
{
    emit m_signalQualityPlot->SignalDataReceived(realValues, imagValues, absValues);
}

void RealTimePlot::SlotDiagramToDraw()
{
    m_polarPlot->replot();
}

void RealTimePlot::SlotRealDataClicked()
{

    m_realSignalShowState = m_realData->checkState();
    m_signalPlot->graph(0)->setVisible(m_realData->checkState());
    m_signalPlot->replot();
}

void RealTimePlot::SlotImagDataClicked()
{
    m_imagSignalShowState = m_imagData->checkState();
    m_signalPlot->graph(1)->setVisible(m_imagData->checkState());
    m_signalPlot->replot();
}

void RealTimePlot::SlotAbsDataClicked()
{
    m_absSignalShowState = m_absData->checkState();
    m_signalPlot->graph(2)->setVisible(m_absData->checkState());
    m_signalPlot->replot();
}

void RealTimePlot::SlotClearImageClicked()
{
//    m_diagramProcessingThread->SlotClearImage();
    m_logScaleState = static_cast<bool>(m_iniSettings.value("SignalPlotLogScale").toUInt());
    if (m_logScaleState != m_logScale->isChecked()) {
        m_logScale->click();
    }

    m_absSignalShowState = static_cast<bool>(m_iniSettings.value("SignalPlotAbs").toUInt());
    m_realSignalShowState = static_cast<bool>(m_iniSettings.value("SignalPlotRePlot").toUInt());
    m_imagSignalShowState = static_cast<bool>(m_iniSettings.value("SignalPlotImPlot").toUInt());
    if (m_absSignalShowState != m_absData->isChecked()) {
        m_absData->click();
    }
    if (m_realSignalShowState != m_realData->isChecked()) {
        m_realData->click();
    }
    if (m_imagSignalShowState != m_imagData->isChecked()) {
        m_imagData->click();
    }

    m_signalLogLowerLimit = m_iniSettings.value("SignalPlotLimitLowerLog").toDouble();
    m_signalLogUpperLimit = m_iniSettings.value("SignalPlotLimitUpperLog").toDouble();
    m_signalLinLowerLimit = m_iniSettings.value("SignalPlotLimitLowerLin").toDouble();
    m_signalLinUpperLimit = m_iniSettings.value("SignalPlotLimitUpperLin").toDouble();

    if (m_logScaleState) {
        m_signalLowerLimit->setValue(m_signalLogLowerLimit);
        m_signalUpperLimit->setValue(m_signalLogUpperLimit);
    } else {
        m_signalLowerLimit->setValue(m_signalLinLowerLimit);
        m_signalUpperLimit->setValue(m_signalLinUpperLimit);
    }

    m_dataProcessingThread->SlotClearImage();
}

void RealTimePlot::SlotSamplingFrequencyChanged(const int &samplingFrequency)
{
    m_samplingFrequency = samplingFrequency;
    m_signalPlot->xAxis->setRange(0, (m_beamLength/(2*m_samplingFrequency))*m_lightSpeed); // in meters
    m_signalPlot->yAxis->setRange(-45, 0); // just for begin, it's rescalable
    emit SignalSamplingFrequencyChanged(m_samplingFrequency);
    m_plotTimer->start(100);
}

void RealTimePlot::SlotRescalePlots()
{
    m_plotTimer->stop();
    SlotResetImageClicked();
}

void RealTimePlot::SlotTimeReplot(quint16 usec)
{
    m_plotTimer->start(100);
}

void RealTimePlot::SLotSetIntensityCoefficient(const int &coefficient)
{
    m_brightness = coefficient;
    emit SignalSetIntensityCoefficient(static_cast<double>(coefficient)/100.0);
}

void RealTimePlot::SLotSetBrightnessCoefficient(const int &coefficient)
{
    m_brightness = coefficient;
    emit SignalSetBrightnessCoefficient(-1 + static_cast<double>(coefficient)/100.0);
}

void RealTimePlot::SLotSetThreshold(const int &value)
{
    m_threshold = value;
    emit SignalSetThreshold(-1.0 + static_cast<double>(value)/100.0);
}

void RealTimePlot::SlotSetColorGradient(const int index)
{
    m_colorMode = index;
    switch (index) {
    case 0: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpGrayscale));
        break;
    }
    case 1: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpHot));
        break;
    }
    case 2: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpCold));
        break;
    }
    case 3: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpNight));
        break;
    }
    case 4: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpCandy));
        break;
    }
    case 5: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpGeography));
        break;
    }
    case 6: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpIon));
        break;
    }
    case 7: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpThermal));
        break;
    }
    case 8: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpPolar));
        break;
    }
    case 9: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpSpectrum));
        break;
    }
    case 10: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpJet));
        break;
    }
    case 11: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpHues));
        break;
    }
    default: {
        m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpGrayscale));
    }
    }
}

void RealTimePlot::SlotRescalePolarPlot()
{
    m_polarPlot->rescaleAxes();
}

void RealTimePlot::SlotResetPolarPlotPointer()
{
    m_coordinateXValue = 0.0;
    m_coordinateYValue = 0.0;
    m_coordinateX->setValue(m_coordinateXValue);
    m_coordinateY->setValue(m_coordinateYValue);
    m_polarPlot->graph(1)->setData(QVector<double>(), QVector<double>());
    m_polarPlot->graph(2)->setData(QVector<double>(), QVector<double>());

    m_showRealTimeCoordinates = false;
    if (!m_showRealTimeCoordinates) {
        m_coordinateX->setValue(m_coordinateXValue);
        m_coordinateY->setValue(m_coordinateYValue);
    }
}

void RealTimePlot::SlotResetPolarSettings()
{
    m_brightness = m_iniSettings.value("Brightness").toUInt();
    m_contrast = m_iniSettings.value("Contrast").toUInt();
    m_threshold = m_iniSettings.value("Threshold").toUInt();

    m_polarBrightness->setValue(m_iniSettings.value("Brightness").toUInt());
    m_polarIntensity->setValue(m_iniSettings.value("Contrast").toUInt());
    m_polarThreshold->setValue(m_iniSettings.value("Threshold").toUInt());
}

void RealTimePlot::SlotGetCoursorCoordinates(QMouseEvent *event)
{
    if (m_showRealTimeCoordinates) {
        double x = m_polarPlot->xAxis->pixelToCoord(event->pos().x());
        double y = m_polarPlot->yAxis->pixelToCoord(event->pos().y());

        m_coordinateX->setValue(x);
        m_coordinateY->setValue(y);
        if (m_rightFlag) {
            QVector<double> X;
            QVector<double> Y;

            X.append(x);
            X.append(m_coordinateXValue);
            Y.append(y);
            Y.append(m_coordinateYValue);

            m_polarPlot->graph(0)->setData(X, Y);
            m_polarPlot->replot();

            m_distance->setValue(sqrt(pow(m_coordinateXValue - x, 2) + pow(m_coordinateYValue - y, 2)));
            m_angle->setValue(180 + atan2(m_coordinateXValue - x, m_coordinateYValue - y)*(180/M_PI));
        }
    }
}

void RealTimePlot::SlotMouseButtonPressed(QMouseEvent *event)
{
    if (event->button() == Qt::MiddleButton || (m_showRealTimeCoordinates && event->button() == Qt::RightButton)) {
        m_coordinateXValue = m_polarPlot->xAxis->pixelToCoord(event->pos().x());
        m_coordinateYValue = m_polarPlot->yAxis->pixelToCoord(event->pos().y());

        m_coordinateX->setValue(m_coordinateXValue);
        m_coordinateY->setValue(m_coordinateYValue);

        QVector<double> Hrange;
        QVector<double> Vrange;
        QVector<double> Vline;
        QVector<double> Hline;

        Hrange.append(m_colorMap->data()->keyRange().lower);
        Hrange.append(m_colorMap->data()->keyRange().upper);

        Vrange.append(m_colorMap->data()->valueRange().lower);
        Vrange.append(m_colorMap->data()->valueRange().upper);

        Vline.append(m_coordinateXValue);
        Vline.append(m_coordinateXValue);

        Hline.append(m_coordinateYValue);
        Hline.append(m_coordinateYValue);

        m_polarPlot->graph(1)->setData(Hrange, Hline);
        m_polarPlot->graph(2)->setData(Vline, Vrange);
    } else if (event->button() == Qt::RightButton) {
        m_rightFlag = true;
        m_showRealTimeCoordinates = true;

        SlotGetCoursorCoordinates(event);
    }
}

void RealTimePlot::SlotMouseButtonReleased(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton) {
        m_coordinateX->setValue(m_coordinateXValue);
        m_coordinateY->setValue(m_coordinateYValue);
        m_distance->setValue(0.0);
        m_angle->setValue(0.0);

        if (m_rightFlag) {
            m_rightFlag = false;
            m_showRealTimeCoordinates = false;
        }
    } else if (event->button() == Qt::LeftButton) {

    }
    m_polarPlot->graph(0)->setData(QVector<double>(), QVector<double>());
    m_polarPlot->replot();
}

void RealTimePlot::SlotLeftMouseButtonDoubleClicked(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_showRealTimeCoordinates = !m_showRealTimeCoordinates;
        if (!m_showRealTimeCoordinates) {
            m_coordinateX->setValue(m_coordinateXValue);
            m_coordinateY->setValue(m_coordinateYValue);
        }
    } else if (event->button() == Qt::RightButton) {
        m_coordinateXValue = 0.0;
        m_coordinateYValue = 0.0;
        m_coordinateX->setValue(m_coordinateXValue);
        m_coordinateY->setValue(m_coordinateYValue);
        m_polarPlot->graph(1)->setData(QVector<double>(), QVector<double>());
        m_polarPlot->graph(2)->setData(QVector<double>(), QVector<double>());
    }
}

void RealTimePlot::SlotSignalUpperLimitChanged(const double &val)
{
    if (!m_scaleIsChanging) {
        m_scaleIsChanging = true;
        if (m_logScaleState) {
            m_signalLogUpperLimit = val;
        } else {
            m_signalLinUpperLimit = val;
            if (m_syncScale->isChecked()) {
                m_signalLinLowerLimit = -val;
                m_signalLowerLimit->setValue(m_signalLinLowerLimit);
            }
        }
        m_signalLowerLimit->setMaximum(m_signalLogUpperLimit);
        SlotResetImageClicked();
        m_scaleIsChanging = false;
    }
}

void RealTimePlot::SlotSignalLowerLimitChanged(const double &val)
{
    if (!m_scaleIsChanging) {
        m_scaleIsChanging = true;
        if (m_logScaleState) {
            m_signalLogLowerLimit = val;
        } else {
            m_signalLinLowerLimit = val;
            if (m_syncScale->isChecked()) {
                m_signalLinUpperLimit = -val;
                m_signalUpperLimit->setValue(m_signalLinUpperLimit);
            }
        }
        m_signalUpperLimit->setMinimum(val);
        SlotResetImageClicked();
        m_scaleIsChanging = false;
    }
}

void RealTimePlot::SlotSignalLogStateChanged(const bool &checkState)
{
    m_scaleIsChanging = true;
    m_logScaleState = checkState;

    if (m_logScaleState) {
        m_signalPlot->yAxis->setLabel(QString("Amplitude, dB"));
        m_signalUpperLimit->setMinimum(-40);
        m_signalUpperLimit->setMaximum(30);
        m_signalUpperLimit->setSingleStep(1);
        m_signalUpperLimit->setDecimals(1);
        m_signalLowerLimit->setMinimum(-120);
        m_signalLowerLimit->setMaximum(0);
        m_signalLowerLimit->setSingleStep(1);
        m_signalLowerLimit->setDecimals(1);

        m_signalUpperLimit->setStepType(QAbstractSpinBox::DefaultStepType);
        m_signalLowerLimit->setStepType(QAbstractSpinBox::DefaultStepType);

        m_signalUpperLimit->setValue(m_signalLogUpperLimit);
        m_signalLowerLimit->setValue(m_signalLogLowerLimit);

        emit SignalSetSignalLimits(m_signalLogLowerLimit, m_signalLogUpperLimit);

        m_syncScale->setVisible(false);
    } else {
        m_signalPlot->yAxis->setLabel(QString("Amplitude, V"));
        if (m_syncScale->isChecked()) {
            m_signalUpperLimit->setMinimum(0.001);
        } else {
            m_signalUpperLimit->setMinimum(-1);
        }
        m_signalUpperLimit->setMaximum(1);
        m_signalUpperLimit->setSingleStep(0.01);
        m_signalUpperLimit->setDecimals(3);
        m_signalLowerLimit->setMinimum(-1);
        if (m_syncScale->isChecked()) {
            m_signalLowerLimit->setMaximum(-0.001);
        } else {
            m_signalLowerLimit->setMaximum(1);
        }
        m_signalLowerLimit->setSingleStep(0.01);
        m_signalLowerLimit->setDecimals(3);

        m_signalUpperLimit->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
        m_signalLowerLimit->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        m_signalUpperLimit->setValue(m_signalLinUpperLimit);
        m_signalLowerLimit->setValue(m_signalLinLowerLimit);

        emit SignalSetSignalLimits(m_signalLinLowerLimit, m_signalLinUpperLimit);

        m_syncScale->setVisible(true);
    }

    if (m_logScaleState) {
        m_signalUpperLimit->setValue(m_signalLogUpperLimit);
        m_signalLowerLimit->setValue(m_signalLogLowerLimit);
    } else {
        m_signalUpperLimit->setValue(m_signalLinUpperLimit);
        m_signalLowerLimit->setValue(m_signalLinLowerLimit);
    }

    emit SignalSignalLogStateChanged(m_logScaleState);
    SlotResetImageClicked();
    m_scaleIsChanging = false;
}

void RealTimePlot::SlotSignalLinSyncStateChanged(const bool &checkState)
{
    if (checkState) {
        m_signalLinLowerLimit = -m_signalLinUpperLimit;
        m_signalLowerLimit->setValue(m_signalLinLowerLimit);
        m_signalLowerLimit->setMaximum(-0.001);
        m_signalUpperLimit->setMinimum(0.001);
    } else {
        m_signalLowerLimit->setMaximum(1);
        m_signalUpperLimit->setMinimum(-1);
    }
}

void RealTimePlot::resizeEvent(QResizeEvent *event)
{
//    m_polarPlot->setMinimumSize(0, 0);
    if (m_polarPlot->height() > m_polarPlot->width()) {
        m_polarPlot->resize(m_polarPlot->width(), m_polarPlot->width() - 25);
    } else {
        m_polarPlot->resize(m_polarPlot->height(), m_polarPlot->height() - 25);
    }
//    m_polarPlot->setMinimumSize(m_polarPlot->width(), m_polarPlot->height());
    m_signalQualityPlot->resizeEvent(event);
}

