/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "calibrationplot.h"

CalibrationPlot::CalibrationPlot(QWidget *parent, QMap<QString, QString> iniSettings) : QWidget(parent)
{
    setIniSettings(iniSettings);

    QVBoxLayout *mainLayout = new QVBoxLayout;

    // Construct a circle diagram
    m_calibrationPlot = new QCustomPlot;
    m_calibrationPlot->setMinimumSize(650, 650);
    m_calibrationPlot->xAxis->setLabel(QString("Distance, m"));
    m_calibrationPlot->yAxis->setLabel(QString("Distance, m"));

    // Allow rescaling by dragging/zooming
    m_calibrationPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    m_calibrationPlot->axisRect()->setupFullAxesBox(true);

    QSizePolicy polarPlotSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    polarPlotSizePolicy.setWidthForHeight(true);
    m_calibrationPlot->setSizePolicy(polarPlotSizePolicy);    

    mainLayout->addWidget(m_calibrationPlot);

    QHBoxLayout *middleLayout = new QHBoxLayout;

    QLabel *xLabel = new QLabel(QString("X:"));
    middleLayout->addWidget(xLabel);

    m_coordinateX = new QDoubleSpinBox;
    m_coordinateX->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_coordinateX->setDecimals(3);
    m_coordinateX->setMinimum(-999999);
    m_coordinateX->setMaximum(999999);
    m_coordinateX->setReadOnly(true);
    m_coordinateX->setAlignment(Qt::AlignRight);
    m_coordinateX->setSuffix(QString(" m"));
    middleLayout->addWidget(m_coordinateX);

    QLabel *yLabel = new QLabel(QString("Y:"));
    middleLayout->addWidget(yLabel);

    m_coordinateY = new QDoubleSpinBox;
    m_coordinateY->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_coordinateY->setDecimals(3);
    m_coordinateY->setMinimum(-999999);
    m_coordinateY->setMaximum(999999);
    m_coordinateY->setReadOnly(true);
    m_coordinateY->setAlignment(Qt::AlignRight);
    m_coordinateY->setSuffix(QString(" m"));
    middleLayout->addWidget(m_coordinateY);

    m_rescalePushButton = new QPushButton(QString(tr("Rescale")));
    connect(m_rescalePushButton, &QPushButton::clicked, this, &CalibrationPlot::SlotResetImageClicked);
    middleLayout->addWidget(m_rescalePushButton);

    middleLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    QLabel *avgLabel = new QLabel(QString(tr("Avg coef")));
    middleLayout->addWidget(avgLabel);

    m_avgDiagramCoefficient = new QSpinBox;
    m_avgDiagramCoefficient->setButtonSymbols(QSpinBox::NoButtons);
    m_avgDiagramCoefficient->setMinimum(1);
    m_avgDiagramCoefficient->setMaximum(9999);
    m_avgDiagramCoefficient->setAlignment(Qt::AlignRight);
    middleLayout->addWidget(m_avgDiagramCoefficient);

    m_exactlyCalibrationNumber = new QCheckBox(QString(tr("Exactly")));
    m_exactlyCalibrationNumber->setChecked(m_exactlyCalibrationNumberValue);
    middleLayout->addWidget(m_exactlyCalibrationNumber);

    m_startButton = new QPushButton(QString(tr("Start")));
    connect(m_startButton, &QPushButton::clicked, this, &CalibrationPlot::SlotStartCalibration);
    middleLayout->addWidget(m_startButton);

    mainLayout->addLayout(middleLayout);

    QHBoxLayout *bottomLayout = new QHBoxLayout;

    QLabel *dLabel = new QLabel(QString("D:"));
    bottomLayout->addWidget(dLabel);

    m_distance = new QDoubleSpinBox;
    m_distance->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_distance->setDecimals(3);
    m_distance->setMinimum(-999999);
    m_distance->setMaximum(999999);
    m_distance->setReadOnly(true);
    m_distance->setAlignment(Qt::AlignRight);
    m_distance->setSuffix(QString(" m"));
    bottomLayout->addWidget(m_distance);

    QLabel *aLabel = new QLabel(QString("φ:"));
    bottomLayout->addWidget(aLabel);

    m_angle = new QDoubleSpinBox;
    m_angle->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_angle->setDecimals(3);
    m_angle->setMinimum(0);
    m_angle->setMaximum(360);
    m_angle->setReadOnly(true);
    m_angle->setAlignment(Qt::AlignRight);
    m_angle->setSuffix(QString(" °"));
    bottomLayout->addWidget(m_angle);

    bottomLayout->addWidget(new QLabel(QString("Offset:")));
    m_angleOffset = new QDoubleSpinBox;
    m_angleOffset->setMinimum(-360.0);
    m_angleOffset->setMaximum(360.0);
    m_angleOffset->setSingleStep(0.01);
    m_angleOffset->setDecimals(2);
    m_angleOffset->setButtonSymbols(QDoubleSpinBox::NoButtons);
    m_angleOffset->setValue(m_calibrationAngleOffsetValue);
    bottomLayout->addWidget(m_angleOffset);

    m_setAngleOffset = new QPushButton(QString("Set"));
    connect(m_setAngleOffset, &QPushButton::clicked, this, &CalibrationPlot::SlotSetAngleOffsetClicked);
    bottomLayout->addWidget(m_setAngleOffset);

    m_calibrationProgressBar = new QProgressBar;
    bottomLayout->addWidget(m_calibrationProgressBar);

    m_stopButton = new QPushButton(QString(tr("Stop")));
    bottomLayout->addWidget(m_stopButton);

    mainLayout->addLayout(bottomLayout);

    m_colorMap = new QCPColorMap(m_calibrationPlot->xAxis, m_calibrationPlot->yAxis);

    m_diagramProcessingThread = new DiagramProcessingThread(m_colorMap, m_iniSettings);

    // Connect signals to corresponding slots
    connect(m_diagramProcessingThread, &DiagramProcessingThread::SignalDataToDraw, this, &CalibrationPlot::SlotDiagramToDraw);
    connect(m_stopButton, &QPushButton::clicked, m_diagramProcessingThread, &DiagramProcessingThread::SlotStopCalibration);

    m_avgDiagramCoefficient->setValue(m_avgDiagramCoefficientValue);

    connect(m_diagramProcessingThread, &DiagramProcessingThread::SignalCalibrationStarted, this, &CalibrationPlot::SlotCalibrationStarted);
    connect(m_diagramProcessingThread, &DiagramProcessingThread::SignalCalibrationFinished, this, &CalibrationPlot::SlotCalibrationFinished);
    connect(this, &CalibrationPlot::SignalSamplingFrequencyChanged, m_diagramProcessingThread, &DiagramProcessingThread::SlotSamplingFrequencyChanged);
    connect(m_diagramProcessingThread, &DiagramProcessingThread::SignalCalibrationProgress, m_calibrationProgressBar, &QProgressBar::setValue);
    connect(m_exactlyCalibrationNumber, &QCheckBox::stateChanged, m_diagramProcessingThread, &DiagramProcessingThread::SlotSetCallibrationAvgCoefficient);

    m_plotTimer = new QTimer;
    connect(m_plotTimer, &QTimer::timeout, this, &CalibrationPlot::SlotResetImageClicked);

    m_statusTimer = new QTimer;
    connect(m_statusTimer, &QTimer::timeout, this, &CalibrationPlot::SlotClearStatusMessage);

    m_stopButton->setEnabled(false);
    m_calibrationProgressBar->setEnabled(false);
    m_calibrationProgressBar->setValue(0);

    m_calibrationPlot->addGraph();
    m_calibrationPlot->graph(0)->setPen(QPen(Qt::red));
    m_calibrationPlot->addGraph();
    m_calibrationPlot->graph(1)->setPen(QPen(Qt::red));
    m_calibrationPlot->addGraph();
    m_calibrationPlot->graph(2)->setPen(QPen(Qt::red));

    connect(m_calibrationPlot, &QCustomPlot::mouseDoubleClick, this, &CalibrationPlot::SlotLeftMouseButtonDoubleClicked);
    connect(m_calibrationPlot, &QCustomPlot::mousePress, this, &CalibrationPlot::SlotMouseButtonPressed);
    connect(m_calibrationPlot, &QCustomPlot::mouseRelease, this, &CalibrationPlot::SlotMouseButtonReleased);
    connect(m_calibrationPlot, &QCustomPlot::mouseMove, this, &CalibrationPlot::SlotGetCoursorCoordinates);
    connect(this, &CalibrationPlot::SignalAdcChannelALengthChanged, m_diagramProcessingThread, &DiagramProcessingThread::SlotAdcChannelALengthChanged);

    setLayout(mainLayout);

    setWindowTitle(QString("Radiocalibration Tool"));
}

CalibrationPlot::~CalibrationPlot()
{
    m_diagramProcessingThread->stopWorkingThread();
}

void CalibrationPlot::setIniSettings(QMap<QString, QString> &iniSettings, const GraphicModuleSetSettings graphicModuleSetSettings)
{
    m_iniSettings = iniSettings;
    IniReader::checkConfiguration(m_iniSettings, SettingsGroup::CalibrationPlotSettings);

    m_avgDiagramCoefficientValue = m_iniSettings["CalibrationDefaultAvgCoefficient"].toUInt();
    m_exactlyCalibrationNumberValue = static_cast<bool>(m_iniSettings["ExactNumberOfAvg"].toUInt());
    m_calibrationAngleOffsetValue = m_iniSettings["BoardAngleOffset"].toDouble();

    if (graphicModuleSetSettings == GraphicModuleSetSettings::InicializationMode) {
        m_iniSettings["MaxPlottingFrequency"] = m_iniSettings["CalibrationMaxPlottingFrequency"];
        m_iniSettings["MaxAvgFactor"] = m_iniSettings["CalibrationAvgFactor"];
        m_iniSettings["AngleBitOffset"] = m_iniSettings["CalibrationAngleBitOffset"];
        m_iniSettings["RangeAvgCoef"] = m_iniSettings["CalibrationRangeAvgCoef"];
    } else {
        if (m_exactlyCalibrationNumber->isChecked() != m_exactlyCalibrationNumberValue) {
            m_exactlyCalibrationNumber->click();
        }
        m_avgDiagramCoefficient->setValue(m_avgDiagramCoefficientValue);
        m_angleOffset->setValue(m_calibrationAngleOffsetValue);
    }

}

QMap<QString, QString> CalibrationPlot::getIniSettings()
{
    QMap<QString, QString> iniSettings;

    iniSettings.insert(QString("CalibrationDefaultAvgCoefficient"), QString::number(m_avgDiagramCoefficientValue));
    iniSettings.insert(QString("ExactNumberOfAvg"), QString::number(m_exactlyCalibrationNumberValue));

    m_iniSettings.insert(iniSettings);

    return iniSettings;
}

void CalibrationPlot::SlotDiagramToDraw()
{
    m_calibrationPlot->replot();
}

void CalibrationPlot::SlotResetImageClicked()
{
    m_calibrationPlot->rescaleAxes(); // In case of mindless zoom
    m_colorMap->rescaleDataRange();
    m_colorMap->data()->recalculateDataBounds(); // In case of average intencity changing
    m_calibrationPlot->replot();
}

void CalibrationPlot::resizeEvent(QResizeEvent *event)
{
    if (m_calibrationPlot->height() > this->width()) {
        m_calibrationPlot->resize(m_calibrationPlot->width(), m_calibrationPlot->width() - 25);
    } else {
        m_calibrationPlot->resize(m_calibrationPlot->height(), m_calibrationPlot->height() - 25);
    }
}

void CalibrationPlot::SlotStartCalibration()
{
    m_diagramProcessingThread->SlotStartCalibration(m_avgDiagramCoefficient->value());
}

void CalibrationPlot::SlotCalibrationStarted()
{
    disconnect(m_exactlyCalibrationNumber, &QCheckBox::stateChanged, m_diagramProcessingThread, &DiagramProcessingThread::SlotSetCallibrationAvgCoefficient);
    m_exactlyCalibrationNumber->setEnabled(false);
    m_startButton->setEnabled(false);
    m_avgDiagramCoefficient->setEnabled(false);
    m_stopButton->setEnabled(true);
    m_calibrationProgressBar->setValue(0);
    m_calibrationProgressBar->setEnabled(true);
    connect(this, &CalibrationPlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
    m_plotTimer->start(1000);
    emit SignalStatusBarMessage(QString(tr("Calibration Started!")));
}

void CalibrationPlot::SlotCalibrationFinished()
{
    connect(m_exactlyCalibrationNumber, &QCheckBox::stateChanged, m_diagramProcessingThread, &DiagramProcessingThread::SlotSetCallibrationAvgCoefficient);
    m_exactlyCalibrationNumber->setEnabled(true);
    m_startButton->setEnabled(true);
    m_avgDiagramCoefficient->setEnabled(true);
    m_stopButton->setEnabled(false);
    m_calibrationProgressBar->setEnabled(false);
    disconnect(this, &CalibrationPlot::SignalSignalToDiagram, m_diagramProcessingThread, &DiagramProcessingThread::SlotProcessData);
    m_plotTimer->stop();
    emit SignalStatusBarMessage(QString(tr("Calibration Finished!")));
    QThread::msleep(500);
    SlotResetImageClicked();
    QThread::msleep(500);
    SlotResetImageClicked();
    m_statusTimer->start(10000);
}

void CalibrationPlot::SlotRescalePlot()
{
    SlotResetImageClicked();
    m_plotTimer->start(1000);
}

void CalibrationPlot::SlotClearStatusMessage()
{
    emit SignalStatusBarMessage(QString(tr("")));
    m_statusTimer->stop();
}

void CalibrationPlot::SlotSetFullPalette(const QPalette &palette)
{
    setPalette(palette);

    m_calibrationPlot->setPalette(palette);
    m_calibrationPlot->setBackground(palette.window());

    m_calibrationPlot->xAxis->setBasePen(QPen(palette.text(), 1));
    m_calibrationPlot->xAxis->setTickPen(QPen(palette.text(), 1));
    m_calibrationPlot->xAxis->setSubTickPen(QPen(palette.text(), 1));
    m_calibrationPlot->xAxis->setTickLabelColor(palette.text().color());
    m_calibrationPlot->xAxis->setLabelColor(palette.text().color());

    m_calibrationPlot->yAxis->setBasePen(QPen(palette.text(), 1));
    m_calibrationPlot->yAxis->setTickPen(QPen(palette.text(), 1));
    m_calibrationPlot->yAxis->setSubTickPen(QPen(palette.text(), 1));
    m_calibrationPlot->yAxis->setTickLabelColor(palette.text().color());
    m_calibrationPlot->yAxis->setLabelColor(palette.text().color());

    m_calibrationPlot->xAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_calibrationPlot->yAxis->grid()->setPen(QPen(palette.text(), 1, Qt::DotLine));
    m_calibrationPlot->xAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_calibrationPlot->yAxis->grid()->setSubGridPen(QPen(palette.text(), 1, Qt::DotLine));
    m_calibrationPlot->xAxis->grid()->setSubGridVisible(true);
    m_calibrationPlot->yAxis->grid()->setSubGridVisible(true);
    m_calibrationPlot->xAxis->grid()->setZeroLinePen(QPen(Qt::red));
    m_calibrationPlot->yAxis->grid()->setZeroLinePen(QPen(Qt::red));

    m_calibrationPlot->xAxis2->setBasePen(QPen(palette.text(), 1));
    m_calibrationPlot->xAxis2->setTickPen(QPen(palette.text(), 1));
    m_calibrationPlot->xAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_calibrationPlot->xAxis2->setTickLabelColor(palette.text().color());
    m_calibrationPlot->xAxis2->setLabelColor(palette.text().color());

    m_calibrationPlot->yAxis2->setBasePen(QPen(palette.text(), 1));
    m_calibrationPlot->yAxis2->setTickPen(QPen(palette.text(), 1));
    m_calibrationPlot->yAxis2->setSubTickPen(QPen(palette.text(), 1));
    m_calibrationPlot->yAxis2->setTickLabelColor(palette.text().color());
    m_calibrationPlot->yAxis2->setLabelColor(palette.text().color());

    m_calibrationPlot->replot();
}

void CalibrationPlot::SlotGetCoursorCoordinates(QMouseEvent *event)
{
    if (m_showRealTimeCoordinates) {
        double x = m_calibrationPlot->xAxis->pixelToCoord(event->pos().x());
        double y = m_calibrationPlot->yAxis->pixelToCoord(event->pos().y());

        m_coordinateX->setValue(x);
        m_coordinateY->setValue(y);
        if (m_rightFlag) {
            QVector<double> X;
            QVector<double> Y;

            X.append(x);
            X.append(m_coordinateXValue);
            Y.append(y);
            Y.append(m_coordinateYValue);

            m_calibrationPlot->graph(0)->setData(X, Y);
            m_calibrationPlot->replot();

            m_distance->setValue(sqrt(pow(m_coordinateXValue - x, 2) + pow(m_coordinateYValue - y, 2)));
            m_angle->setValue(180 + atan2(m_coordinateXValue - x, m_coordinateYValue - y)*(180/M_PI));
        }
    }
}

void CalibrationPlot::SlotMouseButtonPressed(QMouseEvent *event)
{
    if (event->button() == Qt::MiddleButton || (m_showRealTimeCoordinates && event->button() == Qt::RightButton)) {
        m_coordinateXValue = m_calibrationPlot->xAxis->pixelToCoord(event->pos().x());
        m_coordinateYValue = m_calibrationPlot->yAxis->pixelToCoord(event->pos().y());

        m_coordinateX->setValue(m_coordinateXValue);
        m_coordinateY->setValue(m_coordinateYValue);

        QVector<double> Hrange;
        QVector<double> Vrange;
        QVector<double> Vline;
        QVector<double> Hline;

        Hrange.append(m_colorMap->data()->keyRange().lower);
        Hrange.append(m_colorMap->data()->keyRange().upper);

        Vrange.append(m_colorMap->data()->valueRange().lower);
        Vrange.append(m_colorMap->data()->valueRange().upper);

        Vline.append(m_coordinateXValue);
        Vline.append(m_coordinateXValue);

        Hline.append(m_coordinateYValue);
        Hline.append(m_coordinateYValue);

        m_calibrationPlot->graph(1)->setData(Hrange, Hline);
        m_calibrationPlot->graph(2)->setData(Vline, Vrange);
    } else if (event->button() == Qt::RightButton) {
        m_rightFlag = true;
        m_showRealTimeCoordinates = true;

        SlotGetCoursorCoordinates(event);
    }
}

void CalibrationPlot::SlotMouseButtonReleased(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton) {
        m_coordinateX->setValue(m_coordinateXValue);
        m_coordinateY->setValue(m_coordinateYValue);
        m_distance->setValue(0.0);
        m_angle->setValue(0.0);

        if (m_rightFlag) {
            m_rightFlag = false;
            m_showRealTimeCoordinates = false;
        }
    } else if (event->button() == Qt::LeftButton) {

    }
    m_calibrationPlot->graph(0)->setData(QVector<double>(), QVector<double>());
    m_calibrationPlot->replot();
}

void CalibrationPlot::SlotLeftMouseButtonDoubleClicked(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_showRealTimeCoordinates = !m_showRealTimeCoordinates;
        if (!m_showRealTimeCoordinates) {
            m_coordinateX->setValue(m_coordinateXValue);
            m_coordinateY->setValue(m_coordinateYValue);
        }
    } else if (event->button() == Qt::RightButton) {
        m_coordinateXValue = 0.0;
        m_coordinateYValue = 0.0;
        m_coordinateX->setValue(m_coordinateXValue);
        m_coordinateY->setValue(m_coordinateYValue);
        m_calibrationPlot->graph(1)->setData(QVector<double>(), QVector<double>());
        m_calibrationPlot->graph(2)->setData(QVector<double>(), QVector<double>());
    }
}

void CalibrationPlot::SlotSetAngleOffsetClicked()
{
    m_calibrationAngleOffsetValue = m_angleOffset->value();
    m_diagramProcessingThread->SlotAngleOffsetChanged(m_calibrationAngleOffsetValue);
}

