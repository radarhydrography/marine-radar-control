/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef REALTIMEPLOT_H
#define REALTIMEPLOT_H

#include <QWidget>
#include <QObject>
#include <QComboBox>
#include <QVector>
#include <QMutex>
#include <QThread>
#include <QDial>

#include "shared-libs/qcustomplot.h"
#include "graphics-module/dataprocessingthread.h"
#include "graphics-module/diagramprocessingthread.h"
#include "graphics-module/calibrationplot.h"
#include "graphics-module/signalqualityplot.h"


class RealTimePlot : public QWidget
{
    Q_OBJECT

    // Plot for current 2D Signal
    QCustomPlot *m_signalPlot;
    // Plot for circle (planar 3D) diagram
    QCustomPlot *m_polarPlot;
    QCPColorMapData *m_colorMapData;
    QCPColorMap *m_colorMap;
    // Widget for choise a plot type
    QComboBox *m_plotType;
    // Show *** signal
    QCheckBox *m_realData;
    QCheckBox *m_imagData;
    QCheckBox *m_absData;
    QCheckBox *m_logScale;
    bool m_logScaleState = false;
    QCheckBox *m_syncScale;
    // Rescale image
    QToolButton *m_resetImage;
    QToolButton *m_clearImage;
    QPushButton *m_calibratePosition;
    bool m_calibrationMode = 0;

    QToolButton *m_resetSignalPlot;
    QToolButton *m_rescaleSignalPlot;
    QDoubleSpinBox *m_signalUpperLimit;
    QDoubleSpinBox *m_signalLowerLimit;
    double m_signalLogUpperLimit = 0.0;
    double m_signalLogLowerLimit = -40.0;
    double m_signalLinUpperLimit = 1.0;
    double m_signalLinLowerLimit = -1.0;
    bool m_scaleIsChanging = false;

    QWidget *m_polarPlotWidget;

    QToolButton *m_resetPolarPlot;
    QToolButton *m_resetPolarPlotPointer;
    QToolButton *m_rescalePolarPlot;
    QToolButton *m_resetPolarSettings;
    QSpinBox *m_polarIntensity;
    QSpinBox *m_polarBrightness;
    QSpinBox *m_polarThreshold;
    QDial *m_polarIntensityDial;
    QDial *m_polarBrightnessDial;
    QDial *m_polarThresholdDial;
    QComboBox *m_colorGradient;

    QDoubleSpinBox *m_coordinateX;
    QDoubleSpinBox *m_coordinateY;
    double m_coordinateXValue = 0.0;
    double m_coordinateYValue = 0.0;
    QDoubleSpinBox *m_distance;
    QDoubleSpinBox *m_angle;
    bool m_showRealTimeCoordinates = false;
    bool m_rightFlag = false;

    QMap<QString, QString> m_iniSettings;

    QPushButton *m_signalQualityCheckBox;

    SignalQualityPlot *m_signalQualityPlot;

    int m_beamLength = 2048;
    double m_samplingFrequency = 80e6;
    double m_lightSpeed = 299792458;

    CalibrationPlot *m_calibrationPlot = nullptr;

    // Object for processing signal data in another thread
    DataProcessingThread *m_dataProcessingThread;
    // Object for processing and making circle diagram in another thread
    DiagramProcessingThread *m_diagramProcessingThread;

    QTimer *m_plotTimer;
    QTimer *m_defaultPlotDelayTimer;

    QWidget *m_signalPlotWidget;
    QHBoxLayout *m_plotsLayout;
    quint8 m_plotPreviousIndex;

    quint16 m_brightness = 100;
    quint16 m_contrast = 100;
    quint16 m_threshold = 0;
    quint8 m_colorMode = 0;

    bool m_absSignalShowState = true;
    bool m_realSignalShowState = false;
    bool m_imagSignalShowState = false;

public:
    RealTimePlot(QMap<QString, QString> iniSettings = QMap<QString, QString>(), QWidget *parent = nullptr);
    ~RealTimePlot();

    void setFullPalette(const QPalette &palette);

    QGridLayout *m_externalControlWidget;

    void setIniSettings(QMap<QString, QString> &iniSettings, const GraphicModuleSetSettings graphicModuleSetSettings = GraphicModuleSetSettings::InicializationMode);
    QMap<QString, QString> getIniSettings(const GraphicsModuleGetSettings getSettings = GraphicsModuleGetSettings::GetSignalPlotSettings);

private slots:
    void SlotSignalQualytyCheckState();
    void SlotSetDefaultPlot();

public slots:
    // Slot for change a plot type. Contains QString only bacause Qt had a promlem with function overload...
    void SlotPlotTypeChanged(const int &index);
    void SlotResetImageClicked();
    // Slot for reseiving processed raw data prepeaed to draw a current signal and be a part of circle diagram
    void SlotDataToDraw(const QByteArray &absData, const quint16 &angleValue);

    void SlotDataToCheck(const QByteArray &realValues, const QByteArray &imagValues, const QByteArray &absValues);
    // Slot for replot diagram. Data in diagram is being changed fromn another thread
    void SlotDiagramToDraw();

    void SlotRealDataClicked();
    void SlotImagDataClicked();
    void SlotAbsDataClicked();
    void SlotClearImageClicked();

    void SlotSamplingFrequencyChanged(const int &samplingFrequency);

    void SlotRescalePlots();

    void SlotTimeReplot(quint16 usec = 1000);

    void SLotSetIntensityCoefficient(const int &coefficient);
    void SLotSetBrightnessCoefficient(const int &coefficient);
    void SLotSetThreshold(const int &value);
    void SlotSetColorGradient(const int index);

    void SlotRescalePolarPlot();
    void SlotResetPolarPlotPointer();
    void SlotResetPolarSettings();

    void SlotGetCoursorCoordinates(QMouseEvent *event);

    void SlotMouseButtonPressed(QMouseEvent *event);
    void SlotMouseButtonReleased(QMouseEvent *event);
    void SlotLeftMouseButtonDoubleClicked(QMouseEvent *event);

    void SlotSignalUpperLimitChanged(const double &val);
    void SlotSignalLowerLimitChanged(const double &val);

    void SlotSignalLogStateChanged(const bool &checkState);
    void SlotSignalLinSyncStateChanged(const bool &checkState);

    void resizeEvent(QResizeEvent *event) override;

signals:
    void SignalResize();
    // Signal for repeat full beam raw data to processing thread
    void SignalFullBeam(const QByteArray &fullBeam);
    // Signal for repeat processed signal (beam) to diagram processing thread
    void SignalSignalToDiagram(const QByteArray &signal, const quint16 &angleValue);
    // Clear repeaters
    void SignalSetSignalAvgCoefficient(const quint16 &signalAvgCoefficient);
    void SignalSetDiagramAvgCoefficient(const quint16 &diagramAvgCoefficient);
    void SignalAngleOffsetChanged(const quint16 &angleOffset);

    void SignalSamplingFrequencyChanged(const int &samplingFrequency);
    void SignalStatusBarMessage(const QString &statusMessage);

    void SignalTunerClicked();
    void SignalCalibrationClicked();

    void SignalAdcChannelALengthChanged(const quint16 &length);

    void SignalSetIntensityCoefficient(const double &coefficient);
    void SignalSetBrightnessCoefficient(const double &coefficient);
    void SignalSetThreshold(const double &value);

    void SignalSignalLogStateChanged(const bool &checkState);
    void SignalSetSignalLimits(const double &lowerLimit, const double &upperLimit);

};

#endif // REALTIMEPLOT_H
