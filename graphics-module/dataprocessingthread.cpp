/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "dataprocessingthread.h"

#include <math.h>
#include "shared-libs/global.h"

DataProcessingThread::DataProcessingThread(QCustomPlot *signalPlot, QMap<QString, QString> iniSettings, QObject *parent) : QThread(parent)
{
    m_mutex = m_mutex = new QMutex;

    m_signalPlot = signalPlot;

    setIniSettings(iniSettings);

    // Construct main buffer vectors
    m_distance = new QVector<double>;
    m_realSignalLin = new QByteArray;
    m_imagSignalLin = new QByteArray;
    m_absSignalLin = new QByteArray;

    m_realSignal = new QByteArray;
    m_imagSignal = new QByteArray;
    m_absSignal = new QByteArray;

    m_realSignal->resize(2048*sizeof(double));
    m_imagSignal->resize(2048*sizeof(double));
    m_absSignal->resize(2048*sizeof(double));
    m_realSignalLin->resize(2048*sizeof(double));
    m_imagSignalLin->resize(2048*sizeof(double));
    m_absSignalLin->resize(2048*sizeof(double));

    // Construct raw data container
    m_dataToProcess = new QList<QByteArray>;

    m_beamLength = m_iniSettings[QString("FrameLength")].toUInt();
    m_samplingFrequency = m_iniSettings[QString("SamplingFrequency")].toDouble()/pow(2, m_iniSettings["BoardDecimation"].toDouble());
    m_lightSpeed = g_lightSpeed;

    // Allocate space for main buffer vectors
    double zeroValue = 0.0;
    double distValue;
    for (int i = 0; i < m_beamLength; i++) {
        distValue = -m_adcChannelALength*m_lightSpeed/(2*m_samplingFrequency) + (i/(2*m_samplingFrequency))*m_lightSpeed;
        m_distance->append(distValue);
        memcpy(m_realSignalLin->data() + i*sizeof (double), &zeroValue, sizeof (double));
        memcpy(m_imagSignalLin->data() + i*sizeof (double), &zeroValue, sizeof (double));
        memcpy(m_absSignalLin->data() + i*sizeof (double), &zeroValue, sizeof (double));
        memcpy(m_realSignal->data() + i*sizeof (double), &zeroValue, sizeof (double));
        memcpy(m_imagSignal->data() + i*sizeof (double), &zeroValue, sizeof (double));
        memcpy(m_absSignal->data() + i*sizeof (double), &zeroValue, sizeof (double));
    }

    // Construct timer for drawing event
    m_plotTimer = new QTimer;
    m_plotTimerDelay = static_cast<quint16>(1000.0*(1.0/m_iniSettings[QString("MaxPlottingFrequency")].toUInt())); // set time between replot

    SlotClearImage();
    // start replot timer
    m_plotTimer->start(m_plotTimerDelay);
    // connect to drawing event slot
    connect(m_plotTimer, &QTimer::timeout, this, &DataProcessingThread::SlotDrawData);

    startWorkingThread(); 
}

DataProcessingThread::~DataProcessingThread()
{
    stopWorkingThread();
}

// Main working cycle of this thread
void DataProcessingThread::run()
{
    if (m_workingThreadEnable) {
        emit poolStatus(true);
    }

    quint16 angleValue; // current angle value
//    quint16 maxCounter; // buffer for multithread access protection

    QByteArray radarData; // buffer for multithread access protection

    while (m_workingThreadEnable) {

        if (!m_dataToProcess->isEmpty()) { // If it is some data to process
            m_mutex->lock();
            radarData.append(m_dataToProcess->at(0)); // Protected reading to buffer
            m_dataToProcess->erase(m_dataToProcess->begin()); // Delete readed data
//            maxCounter = m_signalAvgCoefficient; // Protected reading of averaging coefficient
            m_mutex->unlock();

            angleValue = dataProcessing(radarData); // data procesing
            radarData.clear(); // clear the buffer

            // Averaging process
//            if (m_avgCounter >= maxCounter) { // if counter is over
//                avgDataProcessing(maxCounter); // process avg data
                emit SignalDataToDraw(*m_absSignal, angleValue); // emit signal to draw new signal
                emit SignalToCheck(*m_realSignalLin, *m_imagSignalLin, *m_absSignalLin);
//            } else { // if not
//                m_avgCounter++; // increment the counter
//            }
        }
        usleep(10);
    }

    if (!m_workingThreadEnable) {
        emit poolStatus(false);
    }
}

void DataProcessingThread::setIniSettings(QMap<QString, QString> &iniSettings)
{
    m_iniSettings = iniSettings;

    IniReader::checkConfiguration(m_iniSettings, SettingsGroup::SignalPlotSettings);

    m_logScale = static_cast<bool>(m_iniSettings.value("SignalPlotLogScale").toUInt());

    if (m_logScale) {
        m_lowerSignalLimit = m_iniSettings.value("SignalPlotLimitLowerLog").toDouble();
        m_upperSignalLimit = m_iniSettings.value("SignalPlotLimitUpperLog").toDouble();
    } else {
        m_lowerSignalLimit = m_iniSettings.value("SignalPlotLimitLowerLin").toDouble();
        m_upperSignalLimit = m_iniSettings.value("SignalPlotLimitUpperLin").toDouble();
    }
}

// Method for raw data processing
quint16 DataProcessingThread::dataProcessing(const QByteArray &radarData)
{
    quint16 angleValue{0}; // Output current angle value

    memcpy(&angleValue, radarData.data() + 2, 2);
//    angleValue += m_angleOffset;
    // Convert 16 bit data to Real, Image and Absolute signals
    qint16 realValue;
    qint16 imagValue;
    double real;
    double imag;
    double re;
    double im;
    double ab;
    double absolute;
    quint8 headerSize = 32;
    quint8 bytesPerSample = 4;

    int num;

    for (int byteNum = headerSize; byteNum < radarData.size() - 3; byteNum += bytesPerSample) {
        // Every odd is real
        memcpy(&realValue, radarData.data() + byteNum, 2);
        // Every even is image
        memcpy(&imagValue, radarData.data() + byteNum + 2, 2);

        re = static_cast<double>(realValue)/11900;
        im = static_cast<double>(imagValue)/11900;
        ab = sqrt(pow(re, 2) + pow(im, 2));
        real = 20*log10((abs(re)) + 0.0001);
        imag = 20*log10((abs(im)) + 0.0001);
        absolute = 10*log10(ab/sqrt(2.0) + 0.0001);

        num = ((byteNum - headerSize)/bytesPerSample)*sizeof (double);

        m_mutex->lock();
        memcpy(m_realSignalLin->data() + num, &re, sizeof (double));
        memcpy(m_imagSignalLin->data() + num, &im, sizeof (double));
        memcpy(m_absSignalLin->data() + num, &ab, sizeof (double));

        memcpy(m_realSignal->data() + num, &real, sizeof (double));
        memcpy(m_imagSignal->data() + num, &imag, sizeof (double));
        memcpy(m_absSignal->data() + num, &absolute, sizeof (double));
        m_mutex->unlock();
    }

    return angleValue;
}

//void DataProcessingThread::SlotDrawData()
//{
//    m_mutex->lock();
//    m_signalPlot->graph(0)->setData(*m_distance, *m_realSignal, true);
//    m_signalPlot->graph(1)->setData(*m_distance, *m_imagSignal, true);
//    m_signalPlot->graph(2)->setData(*m_distance, *m_absSignal, true);
//    m_signalPlot->replot();
//    m_mutex->unlock();
//    m_plotTimer->start(m_plotTimerDelay);
//}

void DataProcessingThread::SlotDrawData()
{
    m_mutex->lock();
    for (int i = 0; i < static_cast<int>(m_realSignal->size()/(sizeof (double))); i++) {
        if (m_logScale) {
    //        memcpy(&m_signalPlot->graph(0)->data()->begin()->key + i, m_distanceBA->data() + i*sizeof (double), sizeof (double));
            memcpy(&((m_signalPlot->graph(0)->data()->begin() + i)->value), m_realSignal->data() + i*sizeof (double), sizeof (double));
    //        memcpy(&m_signalPlot->graph(1)->data()->begin()->key  + i, m_distanceBA->data() + i*sizeof (double), sizeof (double));
            memcpy(&((m_signalPlot->graph(1)->data()->begin() + i)->value), m_imagSignal->data() + i*sizeof (double), sizeof (double));
    //        memcpy(&m_signalPlot->graph(2)->data()->begin()->key + i, m_distanceBA->data() + i*sizeof (double), sizeof (double));
            memcpy(&((m_signalPlot->graph(2)->data()->begin() + i)->value), m_absSignal->data() + i*sizeof (double), sizeof (double));
        } else {
    //        memcpy(&m_signalPlot->graph(0)->data()->begin()->key + i, m_distanceBA->data() + i*sizeof (double), sizeof (double));
            memcpy(&((m_signalPlot->graph(0)->data()->begin() + i)->value), m_realSignalLin->data() + i*sizeof (double), sizeof (double));
    //        memcpy(&m_signalPlot->graph(1)->data()->begin()->key  + i, m_distanceBA->data() + i*sizeof (double), sizeof (double));
            memcpy(&((m_signalPlot->graph(1)->data()->begin() + i)->value), m_imagSignalLin->data() + i*sizeof (double), sizeof (double));
    //        memcpy(&m_signalPlot->graph(2)->data()->begin()->key + i, m_distanceBA->data() + i*sizeof (double), sizeof (double));
            memcpy(&((m_signalPlot->graph(2)->data()->begin() + i)->value), m_absSignalLin->data() + i*sizeof (double), sizeof (double));
        }
    }
    m_signalPlot->replot();
    m_mutex->unlock();
    m_plotTimer->start(m_plotTimerDelay);
}

void DataProcessingThread::startWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = true;
    m_mutex->unlock();

    if (!isRunning()) {
        start();
    }
}

void DataProcessingThread::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

// Slot for recieving raw signal data
void DataProcessingThread::SlotRadarData(const QByteArray &radarData)
{
    m_mutex->lock();
    m_dataToProcess->append(radarData);
    m_mutex->unlock();
}

// Slot for setup angle offset
void DataProcessingThread::SlotSetAngleOffset(const quint16 &angleOffset)
{
    m_mutex->lock();
    m_angleOffset = angleOffset;
    m_mutex->unlock();
}

void DataProcessingThread::SlotSamplingFrequencyChanged(const int &samplingFrequency)
{
    m_samplingFrequency = samplingFrequency;
    m_mutex->lock();
    m_distance->clear();
    for (int i = 0; i < m_beamLength; i++) {
        double distValue = -m_adcChannelALength*m_lightSpeed/(2*m_samplingFrequency) + (i/(2*m_samplingFrequency))*m_lightSpeed;
        m_distance->append(distValue);
    }
    m_mutex->unlock();
    m_signalPlot->yAxis->setRange(-45, 0); // Rescale will manage it
    m_signalPlot->rescaleAxes(); // In case of mindless zoom
    m_signalPlot->yAxis->setRange(-40, m_signalPlot->yAxis->range().upper >= 0 ? m_signalPlot->yAxis->range().upper : 0);

    SlotClearImage();

    m_signalPlot->replot();
}

void DataProcessingThread::SlotClearImage()
{
    m_mutex->lock();
    m_signalPlot->graph(0)->setData(*m_distance, QVector<double>(m_distance->size(), 0));
    m_signalPlot->graph(1)->setData(*m_distance, QVector<double>(m_distance->size(), 0));
    m_signalPlot->graph(2)->setData(*m_distance, QVector<double>(m_distance->size(), 0));
    m_signalPlot->replot();
    m_mutex->unlock();
}

void DataProcessingThread::SlotAdcChannelALengthChanged(const quint16 &length)
{
    m_adcChannelALength = length;
    SlotSamplingFrequencyChanged(m_samplingFrequency);
}

void DataProcessingThread::SlotSignalLogStateChanged(const bool &checkState)
{
    m_mutex->lock();
    m_logScale = checkState;
    m_mutex->unlock();
}

void DataProcessingThread::SlotSetSignalLimits(const double &lowerLimit, const double &upperLimit)
{
    m_lowerSignalLimit = lowerLimit;
    m_upperSignalLimit = upperLimit;
}
