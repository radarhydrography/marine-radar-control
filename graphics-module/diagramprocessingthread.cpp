/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "diagramprocessingthread.h"
#include "shared-libs/inireader.h"
#include "shared-libs/global.h"

DiagramProcessingThread::DiagramProcessingThread(QCPColorMap *colorMap, QMap<QString, QString> iniSettings, QObject *parent) : QThread(parent)
{
    m_mutex = new QMutex;

    m_calibrationMode = static_cast<bool>(iniSettings["CalibrationMode"].toUInt());

    setIniSettings(iniSettings);

    // Parent color map
    m_colorMap = colorMap;

    m_signalBuffer = new QList<QPair<quint16, QByteArray>>; // construct signal buffer

    //Axis X and Y is space around radar and can change from -MaxRange to MaxRange, it's a coordinates

    m_colorMap->data()->setSize(2*(m_diagramRangeSize)/m_rangeAvgCoef, 2*(m_diagramRangeSize )/m_rangeAvgCoef);
    m_colorMap->data()->setRange(QCPRange(-((m_diagramRangeSize)/(2*m_samplingFrequency))*m_lightSpeed, ((m_diagramRangeSize)/(2*m_samplingFrequency))*m_lightSpeed),
                                 QCPRange(-((m_diagramRangeSize)/(2*m_samplingFrequency))*m_lightSpeed, ((m_diagramRangeSize)/(2*m_samplingFrequency))*m_lightSpeed));

    m_colorMap->setGradient(QCPColorGradient(QCPColorGradient::gpGrayscale));

    /*
     *  Next part is just for optimizing computing time
     *  This is precalculated coordinates of opening of the beams
     *  Beam form in space is cone and the values with the same range have to be equal
     *  It is because delta Phi make a beam spot wider when distance increasing
     *  In the near range it is not a problem, but in far range you need to take this into account
     *  But calculating of actual coordinate in the process of thransforming rectangular coordinates to polar is not so light
     *  It containts heavy math operations and freeze a program when you try to calculate it in real time
     *  And respectively it is better to precalculate it
     */

    // Coordinate containers constructing
    m_beamIndexesX = new QList<QList<QList<int>>>;
    m_beamIndexesY = new QList<QList<QList<int>>>;

    quint16 maxDiagramAngleSize = 0b0111111111111111; // max value of the angle sensor (15 bit)
    quint16 uncommpressedAngleValue = 0; // uncompressedValue of compressed in accordance with drawing image compression rate
    quint16 x = 0; // current X
    quint16 y = 0; // current Y
    for (int angleIndex = 0; angleIndex < m_diagramAngleSize; angleIndex++) { // for all compressed diagram direction values
        uncommpressedAngleValue = angleIndex << m_angleBitOffset; // uncompress angle compressed value to fill up all possible directions
        // Without this buffers it doesn't work
        QList<QList<int>> xRangeIndexes;
        QList<QList<int>> yRangeIndexes;
        for (int rangeIndex = 0; rangeIndex < m_diagramRangeSize/m_rangeAvgCoef; rangeIndex++) { // for all compressed diagram range values
            // Without this buffers too
            QList<int> xIndexes;
            QList<int> yIndexes;
            for (quint16 angleDrawIndex = 0; angleDrawIndex < m_angleCut; angleDrawIndex++) { // for all noncompressed diagram direction values (compressed values + filling directions)
                // Calculation of current coordinate transformation
                x = m_diagramRangeSize/m_rangeAvgCoef + round(rangeIndex*sin(2*M_PI*(uncommpressedAngleValue + angleDrawIndex)/maxDiagramAngleSize));
                y = m_diagramRangeSize/m_rangeAvgCoef + round(rangeIndex*cos(2*M_PI*(uncommpressedAngleValue + angleDrawIndex)/maxDiagramAngleSize));
                // If current coordinate doesn't exist you have to add it to the container
                // In near range a lot of coordinates are crossing each other
                // In far range opposite
                if (xIndexes.indexOf(x) == -1 || yIndexes.indexOf(y) == -1) {
                    xIndexes.append(x);
                    yIndexes.append(y);
                }
            }
            // Put values to container
            xRangeIndexes.append(xIndexes);
            yRangeIndexes.append(yIndexes);
        }
        // Put container to container
        m_beamIndexesX->append(xRangeIndexes);
        m_beamIndexesY->append(yRangeIndexes);
    }

    m_calibrationDiagram = new QByteArray;
    m_calibrationDiagram->resize(m_diagramAngleSize*(m_diagramRangeSize - m_adcChannelALength)*sizeof(double));

    m_calibrationgAvgCoefficientList = new QByteArray;
    m_calibrationgAvgCoefficientList->resize(m_diagramAngleSize*sizeof (quint16));

    // Now we have precalculated coordinates to saving calculation time!!!

    // Fill Color Map by Zeros
    for (int xIndex = 0; xIndex < m_colorMap->data()->keySize(); xIndex++) {
        for (int yIndex = 0; yIndex < m_colorMap->data()->valueSize(); yIndex++) {
            m_colorMap->data()->setCell(xIndex, yIndex, -1);
        }
    }

//    m_colorMap->rescaleDataRange();
    m_colorMap->setDataRange(QCPRange(-1, 0));
    m_colorMap->rescaleAxes();

    // Construct timer for drawing event
    m_plotTimer = new QTimer;
    m_plotTimerDelay = static_cast<quint16>(1000.0*(1.0/m_iniSettings[QString("MaxPlottingFrequency")].toUInt())); // set time between replot

    // start replot timer
    m_plotTimer->start(m_plotTimerDelay);
    // connect to drawing event slot
    connect(m_plotTimer, &QTimer::timeout, this, &DiagramProcessingThread::SignalDataToDraw);

    if (m_calibrationMode) {
        m_calibrationAngleOffset = 0;
    } else {
        startWorkingThread();
    }
}


DiagramProcessingThread::~DiagramProcessingThread()
{
    stopWorkingThread();
}

// Main working cycle of this thread
void DiagramProcessingThread::run()
{
    if (m_workingThreadEnable) {
        emit poolStatus(true);
    }

    QPair<quint16, QByteArray> signal; // buffer for multithread access protection

    if (m_calibrationMode) {

        m_mutex->lock();
        m_signalBuffer->clear(); // Delete readed data
        m_mutex->unlock();

        while (m_workingThreadEnable) {

            if (!m_signalBuffer->isEmpty()) { // If it is some data to process
                m_mutex->lock();
                signal = m_signalBuffer->at(0); // Protected reading to buffer
                m_signalBuffer->erase(m_signalBuffer->begin()); // Delete readed data
                m_mutex->unlock();

                signalProcessing(signal.first, signal.second); // signals procesing
            }

//            usleep(10);
        }
    } else {
        while (m_workingThreadEnable) {

            if (!m_signalBuffer->isEmpty()) { // If it is some data to process
                m_mutex->lock();
                signal = m_signalBuffer->at(0); // Protected reading to buffer
                m_signalBuffer->erase(m_signalBuffer->begin()); // Delete readed data
                m_mutex->unlock();

                signalProcessing(signal.first, signal.second); // signals procesing
            }

            usleep(10);
        }
    }

    if (!m_workingThreadEnable) {
        emit poolStatus(false);
    }
}

void DiagramProcessingThread::setIniSettings(QMap<QString, QString> &iniSettings)
{
    m_iniSettings = iniSettings;

    if (m_calibrationMode) {
        IniReader::checkConfiguration(m_iniSettings, SettingsGroup::CalibrationPlotSettings);

        // Very important to set all the next variables!!!
        m_angleBitOffset = m_iniSettings[QString("CalibrationAngleBitOffset")].toUInt(); //set size of angle value offset for drawing image compressing default: 7

        for (int i = 0; i < m_angleBitOffset; i++) m_angleMask = ~(0b1 << i) & m_angleMask; // automatic setup of BitMask to cut insignificant rank of angle value
        m_angleCut = static_cast<quint16>(round(pow(2, m_angleBitOffset))); // automatic setup of the size of the insignificant rank of angle value: 2^i
        m_diagramAngleSize = static_cast<quint16>(round(pow(2, 15 - m_angleBitOffset))); // automatic setup of number of angles (drawing directions): 2^(max_angle_sensor_value - i)

        m_rangeAvgCoef = m_iniSettings[QString("CalibrationRangeAvgCoef")].toUInt(); // set nubmer of nearest neighbors for image size compressing default: 4
        m_rangeAvgCoef = m_rangeAvgCoef == 0 ? 1 : m_rangeAvgCoef;

        m_signalBuffer = new QList<QPair<quint16, QByteArray>>; // construct signal buffer

        m_exactNumberOfAvg = static_cast<bool>(m_iniSettings["ExactNumberOfAvg"].toUInt());
        m_staticAngleOffset = m_iniSettings["BoardAngleOffset"].toDouble();

    } else {
        IniReader::checkConfiguration(m_iniSettings, SettingsGroup::DiagramPlotSettings);

        // Very important to set all the next variables!!!
        m_angleBitOffset = m_iniSettings[QString("AngleBitOffset")].toUInt(); //set size of angle value offset for drawing image compressing default: 7

        for (int i = 0; i < m_angleBitOffset; i++) m_angleMask = ~(0b1 << i) & m_angleMask; // automatic setup of BitMask to cut insignificant rank of angle value
        m_angleCut = static_cast<quint16>(round(pow(2, m_angleBitOffset))); // automatic setup of the size of the insignificant rank of angle value: 2^i
        m_diagramAngleSize = static_cast<quint16>(round(pow(2, 15 - m_angleBitOffset))); // automatic setup of number of angles (drawing directions): 2^(max_angle_sensor_value - i)

        m_rangeAvgCoef = m_iniSettings[QString("RangeAvgCoef")].toUInt(); // set nubmer of nearest neighbors for image size compressing default: 4
        m_rangeAvgCoef = m_rangeAvgCoef == 0 ? 1 : m_rangeAvgCoef;

        m_signalBuffer = new QList<QPair<quint16, QByteArray>>; // construct signal buffer

    }

    quint16 adcBitRateWithSign = m_iniSettings[QString("AdcBitRateWithSign")].toUInt(); // set value of full adc bitrate
    m_maxSignalAmplitude = pow(2, (adcBitRateWithSign - 1));

    // Next part gives opportunity to see the circle diagram like rectangular plot

    m_samplingFrequency = m_iniSettings[QString("SamplingFrequency")].toDouble()/pow(2, m_iniSettings["BoardDecimation"].toDouble());
    m_lightSpeed = g_lightSpeed;
}

void DiagramProcessingThread::signalProcessing(const quint16 &compressedAngleValue, const QByteArray &signal, quint16 rangeAvgCoef, const bool calibrationPlot, quint16 diagramRangeSize, qint16 channelALength)
{
    if (rangeAvgCoef == 0) {rangeAvgCoef = m_rangeAvgCoef == 0 ? 1 : m_rangeAvgCoef;}
    if (diagramRangeSize == 0) {diagramRangeSize = m_diagramRangeSize;}
    if (channelALength == -1) {channelALength = m_adcChannelALength;}
    double signalValue;
    double avgValue;

    double avgBuf;
    quint16 coefBuf;

    bool calibrationProcessing = m_calibrationMode && !calibrationPlot;

    int angleValue;

//    if (calibrationPlot) {
        int deltaCeil = static_cast<int>((m_calibrationAngleOffset)*m_diagramAngleSize/360.0);
        angleValue = (static_cast<int>(compressedAngleValue) + deltaCeil) % m_diagramAngleSize;
        angleValue = angleValue >= 0 ? angleValue : m_diagramAngleSize + angleValue;
//    } else {
//        angleValue = compressedAngleValue;
//    }

    if (calibrationProcessing)
    {
        memcpy(&coefBuf, m_calibrationgAvgCoefficientList->data() + compressedAngleValue*sizeof (quint16), sizeof (quint16));
    }

    for (int xIndex = 0; xIndex < (diagramRangeSize - channelALength); xIndex += rangeAvgCoef) // for every noncompressed range value
    {
        avgValue = 0;
        for (int avgNum = 0; avgNum < rangeAvgCoef; avgNum++){
            memcpy(&signalValue, signal.data() + xIndex*sizeof (double) + avgNum*sizeof (double) + channelALength*sizeof (double), sizeof (double));
            avgValue += signalValue/(rangeAvgCoef*45);
            if (calibrationProcessing) {
                if (coefBuf < m_callibrationAvgCoefficient || !m_exactNumberOfAvg) {
                    memcpy(&avgBuf, m_calibrationDiagram->data() + compressedAngleValue*(diagramRangeSize - channelALength)*sizeof (double) + (xIndex + avgNum)*sizeof (double), sizeof (double));
                    avgBuf += signalValue;
                    memcpy(m_calibrationDiagram->data() + compressedAngleValue*(diagramRangeSize - channelALength)*sizeof (double) + (xIndex + avgNum)*sizeof (double), &avgBuf, sizeof (double));
                }
            }
        }

        m_mutex->lock();
        avgValue = avgValue*m_intesityCoefficient + m_brightnessCoefficient;
        avgValue = avgValue > 0 ? 0 : avgValue;
        avgValue = avgValue > m_threshold ? avgValue : -1;
        m_mutex->unlock();

        for (int beamIndex = 0; beamIndex < m_beamIndexesX->at(angleValue).at(xIndex/rangeAvgCoef).size(); beamIndex++) // for every transormated coordinate in current angle and range
        {
            m_mutex->lock();
            m_colorMap->data()->setCell(m_beamIndexesX->at(angleValue).at(xIndex/rangeAvgCoef).at(beamIndex),  // transformated coordinate X
                                        m_beamIndexesY->at(angleValue).at(xIndex/rangeAvgCoef).at(beamIndex),  // transformated coordinate Y
                                        (avgValue));                      // amplitude value
            // and normalize to max signal amplitude
            m_mutex->unlock();
        }
    }

    if (calibrationProcessing) {
        coefBuf++;
        memcpy(m_calibrationgAvgCoefficientList->data() + compressedAngleValue*sizeof (quint16), &coefBuf, sizeof (quint16));
        double calibrationProgress = 0.0;
        quint16 exitCount = 0;
        for (unsigned long coefNum = 0; coefNum < m_calibrationgAvgCoefficientList->size()/sizeof (quint16); coefNum++) {
            memcpy(&coefBuf, m_calibrationgAvgCoefficientList->data() + coefNum*sizeof (quint16), sizeof (quint16));
            coefBuf = coefBuf/m_callibrationAvgCoefficient;
            if (coefBuf >= 1) {
                coefBuf = 1;
            } else {
                exitCount++;
            }
            calibrationProgress += (static_cast<double>(coefBuf)/static_cast<double>(m_diagramAngleSize))*100.0;
        }

        emit SignalCalibrationProgress(static_cast<int>(calibrationProgress));

        if (exitCount == 0) {
            SlotFinishCalibration();
        }
    }

}

void DiagramProcessingThread::startWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = true;
    m_mutex->unlock();

    if (!isRunning()) {
        start();
    }
}

void DiagramProcessingThread::stopWorkingThread()
{
    m_mutex->lock();
    m_workingThreadEnable = false;
    m_mutex->unlock();
}

// Slot for saving new signal data in buffer
void DiagramProcessingThread::SlotProcessData(const QByteArray &signal, const quint16 &angleValue)
{
    m_mutex->lock();
    m_signalBuffer->append(QPair<quint16, QByteArray>((m_angleMask & angleValue) >> m_angleBitOffset, signal));
    m_mutex->unlock();
}

// Slot for replot circle diagram with timer
void DiagramProcessingThread::SlotReplot()
{
    emit SignalDataToDraw();
    m_plotTimer->start(m_plotTimerDelay);
}

void DiagramProcessingThread::SlotClearImage()
{
    m_mutex->lock();
    for (int xIndex = 0; xIndex < m_colorMap->data()->keySize(); xIndex++) {
        for (int yIndex = 0; yIndex < m_colorMap->data()->valueSize(); yIndex++) {
            m_colorMap->data()->setCell(xIndex, yIndex, -1);
        }
    }
    m_mutex->unlock();
}

void DiagramProcessingThread::SlotStopCalibration()
{
    m_mutex->lock();
    m_stop = true;
    m_mutex->unlock();
    SlotFinishCalibration();
}

void DiagramProcessingThread::SlotStartCalibration(const quint16 &callibrationAvgCoefficient)
{
    m_mutex->lock();

    m_calibrationDiagram->clear();
    m_calibrationDiagram->append(m_diagramAngleSize*(m_diagramRangeSize - m_adcChannelALength)*sizeof(double), 0);

    m_calibrationgAvgCoefficientList->clear();
    m_calibrationgAvgCoefficientList->append(m_diagramAngleSize*sizeof (quint16), 0);

    m_callibrationAvgCoefficient = callibrationAvgCoefficient;
    m_stop = false;
    m_mutex->unlock();
    SlotClearImage();
    startWorkingThread();
    emit SignalCalibrationStarted();
}

void DiagramProcessingThread::SlotFinishCalibration()
{
    emit SignalCalibrationFinished();

    stopWorkingThread();

    double amplValue = 0;
    quint16 coefBuf = m_callibrationAvgCoefficient;

    QByteArray beam;
    beam.resize((m_diagramRangeSize - m_adcChannelALength)*sizeof (double));

    for (int currentAngleValue = 0; currentAngleValue < m_diagramAngleSize; currentAngleValue++) {

        for (int xIndex = 0; xIndex < (m_diagramRangeSize - m_adcChannelALength); xIndex++) // for every noncompressed range value
        {
            memcpy(&amplValue, m_calibrationDiagram->data() + currentAngleValue*(m_diagramRangeSize - m_adcChannelALength)*sizeof (double) + xIndex*sizeof (double), sizeof (double));

            if (!m_exactNumberOfAvg) {
                memcpy(&coefBuf, m_calibrationgAvgCoefficientList->data() + currentAngleValue*sizeof (quint16), sizeof (quint16));
            }

            amplValue /= coefBuf;

            memcpy(beam.data() + xIndex*sizeof (double), &amplValue, sizeof (double));
         }

        signalProcessing(currentAngleValue, beam, 0, true, m_diagramRangeSize - m_adcChannelALength, 0);

    }
}

void DiagramProcessingThread::SlotSamplingFrequencyChanged(const int &samplingFrequency)
{
    m_samplingFrequency = samplingFrequency;

    m_mutex->lock();

    m_colorMap->data()->setSize(2*(m_diagramRangeSize)/m_rangeAvgCoef, 2*(m_diagramRangeSize)/m_rangeAvgCoef);
    m_colorMap->data()->setRange(QCPRange(-((m_diagramRangeSize)/(2*m_samplingFrequency))*m_lightSpeed, ((m_diagramRangeSize)/(2*m_samplingFrequency))*m_lightSpeed),
                                 QCPRange(-((m_diagramRangeSize)/(2*m_samplingFrequency))*m_lightSpeed, ((m_diagramRangeSize)/(2*m_samplingFrequency))*m_lightSpeed));

    m_mutex->unlock();

    emit SignalDataToDraw();
}

void DiagramProcessingThread::SlotSetCallibrationAvgCoefficient(const bool &exactNumberOfAvg)
{
    m_mutex->lock();
    m_exactNumberOfAvg = exactNumberOfAvg;
    m_mutex->unlock();
}

void DiagramProcessingThread::SlotAngleOffsetChanged(const double &offset)
{

    m_calibrationAngleOffset = offset - m_staticAngleOffset;
    SlotFinishCalibration();
}

void DiagramProcessingThread::SlotAdcChannelALengthChanged(const quint16 &length)
{
    m_adcChannelALength = length;
    SlotSamplingFrequencyChanged(m_samplingFrequency);
}

void DiagramProcessingThread::SLotSetIntensityCoefficient(const double &coefficient)
{
    m_mutex->lock();
    m_intesityCoefficient = coefficient;
    m_mutex->unlock();
}

void DiagramProcessingThread::SLotSetBrightnessCoefficient(const double &coefficient)
{
    m_mutex->lock();
    m_brightnessCoefficient = coefficient;
    m_mutex->unlock();
}

void DiagramProcessingThread::SLotSetThreshold(const double &value)
{
    m_mutex->lock();
    m_threshold = value;
    m_mutex->unlock();
}
