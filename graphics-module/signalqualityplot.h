/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef SIGNALQUALITYPLOT_H
#define SIGNALQUALITYPLOT_H

#include <QWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QLabel>
#include <QProgressBar>
#include <QTimer>
#include <QRadioButton>
#include "shared-libs/qcustomplot.h"

#include "graphics-module/signalqualityprocessingthread.h"

class SignalQualityPlot : public QWidget
{
    Q_OBJECT

    QCustomPlot *m_signalQualityPlot;
    QCustomPlot *m_signalAvgPlot;
    QSpinBox *m_signalAvgNumber;
    QCheckBox *m_showAbs;
    QCheckBox *m_showReal;
    QCheckBox *m_showImag;
    QCheckBox *m_holdSignal;
    QPushButton *m_setSignalAvgNumber;
    QPushButton *m_signalRescale;
    QPushButton *m_signalReset;
    QSpinBox *m_windowSize;
    QPushButton *m_set;
    QSpinBox *m_ceilNumber;
    QSpinBox *m_ceilNumberSecond;
    QCheckBox *m_visibleLimits;
    QPushButton *m_setCeilNumber;
    QPushButton *m_start;
    QPushButton *m_stop;
    QPushButton *m_rescale;
    QPushButton *m_reset;
    QCheckBox *m_avgLine;
    QProgressBar *m_progressBar;
//    QRadioButton *m_adcA;
//    QRadioButton *m_adcB;
    QComboBox *m_adcChannel;
    QSpinBox *m_analisysPositionOffset;
    int m_analisysPositionOffsetValue;
    QPushButton *m_setOffset;

    QDoubleSpinBox *m_avgAmplitude;
    QDoubleSpinBox *m_avgAmplitudeSD;
    QDoubleSpinBox *m_circleCoefficient;
    QDoubleSpinBox *m_complexEqualityCoefficient;

    QToolButton *m_refresh;
    QToolButton *m_settings;

    QComboBox *m_channelAlengthChoise;

    SignalQualityProcessingThread *m_processingThread;

    QTimer *m_replotTimer;
    int m_replotTime = static_cast<int>((1/50)*1000);

    quint16 m_channelAlength = 32;
    quint16 m_beamLength = 2048;

    QMap<QString, QString> m_iniSettings;

public:
    explicit SignalQualityPlot(QMap<QString, QString> iniSettings = QMap<QString, QString>(), QWidget *parent = nullptr);
    ~SignalQualityPlot();

    QWidget *m_plotWidget;

    void setFullPalette(const QPalette &palette);

    void setIniSettings(QMap<QString, QString> &iniSettings);
    QMap<QString, QString> getIniSettings();

public slots:
    void SlotProgressStatus(const quint8 &progress);
    void resizeEvent(QResizeEvent *event) override;

    void SlotStartClicked();
    void SlotStopClicked();

private slots:
    void SlotRescaleClicked();
    void SlotRescaleAvgClicked();
    void SlotSetClicked();
    void SlotSetCeilsClicked();
    void SlotSetAvgClicked();
    void SlotSetOffsetClicked();
    void SlotSetChannelALengthChanged(const int &index);
    void SlotOpenSettings();

    void SlotSetAvgAmplitude(const double &value);
    void SlotSetAvgAmplitudeSD(const double &value);
    void SlotSetCircleCoefficient(const double &value);
    void SlotSetComplexEqualityCoefficient(const double &value);

//    void SlotAdcAcheckStateChanged(const bool &checkState);
//    void SlotAdcBcheckStateChanged(const bool &checkState);

    void SlotAdcChannelChanged(const int &index);

    void SlotReplot();

signals:
    void SignalDataReceived(const QByteArray &realValues, const QByteArray &imagValues, const QByteArray &absValues);

};

#endif // SIGNALQUALITYPLOT_H
