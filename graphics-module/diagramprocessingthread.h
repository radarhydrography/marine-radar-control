﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef DIAGRAMPROCESSINGTHREAD_H
#define DIAGRAMPROCESSINGTHREAD_H

#include <QObject>
#include <QThread>
#include <QMutex>

#include "shared-libs/qcustomplot.h"

class DiagramProcessingThread : public QThread
{
    Q_OBJECT

    // Classic variables when you work with threads
    QMutex *m_mutex;
    bool m_workingThreadEnable;

    // Object for circle diagram control
    QCPColorMap *m_colorMap;

    // Signal buffer to process
    QList<QPair<quint16, QByteArray>> *m_signalBuffer;
    QByteArray *m_calibrationDiagram;
    QByteArray *m_calibrationgAvgCoefficientList;

    // Number of angles (drawing directions)
    quint16 m_diagramAngleSize;
    // Number of samples in one direction
    quint16 m_diagramRangeSize = 2048; // change only in case of the communication protocol (beam/frame size) will be changed
    quint16 m_adcChannelALength = 32;

    double m_intesityCoefficient = 1.0;
    double m_brightnessCoefficient = 0;
    double m_threshold = -1.0;

    // Timer for limit number of replots (against freezing)
    QTimer *m_plotTimer;
    // Time between replot (1/MaxFPS)
    quint16 m_plotTimerDelay;

    // Circle beam indexes containers
    QList<QList<QList<int>>> *m_beamIndexesX;
    QList<QList<QList<int>>> *m_beamIndexesY;

    // Size of insignificant rank of angle value
    quint16 m_angleCut;
    // BitMask to cut insignificant rank of angle value
    quint16 m_angleMask = 0b0111111111111111; //dont't change it!!!
    // Size of angle value offset
    quint16 m_angleBitOffset;

    // Max signal amplitude
    quint16 m_maxSignalAmplitude;

    // Nubmer of nearest neighbors for image size compressing
    quint16 m_rangeAvgCoef = 1;
    bool m_calibrationMode;
    bool m_stop;
    bool m_exactNumberOfAvg;
    quint16 m_callibrationAvgCoefficient = 1;

    int m_beamLength = 2048;
    double m_samplingFrequency = 80000000;
    double m_lightSpeed = 299792458;

    double m_calibrationAngleOffset = 0;
    double m_staticAngleOffset = 0;

    QMap<QString, QString> m_iniSettings;

public:
    DiagramProcessingThread(QCPColorMap *colorMap, QMap<QString, QString> iniSettings = QMap<QString, QString>(), QObject *parent = nullptr);
    ~DiagramProcessingThread();

    // Classic...
    void run() override;

    void setIniSettings(QMap<QString, QString> &iniSettings);

private:

    // Method for complete (beam) signal processing
    void signalProcessing(const quint16 &compressedAngleValue, const QByteArray &signal, quint16 rangeAvgCoef = 0, const bool calibrationPlot = false, quint16 diagramRangeSize = 0, qint16 channelALength = -1);

public slots:
    // Clearly
    void startWorkingThread();
    void stopWorkingThread();

    // Slot for recieving signals to add it to diagram
    void SlotProcessData(const QByteArray &signal, const quint16 &angleValue);

    // Slot for repot data on circle diagram with timer
    void SlotReplot();

    void SlotClearImage();

    void SlotStopCalibration();
    void SlotStartCalibration(const quint16 &callibrationAvgCoefficient);
    void SlotFinishCalibration();

    void SlotSamplingFrequencyChanged(const int &samplingFrequency);

    void SlotSetCallibrationAvgCoefficient(const bool &exactNumberOfAvg);

    void SlotAngleOffsetChanged(const double &offset);
    void SlotAdcChannelALengthChanged(const quint16 &length);

    void SLotSetIntensityCoefficient(const double &coefficient);
    void SLotSetBrightnessCoefficient(const double &coefficient);
    void SLotSetThreshold(const double &value);

signals:
    // Clearly
    void poolStatus(const bool &status);
    // Signal to replot data on parent (circle) diagram
    void SignalDataToDraw();
    void SignalCalibrationStarted();
    void SignalCalibrationFinished();

    void SignalCalibrationProgress(const int &progressValue);

};

#endif // DIAGRAMPROCESSINGTHREAD_H
