/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#include "startdialog.h"

#include <QFileDialog>
#include <QGridLayout>
#include <QLabel>

#include "shared-libs/inireader.h"
#include "shared-libs/global.h"

StartDialog::StartDialog(QWidget *parent) : QWidget(parent)
{
    m_showTheWayToIniFileMessageBox = ShowTheWayToIniFileMessageBox();
    m_closeWindowMessageBox = CloseWindowMessageBox();
    m_configurationFileIsCorruptedMessageBox = ConfigurationFileIsCorruptedMessageBox();
    m_newIniFileWidget = NewIniFileWidget();
    m_cancelNewIniFileDialog = CancelNewIniFileDialog();
}

QMessageBox *StartDialog::ShowTheWayToIniFileMessageBox()
{
    QMessageBox *showTheWayToIniFile = new QMessageBox;
    showTheWayToIniFile->setWindowTitle(QString("Settings File"));
    showTheWayToIniFile->setText(QString("Settings file not found or file is corrupted"));
    showTheWayToIniFile->setInformativeText(QString("Do you want to create a new file?"));
    showTheWayToIniFile->setStandardButtons(QMessageBox::Yes | QMessageBox::Open | QMessageBox::Cancel);

    return showTheWayToIniFile;
}

QMessageBox *StartDialog::CloseWindowMessageBox()
{
    QMessageBox *closeWindowMessageBox = new QMessageBox;
    closeWindowMessageBox->setWindowTitle(QString("Close the Application"));
    closeWindowMessageBox->setText(QString("Do you want to close the application?"));
    closeWindowMessageBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    return closeWindowMessageBox;
}

QMessageBox *StartDialog::ConfigurationFileIsCorruptedMessageBox()
{
    QMessageBox *configurationFileIsCorruptedMessageBox = new QMessageBox;
    configurationFileIsCorruptedMessageBox->setWindowTitle(QString("Error Ocqured"));
    configurationFileIsCorruptedMessageBox->setText(QString("File Is Wrong!"));
    configurationFileIsCorruptedMessageBox->setStandardButtons(QMessageBox::Cancel);

    return configurationFileIsCorruptedMessageBox;
}

QDialog *StartDialog::NewIniFileWidget()
{
    QDialog *newIniFileWidget = new QDialog;
    newIniFileWidget->setWindowTitle(QString("Create a new Settings file"));

    QGridLayout *mainLayout = new QGridLayout;

    m_saveNewIniFilePushButton = new QPushButton(QString("Save"));
    m_saveNewIniFilePushButtonAs = new QPushButton(QString("Save As"));
    m_cancelNewIniFilePushButton = new QPushButton(QString("Cancel"));

    connect(m_saveNewIniFilePushButton, &QPushButton::clicked, this, &StartDialog::SlotAcceptNewIniFileDialog);
    connect(m_saveNewIniFilePushButtonAs, &QPushButton::clicked, this, &StartDialog::SlotDoneNewIniFileDialog);
    connect(m_cancelNewIniFilePushButton, &QPushButton::clicked, this, &StartDialog::SlotCancelNewIniFileDialog);

    m_defaultAutostartComboBox = new QComboBox;
    QStringList defaultAutostartStringList({QString("Disabled"), QString("Enabled")});
//    QStringList defaultAutostartStringList({QString("Disabled"), QString("Schedule"), QString("Instant Data Acquisition")});
    m_defaultAutostartComboBox->addItems(defaultAutostartStringList);

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(new QLabel(QString("Autostart:")));
    buttonsLayout->addWidget(m_defaultAutostartComboBox);
    buttonsLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    buttonsLayout->addWidget(m_saveNewIniFilePushButton);
    buttonsLayout->addWidget(m_saveNewIniFilePushButtonAs);
    buttonsLayout->addWidget(m_cancelNewIniFilePushButton);

    m_defaultIniFileNameCheckBox = new QCheckBox(QString("default"));
    m_defaultIniFileNameLineEdit = new QLineEdit;
    m_defaultIniFileNameLineEdit->setMinimumWidth(240);
    m_defaultIniFileNameDialog = new QPushButton(QString("Open"));
    connect(m_defaultIniFileNameCheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultIniFileNameCheckBoxStateChanged);
    connect(m_defaultIniFileNameDialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultIniFileNameDialogClicked);

    m_defaultLogsCheckBox = new QCheckBox(QString("default"));
    m_defaultLogsLineEdit = new QLineEdit;
    m_defaultLogsLineEdit->setMinimumWidth(240);
    m_defaultLogsDialog = new QPushButton(QString("Open"));
    connect(m_defaultLogsCheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultLogsCheckBoxStateChanged);
    connect(m_defaultLogsDialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultLogsDialogClicked);

    m_defaultIconsCheckBox = new QCheckBox(QString("default"));
    m_defaultIconsLineEdit = new QLineEdit;
    m_defaultIconsLineEdit->setMinimumWidth(240);
    m_defaultIconsDialog = new QPushButton(QString("Open"));
    connect(m_defaultIconsCheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultIconsCheckBoxStateChanged);
    connect(m_defaultIconsDialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultIconsDialogClicked);

    m_defaultScheduleCheckBox = new QCheckBox(QString("default"));
    m_defaultScheduleLineEdit = new QLineEdit;
    m_defaultScheduleLineEdit->setMinimumWidth(240);
    m_defaultScheduleDialog = new QPushButton(QString("Open"));
    connect(m_defaultScheduleCheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultScheduleCheckBoxStateChanged);
    connect(m_defaultScheduleDialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultScheduleDialogClicked);

    m_defaultDataFolder0CheckBox = new QCheckBox(QString("default"));
    m_defaultDataFolder0LineEdit = new QLineEdit;
    m_defaultDataFolder0LineEdit->setMinimumWidth(240);
    m_defaultDataFolder0Dialog = new QPushButton(QString("Open"));
    connect(m_defaultDataFolder0CheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultDataFolder0CheckBoxStateChanged);
    connect(m_defaultDataFolder0Dialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultDataFolder0DialogClicked);

    m_defaultDataFolder1CheckBox = new QCheckBox(QString("default"));
    m_defaultDataFolder1LineEdit = new QLineEdit;
    m_defaultDataFolder1LineEdit->setMinimumWidth(240);
    m_defaultDataFolder1Dialog = new QPushButton(QString("Open"));
    connect(m_defaultDataFolder1CheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultDataFolder1CheckBoxStateChanged);
    connect(m_defaultDataFolder1Dialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultDataFolder1DialogClicked);

    m_defaultDataFolder2CheckBox = new QCheckBox(QString("default"));
    m_defaultDataFolder2LineEdit = new QLineEdit;
    m_defaultDataFolder2LineEdit->setMinimumWidth(240);
    m_defaultDataFolder2Dialog = new QPushButton(QString("Open"));
    connect(m_defaultDataFolder2CheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultDataFolder2CheckBoxStateChanged);
    connect(m_defaultDataFolder2Dialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultDataFolder2DialogClicked);

    m_defaultDataFolder3CheckBox = new QCheckBox(QString("default"));
    m_defaultDataFolder3LineEdit = new QLineEdit;
    m_defaultDataFolder3LineEdit->setMinimumWidth(240);
    m_defaultDataFolder3Dialog = new QPushButton(QString("Open"));
    connect(m_defaultDataFolder3CheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultDataFolder3CheckBoxStateChanged);
    connect(m_defaultDataFolder3Dialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultDataFolder3DialogClicked);

    m_defaultDataFolder4CheckBox = new QCheckBox(QString("default"));
    m_defaultDataFolder4LineEdit = new QLineEdit;
    m_defaultDataFolder4LineEdit->setMinimumWidth(240);
    m_defaultDataFolder4Dialog = new QPushButton(QString("Open"));
    connect(m_defaultDataFolder4CheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultDataFolder4CheckBoxStateChanged);
    connect(m_defaultDataFolder4Dialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultDataFolder4DialogClicked);

    m_defaultDataFolder5CheckBox = new QCheckBox(QString("default"));
    m_defaultDataFolder5LineEdit = new QLineEdit;
    m_defaultDataFolder5LineEdit->setMinimumWidth(240);
    m_defaultDataFolder5Dialog = new QPushButton(QString("Open"));
    connect(m_defaultDataFolder5CheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultDataFolder5CheckBoxStateChanged);
    connect(m_defaultDataFolder5Dialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultDataFolder5DialogClicked);

    m_defaultDataFolder6CheckBox = new QCheckBox(QString("default"));
    m_defaultDataFolder6LineEdit = new QLineEdit;
    m_defaultDataFolder6LineEdit->setMinimumWidth(240);
    m_defaultDataFolder6Dialog = new QPushButton(QString("Open"));
    connect(m_defaultDataFolder6CheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultDataFolder6CheckBoxStateChanged);
    connect(m_defaultDataFolder6Dialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultDataFolder6DialogClicked);

    m_defaultDataFolder7CheckBox = new QCheckBox(QString("default"));
    m_defaultDataFolder7LineEdit = new QLineEdit;
    m_defaultDataFolder7LineEdit->setMinimumWidth(240);
    m_defaultDataFolder7Dialog = new QPushButton(QString("Open"));
    connect(m_defaultDataFolder7CheckBox, &QCheckBox::stateChanged, this, &StartDialog::SlotDefaultDataFolder7CheckBoxStateChanged);
    connect(m_defaultDataFolder7Dialog, &QPushButton::clicked, this, &StartDialog::SlotDefaultDataFolder7DialogClicked);

    m_defaultIniFileNameCheckBox->click();
    m_defaultLogsCheckBox->click();
    m_defaultIconsCheckBox->click();
    m_defaultScheduleCheckBox->click();
    m_defaultDataFolder0CheckBox->click();
    m_defaultDataFolder1CheckBox->click();
    m_defaultDataFolder2CheckBox->click();
    m_defaultDataFolder3CheckBox->click();
    m_defaultDataFolder4CheckBox->click();
    m_defaultDataFolder5CheckBox->click();
    m_defaultDataFolder6CheckBox->click();
    m_defaultDataFolder7CheckBox->click();

    connect(m_defaultIniFileNameLineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultIniFileNameLineEditTextChanged);
    connect(m_defaultLogsLineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultLogsLineEditTextChanged);
    connect(m_defaultIconsLineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultIconsLineEditTextChanged);
    connect(m_defaultScheduleLineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultScheduleLineEditTextChanged);
    connect(m_defaultDataFolder0LineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultDataFolder0LineEditTextChanged);
    connect(m_defaultDataFolder1LineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultDataFolder1LineEditTextChanged);
    connect(m_defaultDataFolder2LineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultDataFolder2LineEditTextChanged);
    connect(m_defaultDataFolder3LineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultDataFolder3LineEditTextChanged);
    connect(m_defaultDataFolder4LineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultDataFolder4LineEditTextChanged);
    connect(m_defaultDataFolder5LineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultDataFolder5LineEditTextChanged);
    connect(m_defaultDataFolder6LineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultDataFolder6LineEditTextChanged);
    connect(m_defaultDataFolder7LineEdit, &QLineEdit::textChanged, this, &StartDialog::SlotDefaultDataFolder7LineEditTextChanged);

    mainLayout->addWidget(new QLabel(QString("Ini File: ")), 0, 0);
    mainLayout->addWidget(m_defaultIniFileNameLineEdit, 0, 1);
    mainLayout->addWidget(m_defaultIniFileNameDialog, 0, 2);
    mainLayout->addWidget(m_defaultIniFileNameCheckBox, 0, 3);
    mainLayout->addWidget(new QLabel(QString("Logs: ")), 1, 0);
    mainLayout->addWidget(m_defaultLogsLineEdit, 1, 1);
    mainLayout->addWidget(m_defaultLogsDialog, 1, 2);
    mainLayout->addWidget(m_defaultLogsCheckBox, 1, 3);
    mainLayout->addWidget(new QLabel(QString("Icons: ")), 2, 0);
    mainLayout->addWidget(m_defaultIconsLineEdit, 2, 1);
    mainLayout->addWidget(m_defaultIconsDialog, 2, 2);
    mainLayout->addWidget(m_defaultIconsCheckBox, 2, 3);
    mainLayout->addWidget(new QLabel(QString("Schedule: ")), 3, 0);
    mainLayout->addWidget(m_defaultScheduleLineEdit, 3, 1);
    mainLayout->addWidget(m_defaultScheduleDialog, 3, 2);
    mainLayout->addWidget(m_defaultScheduleCheckBox, 3, 3);
    mainLayout->addWidget(new QLabel(QString("Data Folder 0: ")), 4, 0);
    mainLayout->addWidget(m_defaultDataFolder0LineEdit, 4, 1);
    mainLayout->addWidget(m_defaultDataFolder0Dialog, 4, 2);
    mainLayout->addWidget(m_defaultDataFolder0CheckBox, 4, 3);
    mainLayout->addWidget(new QLabel(QString("Data Folder 1: ")), 5, 0);
    mainLayout->addWidget(m_defaultDataFolder1LineEdit, 5, 1);
    mainLayout->addWidget(m_defaultDataFolder1Dialog, 5, 2);
    mainLayout->addWidget(m_defaultDataFolder1CheckBox, 5, 3);
    mainLayout->addWidget(new QLabel(QString("Data Folder 2: ")), 6, 0);
    mainLayout->addWidget(m_defaultDataFolder2LineEdit, 6, 1);
    mainLayout->addWidget(m_defaultDataFolder2Dialog, 6, 2);
    mainLayout->addWidget(m_defaultDataFolder2CheckBox, 6, 3);
    mainLayout->addWidget(new QLabel(QString("Data Folder 3: ")), 7, 0);
    mainLayout->addWidget(m_defaultDataFolder3LineEdit, 7, 1);
    mainLayout->addWidget(m_defaultDataFolder3Dialog, 7, 2);
    mainLayout->addWidget(m_defaultDataFolder3CheckBox, 7, 3);
    mainLayout->addWidget(new QLabel(QString("Data Folder 4: ")), 8, 0);
    mainLayout->addWidget(m_defaultDataFolder4LineEdit, 8, 1);
    mainLayout->addWidget(m_defaultDataFolder4Dialog, 8, 2);
    mainLayout->addWidget(m_defaultDataFolder4CheckBox, 8, 3);
    mainLayout->addWidget(new QLabel(QString("Data Folder 5: ")), 9, 0);
    mainLayout->addWidget(m_defaultDataFolder5LineEdit, 9, 1);
    mainLayout->addWidget(m_defaultDataFolder5Dialog, 9, 2);
    mainLayout->addWidget(m_defaultDataFolder5CheckBox, 9, 3);
    mainLayout->addWidget(new QLabel(QString("Data Folder 6: ")), 10, 0);
    mainLayout->addWidget(m_defaultDataFolder6LineEdit, 10, 1);
    mainLayout->addWidget(m_defaultDataFolder6Dialog, 10, 2);
    mainLayout->addWidget(m_defaultDataFolder6CheckBox, 10, 3);
    mainLayout->addWidget(new QLabel(QString("Data Folder 7: ")), 11, 0);
    mainLayout->addWidget(m_defaultDataFolder7LineEdit, 11, 1);
    mainLayout->addWidget(m_defaultDataFolder7Dialog, 11, 2);
    mainLayout->addWidget(m_defaultDataFolder7CheckBox, 11, 3);
    mainLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Expanding), 12, 0, 1, 4);
    mainLayout->addLayout(buttonsLayout, 13, 0, 1, 4);

    newIniFileWidget->setLayout(mainLayout);

    return newIniFileWidget;
}

QDialog *StartDialog::CancelNewIniFileDialog()
{
    QMessageBox *closeWindowMessageBox = new QMessageBox;
    closeWindowMessageBox->setWindowTitle(QString("Cancel New Settings File Editing"));
    closeWindowMessageBox->setText(QString("Are You Sure?"));
    closeWindowMessageBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    return closeWindowMessageBox;
}

QMap<QString, QString> StartDialog::newBasicSettings()
{
    QMap<QString, QString> basicSettings;

    basicSettings.insert(QString("config"), m_defaultIniFileNameLineEdit->text());

    QString defaultLogsPath(QString(g_tmpPath + g_pathDevider + "logs"));
    if (m_defaultLogsLineEdit->text() == QString("")) {
        basicSettings.insert(QString("logs"),defaultLogsPath);
    } else {
        basicSettings.insert(QString("logs"), m_defaultLogsLineEdit->text());
    }

    if (m_defaultLogsLineEdit->text() == defaultLogsPath) {
        QDir dir(defaultLogsPath);
        if (!dir.exists()) {
            dir.mkdir(defaultLogsPath);
        }
    }

    basicSettings.insert(QString("icons"), m_defaultIconsLineEdit->text());
    basicSettings.insert(QString("tr"), QString("resources/tr/en.tr"));
    basicSettings.insert(QString("schedule"), m_defaultScheduleLineEdit->text());
    basicSettings.insert(QString("autostart"), QString::number(m_defaultAutostartComboBox->currentIndex()));
    basicSettings.insert(QString("dataFolder0"), m_defaultDataFolder0LineEdit->text());
    basicSettings.insert(QString("dataFolder1"), m_defaultDataFolder1LineEdit->text());
    basicSettings.insert(QString("dataFolder2"), m_defaultDataFolder2LineEdit->text());
    basicSettings.insert(QString("dataFolder3"), m_defaultDataFolder3LineEdit->text());
    basicSettings.insert(QString("dataFolder4"), m_defaultDataFolder4LineEdit->text());
    basicSettings.insert(QString("dataFolder5"), m_defaultDataFolder5LineEdit->text());
    basicSettings.insert(QString("dataFolder6"), m_defaultDataFolder6LineEdit->text());
    basicSettings.insert(QString("dataFolder7"), m_defaultDataFolder7LineEdit->text());

    return basicSettings;
}

bool StartDialog::checkAllLinesForNoErrors()
{
    bool noErrors = true;

    QFile file;
    QDir directory;
    QString text;

    text = m_defaultIniFileNameLineEdit->text();
    noErrors = noErrors && !text.isEmpty() && file.exists(text);
    text = m_defaultLogsLineEdit->text();
    noErrors = noErrors && !text.isEmpty() && directory.exists(text);
    text = m_defaultIconsLineEdit->text();
    noErrors = noErrors && !text.isEmpty() && directory.exists(text);
    text = m_defaultScheduleLineEdit->text();
    noErrors = noErrors && (text.isEmpty() || file.exists(text));
    text = m_defaultDataFolder0LineEdit->text();
    noErrors = noErrors && (text.isEmpty() || directory.exists(text));
    text = m_defaultDataFolder1LineEdit->text();
    noErrors = noErrors && (text.isEmpty() || directory.exists(text));
    text = m_defaultDataFolder2LineEdit->text();
    noErrors = noErrors && (text.isEmpty() || directory.exists(text));
    text = m_defaultDataFolder3LineEdit->text();
    noErrors = noErrors && (text.isEmpty() || directory.exists(text));
    text = m_defaultDataFolder4LineEdit->text();
    noErrors = noErrors && (text.isEmpty() || directory.exists(text));
    text = m_defaultDataFolder5LineEdit->text();
    noErrors = noErrors && (text.isEmpty() || directory.exists(text));
    text = m_defaultDataFolder6LineEdit->text();
    noErrors = noErrors && (text.isEmpty() || directory.exists(text));
    text = m_defaultDataFolder7LineEdit->text();
    noErrors = noErrors && (text.isEmpty() || directory.exists(text));

    m_saveNewIniFilePushButton->setEnabled(noErrors);
    m_saveNewIniFilePushButtonAs->setEnabled(noErrors);

    return noErrors;
}

QString StartDialog::stratDialog()
{
    QString filePath = QString("");

    int limit = 10;
    int counter = 0;
    while (counter < limit) {
        counter++;
        int retNewFile = m_showTheWayToIniFileMessageBox->exec();
        if (retNewFile == QMessageBox::Yes) {
            m_newIniFileWidget->exec();
            if (m_newIniFileName != QString("")) {
                QMap<QString, QString> basicSettings = newBasicSettings();
                IniReader::saveBasicConfiguration(basicSettings, m_newIniFileName);
                filePath = m_newIniFileName;
                break;
            }
        } else if (retNewFile == QMessageBox::Open) {
            filePath = QFileDialog::getOpenFileName(this, QString("Open Settings File"), QString(""), QString("*.ini"), nullptr, QFileDialog::DontUseNativeDialog);
            QMap<QString, QString> basicConfiguration;
            if (filePath != QString("")) {
                basicConfiguration = IniReader::readBasicConfigurationFile(filePath);
            }
            if (IniReader::checkBasicConfiguration(basicConfiguration)) {
                m_configurationFileIsCorruptedMessageBox->exec();
                filePath = QString("");
            } else {
                IniReader::saveRedirectionLink(filePath);
                break;
            }
        } else if (retNewFile == QMessageBox::Abort) {
            break;
        } else {
            int answer = m_closeWindowMessageBox->exec();
            if (answer == QMessageBox::Yes) {
                break;
            }
        }
    }

    if (counter == limit) {

    }

    return filePath;
}

void StartDialog::SlotAcceptNewIniFileDialog()
{
    m_newIniFileName = QString("resources/settings/radar.ini");
    m_newIniFileWidget->accept();
}

void StartDialog::SlotDoneNewIniFileDialog()
{
    QString fileSavingName = QFileDialog::getSaveFileName(this, QString("Saving Settings File"), QString(""), QString("*.ini"), nullptr, QFileDialog::DontUseNativeDialog);
    if (fileSavingName != QString("")) {
        fileSavingName.remove(".ini");
        fileSavingName += QString(".ini");
        IniReader::saveRedirectionLink(fileSavingName);
        m_newIniFileName = fileSavingName;
        m_newIniFileWidget->done(2);
    }

}

void StartDialog::SlotCancelNewIniFileDialog()
{
    int answer = m_cancelNewIniFileDialog->exec();

    if (answer == QMessageBox::Yes) {
        m_newIniFileName = QString("");
        m_newIniFileWidget->reject();
    }
}

void StartDialog::SlotDefaultIniFileNameCheckBoxStateChanged(const bool &checkState)
{
    m_defaultIniFileNameLineEdit->setReadOnly(checkState);
    m_defaultIniFileNameDialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultIniFileNameLineEdit->setText(QString("resources/settings/settings.ini"));
    }
}

void StartDialog::SlotDefaultIniFileNameLineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultIniFileNameLineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultIniFileNameDialogClicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, QString("Ini Settings File"), QString(""), QString("*.ini"), nullptr, QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultIniFileNameLineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultLogsCheckBoxStateChanged(const bool &checkState)
{
    m_defaultLogsLineEdit->setReadOnly(checkState);
    m_defaultLogsDialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultLogsLineEdit->setText(QString(g_tmpPath + g_pathDevider + "logs"));
    }
}

void StartDialog::SlotDefaultLogsLineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultLogsLineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultLogsDialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Logs Folder"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultLogsLineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultIconsCheckBoxStateChanged(const bool &checkState)
{
    m_defaultIconsLineEdit->setReadOnly(checkState);
    m_defaultIconsDialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultIconsLineEdit->setText(QString("resources/icons/"));
    }
}

void StartDialog::SlotDefaultIconsLineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultIconsLineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultIconsDialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Icons Resource Folder"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultIconsLineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultScheduleCheckBoxStateChanged(const bool &checkState)
{
    m_defaultScheduleLineEdit->setReadOnly(checkState);
    m_defaultScheduleDialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultScheduleLineEdit->setText(QString(""));
    }
}

void StartDialog::SlotDefaultScheduleLineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultScheduleLineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultScheduleDialogClicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, QString("Schedule File"), QString(""), QString("*.sc"), nullptr, QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultScheduleLineEdit->setText(fileName);
    }
}


void StartDialog::SlotDefaultDataFolder0CheckBoxStateChanged(const bool &checkState)
{
    m_defaultDataFolder0LineEdit->setReadOnly(checkState);
    m_defaultDataFolder0Dialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultDataFolder0LineEdit->setText(QString("/tmp/Radar_Control/"));
    }
}

void StartDialog::SlotDefaultDataFolder0LineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultDataFolder0LineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultDataFolder0DialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Data Saving Folder 0"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultDataFolder0LineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultDataFolder1CheckBoxStateChanged(const bool &checkState)
{
    m_defaultDataFolder1LineEdit->setReadOnly(checkState);
    m_defaultDataFolder1Dialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultDataFolder1LineEdit->setText(QString(""));
    }
}

void StartDialog::SlotDefaultDataFolder1LineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultDataFolder1LineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultDataFolder1DialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Data Saving Folder 1"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultDataFolder1LineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultDataFolder2CheckBoxStateChanged(const bool &checkState)
{
    m_defaultDataFolder2LineEdit->setReadOnly(checkState);
    m_defaultDataFolder2Dialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultDataFolder2LineEdit->setText(QString(""));
    }
}

void StartDialog::SlotDefaultDataFolder2LineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultDataFolder2LineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultDataFolder2DialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Data Saving Folder 2"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultDataFolder2LineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultDataFolder3CheckBoxStateChanged(const bool &checkState)
{
    m_defaultDataFolder3LineEdit->setReadOnly(checkState);
    m_defaultDataFolder3Dialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultDataFolder3LineEdit->setText(QString(""));
    }
}

void StartDialog::SlotDefaultDataFolder3LineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultDataFolder3LineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultDataFolder3DialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Data Saving Folder 3"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultDataFolder3LineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultDataFolder4CheckBoxStateChanged(const bool &checkState)
{
    m_defaultDataFolder4LineEdit->setReadOnly(checkState);
    m_defaultDataFolder4Dialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultDataFolder4LineEdit->setText(QString(""));
    }
}

void StartDialog::SlotDefaultDataFolder4LineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultDataFolder4LineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultDataFolder4DialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Data Saving Folder 4"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultDataFolder4LineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultDataFolder5CheckBoxStateChanged(const bool &checkState)
{
    m_defaultDataFolder5LineEdit->setReadOnly(checkState);
    m_defaultDataFolder5Dialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultDataFolder5LineEdit->setText(QString(""));
    }
}

void StartDialog::SlotDefaultDataFolder5LineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultDataFolder5LineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultDataFolder5DialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Data Saving Folder 5"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultDataFolder5LineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultDataFolder6CheckBoxStateChanged(const bool &checkState)
{
    m_defaultDataFolder6LineEdit->setReadOnly(checkState);
    m_defaultDataFolder6Dialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultDataFolder6LineEdit->setText(QString(""));
    }
}

void StartDialog::SlotDefaultDataFolder6LineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultDataFolder6LineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultDataFolder6DialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Data Saving Folder 6"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultDataFolder6LineEdit->setText(fileName);
    }
}

void StartDialog::SlotDefaultDataFolder7CheckBoxStateChanged(const bool &checkState)
{
    m_defaultDataFolder7LineEdit->setReadOnly(checkState);
    m_defaultDataFolder7Dialog->setEnabled(!checkState);
    if (checkState) {
        m_defaultDataFolder7LineEdit->setText(QString(""));
    }
}

void StartDialog::SlotDefaultDataFolder7LineEditTextChanged(const QString &text)
{
    checkAllLinesForNoErrors();
    m_defaultDataFolder7LineEdit->setToolTip(text);
}

void StartDialog::SlotDefaultDataFolder7DialogClicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, QString("Data Saving Folder 7"), QString(""), QFileDialog::DontUseNativeDialog);
    if (fileName != QString("")) {
        m_defaultDataFolder7LineEdit->setText(fileName);
    }
}
