﻿/**************************************************************************************
 **************************************************************************************
 * *                                                                                * *
 * *                                     HYDROS                                     * *
 * *                                                                                * *
 * *                     Hydrographic Radar Operational Software                    * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                                                                                * *
 * *    HYDROS is a radar control and data acquisition software that is made        * *
 * *    to be used for scientific purposes in the field of radar hydrography.       * *
 * *                                                                                * *
 * *                                                                                * *
 * *    This file is a part of HYDROS program that is free software:                * *
 * *    you can redistribute it and/or modify it under the terms of the             * *
 * *    GNU General Public License as published by the Free Software Foundation,    * *
 * *    either version 3 of the License, or (at your option) any later version.     * *
 * *                                                                                * *
 * *    This program is distributed in the hope that it will be useful,             * *
 * *    but WITHOUT ANY WARRANTY; without even the implied warranty of              * *
 * *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                        * *
 * *    See the GNU General Public License for more details.                        * *
 * *                                                                                * *
 * *    You should have received a copy of the GNU General Public License           * *
 * *    along with this program.  If not, see <http://www.gnu.org/licenses/>.       * *
 * *                                                                                * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *    Author: Ivan Serdiukov                                                      * *
 * *    Contact: ivan.serdiukov@hereon.de                                           * *
 * *    Date: 13.05.2022                                                            * *
 * *    Vesion: 1.0                                                                 * *
 * *    License: GNU General Public License                                         * *
 * *                                                                                * *
 **************************************************************************************
 * *                                                                                * *
 * *                Copyright (c) 2022 Helmholtz Zentrum hereon GmbH                * *
 * *                                                                                * *
 **************************************************************************************
 **************************************************************************************/

#ifndef STARTDIALOG_H
#define STARTDIALOG_H

#include <QObject>
#include <QWidget>
#include <QMessageBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QFileDialog>
#include <QPushButton>
#include <QComboBox>

class StartDialog : public QWidget
{
    Q_OBJECT

    QMessageBox *m_showTheWayToIniFileMessageBox;
    QMessageBox *m_closeWindowMessageBox;
    QMessageBox *m_configurationFileIsCorruptedMessageBox;

    QDialog *m_newIniFileWidget;
    QDialog *m_cancelNewIniFileDialog;
    QString m_newIniFileName;
    QComboBox *m_defaultAutostartComboBox;
    QPushButton *m_saveNewIniFilePushButton;
    QPushButton *m_saveNewIniFilePushButtonAs;
    QPushButton *m_cancelNewIniFilePushButton;
    QCheckBox *m_defaultIniFileNameCheckBox;
    QLineEdit *m_defaultIniFileNameLineEdit;
    QPushButton *m_defaultIniFileNameDialog;
    QCheckBox *m_defaultLogsCheckBox;
    QLineEdit *m_defaultLogsLineEdit;
    QPushButton *m_defaultLogsDialog;
    QCheckBox *m_defaultIconsCheckBox;
    QLineEdit *m_defaultIconsLineEdit;
    QPushButton *m_defaultIconsDialog;
//    QCheckBox *m_defaultTrCheckBox;
//    QLineEdit *m_defaultTrLineEdit;
//    QPushButton *m_defaultTrDialog;
    QCheckBox *m_defaultScheduleCheckBox;
    QLineEdit *m_defaultScheduleLineEdit;
    QPushButton *m_defaultScheduleDialog;
    QCheckBox *m_defaultDataFolder0CheckBox;
    QLineEdit *m_defaultDataFolder0LineEdit;
    QPushButton *m_defaultDataFolder0Dialog;
    QCheckBox *m_defaultDataFolder1CheckBox;
    QLineEdit *m_defaultDataFolder1LineEdit;
    QPushButton *m_defaultDataFolder1Dialog;
    QCheckBox *m_defaultDataFolder2CheckBox;
    QLineEdit *m_defaultDataFolder2LineEdit;
    QPushButton *m_defaultDataFolder2Dialog;
    QCheckBox *m_defaultDataFolder3CheckBox;
    QLineEdit *m_defaultDataFolder3LineEdit;
    QPushButton *m_defaultDataFolder3Dialog;
    QCheckBox *m_defaultDataFolder4CheckBox;
    QLineEdit *m_defaultDataFolder4LineEdit;
    QPushButton *m_defaultDataFolder4Dialog;
    QCheckBox *m_defaultDataFolder5CheckBox;
    QLineEdit *m_defaultDataFolder5LineEdit;
    QPushButton *m_defaultDataFolder5Dialog;
    QCheckBox *m_defaultDataFolder6CheckBox;
    QLineEdit *m_defaultDataFolder6LineEdit;
    QPushButton *m_defaultDataFolder6Dialog;
    QCheckBox *m_defaultDataFolder7CheckBox;
    QLineEdit *m_defaultDataFolder7LineEdit;
    QPushButton *m_defaultDataFolder7Dialog;

public:

    explicit StartDialog(QWidget *parent = nullptr);

    QMessageBox *ShowTheWayToIniFileMessageBox();
    QMessageBox *CloseWindowMessageBox();
    QMessageBox *ConfigurationFileIsCorruptedMessageBox();
    QDialog *NewIniFileWidget();
    QDialog *CancelNewIniFileDialog();

private:

    QMap<QString, QString> newBasicSettings();
    bool checkAllLinesForNoErrors();

public slots:

    QString stratDialog();

private slots:

    void SlotAcceptNewIniFileDialog();
    void SlotDoneNewIniFileDialog();
    void SlotCancelNewIniFileDialog();

    void SlotDefaultIniFileNameCheckBoxStateChanged(const bool &checkState);
    void SlotDefaultIniFileNameLineEditTextChanged(const QString &text);
    void SlotDefaultIniFileNameDialogClicked();
    void SlotDefaultLogsCheckBoxStateChanged(const bool &checkState);
    void SlotDefaultLogsLineEditTextChanged(const QString &text);
    void SlotDefaultLogsDialogClicked();
    void SlotDefaultIconsCheckBoxStateChanged(const bool &checkState);
    void SlotDefaultIconsLineEditTextChanged(const QString &text);
    void SlotDefaultIconsDialogClicked();
//    void SlotDefaultTrCheckBoxStateChanged(const bool &checkState);
//    void SlotDefaultTrLineEditTextChanged(const QString &text);
//    void SlotDefaultTrDialogClicked();
    void SlotDefaultScheduleCheckBoxStateChanged(const bool &checkState);
    void SlotDefaultScheduleLineEditTextChanged(const QString &text);
    void SlotDefaultScheduleDialogClicked();
    void SlotDefaultDataFolder0CheckBoxStateChanged(const bool &checkState);
    void SlotDefaultDataFolder0LineEditTextChanged(const QString &text);
    void SlotDefaultDataFolder0DialogClicked();
    void SlotDefaultDataFolder1CheckBoxStateChanged(const bool &checkState);
    void SlotDefaultDataFolder1LineEditTextChanged(const QString &text);
    void SlotDefaultDataFolder1DialogClicked();
    void SlotDefaultDataFolder2CheckBoxStateChanged(const bool &checkState);
    void SlotDefaultDataFolder2LineEditTextChanged(const QString &text);
    void SlotDefaultDataFolder2DialogClicked();
    void SlotDefaultDataFolder3CheckBoxStateChanged(const bool &checkState);
    void SlotDefaultDataFolder3LineEditTextChanged(const QString &text);
    void SlotDefaultDataFolder3DialogClicked();
    void SlotDefaultDataFolder4CheckBoxStateChanged(const bool &checkState);
    void SlotDefaultDataFolder4LineEditTextChanged(const QString &text);
    void SlotDefaultDataFolder4DialogClicked();
    void SlotDefaultDataFolder5CheckBoxStateChanged(const bool &checkState);
    void SlotDefaultDataFolder5LineEditTextChanged(const QString &text);
    void SlotDefaultDataFolder5DialogClicked();
    void SlotDefaultDataFolder6CheckBoxStateChanged(const bool &checkState);
    void SlotDefaultDataFolder6LineEditTextChanged(const QString &text);
    void SlotDefaultDataFolder6DialogClicked();
    void SlotDefaultDataFolder7CheckBoxStateChanged(const bool &checkState);
    void SlotDefaultDataFolder7LineEditTextChanged(const QString &text);
    void SlotDefaultDataFolder7DialogClicked();

signals:

};

#endif // STARTDIALOG_H
